.class public final Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/common/players/e;
.implements Lcom/google/android/gms/games/ui/common/requests/e;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/ac;

.field private an:Lcom/google/android/gms/games/ui/common/players/d;

.field private ao:Lcom/google/android/gms/games/ui/ac;

.field private ap:Lcom/google/android/gms/games/ui/common/requests/d;

.field private aq:Lcom/google/android/gms/games/ui/common/requests/e;

.field private ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private as:Ljava/lang/String;

.field private at:Lcom/google/android/gms/games/Player;

.field private au:Ljava/util/ArrayList;

.field private av:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    return-void
.end method

.method private as()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->e()I

    move-result v0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 200
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/p;->a(IILandroid/content/Intent;)V

    .line 174
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->at:Lcom/google/android/gms/games/Player;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->an:Lcom/google/android/gms/games/ui/common/players/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/d;->c()V

    .line 179
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->an:Lcom/google/android/gms/games/ui/common/players/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/Player;)V

    .line 152
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    if-eqz v0, :cond_1

    .line 154
    new-instance v0, Lcom/google/android/gms/common/data/p;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/p;-><init>()V

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Lcom/google/android/gms/common/data/b;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 167
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->as()V

    .line 168
    return-void

    .line 156
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/data/p;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/data/p;-><init>(Ljava/util/ArrayList;)V

    .line 157
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 159
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 160
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/data/p;->b(Ljava/lang/Object;)V

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/e;->a(Lcom/google/android/gms/games/Game;)V

    .line 241
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 4

    .prologue
    .line 265
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->at:Lcom/google/android/gms/games/Player;

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->as:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 270
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/s;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 256
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->a(Landroid/content/Intent;)V

    .line 261
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    invoke-interface {v1, p1}, Lcom/google/android/gms/games/ui/common/requests/e;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 189
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->as()V

    .line 192
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/requests/d;->e()I

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    const/16 v2, 0x384

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/ui/n;->setResult(ILandroid/content/Intent;)V

    .line 193
    return-void

    .line 192
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    if-eqz v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/e;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 184
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 245
    const-string v0, "openAllButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/common/data/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/requests/d;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/b;->a([Lcom/google/android/gms/common/data/b;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/e;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 250
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/c;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/c;

    .line 81
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/requests/c;->a()Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 82
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/requests/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->as:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/requests/m;->e()Lcom/google/android/gms/games/ui/common/requests/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->aq:Lcom/google/android/gms/games/ui/common/requests/e;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 88
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->am:Lcom/google/android/gms/games/ui/ac;

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->am:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->Z:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->am:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->Y:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->h(I)V

    .line 93
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/d;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/players/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->an:Lcom/google/android/gms/games/ui/common/players/d;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ar:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->i()I

    move-result v0

    .line 97
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    .line 98
    packed-switch v0, :pswitch_data_0

    .line 112
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 100
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bt:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bu:I

    const-string v2, "openAllButton"

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 115
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/requests/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->as:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 118
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->am:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->an:Lcom/google/android/gms/games/ui/common/players/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ap:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 123
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 125
    if-eqz p1, :cond_0

    .line 126
    const-string v0, "savedStateRemovedIdList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    .line 128
    const-string v0, "savedStateRemoveCluster"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    .line 130
    :cond_0
    return-void

    .line 107
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bv:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->ao:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->e(Landroid/os/Bundle;)V

    .line 141
    const-string v0, "savedStateRemovedIdList"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->au:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 142
    const-string v0, "savedStateRemoveCluster"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestFragment;->av:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    return-void
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method
