.class public final Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/leaderboards/j;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    const v0, 0x7f040036

    const v1, 0x7f11000d

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 51
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->getIntent()Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->A:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->B:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->C:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DestLeaderboardScoreAct"

    const-string v1, "EXTRA_LEADERBOARD_ID extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LEADERBOARD_TIME_SPAN"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    if-eq v0, v3, :cond_1

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "DestLeaderboardScoreAct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid timespan "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v3, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->D:I

    .line 63
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->a(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const v1, 0x7f0c011e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;

    .line 69
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->ao()V

    .line 70
    return-void

    .line 58
    :cond_2
    const-string v0, "DestLeaderboardScoreAct"

    const-string v1, "EXTRA_GAME extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;->finish()V

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 103
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 108
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 105
    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const v1, 0x7f0c011e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->ac()V

    .line 106
    const/4 v0, 0x1

    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x7f0c0045
        :pswitch_0
    .end packed-switch
.end method
