.class public Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/e;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/gms/search/corpora/e;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->c:I

    .line 61
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->c:I

    .line 78
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->a:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->b:Ljava/lang/String;

    .line 80
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/corpora/e;->a(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;Landroid/os/Parcel;)V

    .line 92
    return-void
.end method
