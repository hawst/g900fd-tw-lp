.class final Lcom/google/android/gms/people/accountswitcherview/q;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/people/accountswitcherview/p;

.field final b:Landroid/os/ParcelFileDescriptor;

.field final c:I

.field final d:I

.field final e:I

.field final synthetic f:Lcom/google/android/gms/people/accountswitcherview/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/accountswitcherview/o;Lcom/google/android/gms/people/accountswitcherview/p;Landroid/os/ParcelFileDescriptor;III)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 253
    iput-object p2, p0, Lcom/google/android/gms/people/accountswitcherview/q;->a:Lcom/google/android/gms/people/accountswitcherview/p;

    .line 254
    iput-object p3, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    .line 255
    iput p4, p0, Lcom/google/android/gms/people/accountswitcherview/q;->c:I

    .line 256
    iput p5, p0, Lcom/google/android/gms/people/accountswitcherview/q;->d:I

    .line 257
    iput p6, p0, Lcom/google/android/gms/people/accountswitcherview/q;->e:I

    .line 258
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 262
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Lcom/google/android/gms/people/accountswitcherview/o;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/google/android/gms/people/x;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 267
    if-nez v1, :cond_2

    move-object v1, v0

    .line 281
    :goto_0
    if-eqz v1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/o;->d(Lcom/google/android/gms/people/accountswitcherview/o;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/q;->a:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v2, v2, Lcom/google/android/gms/people/accountswitcherview/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_1

    .line 287
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 293
    :cond_1
    :goto_1
    return-object v1

    .line 270
    :cond_2
    :try_start_2
    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/h;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 273
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    iget v2, p0, Lcom/google/android/gms/people/accountswitcherview/q;->c:I

    iget v3, p0, Lcom/google/android/gms/people/accountswitcherview/q;->d:I

    iget v4, p0, Lcom/google/android/gms/people/accountswitcherview/q;->e:I

    iget v5, p0, Lcom/google/android/gms/people/accountswitcherview/q;->c:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v6}, Lcom/google/android/gms/people/accountswitcherview/o;->b(Lcom/google/android/gms/people/accountswitcherview/o;)F

    move-result v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v6}, Lcom/google/android/gms/people/accountswitcherview/o;->c(Lcom/google/android/gms/people/accountswitcherview/o;)F

    move-result v6

    div-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Landroid/os/ParcelFileDescriptor;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 275
    if-nez v1, :cond_4

    move-object v1, v0

    .line 276
    goto :goto_0

    .line 278
    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->c:I

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v2}, Lcom/google/android/gms/people/accountswitcherview/o;->b(Lcom/google/android/gms/people/accountswitcherview/o;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    invoke-static {v3}, Lcom/google/android/gms/people/accountswitcherview/o;->c(Lcom/google/android/gms/people/accountswitcherview/o;)F

    move-result v3

    div-float/2addr v2, v3

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    const-string v2, "OwnersImageManager"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 285
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_5

    .line 287
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 290
    :cond_5
    :goto_2
    throw v0

    .line 288
    :catch_1
    move-exception v1

    .line 289
    const-string v2, "OwnersImageManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/q;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 244
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->a:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->a:Lcom/google/android/gms/people/accountswitcherview/p;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/q;->f:Lcom/google/android/gms/people/accountswitcherview/o;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/q;->a:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
