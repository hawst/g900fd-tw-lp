.class public final Lcom/google/android/gms/games/c/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/c/af;

.field public b:Lcom/google/android/gms/games/c/ao;

.field public c:Lcom/google/android/gms/games/c/v;

.field public d:Lcom/google/android/gms/games/c/j;

.field public e:Lcom/google/android/gms/games/c/ae;

.field public f:Lcom/google/android/gms/games/c/t;

.field public g:Lcom/google/android/gms/games/c/w;

.field public h:Lcom/google/android/gms/games/c/ac;

.field public i:Lcom/google/android/gms/games/c/e;

.field public j:Lcom/google/android/gms/games/c/ag;

.field public k:Lcom/google/android/gms/games/c/ap;

.field public l:Lcom/google/android/gms/games/c/ar;

.field public m:Lcom/google/android/gms/games/c/ad;

.field public n:Lcom/google/android/gms/games/c/am;

.field public o:Lcom/google/android/gms/games/c/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8910
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8911
    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/r;->G:I

    .line 8912
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 9170
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 9171
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-eqz v1, :cond_0

    .line 9172
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9175
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-eqz v1, :cond_1

    .line 9176
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9179
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-eqz v1, :cond_2

    .line 9180
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9183
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-eqz v1, :cond_3

    .line 9184
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9187
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-eqz v1, :cond_4

    .line 9188
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9191
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-eqz v1, :cond_5

    .line 9192
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9195
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-eqz v1, :cond_6

    .line 9196
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9199
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-eqz v1, :cond_7

    .line 9200
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9203
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-eqz v1, :cond_8

    .line 9204
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9207
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-eqz v1, :cond_9

    .line 9208
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9211
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-eqz v1, :cond_a

    .line 9212
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9215
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-eqz v1, :cond_b

    .line 9216
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9219
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-eqz v1, :cond_c

    .line 9220
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9223
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-eqz v1, :cond_d

    .line 9224
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9227
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-eqz v1, :cond_e

    .line 9228
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9231
    :cond_e
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8848
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/af;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/af;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/ao;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/c/v;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/c/j;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/c/ae;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/games/c/t;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/gms/games/c/w;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/games/c/ac;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/games/c/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/games/c/ag;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/gms/games/c/ap;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/android/gms/games/c/ar;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ar;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/gms/games/c/ad;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/gms/games/c/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-nez v0, :cond_f

    new-instance v0, Lcom/google/android/gms/games/c/ab;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9120
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-eqz v0, :cond_0

    .line 9121
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9123
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-eqz v0, :cond_1

    .line 9124
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9126
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-eqz v0, :cond_2

    .line 9127
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9129
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-eqz v0, :cond_3

    .line 9130
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9132
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-eqz v0, :cond_4

    .line 9133
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9135
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-eqz v0, :cond_5

    .line 9136
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9138
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-eqz v0, :cond_6

    .line 9139
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9141
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-eqz v0, :cond_7

    .line 9142
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9144
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-eqz v0, :cond_8

    .line 9145
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9147
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-eqz v0, :cond_9

    .line 9148
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9150
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-eqz v0, :cond_a

    .line 9151
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9153
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-eqz v0, :cond_b

    .line 9154
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9156
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-eqz v0, :cond_c

    .line 9157
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9159
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-eqz v0, :cond_d

    .line 9160
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9162
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-eqz v0, :cond_e

    .line 9163
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9165
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 9166
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8936
    if-ne p1, p0, :cond_1

    .line 9078
    :cond_0
    :goto_0
    return v0

    .line 8939
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 8940
    goto :goto_0

    .line 8942
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/r;

    .line 8943
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-nez v2, :cond_3

    .line 8944
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-eqz v2, :cond_4

    move v0, v1

    .line 8945
    goto :goto_0

    .line 8948
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/af;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 8949
    goto :goto_0

    .line 8952
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-nez v2, :cond_5

    .line 8953
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-eqz v2, :cond_6

    move v0, v1

    .line 8954
    goto :goto_0

    .line 8957
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 8958
    goto :goto_0

    .line 8961
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-nez v2, :cond_7

    .line 8962
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-eqz v2, :cond_8

    move v0, v1

    .line 8963
    goto :goto_0

    .line 8966
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 8967
    goto :goto_0

    .line 8970
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-nez v2, :cond_9

    .line 8971
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-eqz v2, :cond_a

    move v0, v1

    .line 8972
    goto :goto_0

    .line 8975
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 8976
    goto :goto_0

    .line 8979
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-nez v2, :cond_b

    .line 8980
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-eqz v2, :cond_c

    move v0, v1

    .line 8981
    goto :goto_0

    .line 8984
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 8985
    goto :goto_0

    .line 8988
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-nez v2, :cond_d

    .line 8989
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-eqz v2, :cond_e

    move v0, v1

    .line 8990
    goto :goto_0

    .line 8993
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 8994
    goto/16 :goto_0

    .line 8997
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-nez v2, :cond_f

    .line 8998
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-eqz v2, :cond_10

    move v0, v1

    .line 8999
    goto/16 :goto_0

    .line 9002
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/w;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 9003
    goto/16 :goto_0

    .line 9006
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-nez v2, :cond_11

    .line 9007
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-eqz v2, :cond_12

    move v0, v1

    .line 9008
    goto/16 :goto_0

    .line 9011
    :cond_11
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 9012
    goto/16 :goto_0

    .line 9015
    :cond_12
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-nez v2, :cond_13

    .line 9016
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-eqz v2, :cond_14

    move v0, v1

    .line 9017
    goto/16 :goto_0

    .line 9020
    :cond_13
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 9021
    goto/16 :goto_0

    .line 9024
    :cond_14
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-nez v2, :cond_15

    .line 9025
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-eqz v2, :cond_16

    move v0, v1

    .line 9026
    goto/16 :goto_0

    .line 9029
    :cond_15
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ag;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 9030
    goto/16 :goto_0

    .line 9033
    :cond_16
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-nez v2, :cond_17

    .line 9034
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-eqz v2, :cond_18

    move v0, v1

    .line 9035
    goto/16 :goto_0

    .line 9038
    :cond_17
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 9039
    goto/16 :goto_0

    .line 9042
    :cond_18
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-nez v2, :cond_19

    .line 9043
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-eqz v2, :cond_1a

    move v0, v1

    .line 9044
    goto/16 :goto_0

    .line 9047
    :cond_19
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 9048
    goto/16 :goto_0

    .line 9051
    :cond_1a
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-nez v2, :cond_1b

    .line 9052
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-eqz v2, :cond_1c

    move v0, v1

    .line 9053
    goto/16 :goto_0

    .line 9056
    :cond_1b
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ad;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move v0, v1

    .line 9057
    goto/16 :goto_0

    .line 9060
    :cond_1c
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-nez v2, :cond_1d

    .line 9061
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-eqz v2, :cond_1e

    move v0, v1

    .line 9062
    goto/16 :goto_0

    .line 9065
    :cond_1d
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/am;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move v0, v1

    .line 9066
    goto/16 :goto_0

    .line 9069
    :cond_1e
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-nez v2, :cond_1f

    .line 9070
    iget-object v2, p1, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-eqz v2, :cond_0

    move v0, v1

    .line 9071
    goto/16 :goto_0

    .line 9074
    :cond_1f
    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    iget-object v3, p1, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 9075
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9083
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9086
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 9088
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 9090
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 9092
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 9094
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 9096
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 9098
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 9100
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 9102
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 9104
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 9106
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 9108
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 9110
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 9112
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    if-nez v2, :cond_e

    :goto_e
    add-int/2addr v0, v1

    .line 9114
    return v0

    .line 9083
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/af;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9086
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->b:Lcom/google/android/gms/games/c/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ao;->hashCode()I

    move-result v0

    goto :goto_1

    .line 9088
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->c:Lcom/google/android/gms/games/c/v;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/v;->hashCode()I

    move-result v0

    goto :goto_2

    .line 9090
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/j;->hashCode()I

    move-result v0

    goto :goto_3

    .line 9092
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->e:Lcom/google/android/gms/games/c/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ae;->hashCode()I

    move-result v0

    goto :goto_4

    .line 9094
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->f:Lcom/google/android/gms/games/c/t;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/t;->hashCode()I

    move-result v0

    goto :goto_5

    .line 9096
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->g:Lcom/google/android/gms/games/c/w;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/w;->hashCode()I

    move-result v0

    goto :goto_6

    .line 9098
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ac;->hashCode()I

    move-result v0

    goto :goto_7

    .line 9100
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->i:Lcom/google/android/gms/games/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/e;->hashCode()I

    move-result v0

    goto :goto_8

    .line 9102
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->j:Lcom/google/android/gms/games/c/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ag;->hashCode()I

    move-result v0

    goto :goto_9

    .line 9104
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ap;->hashCode()I

    move-result v0

    goto :goto_a

    .line 9106
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->l:Lcom/google/android/gms/games/c/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ar;->hashCode()I

    move-result v0

    goto :goto_b

    .line 9108
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->m:Lcom/google/android/gms/games/c/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/ad;->hashCode()I

    move-result v0

    goto :goto_c

    .line 9110
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/c/r;->n:Lcom/google/android/gms/games/c/am;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/am;->hashCode()I

    move-result v0

    goto :goto_d

    .line 9112
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/games/c/r;->o:Lcom/google/android/gms/games/c/ab;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/ab;->hashCode()I

    move-result v1

    goto :goto_e
.end method
