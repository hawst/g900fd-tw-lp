.class public final Lcom/google/android/gms/games/c/af;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:J

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 6220
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/af;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/af;->G:I

    .line 6222
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 6320
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 6321
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/af;->a:Z

    if-eqz v1, :cond_0

    .line 6322
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/af;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6325
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6326
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6329
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 6330
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6333
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 6334
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6337
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 6338
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6341
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 6342
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6345
    :cond_5
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/af;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/af;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/af;->f:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 6297
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/af;->a:Z

    if-eqz v0, :cond_0

    .line 6298
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/af;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 6300
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 6301
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6303
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/games/c/af;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 6304
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6306
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 6307
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6309
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 6310
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6312
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/games/c/af;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 6313
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6315
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 6316
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6237
    if-ne p1, p0, :cond_1

    .line 6274
    :cond_0
    :goto_0
    return v0

    .line 6240
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/af;

    if-nez v2, :cond_2

    move v0, v1

    .line 6241
    goto :goto_0

    .line 6243
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/af;

    .line 6244
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/af;->a:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/af;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 6245
    goto :goto_0

    .line 6247
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 6248
    iget-object v2, p1, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 6249
    goto :goto_0

    .line 6251
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 6252
    goto :goto_0

    .line 6254
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/af;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 6255
    goto :goto_0

    .line 6257
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 6258
    iget-object v2, p1, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 6259
    goto :goto_0

    .line 6261
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 6262
    goto :goto_0

    .line 6264
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 6265
    iget-object v2, p1, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 6266
    goto :goto_0

    .line 6268
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 6269
    goto :goto_0

    .line 6271
    :cond_a
    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/af;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 6272
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 6279
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/af;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6281
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 6283
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/af;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 6285
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 6287
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 6289
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/af;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/af;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 6291
    return v0

    .line 6279
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 6281
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 6285
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 6287
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method
