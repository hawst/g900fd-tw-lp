.class public final Lcom/google/android/gms/games/internal/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;
    .locals 7

    .prologue
    .line 341
    new-instance v0, Lcom/google/android/gms/games/internal/a/t;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/internal/a/t;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;IIZZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/gms/games/internal/a/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/games/internal/a/p;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/gms/games/internal/a/o;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/o;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 206
    new-instance v0, Lcom/google/android/gms/games/internal/a/r;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/r;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 139
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->m()Lcom/google/android/gms/games/Game;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lcom/google/android/gms/games/internal/a/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/q;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/gms/games/internal/a/n;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/n;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcom/google/android/gms/games/internal/a/s;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/s;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 230
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 382
    new-instance v0, Lcom/google/android/gms/games/internal/a/j;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/j;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 237
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 394
    new-instance v0, Lcom/google/android/gms/games/internal/a/k;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/k;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 244
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 460
    new-instance v0, Lcom/google/android/gms/games/internal/a/l;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/l;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 251
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 472
    new-instance v0, Lcom/google/android/gms/games/internal/a/m;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/m;-><init>(Lcom/google/android/gms/games/internal/a/i;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 258
    const/4 v2, 0x2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 265
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 286
    const/4 v2, 0x5

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 293
    const/4 v2, 0x5

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 300
    const/4 v2, 0x6

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final l(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 307
    const/4 v2, 0x6

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/i;->a(Lcom/google/android/gms/common/api/t;IIZZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method
