.class public Lcom/google/android/gms/audiomodem/DtmfEncoding;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/audiomodem/c;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZFIIII)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->a:I

    .line 69
    iput p2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b:I

    .line 70
    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c:Z

    .line 71
    iput p4, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d:F

    .line 72
    iput p5, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e:I

    .line 73
    iput p6, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f:I

    .line 74
    iput p7, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g:I

    .line 75
    iput p8, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h:I

    .line 76
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c:Z

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d:F

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 146
    if-ne p0, p1, :cond_1

    .line 153
    :cond_0
    :goto_0
    return v0

    .line 149
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;

    if-nez v2, :cond_2

    move v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_2
    check-cast p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;

    .line 153
    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->a:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d:F

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 140
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 136
    invoke-static {p0, p1}, Lcom/google/android/gms/audiomodem/c;->a(Lcom/google/android/gms/audiomodem/DtmfEncoding;Landroid/os/Parcel;)V

    .line 137
    return-void
.end method
