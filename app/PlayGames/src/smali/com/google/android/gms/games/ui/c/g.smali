.class public final Lcom/google/android/gms/games/ui/c/g;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;-><init>()V

    return-void
.end method

.method public static P()Lcom/google/android/gms/games/ui/c/g;
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/games/ui/c/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/g;-><init>()V

    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v2, "intent"

    const/high16 v3, 0x1040000

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/g;->g(Landroid/os/Bundle;)V

    .line 35
    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 41
    const-string v1, "intent"

    sget v2, Lcom/google/android/gms/l;->ai:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 44
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    sget v2, Lcom/google/android/gms/l;->ce:I

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 46
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 48
    sget v1, Lcom/google/android/gms/l;->cd:I

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 49
    sget v1, Lcom/google/android/gms/l;->cc:I

    invoke-virtual {v2, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 50
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 57
    const-string v1, "com.google.android.play.games"

    const-string v2, "GPG_destAppUpgrade"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 61
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/support/v4/app/ab;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    const-string v1, "UpgradeDialogFrag"

    const-string v2, "Unable to launch play store intent"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
