.class public final Lcom/google/android/gms/games/ui/common/matches/n;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/matches/o;

.field private final h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;I)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 87
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/n;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->h:Z

    .line 90
    sget v0, Lcom/google/android/gms/h;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/matches/n;->d(II)V

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/n;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 2

    .prologue
    .line 106
    if-nez p1, :cond_0

    .line 107
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    .line 114
    :goto_0
    return-void

    .line 109
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/data/l;

    const-string v1, "external_match_id"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/data/l;-><init>(Lcom/google/android/gms/common/data/b;Ljava/lang/String;)V

    .line 112
    invoke-super {p0, v0}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/n;->i:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/n;->j:Ljava/lang/String;

    .line 102
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/p;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 123
    sget v0, Lcom/google/android/gms/g;->aa:I

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x3

    return v0
.end method
