.class public abstract Lcom/google/android/gms/common/data/m;
.super Lcom/google/android/gms/common/data/b;
.source "SourceFile"


# instance fields
.field protected final b:Lcom/google/android/gms/common/data/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/b;)V
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 23
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 24
    instance-of v0, p1, Lcom/google/android/gms/common/data/m;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Not possible to have nested FilteredDataBuffers."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/common/data/m;->b:Lcom/google/android/gms/common/data/b;

    .line 27
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/common/data/m;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/data/m;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(I)I
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/common/data/m;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->d()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public f_()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/common/data/m;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->f_()V

    .line 47
    return-void
.end method
