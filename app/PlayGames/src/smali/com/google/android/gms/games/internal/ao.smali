.class final Lcom/google/android/gms/games/internal/ao;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/ag;)V
    .locals 0

    .prologue
    .line 809
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 810
    iput-object p1, p0, Lcom/google/android/gms/games/internal/ao;->a:Lcom/google/android/gms/common/api/ag;

    .line 811
    return-void
.end method

.method private static P(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/quest/Quest;
    .locals 3

    .prologue
    .line 825
    new-instance v1, Lcom/google/android/gms/games/quest/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 826
    const/4 v0, 0x0

    .line 828
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 829
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 832
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    .line 835
    return-object v0

    .line 832
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    throw v0
.end method


# virtual methods
.method public final K(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 815
    invoke-static {p1}, Lcom/google/android/gms/games/internal/ao;->P(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/quest/Quest;

    move-result-object v0

    .line 818
    if-eqz v0, :cond_0

    .line 819
    iget-object v1, p0, Lcom/google/android/gms/games/internal/ao;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v2, Lcom/google/android/gms/games/internal/an;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/an;-><init>(Lcom/google/android/gms/games/quest/Quest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 821
    :cond_0
    return-void
.end method
