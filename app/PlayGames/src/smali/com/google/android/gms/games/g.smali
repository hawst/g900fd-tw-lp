.class public final Lcom/google/android/gms/games/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/e;


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:Z

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-boolean v1, p0, Lcom/google/android/gms/games/g;->a:Z

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/g;->b:Z

    .line 161
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/android/gms/games/g;->c:I

    .line 162
    iput-boolean v1, p0, Lcom/google/android/gms/games/g;->d:Z

    .line 163
    const/16 v0, 0x1110

    iput v0, p0, Lcom/google/android/gms/games/g;->e:I

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/g;->f:Ljava/lang/String;

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g;->g:Ljava/util/ArrayList;

    .line 166
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/gms/games/g;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/h;)V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iget-boolean v0, p1, Lcom/google/android/gms/games/h;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/g;->a:Z

    .line 170
    iget-boolean v0, p1, Lcom/google/android/gms/games/h;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/g;->b:Z

    .line 171
    iget v0, p1, Lcom/google/android/gms/games/h;->c:I

    iput v0, p0, Lcom/google/android/gms/games/g;->c:I

    .line 172
    iget-boolean v0, p1, Lcom/google/android/gms/games/h;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/g;->d:Z

    .line 173
    iget v0, p1, Lcom/google/android/gms/games/h;->e:I

    iput v0, p0, Lcom/google/android/gms/games/g;->e:I

    .line 174
    iget-object v0, p1, Lcom/google/android/gms/games/h;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/g;->f:Ljava/lang/String;

    .line 175
    iget-object v0, p1, Lcom/google/android/gms/games/h;->g:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/gms/games/g;->g:Ljava/util/ArrayList;

    .line 176
    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/games/h;B)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/g;-><init>(Lcom/google/android/gms/games/h;)V

    return-void
.end method
