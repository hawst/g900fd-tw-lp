.class final Lcom/google/android/gms/games/ui/ao;
.super Landroid/support/v7/widget/bx;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/ak;

.field private final b:Lcom/google/android/gms/games/ui/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/ak;Lcom/google/android/gms/games/ui/w;)V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Landroid/support/v7/widget/bx;-><init>()V

    .line 299
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    .line 300
    iput-object p2, p0, Lcom/google/android/gms/games/ui/ao;->b:Lcom/google/android/gms/games/ui/w;

    .line 301
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    new-instance v1, Lcom/google/android/gms/games/ui/an;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/ao;->b:Lcom/google/android/gms/games/ui/w;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/games/ui/an;-><init>(Lcom/google/android/gms/games/ui/w;I)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/ak;->a(Lcom/google/android/gms/games/ui/ak;Lcom/google/android/gms/games/ui/an;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    iget-object v0, v0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0}, Landroid/support/v7/widget/bw;->a()V

    .line 306
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/ao;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/ui/ak;->a(II)V

    .line 311
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/ao;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/ui/ak;->b(II)V

    .line 316
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ao;->a:Lcom/google/android/gms/games/ui/ak;

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/ao;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/ui/ak;->c(II)V

    .line 321
    return-void
.end method
