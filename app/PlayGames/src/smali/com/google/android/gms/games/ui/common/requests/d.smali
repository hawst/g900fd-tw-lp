.class public final Lcom/google/android/gms/games/ui/common/requests/d;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/gms/games/ui/common/requests/e;

.field private final i:Z

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/requests/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;I)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 72
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/requests/d;->g:Landroid/content/Context;

    .line 73
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/e;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/d;->h:Lcom/google/android/gms/games/ui/common/requests/e;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/d;->i:Z

    .line 76
    sget v0, Lcom/google/android/gms/h;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/requests/d;->d(II)V

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/requests/d;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/d;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/requests/d;)Lcom/google/android/gms/games/ui/common/requests/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/d;->h:Lcom/google/android/gms/games/ui/common/requests/e;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 1

    .prologue
    .line 90
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/q;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    .line 92
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/requests/d;->j:Ljava/lang/String;

    .line 86
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/f;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/f;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 101
    sget v0, Lcom/google/android/gms/g;->ad:I

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x3

    return v0
.end method
