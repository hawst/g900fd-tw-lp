.class final Lcom/google/android/gms/common/api/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/aq;

.field private final b:I

.field private final c:Lcom/google/android/gms/common/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/aq;ILcom/google/android/gms/common/a;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344
    iput p2, p0, Lcom/google/android/gms/common/api/at;->b:I

    .line 345
    iput-object p3, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    .line 346
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/aq;->i()Landroid/support/v4/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->y_()Landroid/support/v4/app/ag;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ag;->c()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 356
    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x1

    .line 357
    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    iget-object v2, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/aq;->i()Landroid/support/v4/app/ab;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/a;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-static {v0}, Lcom/google/android/gms/common/api/aq;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/aq;->i()Landroid/support/v4/app/ab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/h;->a(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_0

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->a:Lcom/google/android/gms/common/api/aq;

    iget v1, p0, Lcom/google/android/gms/common/api/at;->b:I

    iget-object v2, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/a;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/api/aq;->a(Lcom/google/android/gms/common/api/aq;ILcom/google/android/gms/common/a;)V

    goto :goto_0
.end method
