.class public final Lcom/google/android/gms/games/ui/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field public static a:J

.field public static b:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    const-wide/16 v0, 0x145

    sput-wide v0, Lcom/google/android/gms/games/ui/d/a;->a:J

    .line 30
    const-wide/16 v0, 0x177

    sput-wide v0, Lcom/google/android/gms/games/ui/d/a;->b:J

    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;)Landroid/transition/Transition;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v4, 0x30

    .line 114
    invoke-static {p3, v7}, Lcom/google/android/gms/games/ui/d/a;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;

    move-result-object v0

    .line 115
    new-instance v1, Landroid/transition/Slide;

    invoke-direct {v1, v4}, Landroid/transition/Slide;-><init>(I)V

    invoke-virtual {v1, p1}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 116
    new-instance v2, Landroid/transition/Slide;

    invoke-direct {v2, v4}, Landroid/transition/Slide;-><init>(I)V

    invoke-virtual {v2, p5}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    .line 117
    new-instance v3, Landroid/transition/Slide;

    invoke-direct {v3, v4}, Landroid/transition/Slide;-><init>(I)V

    invoke-virtual {v3, p6}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    .line 118
    new-instance v4, Landroid/transition/Fade;

    invoke-direct {v4}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {v4, p4}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v4

    .line 119
    new-instance v5, Landroid/transition/Slide;

    invoke-direct {v5}, Landroid/transition/Slide;-><init>()V

    invoke-virtual {v5, p2}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v5

    .line 121
    const/4 v6, 0x6

    new-array v6, v6, [Landroid/transition/Transition;

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v0, v6, v4

    const/4 v0, 0x2

    aput-object v2, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v1, v6, v0

    const/4 v0, 0x5

    aput-object v3, v6, v0

    invoke-static {v6}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/play/c/h;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/games/ui/d/a;->b:J

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/Transition;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 99
    invoke-static {p1, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;

    move-result-object v0

    .line 101
    new-array v1, v1, [Landroid/transition/Transition;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/play/c/h;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/games/ui/d/a;->a:J

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)Landroid/transition/Transition;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    new-instance v0, Lcom/google/android/gms/games/ui/d/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/d/b;-><init>()V

    .line 51
    new-instance v1, Lcom/google/android/gms/games/ui/d/c;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/d/c;-><init>()V

    .line 58
    new-instance v2, Lcom/google/android/gms/games/ui/d/j;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/games/ui/d/j;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/d/j;->a(Z)Lcom/google/android/gms/games/ui/d/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/d/j;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/google/android/gms/games/ui/d/p;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/d/p;-><init>()V

    .line 65
    invoke-virtual {v1, p1}, Lcom/google/android/gms/games/ui/d/p;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/games/ui/d/d;

    invoke-direct {v3}, Lcom/google/android/gms/games/ui/d/d;-><init>()V

    invoke-virtual {v2, v3}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 72
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/transition/Transition;

    aput-object v0, v2, v4

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 73
    invoke-static {p0}, Lcom/google/android/play/c/h;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 74
    sget-wide v2, Lcom/google/android/gms/games/ui/d/a;->a:J

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 75
    new-instance v1, Landroid/transition/ArcMotion;

    invoke-direct {v1}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 76
    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)Landroid/transition/Transition;
    .locals 4

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/gms/games/ui/d/h;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/d/h;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/d/h;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 89
    sget-wide v2, Lcom/google/android/gms/games/ui/d/a;->b:J

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 90
    return-object v0
.end method

.method private static a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k()Landroid/view/View;

    move-result-object v1

    .line 134
    if-eqz p1, :cond_0

    new-instance v0, Landroid/transition/Fade;

    invoke-direct {v0}, Landroid/transition/Fade;-><init>()V

    :goto_0
    invoke-virtual {v0, v1}, Landroid/transition/Visibility;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Landroid/transition/Slide;

    const/16 v2, 0x30

    invoke-direct {v0, v2}, Landroid/transition/Slide;-><init>(I)V

    goto :goto_0
.end method
