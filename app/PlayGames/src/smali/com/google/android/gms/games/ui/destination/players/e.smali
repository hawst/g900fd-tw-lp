.class final Lcom/google/android/gms/games/ui/destination/players/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/e;->a:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 244
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;

    check-cast p2, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;

    iget v0, p1, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v4, v0, 0x0

    iget v0, p1, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    add-int/2addr v4, v0

    iget v0, p2, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    if-nez v0, :cond_3

    :goto_2
    add-int/lit8 v1, v1, 0x0

    iget v0, p2, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    if-nez v0, :cond_4

    move v0, v3

    :goto_3
    add-int/2addr v0, v1

    if-le v4, v0, :cond_5

    const/4 v2, -0x1

    :cond_0
    :goto_4
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    if-eq v4, v0, :cond_0

    move v2, v3

    goto :goto_4
.end method
