.class public final Lcom/google/android/gms/people/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/people/h;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;

.field private d:I

.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/h;->a:Lcom/google/android/gms/people/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const v0, 0x1fffff

    iput v0, p0, Lcom/google/android/gms/people/h;->d:I

    .line 227
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/gms/people/h;->h:I

    .line 229
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/h;->i:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/gms/people/h;->c:Ljava/util/Collection;

    .line 246
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/gms/people/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/gms/people/h;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/google/android/gms/people/h;->d:I

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/google/android/gms/people/h;->e:Z

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 324
    iget-wide v0, p0, Lcom/google/android/gms/people/h;->f:J

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/people/h;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/google/android/gms/people/h;->h:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/google/android/gms/people/h;->i:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/google/android/gms/people/h;->j:I

    return v0
.end method
