.class public Lcom/google/android/gms/location/copresence/internal/Operation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/location/copresence/internal/PublishOperation;

.field public final c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

.field public final d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

.field public final e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/g;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/Operation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->f:I

    .line 97
    iput p2, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    .line 98
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->b:Lcom/google/android/gms/location/copresence/internal/PublishOperation;

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    .line 100
    iput-object p5, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    .line 101
    iput-object p6, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    .line 102
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->f:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 120
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/g;->a(Lcom/google/android/gms/location/copresence/internal/Operation;Landroid/os/Parcel;I)V

    .line 121
    return-void
.end method
