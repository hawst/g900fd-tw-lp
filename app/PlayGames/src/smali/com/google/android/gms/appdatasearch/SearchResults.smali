.class public Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ag;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[I

.field final d:[B

.field final e:[Landroid/os/Bundle;

.field final f:[Landroid/os/Bundle;

.field final g:[Landroid/os/Bundle;

.field final h:I

.field final i:[I

.field final j:[Ljava/lang/String;

.field final k:[B

.field final l:[D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/appdatasearch/ag;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ag;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;[B[D)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:I

    .line 103
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    .line 105
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    .line 106
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->e:[Landroid/os/Bundle;

    .line 107
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->f:[Landroid/os/Bundle;

    .line 108
    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->g:[Landroid/os/Bundle;

    .line 109
    iput p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->h:I

    .line 110
    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->i:[I

    .line 111
    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->j:[Ljava/lang/String;

    .line 112
    iput-object p11, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->k:[B

    .line 113
    iput-object p12, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->l:[D

    .line 114
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->h:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 475
    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/appdatasearch/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/af;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 481
    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ag;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    .line 482
    return-void
.end method
