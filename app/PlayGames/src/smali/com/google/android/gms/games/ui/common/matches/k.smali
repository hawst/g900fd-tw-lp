.class public final Lcom/google/android/gms/games/ui/common/matches/k;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/ar;
.implements Lcom/google/android/gms/games/ui/common/matches/b;
.implements Lcom/google/android/gms/games/ui/common/matches/e;
.implements Lcom/google/android/gms/games/ui/common/matches/i;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/matches/a;

.field private an:Lcom/google/android/gms/games/ui/common/matches/d;

.field private ao:Lcom/google/android/gms/games/ui/common/matches/h;

.field private ap:Lcom/google/android/gms/games/ui/common/matches/u;

.field private aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

.field private ar:Z

.field private as:I

.field private at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/google/android/gms/i;->r:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/k;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method private at()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->an:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ao:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/matches/h;->a()I

    move-result v1

    add-int/2addr v1, v0

    .line 247
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->h:Lcom/google/android/gms/games/ui/e/o;

    if-nez v1, :cond_1

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 249
    if-nez v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 252
    :cond_0
    return-void

    .line 247
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private au()V
    .locals 3

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 342
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0

    .line 347
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method private av()V
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 457
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/k;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/k;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Landroid/view/View;)Lcom/google/android/gms/games/ui/e/o;
    .locals 11

    .prologue
    .line 125
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 127
    const/4 v10, 0x0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v10

    .line 131
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/e/o;

    const v2, 0x102000a

    sget v3, Lcom/google/android/gms/g;->aP:I

    sget v4, Lcom/google/android/gms/g;->aY:I

    sget v5, Lcom/google/android/gms/g;->bb:I

    sget v6, Lcom/google/android/gms/g;->ao:I

    move-object v1, p1

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/e/o;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/e/q;Lcom/google/android/gms/games/ui/e/p;Lcom/google/android/gms/games/ui/e/r;I)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 114
    sget v0, Lcom/google/android/gms/g;->aH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    .line 116
    sget-object v0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 48
    check-cast p1, Lcom/google/android/gms/games/multiplayer/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/e;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/e;->c()Lcom/google/android/gms/games/multiplayer/a;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->r()V

    new-instance v2, Lcom/google/android/gms/games/ui/common/matches/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/ui/common/matches/g;-><init>(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->an:Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ao:Lcom/google/android/gms/games/ui/common/matches/h;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->h:Lcom/google/android/gms/games/ui/e/o;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v4

    iget-object v2, v2, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v2

    add-int/2addr v2, v4

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->a()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->w()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->at:Ljava/lang/String;

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->an:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ao:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 175
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v0, p1, v4}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;I)V

    .line 189
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->aj()V

    .line 190
    return-void

    .line 177
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    .line 179
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 184
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v1, p1, v0, v4}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/Game;)V

    .line 257
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 281
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->at()V

    .line 282
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 230
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 359
    :cond_0
    if-eqz p1, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/aq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->at:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/ap;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/k;->b(Z)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/m;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/m;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/c/a;->a(Landroid/support/v4/app/Fragment;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    const-string v2, "confirmationDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->as()V

    goto :goto_0
.end method

.method protected final al()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->au()V

    .line 332
    return-void
.end method

.method final as()V
    .locals 3

    .prologue
    .line 443
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Z)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 446
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->av()V

    .line 447
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->b(Lcom/google/android/gms/games/Game;)V

    .line 262
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 287
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->at()V

    .line 288
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 235
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->at()V

    .line 236
    return-void
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 388
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Z)V

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->as:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 392
    if-eqz p1, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->at:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/ap;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 399
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Z)V

    .line 402
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-static {v0}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/l;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/common/matches/l;-><init>(Lcom/google/android/gms/games/ui/common/matches/k;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 438
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->c()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C_()V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->P()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    const-string v0, "InvitationListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->av()V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 241
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->at()V

    .line 242
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/v;->a()Lcom/google/android/gms/games/ui/common/matches/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ap:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 86
    sget v0, Lcom/google/android/gms/f;->u:I

    sget v1, Lcom/google/android/gms/l;->aw:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/k;->a(III)V

    .line 89
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/a;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    sget-object v0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Z)V

    .line 92
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->an:Lcom/google/android/gms/games/ui/common/matches/d;

    .line 94
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/h;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/h;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/i;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ao:Lcom/google/android/gms/games/ui/common/matches/h;

    .line 97
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->am:Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->an:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ao:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 101
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Landroid/support/v7/widget/bv;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 107
    :cond_0
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->au()V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->au()V

    .line 327
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->aq:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    if-ne p1, v0, :cond_0

    .line 465
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Z)V

    .line 467
    :cond_0
    return-void

    .line 465
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q_()V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->ar:Z

    if-eqz v0, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->av()V

    .line 157
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->q_()V

    .line 158
    return-void
.end method
