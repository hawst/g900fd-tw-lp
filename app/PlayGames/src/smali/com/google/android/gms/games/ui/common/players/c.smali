.class final Lcom/google/android/gms/games/ui/common/players/c;
.super Lcom/google/android/gms/games/ui/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/e/h;


# instance fields
.field private final m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/view/View;

.field private final q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/ImageView;

.field private final t:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/g;-><init>(Landroid/view/View;)V

    .line 186
    sget v0, Lcom/google/android/gms/g;->h:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->L:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->K:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->J:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e(I)V

    .line 193
    sget v0, Lcom/google/android/gms/g;->cs:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->n:Landroid/widget/TextView;

    .line 194
    sget v0, Lcom/google/android/gms/g;->cg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->o:Landroid/widget/TextView;

    .line 196
    sget v0, Lcom/google/android/gms/g;->ch:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    sget v0, Lcom/google/android/gms/g;->ax:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 201
    sget v0, Lcom/google/android/gms/g;->x:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    sget v0, Lcom/google/android/gms/g;->bg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    .line 207
    sget v0, Lcom/google/android/gms/g;->be:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    .line 208
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v3, 0x0

    .line 167
    check-cast p3, Lcom/google/android/gms/games/Player;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/g;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v1, p3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/gms/l;->aZ:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->n:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->b(Lcom/google/android/gms/games/ui/common/players/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->n:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/c;->n:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/players/c;->n:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v4, v1, v5, v1, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_0
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "avatar"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTransitionName(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b()Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "level"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTransitionName(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->c(Lcom/google/android/gms/games/ui/common/players/a;)I

    move-result v0

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/a;

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->f()Landroid/net/Uri;

    move-result-object v5

    sget v6, Lcom/google/android/gms/f;->f:I

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/gms/games/ui/common/players/c;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->o:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->o:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->k()I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->d(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/c;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->d(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->A:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    return-void

    :cond_2
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/l;->L:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->r:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->z:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->t:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/c;->k:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/l;->L:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->p:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/a;

    .line 375
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 376
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/c;->o()Ljava/lang/Object;

    .line 378
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 331
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 332
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    .line 334
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/a;

    .line 335
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 336
    sget v3, Lcom/google/android/gms/g;->h:I

    if-ne v1, v3, :cond_2

    .line 337
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 338
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    const-string v3, "avatar"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 341
    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b()Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "level"

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 343
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/c;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/util/Pair;

    aput-object v1, v4, v5

    aput-object v2, v4, v6

    invoke-interface {v3, v0, v4}, Lcom/google/android/gms/games/ui/common/players/b;->a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 370
    :cond_0
    :goto_1
    return-void

    .line 332
    :cond_1
    const/4 v0, -0x1

    move v2, v0

    goto :goto_0

    .line 346
    :cond_2
    sget v3, Lcom/google/android/gms/g;->ch:I

    if-ne v1, v3, :cond_3

    .line 350
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 351
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/c;->q:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const-string v3, "icon"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 354
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/c;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    new-array v3, v6, [Landroid/util/Pair;

    aput-object v1, v3, v5

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/games/ui/common/players/b;->b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_1

    .line 356
    :cond_3
    sget v3, Lcom/google/android/gms/g;->bg:I

    if-ne v1, v3, :cond_4

    if-ltz v2, :cond_4

    .line 360
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/c;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Player;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/gms/games/ui/common/players/b;->a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V

    goto :goto_1

    .line 361
    :cond_4
    sget v2, Lcom/google/android/gms/g;->be:I

    if-ne v1, v2, :cond_0

    .line 363
    new-instance v1, Landroid/support/v7/widget/bn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 364
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/a;->f(Lcom/google/android/gms/games/ui/common/players/a;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bn;->a(I)V

    .line 365
    new-instance v0, Lcom/google/android/gms/games/ui/e/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/e/g;-><init>(Lcom/google/android/gms/games/ui/e/h;Landroid/view/View;)V

    iput-object v0, v1, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/widget/bp;

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/c;->s:Landroid/widget/ImageView;

    instance-of v2, v0, Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    check-cast v0, Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/f;->k:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v2, Lcom/google/android/gms/games/ui/e/al;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/ui/e/al;-><init>(Landroid/widget/ImageView;)V

    iput-object v2, v1, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bo;

    iget-object v0, v1, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->b()V

    goto :goto_1
.end method
