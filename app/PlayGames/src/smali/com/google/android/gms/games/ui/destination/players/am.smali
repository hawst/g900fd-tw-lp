.class public final Lcom/google/android/gms/games/ui/destination/players/am;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/card/ac;
.implements Lcom/google/android/gms/games/ui/common/players/b;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/card/aa;

.field private ao:Lcom/google/android/gms/games/ui/common/players/a;

.field private ap:Ljava/lang/String;

.field private aq:Lcom/google/android/gms/games/Player;

.field private ar:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    return-void
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 200
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v1, "you_may_know"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/t;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 203
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/h;->a(IILandroid/content/Intent;)V

    .line 128
    if-ne p1, v0, :cond_1

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 130
    invoke-static {p3}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v1

    .line 132
    invoke-interface {v1}, Lcom/google/android/gms/common/a/a/c;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/common/a/a/c;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 134
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, ""

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ap:Ljava/lang/String;

    const/4 v4, 0x5

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->aq:Lcom/google/android/gms/games/Player;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ar:I

    invoke-virtual {v0, v1, p3, v2}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;I)V

    .line 138
    :cond_1
    return-void

    .line 132
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 40
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/am;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/common/players/a;->k()V

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->f_()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 112
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ap:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ap:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 115
    const-string v0, "PlayerListYouMayKnow"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 122
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/am;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    .line 221
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 229
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V
    .locals 4

    .prologue
    .line 243
    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->aq:Lcom/google/android/gms/games/Player;

    .line 244
    iput p3, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ar:I

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ap:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 247
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 249
    return-void
.end method

.method public final am()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;)V

    .line 211
    return-void
.end method

.method public final varargs b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 234
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 258
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 259
    const-string v1, "gamersYouMayKnowWelcomeButton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->an:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "showYouMayKnowWelcome"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 266
    if-eqz v0, :cond_0

    .line 267
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->ab()V

    .line 270
    :cond_0
    return-void
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/am;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    const-string v0, "PlayerListYouMayKnow"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :goto_0
    return-void

    .line 181
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v2, "you_may_know"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 65
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f0200ed

    const v1, 0x7f0f00ba

    const v2, 0x7f0f00b9

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/am;->a(III)V

    .line 71
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v3, 0x7f020095

    const v4, 0x7f0f00d2

    const v5, 0x7f0f01d5

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v3, "gamersYouMayKnowWelcomeButton"

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->an:Lcom/google/android/gms/games/ui/card/aa;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "showYouMayKnowWelcome"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->an:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 81
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0, v6}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 87
    const v0, 0x7f0b01ed

    iput v0, p0, Lcom/google/android/gms/games/ui/p;->al:I

    .line 90
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->an:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ao:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 93
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/am;->a(Landroid/support/v7/widget/bv;)V

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const-string v0, "savedStateAddedPlayer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->aq:Lcom/google/android/gms/games/Player;

    .line 98
    const-string v0, "savedStateAddedPlayerPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ar:I

    .line 101
    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->e(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "savedStateAddedPlayer"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->aq:Lcom/google/android/gms/games/Player;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 107
    const-string v0, "savedStateAddedPlayerPosition"

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->ar:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/am;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 189
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    const-string v0, "PlayerListYouMayKnow"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/am;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 196
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/am;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
