.class public Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:I

.field private final d:[Lcom/google/android/gms/audiomodem/Encoding;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/audiomodem/f;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I[BI[Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput p1, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->a:I

    .line 95
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->b:[B

    .line 96
    iput p3, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->c:I

    .line 97
    iput-object p4, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->d:[Lcom/google/android/gms/audiomodem/Encoding;

    .line 98
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->a:I

    return v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->b:[B

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->c:I

    return v0
.end method

.method public final d()[Lcom/google/android/gms/audiomodem/Encoding;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->d:[Lcom/google/android/gms/audiomodem/Encoding;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 135
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/audiomodem/f;->a(Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;Landroid/os/Parcel;I)V

    .line 136
    return-void
.end method
