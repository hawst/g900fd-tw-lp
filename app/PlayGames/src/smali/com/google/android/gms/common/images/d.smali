.class final Lcom/google/android/gms/common/images/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/common/images/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/images/a;)V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    iput-object p1, p0, Lcom/google/android/gms/common/images/d;->a:Lcom/google/android/gms/common/images/a;

    .line 627
    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 632
    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/common/images/d;->a:Lcom/google/android/gms/common/images/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/a;->a()V

    .line 637
    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 2

    .prologue
    .line 641
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_1

    .line 642
    iget-object v0, p0, Lcom/google/android/gms/common/images/d;->a:Lcom/google/android/gms/common/images/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/a;->a()V

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 643
    :cond_1
    const/16 v0, 0x14

    if-lt p1, v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/google/android/gms/common/images/d;->a:Lcom/google/android/gms/common/images/a;

    iget-object v1, p0, Lcom/google/android/gms/common/images/d;->a:Lcom/google/android/gms/common/images/a;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/a;->b()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/a;->a(I)V

    goto :goto_0
.end method
