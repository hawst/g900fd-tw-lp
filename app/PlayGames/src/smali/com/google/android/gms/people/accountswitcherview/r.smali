.class public final Lcom/google/android/gms/people/accountswitcherview/r;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# static fields
.field private static final a:I


# instance fields
.field private b:Lcom/google/android/gms/people/accountswitcherview/i;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/people/accountswitcherview/v;

.field private e:Lcom/google/android/gms/people/accountswitcherview/t;

.field private f:I

.field private g:Landroid/view/LayoutInflater;

.field private h:I

.field private i:Landroid/content/Context;

.field private j:Ljava/util/ArrayList;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/gms/people/accountswitcherview/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ad;->a:I

    sput v0, Lcom/google/android/gms/people/accountswitcherview/r;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ad;->a:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/r;-><init>(Landroid/content/Context;I)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 74
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    .line 77
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->l:Z

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->i:Landroid/content/Context;

    .line 79
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    :goto_0
    iput p2, p0, Lcom/google/android/gms/people/accountswitcherview/r;->f:I

    .line 80
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->g:Landroid/view/LayoutInflater;

    .line 81
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/s;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/people/accountswitcherview/s;-><init>(Lcom/google/android/gms/people/accountswitcherview/r;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->d:Lcom/google/android/gms/people/accountswitcherview/v;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->e:Lcom/google/android/gms/people/accountswitcherview/t;

    .line 83
    new-array v0, v1, [I

    sget v1, Lcom/google/android/gms/people/accountswitcherview/x;->a:I

    aput v1, v0, v3

    .line 87
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 88
    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/people/accountswitcherview/y;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->h:I

    .line 91
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->n:Lcom/google/android/gms/people/accountswitcherview/a;

    .line 93
    return-void

    .line 79
    :cond_0
    sget p2, Lcom/google/android/gms/people/accountswitcherview/r;->a:I

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/people/model/e;Lcom/google/android/gms/people/model/e;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 353
    .line 355
    if-eqz p2, :cond_2

    invoke-interface {p2}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 357
    :goto_0
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 359
    :goto_1
    const/4 v0, 0x0

    move v3, v2

    move v4, v2

    move v2, v0

    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 360
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/e;

    .line 361
    if-gez v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v2

    .line 364
    :cond_0
    if-gez v3, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 359
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v5, v0

    .line 355
    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 357
    goto :goto_1

    .line 368
    :cond_4
    if-ltz v4, :cond_5

    .line 369
    invoke-interface {p0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 371
    :cond_5
    if-gez v3, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 373
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    :cond_6
    return-object p0
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->d()I

    move-result v0

    .line 299
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->l:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()I
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/people/model/e;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 290
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/e;

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 100
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    if-eq v0, v1, :cond_0

    .line 101
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->notifyDataSetChanged()V

    .line 104
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/accountswitcherview/i;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->b:Lcom/google/android/gms/people/accountswitcherview/i;

    .line 97
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/e;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->notifyDataSetChanged()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->notifyDataSetChanged()V

    .line 260
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->n:Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/a;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->l:Z

    if-eq v0, v1, :cond_0

    .line 108
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->l:Z

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->notifyDataSetChanged()V

    .line 111
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->n:Lcom/google/android/gms/people/accountswitcherview/a;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->n:Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/a;->a()V

    .line 331
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/gms/people/accountswitcherview/r;->l:Z

    if-eqz v3, :cond_2

    :goto_1
    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :cond_0
    add-int/2addr v0, v2

    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->a(I)Lcom/google/android/gms/people/model/e;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 313
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/r;->d()I

    .line 315
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 316
    const-wide/16 v0, -0x2

    .line 320
    :cond_0
    :goto_0
    return-wide v0

    .line 317
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 320
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/e;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x2

    .line 151
    :goto_0
    return v0

    .line 148
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    const/4 v0, 0x1

    goto :goto_0

    .line 151
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 117
    if-nez p2, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/people/accountswitcherview/ad;->c:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 135
    :cond_0
    :goto_0
    return-object p2

    .line 120
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 122
    if-nez p2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/people/accountswitcherview/ad;->b:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 126
    :cond_2
    if-nez p2, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->g:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/r;->f:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 129
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/r;->a(I)Lcom/google/android/gms/people/model/e;

    move-result-object v1

    .line 130
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/r;->b:Lcom/google/android/gms/people/accountswitcherview/i;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/r;->d:Lcom/google/android/gms/people/accountswitcherview/v;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/r;->e:Lcom/google/android/gms/people/accountswitcherview/t;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/r;->c:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/r;->c:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_4
    iget v3, p0, Lcom/google/android/gms/people/accountswitcherview/r;->h:I

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_6

    invoke-interface {v0, p2}, Lcom/google/android/gms/people/accountswitcherview/v;->a(Landroid/view/View;)Lcom/google/android/gms/people/accountswitcherview/u;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Landroid/widget/ImageView;)V

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_5
    :goto_2
    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/u;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/u;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/u;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/u;->a:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/u;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/r;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/people/accountswitcherview/ae;->a:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/u;

    goto :goto_1

    :cond_7
    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Landroid/widget/ImageView;)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/u;->b:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/e;->f()Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x3

    return v0
.end method
