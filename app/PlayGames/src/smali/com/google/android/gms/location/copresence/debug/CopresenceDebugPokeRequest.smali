.class public Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/debug/a;

.field public static a:I

.field public static b:I


# instance fields
.field public final c:Lcom/google/android/gms/location/copresence/internal/d;

.field private final d:I

.field private e:I

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/location/copresence/debug/a;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/debug/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->CREATOR:Lcom/google/android/gms/location/copresence/debug/a;

    .line 29
    const/4 v0, 0x2

    sput v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->a:I

    .line 30
    const/16 v0, 0x20

    sput v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->b:I

    return-void
.end method

.method constructor <init>(II[BLandroid/os/IBinder;)V
    .locals 1
    .param p2    # I
        .annotation runtime Ljava/lang/Deprecated;
        .end annotation
    .end param

    .prologue
    .line 56
    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;-><init>(II[BLcom/google/android/gms/location/copresence/internal/d;)V

    .line 59
    return-void

    .line 56
    :cond_0
    invoke-static {p4}, Lcom/google/android/gms/location/copresence/internal/e;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/d;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(II[BLcom/google/android/gms/location/copresence/internal/d;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->d:I

    .line 64
    iput p2, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->e:I

    .line 65
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->f:[B

    .line 66
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->c:Lcom/google/android/gms/location/copresence/internal/d;

    .line 67
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->d:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->e:I

    return v0
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->f:[B

    return-object v0
.end method

.method final d()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->c:Lcom/google/android/gms/location/copresence/internal/d;

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->c:Lcom/google/android/gms/location/copresence/internal/d;

    invoke-interface {v0}, Lcom/google/android/gms/location/copresence/internal/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 109
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/debug/a;->a(Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;Landroid/os/Parcel;)V

    .line 110
    return-void
.end method
