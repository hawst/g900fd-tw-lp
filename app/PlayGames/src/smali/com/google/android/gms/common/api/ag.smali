.class public final Lcom/google/android/gms/common/api/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Ljava/lang/Object;

.field private final b:Lcom/google/android/gms/common/api/ah;


# direct methods
.method constructor <init>(Landroid/os/Looper;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/gms/common/api/ah;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/api/ah;-><init>(Lcom/google/android/gms/common/api/ag;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/ag;->b:Lcom/google/android/gms/common/api/ah;

    .line 93
    const-string v0, "Listener must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/ag;->a:Ljava/lang/Object;

    .line 94
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/ai;)V
    .locals 2

    .prologue
    .line 101
    const-string v0, "Notifier must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/common/api/ag;->b:Lcom/google/android/gms/common/api/ah;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/api/ah;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/common/api/ag;->b:Lcom/google/android/gms/common/api/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/ah;->sendMessage(Landroid/os/Message;)Z

    .line 104
    return-void
.end method
