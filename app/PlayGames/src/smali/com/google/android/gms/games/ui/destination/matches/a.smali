.class public final Lcom/google/android/gms/games/ui/destination/matches/a;
.super Lcom/google/android/gms/games/ui/common/matches/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/f;
.implements Lcom/google/android/gms/games/ui/c/a/i;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/destination/g;

.field private final b:Lcom/google/android/gms/games/ui/destination/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/g;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/u;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    .line 61
    new-instance v0, Lcom/google/android/gms/games/ui/destination/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/destination/a/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->b:Lcom/google/android/gms/games/ui/destination/a/a;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 6

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    const-string v0, "DestInboxHelper"

    const-string v1, "acceptInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const v2, 0x7f0f0176

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/destination/g;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-static {v1}, Lcom/google/android/gms/games/ui/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/c/f;

    move-result-object v1

    .line 122
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 127
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 128
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/matches/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/matches/b;-><init>(Lcom/google/android/gms/games/ui/destination/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "DestInboxHelper"

    const-string v1, "launchGameForInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "invitation"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v1}, Lcom/google/android/gms/games/internal/az;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 6

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 467
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 469
    const-string v0, "DestInboxHelper"

    const-string v1, "launchGameForMatch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :goto_0
    return-void

    .line 474
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 476
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "turn_based_match"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v1}, Lcom/google/android/gms/games/internal/az;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    goto :goto_0
.end method

.method private g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 482
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 484
    const-string v0, "DestInboxHelper"

    const-string v1, "launchGameForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_0
    return-void

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const v2, 0x7f0f0179

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/destination/g;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 489
    invoke-static {v1}, Lcom/google/android/gms/games/ui/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/c/f;

    move-result-object v1

    .line 490
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 493
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 494
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/matches/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/matches/c;-><init>(Lcom/google/android/gms/games/ui/destination/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 198
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    const-string v0, "DestInboxHelper"

    const-string v1, "onInvitationGameInfoClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 241
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 242
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 243
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/a;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 245
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.SHOW_PUBLIC_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 233
    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 234
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/g;->startActivity(Landroid/content/Intent;)V

    .line 237
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    const-string v0, "DestInboxHelper"

    const-string v1, "onInvitationAccepted: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    if-nez v2, :cond_1

    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    .line 86
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/games/ui/c/a/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/Invitation;)Lcom/google/android/gms/games/ui/c/a/e;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/e/ao;)V

    goto :goto_0

    .line 100
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 218
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    const-string v0, "DestInboxHelper"

    const-string v1, "onInvitationParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 282
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 283
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 285
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 287
    const-string v0, "DestInboxHelper"

    const-string v1, "onMatchClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    .line 295
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 297
    if-nez v1, :cond_1

    .line 300
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 301
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 306
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/ui/c/a/h;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lcom/google/android/gms/games/ui/c/a/h;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 316
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/a;->a(Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 379
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    const-string v0, "DestInboxHelper"

    const-string v1, "onMatchParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/f;)V
    .locals 4

    .prologue
    .line 256
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    .line 257
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->c()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v1

    .line 259
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    .line 261
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    .line 262
    invoke-interface {v2}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 264
    const-string v0, "DestInboxHelper"

    const-string v1, "onTurnBasedMatchInitiated: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :goto_0
    return-void

    .line 267
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 268
    invoke-static {}, Lcom/google/android/gms/games/ui/c/d;->P()Lcom/google/android/gms/games/ui/c/d;

    move-result-object v0

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_1
    if-nez v1, :cond_2

    .line 273
    const-string v1, "DestInboxHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No turn-based match received after accepting invite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->b:Lcom/google/android/gms/games/ui/destination/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/destination/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 212
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 250
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 251
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/a;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 250
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 253
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    const-string v0, "DestInboxHelper"

    const-string v1, "onInvitationDeclined: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    .line 152
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    .line 153
    packed-switch v2, :pswitch_data_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :pswitch_0
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->u()V

    goto :goto_0

    .line 159
    :pswitch_1
    sget-object v2, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/realtime/b;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 323
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 325
    const-string v0, "DestInboxHelper"

    const-string v1, "onMatchDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 330
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->u()V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->b:Lcom/google/android/gms/games/ui/destination/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/destination/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 373
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 171
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    const-string v0, "DestInboxHelper"

    const-string v1, "onInvitationDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    .line 179
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    .line 180
    packed-switch v2, :pswitch_data_0

    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :pswitch_0
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->u()V

    goto :goto_0

    .line 186
    :pswitch_1
    sget-object v2, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/realtime/b;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 339
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 341
    const-string v0, "DestInboxHelper"

    const-string v1, "onMatchRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 349
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 351
    if-nez v2, :cond_1

    .line 354
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 355
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 360
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x1

    invoke-static {v2, v1, v0, v3}, Lcom/google/android/gms/games/ui/c/a/h;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lcom/google/android/gms/games/ui/c/a/h;

    move-result-object v0

    .line 363
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 108
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 416
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    const-string v0, "DestInboxHelper"

    const-string v1, "switchAccountForTurnBasedMatch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 425
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 393
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    const-string v0, "DestInboxHelper"

    const-string v1, "switchAccountForInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 400
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 402
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    invoke-static {p1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/e/ao;)V

    goto :goto_0

    .line 409
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 434
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 436
    const-string v0, "DestInboxHelper"

    const-string v1, "switchAccountForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :goto_0
    return-void

    .line 441
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/matches/a;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method
