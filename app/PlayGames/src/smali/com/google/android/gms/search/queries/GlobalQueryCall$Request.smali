.class public Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/queries/e;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/gms/search/queries/e;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->e:I

    .line 69
    return-void
.end method

.method constructor <init>(ILjava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput p1, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->e:I

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->a:Ljava/lang/String;

    .line 89
    iput p3, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->b:I

    .line 90
    iput p4, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->c:I

    .line 91
    iput-object p5, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    .line 92
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/e;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/e;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/queries/e;->a(Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;Landroid/os/Parcel;I)V

    .line 104
    return-void
.end method
