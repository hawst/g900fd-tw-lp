.class public final Lcom/google/android/gms/games/ui/c/f;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"


# instance fields
.field private aj:I

.field private ak:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/c/f;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/google/android/gms/games/ui/c/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/f;-><init>()V

    iput v3, v0, Lcom/google/android/gms/games/ui/c/f;->aj:I

    iput v3, v0, Lcom/google/android/gms/games/ui/c/f;->ak:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "isCancelable"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "isIndeterminate"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "max"

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/f;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    const-string v0, "progress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/c/f;->aj:I

    .line 60
    const-string v0, "secondaryProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/c/f;->ak:I

    .line 63
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 64
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    sget v2, Lcom/google/android/gms/l;->bk:I

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 66
    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 67
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setCustomTitle(Landroid/view/View;)V

    .line 69
    const-string v1, "isIndeterminate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 70
    const-string v1, "max"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 71
    iget v1, p0, Lcom/google/android/gms/games/ui/c/f;->aj:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 72
    iget v1, p0, Lcom/google/android/gms/games/ui/c/f;->ak:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setSecondaryProgress(I)V

    .line 73
    const-string v1, "isCancelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/c/f;->b(Z)V

    .line 74
    return-object v2
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "progress"

    iget v1, p0, Lcom/google/android/gms/games/ui/c/f;->aj:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v0, "secondaryProgress"

    iget v1, p0, Lcom/google/android/gms/games/ui/c/f;->ak:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    return-void
.end method
