.class public final Lcom/google/android/gms/games/ui/e/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(IIF)I
    .locals 9

    .prologue
    .line 2130
    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    .line 2131
    shr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    .line 2132
    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 2133
    and-int/lit16 v3, p0, 0xff

    .line 2135
    shr-int/lit8 v4, p1, 0x18

    and-int/lit16 v4, v4, 0xff

    .line 2136
    shr-int/lit8 v5, p1, 0x10

    and-int/lit16 v5, v5, 0xff

    .line 2137
    shr-int/lit8 v6, p1, 0x8

    and-int/lit16 v6, v6, 0xff

    .line 2138
    and-int/lit16 v7, p1, 0xff

    .line 2141
    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v8, p2

    .line 2142
    int-to-float v0, v0

    mul-float/2addr v0, p2

    int-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 2143
    int-to-float v1, v1

    mul-float/2addr v1, p2

    int-to-float v4, v5

    mul-float/2addr v4, v8

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 2144
    int-to-float v2, v2

    mul-float/2addr v2, p2

    int-to-float v4, v6

    mul-float/2addr v4, v8

    add-float/2addr v2, v4

    float-to-int v2, v2

    .line 2145
    int-to-float v3, v3

    mul-float/2addr v3, p2

    int-to-float v4, v7

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 2148
    shl-int/lit8 v0, v0, 0x18

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method public static a(IZ)I
    .locals 1

    .prologue
    .line 2178
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    if-eqz p1, :cond_0

    .line 2179
    const/4 p0, 0x2

    move v0, p0

    .line 2181
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2190
    const/4 v0, 0x5

    :pswitch_0
    return v0

    :cond_0
    move v0, p0

    goto :goto_0

    .line 2181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;I)I
    .locals 3

    .prologue
    .line 1626
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The level must be higher or equal to 1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 1628
    sget v0, Lcom/google/android/gms/b;->b:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 1629
    add-int/lit8 v1, p1, -0x1

    array-length v2, v0

    rem-int/2addr v1, v2

    .line 1630
    aget v0, v0, v1

    return v0

    .line 1626
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/ax;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2037
    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 2039
    invoke-interface {p0}, Lcom/google/android/gms/games/ui/ax;->Y()Landroid/app/Activity;

    move-result-object v1

    .line 2040
    instance-of v0, v1, Lcom/google/android/gms/games/ui/n;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2041
    check-cast v0, Lcom/google/android/gms/games/ui/n;

    .line 2042
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 2043
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2052
    :goto_0
    return v0

    .line 2049
    :cond_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2050
    invoke-interface {p0}, Lcom/google/android/gms/games/ui/ax;->Y()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/google/android/gms/c;->b:I

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2052
    iget v0, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 1144
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.play.games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156
    :goto_0
    return-object p0

    .line 1152
    :cond_0
    :try_start_0
    const-string v0, "com.google.android.play.games"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 1156
    :catch_0
    move-exception v0

    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 671
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 672
    const-string v1, "com.google.android.play.games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 677
    invoke-static {p1}, Lcom/google/android/gms/common/internal/s;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 679
    if-eqz p2, :cond_1

    .line 680
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pcampaignid"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 683
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 686
    :cond_1
    return-object v0

    .line 672
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 906
    new-instance v1, Lcom/google/android/gms/common/a/a/a;

    const-string v0, "com.google.android.gms.common.acl.UPDATE_CIRCLES"

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/a/a/a;-><init>(Ljava/lang/String;)V

    .line 907
    invoke-interface {v1, p1}, Lcom/google/android/gms/common/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;

    .line 908
    invoke-static {p2}, Lcom/google/android/gms/people/internal/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;

    .line 911
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 912
    invoke-interface {v1, p3}, Lcom/google/android/gms/common/a/a/d;->a(Ljava/util/List;)Lcom/google/android/gms/common/a/a/d;

    .line 915
    sget v0, Lcom/google/android/gms/l;->K:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 921
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/common/a/a/d;->b(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;

    .line 923
    invoke-interface {v1}, Lcom/google/android/gms/common/a/a/d;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 919
    :cond_0
    sget v0, Lcom/google/android/gms/l;->J:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2094
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096
    new-array v0, v5, [Landroid/graphics/drawable/Drawable;

    aput-object p0, v0, v4

    .line 2097
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2098
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2115
    :goto_0
    return-object v0

    .line 2104
    :cond_0
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 2105
    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 2106
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 2107
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object p0, v2, v4

    aput-object v0, v2, v5

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2110
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 2111
    new-array v2, v5, [I

    const v3, 0x10100a7

    aput v3, v2, v4

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2112
    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2113
    new-array v2, v5, [I

    const v3, 0x10100a1

    aput v3, v2, v4

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2114
    new-array v1, v4, [I

    invoke-virtual {v0, v1, p0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1561
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1572
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1573
    sget v1, Lcom/google/android/gms/i;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1574
    sget v0, Lcom/google/android/gms/g;->ag:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1576
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1577
    return-object v1
.end method

.method public static a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2001
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/ab;

    move-result-object v0

    .line 2002
    const/4 v1, 0x0

    .line 2004
    instance-of v2, v0, Lcom/google/android/gms/games/ui/ax;

    if-eqz v2, :cond_6

    .line 2005
    check-cast v0, Lcom/google/android/gms/games/ui/ax;

    .line 2006
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->R()Z

    move-result v1

    .line 2009
    :goto_0
    if-nez v1, :cond_5

    move v2, v1

    move-object v1, p0

    .line 2011
    :cond_0
    instance-of v4, v1, Lcom/google/android/gms/games/ui/ax;

    if-eqz v4, :cond_1

    move-object v0, v1

    .line 2012
    check-cast v0, Lcom/google/android/gms/games/ui/ax;

    .line 2013
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->R()Z

    move-result v2

    .line 2014
    if-nez v2, :cond_3

    .line 2015
    :cond_1
    if-eqz v1, :cond_2

    .line 2019
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->l()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2021
    :cond_2
    if-nez v1, :cond_0

    .line 2024
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 2027
    :goto_2
    return-object v0

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move-object v0, v3

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1316
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1317
    instance-of v0, v1, Lcom/google/android/gms/common/data/n;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1318
    check-cast v0, Lcom/google/android/gms/common/data/n;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/n;->g_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1322
    :cond_0
    :goto_0
    return-object v1

    .line 1318
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Player;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1722
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    .line 1723
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1724
    sget v1, Lcom/google/android/gms/l;->D:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1728
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 873
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 874
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 877
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 878
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 879
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 880
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 877
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 882
    :cond_0
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 886
    :cond_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 887
    return-object v2
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/e/ao;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 538
    sget v0, Lcom/google/android/gms/l;->O:I

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 541
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 542
    sget v2, Lcom/google/android/gms/i;->e:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 543
    sget v0, Lcom/google/android/gms/g;->af:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 545
    sget v3, Lcom/google/android/gms/l;->N:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 547
    new-instance v0, Lcom/google/android/gms/games/ui/e/ak;

    invoke-direct {v0, p2, p1, p0}, Lcom/google/android/gms/games/ui/e/ak;-><init>(Lcom/google/android/gms/games/ui/e/ao;Lcom/google/android/gms/games/multiplayer/Invitation;Landroid/app/Activity;)V

    .line 569
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 570
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 571
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 572
    const v1, 0x104000a

    invoke-virtual {v3, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 573
    const/high16 v1, 0x1040000

    invoke-virtual {v3, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 576
    sget v1, Lcom/google/android/gms/l;->cb:I

    invoke-virtual {v3, v1, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 577
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 578
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 579
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 580
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    .line 581
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 1505
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "android:id/titleDivider"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1506
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1507
    if-eqz v0, :cond_0

    .line 1508
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1513
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 405
    invoke-static {p5}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "UiUtils"

    const-string v1, "googleApiClient not connected, ignoring request for participant list"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, p3

    if-lez v1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, p3

    new-array v5, v1, [Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v0

    move v1, v0

    :goto_1
    if-ge v4, v6, :cond_4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    aput-object v0, v5, v1

    move v0, v2

    move-object v1, v3

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto :goto_1

    :cond_3
    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    :cond_4
    add-int/2addr v1, p3

    if-eqz v3, :cond_5

    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/Participant;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    aput-object v0, v5, v1

    :cond_5
    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.destination.VIEW_PARTICIPANT_LIST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.PARTICIPANTS"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v2, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.FEATURED_URI"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.ICON_URI"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 837
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 840
    if-nez p2, :cond_0

    .line 841
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 843
    :cond_0
    const-string v0, "com.google.android.gms.games.GAME"

    new-instance v1, Lcom/google/android/gms/games/GameEntity;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/GameEntity;-><init>(Lcom/google/android/gms/games/Game;)V

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 844
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.LAUNCH_GAME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 845
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 852
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 855
    const-string v1, "package"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 856
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 858
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 859
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
    .locals 3

    .prologue
    .line 774
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 775
    invoke-static {p2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 776
    invoke-static {p3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 780
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 781
    const-string v2, "com.google.android.gms.games.SNAPSHOT_METADATA"

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 782
    invoke-static {v1, p1}, Lcom/google/android/gms/games/internal/az;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 783
    invoke-static {p0, p2, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 784
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Landroid/view/View;Landroid/support/v4/view/ao;I)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1942
    sget v0, Lcom/google/android/gms/g;->cs:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1943
    invoke-virtual {p2, p3}, Landroid/support/v4/view/ao;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1945
    const/4 v0, 0x0

    .line 1946
    instance-of v1, p2, Lcom/google/android/gms/games/ui/e/l;

    if-eqz v1, :cond_0

    .line 1947
    check-cast p2, Lcom/google/android/gms/games/ui/e/l;

    invoke-interface {p2}, Lcom/google/android/gms/games/ui/e/l;->g()Lcom/google/android/gms/games/ui/e/k;

    move-result-object v0

    .line 1951
    :cond_0
    if-eqz v0, :cond_5

    .line 1952
    invoke-interface {v0, p3}, Lcom/google/android/gms/games/ui/e/k;->b(I)I

    move-result v0

    move v1, v0

    .line 1955
    :goto_0
    sget v0, Lcom/google/android/gms/g;->aG:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1956
    if-lez v1, :cond_4

    .line 1958
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1959
    const/16 v4, 0xa

    if-lt v1, v4, :cond_1

    .line 1961
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 1962
    sget v4, Lcom/google/android/gms/e;->f:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1968
    :goto_1
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1971
    const/16 v4, 0x10

    invoke-static {v4}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1972
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1977
    :goto_2
    const/16 v3, 0x63

    if-le v1, v3, :cond_3

    .line 1978
    sget v1, Lcom/google/android/gms/l;->P:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1982
    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1986
    :goto_4
    return-void

    .line 1965
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    goto :goto_1

    .line 1974
    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1980
    :cond_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 1984
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public static a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1488
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->l()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1489
    if-eqz v0, :cond_0

    .line 1490
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    .line 1494
    :goto_0
    return-void

    .line 1492
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/support/v7/widget/bn;)V
    .locals 1

    .prologue
    .line 1778
    instance-of v0, p0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 1779
    check-cast p0, Landroid/widget/ImageView;

    .line 1780
    sget v0, Lcom/google/android/gms/f;->m:I

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1781
    new-instance v0, Lcom/google/android/gms/games/ui/e/am;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/e/am;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p1, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bo;

    .line 1788
    iget-object v0, p1, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->b()V

    .line 1789
    return-void
.end method

.method public static a(Landroid/widget/TextView;F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1815
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    .line 1817
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1818
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 1819
    aget-object v0, v0, v5

    .line 1820
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    div-float/2addr v1, p1

    .line 1821
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 1822
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v1

    float-to-int v2, v2

    .line 1823
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    float-to-int v1, v1

    .line 1824
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1825
    invoke-static {v0, v1, v2, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1826
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1827
    invoke-virtual {p0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1830
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/ui/n;)V
    .locals 3

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.VIEW_SHOP_GAMES_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 364
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    .line 371
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v0, "UiUtils"

    const-string v1, "showPopularMultiplayer: Trying to show Popular Multiplayer screen when not in the destination app"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;)V
    .locals 3

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 601
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_MY_PROFILE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 602
    const-string v2, "com.google.android.gms.games.PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 603
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    .line 604
    return-void
.end method

.method public static varargs a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 614
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_MY_PROFILE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 615
    const-string v2, "com.google.android.gms.games.PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 616
    const-string v0, "com.google.android.gms.games.ANIMATION"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 618
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 619
    invoke-static {p0, p2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 621
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 625
    :goto_0
    return-void

    .line 623
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V
    .locals 1

    .prologue
    .line 1756
    invoke-static {p0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 1757
    sget v0, Lcom/google/android/gms/e;->x:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 1759
    sget v0, Lcom/google/android/gms/e;->y:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 1761
    return-void
.end method

.method public static varargs a(ZI[Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1677
    if-eqz p0, :cond_0

    move p1, v0

    .line 1678
    :cond_0
    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 1679
    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1678
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1681
    :cond_1
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 1334
    packed-switch p0, :pswitch_data_0

    .line 1344
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1342
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1334
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z
    .locals 2

    .prologue
    .line 1449
    invoke-static {p0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 1450
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 1451
    invoke-interface {p0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1452
    const-string v0, "UiUtils"

    const-string v1, "googleApiClient not connected...calling activity.finish()"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454
    const-string v0, "UiUtils"

    const-string v1, "calling setResult RESULT_RECONNECT_REQUIRED on activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    const/16 v0, 0x2711

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/n;->setResult(I)V

    .line 1457
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->finish()V

    .line 1458
    const/4 v0, 0x1

    .line 1460
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 523
    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/res/Resources;I)I
    .locals 4

    .prologue
    .line 1636
    invoke-static {p0, p1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 1637
    const/4 v1, 0x3

    new-array v1, v1, [F

    .line 1638
    invoke-static {v0, v1}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 1639
    const/4 v0, 0x2

    aget v2, v1, v0

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    aput v2, v1, v0

    .line 1640
    invoke-static {v1}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/support/v4/app/Fragment;)I
    .locals 2

    .prologue
    .line 2057
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v1

    .line 2058
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 2059
    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/ax;)I

    move-result v0

    return v0

    .line 2058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 704
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/ui/l;->o:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v1}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/ui/l;->q:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v1}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 706
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 708
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    :goto_0
    return-void

    .line 709
    :catch_0
    move-exception v0

    .line 710
    const-string v1, "UiUtils"

    const-string v2, "Unable to launch play store intent"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1697
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1700
    :cond_0
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;)V
    .locals 3

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 636
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 637
    const-string v2, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 638
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    .line 639
    return-void
.end method

.method public static varargs b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 648
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 649
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 650
    const-string v2, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 651
    const-string v0, "com.google.android.gms.games.ANIMATION"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 653
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 654
    invoke-static {p0, p2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 656
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 660
    :goto_0
    return-void

    .line 658
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1167
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1170
    :try_start_0
    const-string v2, "com.google.android.play.games"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1173
    const/4 v0, 0x1

    .line 1175
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1709
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1710
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1712
    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1187
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1189
    :try_start_0
    const-string v2, "com.google.android.play.games"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1191
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1192
    const v2, 0x1312d00

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1196
    :cond_0
    :goto_0
    return v0

    .line 1194
    :catch_0
    move-exception v1

    const-string v1, "UiUtils"

    const-string v2, "Cannot find the installed destination app. Something is clearly wrong."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
