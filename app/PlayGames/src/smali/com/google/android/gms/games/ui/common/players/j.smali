.class public final Lcom/google/android/gms/games/ui/common/players/j;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/res/Resources;Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Lcom/google/android/gms/games/ui/widget/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/widget/a;-><init>()V

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 98
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 104
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    sget v0, Lcom/google/android/gms/l;->bL:I

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 107
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    return-void

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Landroid/widget/TextView;)V
    .locals 12

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->e()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    .line 46
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 47
    if-nez p0, :cond_1

    const/4 v0, 0x1

    .line 48
    :goto_0
    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v4

    .line 50
    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/PlayerLevel;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 51
    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-int v2, v6

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v0

    sub-long v0, v6, v0

    long-to-int v1, v0

    .line 56
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    .line 57
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    new-instance v5, Landroid/graphics/drawable/ClipDrawable;

    const/4 v6, 0x3

    const/4 v7, 0x1

    invoke-direct {v5, v0, v6, v7}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 60
    const/4 v0, 0x0

    .line 61
    if-lez v2, :cond_0

    .line 62
    const-wide v6, 0x40c3880000000000L    # 10000.0

    int-to-double v8, v1

    int-to-double v10, v2

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v0, v6

    .line 64
    :cond_0
    invoke-virtual {v5, v0}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    .line 65
    invoke-virtual {p2, v5}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    invoke-virtual {p2, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 70
    invoke-virtual {p2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 71
    sget v0, Lcom/google/android/gms/l;->bM:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sub-int v1, v2, v1

    int-to-long v8, v1

    invoke-virtual {v3, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {p1, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 88
    :goto_1
    return-void

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    goto :goto_0

    .line 80
    :cond_2
    sget v0, Lcom/google/android/gms/l;->bK:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/games/PlayerLevelInfo;)Z
    .locals 6

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->c()J

    move-result-wide v0

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/games/PlayerLevelInfo;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    sget-object v0, Lcom/google/android/gms/games/ui/l;->j:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
