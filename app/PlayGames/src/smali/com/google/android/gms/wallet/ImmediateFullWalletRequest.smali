.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:I

.field b:Landroid/accounts/Account;

.field c:Ljava/lang/String;

.field d:I

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/lang/String;

.field i:Z

.field j:[Lcom/google/android/gms/wallet/CountrySpecification;

.field k:Ljava/util/ArrayList;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/wallet/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    .line 120
    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    .line 105
    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    .line 106
    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    .line 107
    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    .line 108
    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d:I

    .line 109
    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    .line 110
    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    .line 111
    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    .line 112
    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    .line 113
    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    .line 114
    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    .line 115
    iput-object p12, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    .line 116
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 129
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/g;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    .line 130
    return-void
.end method
