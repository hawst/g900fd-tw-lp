.class public final Lcom/google/android/gms/games/ui/common/players/a;
.super Lcom/google/android/gms/games/ui/e;
.source "SourceFile"


# static fields
.field private static final h:I


# instance fields
.field private g:Ljava/util/HashMap;

.field private final i:Lcom/google/android/gms/games/ui/common/players/b;

.field private j:Ljava/lang/String;

.field private k:Z

.field private final l:I

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    sget v0, Lcom/google/android/gms/i;->D:I

    sput v0, Lcom/google/android/gms/games/ui/common/players/a;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 98
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;ZIII)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;ZIII)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/e;-><init>(Landroid/content/Context;)V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->g:Ljava/util/HashMap;

    .line 105
    const-string v0, "PlayerAvatarEventListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->i:Lcom/google/android/gms/games/ui/common/players/b;

    .line 107
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/common/players/a;->k:Z

    .line 108
    iput p4, p0, Lcom/google/android/gms/games/ui/common/players/a;->l:I

    .line 109
    iput p5, p0, Lcom/google/android/gms/games/ui/common/players/a;->m:I

    .line 111
    sget v0, Lcom/google/android/gms/h;->i:I

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/games/ui/common/players/a;->d(II)V

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/players/a;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/players/a;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->m:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->i:Lcom/google/android/gms/games/ui/common/players/b;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/players/a;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->l:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;I)V
    .locals 5

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 125
    const-string v0, "PlayerAvatarAdapter"

    const-string v1, "Player to mark as added is null. Let\'s do nothing in this case."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Lcom/google/android/gms/common/a/a/c;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 135
    if-nez v3, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->g:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :goto_1
    invoke-virtual {p0, p3}, Lcom/google/android/gms/games/ui/common/players/a;->c(I)V

    goto :goto_0

    .line 138
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    .line 140
    if-lez v1, :cond_2

    .line 141
    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 145
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->g:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/a;->j:Ljava/lang/String;

    .line 121
    return-void
.end method

.method protected final synthetic b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/g;
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/games/ui/common/players/a;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/players/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/common/players/c;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 152
    sget v0, Lcom/google/android/gms/g;->ab:I

    return v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/google/android/gms/e;->M:I

    return v0
.end method
