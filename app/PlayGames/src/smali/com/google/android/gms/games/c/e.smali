.class public final Lcom/google/android/gms/games/c/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:Z

.field public e:I

.field public f:Z

.field public g:Z

.field public h:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5834
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5835
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/e;->c:J

    iput-boolean v2, p0, Lcom/google/android/gms/games/c/e;->d:Z

    iput v2, p0, Lcom/google/android/gms/games/c/e;->e:I

    iput-boolean v2, p0, Lcom/google/android/gms/games/c/e;->f:Z

    iput-boolean v2, p0, Lcom/google/android/gms/games/c/e;->g:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/e;->G:I

    .line 5836
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5948
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 5949
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5950
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5953
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 5954
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5957
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/c/e;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 5958
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/games/c/e;->c:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 5961
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->d:Z

    if-eqz v2, :cond_3

    .line 5962
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/e;->d:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5965
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/c/e;->e:I

    if-eqz v2, :cond_4

    .line 5966
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/gms/games/c/e;->e:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 5969
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->f:Z

    if-eqz v2, :cond_5

    .line 5970
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/e;->f:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5973
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->g:Z

    if-eqz v2, :cond_6

    .line 5974
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/e;->g:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5977
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->h:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 5979
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 5980
    iget-object v3, p0, Lcom/google/android/gms/games/c/e;->h:[I

    aget v3, v3, v1

    .line 5981
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 5979
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5984
    :cond_7
    add-int/2addr v0, v2

    .line 5985
    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5987
    :cond_8
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5766
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/e;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/e;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/e;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/e;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/e;->g:Z

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/android/gms/games/c/e;->h:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/games/c/e;->h:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/gms/games/c/e;->h:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->h()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->h:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->h()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/android/gms/games/c/e;->h:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 5917
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5918
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5920
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5921
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5923
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/games/c/e;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 5924
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/c/e;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 5926
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/e;->d:Z

    if-eqz v0, :cond_3

    .line 5927
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/e;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5929
    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/c/e;->e:I

    if-eqz v0, :cond_4

    .line 5930
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/games/c/e;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5932
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/e;->f:Z

    if-eqz v0, :cond_5

    .line 5933
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/e;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5935
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/e;->g:Z

    if-eqz v0, :cond_6

    .line 5936
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/e;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5938
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v0, v0

    if-lez v0, :cond_7

    .line 5939
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->h:[I

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 5940
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->h:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5939
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5943
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 5944
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5853
    if-ne p1, p0, :cond_1

    .line 5893
    :cond_0
    :goto_0
    return v0

    .line 5856
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 5857
    goto :goto_0

    .line 5859
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/e;

    .line 5860
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 5861
    iget-object v2, p1, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 5862
    goto :goto_0

    .line 5864
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5865
    goto :goto_0

    .line 5867
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 5868
    iget-object v2, p1, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 5869
    goto :goto_0

    .line 5871
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 5872
    goto :goto_0

    .line 5874
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/games/c/e;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/e;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 5875
    goto :goto_0

    .line 5877
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/e;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 5878
    goto :goto_0

    .line 5880
    :cond_8
    iget v2, p0, Lcom/google/android/gms/games/c/e;->e:I

    iget v3, p1, Lcom/google/android/gms/games/c/e;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 5881
    goto :goto_0

    .line 5883
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/e;->f:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 5884
    goto :goto_0

    .line 5886
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/e;->g:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/e;->g:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 5887
    goto :goto_0

    .line 5889
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/c/e;->h:[I

    iget-object v3, p1, Lcom/google/android/gms/games/c/e;->h:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5891
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 5898
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5901
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 5903
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/e;->c:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/e;->c:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/2addr v0, v1

    .line 5905
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/e;->d:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v1

    .line 5906
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/e;->e:I

    add-int/2addr v0, v1

    .line 5907
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/e;->f:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v1

    .line 5908
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/e;->g:Z

    if-eqz v1, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 5909
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->h:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 5911
    return v0

    .line 5898
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5901
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/e;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    move v0, v3

    .line 5905
    goto :goto_2

    :cond_3
    move v0, v3

    .line 5907
    goto :goto_3

    :cond_4
    move v2, v3

    .line 5908
    goto :goto_4
.end method
