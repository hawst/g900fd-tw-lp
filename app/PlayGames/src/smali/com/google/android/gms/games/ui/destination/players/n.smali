.class final Lcom/google/android/gms/games/ui/destination/players/n;
.super Landroid/support/v4/app/bj;
.source "SourceFile"


# instance fields
.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Landroid/widget/TextView;

.field final synthetic e:Lcom/google/android/gms/games/ui/destination/players/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/m;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/n;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/n;->c:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/players/n;->d:Landroid/widget/TextView;

    invoke-direct {p0}, Landroid/support/v4/app/bj;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0c01a6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->a(Lcom/google/android/gms/games/ui/destination/players/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 244
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->b(Lcom/google/android/gms/games/ui/destination/players/m;)Z

    .line 247
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 250
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->d(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/n;->c:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/players/m;->e(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v3

    invoke-static {v1, v0, v2, v3, v5}, Lcom/google/android/play/c/b;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->c(Lcom/google/android/gms/games/ui/destination/players/m;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v0

    .line 253
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->f(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/n;->c:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/players/m;->g(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v4, v2, v3, v6}, Lcom/google/android/play/c/b;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;

    move-result-object v1

    .line 256
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/players/m;->h(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/b;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 257
    invoke-virtual {v2, v0}, Landroid/view/Window;->setEnterTransition(Landroid/transition/Transition;)V

    .line 260
    new-instance v0, Landroid/transition/Slide;

    const/16 v3, 0x30

    invoke-direct {v0, v3}, Landroid/transition/Slide;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/n;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 262
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/transition/Transition;

    aput-object v1, v3, v6

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->c(Lcom/google/android/gms/games/ui/destination/players/m;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 265
    invoke-virtual {v2, v0}, Landroid/view/Window;->setReturnTransition(Landroid/transition/Transition;)V

    .line 267
    const-string v0, "level"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/n;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/n;->e:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/m;->i(Lcom/google/android/gms/games/ui/destination/players/m;)Z

    .line 274
    :cond_0
    return-void
.end method
