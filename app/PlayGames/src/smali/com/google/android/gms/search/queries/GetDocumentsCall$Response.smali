.class public Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/queries/b;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:Lcom/google/android/gms/appdatasearch/DocumentResults;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/google/android/gms/search/queries/b;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->c:I

    .line 126
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;Lcom/google/android/gms/appdatasearch/DocumentResults;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput p1, p0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->c:I

    .line 143
    iput-object p2, p0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 144
    iput-object p3, p0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->b:Lcom/google/android/gms/appdatasearch/DocumentResults;

    .line 145
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/b;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/queries/b;->a(Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;Landroid/os/Parcel;I)V

    .line 157
    return-void
.end method
