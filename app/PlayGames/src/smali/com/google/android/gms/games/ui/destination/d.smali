.class final Lcom/google/android/gms/games/ui/destination/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/b;)V
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/d;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 4

    .prologue
    .line 803
    check-cast p1, Lcom/google/android/gms/games/n;

    invoke-interface {p1}, Lcom/google/android/gms/games/n;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/d;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->b(Lcom/google/android/gms/games/ui/destination/b;)Lcom/google/android/gms/games/ui/destination/s;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/d;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->b(Lcom/google/android/gms/games/ui/destination/b;)Lcom/google/android/gms/games/ui/destination/s;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/n;->c()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/n;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/s;->a(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/b;->N()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateInboxCount - failed with status:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
