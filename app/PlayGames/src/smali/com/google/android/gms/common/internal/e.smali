.class public abstract Lcom/google/android/gms/common/internal/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/f;
.implements Lcom/google/android/gms/common/internal/o;


# static fields
.field public static final f:[Ljava/lang/String;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public final c:Ljava/util/ArrayList;

.field public final d:[Ljava/lang/String;

.field e:Z

.field private final g:Landroid/os/Looper;

.field private final h:Ljava/lang/Object;

.field private i:Landroid/os/IInterface;

.field private j:Lcom/google/android/gms/common/internal/j;

.field private k:I

.field private final l:Lcom/google/android/gms/common/internal/m;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 419
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/internal/e;->f:[Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->h:Ljava/lang/Object;

    .line 382
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    .line 395
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/e;->k:I

    .line 408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/e;->e:Z

    .line 450
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    .line 451
    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->g:Landroid/os/Looper;

    .line 452
    new-instance v0, Lcom/google/android/gms/common/internal/m;

    invoke-direct {v0, p2, p0}, Lcom/google/android/gms/common/internal/m;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/o;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->l:Lcom/google/android/gms/common/internal/m;

    .line 456
    new-instance v0, Lcom/google/android/gms/common/internal/f;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/common/internal/f;-><init>(Lcom/google/android/gms/common/internal/e;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    .line 457
    invoke-virtual {p0, p5}, Lcom/google/android/gms/common/internal/e;->a([Ljava/lang/String;)V

    .line 458
    iput-object p5, p0, Lcom/google/android/gms/common/internal/e;->d:[Ljava/lang/String;

    .line 460
    invoke-static {p3}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/v;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/api/v;)V

    .line 461
    invoke-static {p4}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/api/w;)V

    .line 462
    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/f;Lcom/google/android/gms/common/g;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 483
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/internal/h;

    invoke-direct {v3, p2}, Lcom/google/android/gms/common/internal/h;-><init>(Lcom/google/android/gms/common/f;)V

    new-instance v4, Lcom/google/android/gms/common/internal/k;

    invoke-direct {v4, p3}, Lcom/google/android/gms/common/internal/k;-><init>(Lcom/google/android/gms/common/g;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;[Ljava/lang/String;)V

    .line 485
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/m;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->l:Lcom/google/android/gms/common/internal/m;

    return-object v0
.end method

.method private a(ILandroid/os/IInterface;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 653
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    move v3, v0

    :goto_0
    if-eqz p2, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->b(Z)V

    .line 654
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 655
    :try_start_0
    iput p1, p0, Lcom/google/android/gms/common/internal/e;->k:I

    .line 656
    iput-object p2, p0, Lcom/google/android/gms/common/internal/e;->i:Landroid/os/IInterface;

    .line 657
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v3, v1

    .line 653
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 657
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/e;ILandroid/os/IInterface;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IInterface;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/common/internal/e;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/j;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/common/internal/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/j;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 838
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/gms/common/internal/l;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/l;-><init>(Lcom/google/android/gms/common/internal/e;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 840
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->l:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 939
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/w;)V
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->l:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/g;)V

    .line 945
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/internal/aa;Lcom/google/android/gms/common/internal/i;)V
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public b()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 875
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 806
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ab;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/internal/aa;

    move-result-object v0

    .line 807
    new-instance v1, Lcom/google/android/gms/common/internal/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/internal/i;-><init>(Lcom/google/android/gms/common/internal/e;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/aa;Lcom/google/android/gms/common/internal/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 816
    :goto_0
    return-void

    .line 809
    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 813
    :catch_1
    move-exception v0

    .line 814
    const-string v1, "GmsClient"

    const-string v2, "Remote exception occurred"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected abstract b_()Ljava/lang/String;
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v1, 0x1

    .line 695
    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/e;->e:Z

    .line 697
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IInterface;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/h;->a(Landroid/content/Context;)I

    move-result v0

    .line 700
    if-eqz v0, :cond_1

    .line 703
    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IInterface;)V

    .line 704
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 727
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    if-eqz v0, :cond_2

    .line 710
    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling connect() while still connected, missing disconnect() for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/j;)V

    .line 715
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/internal/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/j;-><init>(Lcom/google/android/gms/common/internal/e;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    .line 716
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;Lcom/google/android/gms/common/internal/j;)Z

    move-result v0

    .line 720
    if-nez v0, :cond_0

    .line 721
    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final c_()Z
    .locals 1

    .prologue
    .line 933
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/e;->e:Z

    return v0
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 748
    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/e;->e:Z

    .line 750
    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    monitor-enter v2

    .line 752
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 753
    :goto_0
    if-ge v1, v3, :cond_0

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/g;->H_()V

    .line 753
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 757
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 760
    const/4 v0, 0x1

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IInterface;)V

    .line 761
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/j;)V

    .line 764
    iput-object v4, p0, Lcom/google/android/gms/common/internal/e;->j:Lcom/google/android/gms/common/internal/j;

    .line 766
    :cond_1
    return-void

    .line 757
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 731
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 732
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/e;->k:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 733
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 741
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 742
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/e;->k:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 743
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Landroid/content/Context;
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 850
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 853
    :cond_0
    return-void
.end method

.method public final i()Landroid/os/IInterface;
    .locals 3

    .prologue
    .line 888
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 889
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/e;->k:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 890
    new-instance v0, Landroid/os/DeadObjectException;

    invoke-direct {v0}, Landroid/os/DeadObjectException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 895
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 892
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->h()V

    .line 893
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->i:Landroid/os/IInterface;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Client is connected but service is null"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 894
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->i:Landroid/os/IInterface;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 893
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
