.class public final Lcom/google/android/gms/wallet/wobs/LabelValueRow;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/util/ArrayList;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/wallet/wobs/d;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/wobs/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->d:I

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c:Ljava/util/ArrayList;

    .line 74
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->d:I

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->a:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->b:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c:Ljava/util/ArrayList;

    .line 66
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->d:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 43
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/wobs/d;->a(Lcom/google/android/gms/wallet/wobs/LabelValueRow;Landroid/os/Parcel;)V

    .line 44
    return-void
.end method
