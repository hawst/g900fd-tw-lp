.class public final Lcom/google/android/gms/games/c/ao;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/c/an;

.field public b:Lcom/google/android/gms/games/c/s;

.field public c:I

.field public d:Lcom/google/android/gms/games/c/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5594
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5595
    iput-object v1, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    iput-object v1, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/ao;->c:I

    iput-object v1, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ao;->G:I

    .line 5596
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 5682
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 5683
    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-eqz v1, :cond_0

    .line 5684
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5687
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-eqz v1, :cond_1

    .line 5688
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5691
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/ao;->c:I

    if-eqz v1, :cond_2

    .line 5692
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/ao;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5695
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-eqz v1, :cond_3

    .line 5696
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5699
    :cond_3
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5559
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/an;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/an;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/s;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/ao;->c:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/c/d;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5665
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-eqz v0, :cond_0

    .line 5666
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5668
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-eqz v0, :cond_1

    .line 5669
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5671
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/ao;->c:I

    if-eqz v0, :cond_2

    .line 5672
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/ao;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5674
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-eqz v0, :cond_3

    .line 5675
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5677
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 5678
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5609
    if-ne p1, p0, :cond_1

    .line 5646
    :cond_0
    :goto_0
    return v0

    .line 5612
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ao;

    if-nez v2, :cond_2

    move v0, v1

    .line 5613
    goto :goto_0

    .line 5615
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ao;

    .line 5616
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-nez v2, :cond_3

    .line 5617
    iget-object v2, p1, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-eqz v2, :cond_4

    move v0, v1

    .line 5618
    goto :goto_0

    .line 5621
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/an;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5622
    goto :goto_0

    .line 5625
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-nez v2, :cond_5

    .line 5626
    iget-object v2, p1, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-eqz v2, :cond_6

    move v0, v1

    .line 5627
    goto :goto_0

    .line 5630
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/s;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 5631
    goto :goto_0

    .line 5634
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/c/ao;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/ao;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 5635
    goto :goto_0

    .line 5637
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-nez v2, :cond_8

    .line 5638
    iget-object v2, p1, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5639
    goto :goto_0

    .line 5642
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5643
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5651
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5654
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5656
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/ao;->c:I

    add-int/2addr v0, v2

    .line 5657
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5659
    return v0

    .line 5651
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->a:Lcom/google/android/gms/games/c/an;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/an;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5654
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ao;->b:Lcom/google/android/gms/games/c/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/s;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5657
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/ao;->d:Lcom/google/android/gms/games/c/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/d;->hashCode()I

    move-result v1

    goto :goto_2
.end method
