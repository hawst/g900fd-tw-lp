.class final Lcom/google/android/gms/games/internal/m;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/ag;)V
    .locals 0

    .prologue
    .line 738
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 739
    iput-object p1, p0, Lcom/google/android/gms/games/internal/m;->a:Lcom/google/android/gms/common/api/ag;

    .line 740
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lcom/google/android/gms/games/internal/m;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v1, Lcom/google/android/gms/games/internal/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 764
    return-void
.end method

.method public final l(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 745
    new-instance v1, Lcom/google/android/gms/games/multiplayer/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 746
    const/4 v0, 0x0

    .line 748
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 749
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 752
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    .line 756
    if-eqz v0, :cond_1

    .line 757
    iget-object v1, p0, Lcom/google/android/gms/games/internal/m;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v2, Lcom/google/android/gms/games/internal/n;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/n;-><init>(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 759
    :cond_1
    return-void

    .line 752
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    throw v0
.end method
