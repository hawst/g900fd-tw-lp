.class public abstract Lcom/google/android/gms/games/ui/destination/g;
.super Lcom/google/android/gms/games/ui/n;
.source "SourceFile"


# instance fields
.field private A:Landroid/app/Dialog;

.field private B:Z

.field public u:Lcom/google/android/gms/games/app/a;

.field protected v:Ljava/lang/String;

.field protected w:Z

.field protected x:Z

.field protected y:Z

.field private z:Lcom/google/android/gms/games/ui/b/a;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/destination/g;-><init>(II)V

    .line 79
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/destination/g;-><init>(IIZ)V

    .line 95
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/destination/g;-><init>(IIZZ)V

    .line 114
    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 133
    const/4 v1, 0x3

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/n;-><init>(IIIIZZ)V

    .line 59
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/g;->w:Z

    .line 65
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/g;->B:Z

    .line 68
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/g;->y:Z

    .line 136
    return-void
.end method


# virtual methods
.method public final O()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 272
    const-string v0, "showWarmWelcome"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 276
    const-string v3, "showOnBoardingFlow"

    invoke-static {p0, v3, v1}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    .line 280
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 281
    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->y:Z

    if-nez v0, :cond_0

    .line 283
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;)I

    move-result v0

    .line 284
    const-string v3, "lastVersionWhatsNewShown"

    invoke-static {p0, v3, v0}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 288
    const-string v0, "showWarmWelcome"

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 294
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/g;->y:Z

    .line 295
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 296
    const/16 v1, 0x7d1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/g;->startActivityForResult(Landroid/content/Intent;I)V

    .line 298
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 280
    goto :goto_0
.end method

.method public final P()Lcom/google/android/gms/games/app/a;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 249
    const-string v0, "DestFragActivityBase"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    .line 263
    :goto_0
    return-void

    .line 256
    :cond_0
    const-string v1, "lastSignedInAccount"

    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lcom/android/a/a;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 259
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/g;->w:Z

    .line 261
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/app/a;->a(Ljava/lang/String;)V

    .line 262
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/n;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 338
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->c()I

    move-result v1

    .line 339
    const-string v2, "DestFragActivityBase"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connection to service apk failed with error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/g;->y:Z

    if-eqz v2, :cond_1

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 351
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/g;->B:Z

    if-nez v1, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->x:Z

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->n:Z

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    const/16 v0, 0x385

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/a;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    const-string v0, "DestFragActivityBase"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    goto :goto_0

    .line 351
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 360
    :cond_3
    const/4 v2, 0x5

    if-ne v1, v2, :cond_4

    .line 362
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->w:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->x:Z

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->l()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    goto :goto_0

    .line 365
    :cond_4
    invoke-static {v1, p0}, Lcom/google/android/gms/common/h;->a(ILandroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->A:Landroid/app/Dialog;

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->A:Landroid/app/Dialog;

    if-nez v0, :cond_5

    .line 368
    const-string v0, "DestFragActivityBase"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    goto :goto_0

    .line 372
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->A:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/gms/common/api/u;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/n;->a(Lcom/google/android/gms/common/api/u;)V

    .line 193
    new-instance v1, Lcom/google/android/gms/games/h;

    invoke-direct {v1, v4}, Lcom/google/android/gms/games/h;-><init>(B)V

    .line 194
    iput-boolean v4, v1, Lcom/google/android/gms/games/h;->b:Z

    const/16 v0, 0x11

    iput v0, v1, Lcom/google/android/gms/games/h;->c:I

    .line 196
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->w:Z

    iput-boolean v0, v1, Lcom/google/android/gms/games/h;->d:Z

    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 200
    const-string v0, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    const-string v3, "com.google.android.gms.games.FORCE_RESOLVE_ACCOUNT_KEY"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/games/h;->f:Ljava/lang/String;

    .line 209
    const-string v0, "com.google.android.gms.games.FORCE_RESOLVE_ACCOUNT_KEY"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 211
    :cond_0
    const-string v0, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 213
    sget-object v0, Lcom/google/android/gms/games/d;->e:Lcom/google/android/gms/common/api/a;

    new-instance v2, Lcom/google/android/gms/games/g;

    invoke-direct {v2, v1, v4}, Lcom/google/android/gms/games/g;-><init>(Lcom/google/android/gms/games/h;B)V

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/common/api/u;->a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/u;

    .line 214
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 448
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->z:Lcom/google/android/gms/games/ui/b/a;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->z:Lcom/google/android/gms/games/ui/b/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/b/a;->a(Landroid/view/KeyEvent;)V

    .line 403
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/n;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->z:Lcom/google/android/gms/games/ui/b/a;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->z:Lcom/google/android/gms/games/ui/b/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/b/a;->a(Landroid/view/MotionEvent;)V

    .line 394
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/n;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected final h()Lcom/google/android/gms/common/api/t;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 140
    new-instance v3, Lcom/google/android/gms/common/api/u;

    invoke-direct {v3, p0, p0, p0}, Lcom/google/android/gms/common/api/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;)V

    sget-object v0, Lcom/google/android/gms/feedback/b;->a:Lcom/google/android/gms/common/api/a;

    iget-object v1, v3, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    invoke-interface {v1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/gms/common/api/a;->c:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v6, v3, Lcom/google/android/gms/common/api/u;->b:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->w:Z

    if-eqz v0, :cond_3

    .line 150
    iput-object v7, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    .line 178
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/common/api/u;->a:Ljava/lang/String;

    .line 185
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/g;->a(Lcom/google/android/gms/common/api/u;)V

    .line 187
    invoke-virtual {v3}, Lcom/google/android/gms/common/api/u;->a()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    return-object v0

    .line 151
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->B:Z

    .line 157
    iput-object v7, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    goto :goto_1

    .line 158
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iput-object v7, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    .line 175
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/g;->B:Z

    goto :goto_1
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 431
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 433
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.games.SHOW_GOOGLE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 434
    const-string v2, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v0, "com.google.android.play.games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/z;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 438
    const-string v2, "com.google.android.gms.games.DEST_APP_VERSION"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/g;->startActivity(Landroid/content/Intent;)V

    .line 443
    :goto_0
    return-void

    .line 441
    :cond_0
    const-string v0, "DestFragActivityBase"

    const-string v1, "onShowSettings: googleApiClient not connected; ignoring menu click"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 302
    const/16 v0, 0x7d1

    if-ne p1, v0, :cond_2

    .line 303
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/g;->y:Z

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 311
    const-string v0, "com.google.android.gms.games.ON_BOARDING_ACCOUNT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 316
    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->v:Ljava/lang/String;

    .line 317
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->l()V

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    .line 334
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 328
    const-string v0, "DestFragActivityBase"

    const-string v1, "User has backed out of on-boarding flow, finishing."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->finish()V

    .line 333
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/n;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/g;->getIntent()Landroid/content/Intent;

    .line 221
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/n;->onCreate(Landroid/os/Bundle;)V

    .line 223
    sget-object v0, Lcom/google/android/gms/games/ui/l;->a:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Lcom/google/android/gms/games/ui/b/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/b/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->z:Lcom/google/android/gms/games/ui/b/a;

    .line 226
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/app/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/app/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    .line 227
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Lcom/google/android/gms/games/ui/n;->onStart()V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/app/a;->a()V

    .line 233
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/android/gms/games/ui/n;->onStop()V

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/app/a;->b()V

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->A:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->A:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 242
    :cond_0
    return-void
.end method
