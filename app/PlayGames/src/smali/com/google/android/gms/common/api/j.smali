.class public abstract Lcom/google/android/gms/common/api/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aj;
.implements Lcom/google/android/gms/common/api/m;


# instance fields
.field protected final a:Lcom/google/android/gms/common/api/l;

.field private final b:Ljava/lang/Object;

.field private final c:Ljava/util/concurrent/CountDownLatch;

.field private final d:Ljava/util/ArrayList;

.field private e:Lcom/google/android/gms/common/api/an;

.field private volatile f:Lcom/google/android/gms/common/api/am;

.field private volatile g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/google/android/gms/common/internal/u;


# direct methods
.method protected constructor <init>(Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    .line 224
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->d:Ljava/util/ArrayList;

    .line 239
    new-instance v0, Lcom/google/android/gms/common/api/l;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/l;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->a:Lcom/google/android/gms/common/api/l;

    .line 240
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/common/api/l;)V
    .locals 2

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    .line 224
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->d:Ljava/util/ArrayList;

    .line 247
    iput-object p1, p0, Lcom/google/android/gms/common/api/j;->a:Lcom/google/android/gms/common/api/l;

    .line 248
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/j;)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->i()V

    return-void
.end method

.method private b(Lcom/google/android/gms/common/api/am;)V
    .locals 4

    .prologue
    .line 441
    iput-object p1, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->j:Lcom/google/android/gms/common/internal/u;

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->e:Lcom/google/android/gms/common/api/an;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->a:Lcom/google/android/gms/common/api/l;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/l;->a()V

    .line 449
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->h:Z

    if-nez v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->a:Lcom/google/android/gms/common/api/l;

    iget-object v2, p0, Lcom/google/android/gms/common/api/j;->e:Lcom/google/android/gms/common/api/an;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->g()Lcom/google/android/gms/common/api/am;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/an;Lcom/google/android/gms/common/api/am;)V

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ak;

    .line 455
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/ak;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 458
    return-void
.end method

.method private f()Z
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 263
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 265
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/j;->d()V

    .line 268
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 423
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 424
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/am;)V

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/j;->i:Z

    .line 428
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 432
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 433
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/am;)V

    .line 435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/j;->i:Z

    .line 437
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/am;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 275
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "await must not be called on the UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 277
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Result has already been consumed"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 284
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->g()Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 275
    goto :goto_0

    :cond_1
    move v1, v2

    .line 277
    goto :goto_1

    .line 281
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->h()V

    goto :goto_2
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/am;
.end method

.method public final a(Ljava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 290
    cmp-long v0, v4, v4

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 292
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v0, :cond_3

    :goto_1
    const-string v0, "Result has already been consumed."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->c:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 295
    if-nez v0, :cond_1

    .line 296
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->i()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 302
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->g()Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    .line 290
    goto :goto_0

    :cond_3
    move v2, v1

    .line 292
    goto :goto_1

    .line 299
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->h()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/api/ak;)V
    .locals 2

    .prologue
    .line 346
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 348
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/ak;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 354
    :goto_1
    monitor-exit v1

    return-void

    .line 346
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 401
    iget-object v3, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v3

    .line 402
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/j;->i:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/j;->h:Z

    if-eqz v2, :cond_1

    .line 403
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/am;)V

    .line 404
    monitor-exit v3

    .line 410
    :goto_0
    return-void

    .line 406
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 407
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 409
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/j;->b(Lcom/google/android/gms/common/api/am;)V

    .line 410
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v1

    .line 406
    goto :goto_1

    :cond_3
    move v0, v1

    .line 407
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/api/an;)V
    .locals 3

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 309
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 310
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    monitor-exit v1

    .line 319
    :goto_1
    return-void

    .line 308
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 313
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->a:Lcom/google/android/gms/common/api/l;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/j;->g()Lcom/google/android/gms/common/api/am;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/an;Lcom/google/android/gms/common/api/am;)V

    .line 319
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 317
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/common/api/j;->e:Lcom/google/android/gms/common/api/an;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/common/internal/u;)V
    .locals 2

    .prologue
    .line 361
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 362
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/common/api/j;->j:Lcom/google/android/gms/common/internal/u;

    .line 363
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 215
    check-cast p1, Lcom/google/android/gms/common/api/am;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/am;)V

    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 368
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 369
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    if-eqz v0, :cond_1

    .line 371
    :cond_0
    monitor-exit v1

    .line 385
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->j:Lcom/google/android/gms/common/internal/u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 375
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->j:Lcom/google/android/gms/common/internal/u;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/u;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381
    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    invoke-static {v0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/am;)V

    .line 382
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/j;->e:Lcom/google/android/gms/common/api/an;

    .line 383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/j;->h:Z

    .line 384
    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/j;->b(Lcom/google/android/gms/common/api/am;)V

    .line 385
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 390
    iget-object v1, p0, Lcom/google/android/gms/common/api/j;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 391
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/j;->h:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/j;->g:Z

    .line 418
    iput-object v1, p0, Lcom/google/android/gms/common/api/j;->f:Lcom/google/android/gms/common/api/am;

    .line 419
    iput-object v1, p0, Lcom/google/android/gms/common/api/j;->e:Lcom/google/android/gms/common/api/an;

    .line 420
    return-void
.end method
