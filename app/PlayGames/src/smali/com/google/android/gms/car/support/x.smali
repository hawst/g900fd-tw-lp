.class final Lcom/google/android/gms/car/support/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/u;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 272
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 277
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->c()V

    .line 283
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/u;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 342
    invoke-static {p0, p1}, Lcom/google/android/gms/car/support/u;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 347
    .line 348
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    if-ltz v2, :cond_1

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 350
    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    add-int/2addr v3, v0

    .line 348
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 350
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 352
    :cond_1
    return v3
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 302
    new-instance v0, Lcom/google/android/gms/car/support/v;

    iget-object v1, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/v;-><init>(Lcom/google/android/gms/car/support/u;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/u;->a(Ljava/lang/Object;)I

    move-result v0

    .line 308
    if-ltz v0, :cond_0

    .line 309
    iget-object v1, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/u;->a(I)V

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eq v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eq v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/u;->a()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/u;->b(I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/u;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/car/support/u;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
