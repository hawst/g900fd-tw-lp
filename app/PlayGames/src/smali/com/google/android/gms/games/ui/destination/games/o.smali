.class final Lcom/google/android/gms/games/ui/destination/games/o;
.super Landroid/app/SharedElementCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Landroid/content/res/Resources;

.field final synthetic e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/games/o;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->b:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/games/o;->c:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/google/android/gms/games/ui/destination/games/o;->d:Landroid/content/res/Resources;

    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 642
    const/4 v0, 0x0

    .line 643
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 644
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->n(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 649
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->o(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    .line 652
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 653
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 654
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 659
    const/16 v1, 0xa

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->d(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->p(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->e(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c010d

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c0110

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c0114

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c010b

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c0251

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v5, v6, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 668
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 669
    if-nez v1, :cond_3

    instance-of v2, v0, Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    .line 675
    check-cast v0, Landroid/widget/ImageView;

    .line 676
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v7, v0

    .line 678
    :goto_0
    if-eqz v7, :cond_1

    .line 679
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->d:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-static {v0, v7, v2, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/graphics/drawable/Drawable;J)V

    .line 684
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v1, 0x7f0c010a

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->d:Landroid/content/res/Resources;

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->q(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/app/Activity;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/Transition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v0

    .line 692
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setEnterTransition(Landroid/transition/Transition;)V

    .line 694
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/o;->a:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v5}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->s(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v5

    const v6, 0x7f0c024f

    invoke-virtual {v5, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/destination/games/o;->c:Landroid/view/ViewGroup;

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->h(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v0

    .line 699
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setReturnTransition(Landroid/transition/Transition;)V

    .line 702
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->t(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)Landroid/transition/Transition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->h(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v0

    .line 707
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/o;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 709
    :cond_2
    return-void

    :cond_3
    move-object v7, v1

    goto/16 :goto_0
.end method
