.class public final Lcom/google/android/gms/games/ui/d/h;
.super Landroid/transition/Transition;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:[I

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android:silhouetteExpando:bounds"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android:silhouetteExpando:windowX"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android:silhouetteExpando:windowY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/d/h;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->b:[I

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/h;->d:Landroid/graphics/drawable/Drawable;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d/h;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private a(Landroid/transition/TransitionValues;)V
    .locals 5

    .prologue
    .line 59
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 60
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 63
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/h;->b:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 65
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:windowX"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/h;->b:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:windowY"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/h;->b:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method


# virtual methods
.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d/h;->a(Landroid/transition/TransitionValues;)V

    .line 52
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d/h;->a(Landroid/transition/TransitionValues;)V

    .line 56
    return-void
.end method

.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 71
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 72
    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->e:Landroid/graphics/Rect;

    iget-object v0, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->f:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->f:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/gms/games/ui/d/h;->c:I

    iget v2, p0, Lcom/google/android/gms/games/ui/d/h;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setX(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/h;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setY(F)V

    const-string v0, "alpha"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    invoke-static {v1, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/google/android/gms/games/ui/d/i;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/games/ui/d/i;-><init>(Lcom/google/android/gms/games/ui/d/h;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 74
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/games/ui/d/h;->a:[Ljava/lang/String;

    return-object v0
.end method
