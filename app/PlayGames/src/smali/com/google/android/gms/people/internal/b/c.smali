.class public final Lcom/google/android/gms/people/internal/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/people/f;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 30
    if-eqz p2, :cond_0

    .line 32
    :goto_0
    new-instance v0, Lcom/google/android/gms/people/internal/b/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/people/internal/b/d;-><init>(Lcom/google/android/gms/people/internal/b/c;Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/people/f;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    sget-object p2, Lcom/google/android/gms/people/f;->a:Lcom/google/android/gms/people/f;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 106
    if-eqz p3, :cond_0

    move-object v5, p3

    .line 108
    :goto_0
    new-instance v0, Lcom/google/android/gms/people/internal/b/f;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/internal/b/f;-><init>(Lcom/google/android/gms/people/internal/b/c;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0

    .line 106
    :cond_0
    sget-object v5, Lcom/google/android/gms/people/d;->a:Lcom/google/android/gms/people/d;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/gms/people/internal/b/h;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/internal/b/h;-><init>(Lcom/google/android/gms/people/internal/b/c;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method
