.class public final Lcom/google/android/gms/car/support/j;
.super Lcom/google/android/gms/car/a;
.source "SourceFile"


# instance fields
.field final b:Landroid/os/Handler;

.field final c:Lcom/google/android/gms/car/support/n;

.field d:Z

.field e:Z

.field f:Z

.field g:Z


# direct methods
.method public static h()V
    .locals 0

    .prologue
    .line 594
    return-void
.end method

.method public static i()V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method static j()V
    .locals 0

    .prologue
    .line 690
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 607
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Local FragmentActivity "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 608
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 609
    const-string v0, " State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 611
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCreated="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 612
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/j;->d:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "mResumed="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 613
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/j;->e:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mStopped="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 614
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/j;->f:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mReallyStopped="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 615
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/j;->g:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 616
    iget-object v0, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 617
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "View Hierarchy:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 618
    return-void
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 238
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/car/a;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    .line 242
    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    sget-object v2, Lcom/google/android/gms/car/support/k;->a:[I

    invoke-virtual {p2, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 244
    if-nez v0, :cond_1

    .line 245
    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 247
    :cond_1
    invoke-virtual {v4, v8, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 248
    invoke-virtual {v4, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 249
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/j;->a()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/gms/car/support/Fragment;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 254
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/car/a;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_2
    if-eq v2, v6, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/support/n;->a(I)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v1

    .line 268
    :cond_3
    if-nez v1, :cond_4

    if-eqz v5, :cond_4

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v1

    .line 271
    :cond_4
    if-nez v1, :cond_5

    .line 272
    iget-object v1, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/car/support/n;->a(I)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v1

    .line 275
    :cond_5
    const-string v4, "CAR.PROJECTION"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 276
    const-string v4, "CAR.PROJECTION"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onCreateView: id=0x"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fname="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " existing="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_6
    if-nez v1, :cond_8

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/j;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v4

    .line 282
    iput-boolean v8, v4, Lcom/google/android/gms/car/support/Fragment;->o:Z

    .line 283
    if-eqz v2, :cond_7

    move v1, v2

    :goto_1
    iput v1, v4, Lcom/google/android/gms/car/support/Fragment;->w:I

    .line 284
    iput v3, v4, Lcom/google/android/gms/car/support/Fragment;->x:I

    .line 285
    iput-object v5, v4, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    .line 286
    iput-boolean v8, v4, Lcom/google/android/gms/car/support/Fragment;->p:Z

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    iput-object v1, v4, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    .line 288
    iget-object v1, v4, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {v4}, Lcom/google/android/gms/car/support/Fragment;->f()V

    .line 289
    iget-object v1, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v1, v4, v8}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;Z)V

    move-object v1, v4

    .line 311
    :goto_2
    iget-object v3, v1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-nez v3, :cond_b

    .line 312
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " did not create a view."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move v1, v3

    .line 283
    goto :goto_1

    .line 291
    :cond_8
    iget-boolean v4, v1, Lcom/google/android/gms/car/support/Fragment;->p:Z

    if-eqz v4, :cond_9

    .line 294
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": Duplicate id 0x"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", tag "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", or parent id 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with another fragment for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 301
    :cond_9
    iput-boolean v8, v1, Lcom/google/android/gms/car/support/Fragment;->p:Z

    .line 305
    iget-boolean v3, v1, Lcom/google/android/gms/car/support/Fragment;->C:Z

    if-nez v3, :cond_a

    .line 306
    iget-object v3, v1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/Fragment;->f()V

    .line 308
    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;)V

    goto :goto_2

    .line 315
    :cond_b
    if-eqz v2, :cond_c

    .line 316
    iget-object v0, v1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 318
    :cond_c
    iget-object v0, v1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_d

    .line 319
    iget-object v0, v1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 321
    :cond_d
    iget-object v0, v1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    goto/16 :goto_0
.end method
