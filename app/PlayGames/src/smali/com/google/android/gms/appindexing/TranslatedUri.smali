.class public Lcom/google/android/gms/appindexing/TranslatedUri;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appindexing/a;


# instance fields
.field final a:I

.field final b:Landroid/net/Uri;

.field final c:Landroid/net/Uri;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/appindexing/a;

    invoke-direct {v0}, Lcom/google/android/gms/appindexing/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/appindexing/TranslatedUri;->CREATOR:Lcom/google/android/gms/appindexing/a;

    return-void
.end method

.method constructor <init>(ILandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->a:I

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->b:Landroid/net/Uri;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->c:Landroid/net/Uri;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->d:Ljava/lang/String;

    .line 52
    iput-object p5, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->e:Ljava/lang/String;

    .line 53
    iput p6, p0, Lcom/google/android/gms/appindexing/TranslatedUri;->f:I

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/gms/appindexing/TranslatedUri;->CREATOR:Lcom/google/android/gms/appindexing/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/appindexing/TranslatedUri;->CREATOR:Lcom/google/android/gms/appindexing/a;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appindexing/a;->a(Lcom/google/android/gms/appindexing/TranslatedUri;Landroid/os/Parcel;I)V

    .line 98
    return-void
.end method
