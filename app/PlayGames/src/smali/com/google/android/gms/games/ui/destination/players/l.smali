.class final Lcom/google/android/gms/games/ui/destination/players/l;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 70
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 65
    check-cast p3, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/l;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->h()Landroid/net/Uri;

    move-result-object v1

    const v2, 0x7f02015c

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/l;->a(Landroid/net/Uri;I)V

    const/4 v1, 0x2

    const v2, 0x4003126f    # 2.048f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/l;->a(IF)V

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/l;->d(Ljava/lang/String;)V

    const v1, 0x7f0f00a9

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/l;->g(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/l;->k:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->e()J

    move-result-wide v2

    const/high16 v4, 0x80000

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/l;->f(Ljava/lang/String;)V

    const v1, 0x7f0f00a6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/l;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/l;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/j;

    .line 104
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/j;->a(Lcom/google/android/gms/games/ui/destination/players/j;)Lcom/google/android/gms/games/ui/destination/players/k;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/l;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/k;->a(Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V

    .line 105
    return-void
.end method
