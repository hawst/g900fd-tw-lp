.class public final Lcom/google/android/gms/games/ui/b/a/a;
.super Lcom/google/android/gms/games/ui/b/a/b;
.source "SourceFile"


# instance fields
.field private final e:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/b/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;)V

    .line 56
    invoke-static {p3}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/a;->e:Landroid/os/Bundle;

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/ui/b/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;Landroid/os/Bundle;)V

    .line 50
    sget-object v1, Lcom/google/android/gms/games/ui/b/a/a;->a:Lcom/google/android/gms/games/ui/b/a/d;

    sget-object v2, Lcom/google/android/gms/games/ui/b/a/a;->a:Lcom/google/android/gms/games/ui/b/a/d;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/games/ui/b/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/b/a/d;->sendMessage(Landroid/os/Message;)Z

    .line 51
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/a;->d:Landroid/view/View;

    const v1, 0x7f0c00f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 65
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 66
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/a;->e:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.games.extra.name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/a;->d:Landroid/view/View;

    const v1, 0x7f0c00f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    const v1, 0x7f0f0040

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/a;->d:Landroid/view/View;

    const v1, 0x7f0c00f7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020128

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 75
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/a;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 76
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    return-void
.end method
