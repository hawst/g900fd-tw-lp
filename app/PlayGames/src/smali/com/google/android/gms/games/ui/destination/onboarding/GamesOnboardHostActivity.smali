.class public Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;
.super Landroid/support/v4/app/ab;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/gms/games/app/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/ab;-><init>()V

    return-void
.end method

.method private e()Lcom/google/android/gms/games/ui/destination/onboarding/d;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const-string v1, "GamesOnboardActivity"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ag;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/d;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/app/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->n:Lcom/google/android/gms/games/app/a;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    const-string v0, "showOnBoardingFlow"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 97
    const-string v1, "com.google.android.gms.games.ON_BOARDING_ACCOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->setResult(ILandroid/content/Intent;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->finish()V

    .line 100
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->e()Lcom/google/android/gms/games/ui/destination/onboarding/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->X()Z

    .line 105
    invoke-super {p0}, Landroid/support/v4/app/ab;->onBackPressed()V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 32
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 34
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 37
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/ab;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->e()Lcom/google/android/gms/games/ui/destination/onboarding/d;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    invoke-virtual {v0}, Landroid/support/v4/app/ag;->a()Landroid/support/v4/app/ar;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/onboarding/d;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/destination/onboarding/d;-><init>()V

    const v2, 0x1020002

    const-string v3, "GamesOnboardActivity"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/ar;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ar;

    invoke-virtual {v0}, Landroid/support/v4/app/ar;->a()I

    .line 39
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/app/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/app/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->n:Lcom/google/android/gms/games/app/a;

    .line 40
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Landroid/support/v4/app/ab;->onResume()V

    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->e()Lcom/google/android/gms/games/ui/destination/onboarding/d;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->R()V

    .line 81
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/support/v4/app/ab;->onStart()V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->n:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/app/a;->a()V

    .line 50
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/support/v4/app/ab;->onStop()V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->n:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/app/a;->b()V

    .line 56
    return-void
.end method
