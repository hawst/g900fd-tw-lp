.class public Lcom/google/android/gms/cloudsave/internal/GetRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/GetRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/List;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/GetRequest;->a:I

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/internal/GetRequest;->b:Ljava/util/List;

    .line 53
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/google/android/gms/cloudsave/internal/d;->a(Lcom/google/android/gms/cloudsave/internal/GetRequest;Landroid/os/Parcel;)V

    .line 40
    return-void
.end method
