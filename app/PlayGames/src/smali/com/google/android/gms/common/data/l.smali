.class public final Lcom/google/android/gms/common/data/l;
.super Lcom/google/android/gms/common/data/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/e;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashSet;

.field private e:Lcom/google/android/gms/common/data/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/m;-><init>(Lcom/google/android/gms/common/data/b;)V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/common/data/l;->c:Ljava/lang/String;

    .line 35
    new-instance v0, Lcom/google/android/gms/common/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 287
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 288
    if-eqz p2, :cond_0

    .line 289
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    iget-object v4, v0, Lcom/google/android/gms/common/data/b;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 293
    iget-object v5, p0, Lcom/google/android/gms/common/data/l;->c:Ljava/lang/String;

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    instance-of v6, v0, Lcom/google/android/gms/common/data/k;

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v7

    move v2, v1

    :goto_0
    if-ge v1, v7, :cond_4

    .line 298
    if-eqz v6, :cond_2

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    check-cast v0, Lcom/google/android/gms/common/data/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/k;->b(I)I

    move-result v0

    .line 303
    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v8

    .line 304
    invoke-virtual {v4, v5, v0, v8}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    .line 308
    if-eqz p2, :cond_5

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 310
    neg-int v0, v2

    add-int/lit8 v0, v0, -0x1

    .line 316
    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 317
    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 320
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    if-eqz p2, :cond_1

    .line 322
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 301
    goto :goto_1

    .line 312
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v10, v2

    move v2, v0

    move v0, v10

    goto :goto_2

    .line 327
    :cond_4
    return-object v3

    :cond_5
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/f;->a(Lcom/google/android/gms/common/data/d;)V

    .line 41
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 102
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const/4 v0, 0x0

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/f;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    .line 110
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/data/l;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/f;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 123
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    move v3, v6

    move v5, v6

    :goto_2
    if-ltz v7, :cond_5

    .line 124
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 125
    if-gez v4, :cond_2

    move v0, v2

    :goto_3
    if-nez v0, :cond_7

    .line 126
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 130
    iget-object v9, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 133
    if-nez v3, :cond_3

    move v0, v2

    move v3, v4

    .line 123
    :goto_4
    add-int/lit8 v4, v7, -0x1

    move v7, v4

    move v5, v3

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v6

    .line 125
    goto :goto_3

    .line 136
    :cond_3
    add-int/lit8 v0, v5, -0x1

    if-ne v4, v0, :cond_4

    .line 139
    add-int/lit8 v4, v5, -0x1

    .line 140
    add-int/lit8 v0, v3, 0x1

    move v3, v4

    goto :goto_4

    .line 144
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/common/data/f;->b_(II)V

    move v0, v2

    move v3, v4

    .line 146
    goto :goto_4

    .line 151
    :cond_5
    if-lez v3, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/common/data/f;->b_(II)V

    goto :goto_0

    .line 156
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-virtual {v0, v8}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_7
    move v0, v3

    move v3, v5

    goto :goto_4

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method

.method public final b(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    :goto_0
    return p1

    .line 58
    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/l;->a()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 59
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v1, v2, :cond_5

    .line 67
    iget-object v3, p0, Lcom/google/android/gms/common/data/l;->d:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 68
    if-ne v0, p1, :cond_3

    move p1, v1

    .line 73
    goto :goto_0

    .line 77
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 65
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 82
    :cond_5
    const/4 p1, -0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/f;->b(Lcom/google/android/gms/common/data/d;)V

    .line 46
    return-void
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/gms/common/data/m;->f_()V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/common/data/l;->e:Lcom/google/android/gms/common/data/f;

    iget-object v0, v0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 90
    return-void
.end method
