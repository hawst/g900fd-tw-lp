.class public final Lcom/google/android/gms/games/ui/c/e;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private aj:Lcom/google/android/gms/games/ui/common/players/h;

.field private ak:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0xb

    const/4 v2, -0x1

    .line 41
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 42
    instance-of v0, v1, Lcom/google/android/gms/games/ui/common/players/i;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Caller must provide profile visibility switching."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 45
    check-cast v0, Lcom/google/android/gms/games/ui/common/players/i;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/players/i;->C()Lcom/google/android/gms/games/ui/common/players/h;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/h;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/e;->aj:Lcom/google/android/gms/games/ui/common/players/h;

    .line 48
    if-nez p1, :cond_2

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/e;->aj:Lcom/google/android/gms/games/ui/common/players/h;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/players/h;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    .line 57
    :goto_0
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 58
    sget v3, Lcom/google/android/gms/i;->G:I

    invoke-virtual {v0, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 61
    sget v0, Lcom/google/android/gms/g;->cj:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 62
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 66
    sget v0, Lcom/google/android/gms/g;->cf:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 68
    iget-boolean v5, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    if-eqz v5, :cond_3

    .line 69
    sget v5, Lcom/google/android/gms/l;->bf:I

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 70
    invoke-static {v6}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1

    sget v2, Lcom/google/android/gms/d;->u:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 77
    :cond_1
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 82
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    if-eqz v0, :cond_4

    .line 83
    sget v2, Lcom/google/android/gms/l;->bb:I

    .line 84
    sget v0, Lcom/google/android/gms/l;->bd:I

    .line 90
    :goto_2
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 92
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    sget v5, Lcom/google/android/gms/l;->bj:I

    invoke-static {v1, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 94
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 95
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 96
    const v1, 0x1080027

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 97
    invoke-virtual {v4, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    invoke-virtual {v4, v0, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 54
    :cond_2
    const-string v0, "saveStatePublicStatus"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    goto :goto_0

    .line 73
    :cond_3
    sget v5, Lcom/google/android/gms/l;->be:I

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 74
    invoke-static {v6}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1

    sget v2, Lcom/google/android/gms/d;->k:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_1

    .line 86
    :cond_4
    sget v2, Lcom/google/android/gms/l;->ba:I

    .line 87
    sget v0, Lcom/google/android/gms/l;->bc:I

    goto :goto_2
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    const-string v0, "saveStatePublicStatus"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 109
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 111
    invoke-virtual {p0, v1}, Landroid/support/v4/app/x;->a(Z)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 113
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/e;->aj:Lcom/google/android/gms/games/ui/common/players/h;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/c/e;->ak:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/gms/games/ui/common/players/h;->a(Z)V

    .line 114
    invoke-virtual {p0, v1}, Landroid/support/v4/app/x;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 113
    goto :goto_1

    .line 116
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/gms/games/ui/l;->l:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 124
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 125
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/c/e;->a(Landroid/content/Intent;)V

    .line 126
    return-void
.end method
