.class final Lcom/google/android/gms/games/ui/destination/players/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/r;

.field final synthetic b:Lcom/google/android/gms/common/api/r;

.field final synthetic c:Lcom/google/android/gms/common/api/r;

.field final synthetic d:Lcom/google/android/gms/common/api/r;

.field final synthetic e:Lcom/google/android/gms/common/api/r;

.field final synthetic f:I

.field final synthetic g:Lcom/google/android/gms/games/ui/destination/players/s;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/s;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;I)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/t;->a:Lcom/google/android/gms/common/api/r;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/t;->b:Lcom/google/android/gms/common/api/r;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/players/t;->c:Lcom/google/android/gms/common/api/r;

    iput-object p5, p0, Lcom/google/android/gms/games/ui/destination/players/t;->d:Lcom/google/android/gms/common/api/r;

    iput-object p6, p0, Lcom/google/android/gms/games/ui/destination/players/t;->e:Lcom/google/android/gms/common/api/r;

    iput p7, p0, Lcom/google/android/gms/games/ui/destination/players/t;->f:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 195
    check-cast p1, Lcom/google/android/gms/common/api/q;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/s;->as:Lcom/google/android/gms/games/ui/ai;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ai;->c(Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/q;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->a:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/u;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/u;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->b:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/v;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/t;->c:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/w;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/t;->d:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/j;

    invoke-interface {v2}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-interface {v0}, Lcom/google/android/gms/games/v;->b()Z

    move-result v0

    invoke-static {v4, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/ui/destination/players/s;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/s;->c(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/destination/main/m;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/s;->b(Lcom/google/android/gms/games/ui/destination/players/s;)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/destination/main/m;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->e:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/x;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/x;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/players/s;->n()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/players/s;->o()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/destination/players/s;->ap:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_1
    invoke-interface {v2}, Lcom/google/android/gms/games/j;->f_()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/ui/destination/players/s;Lcom/google/android/gms/games/w;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/players/t;->f:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/t;->g:Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/s;->d(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v2}, Lcom/google/android/gms/games/j;->f_()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcom/google/android/gms/games/j;->f_()V

    throw v0
.end method
