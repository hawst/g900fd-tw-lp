.class public abstract Lcom/google/android/gms/games/ui/b/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# static fields
.field protected static final a:Lcom/google/android/gms/games/ui/b/a/d;

.field private static final e:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private static final f:Landroid/graphics/Rect;

.field private static final g:Landroid/graphics/Rect;


# instance fields
.field protected final b:Landroid/content/Context;

.field protected final c:Landroid/widget/FrameLayout;

.field protected d:Landroid/view/View;

.field private final h:Landroid/view/animation/Animation;

.field private final i:Landroid/view/animation/Animation;

.field private final j:Lcom/google/android/gms/games/ui/b/a/g;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 65
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/d;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/b/a/d;-><init>(Landroid/os/Looper;B)V

    sput-object v0, Lcom/google/android/gms/games/ui/b/a/b;->a:Lcom/google/android/gms/games/ui/b/a/d;

    .line 113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    .line 114
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/b/a/b;->g:Landroid/graphics/Rect;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->k:Z

    .line 155
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    .line 157
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    .line 159
    iput-object p2, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    const v1, 0x7f050012

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->h:Landroid/view/animation/Animation;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    const v1, 0x7f050008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->i:Landroid/view/animation/Animation;

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/b/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/b/a/g;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/games/ui/b/a/e;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/ui/b/a/e;-><init>(B)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/ui/b/a/b;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/b/a/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/b/a/b;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public static c()V
    .locals 5

    .prologue
    .line 231
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    sget-object v1, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    .line 233
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/b/a/b;

    .line 235
    invoke-direct {v0}, Lcom/google/android/gms/games/ui/b/a/b;->f()V

    .line 238
    sget-object v3, Lcom/google/android/gms/games/ui/b/a/b;->a:Lcom/google/android/gms/games/ui/b/a/d;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/games/ui/b/a/d;->removeMessages(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 241
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 242
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/b/a/b;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04002c

    iget-object v2, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    const v1, 0x7f0c00f6

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/b;->a()V

    return-void
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v1, v1, Lcom/google/android/gms/games/ui/b/a/g;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/b/a/a/a;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    .line 255
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/WindowManager;

    .line 257
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v7}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 258
    const/16 v0, 0x18

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 260
    const/4 v0, -0x3

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v2}, Landroid/widget/FrameLayout;->measure(II)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    .line 267
    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 268
    iput v2, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 270
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    const/16 v0, 0x3e8

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/b/a/g;->a:Landroid/os/IBinder;

    iput-object v0, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 276
    const/16 v0, 0x33

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 279
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v3, v3, Lcom/google/android/gms/games/ui/b/a/g;->d:I

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 280
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v3, v3, Lcom/google/android/gms/games/ui/b/a/g;->e:I

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 281
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v3, v3, Lcom/google/android/gms/games/ui/b/a/g;->g:I

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 282
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v3, v3, Lcom/google/android/gms/games/ui/b/a/g;->f:I

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v0, v0, Lcom/google/android/gms/games/ui/b/a/g;->b:I

    .line 286
    if-nez v0, :cond_0

    .line 287
    const/16 v0, 0x31

    .line 291
    :cond_0
    const/16 v3, 0x11

    invoke-static {v3}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    iget-object v3, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 294
    sget-object v3, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/gms/games/ui/b/a/b;->g:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v5

    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 299
    :goto_0
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 300
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 309
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-interface {v6, v0, v7}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->k:Z

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 321
    :goto_2
    return-void

    .line 296
    :cond_1
    sget-object v3, Lcom/google/android/gms/games/ui/b/a/b;->f:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/gms/games/ui/b/a/b;->g:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 302
    :cond_2
    const/16 v0, 0x7d5

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 303
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v0, v0, Lcom/google/android/gms/games/ui/b/a/g;->b:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_1

    .line 311
    :catch_0
    move-exception v0

    const-string v0, "BasePopup"

    const-string v1, "Cannot show the popup as the given window token is not valid. Either the given view is not attached to a window or you tried to connect the GoogleApiClient in the same lifecycle step as the creation of the GoogleApiClient. See GoogleApiClient.Builder.create() and GoogleApiClient.connect() for more information."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a/b;->e()V

    goto :goto_2
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/b/a/b;)V
    .locals 3

    .prologue
    .line 38
    sget-object v1, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    sget-object v2, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a/b;->d()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 324
    sget-object v1, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    .line 325
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a/b;->f()V

    .line 327
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 328
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/b/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/b/a/b;->d()V

    .line 331
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/b/a/b;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a/b;->e()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->k:Z

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->j:Lcom/google/android/gms/games/ui/b/a/g;

    iget v1, v1, Lcom/google/android/gms/games/ui/b/a/g;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/b/a/a/a;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    .line 344
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 346
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/b;->c:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->k:Z

    .line 349
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->h:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 357
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->a:Lcom/google/android/gms/games/ui/b/a/d;

    sget-object v1, Lcom/google/android/gms/games/ui/b/a/b;->a:Lcom/google/android/gms/games/ui/b/a/d;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gms/games/ui/b/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/b/a/d;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/b;->i:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 360
    sget-object v0, Lcom/google/android/gms/games/ui/b/a/b;->a:Lcom/google/android/gms/games/ui/b/a/d;

    new-instance v1, Lcom/google/android/gms/games/ui/b/a/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/b/a/c;-><init>(Lcom/google/android/gms/games/ui/b/a/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/b/a/d;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 370
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method
