.class public final Lcom/google/android/gms/games/c/ak;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/games/c/ak;


# instance fields
.field public a:I

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6478
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6479
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ak;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ak;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ak;->G:I

    .line 6480
    return-void
.end method

.method public static b()[Lcom/google/android/gms/games/c/ak;
    .locals 2

    .prologue
    .line 6461
    sget-object v0, Lcom/google/android/gms/games/c/ak;->c:[Lcom/google/android/gms/games/c/ak;

    if-nez v0, :cond_1

    .line 6462
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6464
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/c/ak;->c:[Lcom/google/android/gms/games/c/ak;

    if-nez v0, :cond_0

    .line 6465
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/c/ak;

    sput-object v0, Lcom/google/android/gms/games/c/ak;->c:[Lcom/google/android/gms/games/c/ak;

    .line 6467
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6469
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/c/ak;->c:[Lcom/google/android/gms/games/c/ak;

    return-object v0

    .line 6467
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 6530
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 6531
    iget v1, p0, Lcom/google/android/gms/games/c/ak;->a:I

    if-eq v1, v2, :cond_0

    .line 6532
    iget v1, p0, Lcom/google/android/gms/games/c/ak;->a:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6535
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ak;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 6536
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ak;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6539
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/ak;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ak;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 6519
    iget v0, p0, Lcom/google/android/gms/games/c/ak;->a:I

    if-eq v0, v1, :cond_0

    .line 6520
    iget v0, p0, Lcom/google/android/gms/games/c/ak;->a:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6522
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ak;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 6523
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ak;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6525
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 6526
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6491
    if-ne p1, p0, :cond_1

    .line 6504
    :cond_0
    :goto_0
    return v0

    .line 6494
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ak;

    if-nez v2, :cond_2

    move v0, v1

    .line 6495
    goto :goto_0

    .line 6497
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ak;

    .line 6498
    iget v2, p0, Lcom/google/android/gms/games/c/ak;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/ak;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 6499
    goto :goto_0

    .line 6501
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ak;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ak;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 6502
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 6509
    iget v0, p0, Lcom/google/android/gms/games/c/ak;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 6511
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ak;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ak;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 6513
    return v0
.end method
