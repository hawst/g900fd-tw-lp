.class public final Lcom/google/android/gms/games/ui/destination/c/a;
.super Lcom/google/android/gms/common/data/m;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/List;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/b;IJ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/m;-><init>(Lcom/google/android/gms/common/data/b;)V

    .line 27
    iput p2, p0, Lcom/google/android/gms/games/ui/destination/c/a;->d:I

    .line 28
    iget v2, p0, Lcom/google/android/gms/games/ui/destination/c/a;->d:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/c/a;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v3

    if-le v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numIndexes must be smaller or equal to max"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0, p3, p4}, Ljava/util/Random;-><init>(J)V

    invoke-static {v4, v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    invoke-virtual {v4, v1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/c/a;->c:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/c/a;->d:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/c/a;->b:Lcom/google/android/gms/common/data/b;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 3

    .prologue
    .line 38
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/c/a;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/c/a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
