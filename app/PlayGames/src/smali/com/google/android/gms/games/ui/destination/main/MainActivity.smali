.class public final Lcom/google/android/gms/games/ui/destination/main/MainActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/c/a/g;
.implements Lcom/google/android/gms/games/ui/c/a/j;
.implements Lcom/google/android/gms/games/ui/c/a/m;
.implements Lcom/google/android/gms/games/ui/c/a/p;
.implements Lcom/google/android/gms/games/ui/common/a/k;
.implements Lcom/google/android/gms/games/ui/common/a/l;
.implements Lcom/google/android/gms/games/ui/common/a/n;
.implements Lcom/google/android/gms/games/ui/common/matches/v;
.implements Lcom/google/android/gms/games/ui/common/requests/m;
.implements Lcom/google/android/gms/games/ui/destination/q;


# static fields
.field private static final A:[I

.field private static final z:Ljava/lang/String;


# instance fields
.field private B:Landroid/support/v4/app/Fragment;

.field private C:Lcom/google/android/gms/games/ui/destination/matches/a;

.field private D:Lcom/google/android/gms/games/ui/destination/requests/a;

.field private E:Lcom/google/android/gms/games/ui/common/a/b;

.field private F:Z

.field private G:Z

.field private H:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 77
    const-class v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->z:Ljava/lang/String;

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x3

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->A:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    const v0, 0x7f040037

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V
    .locals 2

    .prologue
    .line 71
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->l()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    return-void
.end method

.method static synthetic ak()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method private d(Z)V
    .locals 3

    .prologue
    const v2, 0x7f0c011f

    .line 233
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    .line 234
    if-eqz p1, :cond_0

    .line 235
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/c/b;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    .line 236
    invoke-virtual {v0}, Landroid/support/v4/app/ag;->a()Landroid/support/v4/app/ar;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/ar;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ar;

    .line 238
    invoke-virtual {v0}, Landroid/support/v4/app/ar;->a()I

    .line 245
    :goto_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V

    .line 248
    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/c/b;->b(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V

    .line 249
    return-void

    .line 240
    :cond_0
    invoke-virtual {v0, v2}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    const-string v1, "Failed to find fragment during resume!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->u()Landroid/view/View;

    move-result-object v0

    .line 406
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 407
    check-cast v0, Landroid/view/ViewGroup;

    .line 408
    if-eqz p1, :cond_1

    const/high16 v1, 0x20000

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 411
    :cond_0
    return-void

    .line 408
    :cond_1
    const/high16 v1, 0x60000

    goto :goto_0
.end method


# virtual methods
.method protected final F()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 260
    const-string v1, "drawerHasBeenOpened"

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 262
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected final I()V
    .locals 1

    .prologue
    .line 425
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->I()V

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->F:Z

    .line 430
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->u()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 431
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->e(Z)V

    .line 433
    :cond_0
    return-void
.end method

.method protected final J()V
    .locals 1

    .prologue
    .line 437
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->J()V

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->F:Z

    .line 439
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->e(Z)V

    .line 440
    return-void
.end method

.method public final Q()Lcom/google/android/gms/games/ui/c/a/f;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->C:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->C:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final af()Lcom/google/android/gms/games/ui/c/a/l;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->E:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method

.method public final ah()Lcom/google/android/gms/games/ui/c/a/i;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->C:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final ai()Lcom/google/android/gms/games/ui/c/a/o;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->D:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final aj()V
    .locals 1

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->F:Z

    if-eqz v0, :cond_0

    .line 419
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->e(Z)V

    .line 421
    :cond_0
    return-void
.end method

.method public final c()[I
    .locals 1

    .prologue
    .line 302
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->A:[I

    return-object v0
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 337
    const/4 v3, 0x0

    .line 338
    const/4 v2, -0x1

    .line 339
    const-string v4, "invitationsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 340
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 359
    :goto_0
    if-eqz v1, :cond_5

    .line 360
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 361
    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 362
    const-string v0, "com.google.android.gms.games.GAME_ID"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 363
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 367
    :goto_1
    return-void

    .line 342
    :cond_0
    const-string v4, "myTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 343
    const-string v0, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 344
    goto :goto_0

    .line 345
    :cond_1
    const-string v4, "theirTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 346
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 347
    const/4 v0, 0x2

    goto :goto_0

    .line 348
    :cond_2
    const-string v4, "completedMatchesButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 349
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 350
    const/4 v0, 0x3

    goto :goto_0

    .line 351
    :cond_3
    const-string v4, "giftsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 352
    const-string v1, "com.google.android.gms.games.destination.SHOW_REQUEST_LIST"

    goto :goto_0

    .line 354
    :cond_4
    const-string v0, "wishesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 355
    const-string v0, "com.google.android.gms.games.destination.SHOW_REQUEST_LIST"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 356
    goto :goto_0

    .line 365
    :cond_5
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->z:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSeeMoreClicked - Unexpected buttonTag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v0, v2

    move-object v1, v3

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->D:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 211
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->w:Z

    .line 212
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->G:Z

    .line 213
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 180
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 181
    const-string v0, "com.google.android.gms.games.ui.dialog.loginFailedDialog"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/a;->b(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    .line 183
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 371
    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Lcom/google/android/gms/games/ui/destination/b;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onBackPressed()V

    .line 374
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    if-eqz p1, :cond_0

    .line 116
    const-string v0, "savedStateRetryingSignIn"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->w:Z

    .line 119
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->O()V

    .line 125
    new-instance v0, Lcom/google/android/gms/games/ui/destination/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/matches/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->C:Lcom/google/android/gms/games/ui/destination/matches/a;

    .line 126
    new-instance v0, Lcom/google/android/gms/games/ui/destination/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/requests/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->D:Lcom/google/android/gms/games/ui/destination/requests/a;

    .line 127
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/common/a/b;-><init>(Lcom/google/android/gms/games/ui/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->E:Lcom/google/android/gms/games/ui/common/a/b;

    .line 131
    sget-object v0, Lcom/google/android/gms/games/ui/l;->e:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    const-string v0, "showConfidentialityWarning"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    const-string v0, "GOOGLE CONFIDENTIAL"

    const-string v3, " YOU ARE RUNNING UNRELEASED SOFTWARE. DO NOT DISCUSS OR SHOW EXTERNALLY."

    new-instance v4, Landroid/text/SpannableStringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const/high16 v5, -0x10000

    invoke-direct {v3, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v3, v2, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v3, 0x104000a

    new-instance v4, Lcom/google/android/gms/games/ui/destination/main/k;

    invoke-direct {v4, p0}, Lcom/google/android/gms/games/ui/destination/main/k;-><init>(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 141
    :cond_1
    if-nez p1, :cond_3

    move v0, v1

    .line 143
    :goto_0
    if-eqz v0, :cond_2

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Landroid/content/Intent;)V

    .line 145
    const-string v1, "shuffleHomePageSeed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-static {v3}, Lcom/android/a/a;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 151
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->d(Z)V

    .line 153
    if-eqz p1, :cond_4

    .line 154
    const-string v0, "savedStateShowUpgradeDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->H:Z

    .line 159
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 141
    goto :goto_0

    .line 156
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.SHOW_UPGRADE_DIALOG"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->H:Z

    goto :goto_1
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onNewIntent(Landroid/content/Intent;)V

    .line 220
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 221
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->d(Z)V

    .line 222
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 317
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 323
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 319
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/c/b;->b(Landroid/content/Context;)V

    .line 320
    const/4 v0, 0x1

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c028c
        :pswitch_0
    .end packed-switch
.end method

.method public final onPostResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onPostResume()V

    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->G:Z

    if-eqz v0, :cond_1

    .line 190
    const-string v0, "com.google.android.gms.games.ui.dialog.loginFailedDialog"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/a;->b(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    .line 193
    new-instance v0, Lcom/google/android/gms/games/ui/destination/main/l;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/main/l;-><init>()V

    .line 194
    const-string v1, "com.google.android.gms.games.ui.dialog.loginFailedDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 195
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->G:Z

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->H:Z

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Lcom/google/android/gms/games/ui/c/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/g;-><init>()V

    .line 198
    const-string v1, "com.google.android.gms.games.ui.dialog.upgradeDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 199
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->H:Z

    goto :goto_0
.end method

.method protected final onResume()V
    .locals 2

    .prologue
    .line 226
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onResume()V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/r;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/r;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/destination/r;->a(Lcom/google/android/gms/games/app/a;)V

    .line 230
    :cond_0
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 165
    const-string v0, "savedStateShowUpgradeDialog"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->H:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    const-string v0, "savedStateRetryingSignIn"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 167
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/bb;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->B:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/bb;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/bb;->a()Z

    move-result v0

    .line 331
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public final r_()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    return v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/c/b;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z_()Lcom/google/android/gms/games/ui/common/a/j;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->E:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method
