.class public Lcom/google/android/gms/drive/query/internal/MatchAllFilter;
.super Lcom/google/android/gms/drive/query/internal/AbstractFilter;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/query/internal/j;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/query/internal/j;

    invoke-direct {v0}, Lcom/google/android/gms/drive/query/internal/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;->CREATOR:Lcom/google/android/gms/drive/query/internal/j;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;-><init>(I)V

    .line 36
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/drive/query/internal/AbstractFilter;-><init>()V

    .line 31
    iput p1, p0, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;->a:I

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/query/internal/f;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-interface {p1}, Lcom/google/android/gms/drive/query/internal/f;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/query/internal/j;->a(Lcom/google/android/gms/drive/query/internal/MatchAllFilter;Landroid/os/Parcel;)V

    .line 59
    return-void
.end method
