.class public final Lcom/google/android/gms/games/internal/c;
.super Lcom/google/android/gms/common/internal/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;
.implements Lcom/google/android/gms/common/api/w;


# instance fields
.field g:Lcom/google/android/gms/games/internal/c/b;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/Map;

.field private k:Lcom/google/android/gms/games/PlayerEntity;

.field private l:Lcom/google/android/gms/games/GameEntity;

.field private final m:Lcom/google/android/gms/games/internal/bh;

.field private n:Z

.field private final o:Landroid/os/Binder;

.field private final p:J

.field private final q:Lcom/google/android/gms/games/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;[Ljava/lang/String;ILandroid/view/View;Lcom/google/android/gms/games/g;)V
    .locals 8

    .prologue
    .line 363
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;[Ljava/lang/String;)V

    .line 183
    new-instance v2, Lcom/google/android/gms/games/internal/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/internal/d;-><init>(Lcom/google/android/gms/games/internal/c;)V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->g:Lcom/google/android/gms/games/internal/c/b;

    .line 309
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    .line 364
    iput-object p3, p0, Lcom/google/android/gms/games/internal/c;->h:Ljava/lang/String;

    .line 365
    invoke-static {p4}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->i:Ljava/lang/String;

    .line 366
    new-instance v2, Landroid/os/Binder;

    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->o:Landroid/os/Binder;

    .line 367
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->j:Ljava/util/Map;

    .line 368
    move/from16 v0, p8

    invoke-static {p0, v0}, Lcom/google/android/gms/games/internal/bh;->a(Lcom/google/android/gms/games/internal/c;I)Lcom/google/android/gms/games/internal/bh;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->m:Lcom/google/android/gms/games/internal/bh;

    .line 369
    iget-object v2, p0, Lcom/google/android/gms/games/internal/c;->m:Lcom/google/android/gms/games/internal/bh;

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/internal/bh;->a(Landroid/view/View;)V

    .line 370
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    .line 371
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    .line 373
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/v;)V

    .line 374
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/w;)V

    .line 375
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 6206
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6211
    :goto_0
    return-object v0

    .line 6208
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6209
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 154
    invoke-static {p1}, Lcom/google/android/gms/games/internal/bf;->a(Landroid/os/IBinder;)Lcom/google/android/gms/games/internal/be;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    const-string v0, "com.google.android.gms.games.service.START"

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 7078
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    .line 7079
    const-string v0, "show_welcome_popup"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    .line 7081
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 7082
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->m:Lcom/google/android/gms/games/internal/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/bh;->a()V

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    .line 407
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 7038
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7040
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/be;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7045
    :cond_0
    :goto_0
    return-void

    .line 7042
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    .line 417
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/ag;)V
    .locals 4

    .prologue
    .line 3890
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/m;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/m;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 3891
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3895
    :goto_0
    return-void

    .line 3893
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6407
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/m;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/m;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 6408
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6412
    :goto_0
    return-void

    .line 6410
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;)V
    .locals 3

    .prologue
    .line 6788
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/k;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/k;-><init>(Lcom/google/android/gms/common/api/m;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/be;->t(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6793
    :goto_0
    return-void

    .line 6791
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;I)V
    .locals 2

    .prologue
    .line 4173
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/p;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4178
    :goto_0
    return-void

    .line 4176
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;III)V
    .locals 2

    .prologue
    .line 4980
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/at;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/at;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4985
    :goto_0
    return-void

    .line 4983
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;IIZZ)V
    .locals 6

    .prologue
    .line 5566
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/f;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/f;-><init>(Lcom/google/android/gms/common/api/m;)V

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;IIZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5572
    :goto_0
    return-void

    .line 5570
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;I[I)V
    .locals 2

    .prologue
    .line 4501
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ax;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ax;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;I[I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4507
    :goto_0
    return-void

    .line 4505
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/games/a/f;II)V
    .locals 3

    .prologue
    .line 3600
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/r;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/r;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-virtual {p2}, Lcom/google/android/gms/games/a/f;->e()Lcom/google/android/gms/games/a/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/games/a/g;->a:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Landroid/os/Bundle;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3605
    :goto_0
    return-void

    .line 3603
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5515
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/f;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/f;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5520
    :goto_0
    return-void

    .line 5518
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6043
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/aj;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aj;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6049
    :goto_0
    return-void

    .line 6047
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IIIZ)V
    .locals 7

    .prologue
    .line 3549
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/r;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/r;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3554
    :goto_0
    return-void

    .line 3552
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    .line 5497
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/f;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/f;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5503
    :goto_0
    return-void

    .line 5501
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I[I)V
    .locals 2

    .prologue
    .line 6377
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ax;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ax;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I[I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6383
    :goto_0
    return-void

    .line 6381
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6228
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/av;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/av;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6233
    :goto_0
    return-void

    .line 6231
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 3378
    const/4 v1, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 3384
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid player collection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3378
    :sswitch_0
    const-string v2, "circled"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "played_with"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v0, "nearby"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 3387
    :pswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ak;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ak;-><init>(Lcom/google/android/gms/common/api/m;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3392
    :goto_1
    return-void

    .line 3390
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3378
    :sswitch_data_0
    .sparse-switch
        -0x3e8dd581 -> :sswitch_2
        0x9529ab2 -> :sswitch_1
        0x2eaadd94 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 6902
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/at;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/at;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6907
    :goto_0
    return-void

    .line 6905
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 8

    .prologue
    .line 5779
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/r;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/r;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5785
    :goto_0
    return-void

    .line 5783
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 7

    .prologue
    .line 5987
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ak;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ak;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5992
    :goto_0
    return-void

    .line 5990
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 6126
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/e;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6132
    :goto_0
    return-void

    .line 6130
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;[IIZ)V
    .locals 7

    .prologue
    .line 5193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->g:Lcom/google/android/gms/games/internal/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c/b;->a()V

    .line 5194
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ap;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ap;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;[IIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5199
    :goto_0
    return-void

    .line 5197
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6871
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/au;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/au;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6877
    :goto_0
    return-void

    .line 6875
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 3253
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ak;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ak;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->f(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3258
    :goto_0
    return-void

    .line 3256
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 3472
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/s;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/s;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3477
    :goto_0
    return-void

    .line 3475
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;[IIZ)V
    .locals 2

    .prologue
    .line 5128
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->g:Lcom/google/android/gms/games/internal/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c/b;->a()V

    .line 5129
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ap;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ap;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;[IIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5134
    :goto_0
    return-void

    .line 5132
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/aa;Lcom/google/android/gms/common/internal/i;)V
    .locals 10

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    .line 530
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 531
    const-string v0, "com.google.android.gms.games.key.isHeadless"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget-boolean v1, v1, Lcom/google/android/gms/games/g;->a:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 532
    const-string v0, "com.google.android.gms.games.key.showConnectingPopup"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget-boolean v1, v1, Lcom/google/android/gms/games/g;->b:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 533
    const-string v0, "com.google.android.gms.games.key.connectingPopupGravity"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget v1, v1, Lcom/google/android/gms/games/g;->c:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 534
    const-string v0, "com.google.android.gms.games.key.retryingSignIn"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget-boolean v1, v1, Lcom/google/android/gms/games/g;->d:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 535
    const-string v0, "com.google.android.gms.games.key.sdkVariant"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget v1, v1, Lcom/google/android/gms/games/g;->e:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    const-string v0, "com.google.android.gms.games.key.forceResolveAccountKey"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget-object v1, v1, Lcom/google/android/gms/games/g;->f:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v0, "com.google.android.gms.games.key.proxyApis"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->q:Lcom/google/android/gms/games/g;

    iget-object v1, v1, Lcom/google/android/gms/games/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 539
    const v2, 0x647e90

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/internal/c;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/common/internal/e;->d:[Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/internal/c;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->m:Lcom/google/android/gms/games/internal/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/bh;->b()Landroid/os/IBinder;

    move-result-object v7

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v9}, Lcom/google/android/gms/common/internal/aa;->a(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 543
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6424
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/be;->a(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6428
    :goto_0
    return-void

    .line 6426
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6605
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/be;->c(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6609
    :goto_0
    return-void

    .line 6607
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6303
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/be;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6307
    :goto_0
    return-void

    .line 6305
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6267
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/internal/be;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6271
    :goto_0
    return-void

    .line 6269
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 3350
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/be;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3354
    :goto_0
    return-void

    .line 3352
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final varargs a([Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 442
    move v0, v1

    move v2, v1

    move v3, v1

    .line 444
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    .line 445
    aget-object v5, p1, v0

    .line 446
    const-string v6, "https://www.googleapis.com/auth/games"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    .line 444
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_1
    const-string v6, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    .line 449
    goto :goto_1

    .line 454
    :cond_2
    if-eqz v2, :cond_4

    .line 455
    if-nez v3, :cond_3

    move v0, v4

    :goto_2
    const-string v2, "Cannot have both %s and %s!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "https://www.googleapis.com/auth/games"

    aput-object v5, v3, v1

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 460
    :goto_3
    return-void

    :cond_3
    move v0, v1

    .line 455
    goto :goto_2

    .line 458
    :cond_4
    const-string v0, "Games APIs requires %s to function."

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/games"

    aput-object v4, v2, v1

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public final b()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 510
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->b()Landroid/os/Bundle;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_0

    .line 512
    const-class v1, Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    :cond_0
    :goto_0
    return-object v0

    .line 516
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 4795
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/be;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4799
    :goto_0
    return-void

    .line 4797
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/ag;)V
    .locals 4

    .prologue
    .line 3924
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/ag;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ag;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 3925
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3929
    :goto_0
    return-void

    .line 3927
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6452
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/ag;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ag;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 6453
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6457
    :goto_0
    return-void

    .line 6455
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5702
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/i;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/i;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->q(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5707
    :goto_0
    return-void

    .line 5705
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6065
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/aj;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aj;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6071
    :goto_0
    return-void

    .line 6069
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    .line 5630
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/f;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/f;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/be;->e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5636
    :goto_0
    return-void

    .line 5634
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6247
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/av;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/av;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6252
    :goto_0
    return-void

    .line 6250
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 5724
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/s;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/s;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5729
    :goto_0
    return-void

    .line 5727
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 3680
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/e;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3685
    :goto_0
    return-void

    .line 3683
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6469
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/be;->b(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6473
    :goto_0
    return-void

    .line 6471
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6825
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6829
    :goto_0
    return-void

    .line 6827
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6287
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/internal/be;->b(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6291
    :goto_0
    return-void

    .line 6289
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 6649
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/be;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6653
    :goto_0
    return-void

    .line 6651
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/PlayerEntity;

    .line 467
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->c()V

    .line 468
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/ag;)V
    .locals 4

    .prologue
    .line 3954
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/ao;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ao;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 3955
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/be;->d(Lcom/google/android/gms/games/internal/bb;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3959
    :goto_0
    return-void

    .line 3957
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6497
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/ao;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ao;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 6498
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/be;->d(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6502
    :goto_0
    return-void

    .line 6500
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6025
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ai;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ai;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->s(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6031
    :goto_0
    return-void

    .line 6029
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 6327
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/p;-><init>(Lcom/google/android/gms/common/api/m;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, p2, p3, v2}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6333
    :goto_0
    return-void

    .line 6331
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    .line 5682
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/f;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/f;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/be;->c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5688
    :goto_0
    return-void

    .line 5686
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 6697
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/h;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/be;->a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6702
    :goto_0
    return-void

    .line 6700
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 6086
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/al;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/al;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->g(Lcom/google/android/gms/games/internal/bb;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6091
    :goto_0
    return-void

    .line 6089
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6513
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/be;->d(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6517
    :goto_0
    return-void

    .line 6515
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 473
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->n:Z

    .line 476
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    .line 479
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->c()V

    .line 480
    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->g:Lcom/google/android/gms/games/internal/c/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/c/b;->a()V

    .line 481
    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/be;->a(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 483
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "Failed to notify client disconnect."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 489
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->d()V

    .line 490
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/ag;)V
    .locals 4

    .prologue
    .line 3986
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/aq;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aq;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 3987
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/be;->c(Lcom/google/android/gms/games/internal/bb;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3991
    :goto_0
    return-void

    .line 3989
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6541
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/aq;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aq;-><init>(Lcom/google/android/gms/common/api/ag;)V

    .line 6542
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/be;->c(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6546
    :goto_0
    return-void

    .line 6544
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    .line 5961
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/ak;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ak;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/be;->b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5966
    :goto_0
    return-void

    .line 5964
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 6103
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    new-instance v1, Lcom/google/android/gms/games/internal/am;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/am;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/be;->h(Lcom/google/android/gms/games/internal/bb;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6108
    :goto_0
    return-void

    .line 6106
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6558
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/be;->c(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6562
    :goto_0
    return-void

    .line 6560
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 6806
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/be;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6809
    :goto_0
    return-object v0

    .line 6808
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6809
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3141
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->d()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3144
    :goto_0
    return-object v0

    .line 3143
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3144
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3153
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->e()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3156
    :goto_0
    return-object v0

    .line 3155
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3156
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/android/gms/games/Player;
    .locals 2

    .prologue
    .line 3164
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->h()V

    .line 3165
    monitor-enter p0

    .line 3166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 3169
    :try_start_1
    new-instance v1, Lcom/google/android/gms/games/o;

    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->f()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/o;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3171
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 3172
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/o;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3175
    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3181
    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3184
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0

    .line 3175
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3178
    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 3181
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()Lcom/google/android/gms/games/Game;
    .locals 2

    .prologue
    .line 3192
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->h()V

    .line 3193
    monitor-enter p0

    .line 3194
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/GameEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 3197
    :try_start_1
    new-instance v1, Lcom/google/android/gms/games/a;

    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->h()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3199
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 3200
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/GameEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3203
    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->f_()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3209
    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3212
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/GameEntity;

    return-object v0

    .line 3203
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->f_()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3206
    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 3209
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 3903
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/be;->b(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3907
    :goto_0
    return-void

    .line 3905
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 3937
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/be;->c(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3941
    :goto_0
    return-void

    .line 3939
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final p()V
    .locals 4

    .prologue
    .line 3966
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/be;->e(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3970
    :goto_0
    return-void

    .line 3968
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final q()V
    .locals 4

    .prologue
    .line 3999
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->p:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/be;->d(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4003
    :goto_0
    return-void

    .line 4001
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4156
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4159
    :goto_0
    return-object v0

    .line 4158
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4159
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 6617
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6621
    :goto_0
    return-void

    .line 6619
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final t()Z
    .locals 3

    .prologue
    .line 6631
    const/4 v1, 0x1

    .line 6633
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6637
    :goto_0
    return v0

    .line 6635
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 7053
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7055
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/be;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/be;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7060
    :cond_0
    :goto_0
    return-void

    .line 7057
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
