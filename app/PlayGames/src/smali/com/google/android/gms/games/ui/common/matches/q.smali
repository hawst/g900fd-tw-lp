.class public final Lcom/google/android/gms/games/ui/common/matches/q;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/ar;
.implements Lcom/google/android/gms/games/ui/common/matches/o;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/matches/n;

.field private an:Lcom/google/android/gms/games/ui/common/matches/o;

.field private ao:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method

.method private as()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 252
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 253
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    new-array v2, v3, [I

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    aput v3, v2, v5

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0

    .line 257
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    new-array v3, v3, [I

    iget v4, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    aput v4, v3, v5

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public static b(I)Lcom/google/android/gms/games/ui/common/matches/q;
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Match type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/q;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/q;-><init>()V

    .line 53
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string v2, "match_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/q;->g(Landroid/os/Bundle;)V

    .line 56
    return-object v0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/gms/games/multiplayer/turnbased/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/g;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/g;->c()Lcom/google/android/gms/games/multiplayer/turnbased/a;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->r()V

    iget v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Match type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is invalid"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a()V

    throw v0

    :pswitch_0
    :try_start_2
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/q;->am:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/q;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a()I

    move-result v0

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v0, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->v()V

    goto :goto_0

    :pswitch_1
    :try_start_3
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    goto :goto_1

    :pswitch_2
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->am:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    new-array v1, v5, [I

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    aput v2, v1, v4

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 108
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v0, p1, v6}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;I)V

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->aj()V

    .line 123
    return-void

    .line 111
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    new-array v2, v5, [I

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    aput v3, v2, v4

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 117
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v1, p1, v0, v6}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 178
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method protected final al()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->as()V

    .line 242
    return-void
.end method

.method public final am()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;)V

    .line 237
    :goto_0
    return-void

    .line 234
    :cond_0
    const-string v0, "onEmptyActionTextClicked - Trying to show popular multiplayer when not in destination app"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->am:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->h:Lcom/google/android/gms/games/ui/e/o;

    if-lez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 184
    return-void

    .line 183
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->c()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->C_()V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->P()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const-string v0, "MatchFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/Game;)V

    .line 201
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 196
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/v;->a()Lcom/google/android/gms/games/ui/common/matches/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->an:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 69
    const-string v1, "match_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Must specify a match type!"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 70
    const-string v1, "match_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->ao:I

    .line 72
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    sget v0, Lcom/google/android/gms/l;->ad:I

    .line 78
    :cond_0
    sget v1, Lcom/google/android/gms/f;->x:I

    sget v2, Lcom/google/android/gms/l;->ae:I

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/gms/games/ui/common/matches/q;->a(III)V

    .line 81
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/q;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->am:Lcom/google/android/gms/games/ui/common/matches/n;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/q;->am:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/q;->a(Landroid/support/v7/widget/bv;)V

    .line 84
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->as()V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/q;->as()V

    .line 227
    return-void
.end method
