.class final Lcom/google/android/gms/common/api/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/t;


# instance fields
.field final a:Ljava/util/concurrent/locks/Lock;

.field final b:Landroid/content/Context;

.field final c:Ljava/util/Queue;

.field d:Lcom/google/android/gms/common/a;

.field e:I

.field volatile f:I

.field volatile g:Z

.field h:J

.field i:J

.field final j:Landroid/os/Handler;

.field k:Landroid/content/BroadcastReceiver;

.field final l:Landroid/os/Bundle;

.field m:Z

.field final n:Ljava/util/Set;

.field private final o:Ljava/util/concurrent/locks/Condition;

.field private final p:Lcom/google/android/gms/common/internal/m;

.field private final q:I

.field private final r:Landroid/os/Looper;

.field private s:Z

.field private t:I

.field private final u:Ljava/util/Map;

.field private final v:Ljava/util/List;

.field private final w:Ljava/util/Set;

.field private final x:Lcom/google/android/gms/common/api/ad;

.field private final y:Lcom/google/android/gms/common/api/v;

.field private final z:Lcom/google/android/gms/common/internal/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V
    .locals 12

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    .line 179
    iget-object v2, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->o:Ljava/util/concurrent/locks/Condition;

    .line 194
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    .line 204
    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/gms/common/api/x;->f:I

    .line 210
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/x;->s:Z

    .line 216
    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Lcom/google/android/gms/common/api/x;->h:J

    .line 219
    const-wide/16 v2, 0x1388

    iput-wide v2, p0, Lcom/google/android/gms/common/api/x;->i:J

    .line 230
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->l:Landroid/os/Bundle;

    .line 233
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    .line 244
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->w:Ljava/util/Set;

    .line 251
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->n:Ljava/util/Set;

    .line 256
    new-instance v2, Lcom/google/android/gms/common/api/y;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/y;-><init>(Lcom/google/android/gms/common/api/x;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->x:Lcom/google/android/gms/common/api/ad;

    .line 264
    new-instance v2, Lcom/google/android/gms/common/api/z;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/z;-><init>(Lcom/google/android/gms/common/api/x;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->y:Lcom/google/android/gms/common/api/v;

    .line 325
    new-instance v2, Lcom/google/android/gms/common/api/aa;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/aa;-><init>(Lcom/google/android/gms/common/api/x;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->z:Lcom/google/android/gms/common/internal/o;

    .line 400
    iput-object p1, p0, Lcom/google/android/gms/common/api/x;->b:Landroid/content/Context;

    .line 401
    new-instance v2, Lcom/google/android/gms/common/internal/m;

    iget-object v3, p0, Lcom/google/android/gms/common/api/x;->z:Lcom/google/android/gms/common/internal/o;

    invoke-direct {v2, p2, v3}, Lcom/google/android/gms/common/internal/m;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/o;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    .line 402
    iput-object p2, p0, Lcom/google/android/gms/common/api/x;->r:Landroid/os/Looper;

    .line 403
    new-instance v2, Lcom/google/android/gms/common/api/ae;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/common/api/ae;-><init>(Lcom/google/android/gms/common/api/x;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    .line 404
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/gms/common/api/x;->q:I

    .line 406
    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/v;

    .line 407
    iget-object v4, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0

    .line 410
    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/w;

    .line 411
    iget-object v4, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/g;)V

    goto :goto_1

    .line 414
    :cond_1
    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/gms/common/api/a;

    .line 416
    iget-object v2, v3, Lcom/google/android/gms/common/api/a;->a:Lcom/google/android/gms/common/api/g;

    .line 417
    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 420
    iget-object v10, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    iget-object v11, v3, Lcom/google/android/gms/common/api/a;->b:Lcom/google/android/gms/common/api/h;

    iget-object v7, p0, Lcom/google/android/gms/common/api/x;->y:Lcom/google/android/gms/common/api/v;

    new-instance v8, Lcom/google/android/gms/common/api/ab;

    invoke-direct {v8, p0, v2}, Lcom/google/android/gms/common/api/ab;-><init>(Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/g;)V

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v2 .. v8}, Lcom/google/android/gms/common/api/g;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;)Lcom/google/android/gms/common/api/f;

    move-result-object v2

    invoke-interface {v10, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 443
    :cond_2
    iget-object v2, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/x;->v:Ljava/util/List;

    .line 444
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/af;)V
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 544
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/af;->e()Lcom/google/android/gms/common/api/h;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->x:Lcom/google/android/gms/common/api/ad;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/af;->a(Lcom/google/android/gms/common/api/ad;)V

    .line 554
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z

    if-eqz v0, :cond_1

    .line 555
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/af;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 563
    :goto_1
    return-void

    .line 544
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 559
    :cond_1
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/common/api/af;->e()Lcom/google/android/gms/common/api/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    .line 560
    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/af;->a(Lcom/google/android/gms/common/api/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/x;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget v0, p0, Lcom/google/android/gms/common/api/x;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/x;->t:I

    iget v0, p0, Lcom/google/android/gms/common/api/x;->t:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/x;->s:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/x;->a(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a;->c()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/h;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->f()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/a;)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/common/api/x;->m:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/api/x;->f:I

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->f()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->o:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/x;->g()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->s:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/x;->s:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/x;->a(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->l:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/m;->a(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->l:Landroid/os/Bundle;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/x;I)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/x;->a(I)V

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 570
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 574
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 576
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/af;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/af;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 577
    :catch_0
    move-exception v0

    .line 578
    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 582
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 570
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 582
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 583
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 947
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->r:Landroid/os/Looper;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;
    .locals 2

    .prologue
    .line 588
    const-string v0, "Listener must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 591
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/ag;

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->r:Landroid/os/Looper;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/common/api/ag;-><init>(Landroid/os/Looper;Ljava/lang/Object;)V

    .line 592
    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->w:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/f;

    .line 603
    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 510
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/x;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    .line 515
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 519
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, -0x1

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 715
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/api/x;->f:I

    if-eq v0, v1, :cond_a

    .line 717
    if-ne p1, v4, :cond_4

    .line 718
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 720
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/af;

    .line 722
    invoke-interface {v0}, Lcom/google/android/gms/common/api/af;->b()V

    .line 724
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 787
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 729
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 733
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/af;

    .line 735
    invoke-interface {v0}, Lcom/google/android/gms/common/api/af;->b()V

    goto :goto_1

    .line 737
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ag;

    .line 741
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/common/api/ag;->a:Ljava/lang/Object;

    goto :goto_2

    .line 743
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 745
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 747
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->s:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 787
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 788
    :goto_3
    return-void

    .line 752
    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->e()Z

    move-result v0

    .line 753
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v1

    .line 754
    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/gms/common/api/x;->f:I

    .line 756
    if-eqz v0, :cond_6

    .line 759
    if-ne p1, v4, :cond_5

    .line 760
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    .line 764
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->o:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 768
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->m:Z

    .line 769
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/f;

    .line 770
    invoke-interface {v0}, Lcom/google/android/gms/common/api/f;->e()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 771
    invoke-interface {v0}, Lcom/google/android/gms/common/api/f;->d()V

    goto :goto_4

    .line 774
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->m:Z

    .line 777
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/common/api/x;->f:I

    .line 778
    if-eqz v1, :cond_a

    .line 780
    if-eq p1, v4, :cond_9

    .line 781
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/m;->a(I)V

    .line 783
    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->m:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 787
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 918
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/w;)V
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/m;->a(Lcom/google/android/gms/common/g;)V

    .line 933
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 526
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 528
    invoke-direct {p0}, Lcom/google/android/gms/common/api/x;->g()V

    .line 530
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/af;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :goto_1
    return-object p1

    .line 526
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 532
    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/api/x;->a(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 621
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->s:Z

    .line 622
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 638
    :goto_0
    return-void

    .line 627
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->m:Z

    .line 628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    .line 629
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/api/x;->f:I

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->l:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 631
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/x;->t:I

    .line 633
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/f;

    .line 634
    invoke-interface {v0}, Lcom/google/android/gms/common/api/f;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 637
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)V
    .locals 4

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lcom/google/android/gms/common/internal/m;->b:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/common/internal/m;->b:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/common/internal/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionCallbacks(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-boolean v2, v0, Lcom/google/android/gms/common/internal/m;->d:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/common/internal/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/common/api/w;)V
    .locals 4

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->p:Lcom/google/android/gms/common/internal/m;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lcom/google/android/gms/common/internal/m;->e:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/common/internal/m;->e:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/common/internal/m;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/x;->f()V

    .line 708
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/x;->a(I)V

    .line 709
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 865
    iget v0, p0, Lcom/google/android/gms/common/api/x;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 870
    iget v1, p0, Lcom/google/android/gms/common/api/x;->f:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final f()V
    .locals 2

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 903
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 912
    :goto_0
    return-void

    .line 906
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/x;->g:Z

    .line 907
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 908
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 909
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
