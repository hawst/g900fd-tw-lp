.class public final Lcom/google/android/gms/games/ui/c/d;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;-><init>()V

    return-void
.end method

.method public static P()Lcom/google/android/gms/games/ui/c/d;
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/games/ui/c/d;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/d;-><init>()V

    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 27
    const-string v2, "messageResId"

    const v3, 0x7f0f00f6

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/d;->g(Landroid/os/Bundle;)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    sget v1, Lcom/google/android/gms/l;->aT:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    .line 37
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    const-string v3, "messageResId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 39
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 40
    const v0, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 41
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
