.class public Lcom/google/android/gms/games/internal/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/gms/games/internal/c;

.field protected b:Lcom/google/android/gms/games/internal/bi;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/internal/c;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/games/internal/bh;->a:Lcom/google/android/gms/games/internal/c;

    .line 42
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/internal/bh;->a(I)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/internal/c;IB)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/games/internal/c;I)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/internal/c;I)Lcom/google/android/gms/games/internal/bh;
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Lcom/google/android/gms/games/internal/bj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/internal/bj;-><init>(Lcom/google/android/gms/games/internal/c;I)V

    .line 36
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/games/internal/c;I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bh;->a:Lcom/google/android/gms/games/internal/c;

    iget-object v1, p0, Lcom/google/android/gms/games/internal/bh;->b:Lcom/google/android/gms/games/internal/bi;

    iget-object v1, v1, Lcom/google/android/gms/games/internal/bi;->a:Landroid/os/IBinder;

    iget-object v2, p0, Lcom/google/android/gms/games/internal/bh;->b:Lcom/google/android/gms/games/internal/bi;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "popupLocationInfo.gravity"

    iget v5, v2, Lcom/google/android/gms/games/internal/bi;->b:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.displayId"

    iget v5, v2, Lcom/google/android/gms/games/internal/bi;->c:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.left"

    iget v5, v2, Lcom/google/android/gms/games/internal/bi;->d:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.top"

    iget v5, v2, Lcom/google/android/gms/games/internal/bi;->e:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.right"

    iget v5, v2, Lcom/google/android/gms/games/internal/bi;->f:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.bottom"

    iget v2, v2, Lcom/google/android/gms/games/internal/bi;->g:I

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/games/internal/c;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/games/internal/bi;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/games/internal/bi;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bh;->b:Lcom/google/android/gms/games/internal/bi;

    .line 50
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public final b()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bh;->b:Lcom/google/android/gms/games/internal/bi;

    iget-object v0, v0, Lcom/google/android/gms/games/internal/bi;->a:Landroid/os/IBinder;

    return-object v0
.end method
