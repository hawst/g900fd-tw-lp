.class final Lcom/google/android/gms/games/ui/destination/players/ap;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/view/View;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/ProgressBar;

.field private final r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final s:Landroid/view/View;

.field private final t:Landroid/widget/TextView;

.field private final u:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 82
    const v0, 0x7f0c0125

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->m:Landroid/view/View;

    .line 84
    const v0, 0x7f0c0188

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->n:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0c01e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->o:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0c01ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->p:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0c01f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->q:Landroid/widget/ProgressBar;

    .line 90
    const v0, 0x7f0c0171

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00b1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00b2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00af

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->c(Landroid/view/View;)V

    .line 99
    const v0, 0x7f0c01f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->s:Landroid/view/View;

    .line 100
    const v0, 0x7f0c01f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->t:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0c01f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->u:Landroid/widget/TextView;

    .line 103
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 108
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/an;

    .line 111
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 112
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v5

    .line 114
    if-eqz v5, :cond_3

    move v2, v1

    .line 115
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    .line 116
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->m:Landroid/view/View;

    invoke-static {v4, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/res/Resources;I)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->o:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    .line 123
    if-eqz v2, :cond_1

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->q:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->p:Landroid/widget/TextView;

    invoke-static {v5, v4, v0, v1}, Lcom/google/android/gms/games/ui/common/players/j;->a(Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Landroid/widget/TextView;)V

    .line 128
    :cond_1
    if-eqz v2, :cond_2

    invoke-static {v5}, Lcom/google/android/gms/games/ui/common/players/j;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->u:Landroid/widget/TextView;

    invoke-static {v4, v5, v0, v1}, Lcom/google/android/gms/games/ui/common/players/j;->a(Landroid/content/res/Resources;Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 133
    :cond_2
    return-void

    :cond_3
    move v2, v3

    .line 114
    goto :goto_0

    .line 120
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/an;

    .line 138
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->b(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/ui/destination/players/ao;

    move-result-object v1

    if-nez v1, :cond_0

    .line 149
    :goto_0
    return-void

    .line 142
    :cond_0
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    const-string v3, "avatar"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 145
    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/ap;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b()Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "level"

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 147
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->b(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/ui/destination/players/ao;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/util/Pair;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    invoke-interface {v3, v0, v4}, Lcom/google/android/gms/games/ui/destination/players/ao;->c(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method
