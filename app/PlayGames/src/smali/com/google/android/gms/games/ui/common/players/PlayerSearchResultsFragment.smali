.class public final Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/e/af;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/players/a;

.field private an:Landroid/view/View;

.field private ao:Z

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Z

.field private as:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    sget v0, Lcom/google/android/gms/i;->h:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 43
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ao:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ar:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->as:Z

    .line 67
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 73
    sget v1, Lcom/google/android/gms/g;->L:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->an:Landroid/view/View;

    .line 76
    sget v1, Lcom/google/android/gms/f;->s:I

    sget v2, Lcom/google/android/gms/l;->bz:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(III)V

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->an:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 32
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ar:Z

    if-eqz v0, :cond_1

    const-string v0, "PlayerSearchResFrag"

    const-string v1, "onPlayersLoaded: discarding stray result"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->f_()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->k()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->as:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->as:Z

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Landroid/view/View;

    const v3, 0x102000a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ao:Z

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/players/a;->a(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ap:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "PlayerSearchResFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGoogleApiClientConnected: running pending query \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ap:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ap:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ap:Ljava/lang/String;

    .line 130
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 144
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ao:Z

    if-nez v0, :cond_1

    .line 145
    const-string v0, "PlayerSearchResFrag"

    const-string v1, "doSearch: not connected yet! Stashing away mPendingQuery..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ap:Ljava/lang/String;

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->b()V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->an:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ar:Z

    goto :goto_0

    .line 172
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aq:Ljava/lang/String;

    .line 173
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aq:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->an:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 183
    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ar:Z

    .line 187
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->as:Z

    goto :goto_0
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 253
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    const-string v0, "PlayerSearchResFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aq:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 93
    new-instance v7, Lcom/google/android/gms/games/ui/ac;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v7, v0}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    .line 94
    sget v0, Lcom/google/android/gms/l;->by:I

    invoke-virtual {v7, v0}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v3

    .line 98
    if-eqz v3, :cond_0

    sget v4, Lcom/google/android/gms/j;->f:I

    .line 101
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v2, Lcom/google/android/gms/games/ui/common/players/b;

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;ZIII)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 108
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 109
    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    .line 112
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 113
    return-void

    :cond_0
    move v4, v5

    .line 98
    goto :goto_0
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Ljava/lang/String;)V

    .line 271
    return-void
.end method
