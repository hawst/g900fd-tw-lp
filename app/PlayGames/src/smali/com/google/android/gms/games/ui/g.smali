.class public abstract Lcom/google/android/gms/games/ui/g;
.super Lcom/google/android/gms/games/ui/y;
.source "SourceFile"


# instance fields
.field private m:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 691
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/y;-><init>(Landroid/view/View;)V

    .line 692
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 695
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/y;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 696
    iput-object p3, p0, Lcom/google/android/gms/games/ui/g;->m:Ljava/lang/Object;

    .line 697
    return-void
.end method

.method protected final o()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/gms/games/ui/g;->m:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/gms/common/data/n;

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/google/android/gms/games/ui/g;->m:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/n;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/n;->g_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/g;->m:Ljava/lang/Object;

    .line 704
    :goto_0
    return-object v0

    .line 701
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/g;->m:Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final p()Lcom/google/android/gms/common/data/b;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/gms/games/ui/g;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v0

    return-object v0
.end method
