.class public abstract Lcom/google/android/gms/games/ui/m;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;
.implements Lcom/google/android/gms/common/api/w;
.implements Lcom/google/android/gms/games/ui/ax;


# instance fields
.field protected a:Lcom/google/android/gms/games/ui/n;

.field protected b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected final c:I

.field private d:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/m;->d:Z

    .line 62
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 63
    iput p1, p0, Lcom/google/android/gms/games/ui/m;->c:I

    .line 64
    return-void
.end method


# virtual methods
.method public final A_()V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_0

    .line 359
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/gms/d;->l:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/d;->n:I

    aput v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/gms/d;->o:I

    aput v3, v1, v2

    const/4 v2, 0x3

    sget v3, Lcom/google/android/gms/d;->m:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a([I)V

    .line 365
    :cond_0
    return-void
.end method

.method public final P()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->n()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final Q()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->K:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public S()Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public T()F
    .locals 1

    .prologue
    .line 243
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method public U()Z
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x1

    return v0
.end method

.method public V()I
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public W()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x2

    return v0
.end method

.method public final X()I
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/m;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Y()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    return-object v0
.end method

.method public Z()I
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/m;->W()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 227
    iget v0, p0, Lcom/google/android/gms/games/ui/m;->c:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/n;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/m;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lcom/google/android/gms/games/ui/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/au;-><init>(Lcom/google/android/gms/games/ui/ax;)V

    .line 79
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/au;->a(Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/au;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->A()V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/m;->c:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 125
    const-string v0, "GamesFragment"

    const-string v1, "Unexpected call to onConnectionSuspended - subclasses should unregister as a listener in onStop() and clear data in onDestroyView()"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/m;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 120
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/m;->a(Lcom/google/android/gms/common/api/t;)V

    .line 121
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 201
    new-instance v1, Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 202
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/support/v4/app/Fragment;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 203
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 204
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 205
    const/4 v2, -0x1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->B()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 208
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/t;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public final a_(Z)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 347
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/m;->d:Z

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(IZ)V

    .line 350
    :cond_0
    return-void
.end method

.method public aa()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/ax;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public final ab()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Z)V

    .line 328
    return-void
.end method

.method public ac()V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public ad()Z
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    return v0
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/m;->d:Z

    return v0
.end method

.method public final ag()F
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v0

    .line 372
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()Lcom/google/android/gms/common/api/t;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 150
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 151
    return-object v0
.end method

.method public b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/n;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    .line 70
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    .line 96
    return-void
.end method

.method public p_()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->p_()V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->a(Lcom/google/android/gms/common/api/v;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->a(Lcom/google/android/gms/common/api/w;)V

    .line 103
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->b(Lcom/google/android/gms/common/api/v;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/m;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->b(Lcom/google/android/gms/common/api/w;)V

    .line 109
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->q_()V

    .line 110
    return-void
.end method
