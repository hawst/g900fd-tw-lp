.class public final Lcom/google/android/gms/games/app/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:Lcom/google/android/gms/playlog/b;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/games/ui/l;->h:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/app/a;->a:Z

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/app/a;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Lcom/google/android/gms/playlog/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/playlog/b;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;)Lcom/google/android/gms/playlog/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    .line 118
    iput-object p1, p0, Lcom/google/android/gms/games/app/a;->d:Landroid/content/Context;

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/app/a;->e:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/app/a;->f:Ljava/util/ArrayList;

    .line 121
    return-void
.end method

.method private a(Z)Lcom/google/android/gms/games/c/af;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->e:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/app/a;->a(ZLjava/lang/String;)Lcom/google/android/gms/games/c/af;

    move-result-object v0

    return-object v0
.end method

.method private a(ZLjava/lang/String;)Lcom/google/android/gms/games/c/af;
    .locals 4

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/gms/games/c/af;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/af;-><init>()V

    .line 318
    iput-boolean p1, v0, Lcom/google/android/gms/games/c/af;->a:Z

    .line 319
    iput-object p2, v0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    .line 324
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/app/a;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 325
    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 326
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/google/android/gms/games/c/af;->f:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :goto_0
    return-object v0

    .line 328
    :catch_0
    move-exception v1

    const-string v1, "PowerUpPlayLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find package info for package: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/c/r;)V
    .locals 2

    .prologue
    .line 298
    sget-object v1, Lcom/google/android/gms/games/app/a;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v0, v0, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    :goto_0
    monitor-exit v1

    return-void

    .line 302
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/app/a;->b(Lcom/google/android/gms/games/c/r;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Ljava/lang/String;)Lcom/google/android/gms/games/c/k;
    .locals 1

    .prologue
    .line 347
    new-instance v0, Lcom/google/android/gms/games/c/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/k;-><init>()V

    .line 348
    iput-object p0, v0, Lcom/google/android/gms/games/c/k;->a:Ljava/lang/String;

    .line 349
    return-object v0
.end method

.method private b(Lcom/google/android/gms/games/c/r;)V
    .locals 4

    .prologue
    .line 308
    sget-boolean v0, Lcom/google/android/gms/games/app/a;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "PowerUpPlayLogger"

    invoke-virtual {p1}, Lcom/google/android/gms/games/c/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/gms/games/c/r;->a(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method private static c(I)Lcom/google/android/gms/games/c/j;
    .locals 1

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/gms/games/c/j;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/j;-><init>()V

    .line 335
    iput p0, v0, Lcom/google/android/gms/games/c/j;->a:I

    .line 336
    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 289
    sget-object v1, Lcom/google/android/gms/games/app/a;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 291
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->j()V

    .line 132
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 150
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/app/a;->a(Z)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 151
    invoke-static {p1}, Lcom/google/android/gms/games/app/a;->c(I)Lcom/google/android/gms/games/c/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    .line 152
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 153
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 233
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 234
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/app/a;->a(Z)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 235
    invoke-static {v3}, Lcom/google/android/gms/games/app/a;->c(I)Lcom/google/android/gms/games/c/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    .line 236
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/games/app/a;->b(Ljava/lang/String;)Lcom/google/android/gms/games/c/k;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    .line 237
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    iget-object v1, v1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    iput-boolean v3, v1, Lcom/google/android/gms/games/c/k;->c:Z

    .line 238
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 239
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;I)V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;IZ)V
    .locals 3

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 204
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/app/a;->a(Z)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 205
    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/gms/games/app/a;->c(I)Lcom/google/android/gms/games/c/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    .line 206
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/games/app/a;->b(Ljava/lang/String;)Lcom/google/android/gms/games/c/k;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    .line 207
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    iget-object v1, v1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    iput p2, v1, Lcom/google/android/gms/games/c/k;->b:I

    .line 208
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    iget-object v1, v1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    iput-boolean p3, v1, Lcom/google/android/gms/games/c/k;->c:Z

    .line 209
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 210
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 265
    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    .line 267
    new-instance v1, Lcom/google/android/gms/games/c/r;

    invoke-direct {v1}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 268
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/games/app/a;->a(ZLjava/lang/String;)Lcom/google/android/gms/games/c/af;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 269
    iget-object v0, v1, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    .line 271
    new-instance v0, Lcom/google/android/gms/games/c/ap;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ap;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    .line 272
    iget-object v0, v1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    const/4 v2, 0x4

    iput v2, v0, Lcom/google/android/gms/games/c/ap;->a:I

    .line 273
    iget-object v0, v1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    iput-object p2, v0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    .line 274
    iget-object v0, v1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/gms/games/c/ap;->c:I

    .line 275
    iget-object v0, v1, Lcom/google/android/gms/games/c/r;->k:Lcom/google/android/gms/games/c/ap;

    const-string v2, ""

    iput-object v2, v0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    .line 276
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 277
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 124
    sget-object v2, Lcom/google/android/gms/games/app/a;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;)Lcom/google/android/gms/playlog/b;

    .line 126
    sget-object v3, Lcom/google/android/gms/games/app/a;->b:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/app/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->b(Lcom/google/android/gms/games/c/r;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/app/a;->c()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    :try_start_2
    monitor-exit v2

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(ZJ)V
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/app/a;->a(Z)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 166
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    iput-wide p2, v1, Lcom/google/android/gms/games/c/af;->c:J

    .line 167
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/games/app/a;->c(I)Lcom/google/android/gms/games/c/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    .line 168
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 169
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/games/app/a;->c()V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/app/a;->c:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->k()V

    .line 141
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 178
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/app/a;->a(Z)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 179
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/games/app/a;->c(I)Lcom/google/android/gms/games/c/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    .line 180
    iget-object v1, v0, Lcom/google/android/gms/games/c/r;->d:Lcom/google/android/gms/games/c/j;

    new-instance v2, Lcom/google/android/gms/games/c/m;

    invoke-direct {v2}, Lcom/google/android/gms/games/c/m;-><init>()V

    iput p1, v2, Lcom/google/android/gms/games/c/m;->a:I

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/games/c/m;->b:Z

    iput-object v2, v1, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    .line 181
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/c/r;)V

    .line 182
    return-void
.end method
