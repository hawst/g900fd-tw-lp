.class final Lcom/google/android/gms/games/ui/destination/games/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/games/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/t;)V
    .locals 0

    .prologue
    .line 1587
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/u;->a:Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 1587
    check-cast p1, Lcom/google/android/gms/games/j;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/u;->a:Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->isFinishing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->o()Z

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/internal/game/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    iput-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v4, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v4}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/Game;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->al()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onDataBufferLoaded: couldn\'t load gameId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/s;

    move-result-object v1

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/s;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    throw v0

    :cond_3
    const/4 v0, 0x6

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v2}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/Game;

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, v1, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->p()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Z)Z

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/t;->a()V

    goto/16 :goto_0
.end method
