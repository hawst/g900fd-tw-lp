.class public final Lcom/google/android/gms/people/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/h;

.field static final b:Lcom/google/android/gms/common/api/g;

.field public static final c:Lcom/google/android/gms/common/api/a;

.field public static final d:Lcom/google/android/gms/people/identity/a;

.field public static final e:Lcom/google/android/gms/people/c;

.field public static final f:Lcom/google/android/gms/people/j;

.field public static final g:Lcom/google/android/gms/people/k;

.field public static final h:Lcom/google/android/gms/people/y;

.field public static final i:Lcom/google/android/gms/people/a;

.field public static final j:Lcom/google/android/gms/people/m;

.field public static final k:Lcom/google/android/gms/people/n;

.field public static final l:Lcom/google/android/gms/people/b;

.field public static final m:Lcom/google/android/gms/people/o;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->a:Lcom/google/android/gms/common/api/h;

    .line 116
    new-instance v0, Lcom/google/android/gms/people/q;

    invoke-direct {v0}, Lcom/google/android/gms/people/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->b:Lcom/google/android/gms/common/api/g;

    .line 141
    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/people/p;->b:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/people/p;->a:Lcom/google/android/gms/common/api/h;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/people/p;->c:Lcom/google/android/gms/common/api/a;

    .line 149
    new-instance v0, Lcom/google/android/gms/people/identity/internal/g;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->d:Lcom/google/android/gms/people/identity/a;

    .line 157
    new-instance v0, Lcom/google/android/gms/people/internal/b/c;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    .line 165
    new-instance v0, Lcom/google/android/gms/people/internal/b/j;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->f:Lcom/google/android/gms/people/j;

    .line 171
    new-instance v0, Lcom/google/android/gms/people/internal/b/k;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->g:Lcom/google/android/gms/people/k;

    .line 177
    new-instance v0, Lcom/google/android/gms/people/internal/b/s;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->h:Lcom/google/android/gms/people/y;

    .line 183
    new-instance v0, Lcom/google/android/gms/people/internal/b/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->i:Lcom/google/android/gms/people/a;

    .line 189
    new-instance v0, Lcom/google/android/gms/people/internal/b/p;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->j:Lcom/google/android/gms/people/m;

    .line 194
    new-instance v0, Lcom/google/android/gms/people/internal/b/q;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->k:Lcom/google/android/gms/people/n;

    .line 202
    new-instance v0, Lcom/google/android/gms/people/internal/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->l:Lcom/google/android/gms/people/b;

    .line 208
    new-instance v0, Lcom/google/android/gms/people/internal/b/r;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/p;->m:Lcom/google/android/gms/people/o;

    return-void
.end method
