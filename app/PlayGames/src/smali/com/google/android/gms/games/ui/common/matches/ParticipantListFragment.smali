.class public Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;
.super Lcom/google/android/gms/games/ui/m;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final d:I

.field private static final e:I


# instance fields
.field private aj:Lcom/google/android/gms/games/multiplayer/Participant;

.field private ak:Lcom/google/android/gms/games/ui/common/matches/x;

.field private al:Ljava/util/HashMap;

.field private am:Landroid/widget/ListView;

.field private an:Lcom/google/android/gms/games/ui/common/matches/aa;

.field private ao:Lcom/google/android/gms/games/ui/common/matches/y;

.field private f:[Lcom/google/android/gms/games/multiplayer/Participant;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/google/android/gms/i;->B:I

    sput v0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d:I

    .line 64
    sget v0, Lcom/google/android/gms/g;->bj:I

    sput v0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    sget v0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/m;-><init>(I)V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->al:Ljava/util/HashMap;

    .line 94
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->al:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Lcom/google/android/gms/games/ui/common/matches/aa;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)[Lcom/google/android/gms/games/multiplayer/Participant;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 176
    if-ne p1, v2, :cond_1

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aj:Lcom/google/android/gms/games/multiplayer/Participant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aj:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_2

    .line 179
    :cond_0
    const-string v0, "ParticipantListFrag"

    const-string v1, "no mManagedParticipant or data for manage circles operation."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_1
    :goto_0
    return-void

    .line 183
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 188
    invoke-static {p3}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v3

    .line 190
    invoke-interface {v3}, Lcom/google/android/gms/common/a/a/c;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Lcom/google/android/gms/common/a/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move v0, v2

    .line 192
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Ljava/lang/String;

    invoke-static {v4, v5, v6, v2, v0}, Lcom/google/android/gms/games/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 196
    invoke-interface {v3}, Lcom/google/android/gms/common/a/a/c;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 198
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->al:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aj:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-interface {v3}, Lcom/google/android/gms/common/a/a/c;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 203
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/l;->ca:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aj:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 205
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ak:Lcom/google/android/gms/games/ui/common/matches/x;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/x;->notifyDataSetChanged()V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 190
    goto :goto_1

    :cond_6
    move v0, v1

    .line 196
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->a(Lcom/google/android/gms/common/api/t;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ak:Lcom/google/android/gms/games/ui/common/matches/x;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 98
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->d(Landroid/os/Bundle;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/aa;

    const-string v2, "Parent activity did not implement ParticipantListMetaDataProvider"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/aa;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/y;

    const-string v2, "Parent activity did not implement ParticipantListListener"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/y;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ao:Lcom/google/android/gms/games/ui/common/matches/y;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->E()[Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Ljava/lang/String;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->G()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->H()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->I()Landroid/net/Uri;

    move-result-object v7

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/aa;->J()Landroid/net/Uri;

    move v0, v1

    .line 115
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 116
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    aget-object v2, v2, v0

    .line 117
    if-eqz v2, :cond_0

    .line 118
    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    .line 119
    if-eqz v2, :cond_0

    .line 120
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->al:Ljava/util/HashMap;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->k()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    sget v2, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/l;->aW:I

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    sget v2, Lcom/google/android/gms/g;->ak:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 136
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/x;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:[Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/x;-><init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/content/Context;[Lcom/google/android/gms/games/multiplayer/Participant;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ak:Lcom/google/android/gms/games/ui/common/matches/x;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    sget v2, Lcom/google/android/gms/g;->O:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 142
    if-eqz v0, :cond_3

    .line 147
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:Lcom/google/android/gms/games/ui/n;

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ak:Lcom/google/android/gms/games/ui/common/matches/x;

    new-instance v9, Landroid/widget/FrameLayout;

    invoke-direct {v9, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-interface {v8}, Landroid/widget/Adapter;->getCount()I

    move-result v10

    move v4, v1

    move-object v2, v5

    move v3, v1

    :goto_1
    if-ge v4, v10, :cond_2

    invoke-interface {v8, v4, v2, v9}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v1, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-le v2, v3, :cond_5

    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move-object v2, v6

    goto :goto_1

    :cond_2
    int-to-double v2, v3

    const-wide v8, 0x3ff0cccccccccccdL    # 1.05

    mul-double/2addr v2, v8

    double-to-int v1, v2

    .line 150
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 151
    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 153
    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->an:Lcom/google/android/gms/games/ui/common/matches/aa;

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 159
    if-eqz v7, :cond_4

    .line 160
    invoke-virtual {v0, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;)V

    .line 165
    :cond_3
    :goto_3
    return-void

    .line 162
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 214
    sget v0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 215
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    .line 217
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 218
    sget v4, Lcom/google/android/gms/g;->aE:I

    if-ne v3, v4, :cond_3

    .line 219
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 220
    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aj:Lcom/google/android/gms/games/multiplayer/Participant;

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/games/ui/e/v;

    const/4 v6, 0x1

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/e/v;-><init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/e/y;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    iget-boolean v1, v0, Lcom/google/android/gms/games/ui/e/v;->g:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Restarting a previously canceled ManageCirclesHelper instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v2, v0, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    iput-boolean v7, v0, Lcom/google/android/gms/games/ui/e/v;->c:Z

    iput-object v2, v0, Lcom/google/android/gms/games/ui/e/v;->d:[Ljava/lang/String;

    iput-boolean v7, v0, Lcom/google/android/gms/games/ui/e/v;->e:Z

    iput-object v2, v0, Lcom/google/android/gms/games/ui/e/v;->f:Ljava/util/ArrayList;

    iput-boolean v7, v0, Lcom/google/android/gms/games/ui/e/v;->g:Z

    iget-object v1, v0, Lcom/google/android/gms/games/ui/e/v;->i:Lcom/google/android/gms/common/api/t;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "ManageCirclesHelper"

    const-string v1, "ManageCirclesHelper.start: PeopleClient not connected!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    sget-object v1, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    iget-object v3, v0, Lcom/google/android/gms/games/ui/e/v;->i:Lcom/google/android/gms/common/api/t;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/e/v;->h:Ljava/lang/String;

    invoke-interface {v1, v3, v4, v2}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/games/ui/e/w;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/ui/e/w;-><init>(Lcom/google/android/gms/games/ui/e/v;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    new-instance v1, Lcom/google/android/gms/people/h;

    invoke-direct {v1}, Lcom/google/android/gms/people/h;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v0, Lcom/google/android/gms/games/ui/e/v;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/h;->a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;

    sget-object v2, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    iget-object v3, v0, Lcom/google/android/gms/games/ui/e/v;->i:Lcom/google/android/gms/common/api/t;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/e/v;->h:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/games/ui/e/x;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/ui/e/x;-><init>(Lcom/google/android/gms/games/ui/e/v;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0

    .line 224
    :cond_3
    sget v1, Lcom/google/android/gms/g;->bl:I

    if-ne v3, v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ao:Lcom/google/android/gms/games/ui/common/matches/y;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/y;->a(Lcom/google/android/gms/games/multiplayer/Participant;)V

    goto :goto_0
.end method
