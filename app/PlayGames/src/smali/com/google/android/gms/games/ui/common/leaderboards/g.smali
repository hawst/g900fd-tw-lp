.class public abstract Lcom/google/android/gms/games/ui/common/leaderboards/g;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/common/leaderboards/b;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/leaderboards/h;

.field private an:Lcom/google/android/gms/games/ui/common/leaderboards/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 139
    sget v1, Lcom/google/android/gms/e;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/e;->g:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    .line 142
    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/games/a/n;

    invoke-interface {p1}, Lcom/google/android/gms/games/a/n;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/a/n;->c()Lcom/google/android/gms/games/a/b;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->an:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/a;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->a()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v0, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/a/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/a/m;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 73
    :goto_0
    return-void

    .line 69
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/a/m;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/a;)V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->b(Lcom/google/android/gms/games/a/a;)V

    .line 106
    return-void
.end method

.method protected abstract b(Lcom/google/android/gms/games/a/a;)V
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v1

    .line 41
    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/gms/f;->w:I

    :goto_0
    sget v2, Lcom/google/android/gms/l;->aE:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(III)V

    .line 44
    if-eqz v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(II)V

    .line 50
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->an:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    .line 52
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->R()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    new-instance v1, Lcom/google/android/gms/games/ui/common/leaderboards/h;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/h;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->am:Lcom/google/android/gms/games/ui/common/leaderboards/h;

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->am:Lcom/google/android/gms/games/ui/common/leaderboards/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->an:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(Landroid/support/v7/widget/bv;)V

    .line 60
    return-void

    .line 41
    :cond_2
    sget v0, Lcom/google/android/gms/f;->v:I

    goto :goto_0
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const-string v0, "LeaderboardFrag"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    sget-object v1, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/a/m;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/a/m;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 132
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    goto :goto_0

    .line 128
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/a/m;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/a/m;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_1
.end method
