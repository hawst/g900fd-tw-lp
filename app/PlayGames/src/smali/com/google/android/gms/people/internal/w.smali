.class final Lcom/google/android/gms/people/internal/w;
.super Lcom/google/android/gms/common/internal/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/i;


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/internal/m;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/people/model/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/i;)V
    .locals 0

    .prologue
    .line 616
    iput-object p1, p0, Lcom/google/android/gms/people/internal/w;->b:Lcom/google/android/gms/people/internal/m;

    .line 617
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    .line 618
    iput-object p3, p0, Lcom/google/android/gms/people/internal/w;->c:Lcom/google/android/gms/common/api/Status;

    .line 619
    iput-object p4, p0, Lcom/google/android/gms/people/internal/w;->d:Lcom/google/android/gms/people/model/i;

    .line 620
    return-void
.end method


# virtual methods
.method public final I_()Lcom/google/android/gms/people/model/i;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/people/internal/w;->d:Lcom/google/android/gms/people/model/i;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/gms/people/internal/w;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 610
    check-cast p1, Lcom/google/android/gms/common/api/m;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/w;->f_()V

    .line 632
    return-void
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/gms/people/internal/w;->d:Lcom/google/android/gms/people/model/i;

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/google/android/gms/people/internal/w;->d:Lcom/google/android/gms/people/model/i;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/i;->c()V

    .line 649
    :cond_0
    return-void
.end method
