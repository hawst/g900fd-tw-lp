.class public final Lcom/google/android/gms/games/c/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/c/aq;

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2181
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/u;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/u;->G:I

    .line 2183
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    .line 2240
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 2241
    iget-object v1, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-eqz v1, :cond_0

    .line 2242
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2245
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/c/u;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2246
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/u;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2249
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/aq;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/u;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-eqz v0, :cond_0

    .line 2230
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2232
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/c/u;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2233
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/u;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 2235
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 2236
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2194
    if-ne p1, p0, :cond_1

    .line 2213
    :cond_0
    :goto_0
    return v0

    .line 2197
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 2198
    goto :goto_0

    .line 2200
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/u;

    .line 2201
    iget-object v2, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-nez v2, :cond_3

    .line 2202
    iget-object v2, p1, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2203
    goto :goto_0

    .line 2206
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    iget-object v3, p1, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/aq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2207
    goto :goto_0

    .line 2210
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/games/c/u;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/u;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 2211
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 2218
    iget-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2221
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/u;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/u;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2223
    return v0

    .line 2218
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/u;->a:Lcom/google/android/gms/games/c/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/aq;->hashCode()I

    move-result v0

    goto :goto_0
.end method
