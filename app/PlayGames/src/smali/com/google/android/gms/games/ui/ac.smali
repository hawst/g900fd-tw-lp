.class public final Lcom/google/android/gms/games/ui/ac;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/gms/games/ui/ad;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/gms/i;->L:I

    sput v0, Lcom/google/android/gms/games/ui/ac;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/ac;)Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/ad;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->l:Lcom/google/android/gms/games/ui/ad;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/gms/games/ui/ae;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/ac;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/ac;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ae;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->c:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->i:Z

    .line 125
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ac;->l:Lcom/google/android/gms/games/ui/ad;

    .line 126
    iput-object p2, p0, Lcom/google/android/gms/games/ui/ac;->m:Ljava/lang/String;

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 128
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->i:Z

    .line 110
    iput-object p2, p0, Lcom/google/android/gms/games/ui/ac;->k:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/ac;->j:Ljava/lang/String;

    .line 111
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ac;->l:Lcom/google/android/gms/games/ui/ad;

    .line 112
    iput-object p3, p0, Lcom/google/android/gms/games/ui/ac;->m:Ljava/lang/String;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 114
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ac;->g:Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 69
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->i:Z

    if-eq v0, p1, :cond_0

    .line 173
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/ac;->i:Z

    .line 174
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 176
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/games/ui/ac;->e:I

    return v0
.end method

.method public final d(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 150
    if-lez p1, :cond_1

    .line 151
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 152
    sub-int v2, p1, p2

    .line 153
    if-lez v2, :cond_0

    .line 154
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 155
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/ac;->i(I)V

    .line 162
    :goto_0
    return v0

    .line 157
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    goto :goto_0

    .line 161
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    move v0, v1

    .line 162
    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/ac;->a(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public final h(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/ac;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 78
    return-void
.end method

.method public final i(I)V
    .locals 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/l;->I:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/ac;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/ac;->j:Ljava/lang/String;

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->d()V

    .line 140
    return-void
.end method
