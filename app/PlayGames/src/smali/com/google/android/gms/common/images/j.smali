.class public final Lcom/google/android/gms/common/images/j;
.super Lcom/google/android/gms/common/images/g;
.source "SourceFile"


# instance fields
.field private j:Ljava/lang/ref/WeakReference;


# virtual methods
.method protected final a(Landroid/graphics/drawable/Drawable;ZZZ)V
    .locals 2

    .prologue
    .line 452
    if-nez p3, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/common/images/j;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/f;

    .line 454
    if-eqz v0, :cond_0

    .line 455
    iget-object v1, p0, Lcom/google/android/gms/common/images/j;->a:Lcom/google/android/gms/common/images/h;

    iget-object v1, v1, Lcom/google/android/gms/common/images/h;->a:Landroid/net/Uri;

    invoke-interface {v0, p4}, Lcom/google/android/gms/common/images/f;->a(Z)V

    .line 459
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 429
    instance-of v0, p1, Lcom/google/android/gms/common/images/j;

    if-nez v0, :cond_0

    move v0, v2

    .line 443
    :goto_0
    return v0

    .line 432
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v3

    .line 433
    goto :goto_0

    .line 436
    :cond_1
    check-cast p1, Lcom/google/android/gms/common/images/j;

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/common/images/j;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/f;

    .line 438
    iget-object v1, p1, Lcom/google/android/gms/common/images/j;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/images/f;

    .line 443
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/common/images/j;->a:Lcom/google/android/gms/common/images/h;

    iget-object v1, p0, Lcom/google/android/gms/common/images/j;->a:Lcom/google/android/gms/common/images/h;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 424
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/images/j;->a:Lcom/google/android/gms/common/images/h;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
