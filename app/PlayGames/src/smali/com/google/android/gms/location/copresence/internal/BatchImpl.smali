.class public final Lcom/google/android/gms/location/copresence/internal/BatchImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    .line 75
    return-void
.end method

.method constructor <init>(ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    .line 69
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 213
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/internal/a;->a(Lcom/google/android/gms/location/copresence/internal/BatchImpl;Landroid/os/Parcel;)V

    .line 214
    return-void
.end method
