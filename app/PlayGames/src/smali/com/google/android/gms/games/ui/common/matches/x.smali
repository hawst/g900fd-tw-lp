.class final Lcom/google/android/gms/games/ui/common/matches/x;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/content/Context;[Lcom/google/android/gms/games/multiplayer/Participant;)V
    .locals 1

    .prologue
    .line 329
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/x;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    .line 330
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 332
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/x;->b:Landroid/content/Context;

    .line 333
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/x;->c:Landroid/view/LayoutInflater;

    .line 334
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 338
    if-nez p2, :cond_0

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/x;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/i;->C:I

    invoke-virtual {v0, v1, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/z;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/x;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/games/ui/common/matches/z;-><init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 342
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/z;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/x;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/x;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)[Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v2

    aget-object v2, v2, p1

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/l;->aV:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->g:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    sget v3, Lcom/google/android/gms/l;->aV:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->c(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Lcom/google/android/gms/games/ui/common/matches/aa;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/d;->j:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 343
    return-object p2

    .line 342
    :cond_1
    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/f;->g:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v5, v0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    sget v6, Lcom/google/android/gms/l;->aZ:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    sget v6, Lcom/google/android/gms/l;->aZ:I

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    invoke-static {}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a()I

    move-result v5

    invoke-virtual {v1, v5, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v4, :cond_3

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_5

    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_4
    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    invoke-static {}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a()I

    move-result v3

    invoke-virtual {v1, v3, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method
