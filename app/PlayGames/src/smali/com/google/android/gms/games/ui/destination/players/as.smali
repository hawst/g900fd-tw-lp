.class final Lcom/google/android/gms/games/ui/destination/players/as;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 68
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 10

    .prologue
    const v8, 0x106000b

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 64
    check-cast p3, Lcom/google/android/gms/games/internal/d/a;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/as;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->g()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v3, v1

    :goto_0
    if-nez v3, :cond_4

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->b(Z)V

    if-eqz v3, :cond_5

    const v0, 0x7f0c005b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->h()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/destination/players/as;->b(I)V

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/as;->e(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/as;->t()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/games/internal/d/a;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->a(Landroid/database/CharArrayBuffer;)V

    if-eqz v3, :cond_0

    invoke-virtual {p0, v8}, Lcom/google/android/gms/games/ui/destination/players/as;->f(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/as;->u()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/games/internal/d/a;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->b(Landroid/database/CharArrayBuffer;)V

    if-eqz v3, :cond_1

    invoke-virtual {p0, v8}, Lcom/google/android/gms/games/ui/destination/players/as;->h(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/as;->k:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->e()J

    move-result-wide v6

    const/high16 v5, 0x80000

    invoke-static {v0, v6, v7, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->f(Ljava/lang/String;)V

    if-eqz v3, :cond_7

    invoke-virtual {p0, v8}, Lcom/google/android/gms/games/ui/destination/players/as;->j(I)V

    :goto_3
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->f()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_2

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f0f01e6

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v3, v8, v2

    invoke-virtual {v4, v5, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/players/as;->g(Ljava/lang/String;)V

    :cond_2
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->c()Ljava/lang/String;

    move-result-object v5

    const v8, 0x7f0f00d6

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v3, v9, v2

    aput-object v5, v9, v1

    const/4 v1, 0x2

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x3

    aput-object v0, v9, v1

    invoke-virtual {v4, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/as;->a(Ljava/lang/String;)V

    return-void

    :cond_3
    move v3, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/d/a;->d()Landroid/net/Uri;

    move-result-object v0

    const/16 v5, 0xb

    invoke-static {v5}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v0, 0x0

    :cond_6
    const v5, 0x7f02009f

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/games/ui/destination/players/as;->a(Landroid/net/Uri;I)V

    goto/16 :goto_2

    :cond_7
    const v3, 0x7f0a00cc

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/players/as;->j(I)V

    goto :goto_3
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/as;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/aq;

    .line 158
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/aq;->b(Lcom/google/android/gms/games/ui/destination/players/aq;)Lcom/google/android/gms/games/ui/destination/players/ar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/as;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/d/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/ar;->a(Lcom/google/android/gms/games/internal/d/a;)V

    .line 159
    return-void
.end method

.method protected final s(I)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 140
    const v0, 0x7f0c005b

    if-ne p1, v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/as;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/aq;

    .line 142
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/aq;->a(Lcom/google/android/gms/games/ui/destination/players/aq;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d4

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 143
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 146
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    return-object v0

    .line 149
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown image view type received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
