.class public final Lcom/google/android/gms/car/CarCall$Details;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Landroid/net/Uri;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Landroid/telecom/PhoneAccountHandle;

.field public g:I

.field public h:Landroid/telecom/DisconnectCause;

.field public i:J

.field public j:Landroid/telecom/GatewayInfo;

.field public k:I

.field public l:Landroid/telecom/StatusHints;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lcom/google/android/gms/car/d;

    invoke-direct {v0}, Lcom/google/android/gms/car/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarCall$Details;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;ILjava/lang/String;ILandroid/telecom/PhoneAccountHandle;ILandroid/telecom/DisconnectCause;JLandroid/telecom/GatewayInfo;ILandroid/telecom/StatusHints;)V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput p1, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    .line 246
    iput-object p2, p0, Lcom/google/android/gms/car/CarCall$Details;->b:Landroid/net/Uri;

    .line 247
    iput p3, p0, Lcom/google/android/gms/car/CarCall$Details;->c:I

    .line 248
    iput-object p4, p0, Lcom/google/android/gms/car/CarCall$Details;->d:Ljava/lang/String;

    .line 249
    iput p5, p0, Lcom/google/android/gms/car/CarCall$Details;->e:I

    .line 250
    iput-object p6, p0, Lcom/google/android/gms/car/CarCall$Details;->f:Landroid/telecom/PhoneAccountHandle;

    .line 251
    iput p7, p0, Lcom/google/android/gms/car/CarCall$Details;->g:I

    .line 252
    iput-object p8, p0, Lcom/google/android/gms/car/CarCall$Details;->h:Landroid/telecom/DisconnectCause;

    .line 253
    iput-wide p9, p0, Lcom/google/android/gms/car/CarCall$Details;->i:J

    .line 254
    iput-object p11, p0, Lcom/google/android/gms/car/CarCall$Details;->j:Landroid/telecom/GatewayInfo;

    .line 255
    iput p12, p0, Lcom/google/android/gms/car/CarCall$Details;->k:I

    .line 256
    iput-object p13, p0, Lcom/google/android/gms/car/CarCall$Details;->l:Landroid/telecom/StatusHints;

    .line 257
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v2, 0x27

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Details{handle="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", handlePresentation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall$Details;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callerDisplayName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callerDisplayNamePresentation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall$Details;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountHandle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->f:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callCapabilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall$Details;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", disconnectCause=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->h:Landroid/telecom/DisconnectCause;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectTimeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/car/CarCall$Details;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gatewayInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->j:Landroid/telecom/GatewayInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall$Details;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusHints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->l:Landroid/telecom/StatusHints;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 266
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/d;->a(Lcom/google/android/gms/car/CarCall$Details;Landroid/os/Parcel;I)V

    .line 267
    return-void
.end method
