.class public final Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/gms/games/Player;

.field private g:Lcom/google/android/gms/games/Player;

.field private h:Ljava/util/ArrayList;

.field private i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->j:I

    .line 65
    const v1, 0x7f0a009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->k:I

    .line 66
    const v1, 0x7f0a004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->l:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->m:Z

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->j:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->k:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->m:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->g:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->i:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->l:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f040042

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/aa;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;Ljava/util/ArrayList;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 100
    if-ltz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 101
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 102
    invoke-static {p2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 103
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e:Lcom/google/android/gms/games/Player;

    .line 104
    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->g:Lcom/google/android/gms/games/Player;

    .line 105
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->h:Ljava/util/ArrayList;

    .line 106
    iput p4, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->i:I

    .line 107
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->c(Z)V

    .line 108
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f040042

    return v0
.end method
