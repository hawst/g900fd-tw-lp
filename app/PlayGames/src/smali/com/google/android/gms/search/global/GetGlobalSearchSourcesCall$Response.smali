.class public Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/f;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/gms/search/global/f;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->CREATOR:Lcom/google/android/gms/search/global/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->c:I

    .line 105
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput p1, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->c:I

    .line 127
    iput-object p2, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 128
    iput-object p3, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    .line 129
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->CREATOR:Lcom/google/android/gms/search/global/f;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->CREATOR:Lcom/google/android/gms/search/global/f;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/f;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;Landroid/os/Parcel;I)V

    .line 142
    return-void
.end method
