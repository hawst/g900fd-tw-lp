.class public Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public b:I

.field public c:Z

.field public final d:Lcom/google/android/gms/cloudsave/Entity$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZLcom/google/android/gms/cloudsave/Entity$Key;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;->a:I

    .line 57
    iput p2, p0, Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;->b:I

    .line 58
    iput-boolean p3, p0, Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;->c:Z

    .line 59
    iput-object p4, p0, Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;->d:Lcom/google/android/gms/cloudsave/Entity$Key;

    .line 60
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/a;->a(Lcom/google/android/gms/cloudsave/internal/DeleteEntityResponse;Landroid/os/Parcel;I)V

    .line 43
    return-void
.end method
