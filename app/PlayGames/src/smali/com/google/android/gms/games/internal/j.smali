.class final Lcom/google/android/gms/games/internal/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/n;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773
    iput-object p1, p0, Lcom/google/android/gms/games/internal/j;->a:Lcom/google/android/gms/common/api/Status;

    .line 2774
    iput-object p2, p0, Lcom/google/android/gms/games/internal/j;->b:Landroid/os/Bundle;

    .line 2775
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2794
    iget-object v0, p0, Lcom/google/android/gms/games/internal/j;->b:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2779
    iget-object v0, p0, Lcom/google/android/gms/games/internal/j;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2784
    iget-object v0, p0, Lcom/google/android/gms/games/internal/j;->b:Landroid/os/Bundle;

    const-string v1, "inbox_has_new_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 2789
    iget-object v0, p0, Lcom/google/android/gms/games/internal/j;->b:Landroid/os/Bundle;

    const-string v1, "inbox_total_count"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
