.class public abstract Lcom/google/android/gms/games/ui/destination/games/ab;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/destination/games/ak;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field protected an:Lcom/google/android/gms/games/ui/destination/games/aj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    .line 249
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(Lcom/google/android/gms/games/j;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 0

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/ab;->b(Lcom/google/android/gms/common/api/t;)V

    .line 73
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 155
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->au()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/app/a;->b(I)V

    .line 142
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 148
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/j;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    .line 84
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    .line 88
    const/4 v1, 0x1

    .line 90
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    .line 106
    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    .line 93
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    .line 106
    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 98
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/games/aj;->k()V

    .line 100
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/destination/games/aj;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 103
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a_(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPG_overflowMenu"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(Lcom/google/android/gms/games/app/a;)V

    .line 132
    return-void
.end method

.method protected at()Lcom/google/android/gms/games/ui/w;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/gms/games/ui/destination/games/aj;-><init>(Lcom/google/android/gms/games/ui/n;IILcom/google/android/gms/games/ui/destination/games/ak;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/aj;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    return-object v0
.end method

.method protected abstract au()I
.end method

.method protected abstract b(Lcom/google/android/gms/common/api/t;)V
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f0200d3

    const v1, 0x7f0f0153

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(III)V

    .line 54
    const v0, 0x7f0b01ed

    iput v0, p0, Lcom/google/android/gms/games/ui/p;->al:I

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->at()Lcom/google/android/gms/games/ui/w;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(Landroid/support/v7/widget/bv;)V

    .line 56
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    const-string v0, "GameExploreFragment"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ab;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 121
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/ab;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
