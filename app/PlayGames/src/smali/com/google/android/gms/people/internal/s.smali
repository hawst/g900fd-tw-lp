.class final Lcom/google/android/gms/people/internal/s;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/internal/m;

.field private final b:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/google/android/gms/people/internal/s;->a:Lcom/google/android/gms/people/internal/m;

    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 941
    iput-object p2, p0, Lcom/google/android/gms/people/internal/s;->b:Lcom/google/android/gms/common/api/m;

    .line 942
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 947
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Avatar callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/x;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/m;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    .line 956
    if-eqz p4, :cond_1

    .line 957
    const-string v0, "rewindable"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 958
    const-string v0, "width"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 959
    const-string v0, "height"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 961
    :goto_0
    iget-object v8, p0, Lcom/google/android/gms/people/internal/s;->a:Lcom/google/android/gms/people/internal/m;

    new-instance v0, Lcom/google/android/gms/people/internal/v;

    iget-object v1, p0, Lcom/google/android/gms/people/internal/s;->a:Lcom/google/android/gms/people/internal/m;

    iget-object v2, p0, Lcom/google/android/gms/people/internal/s;->b:Lcom/google/android/gms/common/api/m;

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/internal/v;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;ZII)V

    invoke-virtual {v8, v0}, Lcom/google/android/gms/people/internal/m;->a(Lcom/google/android/gms/common/internal/g;)V

    .line 963
    return-void

    :cond_1
    move v6, v7

    move v5, v7

    goto :goto_0
.end method
