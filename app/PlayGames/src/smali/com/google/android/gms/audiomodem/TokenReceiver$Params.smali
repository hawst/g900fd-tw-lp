.class public Lcom/google/android/gms/audiomodem/TokenReceiver$Params;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[Lcom/google/android/gms/audiomodem/Encoding;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/audiomodem/g;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a:I

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    .line 66
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a:I

    return v0
.end method

.method public final b()[Lcom/google/android/gms/audiomodem/Encoding;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 87
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/audiomodem/g;->a(Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Landroid/os/Parcel;I)V

    .line 88
    return-void
.end method
