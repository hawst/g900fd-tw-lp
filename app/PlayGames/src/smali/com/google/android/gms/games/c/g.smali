.class public final Lcom/google/android/gms/games/c/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Lcom/google/android/gms/games/c/f;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2835
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2836
    iput v1, p0, Lcom/google/android/gms/games/c/g;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/g;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/g;->e:I

    iput v1, p0, Lcom/google/android/gms/games/c/g;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/g;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/g;->G:I

    .line 2837
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 2951
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 2952
    iget v1, p0, Lcom/google/android/gms/games/c/g;->a:I

    if-eqz v1, :cond_0

    .line 2953
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/g;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2956
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2957
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2960
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/g;->c:I

    if-eqz v1, :cond_2

    .line 2961
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/g;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2964
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2965
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2968
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/c/g;->e:I

    if-eqz v1, :cond_4

    .line 2969
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/games/c/g;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2972
    :cond_4
    iget v1, p0, Lcom/google/android/gms/games/c/g;->f:I

    if-eqz v1, :cond_5

    .line 2973
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/games/c/g;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2976
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-eqz v1, :cond_6

    .line 2977
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2980
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/g;->h:Z

    if-eqz v1, :cond_7

    .line 2981
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/g;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2984
    :cond_7
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/android/gms/games/c/g;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/g;->c:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/g;->e:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/g;->f:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/g;->h:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
        0x66 -> :sswitch_2
        0x67 -> :sswitch_2
        0x68 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2922
    iget v0, p0, Lcom/google/android/gms/games/c/g;->a:I

    if-eqz v0, :cond_0

    .line 2923
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/g;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2925
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2926
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2928
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/g;->c:I

    if-eqz v0, :cond_2

    .line 2929
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/g;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2931
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2932
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2934
    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/c/g;->e:I

    if-eqz v0, :cond_4

    .line 2935
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/games/c/g;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2937
    :cond_4
    iget v0, p0, Lcom/google/android/gms/games/c/g;->f:I

    if-eqz v0, :cond_5

    .line 2938
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/games/c/g;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2940
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-eqz v0, :cond_6

    .line 2941
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2943
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/g;->h:Z

    if-eqz v0, :cond_7

    .line 2944
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/g;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2946
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 2947
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2854
    if-ne p1, p0, :cond_1

    .line 2899
    :cond_0
    :goto_0
    return v0

    .line 2857
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 2858
    goto :goto_0

    .line 2860
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/g;

    .line 2861
    iget v2, p0, Lcom/google/android/gms/games/c/g;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/g;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2862
    goto :goto_0

    .line 2864
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2865
    iget-object v2, p1, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 2866
    goto :goto_0

    .line 2868
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2869
    goto :goto_0

    .line 2871
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/c/g;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/g;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2872
    goto :goto_0

    .line 2874
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2875
    iget-object v2, p1, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2876
    goto :goto_0

    .line 2878
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2879
    goto :goto_0

    .line 2881
    :cond_8
    iget v2, p0, Lcom/google/android/gms/games/c/g;->e:I

    iget v3, p1, Lcom/google/android/gms/games/c/g;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2882
    goto :goto_0

    .line 2884
    :cond_9
    iget v2, p0, Lcom/google/android/gms/games/c/g;->f:I

    iget v3, p1, Lcom/google/android/gms/games/c/g;->f:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2885
    goto :goto_0

    .line 2887
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-nez v2, :cond_b

    .line 2888
    iget-object v2, p1, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-eqz v2, :cond_c

    move v0, v1

    .line 2889
    goto :goto_0

    .line 2892
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    iget-object v3, p1, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2893
    goto :goto_0

    .line 2896
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/g;->h:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/g;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2897
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2904
    iget v0, p0, Lcom/google/android/gms/games/c/g;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2906
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2908
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/g;->c:I

    add-int/2addr v0, v2

    .line 2909
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2911
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/g;->e:I

    add-int/2addr v0, v2

    .line 2912
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/g;->f:I

    add-int/2addr v0, v2

    .line 2913
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2915
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/g;->h:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    .line 2916
    return v0

    .line 2906
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2909
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/g;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2913
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/g;->g:Lcom/google/android/gms/games/c/f;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/f;->hashCode()I

    move-result v1

    goto :goto_2

    .line 2915
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method
