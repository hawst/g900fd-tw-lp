.class final Lcom/google/android/gms/games/ui/destination/games/d;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 124
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->g(Z)V

    .line 125
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/b;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 272
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 13

    .prologue
    .line 120
    check-cast p3, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-super/range {p0 .. p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/d;->c(Ljava/lang/String;)V

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v5

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v6

    check-cast p1, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/destination/games/a;->w()I

    move-result v7

    const/4 v1, 0x2

    if-eq v7, v1, :cond_0

    const/4 v1, 0x1

    if-ne v7, v1, :cond_5

    :cond_0
    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    const v2, 0x7f02015c

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(Landroid/net/Uri;I)V

    const/4 v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(IF)V

    :goto_0
    const/4 v2, 0x0

    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->u()I

    move-result v8

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->g()I

    move-result v9

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->d()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v1, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v3, v1

    :goto_1
    if-ge v3, v11, :cond_e

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/game/GameBadge;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/game/GameBadge;->c()I

    move-result v1

    const/4 v12, 0x3

    if-ne v1, v12, :cond_9

    const/4 v1, 0x1

    :goto_2
    if-gtz v9, :cond_1

    if-lez v8, :cond_a

    if-eqz v1, :cond_a

    :cond_1
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_2

    const v2, 0x7f0a00d3

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f02007a

    invoke-virtual {p0, v3, v2, v9, v8}, Lcom/google/android/gms/games/ui/destination/games/d;->a(IIII)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/d;->k:Landroid/content/Context;

    const v3, 0x7f0f01a3

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v11

    const/4 v9, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v10, v9

    invoke-virtual {v2, v3, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->f(Ljava/lang/String;)V

    const v2, 0x7f0a00d3

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->j(I)V

    const v2, 0x7f02009e

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->k(I)V

    :cond_2
    const/4 v2, 0x5

    if-ne v7, v2, :cond_b

    const v2, 0x7f110009

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->m(I)V

    :goto_4
    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f0a0098

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0200cf

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->f(Z)V

    :cond_3
    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v2

    const v3, 0x7f020090

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/games/ui/destination/games/d;->b(Landroid/net/Uri;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->t()Landroid/database/CharArrayBuffer;

    move-result-object v2

    invoke-interface {v5, v2}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(Landroid/database/CharArrayBuffer;)V

    if-eqz v6, :cond_c

    invoke-interface {v6}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->e(Ljava/lang/String;)V

    :goto_5
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/b;

    move-result-object v0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_d

    const v0, 0x7f0f0036

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->n(I)V

    const v0, 0x7f0f0036

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->o(I)V

    :cond_4
    :goto_6
    return-void

    :cond_5
    if-eqz v6, :cond_7

    invoke-interface {v6}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->f()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v1

    :cond_6
    const v2, 0x7f02015c

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(Landroid/net/Uri;I)V

    :goto_7
    const/4 v1, 0x2

    const v2, 0x4003126f    # 2.048f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(IF)V

    goto/16 :goto_0

    :cond_7
    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v1, 0x0

    :cond_8
    const v2, 0x7f02015c

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->a(Landroid/net/Uri;I)V

    goto :goto_7

    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_b
    const v2, 0x7f110008

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->m(I)V

    goto/16 :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->u()Landroid/database/CharArrayBuffer;

    move-result-object v2

    invoke-interface {v5, v2}, Lcom/google/android/gms/games/Game;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/d;->b(Landroid/database/CharArrayBuffer;)V

    goto :goto_5

    :cond_d
    const v0, 0x7f0f0037

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->n(I)V

    const v0, 0x7f0f0037

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->o(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/d;->k(Z)V

    goto :goto_6

    :cond_e
    move v1, v2

    goto/16 :goto_2
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/a;

    .line 255
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->w()I

    move-result v3

    .line 257
    if-eq v3, v1, :cond_0

    if-ne v3, v2, :cond_1

    :cond_0
    move v1, v2

    .line 260
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->b(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v2, v0, v1, p1}, Lcom/google/android/gms/games/ui/destination/games/c;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 261
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 281
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 294
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    .line 283
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v1, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/a;->b(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/c;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/c;->b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 296
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 287
    :pswitch_1
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->b(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/c;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/games/ui/destination/games/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 281
    :pswitch_data_0
    .packed-switch 0x7f0c0287
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->b(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/d;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/c;->b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 266
    return-void
.end method
