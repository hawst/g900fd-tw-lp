.class public final Lcom/google/android/gms/games/internal/a/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/request/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 234
    new-instance v0, Lcom/google/android/gms/games/internal/a/bz;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/bz;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/t;III)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 8

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/gms/games/internal/a/cb;

    const/4 v5, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/cb;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 295
    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/games/internal/a/ca;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ca;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 157
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->q()V

    .line 158
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/request/c;)V
    .locals 2

    .prologue
    .line 151
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 152
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->d(Lcom/google/android/gms/common/api/ag;)V

    .line 153
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/request/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 249
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 250
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->d(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 256
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->d(Ljava/lang/String;)V

    .line 257
    return-void
.end method
