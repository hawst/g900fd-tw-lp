.class final Lcom/google/android/gms/games/ui/destination/players/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/r;

.field final synthetic b:Lcom/google/android/gms/common/api/r;

.field final synthetic c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/d;->c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/d;->a:Lcom/google/android/gms/common/api/r;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/d;->b:Lcom/google/android/gms/common/api/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    .line 188
    check-cast p1, Lcom/google/android/gms/common/api/q;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/q;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/d;->a:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/d;->b:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/achievement/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/d;->c()Lcom/google/android/gms/games/achievement/a;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/gms/games/achievement/d;->c()Lcom/google/android/gms/games/achievement/a;

    move-result-object v4

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/d;->c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->n()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/d;->c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->o()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/d;->c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    invoke-static {v5}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/d;->f_()V

    invoke-interface {v1}, Lcom/google/android/gms/games/achievement/d;->f_()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/d;->c:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;

    invoke-static {v5, v2, v3, v4}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;ILcom/google/android/gms/games/achievement/a;Lcom/google/android/gms/games/achievement/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/d;->f_()V

    invoke-interface {v1}, Lcom/google/android/gms/games/achievement/d;->f_()V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/d;->f_()V

    invoke-interface {v1}, Lcom/google/android/gms/games/achievement/d;->f_()V

    throw v2
.end method
