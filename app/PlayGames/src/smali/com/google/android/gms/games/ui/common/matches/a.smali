.class public final Lcom/google/android/gms/games/ui/common/matches/a;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private g:Lcom/google/android/gms/games/ui/common/matches/b;

.field private h:Z

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/gms/i;->k:I

    sput v0, Lcom/google/android/gms/games/ui/common/matches/a;->e:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/b;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->i:I

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/a;->g:Lcom/google/android/gms/games/ui/common/matches/b;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/a;)Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/a;)I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->i:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/a;)Lcom/google/android/gms/games/ui/common/matches/b;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->g:Lcom/google/android/gms/games/ui/common/matches/b;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/a;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/matches/a;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/c;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/common/matches/a;->h:Z

    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->h:Z

    if-nez v0, :cond_0

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->i:I

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/a;->d()V

    .line 47
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/google/android/gms/games/ui/common/matches/a;->e:I

    return v0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/a;->h:Z

    if-nez v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/common/matches/a;->i:I

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/a;->d()V

    goto :goto_0
.end method
