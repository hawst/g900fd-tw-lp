.class public Lcom/google/android/gms/appdatasearch/Section;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ah;


# instance fields
.field final a:I

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/appdatasearch/ah;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ah;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;ZI)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lcom/google/android/gms/appdatasearch/Section;->a:I

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    .line 52
    iput-boolean p3, p0, Lcom/google/android/gms/appdatasearch/Section;->c:Z

    .line 53
    iput p4, p0, Lcom/google/android/gms/appdatasearch/Section;->d:I

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ah;->a(Lcom/google/android/gms/appdatasearch/Section;Landroid/os/Parcel;)V

    .line 132
    return-void
.end method
