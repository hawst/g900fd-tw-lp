.class final Lcom/google/android/gms/games/internal/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/w;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Ljava/util/List;

.field private final c:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480
    iput-object p1, p0, Lcom/google/android/gms/games/internal/ad;->a:Lcom/google/android/gms/common/api/Status;

    .line 2481
    const-string v0, "game_category_list"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ad;->b:Ljava/util/List;

    .line 2482
    iput-object p2, p0, Lcom/google/android/gms/games/internal/ad;->c:Landroid/os/Bundle;

    .line 2483
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 2497
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ad;->c:Landroid/os/Bundle;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2487
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ad;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 2492
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ad;->b:Ljava/util/List;

    return-object v0
.end method
