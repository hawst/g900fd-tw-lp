.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:Ljava/lang/String;

.field c:Landroid/accounts/Account;

.field d:Landroid/os/Bundle;

.field e:Z

.field f:Z

.field g:Ljava/util/List;

.field h:Landroid/os/Bundle;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field i:Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field j:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field k:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field l:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field m:Ljava/lang/String;

.field n:Landroid/net/Uri;

.field o:Ljava/util/List;

.field p:I

.field q:Ljava/util/List;

.field r:Z

.field s:Lcom/google/android/gms/feedback/ErrorReport;

.field private t:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V
    .locals 2

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Lcom/google/android/gms/feedback/ErrorReport;

    .line 221
    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:I

    .line 224
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->b:Ljava/lang/String;

    .line 225
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    .line 226
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Landroid/os/Bundle;

    .line 229
    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->e:Z

    .line 230
    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->f:Z

    .line 233
    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->g:Ljava/util/List;

    .line 236
    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->h:Landroid/os/Bundle;

    .line 237
    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Landroid/graphics/Bitmap;

    .line 238
    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->j:[B

    .line 239
    iput p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:I

    .line 240
    iput p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:I

    .line 243
    iput-object p13, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->m:Ljava/lang/String;

    .line 246
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    .line 247
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->o:Ljava/util/List;

    .line 248
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->p:I

    .line 249
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->q:Ljava/util/List;

    .line 250
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->r:Z

    .line 251
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Lcom/google/android/gms/feedback/ErrorReport;

    .line 252
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 20

    .prologue
    .line 260
    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/16 v18, 0x0

    new-instance v19, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v19}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V

    .line 279
    return-void
.end method

.method public static a(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 558
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 559
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 560
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 561
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 563
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 446
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    .line 297
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 430
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Landroid/graphics/Bitmap;

    .line 431
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    .line 375
    return-object p0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 542
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/ErrorReport;->a(Landroid/graphics/Bitmap;)V

    .line 552
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/a;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    .line 553
    return-void
.end method
