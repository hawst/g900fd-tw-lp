.class public final Lcom/google/android/gms/games/ui/d/j;
.super Landroid/transition/Transition;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Z

.field private c:[I

.field private d:I

.field private e:Z

.field private f:F

.field private g:F

.field private h:Landroid/graphics/Rect;

.field private i:Landroid/graphics/Rect;

.field private j:Landroid/animation/TimeInterpolator;

.field private k:Landroid/animation/TimeInterpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android:silhouetteExpando:bounds"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android:silhouetteExpando:windowX"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android:silhouetteExpando:windowY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/d/j;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:[I

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Z

    .line 66
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/j;->j:Landroid/animation/TimeInterpolator;

    .line 67
    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/j;->k:Landroid/animation/TimeInterpolator;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d/j;F)F
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/google/android/gms/games/ui/d/j;->f:F

    return p1
.end method

.method private static a(FLandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 221
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 222
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 223
    cmpl-float v1, v1, p0

    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 226
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p0

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 227
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    sub-int v1, v4, v1

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 238
    :cond_0
    :goto_0
    return-object v0

    .line 231
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p0

    float-to-int v2, v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 233
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v1

    iget v4, p1, Landroid/graphics/Rect;->right:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v5, v1

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d/j;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    return-object v0
.end method

.method private a(Landroid/transition/TransitionValues;)V
    .locals 5

    .prologue
    .line 95
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 96
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 98
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/j;->c:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 100
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:windowX"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->c:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:silhouetteExpando:windowY"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->c:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/d/j;)F
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/games/ui/d/j;->f:F

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/d/j;F)F
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/google/android/gms/games/ui/d/j;->g:F

    return p1
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/d/j;)F
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/games/ui/d/j;->g:F

    return v0
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/gms/games/ui/d/j;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->e:Z

    .line 86
    return-object p0
.end method

.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d/j;->a(Landroid/transition/TransitionValues;)V

    .line 107
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d/j;->a(Landroid/transition/TransitionValues;)V

    .line 112
    return-void
.end method

.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 12

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 117
    if-eqz p2, :cond_4

    if-eqz p3, :cond_4

    .line 118
    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget-object v0, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:silhouetteExpando:bounds"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    iget v3, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/d/j;->a(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    :cond_0
    :goto_0
    iget-object v2, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d/j;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/games/ui/d/m;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/ui/d/m;-><init>(Lcom/google/android/gms/games/ui/d/j;)V

    new-instance v4, Lcom/google/android/gms/games/ui/d/n;

    invoke-direct {v4, p0}, Lcom/google/android/gms/games/ui/d/n;-><init>(Lcom/google/android/gms/games/ui/d/j;)V

    invoke-static {v2, v3, v4, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v11}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v2, v11}, Landroid/view/View;->setPivotY(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v4, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v0, v5

    iput v4, p0, Lcom/google/android/gms/games/ui/d/j;->f:F

    iput v5, p0, Lcom/google/android/gms/games/ui/d/j;->g:F

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v6, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v0, v6

    int-to-float v0, v0

    iget-object v6, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v0, v6

    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setX(F)V

    iget-object v6, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setY(F)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setScaleY(F)V

    const-string v6, "scaleX"

    new-array v7, v10, [F

    aput v4, v7, v8

    aput v1, v7, v9

    invoke-static {v2, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-string v4, "scaleY"

    new-array v6, v10, [F

    aput v5, v6, v8

    aput v0, v6, v9

    invoke-static {v2, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    new-instance v0, Lcom/google/android/gms/games/ui/d/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/d/k;-><init>(Lcom/google/android/gms/games/ui/d/j;)V

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/google/android/gms/games/ui/d/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/d/l;-><init>(Lcom/google/android/gms/games/ui/d/j;)V

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->j:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->j:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->k:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->k:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_2
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v1, v4, v8

    aput-object v2, v4, v9

    aput-object v3, v4, v10

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d/j;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 120
    :goto_2
    return-object v0

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    iget v3, p0, Lcom/google/android/gms/games/ui/d/j;->d:I

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/j;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->h:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/d/j;->a(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->i:Landroid/graphics/Rect;

    goto/16 :goto_0

    .line 120
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_1
.end method

.method public final getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/gms/games/ui/d/j;->a:[Ljava/lang/String;

    return-object v0
.end method
