.class public final Lcom/google/android/gms/games/ui/destination/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/destination/g;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/g;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/a/a;->b:Ljava/util/HashMap;

    .line 37
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    .line 42
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 23
    check-cast p1, Lcom/google/android/gms/games/m;

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->c()Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/a/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v5, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/g;->o()Z

    if-nez v3, :cond_0

    const-string v0, "DestMuteGameHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Application was not muted as it should have been. (status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", externalGameId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const v2, 0x7f0f01d0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/g;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/g;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const-string v0, "DestMuteGameHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Display name of muted game with externalGameId: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    const-string v0, "DestMuteGameHelper"

    const-string v1, "muteGame: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/a/a;->b:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const v2, 0x7f0f0178

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/destination/g;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {v1}, Lcom/google/android/gms/games/ui/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/c/f;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/a/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 62
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
