.class public Lcom/google/android/gms/cloudsave/Query;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/cloudsave/c;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/Long;

.field private final e:Ljava/lang/Double;

.field private final f:Ljava/lang/Boolean;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/cloudsave/c;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/Query;->CREATOR:Lcom/google/android/gms/cloudsave/c;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput p1, p0, Lcom/google/android/gms/cloudsave/Query;->a:I

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/Query;->b:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/Query;->c:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/google/android/gms/cloudsave/Query;->d:Ljava/lang/Long;

    .line 62
    iput-object p5, p0, Lcom/google/android/gms/cloudsave/Query;->e:Ljava/lang/Double;

    .line 63
    iput-object p6, p0, Lcom/google/android/gms/cloudsave/Query;->f:Ljava/lang/Boolean;

    .line 64
    iput-object p7, p0, Lcom/google/android/gms/cloudsave/Query;->g:Ljava/lang/String;

    .line 65
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->d:Ljava/lang/Long;

    return-object v0
.end method

.method public final d()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->e:Ljava/lang/Double;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/google/android/gms/cloudsave/Query;->CREATOR:Lcom/google/android/gms/cloudsave/c;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Query;->g:Ljava/lang/String;

    return-object v0
.end method

.method final g()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/google/android/gms/cloudsave/Query;->a:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/google/android/gms/cloudsave/Query;->CREATOR:Lcom/google/android/gms/cloudsave/c;

    invoke-static {p0, p1}, Lcom/google/android/gms/cloudsave/c;->a(Lcom/google/android/gms/cloudsave/Query;Landroid/os/Parcel;)V

    .line 207
    return-void
.end method
