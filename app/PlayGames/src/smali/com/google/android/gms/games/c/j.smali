.class public final Lcom/google/android/gms/games/c/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/c/m;

.field public c:Lcom/google/android/gms/games/c/k;

.field public d:Lcom/google/android/gms/games/c/q;

.field public e:Lcom/google/android/gms/games/c/p;

.field public f:Lcom/google/android/gms/games/c/o;

.field public g:Lcom/google/android/gms/games/c/n;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1521
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1522
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/j;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    iput-object v1, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/j;->G:I

    .line 1523
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 1654
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 1655
    iget v1, p0, Lcom/google/android/gms/games/c/j;->a:I

    if-eqz v1, :cond_0

    .line 1656
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/j;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1659
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-eqz v1, :cond_1

    .line 1660
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1663
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-eqz v1, :cond_2

    .line 1664
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1667
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-eqz v1, :cond_3

    .line 1668
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1671
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-eqz v1, :cond_4

    .line 1672
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1675
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-eqz v1, :cond_5

    .line 1676
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1679
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-eqz v1, :cond_6

    .line 1680
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1683
    :cond_6
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1469
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/j;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/c/q;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/c/p;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/c/o;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/games/c/n;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1628
    iget v0, p0, Lcom/google/android/gms/games/c/j;->a:I

    if-eqz v0, :cond_0

    .line 1629
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/j;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1631
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-eqz v0, :cond_1

    .line 1632
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1634
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-eqz v0, :cond_2

    .line 1635
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1637
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-eqz v0, :cond_3

    .line 1638
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1640
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-eqz v0, :cond_4

    .line 1641
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1643
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-eqz v0, :cond_5

    .line 1644
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1646
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-eqz v0, :cond_6

    .line 1647
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1649
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 1650
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1539
    if-ne p1, p0, :cond_1

    .line 1603
    :cond_0
    :goto_0
    return v0

    .line 1542
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 1543
    goto :goto_0

    .line 1545
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/j;

    .line 1546
    iget v2, p0, Lcom/google/android/gms/games/c/j;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/j;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1547
    goto :goto_0

    .line 1549
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-nez v2, :cond_4

    .line 1550
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1551
    goto :goto_0

    .line 1554
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1555
    goto :goto_0

    .line 1558
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-nez v2, :cond_6

    .line 1559
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-eqz v2, :cond_7

    move v0, v1

    .line 1560
    goto :goto_0

    .line 1563
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1564
    goto :goto_0

    .line 1567
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-nez v2, :cond_8

    .line 1568
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-eqz v2, :cond_9

    move v0, v1

    .line 1569
    goto :goto_0

    .line 1572
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1573
    goto :goto_0

    .line 1576
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-nez v2, :cond_a

    .line 1577
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1578
    goto :goto_0

    .line 1581
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1582
    goto :goto_0

    .line 1585
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-nez v2, :cond_c

    .line 1586
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-eqz v2, :cond_d

    move v0, v1

    .line 1587
    goto :goto_0

    .line 1590
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1591
    goto :goto_0

    .line 1594
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-nez v2, :cond_e

    .line 1595
    iget-object v2, p1, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1596
    goto/16 :goto_0

    .line 1599
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    iget-object v3, p1, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1600
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1608
    iget v0, p0, Lcom/google/android/gms/games/c/j;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1610
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1612
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1614
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1616
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1618
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1620
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1622
    return v0

    .line 1610
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->b:Lcom/google/android/gms/games/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/m;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1612
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->c:Lcom/google/android/gms/games/c/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/k;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1614
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->d:Lcom/google/android/gms/games/c/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/q;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1616
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->e:Lcom/google/android/gms/games/c/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/p;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1618
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/j;->f:Lcom/google/android/gms/games/c/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/o;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1620
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/c/j;->g:Lcom/google/android/gms/games/c/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/n;->hashCode()I

    move-result v1

    goto :goto_5
.end method
