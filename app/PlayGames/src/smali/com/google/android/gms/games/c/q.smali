.class public final Lcom/google/android/gms/games/c/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1157
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1158
    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->d:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/q;->G:I

    .line 1159
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1239
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 1240
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->a:Z

    if-eqz v2, :cond_0

    .line 1241
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/q;->a:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1244
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->b:Z

    if-eqz v2, :cond_1

    .line 1245
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/q;->b:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1248
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->c:Z

    if-eqz v2, :cond_2

    .line 1249
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/q;->c:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1252
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->d:Z

    if-eqz v2, :cond_3

    .line 1253
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/q;->d:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1256
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 1259
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 1260
    iget-object v4, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1261
    if-eqz v4, :cond_4

    .line 1262
    add-int/lit8 v3, v3, 0x1

    .line 1263
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1259
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1267
    :cond_5
    add-int/2addr v0, v2

    .line 1268
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1270
    :cond_6
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/q;->d:Z

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1214
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->a:Z

    if-eqz v0, :cond_0

    .line 1215
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/q;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1217
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->b:Z

    if-eqz v0, :cond_1

    .line 1218
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/q;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1220
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->c:Z

    if-eqz v0, :cond_2

    .line 1221
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/q;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1223
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->d:Z

    if-eqz v0, :cond_3

    .line 1224
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/q;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1226
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1227
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 1228
    iget-object v1, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1229
    if-eqz v1, :cond_4

    .line 1230
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1227
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1234
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 1235
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1173
    if-ne p1, p0, :cond_1

    .line 1196
    :cond_0
    :goto_0
    return v0

    .line 1176
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 1177
    goto :goto_0

    .line 1179
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/q;

    .line 1180
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->a:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/q;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1181
    goto :goto_0

    .line 1183
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/q;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1184
    goto :goto_0

    .line 1186
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/q;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1187
    goto :goto_0

    .line 1189
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/q;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/q;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1190
    goto :goto_0

    .line 1192
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1194
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 1201
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1203
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 1204
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/q;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 1205
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/q;->d:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1206
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/c/q;->e:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1208
    return v0

    :cond_0
    move v0, v2

    .line 1201
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1203
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1204
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1205
    goto :goto_3
.end method
