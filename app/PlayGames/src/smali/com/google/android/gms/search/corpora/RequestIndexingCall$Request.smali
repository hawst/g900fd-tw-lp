.class public Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/g;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/search/corpora/g;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->d:I

    .line 63
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->d:I

    .line 81
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->a:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->b:Ljava/lang/String;

    .line 83
    iput-wide p4, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->c:J

    .line 84
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/g;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/g;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/corpora/g;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;Landroid/os/Parcel;)V

    .line 96
    return-void
.end method
