.class public final Lcom/google/android/gms/games/ui/destination/games/aa;
.super Lcom/google/android/gms/games/ui/common/a/h;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/b;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/a/h;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/a/h;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 68
    const v0, 0x7f040033

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 70
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const v0, 0x7f0c00f2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 72
    const v3, 0x7f0f0069

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 73
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {v1, v2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 77
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v10

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 86
    new-instance v0, Lcom/google/android/gms/games/ui/e/o;

    const v2, 0x102000a

    const v3, 0x7f0c016a

    const v4, 0x7f0c0118

    const v5, 0x7f0c0164

    const v6, 0x7f0c0169

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/e/o;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/e/q;Lcom/google/android/gms/games/ui/e/p;Lcom/google/android/gms/games/ui/e/r;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->h:Lcom/google/android/gms/games/ui/e/o;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 91
    return-object v1
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 124
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.games.destination.VIEW_COMPLETED_QUEST_LIST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 125
    const-string v2, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v2, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 128
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;I)V

    .line 112
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/aa;->a(Lcom/google/android/gms/games/app/a;)V

    .line 102
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/a/h;->d(Landroid/os/Bundle;)V

    .line 42
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 43
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    .line 45
    new-instance v0, Lcom/google/android/gms/games/ui/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->an:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b;)V

    .line 47
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/a;->c(Z)V

    .line 48
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/a;->c()V

    .line 50
    new-instance v1, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 51
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/aa;->am:Lcom/google/android/gms/games/ui/common/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 52
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 54
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/aa;->ah()V

    .line 59
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/aa;->a(Landroid/support/v7/widget/bv;)V

    .line 60
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c00f2

    if-ne v0, v1, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/aa;->a()V

    .line 119
    :cond_0
    return-void
.end method
