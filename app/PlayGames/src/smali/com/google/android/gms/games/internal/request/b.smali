.class public final Lcom/google/android/gms/games/internal/request/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "requestId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "outcome"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/internal/request/b;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILjava/util/HashMap;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lcom/google/android/gms/games/internal/request/b;->b:I

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/games/internal/request/b;->c:Ljava/util/HashMap;

    .line 48
    return-void
.end method

.method private synthetic constructor <init>(ILjava/util/HashMap;B)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/internal/request/b;-><init>(ILjava/util/HashMap;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/internal/request/b;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 100
    new-instance v3, Lcom/google/android/gms/games/internal/request/c;

    invoke-direct {v3}, Lcom/google/android/gms/games/internal/request/c;-><init>()V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->e()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/games/internal/request/c;->b:I

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->g()I

    move-result v4

    move v0, v1

    .line 104
    :goto_0
    if-ge v0, v4, :cond_1

    .line 105
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v2

    .line 106
    const-string v5, "requestId"

    invoke-virtual {p0, v5, v0, v2}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "outcome"

    invoke-virtual {p0, v6, v0, v2}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, v3, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :pswitch_0
    const/4 v2, 0x1

    goto :goto_1

    .line 110
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/internal/request/b;

    iget v2, v3, Lcom/google/android/gms/games/internal/request/c;->b:I

    iget-object v3, v3, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/games/internal/request/b;-><init>(ILjava/util/HashMap;B)V

    return-object v0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
