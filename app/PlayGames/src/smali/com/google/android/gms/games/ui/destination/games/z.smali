.class public final Lcom/google/android/gms/games/ui/destination/games/z;
.super Lcom/google/android/gms/games/ui/common/leaderboards/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;I)V

    .line 81
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/z;->a(Lcom/google/android/gms/games/app/a;)V

    .line 70
    return-void
.end method

.method protected final b(Lcom/google/android/gms/games/a/a;)V
    .locals 5

    .prologue
    .line 46
    invoke-interface {p1}, Lcom/google/android/gms/games/a/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/z;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 54
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/z;->d:Lcom/google/android/gms/games/ui/n;

    const-class v4, Lcom/google/android/gms/games/ui/destination/leaderboards/DestinationLeaderboardScoreActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    const-string v3, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v3, "com.google.android.gms.games.GAME"

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 57
    const-string v3, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/z;->d:Lcom/google/android/gms/games/ui/n;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {v4, v0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;Lcom/google/android/gms/games/Game;)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/n;->startActivity(Landroid/content/Intent;)V

    .line 60
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 34
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d(Landroid/os/Bundle;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/z;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    .line 38
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/z;->ah()V

    .line 42
    :cond_0
    return-void
.end method
