.class public abstract Lcom/google/android/gms/games/ui/destination/players/m;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/games/c;
.implements Lcom/google/android/gms/games/ui/destination/players/v;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/play/headerlist/m;


# instance fields
.field private aA:J

.field private aB:J

.field private aC:J

.field private aD:Z

.field protected an:Lcom/google/android/gms/games/Player;

.field protected ao:Lcom/google/android/gms/games/Player;

.field protected ap:Z

.field protected aq:Ljava/lang/String;

.field protected ar:Lcom/google/android/gms/games/ui/destination/players/u;

.field protected as:Lcom/google/android/gms/games/ui/ai;

.field protected at:Z

.field private au:Lcom/google/android/gms/games/ui/destination/players/w;

.field private av:Landroid/view/View;

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:I


# direct methods
.method public constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    .line 78
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aw:Z

    .line 84
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ay:Z

    .line 92
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aC:J

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aD:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->at:Z

    .line 97
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ax:Z

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/m;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ay:Z

    return v0
.end method

.method private at()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->av:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->Q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->j()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/res/Resources;I)I

    move-result v0

    .line 407
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->av:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/m;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ay:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/m;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aB:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/destination/players/m;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aD:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/games/ui/destination/players/m;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aD:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/games/ui/destination/players/m;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aA:J

    return-wide v0
.end method

.method static synthetic l(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/players/w;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    return-object v0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public final Z()I
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 317
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->a(Landroid/content/Context;)I

    move-result v1

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0143

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 425
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->an:Lcom/google/android/gms/games/Player;

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->an:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    .line 429
    const-string v0, "PDBFragment"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 451
    :goto_0
    return-void

    .line 436
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ax:Z

    if-nez v0, :cond_1

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->an:Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    .line 438
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->au()V

    .line 441
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aq:Ljava/lang/String;

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aq:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 444
    const-string v0, "PDBFragment"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    goto :goto_0

    .line 450
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/m;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 461
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public final ak()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v0

    .line 498
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/m;->a(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    .line 499
    iget-object v2, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    .line 501
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 502
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/destination/b;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 503
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 504
    const v5, 0x7f0a00d5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 505
    const v6, 0x7f0a00d0

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 507
    iget-boolean v6, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aw:Z

    if-eqz v6, :cond_0

    cmpl-float v6, v0, v8

    if-nez v6, :cond_0

    .line 508
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aw:Z

    .line 511
    :cond_0
    invoke-static {v2, v9}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 512
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aw:Z

    .line 525
    :cond_1
    :goto_0
    invoke-static {v2, v9}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 526
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/destination/players/u;->a(F)V

    .line 533
    :goto_1
    return-void

    .line 513
    :cond_2
    iget-boolean v6, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aw:Z

    if-nez v6, :cond_1

    .line 514
    cmpl-float v6, v0, v8

    if-lez v6, :cond_3

    .line 516
    div-float v6, v0, v1

    sub-float v6, v7, v6

    .line 517
    invoke-static {v5, v3, v6}, Lcom/google/android/gms/games/ui/e/aj;->a(IIF)I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 520
    :cond_3
    invoke-virtual {v4, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 528
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 529
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 530
    div-float/2addr v0, v1

    .line 531
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-static {v0, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/u;->a(F)V

    goto :goto_1
.end method

.method protected final au()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/b;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/u;->a(Lcom/google/android/gms/games/Player;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->at()V

    goto :goto_0
.end method

.method protected av()V
    .locals 4

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->j()Landroid/content/res/Resources;

    move-result-object v0

    .line 146
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 153
    :goto_0
    new-instance v1, Lcom/google/android/gms/games/ui/ai;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ai;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->as:Lcom/google/android/gms/games/ui/ai;

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->as:Lcom/google/android/gms/games/ui/ai;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/ai;->c(Z)V

    .line 159
    const v1, 0x7f0b0143

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 160
    const v2, 0x7f0b0142

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 162
    const v3, 0x7f0b014a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 163
    sub-int v1, v2, v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 165
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->as:Lcom/google/android/gms/games/ui/ai;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ai;->f(I)V

    .line 166
    return-void

    .line 149
    :cond_0
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ax:Z

    invoke-direct {v1, v2, v3, p0}, Lcom/google/android/gms/games/ui/destination/players/u;-><init>(Landroid/content/Context;ZLcom/google/android/gms/games/ui/destination/players/v;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    goto :goto_0
.end method

.method public aw()V
    .locals 0

    .prologue
    .line 485
    return-void
.end method

.method public ax()V
    .locals 0

    .prologue
    .line 489
    return-void
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 328
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 330
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ax:Z

    invoke-direct {v0, v2, v3, p0}, Lcom/google/android/gms/games/ui/destination/players/u;-><init>(Landroid/content/Context;ZLcom/google/android/gms/games/ui/destination/players/v;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/destination/players/u;->a(I)I

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/w;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/games/ui/destination/players/u;->a(Lcom/google/android/gms/games/ui/bg;I)V

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/w;->a:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    new-instance v2, Lcom/google/android/gms/games/ui/destination/players/p;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/ui/destination/players/p;-><init>(Lcom/google/android/gms/games/ui/destination/players/m;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/destination/players/u;->a(Landroid/support/v7/widget/bx;)V

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/w;->a:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 367
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 368
    const/4 v2, -0x1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 369
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/w;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 373
    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/destination/h;->a(Landroid/content/Context;)I

    move-result v1

    .line 374
    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 375
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/destination/players/w;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    return-void
.end method

.method protected abstract b(Lcom/google/android/gms/common/api/t;)V
.end method

.method public final b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    .prologue
    .line 466
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_0

    .line 468
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aq:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    .line 475
    :goto_0
    return-void

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 382
    const v0, 0x7f04003b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 384
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 385
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 386
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 388
    :cond_0
    const v0, 0x7f0c0124

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->av:Landroid/view/View;

    .line 391
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->at()V

    .line 392
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v2, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 102
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->av()V

    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ax:Z

    if-eqz v0, :cond_4

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    .line 114
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->au()V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/destination/b;->c(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/m;)V

    .line 121
    iput v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->az:I

    .line 122
    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->az:I

    .line 126
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->az:I

    if-ne v6, v0, :cond_3

    .line 127
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/c/i;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->setAllowEnterTransitionOverlap(Z)V

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->az:I

    if-ne v6, v0, :cond_1

    const-wide/16 v0, 0x1c2

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aC:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aA:J

    const-wide/16 v0, 0x1c2

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aC:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aB:J

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/gms/games/ui/destination/players/m;->at:Z

    .line 128
    :cond_2
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 130
    :cond_3
    :goto_1
    return-void

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ao:Lcom/google/android/gms/games/Player;

    goto :goto_0

    .line 128
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->j()Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/players/w;->o()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->au:Lcom/google/android/gms/games/ui/destination/players/w;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/players/w;->o()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b()Landroid/widget/TextView;

    move-result-object v2

    const-string v1, "avatar"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/games/ui/d/e;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/d/e;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/d/e;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    new-instance v3, Landroid/transition/ArcMotion;

    invoke-direct {v3}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v1, v3}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aA:J

    invoke-virtual {v1, v4, v5}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    new-instance v3, Landroid/transition/ChangeImageTransform;

    invoke-direct {v3}, Landroid/transition/ChangeImageTransform;-><init>()V

    invoke-virtual {v3, v0}, Landroid/transition/ChangeImageTransform;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/destination/players/m;->aA:J

    invoke-virtual {v0, v4, v5}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    const-string v3, "level"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTransitionName(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/games/ui/d/g;

    invoke-direct {v3, v6}, Lcom/google/android/gms/games/ui/d/g;-><init>(Z)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/ui/d/g;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/games/ui/d/g;

    invoke-direct {v4, v7}, Lcom/google/android/gms/games/ui/d/g;-><init>(Z)V

    invoke-virtual {v4, v2}, Lcom/google/android/gms/games/ui/d/g;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v4

    new-array v5, v9, [Landroid/transition/Transition;

    aput-object v1, v5, v7

    aput-object v0, v5, v6

    aput-object v3, v5, v8

    invoke-static {v5}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v5}, Lcom/google/android/play/a/b;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/destination/b;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    new-array v5, v9, [Landroid/transition/Transition;

    aput-object v1, v5, v7

    aput-object v0, v5, v6

    aput-object v4, v5, v8

    invoke-static {v5}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1}, Lcom/google/android/play/a/b;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/b;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024e

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/m;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v4, 0x102000a

    invoke-virtual {v1, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/m;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v5, Lcom/google/android/gms/games/ui/destination/players/n;

    invoke-direct {v5, p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/n;-><init>(Lcom/google/android/gms/games/ui/destination/players/m;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/widget/TextView;)V

    invoke-virtual {v4, v5}, Lcom/google/android/gms/games/ui/destination/b;->a(Landroid/support/v4/app/bj;)V

    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/o;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/ui/destination/players/o;-><init>(Lcom/google/android/gms/games/ui/destination/players/m;Landroid/widget/TextView;)V

    invoke-virtual {v3, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    goto/16 :goto_1
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/m;->ap:Z

    .line 420
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/h;->f()V

    .line 421
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 413
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/h;->v()V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/m;->a(Lcom/google/android/gms/games/app/a;)V

    .line 415
    return-void
.end method
