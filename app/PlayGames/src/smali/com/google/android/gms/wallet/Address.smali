.class public final Lcom/google/android/gms/wallet/Address;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Z

.field k:Ljava/lang/String;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/wallet/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/Address;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/Address;->l:I

    .line 78
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput p1, p0, Lcom/google/android/gms/wallet/Address;->l:I

    .line 95
    iput-object p2, p0, Lcom/google/android/gms/wallet/Address;->a:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/wallet/Address;->b:Ljava/lang/String;

    .line 97
    iput-object p4, p0, Lcom/google/android/gms/wallet/Address;->c:Ljava/lang/String;

    .line 98
    iput-object p5, p0, Lcom/google/android/gms/wallet/Address;->d:Ljava/lang/String;

    .line 99
    iput-object p6, p0, Lcom/google/android/gms/wallet/Address;->e:Ljava/lang/String;

    .line 100
    iput-object p7, p0, Lcom/google/android/gms/wallet/Address;->f:Ljava/lang/String;

    .line 101
    iput-object p8, p0, Lcom/google/android/gms/wallet/Address;->g:Ljava/lang/String;

    .line 102
    iput-object p9, p0, Lcom/google/android/gms/wallet/Address;->h:Ljava/lang/String;

    .line 103
    iput-object p10, p0, Lcom/google/android/gms/wallet/Address;->i:Ljava/lang/String;

    .line 104
    iput-boolean p11, p0, Lcom/google/android/gms/wallet/Address;->j:Z

    .line 105
    iput-object p12, p0, Lcom/google/android/gms/wallet/Address;->k:Ljava/lang/String;

    .line 106
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/gms/wallet/Address;->l:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/a;->a(Lcom/google/android/gms/wallet/Address;Landroid/os/Parcel;)V

    .line 41
    return-void
.end method
