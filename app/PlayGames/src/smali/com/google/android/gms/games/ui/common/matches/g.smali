.class public final Lcom/google/android/gms/games/ui/common/matches/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/data/p;

.field final b:Lcom/google/android/gms/common/data/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/b;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/google/android/gms/common/data/p;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/p;

    .line 23
    new-instance v0, Lcom/google/android/gms/common/data/p;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    .line 28
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v5

    move v4, v0

    move-object v3, v1

    :goto_0
    if-ge v4, v5, :cond_4

    .line 33
    invoke-virtual {p1, v4}, Lcom/google/android/gms/common/data/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/InvitationEntity;

    .line 34
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/InvitationEntity;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v2

    .line 35
    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v6

    .line 40
    if-eqz v6, :cond_0

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->k()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 41
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v1, v3

    .line 32
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move-object v1, v0

    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 47
    if-eqz v1, :cond_2

    .line 48
    new-instance v3, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;-><init>(Ljava/util/ArrayList;)V

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    .line 53
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 54
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    .line 56
    :cond_3
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v3

    goto :goto_1

    .line 62
    :cond_4
    if-eqz v1, :cond_5

    .line 63
    new-instance v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;-><init>(Ljava/util/ArrayList;)V

    .line 64
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    .line 66
    :cond_5
    return-void
.end method
