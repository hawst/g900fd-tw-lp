.class public final Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/c/a/g;
.implements Lcom/google/android/gms/games/ui/c/a/j;
.implements Lcom/google/android/gms/games/ui/c/a/m;
.implements Lcom/google/android/gms/games/ui/c/a/p;
.implements Lcom/google/android/gms/games/ui/common/a/k;
.implements Lcom/google/android/gms/games/ui/common/a/l;
.implements Lcom/google/android/gms/games/ui/common/a/n;
.implements Lcom/google/android/gms/games/ui/common/matches/v;
.implements Lcom/google/android/gms/games/ui/common/requests/m;
.implements Lcom/google/android/gms/games/ui/destination/q;


# static fields
.field private static final z:[I


# instance fields
.field private A:Lcom/google/android/gms/games/ui/destination/matches/a;

.field private B:Lcom/google/android/gms/games/ui/destination/requests/a;

.field private C:Lcom/google/android/gms/games/ui/common/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x3

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->z:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 85
    const v0, 0x7f040035

    const v1, 0x7f11000c

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZZ)V

    .line 87
    return-void
.end method


# virtual methods
.method protected final E()V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LAUNCHED_VIA_API"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.MAIN_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->finish()V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->E()V

    goto :goto_0
.end method

.method public final Q()Lcom/google/android/gms/games/ui/c/a/f;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->A:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->A:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final af()Lcom/google/android/gms/games/ui/c/a/l;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->C:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method

.method public final ah()Lcom/google/android/gms/games/ui/c/a/i;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->A:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final ai()Lcom/google/android/gms/games/ui/c/a/o;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->B:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final c()[I
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->z:[I

    return-object v0
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 165
    const/4 v3, 0x0

    .line 166
    const/4 v2, -0x1

    .line 167
    const-string v4, "invitationsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 168
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 187
    :goto_0
    if-eqz v1, :cond_0

    .line 188
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 189
    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 190
    const-string v0, "com.google.android.gms.games.GAME_ID"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->startActivity(Landroid/content/Intent;)V

    .line 193
    :cond_0
    return-void

    .line 170
    :cond_1
    const-string v4, "myTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 171
    const-string v0, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 172
    goto :goto_0

    .line 173
    :cond_2
    const-string v4, "theirTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 174
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 175
    const/4 v0, 0x2

    goto :goto_0

    .line 176
    :cond_3
    const-string v4, "completedMatchesButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 177
    const-string v1, "com.google.android.gms.games.destination.SHOW_MULTIPLAYER_LIST"

    .line 178
    const/4 v0, 0x3

    goto :goto_0

    .line 179
    :cond_4
    const-string v4, "giftsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 180
    const-string v1, "com.google.android.gms.games.destination.SHOW_REQUEST_LIST"

    goto :goto_0

    .line 182
    :cond_5
    const-string v0, "wishesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 183
    const-string v0, "com.google.android.gms.games.destination.SHOW_REQUEST_LIST"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 184
    goto :goto_0

    :cond_6
    move v0, v2

    move-object v1, v3

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->B:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 93
    new-instance v0, Lcom/google/android/gms/games/ui/destination/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/matches/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->A:Lcom/google/android/gms/games/ui/destination/matches/a;

    .line 94
    new-instance v0, Lcom/google/android/gms/games/ui/destination/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/requests/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->B:Lcom/google/android/gms/games/ui/destination/requests/a;

    .line 95
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/common/a/b;-><init>(Lcom/google/android/gms/games/ui/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->C:Lcom/google/android/gms/games/ui/common/a/b;

    .line 96
    return-void
.end method

.method public final r_()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    const-string v0, "mobile_inbox"

    return-object v0
.end method

.method public final z_()Lcom/google/android/gms/games/ui/common/a/j;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity;->C:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method
