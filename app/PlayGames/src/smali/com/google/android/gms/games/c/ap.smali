.class public final Lcom/google/android/gms/games/c/ap;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7954
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7955
    iput v1, p0, Lcom/google/android/gms/games/c/ap;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/ap;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/ap;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ap;->G:I

    .line 7956
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 8039
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 8040
    iget v1, p0, Lcom/google/android/gms/games/c/ap;->a:I

    if-eqz v1, :cond_0

    .line 8041
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/ap;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8044
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 8045
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8048
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/ap;->c:I

    if-eqz v1, :cond_2

    .line 8049
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/ap;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8052
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 8053
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8056
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/c/ap;->e:I

    if-eqz v1, :cond_4

    .line 8057
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/games/c/ap;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8060
    :cond_4
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7902
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/ap;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/games/c/ap;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/android/gms/games/c/ap;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8019
    iget v0, p0, Lcom/google/android/gms/games/c/ap;->a:I

    if-eqz v0, :cond_0

    .line 8020
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/ap;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8022
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8023
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8025
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/ap;->c:I

    if-eqz v0, :cond_2

    .line 8026
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/ap;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8028
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 8029
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8031
    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/c/ap;->e:I

    if-eqz v0, :cond_4

    .line 8032
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/games/c/ap;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8034
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 8035
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7970
    if-ne p1, p0, :cond_1

    .line 8000
    :cond_0
    :goto_0
    return v0

    .line 7973
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ap;

    if-nez v2, :cond_2

    move v0, v1

    .line 7974
    goto :goto_0

    .line 7976
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ap;

    .line 7977
    iget v2, p0, Lcom/google/android/gms/games/c/ap;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/ap;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 7978
    goto :goto_0

    .line 7980
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 7981
    iget-object v2, p1, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 7982
    goto :goto_0

    .line 7984
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 7985
    goto :goto_0

    .line 7987
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/c/ap;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/ap;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 7988
    goto :goto_0

    .line 7990
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 7991
    iget-object v2, p1, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 7992
    goto :goto_0

    .line 7994
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 7995
    goto :goto_0

    .line 7997
    :cond_8
    iget v2, p0, Lcom/google/android/gms/games/c/ap;->e:I

    iget v3, p1, Lcom/google/android/gms/games/c/ap;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 7998
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8005
    iget v0, p0, Lcom/google/android/gms/games/c/ap;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8007
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8009
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/ap;->c:I

    add-int/2addr v0, v2

    .line 8010
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 8012
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/ap;->e:I

    add-int/2addr v0, v1

    .line 8013
    return v0

    .line 8007
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ap;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8010
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/ap;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
