.class public final Lcom/google/android/gms/games/ui/destination/api/ApiActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 17
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/api/ApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "com.google.android.gms.games.MIN_VERSION"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-le v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.MAIN_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.SHOW_UPGRADE_DIALOG"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    const-string v1, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v0, "GamesDestApi"

    const-string v1, "We don\'t have a valid target account key."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 23
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/api/ApiActivity;->isFinishing()Z

    move-result v0

    const-string v1, "The ApiActivity should be finishing once the API has been handled"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 25
    return-void

    .line 21
    :catch_0
    move-exception v0

    const-string v0, "GamesDestApi"

    const-string v1, "Cannot find our own package info in the PackageManager."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "GamesDestApi"

    const-string v1, "Cannot find our own package info in the PackageManager."

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_0
    const-string v0, "com.google.android.gms.games.SCREEN"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.MAIN_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_GAME_DETAIL"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v0, :cond_1

    const-string v4, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_3
    packed-switch v3, :pswitch_data_0

    :goto_4
    :pswitch_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "com.google.android.gms.games.GAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    if-nez v0, :cond_2

    const-string v0, "GamesDestApi"

    const-string v1, "We don\'t have the game object for a Game API intent."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_2
    const-string v4, "com.google.android.gms.games.GAME"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_3

    :pswitch_1
    const-string v0, "com.google.android.gms.games.TAB"

    const/16 v3, 0xa

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "com.google.android.gms.games.TAB"

    const/16 v3, 0xb

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "com.google.android.gms.games.TAB"

    const/16 v3, 0xc

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "com.google.android.gms.games.TAB"

    const/16 v3, 0xd

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_4

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_LEADERBOARD_SCORES"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.games.GAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    if-nez v0, :cond_3

    const-string v0, "GamesDestApi"

    const-string v1, "We don\'t have the game object for a Leaderboard Score API intent."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    :cond_3
    const-string v3, "com.google.android.gms.games.GAME"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v0, "GamesDestApi"

    const-string v1, "We don\'t have the leaderboard Id for a Leaderboard Score API intent."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    :cond_4
    const-string v3, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    goto/16 :goto_0

    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.VIEW_MY_PROFILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.NOTIFICATION_OPENED"

    const-string v3, "com.google.android.gms.games.NOTIFICATION_OPENED"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_5

    const-string v0, "GamesDestApi"

    const-string v1, "We don\'t have the other player object for profile comparison API intent."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    :cond_5
    const-string v3, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v0, v1

    goto/16 :goto_0

    :sswitch_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.SHOW_INBOX"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "GamesDestApi"

    const-string v1, "Player Detail is deprecated, going to Play Now instead."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    const-string v3, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.FORCE_RESOLVE_ACCOUNT_KEY"

    const-string v3, "com.google.android.gms.games.FORCE_RESOLVE_ACCOUNT_KEY"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.LAUNCHED_VIA_API"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x41a -> :sswitch_0
        0x41b -> :sswitch_0
        0x41c -> :sswitch_0
        0x41d -> :sswitch_1
        0x41e -> :sswitch_0
        0x41f -> :sswitch_0
        0x44c -> :sswitch_5
        0x44d -> :sswitch_2
        0x44e -> :sswitch_3
        0x4b0 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x41b
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
