.class final Lcom/google/android/gms/games/ui/e/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/dm;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/e/ab;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/e/ab;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/e/ab;->f:Z

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/e/ab;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/e/ab;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->c()V

    .line 224
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/e/ab;->g:Z

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/e/ab;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/e/ab;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 254
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iput-object p1, v0, Lcom/google/android/gms/games/ui/e/ab;->c:Ljava/lang/String;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/e/ab;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ad;->a:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/e/ab;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 264
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
