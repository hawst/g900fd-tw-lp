.class public Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/common/data/DataHolder;

.field public final c:I

.field public final d:I

.field public final e:J

.field public f:Lcom/google/android/gms/cloudsave/Entity$Key;

.field public g:J

.field public h:J

.field public final i:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/h;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/data/DataHolder;IIJLcom/google/android/gms/cloudsave/Entity$Key;JJLcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->a:I

    .line 83
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 84
    iput p3, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->c:I

    .line 85
    iput p4, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->d:I

    .line 86
    iput-wide p5, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->e:J

    .line 87
    iput-object p7, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->f:Lcom/google/android/gms/cloudsave/Entity$Key;

    .line 88
    iput-wide p8, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->g:J

    .line 89
    iput-wide p10, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->h:J

    .line 90
    iput-object p12, p0, Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;->i:Lcom/google/android/gms/common/data/DataHolder;

    .line 91
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 63
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/h;->a(Lcom/google/android/gms/cloudsave/internal/ResolveConflictRequest;Landroid/os/Parcel;I)V

    .line 64
    return-void
.end method
