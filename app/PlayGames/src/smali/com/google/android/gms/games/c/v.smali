.class public final Lcom/google/android/gms/games/c/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/c/aa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5411
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5412
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/v;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/v;->G:I

    .line 5413
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 5469
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 5470
    iget v1, p0, Lcom/google/android/gms/games/c/v;->a:I

    if-eqz v1, :cond_0

    .line 5471
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/v;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5474
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-eqz v1, :cond_1

    .line 5475
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5478
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5354
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/v;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/aa;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5458
    iget v0, p0, Lcom/google/android/gms/games/c/v;->a:I

    if-eqz v0, :cond_0

    .line 5459
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/v;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5461
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-eqz v0, :cond_1

    .line 5462
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5464
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 5465
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5424
    if-ne p1, p0, :cond_1

    .line 5443
    :cond_0
    :goto_0
    return v0

    .line 5427
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 5428
    goto :goto_0

    .line 5430
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/v;

    .line 5431
    iget v2, p0, Lcom/google/android/gms/games/c/v;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/v;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5432
    goto :goto_0

    .line 5434
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-nez v2, :cond_4

    .line 5435
    iget-object v2, p1, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5436
    goto :goto_0

    .line 5439
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    iget-object v3, p1, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5440
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5448
    iget v0, p0, Lcom/google/android/gms/games/c/v;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5450
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 5452
    return v0

    .line 5450
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/v;->b:Lcom/google/android/gms/games/c/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/aa;->hashCode()I

    move-result v0

    goto :goto_0
.end method
