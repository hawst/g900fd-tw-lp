.class public final Lcom/google/android/gms/games/ui/common/matches/h;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/matches/i;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/i;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/matches/h;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/i;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/i;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/i;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->g:Lcom/google/android/gms/games/ui/common/matches/i;

    .line 74
    sget v0, Lcom/google/android/gms/h;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/matches/h;->d(II)V

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/h;)Lcom/google/android/gms/games/ui/common/matches/i;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->g:Lcom/google/android/gms/games/ui/common/matches/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 1

    .prologue
    .line 90
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/q;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    .line 92
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/h;->h:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/h;->i:Ljava/lang/String;

    .line 86
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/j;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/j;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 166
    sget v0, Lcom/google/android/gms/g;->X:I

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x3

    return v0
.end method
