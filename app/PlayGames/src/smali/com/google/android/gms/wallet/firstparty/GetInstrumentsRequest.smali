.class public final Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:[I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/wallet/firstparty/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/firstparty/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;-><init>(I[I)V

    .line 58
    return-void
.end method

.method constructor <init>(I[I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;->b:I

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;->a:[I

    .line 54
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;->b:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/firstparty/a;->a(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
