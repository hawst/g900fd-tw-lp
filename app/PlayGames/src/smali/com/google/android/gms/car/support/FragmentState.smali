.class final Lcom/google/android/gms/car/support/FragmentState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field final c:Z

.field final d:I

.field final e:I

.field final f:Ljava/lang/String;

.field final g:Z

.field final h:Z

.field final i:Landroid/os/Bundle;

.field j:Landroid/os/Bundle;

.field k:Lcom/google/android/gms/car/support/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/gms/car/support/r;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->a:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->b:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->c:Z

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->d:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->e:I

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->f:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->g:Z

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->h:Z

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    .line 88
    return-void

    :cond_0
    move v0, v2

    .line 80
    goto :goto_0

    :cond_1
    move v0, v2

    .line 84
    goto :goto_1

    :cond_2
    move v1, v2

    .line 85
    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/support/j;Lcom/google/android/gms/car/support/Fragment;)Lcom/google/android/gms/car/support/Fragment;
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    .line 118
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/j;->e()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 99
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/j;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/j;->e()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->b:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/car/support/Fragment;->a(ILcom/google/android/gms/car/support/Fragment;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->c:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->o:Z

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->q:Z

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->d:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->w:I

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->e:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->x:I

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->g:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->B:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->h:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->A:Z

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p1, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    .line 115
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Instantiated fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    goto/16 :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->g:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->h:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 138
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v0, v2

    .line 134
    goto :goto_1

    :cond_2
    move v1, v2

    .line 135
    goto :goto_2
.end method
