.class public Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/people/identity/models/Person;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;


# instance fields
.field A:Ljava/util/List;

.field B:Ljava/lang/String;

.field C:Ljava/util/List;

.field D:Ljava/util/List;

.field E:Ljava/util/List;

.field F:Ljava/util/List;

.field G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

.field H:Ljava/util/List;

.field I:Ljava/util/List;

.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/lang/String;

.field f:Ljava/util/List;

.field g:Ljava/util/List;

.field h:Ljava/util/List;

.field i:Ljava/util/List;

.field j:Ljava/util/List;

.field k:Ljava/lang/String;

.field l:Ljava/util/List;

.field m:Ljava/util/List;

.field n:Ljava/lang/String;

.field o:Ljava/util/List;

.field p:Ljava/util/List;

.field q:Ljava/lang/String;

.field r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

.field s:Ljava/util/List;

.field t:Ljava/util/List;

.field u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

.field v:Ljava/util/List;

.field w:Ljava/util/List;

.field x:Ljava/util/List;

.field y:Ljava/util/List;

.field z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->b:I

    .line 245
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    .line 246
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    .line 287
    iput p2, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->b:I

    .line 288
    iput-object p3, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->c:Ljava/util/List;

    .line 289
    iput-object p4, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    .line 290
    iput-object p5, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->e:Ljava/lang/String;

    .line 291
    iput-object p6, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->f:Ljava/util/List;

    .line 292
    iput-object p7, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->g:Ljava/util/List;

    .line 293
    iput-object p8, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->h:Ljava/util/List;

    .line 294
    iput-object p9, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->i:Ljava/util/List;

    .line 295
    iput-object p10, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    .line 296
    iput-object p11, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->k:Ljava/lang/String;

    .line 297
    iput-object p12, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    .line 298
    iput-object p13, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->m:Ljava/util/List;

    .line 299
    iput-object p14, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->n:Ljava/lang/String;

    .line 300
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    .line 301
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    .line 302
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->q:Ljava/lang/String;

    .line 303
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    .line 304
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->s:Ljava/util/List;

    .line 305
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    .line 306
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    .line 307
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    .line 308
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    .line 309
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->x:Ljava/util/List;

    .line 310
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    .line 311
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    .line 312
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->A:Ljava/util/List;

    .line 313
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->B:Ljava/lang/String;

    .line 314
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    .line 315
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->D:Ljava/util/List;

    .line 316
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->E:Ljava/util/List;

    .line 317
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->F:Ljava/util/List;

    .line 318
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    .line 319
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    .line 320
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    .line 321
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1679
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1684
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/people/identity/internal/models/a;->a(Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;Landroid/os/Parcel;I)V

    .line 1685
    return-void
.end method
