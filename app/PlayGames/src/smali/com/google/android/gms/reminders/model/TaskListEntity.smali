.class public Lcom/google/android/gms/reminders/model/TaskListEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/reminders/model/TaskList;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private final b:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/reminders/model/i;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/model/TaskListEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/Integer;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/reminders/model/TaskListEntity;->b:Ljava/lang/Integer;

    .line 58
    iput p1, p0, Lcom/google/android/gms/reminders/model/TaskListEntity;->a:I

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/reminders/model/TaskList;)V
    .locals 1

    .prologue
    .line 43
    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/TaskList;->c()Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/reminders/model/TaskListEntity;-><init>(Ljava/lang/Integer;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/reminders/model/TaskListEntity;-><init>(ILjava/lang/Integer;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskListEntity;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 117
    instance-of v0, p1, Lcom/google/android/gms/reminders/model/TaskList;

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 124
    :goto_0
    return v0

    .line 120
    :cond_0
    if-ne p0, p1, :cond_1

    .line 121
    const/4 v0, 0x1

    goto :goto_0

    .line 124
    :cond_1
    check-cast p1, Lcom/google/android/gms/reminders/model/TaskList;

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/TaskList;->c()Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/TaskList;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g_()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 129
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskListEntity;->b:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 91
    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/model/i;->a(Lcom/google/android/gms/reminders/model/TaskListEntity;Landroid/os/Parcel;)V

    .line 92
    return-void
.end method
