.class public final Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/games/ak;


# static fields
.field private static A:Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;

.field private static final B:Ljava/lang/Object;


# instance fields
.field private C:Lcom/google/android/gms/games/ui/e/ab;

.field z:Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->B:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    const v0, 0x7f040034

    const v1, 0x7f110011

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 60
    return-void
.end method

.method static d(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 207
    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    sget-object v2, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->A:Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;

    .line 209
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    if-eqz v2, :cond_0

    .line 211
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-object v0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 211
    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->z:Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 183
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 184
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->d()V

    .line 176
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 177
    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 2

    .prologue
    .line 193
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GPG_overflowMenu"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 188
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->getIntent()Landroid/content/Intent;

    .line 66
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const v1, 0x7f0c011c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->z:Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->z:Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 77
    new-instance v0, Lcom/google/android/gms/games/ui/e/ab;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->z:Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;

    const v2, 0x7f0f0195

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/e/ab;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/af;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->a(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->b()V

    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 123
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    const-string v0, "com.google.android.gms.games.ui.destination.games.SUGGESTION_SELECTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/games/GamesSuggestionProvider;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    iget-object v2, v1, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/games/ui/e/ab;->g:Z

    iget-object v2, v1, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2, v0, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    iget-object v2, v1, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    iput-boolean v3, v1, Lcom/google/android/gms/games/ui/e/ab;->g:Z

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onPause()V

    .line 100
    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 101
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->A:Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;

    .line 102
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onResume()V

    .line 92
    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 93
    :try_start_0
    sput-object p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->A:Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;

    .line 94
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->b(Landroid/os/Bundle;)V

    .line 141
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->e()Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onStart()V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/e/ab;->d:Z

    .line 87
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onStop()V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;->C:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->a()V

    .line 109
    return-void
.end method
