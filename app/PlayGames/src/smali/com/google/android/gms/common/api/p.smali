.class public final Lcom/google/android/gms/common/api/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/List;

    .line 39
    invoke-interface {p1}, Lcom/google/android/gms/common/api/t;->a()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->b:Landroid/os/Looper;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/n;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/common/api/n;

    iget-object v1, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/common/api/p;->b:Landroid/os/Looper;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/n;-><init>(Ljava/util/List;Landroid/os/Looper;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/common/api/r;

    iget-object v1, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/r;-><init>(I)V

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    return-object v0
.end method
