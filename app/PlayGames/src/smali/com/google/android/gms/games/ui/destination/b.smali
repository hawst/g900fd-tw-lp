.class public abstract Lcom/google/android/gms/games/ui/destination/b;
.super Lcom/google/android/gms/games/ui/destination/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/gms/people/accountswitcherview/an;
.implements Lcom/google/android/gms/people/accountswitcherview/aq;
.implements Lcom/google/android/gms/people/accountswitcherview/d;
.implements Lcom/google/android/gms/people/accountswitcherview/e;


# static fields
.field private static final z:Ljava/lang/String;


# instance fields
.field private A:Landroid/support/v4/widget/DrawerLayout;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/ListView;

.field private D:Lcom/google/android/gms/games/ui/destination/s;

.field private E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

.field private F:Lcom/google/android/gms/people/accountswitcherview/i;

.field private G:Lcom/google/android/gms/people/accountswitcherview/r;

.field private H:Lcom/google/android/gms/people/model/f;

.field private I:Ljava/lang/String;

.field private J:Landroid/support/v4/app/a;

.field private K:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 101
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 117
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZZ)V

    .line 136
    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/games/ui/destination/g;-><init>(IIZZ)V

    .line 156
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 159
    return-void

    .line 156
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static K()V
    .locals 0

    .prologue
    .line 529
    return-void
.end method

.method static synthetic N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    return-object v0
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    invoke-virtual {v0}, Landroid/support/v4/app/a;->a()V

    .line 479
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/b;)Landroid/support/v4/app/a;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    return-object v0
.end method

.method private af()V
    .locals 3

    .prologue
    .line 730
    new-instance v0, Lcom/google/android/gms/people/f;

    invoke-direct {v0}, Lcom/google/android/gms/people/f;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/people/f;->b()Lcom/google/android/gms/people/f;

    move-result-object v0

    .line 732
    sget-object v1, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/people/f;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/c;-><init>(Lcom/google/android/gms/games/ui/destination/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 739
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/b;)Lcom/google/android/gms/games/ui/destination/s;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    return-object v0
.end method


# virtual methods
.method protected final A()V
    .locals 2

    .prologue
    .line 273
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/g;->A()V

    .line 278
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->c()V

    .line 282
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 283
    return-void
.end method

.method public E()V
    .locals 0

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->onBackPressed()V

    .line 336
    return-void
.end method

.method protected F()Z
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    return v0
.end method

.method public final G()V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->f(Landroid/view/View;)V

    .line 502
    :cond_0
    return-void
.end method

.method public final H()V
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/b;->K:Z

    .line 506
    return-void
.end method

.method public I()V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method public J()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 512
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/b;->K:Z

    if-eqz v0, :cond_0

    .line 513
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/b;->K:Z

    .line 515
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/CharSequence;)V

    .line 516
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->a(Lcom/google/android/gms/games/ui/n;)V

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b(I)V

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    .line 526
    :cond_1
    return-void
.end method

.method public final L()V
    .locals 5

    .prologue
    .line 668
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 669
    const-string v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 671
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->startActivity(Landroid/content/Intent;)V

    .line 672
    return-void
.end method

.method public final M()V
    .locals 5

    .prologue
    .line 676
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SYNC_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 677
    const-string v1, "settings"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 678
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->startActivity(Landroid/content/Intent;)V

    .line 679
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/g;->a(Landroid/os/Bundle;)V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->u()V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 188
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->I:Ljava/lang/String;

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/b;->af()V

    .line 193
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/b;->x:Z

    if-eqz v0, :cond_0

    .line 194
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/b;->x:Z

    .line 198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 201
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->startActivity(Landroid/content/Intent;)V

    .line 202
    invoke-virtual {p0, v2, v2}, Lcom/google/android/gms/games/ui/destination/b;->overridePendingTransition(II)V

    .line 204
    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/api/u;)V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/g;->a(Lcom/google/android/gms/common/api/u;)V

    .line 240
    new-instance v0, Lcom/google/android/gms/people/t;

    invoke-direct {v0}, Lcom/google/android/gms/people/t;-><init>()V

    .line 241
    const/16 v1, 0x76

    iput v1, v0, Lcom/google/android/gms/people/t;->a:I

    .line 242
    sget-object v1, Lcom/google/android/gms/people/p;->c:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/t;->a()Lcom/google/android/gms/people/s;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/common/api/u;->a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/u;

    .line 243
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 4

    .prologue
    .line 683
    invoke-virtual {p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v0

    .line 684
    if-nez v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 688
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 693
    :cond_2
    sget-object v1, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown navigation mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/people/g;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 745
    invoke-interface {p1}, Lcom/google/android/gms/people/g;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 746
    invoke-interface {p1}, Lcom/google/android/gms/people/g;->E_()Lcom/google/android/gms/people/model/f;

    move-result-object v3

    .line 748
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 749
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 753
    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/model/f;->b(I)Lcom/google/android/gms/people/model/e;

    move-result-object v1

    .line 754
    invoke-virtual {v3}, Lcom/google/android/gms/people/model/f;->a()I

    move-result v5

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    .line 755
    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/model/f;->b(I)Lcom/google/android/gms/people/model/e;

    move-result-object v0

    .line 756
    invoke-interface {v0}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/games/ui/destination/b;->I:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 754
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 759
    :cond_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_1

    .line 762
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    if-nez v0, :cond_2

    .line 765
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    .line 766
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/b;->F:Lcom/google/android/gms/people/accountswitcherview/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/r;->a(Lcom/google/android/gms/people/accountswitcherview/i;)V

    .line 767
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/r;->a()V

    .line 768
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/r;->b()V

    .line 770
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/r;->a(Ljava/util/List;)V

    .line 771
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/e;)V

    .line 774
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->H:Lcom/google/android/gms/people/model/f;

    if-eqz v0, :cond_3

    .line 775
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->H:Lcom/google/android/gms/people/model/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/f;->f_()V

    .line 777
    :cond_3
    iput-object v3, p0, Lcom/google/android/gms/games/ui/destination/b;->H:Lcom/google/android/gms/people/model/f;

    .line 787
    :cond_4
    :goto_2
    return-void

    .line 779
    :cond_5
    sget-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "onOwnersLoaded - failed to load owners status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    if-eqz v3, :cond_4

    .line 784
    invoke-virtual {v3}, Lcom/google/android/gms/people/model/f;->f_()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/people/model/e;)V
    .locals 2

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 628
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    .line 630
    invoke-interface {p1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/e;)V

    .line 636
    sget-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    const-string v1, "onChildClick - Trying to sign-in with an account we are already signed into..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/people/model/e;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/b;->x:Z

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->G()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->c()V

    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->v:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->l()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    .line 641
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 703
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 704
    const v1, 0x7f0c0245

    if-eq v0, v1, :cond_0

    const v1, 0x7f0c00d0

    if-eq v0, v1, :cond_0

    const v1, 0x7f0c0278

    if-ne v0, v1, :cond_1

    .line 707
    :cond_0
    const/16 v0, 0x64

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Lcom/google/android/gms/games/ui/destination/b;I)V

    .line 711
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/s;->notifyDataSetChanged()V

    .line 712
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->G()V

    .line 724
    :goto_0
    return-void

    .line 713
    :cond_1
    const v1, 0x7f0c0277

    if-ne v0, v1, :cond_3

    .line 715
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v0

    .line 716
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 719
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b(I)V

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    goto :goto_0

    .line 716
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 722
    :cond_3
    sget-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    .line 166
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/g;->onCreate(Landroid/os/Bundle;)V

    .line 169
    const v0, 0x7f0c00fb

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    const-string v1, "Missing \'drawer_container\' in activity layout"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->a()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lcom/google/android/gms/games/ui/destination/e;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/games/ui/destination/e;-><init>(Lcom/google/android/gms/games/ui/destination/b;B)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/support/v4/widget/p;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/q;

    const/4 v1, 0x3

    iput v1, v0, Landroid/support/v4/widget/q;->a:I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/support/v4/widget/q;->width:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/support/v4/app/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {v0, p0, v1}, Landroid/support/v4/app/a;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    instance-of v1, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f0f0098

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/b;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->A:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->I()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/b;->Q()V

    .line 170
    :cond_1
    const v0, 0x7f0c027f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/i;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/accountswitcherview/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->F:Lcom/google/android/gms/people/accountswitcherview/i;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040026

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    new-instance v1, Lcom/google/android/gms/games/ui/destination/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/f;-><init>(Lcom/google/android/gms/games/ui/destination/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/accountswitcherview/ar;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/common/api/t;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->F:Lcom/google/android/gms/people/accountswitcherview/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/accountswitcherview/i;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/accountswitcherview/an;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/accountswitcherview/aq;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/gms/games/ui/destination/s;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/destination/s;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->C:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 176
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->F:Lcom/google/android/gms/people/accountswitcherview/i;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->F:Lcom/google/android/gms/people/accountswitcherview/i;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/i;->a()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/r;->c()V

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->H:Lcom/google/android/gms/people/model/f;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->H:Lcom/google/android/gms/people/model/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/f;->f_()V

    .line 261
    :cond_2
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/g;->onDestroy()V

    .line 262
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    if-eqz v0, :cond_0

    .line 610
    add-int/lit8 v0, p3, -0x1

    .line 611
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->E:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/accountswitcherview/r;->a(I)Lcom/google/android/gms/people/model/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/e;)V

    .line 613
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/r;->a(I)Lcom/google/android/gms/people/model/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/people/model/e;)V

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 615
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->L()V

    goto :goto_0

    .line 616
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->G:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 617
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->M()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 292
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/b;->J:Landroid/support/v4/app/a;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/a;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    :goto_0
    return v0

    .line 297
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 327
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/g;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 304
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->E()V

    goto :goto_0

    .line 297
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/g;->onPostCreate(Landroid/os/Bundle;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/b;->Q()V

    .line 212
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/g;->onResume()V

    .line 231
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/b;->Q()V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/b;->D:Lcom/google/android/gms/games/ui/destination/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/s;->notifyDataSetChanged()V

    .line 235
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/g;->onStart()V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 221
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/b;->af()V

    .line 224
    :cond_0
    return-void
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 794
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/g;->u()V

    .line 795
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 796
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 797
    sget-object v0, Lcom/google/android/gms/games/ui/destination/b;->z:Ljava/lang/String;

    const-string v1, "updateInboxCount - trying to update count when not connected..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    :goto_0
    return-void

    .line 802
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/l;->d(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/d;-><init>(Lcom/google/android/gms/games/ui/destination/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
