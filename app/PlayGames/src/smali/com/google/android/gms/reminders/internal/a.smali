.class public Lcom/google/android/gms/reminders/internal/a;
.super Lcom/google/android/gms/common/data/g;
.source "SourceFile"


# virtual methods
.method protected final b(Ljava/lang/String;)J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final c(Ljava/lang/String;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final f(Ljava/lang/String;)F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->f(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method protected final g(Ljava/lang/String;)D
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->g(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method protected final k(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/gms/reminders/internal/a;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->b(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method protected final l(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/android/gms/reminders/internal/a;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected final m(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/google/android/gms/reminders/internal/a;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/g;->g(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method
