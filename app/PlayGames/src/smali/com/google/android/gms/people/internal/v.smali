.class final Lcom/google/android/gms/people/internal/v;
.super Lcom/google/android/gms/common/internal/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/l;


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/internal/m;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Landroid/os/ParcelFileDescriptor;

.field private final e:Z

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;ZII)V
    .locals 0

    .prologue
    .line 977
    iput-object p1, p0, Lcom/google/android/gms/people/internal/v;->b:Lcom/google/android/gms/people/internal/m;

    .line 978
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    .line 979
    iput-object p3, p0, Lcom/google/android/gms/people/internal/v;->c:Lcom/google/android/gms/common/api/Status;

    .line 980
    iput-object p4, p0, Lcom/google/android/gms/people/internal/v;->d:Landroid/os/ParcelFileDescriptor;

    .line 981
    iput-boolean p5, p0, Lcom/google/android/gms/people/internal/v;->e:Z

    .line 982
    iput p6, p0, Lcom/google/android/gms/people/internal/v;->f:I

    .line 983
    iput p7, p0, Lcom/google/android/gms/people/internal/v;->g:I

    .line 984
    return-void
.end method


# virtual methods
.method public final F_()Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/google/android/gms/people/internal/v;->d:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/android/gms/people/internal/v;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 966
    check-cast p1, Lcom/google/android/gms/common/api/m;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 995
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/v;->f_()V

    .line 996
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1015
    iget v0, p0, Lcom/google/android/gms/people/internal/v;->f:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1020
    iget v0, p0, Lcom/google/android/gms/people/internal/v;->g:I

    return v0
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/google/android/gms/people/internal/v;->d:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/google/android/gms/people/internal/v;->d:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1028
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
