.class public final Lcom/google/android/gms/common/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/a/a/c;
.implements Lcom/google/android/gms/common/a/a/d;


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    .line 98
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/a/a/a;-><init>(Landroid/content/Intent;)V

    .line 91
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;
    .locals 5

    .prologue
    .line 24
    const-string v0, "People qualified ID"

    invoke-static {p1, v0}, Lcom/google/android/gms/people/internal/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic a(Ljava/util/List;)Lcom/google/android/gms/common/a/a/d;
    .locals 4

    .prologue
    .line 24
    if-nez p1, :cond_1

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    instance-of v1, v0, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/ArrayList;

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    goto :goto_1

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/String;)Lcom/google/android/gms/common/a/a/d;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 373
    iget-object v1, p0, Lcom/google/android/gms/common/a/a/a;->a:Landroid/content/Intent;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const-string v0, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    const-string v0, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v2

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
