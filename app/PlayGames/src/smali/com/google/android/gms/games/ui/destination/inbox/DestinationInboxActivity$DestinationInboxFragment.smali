.class public final Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;
.super Lcom/google/android/gms/games/ui/destination/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ar;
.implements Lcom/google/android/gms/games/ui/as;
.implements Lcom/google/android/gms/games/ui/at;
.implements Lcom/google/android/gms/games/ui/e/m;


# static fields
.field private static final g:[Lcom/google/android/gms/games/ui/e/ai;

.field private static final h:Lcom/google/android/gms/games/ui/e/ah;


# instance fields
.field private i:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 215
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/games/ui/e/ai;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/common/matches/r;

    const v4, 0x7f0f0151

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/common/requests/k;

    const v4, 0x7f0f018e

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/common/a/i;

    const v4, 0x7f0f017e

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    .line 226
    new-instance v0, Lcom/google/android/gms/games/ui/e/ah;

    sget-object v1, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/e/ah;-><init>([Lcom/google/android/gms/games/ui/e/ai;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    const v1, 0x7f040096

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/i;-><init>(Lcom/google/android/gms/games/ui/e/ah;I)V

    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->i:[I

    .line 234
    return-void

    .line 230
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;)[I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->i:[I

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->f:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/l;->d(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/inbox/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/inbox/a;-><init>(Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 279
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x1

    return v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    const-string v0, "DestinationInboxFragment"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :goto_0
    return-void

    .line 290
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->b(Lcom/google/android/gms/common/api/t;)V

    .line 255
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->u()V

    .line 263
    return-void
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 331
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->i:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->i:[I

    aget v0, v0, p1

    .line 334
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->b(I)I

    move-result v0

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->ai()Ljava/util/ArrayList;

    move-result-object v2

    .line 320
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 321
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 322
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 323
    instance-of v4, v0, Lcom/google/android/gms/games/ui/as;

    if-eqz v4, :cond_0

    .line 324
    check-cast v0, Lcom/google/android/gms/games/ui/as;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/as;->d()V

    .line 321
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 327
    :cond_1
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 238
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->d(Landroid/os/Bundle;)V

    .line 240
    if-nez p1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 242
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 244
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 246
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->ai()Ljava/util/ArrayList;

    move-result-object v2

    .line 308
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 309
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 310
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 311
    instance-of v4, v0, Lcom/google/android/gms/games/ui/at;

    if-eqz v4, :cond_0

    .line 312
    check-cast v0, Lcom/google/android/gms/games/ui/at;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/at;->e()V

    .line 309
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 315
    :cond_1
    return-void
.end method

.method public final n_()V
    .locals 5

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;->ai()Ljava/util/ArrayList;

    move-result-object v2

    .line 296
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 297
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 298
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 299
    instance-of v4, v0, Lcom/google/android/gms/games/ui/ar;

    if-eqz v4, :cond_0

    .line 300
    check-cast v0, Lcom/google/android/gms/games/ui/ar;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ar;->n_()V

    .line 297
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 303
    :cond_1
    return-void
.end method
