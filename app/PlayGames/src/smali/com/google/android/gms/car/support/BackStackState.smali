.class final Lcom/google/android/gms/car/support/BackStackState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:[I

.field final b:I

.field final c:I

.field final d:Ljava/lang/String;

.field final e:I

.field final f:I

.field final g:Ljava/lang/CharSequence;

.field final h:I

.field final i:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/google/android/gms/car/support/e;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/BackStackState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/BackStackState;->b:I

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/BackStackState;->c:I

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->d:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/BackStackState;->e:I

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/BackStackState;->f:I

    .line 93
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->g:Ljava/lang/CharSequence;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/BackStackState;->h:I

    .line 95
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->i:Ljava/lang/CharSequence;

    .line 96
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/support/n;)Lcom/google/android/gms/car/support/c;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 99
    new-instance v6, Lcom/google/android/gms/car/support/c;

    invoke-direct {v6, p1}, Lcom/google/android/gms/car/support/c;-><init>(Lcom/google/android/gms/car/support/n;)V

    move v1, v2

    move v0, v2

    .line 102
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 103
    new-instance v7, Lcom/google/android/gms/car/support/d;

    invoke-direct {v7}, Lcom/google/android/gms/car/support/d;-><init>()V

    .line 104
    iget-object v3, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v0, v3, v0

    iput v0, v7, Lcom/google/android/gms/car/support/d;->c:I

    .line 105
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Instantiate "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " op #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " base fragment #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    aget v5, v5, v4

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    .line 108
    if-ltz v0, :cond_2

    .line 109
    iget-object v4, p1, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 110
    iput-object v0, v7, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 114
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Lcom/google/android/gms/car/support/d;->e:I

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Lcom/google/android/gms/car/support/d;->f:I

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Lcom/google/android/gms/car/support/d;->g:I

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v5, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Lcom/google/android/gms/car/support/d;->h:I

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v3, v5, 0x1

    aget v8, v0, v5

    .line 119
    if-lez v8, :cond_3

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v7, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    move v4, v2

    .line 121
    :goto_2
    if-ge v4, v8, :cond_3

    .line 122
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Instantiate "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " set remove fragment #"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    aget v9, v9, v3

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    add-int/lit8 v5, v3, 0x1

    aget v3, v9, v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 125
    iget-object v3, v7, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v3, v5

    goto :goto_2

    .line 112
    :cond_2
    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    goto :goto_1

    .line 128
    :cond_3
    invoke-virtual {v6, v7}, Lcom/google/android/gms/car/support/c;->a(Lcom/google/android/gms/car/support/d;)V

    .line 129
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    .line 130
    goto/16 :goto_0

    .line 131
    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->b:I

    iput v0, v6, Lcom/google/android/gms/car/support/c;->i:I

    .line 132
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->c:I

    iput v0, v6, Lcom/google/android/gms/car/support/c;->j:I

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->d:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/car/support/c;->m:Ljava/lang/String;

    .line 134
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->e:I

    iput v0, v6, Lcom/google/android/gms/car/support/c;->o:I

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/google/android/gms/car/support/c;->k:Z

    .line 136
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->f:I

    iput v0, v6, Lcom/google/android/gms/car/support/c;->p:I

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->g:Ljava/lang/CharSequence;

    iput-object v0, v6, Lcom/google/android/gms/car/support/c;->q:Ljava/lang/CharSequence;

    .line 138
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->h:I

    iput v0, v6, Lcom/google/android/gms/car/support/c;->r:I

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->i:Ljava/lang/CharSequence;

    iput-object v0, v6, Lcom/google/android/gms/car/support/c;->s:Ljava/lang/CharSequence;

    .line 140
    invoke-virtual {v6}, Lcom/google/android/gms/car/support/c;->a()V

    .line 141
    return-object v6
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 152
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->g:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 158
    iget v0, p0, Lcom/google/android/gms/car/support/BackStackState;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/car/support/BackStackState;->i:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 160
    return-void
.end method
