.class public final Lcom/google/android/gms/games/ui/common/matches/t;
.super Lcom/google/android/gms/games/ui/c/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/r;

    .line 395
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/r;->f(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/n;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/gms/games/ui/aq;

    if-eqz v1, :cond_0

    .line 397
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/r;->g(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/n;

    .line 399
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 415
    const-string v0, "MultiplayerInboxFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :goto_0
    return-void

    .line 401
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->aj:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/r;->b(Z)V

    goto :goto_0

    .line 408
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/r;->as()V

    goto :goto_0

    .line 399
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
