.class public Lcom/google/android/gms/cloudsave/internal/QueryRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/cloudsave/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/f;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/QueryRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/cloudsave/Query;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/QueryRequest;->a:I

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/internal/QueryRequest;->b:Lcom/google/android/gms/cloudsave/Query;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/f;->a(Lcom/google/android/gms/cloudsave/internal/QueryRequest;Landroid/os/Parcel;I)V

    .line 38
    return-void
.end method
