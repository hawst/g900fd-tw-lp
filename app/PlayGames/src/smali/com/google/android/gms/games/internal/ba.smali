.class public final Lcom/google/android/gms/games/internal/ba;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/internal/t;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/common/internal/t;

    const-string v1, "Games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/gms/common/internal/t;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 61
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 69
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/gms/games/internal/ba;->a:Lcom/google/android/gms/common/internal/t;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void
.end method
