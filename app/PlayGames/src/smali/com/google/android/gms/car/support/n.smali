.class final Lcom/google/android/gms/car/support/n;
.super Lcom/google/android/gms/car/support/m;
.source "SourceFile"


# static fields
.field static final A:Landroid/view/animation/Interpolator;

.field static final B:Landroid/view/animation/Interpolator;

.field static final C:Landroid/view/animation/Interpolator;

.field static a:Z

.field static final b:Z

.field static final z:Landroid/view/animation/Interpolator;


# instance fields
.field c:Ljava/util/ArrayList;

.field d:[Ljava/lang/Runnable;

.field e:Z

.field f:Ljava/util/ArrayList;

.field g:Ljava/util/ArrayList;

.field h:Ljava/util/ArrayList;

.field i:Ljava/util/ArrayList;

.field j:Ljava/util/ArrayList;

.field k:Ljava/util/ArrayList;

.field l:Ljava/util/ArrayList;

.field m:Ljava/util/ArrayList;

.field n:I

.field o:Lcom/google/android/gms/car/support/j;

.field p:Lcom/google/android/gms/car/support/l;

.field q:Lcom/google/android/gms/car/support/Fragment;

.field r:Z

.field s:Z

.field t:Z

.field u:Ljava/lang/String;

.field v:Z

.field w:Landroid/os/Bundle;

.field x:Landroid/util/SparseArray;

.field y:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 408
    sput-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    .line 411
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/google/android/gms/car/support/n;->b:Z

    .line 748
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/n;->z:Landroid/view/animation/Interpolator;

    .line 749
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/n;->A:Landroid/view/animation/Interpolator;

    .line 750
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/n;->B:Landroid/view/animation/Interpolator;

    .line 751
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/n;->C:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/car/support/m;-><init>()V

    .line 434
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/support/n;->n:I

    .line 446
    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->w:Landroid/os/Bundle;

    .line 447
    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    .line 449
    new-instance v0, Lcom/google/android/gms/car/support/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/o;-><init>(Lcom/google/android/gms/car/support/n;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->y:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(FF)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 771
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 772
    sget-object v1, Lcom/google/android/gms/car/support/n;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 773
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 774
    return-object v0
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 757
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 758
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 760
    sget-object v1, Lcom/google/android/gms/car/support/n;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 761
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 762
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 763
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 764
    sget-object v1, Lcom/google/android/gms/car/support/n;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 765
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 766
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 767
    return-object v9
.end method

.method private a(Lcom/google/android/gms/car/support/Fragment;IZI)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 779
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->G:I

    invoke-static {}, Lcom/google/android/gms/car/support/Fragment;->h()Landroid/view/animation/Animation;

    .line 781
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->G:I

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->a()Landroid/content/Context;

    move-result-object v0

    iget v2, p1, Lcom/google/android/gms/car/support/Fragment;->G:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 788
    if-eqz v0, :cond_0

    .line 834
    :goto_0
    return-object v0

    .line 793
    :cond_0
    if-nez p2, :cond_1

    move-object v0, v1

    .line 794
    goto :goto_0

    .line 797
    :cond_1
    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    .line 798
    :goto_1
    if-gez v0, :cond_5

    move-object v0, v1

    .line 799
    goto :goto_0

    .line 797
    :sswitch_0
    if-eqz p3, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p3, :cond_3

    const/4 v0, 0x3

    goto :goto_1

    :cond_3
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    if-eqz p3, :cond_4

    const/4 v0, 0x5

    goto :goto_1

    :cond_4
    const/4 v0, 0x6

    goto :goto_1

    .line 802
    :cond_5
    packed-switch v0, :pswitch_data_0

    .line 817
    if-nez p4, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->g()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 818
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->g()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 820
    :cond_6
    if-nez p4, :cond_7

    move-object v0, v1

    .line 821
    goto :goto_0

    .line 804
    :pswitch_0
    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Lcom/google/android/gms/car/support/n;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 806
    :pswitch_1
    invoke-static {v3, v5, v3, v4}, Lcom/google/android/gms/car/support/n;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 808
    :pswitch_2
    invoke-static {v5, v3, v4, v3}, Lcom/google/android/gms/car/support/n;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 810
    :pswitch_3
    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Lcom/google/android/gms/car/support/n;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 812
    :pswitch_4
    invoke-static {v4, v3}, Lcom/google/android/gms/car/support/n;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 814
    :pswitch_5
    invoke-static {v3, v4}, Lcom/google/android/gms/car/support/n;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_7
    move-object v0, v1

    .line 834
    goto :goto_0

    .line 797
    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    .line 802
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 577
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 578
    if-ne v1, v0, :cond_1

    .line 579
    const/4 v0, 0x0

    .line 590
    :cond_0
    :goto_0
    return-object v0

    .line 581
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 582
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/RuntimeException;)V

    .line 585
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 586
    if-nez v0, :cond_0

    .line 587
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method private a(ILcom/google/android/gms/car/support/c;)V
    .locals 4

    .prologue
    .line 1420
    monitor-enter p0

    .line 1421
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1422
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    .line 1424
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1425
    if-ge p1, v0, :cond_2

    .line 1426
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1441
    :goto_0
    monitor-exit p0

    return-void

    .line 1429
    :cond_2
    :goto_1
    if-ge v0, p1, :cond_5

    .line 1430
    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1431
    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 1432
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    .line 1434
    :cond_3
    sget-boolean v1, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v1, :cond_4

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding available back stack index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1435
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1436
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1438
    :cond_5
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_6

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 4

    .prologue
    .line 457
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    new-instance v0, Lcom/google/android/gms/car/support/t;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/t;-><init>(Ljava/lang/String;)V

    .line 460
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    if-eqz v0, :cond_0

    .line 463
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    const-string v2, "  "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/gms/car/support/j;->a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :goto_0
    throw p1

    .line 464
    :catch_0
    move-exception v0

    .line 465
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 469
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 470
    :catch_1
    move-exception v0

    .line 471
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1108
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/google/android/gms/car/support/n;->a(IIIZ)V

    .line 1109
    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1142
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1150
    :cond_0
    return-void

    :cond_1
    move v6, v3

    .line 1144
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 1145
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/Fragment;

    .line 1146
    if-eqz v1, :cond_2

    .line 1147
    iget-boolean v0, v1, Lcom/google/android/gms/car/support/Fragment;->K:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->v:Z

    .line 1144
    :cond_2
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1147
    :cond_3
    iput-boolean v3, v1, Lcom/google/android/gms/car/support/Fragment;->K:Z

    iget v2, p0, Lcom/google/android/gms/car/support/n;->n:I

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    goto :goto_1
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1503
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1504
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1505
    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1504
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1508
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/car/support/Fragment;
    .locals 3

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1308
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1309
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 1310
    if-eqz v0, :cond_1

    iget v2, v0, Lcom/google/android/gms/car/support/Fragment;->w:I

    if-ne v2, p1, :cond_1

    .line 1324
    :cond_0
    :goto_1
    return-object v0

    .line 1308
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1315
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1318
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 1319
    if-eqz v0, :cond_3

    iget v2, v0, Lcom/google/android/gms/car/support/Fragment;->w:I

    if-eq v2, p1, :cond_0

    .line 1317
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1324
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;
    .locals 3

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 1331
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1332
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 1333
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1347
    :cond_0
    :goto_1
    return-object v0

    .line 1331
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1338
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1341
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 1342
    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1340
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1347
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(IIIZ)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1112
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1116
    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, Lcom/google/android/gms/car/support/n;->n:I

    if-ne v0, p1, :cond_2

    .line 1139
    :cond_1
    :goto_0
    return-void

    .line 1120
    :cond_2
    iput p1, p0, Lcom/google/android/gms/car/support/n;->n:I

    .line 1121
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    .line 1122
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_4

    .line 1124
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/Fragment;

    .line 1125
    if-eqz v1, :cond_3

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 1126
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 1123
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1130
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/car/support/n;->l()V

    .line 1134
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/n;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1135
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-static {}, Lcom/google/android/gms/car/support/j;->h()V

    .line 1136
    iput-boolean v5, p0, Lcom/google/android/gms/car/support/n;->r:Z

    goto :goto_0
.end method

.method final a(Landroid/os/Parcelable;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 1769
    if-nez p1, :cond_1

    .line 1876
    :cond_0
    :goto_0
    return-void

    .line 1770
    :cond_1
    check-cast p1, Lcom/google/android/gms/car/support/FragmentManagerState;

    .line 1771
    iget-object v0, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    if-eqz v0, :cond_0

    .line 1775
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    .line 1797
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1798
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_2
    move v0, v1

    .line 1800
    :goto_1
    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 1801
    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    aget-object v2, v2, v0

    .line 1802
    if-eqz v2, :cond_4

    .line 1803
    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/car/support/FragmentState;->a(Lcom/google/android/gms/car/support/j;Lcom/google/android/gms/car/support/Fragment;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v3

    .line 1804
    sget-boolean v4, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v4, :cond_3

    const-string v4, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "restoreAllState: active #"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1805
    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1809
    iput-object v7, v2, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/Fragment;

    .line 1800
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1811
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1812
    iget-object v2, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    if-nez v2, :cond_5

    .line 1813
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    .line 1815
    :cond_5
    sget-boolean v2, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v2, :cond_6

    const-string v2, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: avail #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1821
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    if-eqz v0, :cond_b

    .line 1838
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    move v2, v1

    .line 1839
    :goto_3
    iget-object v0, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    array-length v0, v0

    if-ge v2, v0, :cond_c

    .line 1840
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    aget v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 1841
    if-nez v0, :cond_8

    .line 1842
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    aget v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/RuntimeException;)V

    .line 1845
    :cond_8
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/gms/car/support/Fragment;->l:Z

    .line 1846
    sget-boolean v3, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v3, :cond_9

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: added #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1847
    :cond_9
    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1850
    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1839
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1853
    :cond_b
    iput-object v7, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    .line 1857
    :cond_c
    iget-object v0, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    if-eqz v0, :cond_f

    .line 1858
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    move v0, v1

    .line 1859
    :goto_4
    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1860
    iget-object v2, p1, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    aget-object v2, v2, v0

    invoke-virtual {v2, p0}, Lcom/google/android/gms/car/support/BackStackState;->a(Lcom/google/android/gms/car/support/n;)Lcom/google/android/gms/car/support/c;

    move-result-object v2

    .line 1861
    sget-boolean v3, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v3, :cond_d

    .line 1862
    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: back stack #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/gms/car/support/c;->o:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1864
    new-instance v3, Lcom/google/android/gms/car/support/t;

    const-string v4, "FragmentManager"

    invoke-direct {v3, v4}, Lcom/google/android/gms/car/support/t;-><init>(Ljava/lang/String;)V

    .line 1865
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1866
    const-string v3, "  "

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/gms/car/support/c;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 1868
    :cond_d
    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1869
    iget v3, v2, Lcom/google/android/gms/car/support/c;->o:I

    if-ltz v3, :cond_e

    .line 1870
    iget v3, v2, Lcom/google/android/gms/car/support/c;->o:I

    invoke-direct {p0, v3, v2}, Lcom/google/android/gms/car/support/n;->a(ILcom/google/android/gms/car/support/c;)V

    .line 1859
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1874
    :cond_f
    iput-object v7, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method final a(Lcom/google/android/gms/car/support/Fragment;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1104
    iget v2, p0, Lcom/google/android/gms/car/support/n;->n:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 1105
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/support/Fragment;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1209
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "remove: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " nesting="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/google/android/gms/car/support/Fragment;->r:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1211
    :goto_0
    iget-boolean v2, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_4

    .line 1212
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 1213
    iget-object v2, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1215
    :cond_2
    iget-boolean v2, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v2, :cond_3

    .line 1216
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1218
    :cond_3
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    .line 1219
    iput-boolean v1, p1, Lcom/google/android/gms/car/support/Fragment;->m:Z

    .line 1220
    if-eqz v0, :cond_6

    move v2, v5

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 1223
    :cond_4
    return-void

    :cond_5
    move v0, v5

    .line 1210
    goto :goto_0

    :cond_6
    move v2, v1

    .line 1220
    goto :goto_1
.end method

.method final a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 852
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    .line 855
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->m:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    if-le p2, v0, :cond_2

    .line 857
    iget p2, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    .line 861
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->K:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    if-ge v0, v8, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    .line 864
    :cond_3
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    if-ge v0, p2, :cond_1d

    .line 868
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->o:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->p:Z

    if-nez v0, :cond_4

    .line 1101
    :goto_0
    return-void

    .line 871
    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 876
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    .line 877
    iget v2, p1, Lcom/google/android/gms/car/support/Fragment;->c:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 879
    :cond_5
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1100
    :cond_6
    :goto_1
    iput p2, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    goto :goto_0

    .line 881
    :pswitch_0
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_9

    .line 883
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    .line 885
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/support/n;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->i:Lcom/google/android/gms/car/support/Fragment;

    .line 887
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->i:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_8

    .line 888
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lcom/google/android/gms/car/support/Fragment;->k:I

    .line 891
    :cond_8
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->L:Z

    .line 893
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->L:Z

    if-nez v0, :cond_9

    .line 894
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/Fragment;->K:Z

    .line 895
    if-le p2, v6, :cond_9

    move p2, v6

    .line 900
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    .line 901
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->v:Lcom/google/android/gms/car/support/Fragment;

    .line 902
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    iget-object v0, v0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    :goto_2
    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    .line 904
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 905
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->g()V

    .line 906
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_b

    .line 907
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 902
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iget-object v0, v0, Lcom/google/android/gms/car/support/j;->c:Lcom/google/android/gms/car/support/n;

    goto :goto_2

    .line 910
    :cond_b
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->v:Lcom/google/android/gms/car/support/Fragment;

    if-nez v0, :cond_c

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-static {}, Lcom/google/android/gms/car/support/j;->i()V

    .line 914
    :cond_c
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->C:Z

    if-nez v0, :cond_d

    .line 915
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/os/Bundle;)V

    .line 917
    :cond_d
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->C:Z

    .line 918
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->o:Z

    if-eqz v0, :cond_f

    .line 922
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->e()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    .line 924
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_1b

    .line 925
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    .line 926
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/car/support/aa;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    .line 927
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 928
    :cond_e
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/car/support/Fragment;->i()V

    .line 934
    :cond_f
    :goto_3
    :pswitch_1
    if-le p2, v5, :cond_17

    .line 935
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_10

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto ACTIVITY_CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    :cond_10
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->o:Z

    if-nez v0, :cond_15

    .line 938
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->x:I

    if-eqz v0, :cond_33

    .line 939
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->p:Lcom/google/android/gms/car/support/l;

    iget v1, p1, Lcom/google/android/gms/car/support/Fragment;->x:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/support/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 940
    if-nez v0, :cond_11

    iget-boolean v1, p1, Lcom/google/android/gms/car/support/Fragment;->q:Z

    if-nez v1, :cond_11

    .line 941
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No view found for id 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/gms/car/support/Fragment;->x:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->c()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p1, Lcom/google/android/gms/car/support/Fragment;->x:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") for fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/RuntimeException;)V

    .line 948
    :cond_11
    :goto_4
    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    .line 949
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->e()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    .line 951
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v1, :cond_1c

    .line 952
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    iput-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    .line 953
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/gms/car/support/aa;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    .line 954
    if-eqz v0, :cond_13

    .line 955
    invoke-direct {p0, p1, p3, v5, p4}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 957
    if-eqz v1, :cond_12

    .line 958
    iget-object v2, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 960
    :cond_12
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 962
    :cond_13
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    if-eqz v0, :cond_14

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 963
    :cond_14
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/car/support/Fragment;->i()V

    .line 969
    :cond_15
    :goto_5
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->l()V

    .line 970
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_16

    .line 971
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->a()V

    .line 973
    :cond_16
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    .line 977
    :cond_17
    :pswitch_2
    if-le p2, v6, :cond_19

    .line 978
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_18

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto STARTED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    :cond_18
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->m()V

    .line 982
    :cond_19
    :pswitch_3
    if-le p2, v8, :cond_6

    .line 983
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_1a

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto RESUMED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    :cond_1a
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/Fragment;->n:Z

    .line 985
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->n()V

    .line 986
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    .line 987
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    goto/16 :goto_1

    .line 930
    :cond_1b
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    goto/16 :goto_3

    .line 965
    :cond_1c
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    goto :goto_5

    .line 990
    :cond_1d
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    if-le v0, p2, :cond_6

    .line 991
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->a:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 1054
    :cond_1e
    :goto_6
    :pswitch_4
    if-gtz p2, :cond_6

    .line 1055
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->t:Z

    if-eqz v0, :cond_1f

    .line 1056
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    if-eqz v0, :cond_1f

    .line 1063
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    .line 1064
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    .line 1065
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1068
    :cond_1f
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    if-eqz v0, :cond_2b

    .line 1073
    iput p2, p1, Lcom/google/android/gms/car/support/Fragment;->c:I

    move p2, v5

    .line 1074
    goto/16 :goto_1

    .line 993
    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_21

    .line 994
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_20

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom RESUMED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    :cond_20
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->o()V

    .line 996
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->n:Z

    .line 999
    :cond_21
    :pswitch_6
    if-ge p2, v8, :cond_23

    .line 1000
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_22

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom STARTED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    :cond_22
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->p()V

    .line 1004
    :cond_23
    :pswitch_7
    if-ge p2, v6, :cond_25

    .line 1005
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_24

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom STOPPED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    :cond_24
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->q()V

    .line 1009
    :cond_25
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_1e

    .line 1010
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_26

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom ACTIVITY_CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    :cond_26
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_27

    .line 1014
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->f()Z

    move-result v0

    if-nez v0, :cond_27

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_27

    .line 1015
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    if-nez v0, :cond_2a

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    :goto_7
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_27

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    iput-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    iput-object v7, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    .line 1018
    :cond_27
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->r()V

    .line 1019
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_29

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_29

    .line 1021
    iget v0, p0, Lcom/google/android/gms/car/support/n;->n:I

    if-lez v0, :cond_32

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->t:Z

    if-nez v0, :cond_32

    .line 1022
    invoke-direct {p0, p1, p3, v3, p4}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1025
    :goto_8
    if-eqz v0, :cond_28

    .line 1027
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    iput-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    .line 1028
    iput p2, p1, Lcom/google/android/gms/car/support/Fragment;->c:I

    .line 1029
    new-instance v1, Lcom/google/android/gms/car/support/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/support/p;-><init>(Lcom/google/android/gms/car/support/n;Lcom/google/android/gms/car/support/Fragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1045
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1047
    :cond_28
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1049
    :cond_29
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    .line 1050
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    .line 1051
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    goto/16 :goto_6

    .line 1015
    :cond_2a
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_7

    .line 1076
    :cond_2b
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_2c

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    :cond_2c
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->C:Z

    if-nez v0, :cond_2d

    .line 1078
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->s()V

    .line 1081
    :cond_2d
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1082
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->k()V

    .line 1083
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_2e

    .line 1084
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1087
    :cond_2e
    if-nez p5, :cond_6

    .line 1088
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->C:Z

    if-nez v0, :cond_31

    .line 1089
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->f:I

    if-ltz v0, :cond_6

    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_2f

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Freeing fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2f
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_30

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/car/support/j;->j()V

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/Fragment;->j()V

    goto/16 :goto_1

    .line 1091
    :cond_31
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    .line 1092
    iput-object v7, p1, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    goto/16 :goto_1

    :cond_32
    move-object v0, v7

    goto/16 :goto_8

    :cond_33
    move-object v0, v7

    goto/16 :goto_4

    .line 879
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 991
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/car/support/Fragment;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    .line 1190
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "add: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    :cond_1
    iget v0, p1, Lcom/google/android/gms/car/support/Fragment;->f:I

    if-gez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/car/support/Fragment;->a(ILcom/google/android/gms/car/support/Fragment;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_4

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Allocated fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1192
    :cond_4
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    if-nez v0, :cond_8

    .line 1193
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1194
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1191
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/car/support/Fragment;->a(ILcom/google/android/gms/car/support/Fragment;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1196
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1197
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    .line 1198
    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->m:Z

    .line 1199
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v0, :cond_7

    .line 1200
    iput-boolean v3, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1202
    :cond_7
    if-eqz p2, :cond_8

    .line 1203
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;)V

    .line 1206
    :cond_8
    return-void
.end method

.method final a(Lcom/google/android/gms/car/support/c;)V
    .locals 1

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1512
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    .line 1514
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1515
    invoke-direct {p0}, Lcom/google/android/gms/car/support/n;->m()V

    .line 1516
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/support/j;Lcom/google/android/gms/car/support/l;Lcom/google/android/gms/car/support/Fragment;)V
    .locals 2

    .prologue
    .line 1880
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    .line 1882
    iput-object p2, p0, Lcom/google/android/gms/car/support/n;->p:Lcom/google/android/gms/car/support/l;

    .line 1883
    iput-object p3, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    .line 1884
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 642
    if-lez v4, :cond_1

    .line 643
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 644
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 645
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 646
    :goto_0
    if-ge v2, v4, :cond_1

    .line 647
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 648
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 649
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 650
    if-eqz v0, :cond_0

    .line 651
    invoke-virtual {v0, v3, p2, p3, p4}, Lcom/google/android/gms/car/support/Fragment;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 646
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 657
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 659
    if-lez v4, :cond_2

    .line 660
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 661
    :goto_1
    if-ge v2, v4, :cond_2

    .line 662
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 663
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 664
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 661
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 669
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 670
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 671
    if-lez v4, :cond_3

    .line 672
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 673
    :goto_2
    if-ge v2, v4, :cond_3

    .line 674
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 675
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 676
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 673
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 681
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 683
    if-lez v4, :cond_4

    .line 684
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 685
    :goto_3
    if-ge v2, v4, :cond_4

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/c;

    .line 687
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 688
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/c;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 689
    invoke-virtual {v0, v3, p3}, Lcom/google/android/gms/car/support/c;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 685
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 694
    :cond_4
    monitor-enter p0

    .line 695
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 696
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 697
    if-lez v3, :cond_5

    .line 698
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 699
    :goto_4
    if-ge v2, v3, :cond_5

    .line 700
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/c;

    .line 701
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 702
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 699
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 707
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 708
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 711
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 713
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 714
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 715
    if-lez v2, :cond_7

    .line 716
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 717
    :goto_5
    if-ge v1, v2, :cond_7

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 719
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 720
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 725
    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 726
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 727
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->p:Lcom/google/android/gms/car/support/l;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 728
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_8

    .line 729
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 731
    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/n;->n:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 732
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 733
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 734
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->r:Z

    if-eqz v0, :cond_9

    .line 735
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 736
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->r:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 738
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->u:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 739
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->u:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 742
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 743
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 746
    :cond_b
    return-void
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1459
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/n;->e:Z

    if-eqz v0, :cond_0

    .line 1460
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1463
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iget-object v3, v3, Lcom/google/android/gms/car/support/j;->b:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 1464
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 1472
    :goto_0
    monitor-enter p0

    .line 1473
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 1474
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1495
    iget-boolean v2, p0, Lcom/google/android/gms/car/support/n;->v:Z

    if-eqz v2, :cond_3

    .line 1496
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/n;->v:Z

    .line 1497
    invoke-direct {p0}, Lcom/google/android/gms/car/support/n;->l()V

    .line 1499
    :cond_3
    return v0

    .line 1477
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1478
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    array-length v0, v0

    if-ge v0, v3, :cond_6

    .line 1479
    :cond_5
    new-array v0, v3, [Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    .line 1481
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1482
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1483
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    iget-object v0, v0, Lcom/google/android/gms/car/support/j;->b:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->y:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1484
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1486
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/n;->e:Z

    move v0, v1

    .line 1487
    :goto_1
    if-ge v0, v3, :cond_7

    .line 1488
    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    aget-object v4, v4, v0

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 1489
    iget-object v4, p0, Lcom/google/android/gms/car/support/n;->d:[Ljava/lang/Runnable;

    const/4 v5, 0x0

    aput-object v5, v4, v0

    .line 1487
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1491
    :cond_7
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/n;->e:Z

    move v0, v2

    .line 1493
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1887
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1888
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/support/Fragment;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1226
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hide: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    if-nez v0, :cond_4

    .line 1228
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    .line 1229
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1230
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1232
    if-eqz v0, :cond_1

    .line 1233
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1235
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1237
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v0, :cond_3

    .line 1238
    iput-boolean v3, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1240
    :cond_3
    invoke-static {}, Lcom/google/android/gms/car/support/Fragment;->d()V

    .line 1242
    :cond_4
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1891
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1892
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1893
    return-void
.end method

.method public final c(Lcom/google/android/gms/car/support/Fragment;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1245
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "show: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    if-eqz v0, :cond_4

    .line 1247
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->z:Z

    .line 1248
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1249
    invoke-direct {p0, p1, p2, v4, p3}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1251
    if-eqz v0, :cond_1

    .line 1252
    iget-object v1, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1254
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1256
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v0, :cond_3

    .line 1257
    iput-boolean v4, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1259
    :cond_3
    invoke-static {}, Lcom/google/android/gms/car/support/Fragment;->d()V

    .line 1261
    :cond_4
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1896
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1897
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1898
    return-void
.end method

.method public final d(Lcom/google/android/gms/car/support/Fragment;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1264
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "detach: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    if-nez v0, :cond_4

    .line 1266
    iput-boolean v2, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    .line 1267
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    if-eqz v0, :cond_4

    .line 1269
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1270
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "remove from detach: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1273
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v0, :cond_3

    .line 1274
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1276
    :cond_3
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 1277
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 1280
    :cond_4
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1901
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1902
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1903
    return-void
.end method

.method public final e(Lcom/google/android/gms/car/support/Fragment;II)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1283
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attach: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    if-eqz v0, :cond_5

    .line 1285
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/Fragment;->A:Z

    .line 1286
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    if-nez v0, :cond_5

    .line 1287
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    .line 1290
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1291
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1293
    :cond_2
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "add from attach: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1295
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/Fragment;->l:Z

    .line 1296
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->D:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/Fragment;->E:Z

    if-eqz v0, :cond_4

    .line 1297
    iput-boolean v3, p0, Lcom/google/android/gms/car/support/n;->r:Z

    .line 1299
    :cond_4
    iget v2, p0, Lcom/google/android/gms/car/support/n;->n:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    .line 1302
    :cond_5
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1906
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1907
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1908
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1911
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1912
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1918
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->s:Z

    .line 1920
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1921
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1924
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1925
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1928
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1929
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1932
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/n;->t:Z

    .line 1933
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/n;->a()Z

    .line 1934
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/n;->b(I)V

    .line 1935
    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->o:Lcom/google/android/gms/car/support/j;

    .line 1936
    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->p:Lcom/google/android/gms/car/support/l;

    .line 1937
    iput-object v1, p0, Lcom/google/android/gms/car/support/n;->q:Lcom/google/android/gms/car/support/Fragment;

    .line 1938
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 618
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 619
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
