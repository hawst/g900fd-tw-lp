.class public final Lcom/google/android/gms/games/c/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/c/x;

.field public c:Lcom/google/android/gms/games/c/y;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4609
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4610
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/w;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    iput-object v1, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/w;->G:I

    .line 4611
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 4682
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 4683
    iget v1, p0, Lcom/google/android/gms/games/c/w;->a:I

    if-eqz v1, :cond_0

    .line 4684
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/w;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4687
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-eqz v1, :cond_1

    .line 4688
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4691
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-eqz v1, :cond_2

    .line 4692
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4695
    :cond_2
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4578
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/w;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/x;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/y;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4668
    iget v0, p0, Lcom/google/android/gms/games/c/w;->a:I

    if-eqz v0, :cond_0

    .line 4669
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/w;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4671
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-eqz v0, :cond_1

    .line 4672
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4674
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-eqz v0, :cond_2

    .line 4675
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4677
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 4678
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4623
    if-ne p1, p0, :cond_1

    .line 4651
    :cond_0
    :goto_0
    return v0

    .line 4626
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 4627
    goto :goto_0

    .line 4629
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/w;

    .line 4630
    iget v2, p0, Lcom/google/android/gms/games/c/w;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/w;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4631
    goto :goto_0

    .line 4633
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-nez v2, :cond_4

    .line 4634
    iget-object v2, p1, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4635
    goto :goto_0

    .line 4638
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    iget-object v3, p1, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/x;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4639
    goto :goto_0

    .line 4642
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-nez v2, :cond_6

    .line 4643
    iget-object v2, p1, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-eqz v2, :cond_0

    move v0, v1

    .line 4644
    goto :goto_0

    .line 4647
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    iget-object v3, p1, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4648
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4656
    iget v0, p0, Lcom/google/android/gms/games/c/w;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4658
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4660
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4662
    return v0

    .line 4658
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/w;->b:Lcom/google/android/gms/games/c/x;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/x;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4660
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/w;->c:Lcom/google/android/gms/games/c/y;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/y;->hashCode()I

    move-result v1

    goto :goto_1
.end method
