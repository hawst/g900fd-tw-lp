.class public final Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/destination/players/b;
.implements Lcom/google/android/gms/games/ui/destination/r;


# instance fields
.field private an:Lcom/google/android/gms/games/Player;

.field private ao:Lcom/google/android/gms/games/Player;

.field private ap:Lcom/google/android/gms/games/internal/game/ExtendedGame;

.field private aq:Z

.field private ar:Z

.field private as:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;ILcom/google/android/gms/games/achievement/a;Lcom/google/android/gms/games/achievement/a;)V
    .locals 19

    .prologue
    .line 42
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/achievement/a;->a()I

    move-result v18

    move/from16 v16, v2

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/achievement/a;->b(I)Lcom/google/android/gms/games/achievement/Achievement;

    move-result-object v13

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/games/achievement/a;->a()I

    move-result v5

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_3

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/achievement/a;->b(I)Lcom/google/android/gms/games/achievement/Achievement;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v15, v2

    :goto_2
    new-instance v2, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->f()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ap:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v5}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v6

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->q()J

    move-result-wide v8

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->g()Landroid/net/Uri;

    move-result-object v10

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->i()Landroid/net/Uri;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->an:Lcom/google/android/gms/games/Player;

    invoke-interface {v13}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    if-nez v15, :cond_1

    const/4 v15, 0x2

    :goto_3
    invoke-direct/range {v2 .. v15}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/net/Uri;Landroid/net/Uri;Lcom/google/android/gms/games/Player;ILcom/google/android/gms/games/Player;I)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_0

    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_1
    invoke-interface {v15}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v15

    goto :goto_3

    :cond_2
    new-instance v2, Lcom/google/android/gms/games/ui/destination/players/e;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/ui/destination/players/e;-><init>(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;)V

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v2, Lcom/google/android/gms/common/data/p;

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/data/p;-><init>(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->as:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;->a(Lcom/google/android/gms/common/data/b;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/p;->a()I

    move-result v2

    const/4 v4, 0x0

    move/from16 v0, p1

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    return-void

    :cond_3
    move-object v15, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->aq:Z

    return v0
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ar:Z

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    .line 145
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->c(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/common/api/t;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "AchvCompareFragment"

    const-string v1, "onResult: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->an:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ap:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v2}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    .line 173
    new-instance v3, Lcom/google/android/gms/common/api/p;

    invoke-direct {v3, p1}, Lcom/google/android/gms/common/api/p;-><init>(Lcom/google/android/gms/common/api/t;)V

    .line 177
    sget-object v4, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p1, v0, v5, v6}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v0

    .line 182
    sget-object v4, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, p1, v1, v2, v6}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v1

    .line 187
    invoke-virtual {v3}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v2

    .line 188
    new-instance v3, Lcom/google/android/gms/games/ui/destination/players/d;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/players/d;-><init>(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 2

    .prologue
    .line 42
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ar:Z

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->c(Lcom/google/android/gms/common/api/t;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->an:Lcom/google/android/gms/games/Player;

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->an:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    .line 133
    const-string v0, "AchvCompareFragment"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 107
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;)V
    .locals 3

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->c:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 283
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 66
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->aq:Z

    .line 67
    const-string v0, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    .line 70
    const-string v0, "AchvCompareFragment"

    const-string v1, "We don\'t have another player to compare to, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 102
    :goto_0
    return-void

    .line 76
    :cond_0
    const-string v0, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ap:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ap:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-nez v0, :cond_1

    .line 78
    const-string v0, "AchvCompareFragment"

    const-string v1, "EXTRA_EXTENDED_GAME must be given to achievement comparison activity."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 83
    :cond_1
    if-nez p1, :cond_2

    .line 84
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ar:Z

    .line 89
    :goto_1
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    .line 90
    const v1, 0x7f0f0038

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 92
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/b;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->as:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;

    .line 95
    new-instance v1, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 96
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->as:Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 98
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    .line 99
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/b;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 86
    :cond_2
    const-string v0, "savedStateOtherPlayerLoaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ar:Z

    goto :goto_1
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->e(Landroid/os/Bundle;)V

    .line 113
    const-string v0, "savedStateOtherPlayerLoaded"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->ar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->aq:Z

    .line 119
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/h;->f()V

    .line 120
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 288
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    const-string v0, "AchvCompareFragment"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 294
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonFragment;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
