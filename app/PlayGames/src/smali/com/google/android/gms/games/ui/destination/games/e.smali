.class public final Lcom/google/android/gms/games/ui/destination/games/e;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

.field private an:Lcom/google/android/gms/common/images/ImageManager;

.field private ao:Landroid/widget/LinearLayout;

.field private ap:Lcom/google/android/gms/games/Game;

.field private aq:Lcom/google/android/gms/games/internal/game/GameBadge;

.field private ar:Ljava/util/ArrayList;

.field private as:Lcom/google/android/gms/games/ui/destination/games/i;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    .line 297
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/e;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ao:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/e;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/Game;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/e;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->aq:Lcom/google/android/gms/games/internal/game/GameBadge;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ar:Ljava/util/ArrayList;

    .line 158
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 159
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/GameBadge;

    .line 162
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->c()I

    move-result v3

    if-nez v3, :cond_0

    .line 165
    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->aq:Lcom/google/android/gms/games/internal/game/GameBadge;

    .line 158
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ar:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 175
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/internal/game/GameBadge;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->aq:Lcom/google/android/gms/games/internal/game/GameBadge;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/games/e;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ao:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/games/e;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ar:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/common/images/ImageManager;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->an:Lcom/google/android/gms/common/images/ImageManager;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;I)V

    .line 146
    :cond_0
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/app/a;)V

    .line 134
    return-void
.end method

.method final as()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->as:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->d()V

    .line 92
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 66
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->an:Lcom/google/android/gms/common/images/ImageManager;

    .line 70
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/destination/games/i;-><init>(Lcom/google/android/gms/games/ui/destination/games/e;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->as:Lcom/google/android/gms/games/ui/destination/games/i;

    .line 72
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/e;->ah()V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->as:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Landroid/support/v7/widget/bv;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/app/a;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 83
    return-void
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_1

    .line 99
    instance-of v1, v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 101
    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 102
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aj()Ljava/lang/String;

    move-result-object v1

    .line 108
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    invoke-static {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 113
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPG_gameDetail_about"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ap:Lcom/google/android/gms/games/Game;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ah()Lcom/google/android/gms/games/internal/game/ExtendedGame;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ar:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->aq:Lcom/google/android/gms/games/internal/game/GameBadge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/e;->aq:Lcom/google/android/gms/games/internal/game/GameBadge;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->ar:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/destination/games/h;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/games/ui/destination/games/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/e;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const-string v2, "showBadgesDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x7f0c00ff
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
