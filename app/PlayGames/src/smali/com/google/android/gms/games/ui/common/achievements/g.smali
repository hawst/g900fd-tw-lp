.class public final Lcom/google/android/gms/games/ui/common/achievements/g;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I

.field private static final g:I


# instance fields
.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/gms/i;->J:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/g;->e:I

    .line 19
    sget v0, Lcom/google/android/gms/i;->K:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/g;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 27
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->j:Z

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/achievements/g;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->j:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/achievements/g;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->h:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/achievements/g;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->i:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 50
    new-instance v1, Lcom/google/android/gms/games/ui/common/achievements/h;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->d:Landroid/view/LayoutInflater;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->j:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/games/ui/common/achievements/g;->e:I

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/common/achievements/h;-><init>(Landroid/view/View;)V

    return-object v1

    :cond_0
    sget v0, Lcom/google/android/gms/games/ui/common/achievements/g;->g:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/google/android/gms/g;->S:I

    return v0
.end method

.method public final d(II)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->h:I

    .line 43
    iput p2, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->i:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/g;->d()V

    .line 46
    return-void
.end method
