.class public final Lcom/google/android/gms/games/ui/destination/games/k;
.super Lcom/google/android/gms/games/ui/common/achievements/f;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/achievements/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/k;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;I)V

    .line 54
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/k;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/k;->a(Lcom/google/android/gms/games/app/a;)V

    .line 43
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/achievements/f;->d(Landroid/os/Bundle;)V

    .line 26
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 27
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/k;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    .line 29
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/k;->ah()V

    .line 33
    :cond_0
    return-void
.end method
