.class public final Lcom/google/android/gms/games/ui/destination/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    sput v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    return-void
.end method

.method public static a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 122
    sget v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 142
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :sswitch_0
    new-instance v0, Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/main/d;-><init>()V

    .line 139
    :goto_0
    return-object v0

    .line 127
    :sswitch_1
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;-><init>()V

    goto :goto_0

    .line 130
    :sswitch_2
    new-instance v0, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/inbox/DestinationInboxActivity$DestinationInboxFragment;-><init>()V

    goto :goto_0

    .line 133
    :sswitch_3
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;-><init>()V

    goto :goto_0

    .line 136
    :sswitch_4
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;-><init>()V

    goto :goto_0

    .line 139
    :sswitch_5
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/s;-><init>()V

    goto :goto_0

    .line 122
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x64 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 76
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 79
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/x;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/destination/x;-><init>()V

    invoke-virtual {v8, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 81
    const/4 v9, 0x1

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0091

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b4

    const v4, 0x7f0200b5

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 85
    const/4 v9, 0x2

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0093

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b0

    const v4, 0x7f0200b1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 89
    const/4 v9, 0x3

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0092

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200ae

    const v4, 0x7f0200af

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 93
    const/4 v9, 0x4

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0095

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b2

    const v4, 0x7f0200b3

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 97
    const/4 v9, 0x5

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0096

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200ac

    const v4, 0x7f0200ad

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 102
    const/4 v0, 0x6

    new-instance v1, Lcom/google/android/gms/games/ui/destination/v;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/destination/v;-><init>()V

    invoke-virtual {v8, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 104
    const/4 v9, 0x7

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0097

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 108
    const/16 v9, 0x8

    new-instance v0, Lcom/google/android/gms/games/ui/destination/u;

    const v1, 0x7f0f0090

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/destination/u;-><init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 113
    return-object v8
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 345
    const-string v0, "com.google.android.gms.games.NAV_DRAWER_INDEX"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    .line 348
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/ui/destination/b;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    invoke-static {p0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 271
    instance-of v0, p0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    if-ne p1, v0, :cond_0

    .line 338
    :goto_0
    return-void

    .line 276
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 336
    const-string v0, "NavDrawerUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDrawerItemClicked: unexpected item: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :sswitch_0
    sput p1, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    .line 288
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 289
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 290
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 291
    const-string v0, "com.google.android.gms.games.NAV_DRAWER_INDEX"

    sget v2, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    const/16 v0, 0x64

    if-ne p1, v0, :cond_2

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 297
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 298
    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v2, v0}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    .line 299
    if-nez v0, :cond_1

    .line 301
    const-string v0, "NavDrawerUtils"

    const-string v1, "We don\'t have a current player, something went wrong. Let\'s do nothing."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_1
    const-string v2, "com.google.android.gms.games.PLAYER"

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 319
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/b;->startActivity(Landroid/content/Intent;)V

    .line 320
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/games/ui/destination/b;->overridePendingTransition(II)V

    goto :goto_0

    .line 314
    :cond_3
    const-string v0, "NavDrawerUtils"

    const-string v1, "\'My Profile\' item: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 325
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->i()V

    goto :goto_0

    .line 332
    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/b;->H()V

    goto :goto_0

    .line 276
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 153
    const/4 v0, 0x0

    .line 154
    sget v1, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    sparse-switch v1, :sswitch_data_0

    .line 180
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :sswitch_0
    const v0, 0x7f0f0091

    .line 185
    :goto_0
    :sswitch_1
    if-lez v0, :cond_0

    .line 186
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->setTitle(I)V

    .line 192
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Ljava/lang/CharSequence;)V

    .line 193
    return-void

    .line 160
    :sswitch_2
    const v0, 0x7f0f008c

    .line 161
    goto :goto_0

    .line 164
    :sswitch_3
    const v0, 0x7f0f007a

    .line 165
    goto :goto_0

    .line 168
    :sswitch_4
    const v0, 0x7f0f00bc

    .line 169
    goto :goto_0

    .line 172
    :sswitch_5
    const v0, 0x7f0f00c8

    .line 173
    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 154
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Lcom/google/android/gms/games/ui/destination/b;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 202
    sget v1, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    packed-switch v1, :pswitch_data_0

    .line 207
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/destination/c/b;->a(Lcom/google/android/gms/games/ui/destination/b;I)V

    .line 208
    :goto_0
    return v0

    .line 204
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 351
    sget v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    return v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 360
    sget v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 369
    const-string v0, "No search handler for this drawer!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/Object;)V

    .line 371
    :goto_0
    return-void

    .line 363
    :sswitch_0
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 366
    :sswitch_1
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 360
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b(Lcom/google/android/gms/games/ui/destination/main/MainActivity;)V
    .locals 4

    .prologue
    const v0, 0x7f11000c

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 219
    sget v2, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    sparse-switch v2, :sswitch_data_0

    .line 251
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCurrentMenu: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :sswitch_0
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    .line 255
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(I)V

    .line 256
    return-void

    .line 227
    :sswitch_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    move v0, v1

    .line 228
    goto :goto_0

    .line 232
    :sswitch_2
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    goto :goto_0

    .line 236
    :sswitch_3
    const v0, 0x7f11000f

    .line 237
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    goto :goto_0

    .line 241
    :sswitch_4
    const v0, 0x7f110010

    .line 242
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    goto :goto_0

    .line 246
    :sswitch_5
    const v0, 0x7f11000e

    .line 247
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->b(Z)V

    goto :goto_0

    .line 219
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x64 -> :sswitch_5
    .end sparse-switch
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    sget v0, Lcom/google/android/gms/games/ui/destination/c/b;->a:I

    packed-switch v0, :pswitch_data_0

    .line 389
    const-string v0, "mobile_games_default"

    :goto_0
    return-object v0

    .line 379
    :pswitch_0
    const-string v0, "mobile_play_now"

    goto :goto_0

    .line 381
    :pswitch_1
    const-string v0, "mobile_my_games"

    goto :goto_0

    .line 383
    :pswitch_2
    const-string v0, "mobile_inbox"

    goto :goto_0

    .line 385
    :pswitch_3
    const-string v0, "mobile_players"

    goto :goto_0

    .line 387
    :pswitch_4
    const-string v0, "mobile_explore"

    goto :goto_0

    .line 377
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
