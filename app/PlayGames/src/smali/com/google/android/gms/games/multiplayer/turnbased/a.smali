.class public final Lcom/google/android/gms/games/multiplayer/turnbased/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/games/multiplayer/a;

.field public final b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

.field public final c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

.field public final d:Lcom/google/android/gms/games/multiplayer/turnbased/c;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    new-instance v1, Lcom/google/android/gms/games/multiplayer/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    .line 45
    :goto_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_1

    .line 47
    new-instance v1, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    .line 53
    :goto_1
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_2

    .line 55
    new-instance v1, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    .line 61
    :goto_2
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    new-instance v1, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    .line 67
    :goto_3
    return-void

    .line 41
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    goto :goto_0

    .line 49
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    goto :goto_1

    .line 57
    :cond_2
    iput-object v2, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    goto :goto_2

    .line 65
    :cond_3
    iput-object v2, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    goto :goto_3
.end method

.method private static a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 70
    packed-switch p1, :pswitch_data_0

    const-string v0, "MatchTurnStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown match turn status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TURN_STATUS_UNKNOWN"

    .line 71
    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_1
    return-object v0

    .line 70
    :pswitch_0
    const-string v0, "TURN_STATUS_INVITED"

    goto :goto_0

    :pswitch_1
    const-string v0, "TURN_STATUS_MY_TURN"

    goto :goto_0

    :pswitch_2
    const-string v0, "TURN_STATUS_THEIR_TURN"

    goto :goto_0

    :pswitch_3
    const-string v0, "TURN_STATUS_COMPLETE"

    goto :goto_0

    .line 74
    :cond_0
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1

    .line 70
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->f_()V

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_2

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->f_()V

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->f_()V

    .line 150
    :cond_3
    return-void
.end method
