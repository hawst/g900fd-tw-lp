.class final Lcom/google/android/gms/games/ui/common/achievements/c;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 69
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 11

    .prologue
    .line 65
    check-cast p3, Lcom/google/android/gms/games/achievement/Achievement;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/c;->b(Z)V

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v5

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/c;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/achievements/c;->e(Z)V

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    if-ne v5, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v4, ""

    const-string v3, ""

    if-nez v0, :cond_4

    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown achievement state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->g()Landroid/net/Uri;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->j:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/c;->a(Landroid/net/Uri;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/c;->k:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->q()J

    move-result-wide v2

    const/high16 v1, 0x80000

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/common/achievements/c;->f(Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/d;->s:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/c;->j(I)V

    move-object v1, v3

    :goto_1
    if-nez v5, :cond_2

    const/16 v0, 0xff

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/c;->c(I)V

    if-nez v5, :cond_3

    sget v0, Lcom/google/android/gms/l;->bF:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_3
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/c;->t()Landroid/database/CharArrayBuffer;

    move-result-object v2

    invoke-interface {p3, v2}, Lcom/google/android/gms/games/achievement/Achievement;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/achievements/c;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/c;->u()Landroid/database/CharArrayBuffer;

    move-result-object v2

    invoke-interface {p3, v2}, Lcom/google/android/gms/games/achievement/Achievement;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/achievements/c;->b(Landroid/database/CharArrayBuffer;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-lez v7, :cond_7

    sget v4, Lcom/google/android/gms/l;->z:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {v6, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/achievements/c;->g(Ljava/lang/String;)V

    :goto_4
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->f()Ljava/lang/String;

    move-result-object v3

    :goto_5
    if-eqz v5, :cond_1

    sget v5, Lcom/google/android/gms/d;->c:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/common/achievements/c;->f(I)V

    sget v5, Lcom/google/android/gms/d;->b:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/common/achievements/c;->h(I)V

    sget v5, Lcom/google/android/gms/d;->e:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/common/achievements/c;->l(I)V

    :cond_1
    sget v5, Lcom/google/android/gms/l;->bD:I

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v4, 0x1

    aput-object v3, v7, v4

    const/4 v3, 0x2

    aput-object v1, v7, v3

    const/4 v1, 0x3

    aput-object v2, v7, v1

    const/4 v1, 0x4

    aput-object v0, v7, v1

    invoke-virtual {v6, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/c;->a(Ljava/lang/String;)V

    return-void

    :pswitch_1
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->i()Landroid/net/Uri;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->i:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/c;->a(Landroid/net/Uri;I)V

    move-object v1, v3

    goto/16 :goto_1

    :pswitch_2
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/f;->h:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/c;->a(Landroid/net/Uri;I)V

    move-object v1, v3

    goto/16 :goto_1

    :cond_2
    sget v0, Lcom/google/android/gms/d;->r:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    goto/16 :goto_2

    :cond_3
    sget v0, Lcom/google/android/gms/l;->bE:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_3

    :cond_4
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->o()I

    move-result v0

    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->k()I

    move-result v2

    if-gtz v2, :cond_5

    const-string v1, "AchievementAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Inconsistent achievement "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": TYPE_INCREMENTAL, but totalSteps = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    :cond_5
    if-lt v0, v2, :cond_8

    const-string v1, "AchievementAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Inconsistent achievement "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": STATE_REVEALED, but steps = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " / "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    :goto_6
    sget v0, Lcom/google/android/gms/g;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/c;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b(II)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move-object v0, v3

    goto/16 :goto_3

    :cond_6
    sget v2, Lcom/google/android/gms/l;->u:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/achievements/c;->e(I)V

    sget v2, Lcom/google/android/gms/l;->t:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/achievements/c;->g(I)V

    sget v2, Lcom/google/android/gms/l;->u:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v2, Lcom/google/android/gms/l;->t:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v10

    goto/16 :goto_5

    :cond_7
    move-object v2, v4

    goto/16 :goto_4

    :cond_8
    move v1, v0

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/achievements/a;

    .line 233
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/a;->a(Lcom/google/android/gms/games/ui/common/achievements/a;)Lcom/google/android/gms/games/ui/common/achievements/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/c;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/Achievement;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/achievements/b;->a(Lcom/google/android/gms/games/achievement/Achievement;)V

    .line 234
    return-void
.end method

.method protected final s(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 213
    sget v0, Lcom/google/android/gms/g;->d:I

    if-ne p1, v0, :cond_0

    .line 214
    new-instance v0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/c;->k:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;-><init>(Landroid/content/Context;)V

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/c;->k:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/m;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setTextAppearance(Landroid/content/Context;I)V

    .line 216
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 219
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setGravity(I)V

    .line 220
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    sget v1, Lcom/google/android/gms/g;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setId(I)V

    .line 222
    return-object v0

    .line 224
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown image view type received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
