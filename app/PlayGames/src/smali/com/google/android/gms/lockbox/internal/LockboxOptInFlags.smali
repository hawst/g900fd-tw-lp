.class public Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/lockbox/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/internal/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->a:I

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->b:Ljava/lang/String;

    .line 40
    iput-boolean p3, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->c:Z

    .line 41
    iput-boolean p4, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->d:Z

    .line 42
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 128
    invoke-static {p0, p1}, Lcom/google/android/gms/lockbox/internal/a;->a(Lcom/google/android/gms/lockbox/internal/LockboxOptInFlags;Landroid/os/Parcel;)V

    .line 129
    return-void
.end method
