.class public abstract Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field private static final b:Ljava/lang/Object;

.field private static c:Ljava/lang/ClassLoader;

.field private static d:Ljava/lang/Integer;


# instance fields
.field public a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->b:Ljava/lang/Object;

    .line 32
    sput-object v1, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->c:Ljava/lang/ClassLoader;

    .line 36
    sput-object v1, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->d:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a:Z

    .line 44
    return-void
.end method

.method private static a()Ljava/lang/ClassLoader;
    .locals 2

    .prologue
    .line 192
    sget-object v1, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->c:Ljava/lang/ClassLoader;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/Integer;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    if-eqz p2, :cond_0

    .line 233
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(I)Z

    move-result v0

    .line 246
    :goto_0
    return v0

    .line 240
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 241
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 242
    invoke-static {v2}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 242
    goto :goto_1
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 157
    if-nez p3, :cond_1

    if-nez p4, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    invoke-direct {p2, p3, p4}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 164
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 299
    :try_start_0
    const-string v1, "NULL"

    invoke-virtual {p0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 300
    const-string v2, "SAFE_PARCELABLE_NULL_STRING"

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 307
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 303
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method protected static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 280
    invoke-static {}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 281
    if-nez v0, :cond_0

    .line 282
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    .line 287
    :cond_0
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 288
    invoke-static {v0}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static d_()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 202
    sget-object v1, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 203
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->d:Ljava/lang/Integer;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a:Z

    .line 267
    return-void
.end method

.method protected abstract a(I)Z
.end method
