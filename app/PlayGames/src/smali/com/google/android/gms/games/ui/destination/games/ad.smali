.class public final Lcom/google/android/gms/games/ui/destination/games/ad;
.super Lcom/google/android/gms/games/ui/destination/games/ab;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 218
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(Lcom/google/android/gms/games/j;)V

    return-void
.end method

.method public final au()I
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x6

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 222
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ad;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aa;->e(Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/i;->g(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 225
    return-void
.end method

.method public final b_(I)V
    .locals 3

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ad;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 235
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    const-string v0, "GameExploreFragment"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/ad;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->e(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/i;->h(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
