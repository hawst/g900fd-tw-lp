.class public Lcom/google/android/gms/cloudsave/internal/SaveRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/j;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/SaveRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/SaveRequest;->a:I

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/internal/SaveRequest;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 53
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/j;->a(Lcom/google/android/gms/cloudsave/internal/SaveRequest;Landroid/os/Parcel;I)V

    .line 40
    return-void
.end method
