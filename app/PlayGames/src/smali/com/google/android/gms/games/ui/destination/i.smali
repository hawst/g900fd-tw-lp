.class public abstract Lcom/google/android/gms/games/ui/destination/i;
.super Lcom/google/android/gms/games/ui/destination/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/e/k;


# instance fields
.field public e:Landroid/support/v4/view/ViewPager;

.field protected f:Lcom/google/android/gms/games/ui/e/b;

.field private final g:Lcom/google/android/gms/games/ui/e/ah;

.field private h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

.field private i:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/e/ah;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/destination/a;-><init>(I)V

    .line 76
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/i;->g:Lcom/google/android/gms/games/ui/e/ah;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/i;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/i;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/i;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->b(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 219
    instance-of v0, p1, Lcom/google/android/gms/games/ui/p;

    if-eqz v0, :cond_0

    .line 220
    check-cast p1, Lcom/google/android/gms/games/ui/p;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/p;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 224
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 215
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/i;->b(Landroid/support/v4/app/Fragment;)V

    .line 216
    return-void
.end method


# virtual methods
.method public final S()Z
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x1

    return v0
.end method

.method public final W()I
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/a;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 89
    const v0, 0x7f0c016c

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    const-string v1, "layout resource did not include include a ViewPager with id \'pager\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 93
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v1, Lcom/google/android/gms/games/ui/n;

    .line 94
    new-instance v0, Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->k()Landroid/support/v4/app/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/i;->g:Lcom/google/android/gms/games/ui/e/ah;

    iget-object v3, v3, Lcom/google/android/gms/games/ui/e/ah;->a:[Lcom/google/android/gms/games/ui/e/ai;

    const/4 v4, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/e/b;-><init>(Landroid/content/Context;Landroid/support/v4/app/ag;[Lcom/google/android/gms/games/ui/e/ai;Lcom/google/android/gms/games/ui/e/f;Lcom/google/android/gms/games/ui/e/k;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    new-instance v1, Lcom/google/android/gms/games/ui/destination/j;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/j;-><init>(Lcom/google/android/gms/games/ui/destination/i;)V

    iput-object v1, v0, Lcom/google/android/gms/games/ui/e/b;->d:Lcom/google/android/gms/games/ui/e/d;

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ao;)V

    .line 107
    const v0, 0x7f0c01a4

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->R()Z

    move-result v0

    if-nez v0, :cond_2

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c(I)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Landroid/support/v4/view/ViewPager;)V

    .line 114
    new-instance v0, Lcom/google/android/gms/games/ui/e/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/c;-><init>(Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/e/b;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/cc;)V

    .line 136
    :goto_0
    if-nez p3, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->g:Lcom/google/android/gms/games/ui/e/ah;

    iget v0, v0, Lcom/google/android/gms/games/ui/e/ah;->b:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->R()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    .line 145
    :cond_1
    :goto_1
    return-object v6

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->setVisibility(I)V

    .line 120
    instance-of v0, v6, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    move-object v0, v6

    .line 121
    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 122
    new-instance v1, Lcom/google/android/gms/games/ui/destination/k;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/i;->h:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/gms/games/ui/destination/k;-><init>(Lcom/google/android/gms/games/ui/destination/i;Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/e/b;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/support/v4/view/cc;)V

    .line 131
    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i()V

    goto :goto_0

    .line 137
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/i;->c(I)V

    goto :goto_1
.end method

.method protected final ai()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/b;->f()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 179
    const/4 v0, -0x1

    return v0
.end method

.method public final b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/gms/games/ui/destination/l;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/destination/l;-><init>(Lcom/google/android/gms/games/ui/destination/i;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->i:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->i:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    return-object v0
.end method

.method public v()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/a;->v()V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->f:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 154
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/r;

    if-eqz v1, :cond_0

    .line 155
    check-cast v0, Lcom/google/android/gms/games/ui/destination/r;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/i;->ah()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/destination/r;->a(Lcom/google/android/gms/games/app/a;)V

    .line 157
    :cond_0
    return-void
.end method
