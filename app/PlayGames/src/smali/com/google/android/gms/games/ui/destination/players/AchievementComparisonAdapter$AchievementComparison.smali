.class Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:J

.field final e:J

.field final f:Landroid/net/Uri;

.field final g:Landroid/net/Uri;

.field final h:Lcom/google/android/gms/games/Player;

.field final i:I

.field final j:Lcom/google/android/gms/games/Player;

.field final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->a:Ljava/lang/String;

    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->b:Ljava/lang/String;

    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->c:Ljava/lang/String;

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->d:J

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->e:J

    .line 218
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->f:Landroid/net/Uri;

    .line 219
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->g:Landroid/net/Uri;

    .line 221
    const-class v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->h:Lcom/google/android/gms/games/Player;

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    .line 224
    const-class v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->j:Lcom/google/android/gms/games/Player;

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    .line 226
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/net/Uri;Landroid/net/Uri;Lcom/google/android/gms/games/Player;ILcom/google/android/gms/games/Player;I)V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->a:Ljava/lang/String;

    .line 232
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->b:Ljava/lang/String;

    .line 233
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->c:Ljava/lang/String;

    .line 234
    iput-wide p4, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->d:J

    .line 235
    iput-wide p6, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->e:J

    .line 236
    iput-object p8, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->f:Landroid/net/Uri;

    .line 237
    iput-object p9, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->g:Landroid/net/Uri;

    .line 239
    iput-object p10, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->h:Lcom/google/android/gms/games/Player;

    .line 240
    iput p11, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    .line 242
    iput-object p12, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->j:Lcom/google/android/gms/games/Player;

    .line 243
    iput p13, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    .line 244
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 256
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 257
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->h:Lcom/google/android/gms/games/Player;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 262
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->j:Lcom/google/android/gms/games/Player;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 265
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    return-void
.end method
