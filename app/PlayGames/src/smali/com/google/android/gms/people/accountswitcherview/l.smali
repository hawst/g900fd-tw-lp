.class public final Lcom/google/android/gms/people/accountswitcherview/l;
.super Lcom/google/android/gms/people/accountswitcherview/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/accountswitcherview/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Z)V

    .line 29
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/people/accountswitcherview/aa;->a:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/m;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/accountswitcherview/m;-><init>(Lcom/google/android/gms/people/accountswitcherview/l;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/l;->a(Lcom/google/android/gms/people/accountswitcherview/p;)V

    .line 69
    return-void
.end method

.method protected final a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 73
    if-nez p2, :cond_0

    .line 74
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/l;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/l;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
