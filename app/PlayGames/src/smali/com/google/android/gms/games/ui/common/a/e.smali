.class public final Lcom/google/android/gms/games/ui/common/a/e;
.super Lcom/google/android/gms/games/ui/e;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/n;

.field private final h:Lcom/google/android/gms/games/ui/common/a/g;

.field private final i:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Z)V
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/e;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/a/e;->g:Lcom/google/android/gms/games/ui/n;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/a/e;->h:Lcom/google/android/gms/games/ui/common/a/g;

    .line 27
    sget v0, Lcom/google/android/gms/i;->N:I

    iput v0, p0, Lcom/google/android/gms/games/ui/common/a/e;->i:I

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/a/e;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/e;->g:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/a/e;)Lcom/google/android/gms/games/ui/common/a/g;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/e;->h:Lcom/google/android/gms/games/ui/common/a/g;

    return-object v0
.end method


# virtual methods
.method protected final b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/g;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/f;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/e;->d:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/google/android/gms/games/ui/common/a/e;->i:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/a/f;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/g;->ac:I

    return v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/e;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method
