.class public Lcom/google/android/gms/audiomodem/DsssEncoding;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:F

.field private final h:I

.field private final i:F

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/gms/audiomodem/b;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/DsssEncoding;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZZIIFIFIIII)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput p1, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    .line 94
    iput p2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    .line 95
    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    .line 96
    iput-boolean p4, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    .line 97
    iput p5, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    .line 98
    iput p6, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    .line 99
    iput p7, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    .line 100
    iput p8, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    .line 101
    iput p9, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    .line 102
    iput p10, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    .line 103
    iput p11, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    .line 104
    iput p12, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    .line 105
    iput p13, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->m:I

    .line 106
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 200
    if-ne p0, p1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 203
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    if-nez v2, :cond_2

    move v0, v1

    .line 204
    goto :goto_0

    .line 206
    :cond_2
    check-cast p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    .line 207
    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->m:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->m:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 193
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()F
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->m:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 189
    invoke-static {p0, p1}, Lcom/google/android/gms/audiomodem/b;->a(Lcom/google/android/gms/audiomodem/DsssEncoding;Landroid/os/Parcel;)V

    .line 190
    return-void
.end method
