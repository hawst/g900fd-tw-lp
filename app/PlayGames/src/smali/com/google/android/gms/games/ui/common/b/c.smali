.class final Lcom/google/android/gms/games/ui/common/b/c;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/support/v7/widget/CardView;

.field private final n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    move-object v0, p1

    .line 60
    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/b/c;->m:Landroid/support/v7/widget/CardView;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/c;->m:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    sget v0, Lcom/google/android/gms/g;->cn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/b/c;->n:Landroid/widget/TextView;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/b/a;

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/b/c;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/b/a;->a(Lcom/google/android/gms/games/ui/common/b/a;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 71
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/b/a;

    .line 76
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/b/a;->d(Lcom/google/android/gms/games/ui/common/b/a;)Lcom/google/android/gms/games/ui/common/b/b;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/b/a;->b(Lcom/google/android/gms/games/ui/common/b/a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/b/a;->c(Lcom/google/android/gms/games/ui/common/b/a;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/ui/common/b/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method
