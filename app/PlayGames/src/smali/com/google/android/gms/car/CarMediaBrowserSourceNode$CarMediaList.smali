.class public Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[B

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/car/k;

    invoke-direct {v0}, Lcom/google/android/gms/car/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->a:I

    .line 71
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[BI)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->a:I

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->b:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->c:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->d:[B

    .line 66
    iput p5, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->e:I

    .line 67
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 80
    invoke-static {p0, p1}, Lcom/google/android/gms/car/k;->a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;Landroid/os/Parcel;)V

    .line 81
    return-void
.end method
