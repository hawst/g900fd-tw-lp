.class public Lcom/google/android/gms/reminders/model/DateTimeEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/reminders/model/DateTime;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private final b:Ljava/lang/Integer;

.field private final c:Ljava/lang/Integer;

.field private final d:Ljava/lang/Integer;

.field private final e:Lcom/google/android/gms/reminders/model/TimeEntity;

.field private final f:Ljava/lang/Integer;

.field private final g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/reminders/model/b;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/reminders/model/TimeEntity;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->b:Ljava/lang/Integer;

    .line 98
    iput-object p3, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->c:Ljava/lang/Integer;

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->d:Ljava/lang/Integer;

    .line 100
    iput-object p5, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->e:Lcom/google/android/gms/reminders/model/TimeEntity;

    .line 101
    iput-object p6, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->f:Ljava/lang/Integer;

    .line 102
    iput-object p7, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->g:Ljava/lang/Long;

    .line 103
    iput p1, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->a:I

    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/reminders/model/DateTime;)V
    .locals 8

    .prologue
    .line 58
    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->e()Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->f()Lcom/google/android/gms/reminders/model/Time;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->g()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->h()Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/reminders/model/DateTimeEntity;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/reminders/model/Time;Ljava/lang/Integer;Ljava/lang/Long;Z)V

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/reminders/model/Time;Ljava/lang/Integer;Ljava/lang/Long;Z)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->a:I

    .line 76
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->b:Ljava/lang/Integer;

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->c:Ljava/lang/Integer;

    .line 78
    iput-object p3, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->d:Ljava/lang/Integer;

    .line 79
    iput-object p5, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->f:Ljava/lang/Integer;

    .line 80
    iput-object p6, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->g:Ljava/lang/Long;

    .line 81
    if-eqz p7, :cond_0

    .line 82
    check-cast p4, Lcom/google/android/gms/reminders/model/TimeEntity;

    .line 84
    :goto_0
    iput-object p4, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->e:Lcom/google/android/gms/reminders/model/TimeEntity;

    .line 86
    return-void

    .line 84
    :cond_0
    if-nez p4, :cond_1

    const/4 p4, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/reminders/model/TimeEntity;

    invoke-direct {v0, p4}, Lcom/google/android/gms/reminders/model/TimeEntity;-><init>(Lcom/google/android/gms/reminders/model/Time;)V

    move-object p4, v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 219
    instance-of v2, p1, Lcom/google/android/gms/reminders/model/DateTime;

    if-nez v2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 223
    goto :goto_0

    .line 226
    :cond_2
    check-cast p1, Lcom/google/android/gms/reminders/model/DateTime;

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->c()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->d()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->e()Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->e()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->f()Lcom/google/android/gms/reminders/model/Time;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->f()Lcom/google/android/gms/reminders/model/Time;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->g()Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->g()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->h()Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/DateTime;->h()Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Lcom/google/android/gms/reminders/model/Time;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->e:Lcom/google/android/gms/reminders/model/TimeEntity;

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public final g_()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->g:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 231
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->b:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->c:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->e:Lcom/google/android/gms/reminders/model/TimeEntity;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->f:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTimeEntity;->g:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 169
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/reminders/model/b;->a(Lcom/google/android/gms/reminders/model/DateTimeEntity;Landroid/os/Parcel;I)V

    .line 170
    return-void
.end method
