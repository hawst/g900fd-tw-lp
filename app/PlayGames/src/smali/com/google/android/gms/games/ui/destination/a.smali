.class public abstract Lcom/google/android/gms/games/ui/destination/a;
.super Lcom/google/android/gms/games/ui/m;
.source "SourceFile"


# instance fields
.field protected d:Lcom/google/android/gms/games/ui/destination/b;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/m;-><init>(I)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/m;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 34
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    if-eqz v1, :cond_0

    .line 35
    check-cast v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    .line 36
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->aj()V

    .line 38
    :cond_0
    return-void
.end method

.method public final ah()Lcom/google/android/gms/games/app/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/a;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->d(Landroid/os/Bundle;)V

    .line 26
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/a;->d:Lcom/google/android/gms/games/ui/destination/b;

    .line 27
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/gms/games/ui/m;->v()V

    .line 43
    instance-of v0, p0, Lcom/google/android/gms/games/ui/destination/r;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 44
    check-cast v0, Lcom/google/android/gms/games/ui/destination/r;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/a;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/b;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/destination/r;->a(Lcom/google/android/gms/games/app/a;)V

    .line 46
    :cond_0
    return-void
.end method
