.class public final Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/g;
.implements Lcom/google/android/gms/games/ui/c/a/j;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# instance fields
.field private A:I

.field private z:Lcom/google/android/gms/games/ui/destination/matches/a;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50
    const v0, 0x7f04005e

    const v1, 0x7f11000c

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZZ)V

    .line 52
    return-void
.end method


# virtual methods
.method public final Q()Lcom/google/android/gms/games/ui/c/a/f;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final ah()Lcom/google/android/gms/games/ui/c/a/i;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/destination/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/matches/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    .line 64
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    if-ne v0, v2, :cond_1

    .line 65
    const-string v0, "DestMultiplayerListAct"

    const-string v1, "Fragment Index not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->finish()V

    goto :goto_0

    .line 69
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/k;-><init>()V

    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/ag;->a()Landroid/support/v4/app/ar;

    move-result-object v1

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/ar;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ar;

    invoke-virtual {v1}, Landroid/support/v4/app/ar;->a()I

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    const v0, 0x7f0f00ee

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationMultiplayerListActivity;->setTitle(I)V

    goto/16 :goto_0

    :pswitch_5
    const v0, 0x7f0f00ef

    goto :goto_2

    :pswitch_6
    const v0, 0x7f0f00f2

    goto :goto_2

    :pswitch_7
    const v0, 0x7f0f00ed

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
