.class final Lcom/google/android/gms/games/ui/destination/onboarding/a;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/a;->a:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    .line 120
    const v0, 0x7f040076

    const/4 v1, -0x1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 121
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/a;->b:Landroid/view/LayoutInflater;

    .line 122
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 128
    if-nez p2, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/a;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f040076

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 130
    new-instance v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/games/ui/destination/onboarding/b;-><init>(Lcom/google/android/gms/games/ui/destination/onboarding/a;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 134
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;

    .line 136
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/onboarding/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 137
    iput-object v1, v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;->b:Landroid/accounts/Account;

    .line 138
    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;->a:Landroid/widget/TextView;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    return-object p2
.end method
