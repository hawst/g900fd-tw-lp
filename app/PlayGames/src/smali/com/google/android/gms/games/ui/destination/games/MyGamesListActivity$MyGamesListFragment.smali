.class public final Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;
.super Lcom/google/android/gms/games/ui/destination/i;
.source "SourceFile"


# static fields
.field private static final g:[Lcom/google/android/gms/games/ui/e/ai;

.field private static final h:Lcom/google/android/gms/games/ui/e/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/games/ui/e/ai;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ai;

    const v4, 0x7f0f0077

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ag;

    const v4, 0x7f0f0073

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ah;

    const v4, 0x7f0f0076

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    .line 59
    new-instance v0, Lcom/google/android/gms/games/ui/e/ah;

    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/e/ah;-><init>([Lcom/google/android/gms/games/ui/e/ai;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    const v1, 0x7f040096

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/i;-><init>(Lcom/google/android/gms/games/ui/e/ah;I)V

    .line 68
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public final V()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 103
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->a(Landroid/view/ViewGroup;)V

    .line 104
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->d(Landroid/os/Bundle;)V

    .line 74
    if-nez p1, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity$MyGamesListFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 76
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 79
    :cond_0
    return-void
.end method
