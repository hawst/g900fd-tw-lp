.class public final Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/p;
.implements Lcom/google/android/gms/games/ui/common/requests/c;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# instance fields
.field private A:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private B:Ljava/lang/String;

.field private z:Lcom/google/android/gms/games/ui/destination/requests/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    const v0, 0x7f04004a

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/internal/request/GameRequestCluster;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->A:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    return-object v0
.end method

.method public final ai()Lcom/google/android/gms/games/ui/c/a/o;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->A:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->B:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/google/android/gms/games/ui/destination/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/requests/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->A:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->i()I

    move-result v0

    .line 52
    packed-switch v0, :pswitch_data_0

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :pswitch_0
    const v0, 0x7f0f018a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->setTitle(I)V

    .line 65
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->A:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 58
    :pswitch_1
    const v0, 0x7f0f018c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationPublicRequestActivity;->setTitle(I)V

    goto :goto_1

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
