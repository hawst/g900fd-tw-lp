.class public final Lcom/google/android/gms/games/ui/common/requests/j;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/at;
.implements Lcom/google/android/gms/games/ui/common/requests/e;
.implements Lcom/google/android/gms/games/ui/common/requests/h;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/requests/d;

.field private an:Lcom/google/android/gms/games/ui/common/requests/g;

.field private ao:Lcom/google/android/gms/games/ui/ac;

.field private ap:Lcom/google/android/gms/games/ui/common/requests/l;

.field private aq:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method

.method private as()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/requests/g;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 195
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->at()V

    .line 196
    return-void
.end method

.method private at()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/requests/g;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 206
    iget v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    if-le v3, v1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    if-lez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 212
    return-void

    :cond_1
    move v0, v2

    .line 207
    goto :goto_0

    :cond_2
    move v1, v2

    .line 211
    goto :goto_1
.end method

.method private au()V
    .locals 5

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    const-string v0, "ReqFrag"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 280
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 281
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 282
    sget-object v1, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    iget v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0

    .line 285
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v1

    iget v4, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public static b(I)Lcom/google/android/gms/games/ui/common/requests/j;
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/j;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/requests/j;-><init>()V

    .line 60
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 61
    const-string v2, "request_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/j;->g(Landroid/os/Bundle;)V

    .line 64
    return-object v0
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/gms/games/request/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/e;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-interface {p1, v1}, Lcom/google/android/gms/games/request/e;->a(I)Lcom/google/android/gms/games/request/a;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->r()V

    new-instance v2, Lcom/google/android/gms/games/ui/common/requests/a;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/ui/common/requests/a;-><init>(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/requests/a;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->a()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->at()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->x()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 136
    :goto_0
    return-void

    .line 131
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    iget v3, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-interface {v1, p1, v2, v0, v3}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/Game;)V

    .line 217
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    .line 228
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->as()V

    .line 229
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 183
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->as()V

    .line 184
    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 178
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->c()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->P()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    const-string v0, "ReqFrag"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 294
    const-string v0, "openAllButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/common/data/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/requests/d;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/requests/g;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/b;->a([Lcom/google/android/gms/common/data/b;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/l;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 299
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 73
    const-string v1, "request_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Must specify a request type!"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 75
    const-string v1, "request_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    .line 77
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    .line 78
    iget v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    packed-switch v0, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/common/requests/j;->aq:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bt:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bu:I

    const-string v2, "openAllButton"

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 95
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/requests/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    .line 96
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/g;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/requests/g;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/requests/m;->e()Lcom/google/android/gms/games/ui/common/requests/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ap:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 101
    sget v0, Lcom/google/android/gms/f;->t:I

    sget v1, Lcom/google/android/gms/l;->bw:I

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/gms/games/ui/common/requests/j;->a(III)V

    .line 104
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->am:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/j;->an:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 108
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/j;->a(Landroid/support/v7/widget/bv;)V

    .line 109
    return-void

    .line 87
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bv:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/j;->ao:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    :goto_0
    return-void

    .line 258
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->au()V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/j;->au()V

    .line 268
    return-void
.end method
