.class public final Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"


# instance fields
.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    const v0, 0x7f04003a

    const v1, 0x7f11000e

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->z:Z

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->a(Landroid/os/Bundle;)V

    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->z:Z

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->z:Z

    .line 94
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;)V

    .line 96
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 56
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 58
    const-string v3, "com.google.android.gms.games.destination.VIEW_MY_PROFILE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 66
    :goto_0
    const-string v3, "com.google.android.gms.games.NOTIFICATION_OPENED"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->z:Z

    .line 69
    iget-object v1, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    .line 70
    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/s;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/s;-><init>()V

    .line 71
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/ag;->a()Landroid/support/v4/app/ar;

    move-result-object v1

    .line 72
    const v2, 0x7f0c0122

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/ar;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ar;

    .line 73
    invoke-virtual {v1}, Landroid/support/v4/app/ar;->a()I

    .line 74
    return-void

    .line 60
    :cond_0
    const-string v3, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    const/4 v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Intent action is invalid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "EXTRA_OTHER_PLAYER and EXTRA_PLAYER_ID cannot both be null for the Profile comparison screen"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/x;-><init>()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 130
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 127
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;)V

    .line 128
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c028c
        :pswitch_0
    .end packed-switch
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onStop()V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LAUNCHED_VIA_API"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailActivity;->finish()V

    .line 86
    :cond_0
    return-void
.end method
