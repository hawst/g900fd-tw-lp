.class public final Lcom/google/android/gms/games/ui/c/a/h;
.super Lcom/google/android/gms/games/ui/c/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a/a;-><init>()V

    .line 22
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lcom/google/android/gms/games/ui/c/a/h;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/games/ui/c/a/h;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/a/h;-><init>()V

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v2, "match"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 46
    const-string v2, "isRematch"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/a/h;->g(Landroid/os/Bundle;)V

    .line 49
    return-object v0
.end method


# virtual methods
.method protected final P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 55
    const-string v1, "match"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 56
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final Q()V
    .locals 4

    .prologue
    .line 61
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 62
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 63
    const-string v0, "match"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 64
    const-string v3, "isRematch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 67
    instance-of v3, v1, Lcom/google/android/gms/games/ui/c/a/i;

    if-eqz v3, :cond_0

    .line 68
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/i;

    .line 78
    :goto_0
    if-eqz v2, :cond_2

    .line 79
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/c/a/i;->e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 83
    :goto_1
    return-void

    .line 69
    :cond_0
    instance-of v3, v1, Lcom/google/android/gms/games/ui/c/a/j;

    if-eqz v3, :cond_1

    .line 70
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/j;

    .line 71
    invoke-interface {v1}, Lcom/google/android/gms/games/ui/c/a/j;->ah()Lcom/google/android/gms/games/ui/c/a/i;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/c/a/i;

    goto :goto_0

    .line 73
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MatchChangeAccountDialogFragment must be used with a parent Activity which implements MatchAccountSwitcher or MatchAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_2
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/c/a/i;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_1
.end method
