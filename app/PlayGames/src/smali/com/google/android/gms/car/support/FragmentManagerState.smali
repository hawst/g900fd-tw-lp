.class final Lcom/google/android/gms/car/support/FragmentManagerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:[Lcom/google/android/gms/car/support/FragmentState;

.field b:[I

.field c:[Lcom/google/android/gms/car/support/BackStackState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/google/android/gms/car/support/q;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/FragmentManagerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    sget-object v0, Lcom/google/android/gms/car/support/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/car/support/FragmentState;

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    .line 367
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    .line 368
    sget-object v0, Lcom/google/android/gms/car/support/BackStackState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/car/support/BackStackState;

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    .line 369
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->a:[Lcom/google/android/gms/car/support/FragmentState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->b:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentManagerState;->c:[Lcom/google/android/gms/car/support/BackStackState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 381
    return-void
.end method
