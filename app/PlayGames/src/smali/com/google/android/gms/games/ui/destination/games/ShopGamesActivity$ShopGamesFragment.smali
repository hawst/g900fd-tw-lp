.class public final Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;
.super Lcom/google/android/gms/games/ui/destination/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bb;


# static fields
.field private static final g:[Lcom/google/android/gms/games/ui/e/ai;

.field private static final h:Lcom/google/android/gms/games/ui/e/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/games/ui/e/ai;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ac;

    const v4, 0x7f0f0075

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ad;

    const v4, 0x7f0f0079

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/games/ae;

    const v4, 0x7f0f0078

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    .line 72
    new-instance v0, Lcom/google/android/gms/games/ui/e/ah;

    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/e/ah;-><init>([Lcom/google/android/gms/games/ui/e/ai;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    const v1, 0x7f040096

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/i;-><init>(Lcom/google/android/gms/games/ui/e/ah;I)V

    .line 81
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public final V()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;)V

    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->a(Landroid/view/ViewGroup;)V

    .line 126
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->d(Landroid/os/Bundle;)V

    .line 87
    if-nez p1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity$ShopGamesFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 89
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 93
    :cond_0
    return-void
.end method
