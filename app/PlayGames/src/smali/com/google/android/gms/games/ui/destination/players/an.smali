.class public final Lcom/google/android/gms/games/ui/destination/players/an;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/games/ui/destination/players/ao;

.field private g:Lcom/google/android/gms/games/Player;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/ao;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 44
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/an;->e:Lcom/google/android/gms/games/ui/destination/players/ao;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/an;->g:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/an;)Lcom/google/android/gms/games/ui/destination/players/ao;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/an;->e:Lcom/google/android/gms/games/ui/destination/players/ao;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/ap;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/an;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f040097

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/ap;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 1

    .prologue
    .line 49
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/an;->g:Lcom/google/android/gms/games/Player;

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/an;->c(Z)V

    .line 51
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f040097

    return v0
.end method
