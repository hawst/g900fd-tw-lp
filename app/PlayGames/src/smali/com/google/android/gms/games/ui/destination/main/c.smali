.class final Lcom/google/android/gms/games/ui/destination/main/c;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 72
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x7f02015c

    const/4 v3, 0x2

    .line 68
    check-cast p3, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->c(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    check-cast p1, Lcom/google/android/gms/games/ui/destination/main/a;

    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/destination/main/a;->w()I

    move-result v0

    if-eq v0, v3, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/games/ui/destination/main/c;->a(Landroid/net/Uri;I)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->a(IF)V

    :goto_0
    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v0

    const v2, 0x7f020090

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/destination/main/c;->b(Landroid/net/Uri;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/c;->t()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/c;->u()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/Game;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->b(Landroid/database/CharArrayBuffer;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/c;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->f(Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v0

    const/16 v2, 0xb

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/games/ui/destination/main/c;->a(Landroid/net/Uri;I)V

    const v0, 0x4003126f    # 2.048f

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/destination/main/c;->a(IF)V

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/main/a;

    .line 126
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/main/a;->w()I

    move-result v3

    .line 128
    if-eq v3, v1, :cond_0

    if-ne v3, v2, :cond_1

    :cond_0
    move v1, v2

    .line 131
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/a;->a(Lcom/google/android/gms/games/ui/destination/main/a;)Lcom/google/android/gms/games/ui/destination/main/b;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/c;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v2, v0, v1, p1}, Lcom/google/android/gms/games/ui/destination/main/b;->b(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 132
    return-void
.end method
