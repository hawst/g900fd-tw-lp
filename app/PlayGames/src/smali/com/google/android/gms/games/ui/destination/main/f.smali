.class final Lcom/google/android/gms/games/ui/destination/main/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/r;

.field final synthetic b:Lcom/google/android/gms/common/api/r;

.field final synthetic c:Lcom/google/android/gms/games/ui/destination/main/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V
    .locals 0

    .prologue
    .line 1041
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/main/f;->a:Lcom/google/android/gms/common/api/r;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/main/f;->b:Lcom/google/android/gms/common/api/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 16

    .prologue
    .line 1041
    check-cast p1, Lcom/google/android/gms/common/api/q;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->a:Lcom/google/android/gms/common/api/r;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/u;

    invoke-interface {v1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->g()I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/destination/main/f;->b:Lcom/google/android/gms/common/api/r;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/u;

    invoke-interface {v2}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->g()I

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->j(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->k(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->l(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    invoke-interface {v1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->a()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->m(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/destination/b;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    new-instance v11, Lcom/google/android/gms/common/data/p;

    invoke-direct {v11}, Lcom/google/android/gms/common/data/p;-><init>()V

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v10}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v13

    move v7, v3

    :goto_1
    if-ge v7, v13, :cond_4

    invoke-virtual {v10, v7}, Lcom/google/android/gms/common/data/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/Player;

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/Player;

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    move-result-object v14

    if-eqz v14, :cond_3

    invoke-interface {v14}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->c()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/f;

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/gms/games/ui/destination/players/f;

    invoke-direct {v4, v14}, Lcom/google/android/gms/games/ui/destination/players/f;-><init>(Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V

    invoke-virtual {v12, v15, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11, v4}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    :cond_2
    iget-object v4, v4, Lcom/google/android/gms/games/ui/destination/players/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    invoke-virtual {v11}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v1

    if-nez v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/main/d;->n(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/main/d;->o(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/players/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/players/g;->b()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/main/d;->p(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/card/aa;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    const/4 v1, 0x1

    :goto_2
    invoke-interface {v2}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->a()I

    move-result v4

    if-nez v4, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->q(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/common/players/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/players/a;->b()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/main/d;->r(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    invoke-static {v3, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Lcom/google/android/gms/games/ui/destination/main/d;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v5

    :goto_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/main/d;->i(Lcom/google/android/gms/games/ui/destination/main/d;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    goto/16 :goto_0

    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v1, v11}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/data/b;)V

    move v1, v6

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/main/f;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v4, v3, v8, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/games/o;IZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v3, 0x0

    goto :goto_4

    :catchall_0
    move-exception v1

    move v3, v5

    :goto_5
    if-eqz v3, :cond_8

    invoke-interface {v2}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/o;->f_()V

    :cond_8
    throw v1

    :catchall_1
    move-exception v1

    goto :goto_5
.end method
