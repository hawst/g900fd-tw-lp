.class public final Lcom/google/android/gms/games/ui/destination/main/d;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/c/a/m;
.implements Lcom/google/android/gms/games/ui/card/ac;
.implements Lcom/google/android/gms/games/ui/common/a/k;
.implements Lcom/google/android/gms/games/ui/common/a/m;
.implements Lcom/google/android/gms/games/ui/common/players/b;
.implements Lcom/google/android/gms/games/ui/destination/games/c;
.implements Lcom/google/android/gms/games/ui/destination/main/b;
.implements Lcom/google/android/gms/games/ui/destination/main/o;
.implements Lcom/google/android/gms/games/ui/destination/players/ao;
.implements Lcom/google/android/gms/games/ui/destination/players/h;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/j;


# static fields
.field private static final an:Ljava/lang/String;


# instance fields
.field private aA:Lcom/google/android/gms/games/ui/card/aa;

.field private aB:Lcom/google/android/gms/games/ui/ac;

.field private aC:Lcom/google/android/gms/games/ui/destination/main/a;

.field private aD:Lcom/google/android/gms/games/ui/ac;

.field private aE:Lcom/google/android/gms/games/ui/card/aa;

.field private aF:Lcom/google/android/gms/games/ui/common/players/a;

.field private aG:Lcom/google/android/gms/games/ui/card/aa;

.field private aH:Lcom/google/android/gms/games/ui/ak;

.field private aI:Lcom/google/android/gms/games/ui/az;

.field private aJ:Z

.field private aK:Z

.field private aL:Ljava/lang/String;

.field private aM:Lcom/google/android/gms/games/Player;

.field private aN:I

.field private aO:Z

.field private aP:Ljava/util/concurrent/CountDownLatch;

.field private aQ:Z

.field private aR:Z

.field private aS:I

.field private aT:Ljava/lang/String;

.field private aU:Ljava/lang/String;

.field private aV:Lcom/google/android/gms/games/ui/common/a/b;

.field private aW:Z

.field private aX:J

.field private ao:Lcom/google/android/gms/games/ui/destination/main/m;

.field private ap:Lcom/google/android/gms/games/ui/card/aa;

.field private aq:Lcom/google/android/gms/games/ui/card/aa;

.field private ar:Lcom/google/android/gms/games/ui/destination/players/an;

.field private as:Lcom/google/android/gms/games/ui/ac;

.field private at:Lcom/google/android/gms/games/ui/common/players/a;

.field private au:Lcom/google/android/gms/games/ui/destination/quests/c;

.field private av:Lcom/google/android/gms/games/ui/ac;

.field private aw:Lcom/google/android/gms/games/ui/destination/games/a;

.field private ax:Lcom/google/android/gms/games/ui/card/aa;

.field private ay:Lcom/google/android/gms/games/ui/ac;

.field private az:Lcom/google/android/gms/games/ui/destination/players/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    .line 1307
    return-void
.end method

.method private a(Lcom/google/android/gms/common/data/b;ILcom/google/android/gms/games/ui/e;)V
    .locals 4

    .prologue
    .line 1355
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v0

    .line 1357
    if-gt v0, p2, :cond_0

    .line 1360
    invoke-virtual {p3, p1}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/common/data/b;)V

    .line 1370
    :goto_0
    return-void

    .line 1363
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "shuffleHomePageSeed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    .line 1366
    new-instance v2, Lcom/google/android/gms/games/ui/destination/c/a;

    invoke-direct {v2, p1, p2, v0, v1}, Lcom/google/android/gms/games/ui/destination/c/a;-><init>(Lcom/google/android/gms/common/data/b;IJ)V

    .line 1368
    invoke-virtual {p3, v2}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/common/data/b;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ar:Lcom/google/android/gms/games/ui/destination/players/an;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 503
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/data/b;)V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    const v2, 0x7f0d0035

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->az:Lcom/google/android/gms/games/ui/destination/players/g;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/common/data/b;ILcom/google/android/gms/games/ui/e;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aA:Lcom/google/android/gms/games/ui/card/aa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/games/o;IZ)V
    .locals 11

    .prologue
    const v10, 0x7f0d0046

    const v9, 0x7f0d001e

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->j()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aR:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    if-gt p2, v0, :cond_2

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    mul-int v6, v0, v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-direct {p0, p1, v6, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/common/data/b;ILcom/google/android/gms/games/ui/e;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v8

    move v3, v2

    :goto_0
    if-ge v3, v8, :cond_0

    invoke-virtual {v7, v3}, Lcom/google/android/gms/common/data/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-le v0, v6, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    :goto_2
    if-eqz v0, :cond_7

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    mul-int v4, v0, v3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    new-instance v6, Lcom/google/android/gms/common/data/l;

    const-string v0, "external_player_id"

    invoke-direct {v6, p1, v0}, Lcom/google/android/gms/common/data/l;-><init>(Lcom/google/android/gms/common/data/b;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_3
    if-ge v3, v7, :cond_4

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/common/data/l;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->b()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aQ:Z

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1}, Lcom/google/android/gms/games/o;->a()I

    move-result v3

    if-le v3, v4, :cond_6

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    :goto_6
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/common/data/b;ILcom/google/android/gms/games/ui/e;)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_5

    :cond_7
    if-nez p3, :cond_8

    :goto_7
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Z)V

    goto :goto_6

    :cond_8
    move v1, v2

    goto :goto_7
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/d;Z)Z
    .locals 0

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aW:Z

    return p1
.end method

.method static synthetic au()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    return-object v0
.end method

.method private av()V
    .locals 4

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aP:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 877
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aP:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 878
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aX:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/app/a;->a(ZJ)V

    .line 880
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 882
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 507
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 508
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aR:Z

    .line 509
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/main/d;Z)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->b()V

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 1232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 1233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aG:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 1234
    return-void
.end method

.method private varargs c(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 718
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "### Game clicked: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 721
    return-void
.end method

.method private c(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 513
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aW:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/m;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    return-object v0
.end method

.method private d(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->av:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 517
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aw:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ax:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 519
    return-void
.end method

.method private e(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->az:Lcom/google/android/gms/games/ui/destination/players/g;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aA:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 525
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method private f(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aB:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 529
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aC:Lcom/google/android/gms/games/ui/destination/main/a;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 530
    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/games/a;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aw:Lcom/google/android/gms/games/ui/destination/games/a;

    return-object v0
.end method

.method private g(Lcom/google/android/gms/games/ui/am;)V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 534
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aG:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 537
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/card/aa;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ax:Lcom/google/android/gms/games/ui/card/aa;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/destination/main/d;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->av()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z

    move-result v0

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/players/g;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->az:Lcom/google/android/gms/games/ui/destination/players/g;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/card/aa;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aA:Lcom/google/android/gms/games/ui/card/aa;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/common/players/a;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z

    move-result v0

    return v0
.end method

.method static synthetic t(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aB:Lcom/google/android/gms/games/ui/ac;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/a;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aC:Lcom/google/android/gms/games/ui/destination/main/a;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method


# virtual methods
.method public final D()Z
    .locals 1

    .prologue
    .line 897
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aW:Z

    return v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 593
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/h;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 594
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 595
    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 675
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/h;->a(IILandroid/content/Intent;)V

    .line 677
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 679
    invoke-static {p3}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v0

    .line 681
    invoke-interface {v0}, Lcom/google/android/gms/common/a/a/c;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/common/a/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 684
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, ""

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aL:Ljava/lang/String;

    const/4 v4, 0x6

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 687
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aO:Z

    if-eqz v0, :cond_3

    .line 688
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aM:Lcom/google/android/gms/games/Player;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aN:I

    invoke-virtual {v0, v1, p3, v2}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;I)V

    .line 695
    :cond_1
    :goto_1
    return-void

    .line 681
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 691
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aM:Lcom/google/android/gms/games/Player;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aN:I

    invoke-virtual {v0, v1, p3, v2}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 615
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/o;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 616
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 619
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    .line 620
    if-nez v0, :cond_1

    .line 622
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 671
    :goto_0
    return-void

    .line 628
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ar:Lcom/google/android/gms/games/ui/destination/players/an;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/an;->a(Lcom/google/android/gms/games/Player;)V

    .line 630
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aL:Ljava/lang/String;

    .line 631
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aL:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 633
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    goto :goto_0

    .line 639
    :cond_2
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aP:Ljava/util/concurrent/CountDownLatch;

    .line 642
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/h;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/games/ui/destination/main/h;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 646
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v3

    .line 647
    new-array v4, v5, [I

    const/4 v0, 0x2

    aput v0, v4, v6

    .line 648
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    const/4 v2, 0x0

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[IIZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/main/e;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aw:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->c()I

    move-result v0

    .line 659
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/i;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/games/ui/destination/main/i;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 664
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/i;->c(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/g;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/games/ui/destination/main/g;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 670
    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/p;-><init>(Lcom/google/android/gms/common/api/t;)V

    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v2, "circled"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->c(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/gms/games/t;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v3, "you_may_know"

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v4

    invoke-interface {v2, p1, v3, v4}, Lcom/google/android/gms/games/t;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/games/ui/destination/main/f;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/main/f;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/an;)V

    goto/16 :goto_0
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    .line 844
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 845
    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 852
    :goto_0
    return-void

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 4

    .prologue
    .line 600
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aX:J

    .line 601
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aX:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/games/app/a;->a(ZJ)V

    .line 603
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 726
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 727
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 2

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/app/b;->b(Landroid/content/Context;Lcom/google/android/gms/games/Game;)V

    .line 950
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/quest/f;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 953
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    .line 954
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->c()Lcom/google/android/gms/games/quest/b;

    move-result-object v3

    .line 956
    sget-object v4, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onQuestsLoaded... statusCode = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", data = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 974
    invoke-virtual {v3}, Lcom/google/android/gms/games/quest/b;->f_()V

    :goto_0
    return-void

    .line 963
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    .line 974
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aT:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aU:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/quest/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-virtual {v3}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->c(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 972
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->av()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 975
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/games/quest/b;->f_()V

    :cond_1
    throw v0

    :cond_2
    move v0, v1

    .line 969
    goto :goto_1

    .line 975
    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V
    .locals 4

    .prologue
    .line 866
    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aM:Lcom/google/android/gms/games/Player;

    .line 867
    iput p3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aN:I

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aO:Z

    .line 869
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aL:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 871
    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 873
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/ui/destination/players/f;I[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 786
    iget-object v0, p1, Lcom/google/android/gms/games/ui/destination/players/f;->a:Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    .line 789
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, p2, p3}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Ljava/lang/String;I[Landroid/util/Pair;)V

    .line 791
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    return-void
.end method

.method public final af()Lcom/google/android/gms/games/ui/c/a/l;
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aV:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method

.method public final at()V
    .locals 3

    .prologue
    .line 890
    new-instance v0, Lcom/google/android/gms/games/ui/c/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/e;-><init>()V

    .line 891
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "com.google.android.gms.games.ui.dialog.privateLevelDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 893
    return-void
.end method

.method public final varargs b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 857
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    .prologue
    .line 731
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Play button clicked -- Launching "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 734
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    .line 735
    if-eqz v0, :cond_0

    .line 736
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aL:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 738
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    .line 743
    :goto_0
    return-void

    .line 741
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final varargs b(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 753
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 754
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 801
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 802
    const-string v1, "welcomeButton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 803
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    .line 804
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ap:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 806
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "showWarmWelcome"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 809
    if-eqz v0, :cond_0

    .line 810
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->ab()V

    .line 837
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    const-string v1, "whatsNewButton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 813
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aK:Z

    .line 814
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aq:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 816
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "lastVersionWhatsNewShown"

    iget v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 820
    if-eqz v0, :cond_0

    .line 821
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->ab()V

    goto :goto_0

    .line 823
    :cond_2
    const-string v0, "continuePlayingNullButton"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 824
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 825
    const-string v1, "com.google.android.gms.games.TAB"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 826
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 827
    :cond_3
    const-string v0, "friendsArePlayingNullButton"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "gamersYouMayKnowNullButton"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 829
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 830
    :cond_5
    const-string v0, "gamersYouMayKnowWelcomeButton"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aQ:Z

    .line 832
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 834
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "showYouMayKnowWelcome"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final b_(Z)V
    .locals 2

    .prologue
    .line 902
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 913
    :goto_0
    return-void

    .line 906
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 907
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/main/m;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 910
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->ab()V

    .line 912
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/destination/main/m;->a(Z)V

    goto :goto_0
.end method

.method public final varargs c(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 714
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 760
    const-string v0, "myGames"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 761
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 762
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 764
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/content/Intent;)V

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    const-string v0, "featured"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 766
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 767
    const-string v1, "com.google.android.gms.games.TAB"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 768
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 769
    :cond_2
    const-string v0, "friendsArePlaying"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 770
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 771
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 773
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 774
    :cond_3
    const-string v0, "youMayKnow"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 776
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 778
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const v12, 0x7f0d0046

    const/4 v3, 0x0

    const/4 v6, 0x1

    const v11, 0x7f0f004f

    const/4 v7, 0x0

    .line 203
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityCreated()...  this = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/main/d;->j()Landroid/content/res/Resources;

    move-result-object v1

    .line 207
    const v0, 0x7f090006

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    .line 210
    if-nez p1, :cond_5

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "showWarmWelcome"

    invoke-static {v0, v2, v6}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "lastVersionWhatsNewShown"

    const-string v4, "playGames.sharedPrefs"

    invoke-virtual {v0, v4, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, v2, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    if-le v2, v0, :cond_3

    move v0, v6

    :goto_0
    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    if-nez v0, :cond_4

    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aK:Z

    .line 224
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "lastVersionWhatsNewShown"

    iget v4, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    invoke-static {v0, v2, v4}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 231
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/az;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    const v4, 0x7f0b00d6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v0, v2, v1, v7}, Lcom/google/android/gms/games/ui/az;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aI:Lcom/google/android/gms/games/ui/az;

    .line 235
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/a/b;-><init>(Lcom/google/android/gms/games/ui/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aV:Lcom/google/android/gms/games/ui/common/a/b;

    .line 242
    new-instance v0, Lcom/google/android/gms/games/ui/destination/main/m;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/destination/main/m;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/destination/main/o;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/destination/main/m;->c(Z)V

    .line 247
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v4, 0x7f020096

    const v5, 0x7f0f01dc

    const v9, 0x7f0f01db

    const v10, 0x7f0f015b

    invoke-direct {v2, v4, v5, v9, v10}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v4, "welcomeButton"

    invoke-direct {v0, v1, v2, p0, v4}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ap:Lcom/google/android/gms/games/ui/card/aa;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ap:Lcom/google/android/gms/games/ui/card/aa;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 255
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v4, 0x7f020097

    const v5, 0x7f0f01de

    const v9, 0x7f0f01dd

    const v10, 0x7f0f015b

    invoke-direct {v2, v4, v5, v9, v10}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v4, "whatsNewButton"

    invoke-direct {v0, v1, v2, p0, v4}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aq:Lcom/google/android/gms/games/ui/card/aa;

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aq:Lcom/google/android/gms/games/ui/card/aa;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aK:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 263
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/an;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/destination/players/an;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/ao;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ar:Lcom/google/android/gms/games/ui/destination/players/an;

    .line 267
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    const v1, 0x7f0f007c

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->as:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "youMayKnow"

    invoke-virtual {v0, p0, v11, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 272
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0, v12}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->at:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->d()V

    .line 278
    new-instance v0, Lcom/google/android/gms/games/ui/destination/quests/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aV:Lcom/google/android/gms/games/ui/common/a/b;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/quests/c;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Lcom/google/android/gms/games/ui/common/a/m;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/destination/quests/c;->c(Z)V

    .line 282
    if-nez p1, :cond_6

    .line 283
    iput-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aT:Ljava/lang/String;

    .line 284
    iput-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aU:Ljava/lang/String;

    .line 293
    :goto_3
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->av:Lcom/google/android/gms/games/ui/ac;

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->av:Lcom/google/android/gms/games/ui/ac;

    const v1, 0x7f0f0082

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->av:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "myGames"

    invoke-virtual {v0, p0, v11, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 298
    sget-object v0, Lcom/google/android/gms/games/ui/l;->q:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v8}, Lcom/google/android/gms/games/ui/e/aj;->a(IZ)I

    move-result v5

    .line 300
    packed-switch v5, :pswitch_data_0

    .line 308
    const v4, 0x7f0d0015

    .line 311
    :goto_4
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;II)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aw:Lcom/google/android/gms/games/ui/destination/games/a;

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aw:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/a;->d()V

    .line 316
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v3, 0x7f020093

    const v4, 0x7f0f00d1

    const v5, 0x7f0f00d0

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v3, "continuePlayingNullButton"

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ax:Lcom/google/android/gms/games/ui/card/aa;

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ax:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 325
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    const v1, 0x7f0f007b

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ay:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "friendsArePlaying"

    invoke-virtual {v0, p0, v11, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 331
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/g;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    sget-object v0, Lcom/google/android/gms/games/ui/l;->q:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v8}, Lcom/google/android/gms/games/ui/e/aj;->a(IZ)I

    move-result v0

    invoke-direct {v1, v2, p0, v0}, Lcom/google/android/gms/games/ui/destination/players/g;-><init>(Landroid/app/Activity;Lcom/google/android/gms/games/ui/destination/players/h;I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->az:Lcom/google/android/gms/games/ui/destination/players/g;

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->az:Lcom/google/android/gms/games/ui/destination/players/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/players/g;->d()V

    .line 336
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v3, 0x7f020092

    const v4, 0x7f0f00cf

    const v5, 0x7f0f00ce

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v3, "friendsArePlayingNullButton"

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aA:Lcom/google/android/gms/games/ui/card/aa;

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aA:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 345
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aB:Lcom/google/android/gms/games/ui/ac;

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aB:Lcom/google/android/gms/games/ui/ac;

    const v1, 0x7f0f007d

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aB:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "featured"

    invoke-virtual {v0, p0, v11, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 350
    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    sget-object v0, Lcom/google/android/gms/games/ui/l;->q:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v8}, Lcom/google/android/gms/games/ui/e/aj;->a(IZ)I

    move-result v0

    invoke-direct {v1, v2, p0, v0}, Lcom/google/android/gms/games/ui/destination/main/a;-><init>(Landroid/app/Activity;Lcom/google/android/gms/games/ui/destination/main/b;I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aC:Lcom/google/android/gms/games/ui/destination/main/a;

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aC:Lcom/google/android/gms/games/ui/destination/main/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/main/a;->d()V

    .line 356
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    const v1, 0x7f0f007c

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aD:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "youMayKnow"

    invoke-virtual {v0, p0, v11, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    .line 362
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v3, 0x7f020095

    const v4, 0x7f0f00d2

    const v5, 0x7f0f01d5

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v3, "gamersYouMayKnowWelcomeButton"

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "showYouMayKnowWelcome"

    invoke-static {v0, v1, v6}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aQ:Z

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aE:Lcom/google/android/gms/games/ui/card/aa;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aQ:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 372
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0, v12}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aF:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/a;->d()V

    .line 377
    new-instance v0, Lcom/google/android/gms/games/ui/card/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->am:Lcom/google/android/gms/games/ui/destination/b;

    new-instance v2, Lcom/google/android/gms/games/ui/card/ab;

    const v3, 0x7f020094

    const v4, 0x7f0f00cf

    const v5, 0x7f0f00ce

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/google/android/gms/games/ui/card/ab;-><init>(IIII)V

    const-string v3, "gamersYouMayKnowNullButton"

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aG:Lcom/google/android/gms/games/ui/card/aa;

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aG:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    .line 386
    new-instance v1, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aI:Lcom/google/android/gms/games/ui/az;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ao:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->ap:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aq:Lcom/google/android/gms/games/ui/card/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 395
    sget-object v0, Lcom/google/android/gms/games/ui/l;->o:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 396
    packed-switch v0, :pswitch_data_1

    .line 401
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 403
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    const-string v2, "Unknown ordering flag provided. Using default."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 407
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Lcom/google/android/gms/games/ui/am;)V

    .line 408
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 409
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 410
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 411
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 412
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    .line 489
    :goto_5
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aH:Lcom/google/android/gms/games/ui/ak;

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aH:Lcom/google/android/gms/games/ui/ak;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Landroid/support/v7/widget/bv;)V

    .line 493
    if-eqz p1, :cond_2

    .line 494
    const-string v0, "savedStateAddedPlayer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aM:Lcom/google/android/gms/games/Player;

    .line 495
    const-string v0, "savedStateAddedPlayerPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aN:I

    .line 496
    const-string v0, "savedStateAddedPlayerIsPushAdapter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aO:Z

    .line 499
    :cond_2
    return-void

    :cond_3
    move v0, v7

    .line 216
    goto/16 :goto_0

    :cond_4
    move v0, v7

    goto/16 :goto_1

    .line 219
    :cond_5
    const-string v0, "savedStateShowWelcome"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    .line 220
    const-string v0, "savedStateShowWhatsNew"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aK:Z

    .line 221
    const-string v0, "savedStateCurrentVersion"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    goto/16 :goto_2

    .line 286
    :cond_6
    const-string v0, "savedStateCarouselCurrentQuestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aT:Ljava/lang/String;

    .line 288
    const-string v0, "savedStateCarouselPreviousQuestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aU:Ljava/lang/String;

    goto/16 :goto_3

    .line 302
    :pswitch_0
    const v4, 0x7f0d0013

    .line 303
    goto/16 :goto_4

    .line 305
    :pswitch_1
    const v4, 0x7f0d0014

    .line 306
    goto/16 :goto_4

    .line 417
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 418
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Lcom/google/android/gms/games/ui/am;)V

    .line 419
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 420
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 421
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 422
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 423
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto :goto_5

    .line 428
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 429
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Lcom/google/android/gms/games/ui/am;)V

    .line 430
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 431
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 432
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 433
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 434
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 439
    :pswitch_4
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 440
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->b(Lcom/google/android/gms/games/ui/am;)V

    .line 441
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 442
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 443
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 444
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 445
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 450
    :pswitch_5
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 451
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 452
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 453
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 454
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 455
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 460
    :pswitch_6
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 461
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 462
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 463
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 464
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 465
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 470
    :pswitch_7
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 471
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 472
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 473
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 474
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 475
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 480
    :pswitch_8
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->a(Lcom/google/android/gms/games/ui/am;)V

    .line 481
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->c(Lcom/google/android/gms/games/ui/am;)V

    .line 482
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->d(Lcom/google/android/gms/games/ui/am;)V

    .line 483
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/am;)V

    .line 484
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/am;)V

    .line 485
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/am;)V

    goto/16 :goto_5

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 396
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 571
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->e(Landroid/os/Bundle;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aT:Ljava/lang/String;

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->au:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aU:Ljava/lang/String;

    .line 576
    const-string v0, "savedStateShowWelcome"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aJ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 577
    const-string v0, "savedStateAddedPlayer"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aM:Lcom/google/android/gms/games/Player;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 578
    const-string v0, "savedStateAddedPlayerPosition"

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aN:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 579
    const-string v0, "savedStateAddedPlayerIsPushAdapter"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aO:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 580
    const-string v0, "savedStateShowWhatsNew"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aK:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 581
    const-string v0, "savedStateCarouselCurrentQuestId"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const-string v0, "savedStateCarouselPreviousQuestId"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v0, "savedStateCurrentVersion"

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aS:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 584
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 563
    sget-object v0, Lcom/google/android/gms/games/ui/destination/main/d;->an:Ljava/lang/String;

    const-string v1, "onDestroyView()..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aH:Lcom/google/android/gms/games/ui/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ak;->b()V

    .line 566
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/h;->f()V

    .line 567
    return-void
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 705
    return-void
.end method

.method public final z_()Lcom/google/android/gms/games/ui/common/a/j;
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/d;->aV:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method
