.class final Lcom/google/android/gms/games/ui/common/matches/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/common/matches/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/common/matches/k;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 4

    .prologue
    .line 405
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->o()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->o()Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->e()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Lcom/google/android/gms/games/ui/common/matches/k;I)I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->c(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/k;->b(Lcom/google/android/gms/games/ui/common/matches/k;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/a;->f(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->e(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/k;->d(Lcom/google/android/gms/games/ui/common/matches/k;)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/l;->a:Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/common/matches/k;->b(Lcom/google/android/gms/games/ui/common/matches/k;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    :cond_4
    throw v0
.end method
