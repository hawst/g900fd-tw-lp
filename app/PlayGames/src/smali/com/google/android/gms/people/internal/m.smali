.class public final Lcom/google/android/gms/people/internal/m;
.super Lcom/google/android/gms/common/internal/e;
.source "SourceFile"


# static fields
.field private static volatile j:Landroid/os/Bundle;

.field private static volatile k:Landroid/os/Bundle;


# instance fields
.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field private final i:Ljava/util/HashMap;

.field private l:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;[Ljava/lang/String;)V

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/internal/m;->i:Ljava/util/HashMap;

    .line 2193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/internal/m;->l:Ljava/lang/Long;

    .line 143
    iput-object p5, p0, Lcom/google/android/gms/people/internal/m;->g:Ljava/lang/String;

    .line 144
    iput-object p6, p0, Lcom/google/android/gms/people/internal/m;->h:Ljava/lang/String;

    .line 145
    return-void
.end method

.method static synthetic a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 93
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    return-object v2

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/people/model/i;
    .locals 4

    .prologue
    .line 93
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/people/model/i;

    new-instance v1, Lcom/google/android/gms/people/internal/a/d;

    sget-object v2, Lcom/google/android/gms/people/internal/m;->k:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Lcom/google/android/gms/people/internal/a/d;-><init>(Landroid/os/Bundle;)V

    new-instance v2, Lcom/google/android/gms/people/internal/a/c;

    sget-object v3, Lcom/google/android/gms/people/internal/m;->j:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Lcom/google/android/gms/people/internal/a/c;-><init>(Landroid/os/Bundle;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/people/model/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/people/internal/a/d;Lcom/google/android/gms/people/internal/a/c;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/people/internal/m;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/people/internal/m;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 233
    monitor-enter p0

    if-nez p1, :cond_0

    .line 241
    :goto_0
    monitor-exit p0

    return-void

    .line 236
    :cond_0
    :try_start_0
    const-string v0, "use_contactables_api"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/a/a;->a(Z)V

    .line 238
    sget-object v0, Lcom/google/android/gms/people/internal/l;->a:Lcom/google/android/gms/people/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/internal/l;->a(Landroid/os/Bundle;)V

    .line 239
    const-string v0, "config.email_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/m;->j:Landroid/os/Bundle;

    .line 240
    const-string v0, "config.phone_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/m;->k:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 92
    invoke-static {p1}, Lcom/google/android/gms/people/internal/h;->a(Landroid/os/IBinder;)Lcom/google/android/gms/people/internal/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/internal/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 885
    new-instance v2, Lcom/google/android/gms/people/internal/s;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/people/internal/s;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V

    .line 889
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    const/4 v3, 0x0

    invoke-interface {v0, v2, p2, p3, v3}, Lcom/google/android/gms/people/internal/g;->c(Lcom/google/android/gms/people/internal/d;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/u;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 896
    :goto_0
    return-object v0

    .line 894
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v1, v1, v1}, Lcom/google/android/gms/people/internal/s;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    move-object v0, v1

    .line 896
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/u;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 851
    new-instance v1, Lcom/google/android/gms/people/internal/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/internal/s;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V

    .line 854
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/g;->b(Lcom/google/android/gms/people/internal/d;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/u;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 859
    :goto_0
    return-object v0

    .line 857
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6, v6}, Lcom/google/android/gms/people/internal/s;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    move-object v0, v6

    .line 859
    goto :goto_0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 199
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    .line 200
    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/internal/m;->a(Landroid/os/Bundle;)V

    .line 203
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 205
    return-void

    .line 203
    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)V
    .locals 14

    .prologue
    .line 554
    if-nez p4, :cond_0

    .line 555
    sget-object p4, Lcom/google/android/gms/people/h;->a:Lcom/google/android/gms/people/h;

    .line 557
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->b()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->c()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->d()Z

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->e()J

    move-result-wide v8

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->g()I

    move-result v11

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->h()I

    move-result v12

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->i()I

    move-result v13

    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->h()V

    new-instance v1, Lcom/google/android/gms/people/internal/t;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/internal/t;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V

    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_0
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-interface/range {v0 .. v13}, Lcom/google/android/gms/people/internal/g;->a(Lcom/google/android/gms/people/internal/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    .line 561
    :goto_1
    return-void

    .line 557
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/t;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 459
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->h()V

    .line 462
    new-instance v1, Lcom/google/android/gms/people/internal/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/internal/p;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V

    .line 464
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/people/internal/g;->a(Lcom/google/android/gms/people/internal/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :goto_0
    return-void

    .line 469
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/p;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;ZI)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 377
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->h()V

    .line 380
    new-instance v1, Lcom/google/android/gms/people/internal/r;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/internal/r;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V

    .line 382
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p2

    move v6, p3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/people/internal/g;->a(Lcom/google/android/gms/people/internal/d;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :goto_0
    return-void

    .line 387
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lcom/google/android/gms/people/internal/r;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/aa;Lcom/google/android/gms/common/internal/i;)V
    .locals 3

    .prologue
    .line 187
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 188
    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lcom/google/android/gms/people/internal/m;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lcom/google/android/gms/people/internal/m;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "support_new_image_callback"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 191
    const v1, 0x647e90

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/aa;->b(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 194
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/internal/g;)V
    .locals 3

    .prologue
    .line 166
    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 167
    return-void

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 209
    iget-object v6, p0, Lcom/google/android/gms/people/internal/m;->i:Ljava/util/HashMap;

    monitor-enter v6

    .line 215
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/people/internal/m;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/internal/q;

    .line 217
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/g;->a(Lcom/google/android/gms/people/internal/d;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    :try_start_1
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 225
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/internal/m;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 226
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->d()V

    .line 228
    return-void

    .line 222
    :catch_1
    move-exception v0

    .line 223
    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "PeopleService is in unexpected state"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method
