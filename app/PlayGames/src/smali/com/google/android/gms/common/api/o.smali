.class final Lcom/google/android/gms/common/api/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ak;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/n;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 5

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget-object v1, v0, Lcom/google/android/gms/common/api/n;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    monitor-exit v1

    .line 103
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/common/api/n;->d:Z

    .line 87
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget v2, v0, Lcom/google/android/gms/common/api/n;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/google/android/gms/common/api/n;->b:I

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget v0, v0, Lcom/google/android/gms/common/api/n;->b:I

    if-nez v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget-boolean v0, v0, Lcom/google/android/gms/common/api/n;->d:Z

    if-eqz v0, :cond_4

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    invoke-static {v0}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/n;)V

    .line 103
    :cond_2
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 84
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/common/api/n;->c:Z

    goto :goto_1

    .line 95
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget-boolean v0, v0, Lcom/google/android/gms/common/api/n;->c:Z

    if-eqz v0, :cond_5

    .line 96
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xd

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 100
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    new-instance v3, Lcom/google/android/gms/common/api/q;

    iget-object v4, p0, Lcom/google/android/gms/common/api/o;->a:Lcom/google/android/gms/common/api/n;

    iget-object v4, v4, Lcom/google/android/gms/common/api/n;->e:[Lcom/google/android/gms/common/api/aj;

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/common/api/q;-><init>(Lcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/common/api/aj;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/am;)V

    goto :goto_2

    .line 98
    :cond_5
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method
