.class public final Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/ads/internal/client/b;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/ads/internal/client/b;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/client/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->CREATOR:Lcom/google/android/gms/ads/internal/client/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 114
    const/4 v1, 0x2

    const-string v2, "interstitial_mb"

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 122
    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    iput p1, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->a:I

    .line 207
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    .line 208
    iput p3, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    .line 209
    iput p4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    .line 210
    iput-boolean p5, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    .line 211
    iput p6, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    .line 212
    iput p7, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    .line 213
    iput-object p8, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 214
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 227
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/ads/internal/client/b;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Landroid/os/Parcel;I)V

    .line 228
    return-void
.end method
