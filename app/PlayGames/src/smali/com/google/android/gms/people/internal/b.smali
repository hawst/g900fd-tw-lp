.class public final Lcom/google/android/gms/people/internal/b;
.super Lcom/google/android/gms/common/data/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/model/b;


# instance fields
.field private final c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/g;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 33
    iput-object p3, p0, Lcom/google/android/gms/people/internal/b;->c:Landroid/os/Bundle;

    .line 34
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "circle_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/internal/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    const-string v0, "type"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/internal/b;->c(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x2

    .line 77
    :pswitch_1
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/people/internal/b;->c:Landroid/os/Bundle;

    const-string v2, "localized_group_names"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    .line 80
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/internal/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
