.class public Lcom/google/android/gms/location/GeofencingRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/d;


# instance fields
.field private final a:I

.field private final b:Ljava/util/List;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/gms/location/d;

    invoke-direct {v0}, Lcom/google/android/gms/location/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/GeofencingRequest;->CREATOR:Lcom/google/android/gms/location/d;

    return-void
.end method

.method constructor <init>(ILjava/util/List;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/google/android/gms/location/GeofencingRequest;->a:I

    .line 78
    iput-object p2, p0, Lcom/google/android/gms/location/GeofencingRequest;->b:Ljava/util/List;

    .line 79
    iput p3, p0, Lcom/google/android/gms/location/GeofencingRequest;->c:I

    .line 80
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/location/GeofencingRequest;->a:I

    return v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/location/GeofencingRequest;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/google/android/gms/location/GeofencingRequest;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/gms/location/GeofencingRequest;->CREATOR:Lcom/google/android/gms/location/d;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/location/GeofencingRequest;->CREATOR:Lcom/google/android/gms/location/d;

    invoke-static {p0, p1}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/os/Parcel;)V

    .line 132
    return-void
.end method
