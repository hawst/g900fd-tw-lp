.class final Lcom/google/android/gms/people/internal/t;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/internal/m;

.field private final b:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/google/android/gms/people/internal/t;->a:Lcom/google/android/gms/people/internal/m;

    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 585
    iput-object p2, p0, Lcom/google/android/gms/people/internal/t;->b:Lcom/google/android/gms/common/api/m;

    .line 586
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    .prologue
    .line 590
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/x;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/m;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 596
    iget-object v1, p0, Lcom/google/android/gms/people/internal/t;->a:Lcom/google/android/gms/people/internal/m;

    invoke-static {p3}, Lcom/google/android/gms/people/internal/m;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/people/model/i;

    move-result-object v1

    .line 598
    iget-object v2, p0, Lcom/google/android/gms/people/internal/t;->a:Lcom/google/android/gms/people/internal/m;

    new-instance v3, Lcom/google/android/gms/people/internal/w;

    iget-object v4, p0, Lcom/google/android/gms/people/internal/t;->a:Lcom/google/android/gms/people/internal/m;

    iget-object v5, p0, Lcom/google/android/gms/people/internal/t;->b:Lcom/google/android/gms/common/api/m;

    invoke-direct {v3, v4, v5, v0, v1}, Lcom/google/android/gms/people/internal/w;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/i;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/internal/m;->a(Lcom/google/android/gms/common/internal/g;)V

    .line 599
    return-void
.end method
