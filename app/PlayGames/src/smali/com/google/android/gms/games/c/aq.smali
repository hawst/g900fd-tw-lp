.class public final Lcom/google/android/gms/games/c/aq;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/games/c/aq;


# instance fields
.field public a:I

.field public b:[Lcom/google/android/gms/games/c/aq;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1905
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1906
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/aq;->a:I

    invoke-static {}, Lcom/google/android/gms/games/c/aq;->b()[Lcom/google/android/gms/games/c/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/aq;->G:I

    .line 1907
    return-void
.end method

.method public static b()[Lcom/google/android/gms/games/c/aq;
    .locals 2

    .prologue
    .line 1885
    sget-object v0, Lcom/google/android/gms/games/c/aq;->d:[Lcom/google/android/gms/games/c/aq;

    if-nez v0, :cond_1

    .line 1886
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1888
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/c/aq;->d:[Lcom/google/android/gms/games/c/aq;

    if-nez v0, :cond_0

    .line 1889
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/c/aq;

    sput-object v0, Lcom/google/android/gms/games/c/aq;->d:[Lcom/google/android/gms/games/c/aq;

    .line 1891
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1893
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/c/aq;->d:[Lcom/google/android/gms/games/c/aq;

    return-object v0

    .line 1891
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()I
    .locals 5

    .prologue
    .line 1976
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 1977
    iget v1, p0, Lcom/google/android/gms/games/c/aq;->a:I

    if-eqz v1, :cond_0

    .line 1978
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/aq;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1981
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1982
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1983
    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    aget-object v2, v2, v0

    .line 1984
    if-eqz v2, :cond_1

    .line 1985
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1982
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1990
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1991
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1994
    :cond_4
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1779
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/android/gms/games/c/aq;->a:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/c/aq;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/games/c/aq;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/aq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/games/c/aq;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/aq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0xf -> :sswitch_2
        0x10 -> :sswitch_2
        0x11 -> :sswitch_2
        0x12 -> :sswitch_2
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1a -> :sswitch_2
        0x1b -> :sswitch_2
        0x1c -> :sswitch_2
        0x1d -> :sswitch_2
        0x1e -> :sswitch_2
        0x1f -> :sswitch_2
        0x20 -> :sswitch_2
        0x21 -> :sswitch_2
        0x22 -> :sswitch_2
        0x23 -> :sswitch_2
        0x24 -> :sswitch_2
        0x25 -> :sswitch_2
        0x26 -> :sswitch_2
        0x27 -> :sswitch_2
        0x28 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xca -> :sswitch_2
        0xcb -> :sswitch_2
        0xcc -> :sswitch_2
        0xcd -> :sswitch_2
        0xce -> :sswitch_2
        0xcf -> :sswitch_2
        0xd0 -> :sswitch_2
        0xd1 -> :sswitch_2
        0xd2 -> :sswitch_2
        0xd3 -> :sswitch_2
        0x190 -> :sswitch_2
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_2
        0x1f6 -> :sswitch_2
        0x1f7 -> :sswitch_2
        0x1f8 -> :sswitch_2
        0x1f9 -> :sswitch_2
        0x1fa -> :sswitch_2
        0x258 -> :sswitch_2
        0x259 -> :sswitch_2
        0x25a -> :sswitch_2
        0x25b -> :sswitch_2
        0x25c -> :sswitch_2
        0x25d -> :sswitch_2
        0x25e -> :sswitch_2
        0x25f -> :sswitch_2
        0x260 -> :sswitch_2
        0x261 -> :sswitch_2
        0x262 -> :sswitch_2
        0x263 -> :sswitch_2
        0x264 -> :sswitch_2
        0x265 -> :sswitch_2
        0x266 -> :sswitch_2
        0x267 -> :sswitch_2
        0x268 -> :sswitch_2
        0x269 -> :sswitch_2
        0x2bc -> :sswitch_2
        0x2bd -> :sswitch_2
        0x2be -> :sswitch_2
        0x2bf -> :sswitch_2
        0x2c0 -> :sswitch_2
        0x2c1 -> :sswitch_2
        0x2c2 -> :sswitch_2
        0x2c3 -> :sswitch_2
        0x321 -> :sswitch_2
        0x322 -> :sswitch_2
        0x323 -> :sswitch_2
        0x324 -> :sswitch_2
        0x325 -> :sswitch_2
        0x326 -> :sswitch_2
        0x327 -> :sswitch_2
        0x384 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1957
    iget v0, p0, Lcom/google/android/gms/games/c/aq;->a:I

    if-eqz v0, :cond_0

    .line 1958
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/aq;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1960
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1961
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1962
    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    aget-object v1, v1, v0

    .line 1963
    if-eqz v1, :cond_1

    .line 1964
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1961
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1968
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1969
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1971
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 1972
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1919
    if-ne p1, p0, :cond_1

    .line 1940
    :cond_0
    :goto_0
    return v0

    .line 1922
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/aq;

    if-nez v2, :cond_2

    move v0, v1

    .line 1923
    goto :goto_0

    .line 1925
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/aq;

    .line 1926
    iget v2, p0, Lcom/google/android/gms/games/c/aq;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/aq;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1927
    goto :goto_0

    .line 1929
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    iget-object v3, p1, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1931
    goto :goto_0

    .line 1933
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1934
    iget-object v2, p1, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1935
    goto :goto_0

    .line 1937
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1938
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1945
    iget v0, p0, Lcom/google/android/gms/games/c/aq;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1947
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/c/aq;->b:[Lcom/google/android/gms/games/c/aq;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1949
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1951
    return v0

    .line 1949
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/aq;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
