.class public Lcom/google/android/gms/common/data/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[Ljava/lang/String;

.field final b:Ljava/util/ArrayList;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashMap;

.field private e:Z

.field private f:Ljava/lang/String;


# direct methods
.method private constructor <init>([Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/data/i;->a:[Ljava/lang/String;

    .line 765
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/i;->b:Ljava/util/ArrayList;

    .line 766
    iput-object p2, p0, Lcom/google/android/gms/common/data/i;->c:Ljava/lang/String;

    .line 767
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/i;->d:Ljava/util/HashMap;

    .line 768
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/data/i;->e:Z

    .line 769
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/data/i;->f:Ljava/lang/String;

    .line 770
    return-void
.end method

.method synthetic constructor <init>([Ljava/lang/String;Ljava/lang/String;B)V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/data/i;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
