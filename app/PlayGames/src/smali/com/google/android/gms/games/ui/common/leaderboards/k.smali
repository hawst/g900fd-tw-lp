.class public final Lcom/google/android/gms/games/ui/common/leaderboards/k;
.super Lcom/google/android/gms/games/ui/e;
.source "SourceFile"


# static fields
.field private static final i:I


# instance fields
.field protected g:J

.field protected h:Ljava/lang/String;

.field private final j:Lcom/google/android/gms/games/ui/common/leaderboards/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/google/android/gms/i;->m:I

    sput v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/l;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/e;-><init>(Landroid/content/Context;)V

    .line 52
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->g:J

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->j:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/k;)Lcom/google/android/gms/games/ui/common/leaderboards/l;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->j:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    return-object v0
.end method

.method private v()Z
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 97
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Z)V

    .line 98
    iput-wide p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->g:J

    .line 99
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(II)V

    .line 102
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/support/v7/widget/cr;I)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/gms/games/ui/g;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/games/ui/g;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 2

    .prologue
    .line 58
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/games/a/f;

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 62
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/e;->e:Z

    if-eqz v1, :cond_1

    .line 63
    add-int/lit8 v0, v0, -0x1

    .line 66
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/common/data/b;)V

    .line 69
    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->c(I)V

    .line 72
    :cond_2
    return-void

    .line 58
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/g;I)V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:Ljava/lang/String;

    const-string v1, "Must set a player ID before binding views"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 123
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must set the number of scores before binding views"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 124
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/games/ui/g;I)V

    .line 125
    return-void

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 84
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:Ljava/lang/String;

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(II)V

    .line 88
    :cond_0
    return-void
.end method

.method protected final b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/g;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/m;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/m;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 110
    sget v0, Lcom/google/android/gms/g;->Z:I

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method
