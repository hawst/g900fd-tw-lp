.class public abstract Lcom/google/android/gms/games/ui/bf;
.super Lcom/google/android/gms/games/ui/w;
.source "SourceFile"


# instance fields
.field protected final c:Landroid/content/Context;

.field protected final d:Landroid/view/LayoutInflater;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/w;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/bf;->e:Z

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bf;->c:Landroid/content/Context;

    .line 48
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bf;->d:Landroid/view/LayoutInflater;

    .line 49
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/bf;->e:Z

    .line 50
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bf;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bf;->b()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cr;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/bf;->a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/cr;I)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/gms/games/ui/bg;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/bf;->a(Lcom/google/android/gms/games/ui/bg;I)V

    return-void
.end method

.method public a(Lcom/google/android/gms/games/ui/bg;I)V
    .locals 0

    .prologue
    .line 111
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 112
    return-void
.end method

.method public abstract b()I
.end method

.method public final b(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/bf;->a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bf;->g()Z

    move-result v0

    .line 65
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/bf;->e:Z

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bf;->g()Z

    move-result v1

    .line 68
    if-eq v0, v1, :cond_0

    .line 70
    if-eqz v1, :cond_1

    .line 71
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/bf;->d(I)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/bf;->e(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bf;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/bf;->c(I)V

    .line 96
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lcom/google/android/gms/games/ui/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/bf;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method
