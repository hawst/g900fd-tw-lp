.class public final Lcom/google/android/gms/cloudsave/Entity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/cloudsave/a;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/gms/cloudsave/Entity$Key;

.field private final c:Landroid/os/Bundle;

.field private d:J

.field private e:J

.field private f:Lcom/google/android/gms/cloudsave/Entity;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/gms/cloudsave/a;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/Entity;->CREATOR:Lcom/google/android/gms/cloudsave/a;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/cloudsave/Entity$Key;Landroid/os/Bundle;JJLcom/google/android/gms/cloudsave/Entity;Z)V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    iput p1, p0, Lcom/google/android/gms/cloudsave/Entity;->a:I

    .line 232
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/Entity;->b:Lcom/google/android/gms/cloudsave/Entity$Key;

    .line 233
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    .line 234
    iput-wide p4, p0, Lcom/google/android/gms/cloudsave/Entity;->d:J

    .line 235
    iput-wide p6, p0, Lcom/google/android/gms/cloudsave/Entity;->e:J

    .line 236
    iput-object p8, p0, Lcom/google/android/gms/cloudsave/Entity;->f:Lcom/google/android/gms/cloudsave/Entity;

    .line 237
    iput-boolean p9, p0, Lcom/google/android/gms/cloudsave/Entity;->g:Z

    .line 238
    return-void
.end method

.method private a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 563
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 564
    check-cast v0, Ljava/lang/String;

    .line 572
    :goto_0
    return-object v0

    .line 565
    :cond_0
    instance-of v1, v0, Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 566
    check-cast v0, Landroid/os/Bundle;

    .line 567
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568
    if-eqz v0, :cond_1

    .line 569
    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, p2

    .line 572
    goto :goto_0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 639
    instance-of v1, v0, [Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 640
    check-cast v0, [Ljava/lang/String;

    .line 648
    :goto_0
    return-object v0

    .line 641
    :cond_0
    instance-of v1, v0, Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 642
    check-cast v0, Landroid/os/Bundle;

    .line 643
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 644
    if-eqz v0, :cond_1

    .line 645
    check-cast v0, [Ljava/lang/String;

    goto :goto_0

    .line 648
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)D
    .locals 4

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private d(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 581
    if-nez v0, :cond_0

    .line 582
    const/4 v0, 0x0

    .line 584
    :cond_0
    return-object v0
.end method

.method private e(Ljava/lang/String;)[J
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 592
    if-nez v0, :cond_0

    .line 593
    const/4 v0, 0x0

    .line 595
    :cond_0
    return-object v0
.end method

.method private f(Ljava/lang/String;)[D
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    move-result-object v0

    .line 603
    if-nez v0, :cond_0

    .line 604
    const/4 v0, 0x0

    .line 606
    :cond_0
    return-object v0
.end method

.method private g(Ljava/lang/String;)[Z
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 614
    if-nez v0, :cond_0

    .line 615
    const/4 v0, 0x0

    .line 617
    :cond_0
    return-object v0
.end method

.method private h(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 665
    if-nez v1, :cond_0

    .line 666
    const/4 v0, 0x1

    .line 692
    :goto_0
    return v0

    .line 669
    :cond_0
    instance-of v0, v1, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 670
    const/4 v0, 0x2

    goto :goto_0

    .line 671
    :cond_1
    instance-of v0, v1, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 672
    const/4 v0, 0x3

    goto :goto_0

    .line 673
    :cond_2
    instance-of v0, v1, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 674
    const/4 v0, 0x4

    goto :goto_0

    .line 675
    :cond_3
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 676
    const/4 v0, 0x5

    goto :goto_0

    .line 677
    :cond_4
    instance-of v0, v1, [B

    if-eqz v0, :cond_5

    .line 678
    const/4 v0, 0x6

    goto :goto_0

    .line 679
    :cond_5
    instance-of v0, v1, [J

    if-eqz v0, :cond_6

    .line 680
    const/4 v0, 0x7

    goto :goto_0

    .line 681
    :cond_6
    instance-of v0, v1, [D

    if-eqz v0, :cond_7

    .line 682
    const/16 v0, 0x8

    goto :goto_0

    .line 683
    :cond_7
    instance-of v0, v1, [Z

    if-eqz v0, :cond_8

    .line 684
    const/16 v0, 0x9

    goto :goto_0

    .line 685
    :cond_8
    instance-of v0, v1, [Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 686
    const/16 v0, 0xa

    goto :goto_0

    .line 687
    :cond_9
    instance-of v0, v1, Landroid/os/Bundle;

    if-eqz v0, :cond_a

    move-object v0, v1

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 688
    const/16 v0, 0xb

    goto :goto_0

    .line 689
    :cond_a
    instance-of v0, v1, Landroid/os/Bundle;

    if-eqz v0, :cond_b

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 690
    const/16 v0, 0xc

    goto :goto_0

    .line 692
    :cond_b
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Lcom/google/android/gms/cloudsave/Entity;
    .locals 2

    .prologue
    .line 982
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 984
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity;->CREATOR:Lcom/google/android/gms/cloudsave/a;

    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/cloudsave/a;->a(Lcom/google/android/gms/cloudsave/Entity;Landroid/os/Parcel;I)V

    .line 985
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 986
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity;->CREATOR:Lcom/google/android/gms/cloudsave/a;

    invoke-static {v1}, Lcom/google/android/gms/cloudsave/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/cloudsave/Entity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 988
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/google/android/gms/cloudsave/Entity;->a:I

    return v0
.end method

.method final b()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/google/android/gms/cloudsave/Entity;->d:J

    return-wide v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/cloudsave/Entity;->h()Lcom/google/android/gms/cloudsave/Entity;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/google/android/gms/cloudsave/Entity;->e:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 970
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity;->CREATOR:Lcom/google/android/gms/cloudsave/a;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/cloudsave/Entity;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->f:Lcom/google/android/gms/cloudsave/Entity;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 758
    if-nez p1, :cond_0

    move v0, v1

    .line 867
    :goto_0
    return v0

    .line 761
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/cloudsave/Entity;

    if-nez v0, :cond_1

    move v0, v1

    .line 762
    goto :goto_0

    .line 765
    :cond_1
    check-cast p1, Lcom/google/android/gms/cloudsave/Entity;

    .line 766
    if-ne p0, p1, :cond_2

    move v0, v2

    .line 767
    goto :goto_0

    .line 770
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->b:Lcom/google/android/gms/cloudsave/Entity$Key;

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity;->b:Lcom/google/android/gms/cloudsave/Entity$Key;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cloudsave/Entity$Key;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v3

    if-eq v0, v3, :cond_4

    :cond_3
    move v0, v1

    .line 772
    goto :goto_0

    .line 775
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    :pswitch_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 776
    iget-object v4, p1, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->h(Ljava/lang/String;)I

    move-result v4

    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->h(Ljava/lang/String;)I

    move-result v5

    if-eq v4, v5, :cond_7

    :cond_6
    move v0, v1

    .line 778
    goto :goto_0

    .line 780
    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->h(Ljava/lang/String;)I

    move-result v4

    .line 781
    packed-switch v4, :pswitch_data_0

    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 863
    goto :goto_0

    .line 783
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;)J

    move-result-wide v4

    .line 784
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 785
    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    move v0, v1

    .line 786
    goto :goto_0

    .line 790
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->b(Ljava/lang/String;)D

    move-result-wide v4

    .line 791
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->b(Ljava/lang/String;)D

    move-result-wide v6

    .line 792
    cmpl-double v0, v4, v6

    if-eqz v0, :cond_5

    move v0, v1

    .line 793
    goto :goto_0

    .line 797
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->c(Ljava/lang/String;)Z

    move-result v4

    .line 798
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->c(Ljava/lang/String;)Z

    move-result v0

    .line 799
    if-eq v4, v0, :cond_5

    move v0, v1

    .line 800
    goto/16 :goto_0

    .line 804
    :pswitch_5
    const-string v4, ""

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 805
    const-string v5, ""

    invoke-direct {p1, v0, v5}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 806
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 807
    goto/16 :goto_0

    .line 811
    :pswitch_6
    invoke-direct {p0, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 812
    invoke-direct {p1, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 813
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 814
    goto/16 :goto_0

    .line 818
    :pswitch_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->d(Ljava/lang/String;)[B

    move-result-object v4

    .line 819
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 820
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 821
    goto/16 :goto_0

    .line 825
    :pswitch_8
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->e(Ljava/lang/String;)[J

    move-result-object v4

    .line 826
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->e(Ljava/lang/String;)[J

    move-result-object v0

    .line 827
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 828
    goto/16 :goto_0

    .line 832
    :pswitch_9
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->f(Ljava/lang/String;)[D

    move-result-object v4

    .line 833
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->f(Ljava/lang/String;)[D

    move-result-object v0

    .line 834
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 835
    goto/16 :goto_0

    .line 839
    :pswitch_a
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->g(Ljava/lang/String;)[Z

    move-result-object v4

    .line 840
    invoke-direct {p1, v0}, Lcom/google/android/gms/cloudsave/Entity;->g(Ljava/lang/String;)[Z

    move-result-object v0

    .line 841
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 842
    goto/16 :goto_0

    .line 846
    :pswitch_b
    invoke-direct {p0, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 847
    invoke-direct {p1, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 848
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 849
    goto/16 :goto_0

    .line 853
    :pswitch_c
    invoke-direct {p0, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 854
    invoke-direct {p1, v0, v8}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 855
    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 856
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 867
    goto/16 :goto_0

    .line 781
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_6
        :pswitch_c
    .end packed-switch
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/gms/cloudsave/Entity;->g:Z

    return v0
.end method

.method public final g()Lcom/google/android/gms/cloudsave/Entity$Key;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/cloudsave/Entity;->b:Lcom/google/android/gms/cloudsave/Entity$Key;

    return-object v0
.end method

.method public final hashCode()I
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 873
    iget v0, p0, Lcom/google/android/gms/cloudsave/Entity;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 877
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity;->b:Lcom/google/android/gms/cloudsave/Entity$Key;

    invoke-virtual {v1}, Lcom/google/android/gms/cloudsave/Entity$Key;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 878
    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity;->c:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 879
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    add-int/2addr v1, v4

    .line 880
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->h(Ljava/lang/String;)I

    move-result v4

    .line 881
    packed-switch v4, :pswitch_data_0

    :cond_0
    move v0, v1

    :cond_1
    move v1, v0

    .line 955
    goto :goto_0

    .line 883
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 884
    mul-int/lit8 v0, v1, 0x1f

    int-to-long v0, v0

    add-long/2addr v0, v6

    long-to-int v0, v0

    move v1, v0

    .line 885
    goto :goto_0

    .line 887
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->b(Ljava/lang/String;)D

    move-result-wide v6

    .line 888
    mul-int/lit8 v0, v1, 0x1f

    int-to-double v0, v0

    add-double/2addr v0, v6

    double-to-int v0, v0

    move v1, v0

    .line 889
    goto :goto_0

    .line 891
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->c(Ljava/lang/String;)Z

    move-result v0

    .line 892
    mul-int/lit8 v1, v1, 0x1f

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v1

    move v1, v0

    .line 893
    goto :goto_0

    :cond_2
    move v0, v3

    .line 892
    goto :goto_1

    .line 895
    :pswitch_3
    const-string v4, ""

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 896
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 897
    goto :goto_0

    .line 899
    :pswitch_4
    const-string v4, ""

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 900
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 901
    goto :goto_0

    .line 903
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->d(Ljava/lang/String;)[B

    move-result-object v6

    .line 904
    if-eqz v6, :cond_0

    .line 905
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_2
    if-ge v1, v7, :cond_3

    aget-byte v4, v6, v1

    .line 908
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v4, v0

    .line 907
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_2

    :cond_3
    move v1, v0

    .line 910
    goto :goto_0

    .line 912
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->e(Ljava/lang/String;)[J

    move-result-object v6

    .line 913
    if-eqz v6, :cond_0

    .line 914
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_3
    if-ge v1, v7, :cond_4

    aget-wide v8, v6, v1

    .line 917
    mul-int/lit8 v0, v0, 0x1f

    int-to-long v10, v0

    add-long/2addr v8, v10

    long-to-int v4, v8

    .line 916
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_3

    :cond_4
    move v1, v0

    .line 919
    goto/16 :goto_0

    .line 921
    :pswitch_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->f(Ljava/lang/String;)[D

    move-result-object v6

    .line 922
    if-eqz v6, :cond_0

    .line 923
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_4
    if-ge v1, v7, :cond_5

    aget-wide v8, v6, v1

    .line 926
    mul-int/lit8 v0, v0, 0x1f

    int-to-double v10, v0

    add-double/2addr v8, v10

    double-to-int v4, v8

    .line 925
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_4

    :cond_5
    move v1, v0

    .line 928
    goto/16 :goto_0

    .line 930
    :pswitch_8
    invoke-direct {p0, v0}, Lcom/google/android/gms/cloudsave/Entity;->g(Ljava/lang/String;)[Z

    move-result-object v6

    .line 931
    if-eqz v6, :cond_0

    .line 932
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_5
    if-ge v1, v7, :cond_7

    aget-boolean v4, v6, v1

    .line 935
    mul-int/lit8 v8, v0, 0x1f

    if-eqz v4, :cond_6

    move v0, v2

    :goto_6
    add-int v4, v8, v0

    .line 934
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_5

    :cond_6
    move v0, v3

    .line 935
    goto :goto_6

    :cond_7
    move v1, v0

    .line 937
    goto/16 :goto_0

    .line 939
    :pswitch_9
    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 940
    if-eqz v6, :cond_0

    .line 941
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_7
    if-ge v1, v7, :cond_8

    aget-object v4, v6, v1

    .line 944
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    add-int/2addr v4, v0

    .line 943
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_7

    :cond_8
    move v1, v0

    .line 946
    goto/16 :goto_0

    .line 948
    :pswitch_a
    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/cloudsave/Entity;->a(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 949
    if-eqz v6, :cond_0

    .line 950
    array-length v7, v6

    move v0, v1

    move v1, v3

    :goto_8
    if-ge v1, v7, :cond_1

    aget-object v4, v6, v1

    .line 953
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    add-int/2addr v4, v0

    .line 952
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_8

    .line 964
    :cond_9
    return v1

    .line 881
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_a
    .end packed-switch
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 976
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity;->CREATOR:Lcom/google/android/gms/cloudsave/a;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/a;->a(Lcom/google/android/gms/cloudsave/Entity;Landroid/os/Parcel;I)V

    .line 977
    return-void
.end method
