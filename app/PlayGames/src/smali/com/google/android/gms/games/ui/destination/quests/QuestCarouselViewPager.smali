.class public final Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "SourceFile"


# instance fields
.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;->d:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;->d:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 4

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;->getMeasuredWidth()I

    move-result v2

    .line 40
    const v3, 0x7f09000b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 41
    if-eqz v3, :cond_1

    .line 42
    const v3, 0x7f0b015d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 65
    :goto_0
    neg-int v2, v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 66
    iget v1, p0, Landroid/support/v4/view/ViewPager;->c:I

    if-eq v1, v0, :cond_0

    .line 77
    iget v1, p0, Landroid/support/v4/view/ViewPager;->c:I

    iput v0, p0, Landroid/support/v4/view/ViewPager;->c:I

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    invoke-super {p0, v2, v2, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 79
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/QuestCarouselViewPager;->b(I)V

    .line 80
    return-void

    .line 44
    :cond_1
    const v3, 0x7f0b01dd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method
