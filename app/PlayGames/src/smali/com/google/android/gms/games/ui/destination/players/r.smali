.class public final Lcom/google/android/gms/games/ui/destination/players/r;
.super Lcom/google/android/gms/games/ui/destination/games/ab;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;-><init>()V

    return-void
.end method


# virtual methods
.method protected final au()I
    .locals 1

    .prologue
    .line 293
    const/16 v0, 0xa

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 284
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;

    move-result-object v0

    .line 285
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 289
    return-void
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/r;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 299
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 308
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;

    move-result-object v0

    .line 309
    sget-object v2, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/r;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/gms/games/i;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
