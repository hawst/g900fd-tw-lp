.class public final Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Rect;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    sget v1, Lcom/google/android/gms/e;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    .line 60
    sget v1, Lcom/google/android/gms/e;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->f:I

    .line 62
    sget v1, Lcom/google/android/gms/e;->q:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->g:I

    .line 64
    sget v1, Lcom/google/android/gms/e;->p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->setWillNotDraw(Z)V

    .line 70
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Player;I)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v1, -0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->g:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 96
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_3

    .line 98
    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    .line 100
    :goto_0
    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Lcom/google/android/gms/games/ui/widget/a;

    invoke-direct {v3}, Lcom/google/android/gms/games/ui/widget/a;-><init>()V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Lcom/google/android/gms/games/ui/widget/a;

    invoke-direct {v3}, Lcom/google/android/gms/games/ui/widget/a;-><init>()V

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-static {v5}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 102
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->l:Landroid/graphics/drawable/Drawable;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    .line 106
    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->m:F

    .line 108
    invoke-static {v5}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 113
    :goto_2
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 225
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->j:Landroid/graphics/drawable/Drawable;

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->j:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->k:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 236
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->k:Landroid/graphics/drawable/Drawable;

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->k:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 243
    :cond_3
    return-void
.end method

.method protected final drawableStateChanged()V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->invalidate()V

    .line 219
    return-void
.end method

.method protected final onFinishInflate()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 76
    sget v0, Lcom/google/android/gms/g;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 78
    sget v0, Lcom/google/android/gms/g;->P:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->b:Landroid/view/View;

    .line 79
    sget v0, Lcom/google/android/gms/g;->o:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    .line 80
    sget v0, Lcom/google/android/gms/g;->p:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    .line 81
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 174
    sub-int v0, p5, p3

    .line 175
    sub-int v1, p4, p2

    .line 180
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    const/4 v3, 0x0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 182
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 183
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->i:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 188
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    div-int/lit8 v5, v1, 0x2

    div-int/lit8 v6, v0, 0x2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    sub-int v6, v0, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->layout(IIII)V

    .line 196
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->m:F

    div-float/2addr v2, v3

    .line 199
    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->b:Landroid/view/View;

    div-int/lit8 v4, v1, 0x2

    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v4, v5

    float-to-int v2, v2

    sub-int v2, v0, v2

    div-int/lit8 v5, v1, 0x2

    div-int/lit8 v6, v0, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v3, v4, v2, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 205
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    sub-int v3, v1, v3

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->g:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->f:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->g:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->g:I

    add-int/2addr v5, v1

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->f:I

    sub-int v6, v0, v6

    iget v7, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->g:I

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 211
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    sub-int v3, v1, v3

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->f:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->f:I

    sub-int/2addr v0, v5

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 213
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 146
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 148
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->measure(II)V

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    sub-int v2, v0, v2

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->h:I

    sub-int v3, v0, v3

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->measure(II)V

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->b:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->c:Landroid/view/View;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->e:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 170
    return-void
.end method
