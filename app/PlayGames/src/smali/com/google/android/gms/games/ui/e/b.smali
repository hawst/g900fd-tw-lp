.class public final Lcom/google/android/gms/games/ui/e/b;
.super Landroid/support/v4/app/an;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/e/l;


# instance fields
.field public a:Ljava/util/ArrayList;

.field b:Landroid/content/Context;

.field public c:Z

.field public d:Lcom/google/android/gms/games/ui/e/d;

.field private e:Landroid/util/SparseArray;

.field private f:Landroid/support/v4/app/ag;

.field private g:Lcom/google/android/gms/games/ui/e/f;

.field private h:Lcom/google/android/gms/games/ui/e/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/ag;[Lcom/google/android/gms/games/ui/e/ai;Lcom/google/android/gms/games/ui/e/f;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 132
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/e/b;-><init>(Landroid/content/Context;Landroid/support/v4/app/ag;[Lcom/google/android/gms/games/ui/e/ai;Lcom/google/android/gms/games/ui/e/f;Lcom/google/android/gms/games/ui/e/k;)V

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/ag;[Lcom/google/android/gms/games/ui/e/ai;Lcom/google/android/gms/games/ui/e/f;Lcom/google/android/gms/games/ui/e/k;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 153
    invoke-direct {p0, p2}, Landroid/support/v4/app/an;-><init>(Landroid/support/v4/app/ag;)V

    .line 107
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/e/b;->c:Z

    .line 154
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/b;->b:Landroid/content/Context;

    .line 155
    iput-object p2, p0, Lcom/google/android/gms/games/ui/e/b;->f:Landroid/support/v4/app/ag;

    .line 156
    iput-object p4, p0, Lcom/google/android/gms/games/ui/e/b;->g:Lcom/google/android/gms/games/ui/e/f;

    .line 157
    iput-object p5, p0, Lcom/google/android/gms/games/ui/e/b;->h:Lcom/google/android/gms/games/ui/e/k;

    .line 159
    invoke-static {p3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    .line 165
    array-length v1, p3

    iget-object v2, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 168
    new-instance v0, Landroid/util/SparseArray;

    array-length v1, p3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    .line 169
    return-void

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 307
    if-ltz p1, :cond_2

    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    move v0, v2

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/e/ai;

    .line 311
    iget-object v1, v0, Lcom/google/android/gms/games/ui/e/ai;->a:Ljava/lang/Class;

    .line 313
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 317
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 319
    iget-object v0, v0, Lcom/google/android/gms/games/ui/e/ai;->c:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->g(Landroid/os/Bundle;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->d:Lcom/google/android/gms/games/ui/e/d;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->d:Lcom/google/android/gms/games/ui/e/d;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/games/ui/e/d;->a(Landroid/support/v4/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 345
    if-eqz v0, :cond_1

    .line 346
    const-string v4, "GFragmentPagerAdapter"

    const-string v5, "getItem(): fragment at this position was already instantiated!"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - position: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - previous instance: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - new instance:      "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_1
    if-nez v0, :cond_4

    :goto_2
    const-string v0, "getItem(): fragment at this position was already instantiated!"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 355
    return-object v1

    :cond_2
    move v0, v3

    .line 307
    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 308
    goto/16 :goto_1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t instantiate Fragment at pos "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 326
    :catch_1
    move-exception v0

    .line 327
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t instantiate Fragment at pos "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v2, v3

    .line 351
    goto :goto_2
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    .line 289
    check-cast p1, Landroid/os/Bundle;

    .line 290
    const-string v0, "FRAGMENT_TAGS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 291
    if-nez v1, :cond_1

    .line 302
    :cond_0
    return-void

    .line 296
    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 297
    aget-object v2, v1, v0

    .line 298
    if-eqz v2, :cond_2

    .line 299
    iget-object v3, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/e/b;->f:Landroid/support/v4/app/ag;

    invoke-virtual {v4, v2}, Landroid/support/v4/app/ag;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 296
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/e/ai;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/b;->c:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/e/b;->d()V

    goto :goto_0
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 272
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 274
    new-array v4, v3, [Ljava/lang/String;

    .line 275
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 277
    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 275
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 280
    :cond_0
    const/4 v0, 0x0

    aput-object v0, v4, v1

    goto :goto_1

    .line 283
    :cond_1
    const-string v0, "FRAGMENT_TAGS"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 284
    return-object v2
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 368
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/e/ai;

    iget v0, v0, Lcom/google/android/gms/games/ui/e/ai;->b:I

    .line 371
    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 368
    goto :goto_0

    :cond_1
    move v1, v2

    .line 369
    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 251
    :goto_0
    return-object v0

    .line 247
    :cond_0
    if-ltz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 247
    goto :goto_1

    :cond_2
    move v1, v2

    .line 248
    goto :goto_2
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 235
    invoke-super {p0}, Landroid/support/v4/app/an;->d()V

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/e/b;->c:Z

    .line 237
    return-void
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->g:Lcom/google/android/gms/games/ui/e/f;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->g:Lcom/google/android/gms/games/ui/e/f;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/f;->a()F

    move-result v0

    .line 176
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final f()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 258
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 259
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 261
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 265
    :cond_1
    return-object v2
.end method

.method public final g()Lcom/google/android/gms/games/ui/e/k;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/b;->h:Lcom/google/android/gms/games/ui/e/k;

    return-object v0
.end method
