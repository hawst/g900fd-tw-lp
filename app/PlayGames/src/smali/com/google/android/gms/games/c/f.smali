.class public final Lcom/google/android/gms/games/c/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3087
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3088
    iput v0, p0, Lcom/google/android/gms/games/c/f;->a:I

    iput v0, p0, Lcom/google/android/gms/games/c/f;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/f;->G:I

    .line 3089
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 3138
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 3139
    iget v1, p0, Lcom/google/android/gms/games/c/f;->a:I

    if-eqz v1, :cond_0

    .line 3140
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/f;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3143
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/c/f;->b:I

    if-eqz v1, :cond_1

    .line 3144
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/c/f;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3147
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3064
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/f;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/f;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3127
    iget v0, p0, Lcom/google/android/gms/games/c/f;->a:I

    if-eqz v0, :cond_0

    .line 3128
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/f;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3130
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/c/f;->b:I

    if-eqz v0, :cond_1

    .line 3131
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/c/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3133
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 3134
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3100
    if-ne p1, p0, :cond_1

    .line 3113
    :cond_0
    :goto_0
    return v0

    .line 3103
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 3104
    goto :goto_0

    .line 3106
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/f;

    .line 3107
    iget v2, p0, Lcom/google/android/gms/games/c/f;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/f;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3108
    goto :goto_0

    .line 3110
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/c/f;->b:I

    iget v3, p1, Lcom/google/android/gms/games/c/f;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3111
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3118
    iget v0, p0, Lcom/google/android/gms/games/c/f;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3120
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/f;->b:I

    add-int/2addr v0, v1

    .line 3121
    return v0
.end method
