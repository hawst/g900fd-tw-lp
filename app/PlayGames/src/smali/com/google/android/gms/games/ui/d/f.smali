.class final Lcom/google/android/gms/games/ui/d/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/graphics/Rect;

.field private final c:I

.field private final d:I

.field private final e:Landroid/graphics/Rect;

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/PathMeasure;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/transition/PathMotion;)V
    .locals 4

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/f;->a:Landroid/view/View;

    .line 74
    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/d/f;->c:I

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/d/f;->d:I

    .line 77
    iput-object p3, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/d/f;->f:I

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/d/f;->g:I

    .line 80
    iget v0, p0, Lcom/google/android/gms/games/ui/d/f;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/gms/games/ui/d/f;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/gms/games/ui/d/f;->f:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/games/ui/d/f;->g:I

    int-to-float v3, v3

    invoke-virtual {p4, v0, v1, v2, v3}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v0

    .line 81
    new-instance v1, Landroid/graphics/PathMeasure;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->h:Landroid/graphics/PathMeasure;

    .line 82
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 86
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->h:Landroid/graphics/PathMeasure;

    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v1

    mul-float/2addr v1, v0

    .line 90
    const/4 v2, 0x2

    new-array v2, v2, [F

    .line 91
    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->h:Landroid/graphics/PathMeasure;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v2, v4}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 94
    aget v1, v2, v6

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->c:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v4, v5, v0

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->f:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 96
    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->a:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLeft(I)V

    .line 97
    aget v1, v2, v6

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->c:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v4, v5, v0

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->f:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 99
    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->a:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setRight(I)V

    .line 100
    aget v1, v2, v7

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->d:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v4, v5, v0

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/gms/games/ui/d/f;->g:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 102
    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/f;->a:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setTop(I)V

    .line 103
    aget v1, v2, v7

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/f;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/gms/games/ui/d/f;->d:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sub-float v3, v5, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/f;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/gms/games/ui/d/f;->g:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/f;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBottom(I)V

    .line 106
    return-void
.end method
