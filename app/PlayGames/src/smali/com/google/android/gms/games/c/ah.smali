.class public final Lcom/google/android/gms/games/c/ah;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/android/gms/games/c/aj;

.field public c:[Lcom/google/android/gms/games/c/ai;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6957
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6958
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/games/c/aj;->b()[Lcom/google/android/gms/games/c/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    invoke-static {}, Lcom/google/android/gms/games/c/ai;->b()[Lcom/google/android/gms/games/c/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ah;->G:I

    .line 6959
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7035
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 7036
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 7037
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7040
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 7041
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 7042
    iget-object v3, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    aget-object v3, v3, v0

    .line 7043
    if-eqz v3, :cond_1

    .line 7044
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7041
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 7049
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 7050
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 7051
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    aget-object v2, v2, v1

    .line 7052
    if-eqz v2, :cond_4

    .line 7053
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7050
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7058
    :cond_5
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6440
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/c/aj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/games/c/aj;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/aj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/games/c/aj;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/aj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/c/ai;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/games/c/ai;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/games/c/ai;

    invoke-direct {v3}, Lcom/google/android/gms/games/c/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7011
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7012
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7014
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 7015
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 7016
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    aget-object v2, v2, v0

    .line 7017
    if-eqz v2, :cond_1

    .line 7018
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7015
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7022
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 7023
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 7024
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    aget-object v0, v0, v1

    .line 7025
    if-eqz v0, :cond_3

    .line 7026
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7023
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7030
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 7031
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6971
    if-ne p1, p0, :cond_1

    .line 6993
    :cond_0
    :goto_0
    return v0

    .line 6974
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ah;

    if-nez v2, :cond_2

    move v0, v1

    .line 6975
    goto :goto_0

    .line 6977
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ah;

    .line 6978
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 6979
    iget-object v2, p1, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6980
    goto :goto_0

    .line 6982
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6983
    goto :goto_0

    .line 6985
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 6987
    goto :goto_0

    .line 6989
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 6991
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6998
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7001
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/c/ah;->b:[Lcom/google/android/gms/games/c/aj;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7003
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/c/ah;->c:[Lcom/google/android/gms/games/c/ai;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7005
    return v0

    .line 6998
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ah;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
