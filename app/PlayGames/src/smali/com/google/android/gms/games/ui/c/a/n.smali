.class public final Lcom/google/android/gms/games/ui/c/a/n;
.super Lcom/google/android/gms/games/ui/c/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a/a;-><init>()V

    .line 23
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/games/ui/c/a/n;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/games/ui/c/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/a/n;-><init>()V

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-static {p2}, Lcom/google/android/gms/common/data/o;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 47
    const-string v3, "requests"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/a/n;->g(Landroid/os/Bundle;)V

    .line 50
    return-object v0
.end method


# virtual methods
.method protected final P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 56
    const-string v1, "requests"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 57
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final Q()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 63
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 64
    const-string v2, "requests"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 67
    instance-of v2, v0, Lcom/google/android/gms/games/ui/c/a/o;

    if-eqz v2, :cond_0

    .line 68
    check-cast v0, Lcom/google/android/gms/games/ui/c/a/o;

    .line 78
    :goto_0
    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/c/a/o;->a(Ljava/util/ArrayList;)V

    .line 79
    return-void

    .line 69
    :cond_0
    instance-of v2, v0, Lcom/google/android/gms/games/ui/c/a/p;

    if-eqz v2, :cond_1

    .line 70
    check-cast v0, Lcom/google/android/gms/games/ui/c/a/p;

    .line 71
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/c/a/p;->ai()Lcom/google/android/gms/games/ui/c/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/c/a/o;

    goto :goto_0

    .line 73
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RequestChangeAccountDialogFragment must be used with a parent Activity which implements RequestAccountSwitcher or RequestAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
