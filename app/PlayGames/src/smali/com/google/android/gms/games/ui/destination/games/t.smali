.class final Lcom/google/android/gms/games/ui/destination/games/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/e/u;


# instance fields
.field a:Z

.field b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

.field c:Lcom/google/android/gms/games/o;

.field d:I

.field final synthetic e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

.field private f:Z

.field private g:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 1532
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1636
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1569
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 1570
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1573
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->finish()V

    .line 1618
    :goto_0
    return-void

    .line 1579
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1580
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->f:Z

    if-nez v0, :cond_2

    .line 1585
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/s;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/s;->a(I)V

    .line 1586
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/games/ui/destination/games/u;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/ui/destination/games/u;-><init>(Lcom/google/android/gms/games/ui/destination/games/t;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 1593
    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/destination/games/t;->f:Z

    .line 1599
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/t;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    if-nez v0, :cond_3

    .line 1600
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->g:Z

    if-nez v0, :cond_3

    .line 1603
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/s;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/s;->a(I)V

    .line 1604
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v2, "circled"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xf

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/games/v;-><init>(Lcom/google/android/gms/games/ui/destination/games/t;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 1613
    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/destination/games/t;->g:Z

    .line 1617
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/t;->b()V

    goto :goto_0
.end method

.method final b()V
    .locals 15

    .prologue
    .line 1732
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->C(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    .line 1735
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->D(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    .line 1737
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->E(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/e/b;->c:Z

    .line 1739
    const/4 v3, 0x0

    .line 1740
    const/4 v2, 0x0

    .line 1741
    const/4 v5, 0x0

    .line 1742
    const/4 v4, 0x0

    .line 1743
    const/4 v1, 0x0

    .line 1745
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v0, :cond_27

    .line 1747
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->d()Ljava/util/ArrayList;

    move-result-object v7

    .line 1748
    const/4 v0, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_2

    .line 1749
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/GameBadge;

    .line 1750
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->c()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_0

    .line 1751
    const/4 v0, 0x1

    move v14, v1

    move v1, v2

    move v2, v0

    move v0, v14

    .line 1748
    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1752
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->c()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 1753
    const/4 v0, 0x1

    move v2, v3

    move v14, v0

    move v0, v1

    move v1, v14

    goto :goto_1

    .line 1754
    :cond_1
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->c()I

    move-result v0

    const/16 v9, 0x8

    if-ne v0, v9, :cond_28

    .line 1755
    const/4 v0, 0x1

    move v1, v2

    move v2, v3

    goto :goto_1

    .line 1760
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 1761
    const/4 v0, 0x1

    move v4, v3

    move v3, v2

    move v2, v0

    .line 1767
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    if-lez v0, :cond_3

    .line 1768
    const/4 v0, 0x1

    move v5, v0

    .line 1772
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-nez v0, :cond_f

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_1d

    .line 1773
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/s;

    move-result-object v0

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/s;->a(I)V

    .line 1775
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->F(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/b;->a(Lcom/google/android/gms/games/ui/e/ai;)V

    .line 1776
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->g()I

    move-result v0

    if-gtz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->u()I

    move-result v0

    if-lez v0, :cond_4

    if-nez v4, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/16 v6, 0xa

    if-ne v0, v6, :cond_11

    :cond_5
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_6

    .line 1777
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->G(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->an()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/b;->a(Lcom/google/android/gms/games/ui/e/ai;)V

    .line 1779
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->v()I

    move-result v0

    if-lez v0, :cond_7

    if-nez v3, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/16 v6, 0xb

    if-ne v0, v6, :cond_12

    :cond_8
    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_9

    .line 1780
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->H(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ao()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/b;->a(Lcom/google/android/gms/games/ui/e/ai;)V

    .line 1782
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->o()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/16 v6, 0xc

    if-ne v0, v6, :cond_13

    :cond_a
    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_b

    .line 1783
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->I(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/b;->a(Lcom/google/android/gms/games/ui/e/ai;)V

    .line 1785
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    if-nez v1, :cond_d

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/16 v6, 0xd

    if-ne v0, v6, :cond_14

    :cond_d
    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_e

    .line 1786
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->J(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/e/b;->a(Lcom/google/android/gms/games/ui/e/ai;)V

    .line 1792
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->K(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/4 v6, -0x1

    if-ne v0, v6, :cond_1c

    .line 1793
    const/4 v11, -0x1

    .line 1794
    const/4 v10, -0x1

    .line 1795
    const/4 v9, -0x1

    .line 1796
    const/4 v8, -0x1

    .line 1797
    const/4 v6, -0x1

    .line 1798
    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/b;->c()I

    move-result v12

    :goto_8
    if-ge v7, v12, :cond_1b

    .line 1799
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->M(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v13

    if-ltz v7, :cond_15

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    iget-object v0, v13, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_16

    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    iget-object v0, v13, Lcom/google/android/gms/games/ui/e/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/e/ai;

    .line 1800
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v13

    if-ne v0, v13, :cond_17

    move v0, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v7

    .line 1798
    :goto_b
    add-int/lit8 v7, v7, 0x1

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v0

    goto :goto_8

    .line 1772
    :cond_f
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/t;->e()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    if-nez v0, :cond_10

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_10
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 1776
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 1779
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 1782
    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 1785
    :cond_14
    const/4 v0, 0x0

    goto :goto_7

    .line 1799
    :cond_15
    const/4 v0, 0x0

    goto :goto_9

    :cond_16
    const/4 v0, 0x0

    goto :goto_a

    .line 1802
    :cond_17
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->an()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v13

    if-ne v0, v13, :cond_18

    move v0, v6

    move v10, v11

    move v6, v8

    move v8, v9

    move v9, v7

    .line 1803
    goto :goto_b

    .line 1804
    :cond_18
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ao()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v13

    if-ne v0, v13, :cond_19

    move v0, v6

    move v9, v10

    move v6, v8

    move v10, v11

    move v8, v7

    .line 1805
    goto :goto_b

    .line 1806
    :cond_19
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v13

    if-ne v0, v13, :cond_1a

    move v0, v6

    move v8, v9

    move v6, v7

    move v9, v10

    move v10, v11

    .line 1807
    goto :goto_b

    .line 1808
    :cond_1a
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq()Lcom/google/android/gms/games/ui/e/ai;

    move-result-object v13

    if-ne v0, v13, :cond_26

    move v0, v7

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    .line 1809
    goto :goto_b

    .line 1813
    :cond_1b
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1833
    if-ltz v11, :cond_1f

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 1834
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v11}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    .line 1835
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    if-eqz v0, :cond_1c

    .line 1836
    if-ltz v11, :cond_20

    if-eqz v2, :cond_20

    .line 1837
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v11}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    .line 1853
    :cond_1c
    :goto_d
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->K(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_24

    const/4 v0, 0x1

    :goto_e
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 1855
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/b;->d()V

    .line 1856
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->K(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)V

    .line 1861
    :cond_1d
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1862
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_f
    if-ge v1, v3, :cond_25

    .line 1863
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1864
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->n()Z

    move-result v4

    if-nez v4, :cond_1e

    instance-of v4, v0, Lcom/google/android/gms/games/ui/destination/games/e;

    if-eqz v4, :cond_1e

    .line 1865
    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->as()V

    .line 1862
    :cond_1e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 1815
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v10}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1819
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v9}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1823
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v8}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1827
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v6}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1833
    :cond_1f
    const/4 v0, 0x0

    goto :goto_c

    .line 1838
    :cond_20
    if-ltz v8, :cond_21

    if-eqz v5, :cond_21

    .line 1839
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v8}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1840
    :cond_21
    if-ltz v6, :cond_22

    if-eqz v1, :cond_22

    .line 1841
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v6}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1842
    :cond_22
    if-ltz v10, :cond_23

    if-eqz v4, :cond_23

    .line 1843
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v10}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto :goto_d

    .line 1844
    :cond_23
    if-ltz v9, :cond_1c

    if-eqz v3, :cond_1c

    .line 1845
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->e:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0, v9}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I

    goto/16 :goto_d

    .line 1853
    :cond_24
    const/4 v0, 0x0

    goto/16 :goto_e

    .line 1868
    :cond_25
    return-void

    :cond_26
    move v0, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    goto/16 :goto_b

    :cond_27
    move v14, v4

    move v4, v3

    move v3, v2

    move v2, v14

    goto/16 :goto_2

    :cond_28
    move v0, v1

    move v1, v2

    move v2, v3

    goto/16 :goto_1

    .line 1813
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final c()Z
    .locals 1

    .prologue
    .line 1899
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1932
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/t;->a()V

    .line 1933
    return-void
.end method
