.class public final Lcom/google/android/gms/games/ui/common/matches/d;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/matches/e;

.field private final h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/matches/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;I)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/e;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->g:Lcom/google/android/gms/games/ui/common/matches/e;

    .line 96
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->h:Z

    .line 98
    sget v0, Lcom/google/android/gms/h;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/matches/d;->d(II)V

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/d;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/d;->g:Lcom/google/android/gms/games/ui/common/matches/e;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 1

    .prologue
    .line 114
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/q;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 115
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    .line 116
    return-void

    .line 114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/d;->i:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/d;->j:Ljava/lang/String;

    .line 110
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/f;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/f;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 125
    sget v0, Lcom/google/android/gms/g;->W:I

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x3

    return v0
.end method
