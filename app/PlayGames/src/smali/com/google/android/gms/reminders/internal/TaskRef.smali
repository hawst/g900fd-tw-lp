.class public Lcom/google/android/gms/reminders/internal/TaskRef;
.super Lcom/google/android/gms/reminders/internal/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/reminders/model/Task;


# direct methods
.method private n(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/DateTime;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "hour"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "minute"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "second"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 97
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "year"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "month"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "day"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "period"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "absolute_time_ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    :goto_1
    return-object v1

    .line 95
    :cond_0
    new-instance v2, Lcom/google/android/gms/reminders/model/j;

    invoke-direct {v2}, Lcom/google/android/gms/reminders/model/j;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "hour"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/reminders/model/j;->a:Ljava/lang/Integer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "minute"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/reminders/model/j;->b:Ljava/lang/Integer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "second"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/reminders/model/j;->c:Ljava/lang/Integer;

    new-instance v0, Lcom/google/android/gms/reminders/model/TimeEntity;

    iget-object v3, v2, Lcom/google/android/gms/reminders/model/j;->a:Ljava/lang/Integer;

    iget-object v4, v2, Lcom/google/android/gms/reminders/model/j;->b:Ljava/lang/Integer;

    iget-object v2, v2, Lcom/google/android/gms/reminders/model/j;->c:Ljava/lang/Integer;

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/gms/reminders/model/TimeEntity;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 106
    :cond_1
    new-instance v6, Lcom/google/android/gms/reminders/model/a;

    invoke-direct {v6}, Lcom/google/android/gms/reminders/model/a;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "year"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/reminders/model/a;->a:Ljava/lang/Integer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "month"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/reminders/model/a;->b:Ljava/lang/Integer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "day"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/reminders/model/a;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/reminders/model/Time;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/reminders/model/Time;

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/a;->d:Lcom/google/android/gms/reminders/model/Time;

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/a;->e:Ljava/lang/Integer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "absolute_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/a;->f:Ljava/lang/Long;

    .line 113
    new-instance v0, Lcom/google/android/gms/reminders/model/DateTimeEntity;

    iget-object v1, v6, Lcom/google/android/gms/reminders/model/a;->a:Ljava/lang/Integer;

    iget-object v2, v6, Lcom/google/android/gms/reminders/model/a;->b:Ljava/lang/Integer;

    iget-object v3, v6, Lcom/google/android/gms/reminders/model/a;->c:Ljava/lang/Integer;

    iget-object v4, v6, Lcom/google/android/gms/reminders/model/a;->d:Lcom/google/android/gms/reminders/model/Time;

    iget-object v5, v6, Lcom/google/android/gms/reminders/model/a;->e:Ljava/lang/Integer;

    iget-object v6, v6, Lcom/google/android/gms/reminders/model/a;->f:Ljava/lang/Long;

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/reminders/model/DateTimeEntity;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/reminders/model/Time;Ljava/lang/Integer;Ljava/lang/Long;Z)V

    move-object v1, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-direct {v0, p0}, Lcom/google/android/gms/reminders/model/TaskEntity;-><init>(Lcom/google/android/gms/reminders/model/Task;)V

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/reminders/model/TaskId;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/reminders/model/f;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/f;-><init>()V

    const-string v1, "server_assigned_id"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/reminders/model/f;->a:Ljava/lang/Long;

    const-string v1, "client_assigned_id"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/reminders/model/f;->b:Ljava/lang/String;

    const-string v1, "client_assigned_thread_id"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/reminders/model/f;->c:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/reminders/model/TaskIdEntity;

    iget-object v2, v0, Lcom/google/android/gms/reminders/model/f;->a:Ljava/lang/Long;

    iget-object v3, v0, Lcom/google/android/gms/reminders/model/f;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/reminders/model/f;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/reminders/model/TaskIdEntity;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final d()Lcom/google/android/gms/reminders/model/TaskList;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/reminders/model/h;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/h;-><init>()V

    const-string v1, "task_list"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/reminders/model/h;->a:Ljava/lang/Integer;

    new-instance v1, Lcom/google/android/gms/reminders/model/TaskListEntity;

    iget-object v0, v0, Lcom/google/android/gms/reminders/model/h;->a:Ljava/lang/Integer;

    invoke-direct {v1, v0}, Lcom/google/android/gms/reminders/model/TaskListEntity;-><init>(Ljava/lang/Integer;)V

    return-object v1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "title"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 173
    instance-of v0, p1, Lcom/google/android/gms/reminders/model/Task;

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 179
    :goto_0
    return v0

    .line 176
    :cond_0
    if-ne p0, p1, :cond_1

    .line 177
    const/4 v0, 0x1

    goto :goto_0

    .line 179
    :cond_1
    check-cast p1, Lcom/google/android/gms/reminders/model/Task;

    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/model/TaskEntity;->a(Lcom/google/android/gms/reminders/model/Task;Lcom/google/android/gms/reminders/model/Task;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 51
    const-string v0, "created_time_millis"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 56
    const-string v0, "archived_time_ms"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 61
    const-string v0, "archived"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->d(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 184
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->c()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->d()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "created_time_millis"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "archived_time_ms"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->h()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->i()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "snoozed_time_millis"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "due_date_"

    invoke-direct {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->n(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "event_date_"

    invoke-direct {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->n(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/TaskRef;->o()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "location_snoozed_until_ms"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 66
    const-string v0, "deleted"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->d(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    const-string v0, "pinned"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->d(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 76
    const-string v0, "snoozed"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->d(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 81
    const-string v0, "snoozed_time_millis"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    .prologue
    .line 86
    const-string v0, "due_date_"

    invoke-direct {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->n(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    .prologue
    .line 91
    const-string v0, "event_date_"

    invoke-direct {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->n(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/google/android/gms/reminders/model/Location;
    .locals 7

    .prologue
    .line 132
    const-string v0, "lat"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "lng"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "radius_meters"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "location_type"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "display_address"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    new-instance v6, Lcom/google/android/gms/reminders/model/c;

    invoke-direct {v6}, Lcom/google/android/gms/reminders/model/c;-><init>()V

    const-string v0, "lat"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->m(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->a:Ljava/lang/Double;

    const-string v0, "lng"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->m(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->b:Ljava/lang/Double;

    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->c:Ljava/lang/String;

    const-string v0, "radius_meters"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->d:Ljava/lang/Integer;

    const-string v0, "location_type"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->l(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->e:Ljava/lang/Integer;

    const-string v0, "display_address"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/reminders/model/c;->f:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/reminders/model/LocationEntity;

    iget-object v1, v6, Lcom/google/android/gms/reminders/model/c;->a:Ljava/lang/Double;

    iget-object v2, v6, Lcom/google/android/gms/reminders/model/c;->b:Ljava/lang/Double;

    iget-object v3, v6, Lcom/google/android/gms/reminders/model/c;->c:Ljava/lang/String;

    iget-object v4, v6, Lcom/google/android/gms/reminders/model/c;->d:Ljava/lang/Integer;

    iget-object v5, v6, Lcom/google/android/gms/reminders/model/c;->e:Ljava/lang/Integer;

    iget-object v6, v6, Lcom/google/android/gms/reminders/model/c;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/reminders/model/LocationEntity;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final p()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 153
    const-string v0, "location_snoozed_until_ms"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/TaskRef;->k(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-direct {v0, p0}, Lcom/google/android/gms/reminders/model/TaskEntity;-><init>(Lcom/google/android/gms/reminders/model/Task;)V

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/reminders/model/TaskEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 169
    return-void
.end method
