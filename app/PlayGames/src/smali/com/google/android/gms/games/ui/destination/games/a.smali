.class public final Lcom/google/android/gms/games/ui/destination/games/a;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/destination/games/c;

.field private final h:Lcom/google/android/gms/games/ui/destination/games/b;

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;)V
    .locals 6

    .prologue
    .line 78
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;II)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;)V
    .locals 6

    .prologue
    .line 84
    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;II)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;B)V
    .locals 6

    .prologue
    .line 89
    const v4, 0x7f0d0017

    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;II)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;II)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-static {p2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 98
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/games/a;->g:Lcom/google/android/gms/games/ui/destination/games/c;

    .line 99
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/games/a;->h:Lcom/google/android/gms/games/ui/destination/games/b;

    .line 100
    iput p5, p0, Lcom/google/android/gms/games/ui/destination/games/a;->i:I

    .line 102
    invoke-virtual {p0, p4}, Lcom/google/android/gms/games/ui/destination/games/a;->h(I)V

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/a;->h:Lcom/google/android/gms/games/ui/destination/games/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/a;)Lcom/google/android/gms/games/ui/destination/games/c;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/a;->g:Lcom/google/android/gms/games/ui/destination/games/c;

    return-object v0
.end method


# virtual methods
.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/d;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/destination/games/d;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f0c002c

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/a;->i:I

    return v0
.end method
