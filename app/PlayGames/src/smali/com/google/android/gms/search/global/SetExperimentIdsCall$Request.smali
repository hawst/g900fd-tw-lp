.class public Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/i;


# instance fields
.field public a:[B

.field public b:Z

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/search/global/i;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->c:I

    .line 58
    return-void
.end method

.method constructor <init>(I[BZ)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->c:I

    .line 75
    iput-object p2, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->a:[B

    .line 76
    iput-boolean p3, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->b:Z

    .line 77
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/global/i;->a(Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;Landroid/os/Parcel;)V

    .line 90
    return-void
.end method
