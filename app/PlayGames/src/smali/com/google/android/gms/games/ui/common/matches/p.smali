.class final Lcom/google/android/gms/games/ui/common/matches/p;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 135
    return-void
.end method

.method private C()V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 252
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 289
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 290
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 12

    .prologue
    .line 131
    check-cast p3, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->y()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v2

    sget v1, Lcom/google/android/gms/f;->g:I

    :goto_0
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->e(Z)V

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Landroid/net/Uri;I)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->d(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->k:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/l;->cg:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->b(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->x()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v8

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    if-eqz v7, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v2, v0

    :goto_3
    if-ge v2, v10, :cond_6

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v7, :cond_5

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    if-nez v11, :cond_1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v2

    sget v1, Lcom/google/android/gms/f;->f:I

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v8, :cond_7

    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    sget v0, Lcom/google/android/gms/f;->g:I

    invoke-virtual {p0, v9, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Ljava/util/ArrayList;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->j(Z)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->j(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->k:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/l;->cf:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->h()I

    move-result v1

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->g()I

    move-result v2

    const/4 v4, 0x3

    if-ne v1, v4, :cond_8

    const/4 v1, 0x2

    if-ne v2, v1, :cond_8

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->q()Z

    move-result v1

    if-eqz v1, :cond_8

    sget v1, Lcom/google/android/gms/l;->bH:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->n(I)V

    sget v1, Lcom/google/android/gms/l;->bI:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->o(I)V

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->u()Landroid/database/CharArrayBuffer;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v0

    if-eqz v0, :cond_9

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->a(F)V

    :cond_9
    sget v0, Lcom/google/android/gms/j;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->m(I)V

    return-void

    :cond_a
    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 260
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 261
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->k:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/l;->aQ:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 296
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 297
    sget v4, Lcom/google/android/gms/g;->aS:I

    if-ne v2, v4, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->p()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/l;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/data/l;->a(Ljava/lang/String;)V

    .line 300
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    move v0, v3

    .line 310
    :goto_0
    return v0

    .line 302
    :cond_0
    sget v4, Lcom/google/android/gms/g;->aV:I

    if-ne v2, v4, :cond_1

    .line 303
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v4, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 305
    goto :goto_0

    .line 306
    :cond_1
    sget v4, Lcom/google/android/gms/g;->aU:I

    if-ne v2, v4, :cond_2

    .line 307
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/Game;)V

    move v0, v3

    .line 308
    goto :goto_0

    .line 310
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r(I)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->C()V

    .line 279
    return-void
.end method

.method public final x()V
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 272
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method public final z()V
    .locals 0

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->C()V

    .line 284
    return-void
.end method
