.class public final Lcom/google/android/gms/cloudsave/Entity$Key;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/cloudsave/b;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/cloudsave/b;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/Entity$Key;->CREATOR:Lcom/google/android/gms/cloudsave/b;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput p1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->e:I

    .line 89
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->a:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->b:Ljava/lang/String;

    .line 91
    iput-object p4, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->c:Ljava/lang/String;

    .line 92
    iput-object p5, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->d:Ljava/lang/String;

    .line 93
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->e:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity$Key;->CREATOR:Lcom/google/android/gms/cloudsave/b;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 154
    if-ne p0, p1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/cloudsave/Entity$Key;

    if-nez v2, :cond_2

    move v0, v1

    .line 159
    goto :goto_0

    .line 162
    :cond_2
    check-cast p1, Lcom/google/android/gms/cloudsave/Entity$Key;

    .line 164
    iget v2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->e:I

    iget v3, p1, Lcom/google/android/gms/cloudsave/Entity$Key;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity$Key;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity$Key;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity$Key;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cloudsave/Entity$Key;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->e:I

    add-int/lit16 v0, v0, 0x20f

    .line 178
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "User:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Workspace:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cloudsave/Entity$Key;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/gms/cloudsave/Entity$Key;->CREATOR:Lcom/google/android/gms/cloudsave/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/cloudsave/b;->a(Lcom/google/android/gms/cloudsave/Entity$Key;Landroid/os/Parcel;)V

    .line 142
    return-void
.end method
