.class public Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cc;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/widget/finsky/c;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

.field private final d:I

.field private e:I

.field private f:Landroid/support/v4/view/ViewPager;

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->h:Z

    .line 51
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->setHorizontalScrollBarEnabled(Z)V

    .line 53
    new-instance v0, Lcom/google/android/gms/games/ui/widget/finsky/c;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/c;-><init>(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a:Lcom/google/android/gms/games/ui/widget/finsky/c;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->b:Landroid/view/LayoutInflater;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/e;->Y:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->d:I

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v1

    .line 96
    const/4 v0, 0x0

    .line 97
    instance-of v2, v1, Lcom/google/android/gms/games/ui/e/l;

    if-eqz v2, :cond_7

    move-object v0, v1

    .line 98
    check-cast v0, Lcom/google/android/gms/games/ui/e/l;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/l;->g()Lcom/google/android/gms/games/ui/e/k;

    move-result-object v0

    move-object v2, v0

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->removeAllViews()V

    move v3, v4

    .line 102
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/view/ao;->c()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->b:Landroid/view/LayoutInflater;

    sget v5, Lcom/google/android/gms/i;->O:I

    iget-object v6, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, v5, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 107
    sget v0, Lcom/google/android/gms/g;->cs:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 108
    invoke-virtual {v1, v3}, Landroid/support/v4/view/ao;->b(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    sget v0, Lcom/google/android/gms/g;->aG:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    if-eqz v2, :cond_6

    .line 114
    invoke-interface {v2, v3}, Lcom/google/android/gms/games/ui/e/k;->b(I)I

    move-result v5

    .line 117
    :goto_2
    if-lez v5, :cond_3

    .line 118
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    new-instance v7, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v7}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 122
    const/16 v8, 0xa

    if-lt v5, v8, :cond_0

    .line 124
    invoke-virtual {v7, v4}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/gms/e;->f:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 131
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/gms/d;->u:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 135
    const/16 v8, 0x10

    invoke-static {v8}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 136
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 142
    :goto_4
    const/16 v7, 0x63

    if-le v5, v7, :cond_2

    .line 143
    sget v5, Lcom/google/android/gms/l;->P:I

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 153
    :goto_5
    new-instance v0, Lcom/google/android/gms/games/ui/widget/finsky/a;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/games/ui/widget/finsky/a;-><init>(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;I)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->addView(Landroid/view/View;)V

    .line 102
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 128
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    goto :goto_3

    .line 138
    :cond_1
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 145
    :cond_2
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 148
    :cond_3
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 162
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->h:Z

    if-eqz v0, :cond_5

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(II)V

    .line 180
    :goto_6
    return-void

    .line 167
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/widget/finsky/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/widget/finsky/b;-><init>(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_6

    :cond_6
    move v5, v4

    goto/16 :goto_2

    :cond_7
    move-object v2, v0

    goto/16 :goto_0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 214
    add-int/2addr v0, p2

    .line 215
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 216
    :cond_2
    iget v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->d:I

    sub-int/2addr v0, v1

    .line 219
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->g:I

    if-eq v0, v1, :cond_0

    .line 223
    iput v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->g:I

    .line 224
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->scrollTo(II)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;I)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(II)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->e:I

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->b(I)V

    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(II)V

    .line 206
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 193
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->a(IF)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    .line 192
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(II)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a:Lcom/google/android/gms/games/ui/widget/finsky/c;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ao;->b(Landroid/database/DataSetObserver;)V

    .line 87
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a:Lcom/google/android/gms/games/ui/widget/finsky/c;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ao;->a(Landroid/database/DataSetObserver;)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a()V

    .line 91
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 197
    iput p1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->e:I

    .line 198
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->a(I)V

    .line 73
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 65
    sget v0, Lcom/google/android/gms/g;->bi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    .line 66
    return-void
.end method
