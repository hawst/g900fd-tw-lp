.class final Lcom/google/android/gms/common/internal/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/common/internal/r;

.field final c:Ljava/util/HashSet;

.field d:I

.field e:Z

.field f:Landroid/os/IBinder;

.field g:Landroid/content/ComponentName;

.field final synthetic h:Lcom/google/android/gms/common/internal/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/p;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/gms/common/internal/q;->h:Lcom/google/android/gms/common/internal/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p2, p0, Lcom/google/android/gms/common/internal/q;->a:Ljava/lang/String;

    .line 90
    new-instance v0, Lcom/google/android/gms/common/internal/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/r;-><init>(Lcom/google/android/gms/common/internal/q;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/q;->b:Lcom/google/android/gms/common/internal/r;

    .line 91
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/q;->c:Ljava/util/HashSet;

    .line 92
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/internal/q;->d:I

    .line 93
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/q;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/common/internal/q;->h:Lcom/google/android/gms/common/internal/p;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/p;->b(Lcom/google/android/gms/common/internal/p;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/q;->b:Lcom/google/android/gms/common/internal/r;

    const/16 v3, 0x81

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/q;->e:Z

    .line 102
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/q;->e:Z

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/common/internal/q;->d:I

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/q;->h:Lcom/google/android/gms/common/internal/p;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Lcom/google/android/gms/common/internal/p;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/q;->b:Lcom/google/android/gms/common/internal/r;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/internal/j;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/common/internal/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/internal/j;)Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/common/internal/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
