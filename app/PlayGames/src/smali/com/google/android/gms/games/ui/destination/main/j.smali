.class final Lcom/google/android/gms/games/ui/destination/main/j;
.super Lcom/google/android/gms/common/data/m;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/games/ui/destination/main/d;

.field private final d:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/data/b;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1313
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/main/j;->c:Lcom/google/android/gms/games/ui/destination/main/d;

    .line 1314
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/data/m;-><init>(Lcom/google/android/gms/common/data/b;)V

    .line 1311
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/j;->d:Ljava/util/ArrayList;

    .line 1317
    invoke-virtual {p2}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v4

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 1318
    invoke-virtual {p2, v3}, Lcom/google/android/gms/common/data/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 1319
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->f()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 1320
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/j;->d:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1321
    add-int/lit8 v0, v1, 0x1

    .line 1322
    if-eq v0, p3, :cond_1

    .line 1323
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1319
    goto :goto_1

    .line 1327
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected final b(I)I
    .locals 1

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
