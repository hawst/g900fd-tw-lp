.class public Lcom/google/android/gms/games/ui/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/app/ab;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ab;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ab;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/ab;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ab;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method can only be called from client or headless UI"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/a/a;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 61
    :goto_1
    return-void

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 57
    :cond_2
    const v0, 0x10008000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ab;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected b(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    iget-object v2, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 74
    :cond_0
    :goto_1
    return v0

    .line 65
    :cond_1
    invoke-static {}, Lcom/google/android/gms/games/ui/c/c;->P()Lcom/google/android/gms/games/ui/c/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    const-string v4, "com.google.android.gms.games.ui.dialog.installDialog"

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    move v2, v1

    goto :goto_0

    .line 69
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aj;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    .line 74
    goto :goto_1

    .line 69
    :cond_3
    invoke-static {}, Lcom/google/android/gms/games/ui/c/g;->P()Lcom/google/android/gms/games/ui/c/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/a/a;->a:Landroid/support/v4/app/ab;

    const-string v4, "com.google.android.gms.games.ui.dialog.upgradeDialog"

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    move v2, v1

    goto :goto_2
.end method
