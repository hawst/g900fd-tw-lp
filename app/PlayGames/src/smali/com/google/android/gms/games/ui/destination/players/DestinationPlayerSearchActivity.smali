.class public final Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/players/b;


# instance fields
.field private z:Lcom/google/android/gms/games/ui/e/ab;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    const v0, 0x7f040047

    const v1, 0x7f110011

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 42
    return-void
.end method


# virtual methods
.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->d()V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public final varargs b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->getIntent()Landroid/content/Intent;

    .line 48
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 50
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const v1, 0x7f0c014e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;

    .line 52
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 56
    new-instance v1, Lcom/google/android/gms/games/ui/e/ab;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/ui/e/ab;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/af;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->a(Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->b()V

    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->a(Landroid/content/Intent;)V

    .line 84
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/ab;->b(Landroid/os/Bundle;)V

    .line 90
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->e()Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onStart()V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/e/ab;->d:Z

    .line 71
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onStop()V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;->z:Lcom/google/android/gms/games/ui/e/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/ab;->a()V

    .line 65
    return-void
.end method
