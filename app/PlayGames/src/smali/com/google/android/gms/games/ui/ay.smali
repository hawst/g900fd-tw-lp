.class public final Lcom/google/android/gms/games/ui/ay;
.super Landroid/support/v7/widget/bx;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v7/widget/bv;

.field private b:I


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ay;->a:Landroid/support/v7/widget/bv;

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 48
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ay;->a:Landroid/support/v7/widget/bv;

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 57
    add-int v0, p1, p2

    iget v3, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-gt v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0

    :cond_1
    move v1, v2

    .line 57
    goto :goto_1
.end method

.method public final b(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    iget v0, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 67
    iget v0, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ay;->a:Landroid/support/v7/widget/bv;

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 66
    goto :goto_0

    :cond_1
    move v1, v2

    .line 68
    goto :goto_1
.end method

.method public final c(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    add-int v0, p1, p2

    iget v3, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 89
    iget v0, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ay;->a:Landroid/support/v7/widget/bv;

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/ay;->b:I

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 91
    return-void

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0

    :cond_1
    move v1, v2

    .line 90
    goto :goto_1
.end method
