.class public Lcom/google/android/gms/car/support/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static b:[Ljava/lang/Object;

.field static c:I

.field static d:[Ljava/lang/Object;

.field static e:I


# instance fields
.field f:[I

.field g:[Ljava/lang/Object;

.field h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    sget-object v0, Lcom/google/android/gms/car/support/f;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 211
    sget-object v0, Lcom/google/android/gms/car/support/f;->c:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 213
    return-void
.end method

.method static a([I[Ljava/lang/Object;I)V
    .locals 4

    .prologue
    const/16 v2, 0xa

    const/4 v3, 0x2

    .line 174
    array-length v0, p0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 175
    const-class v1, Lcom/google/android/gms/car/support/a;

    monitor-enter v1

    .line 176
    :try_start_0
    sget v0, Lcom/google/android/gms/car/support/ab;->e:I

    if-ge v0, v2, :cond_1

    .line 177
    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/gms/car/support/ab;->d:[Ljava/lang/Object;

    aput-object v2, p1, v0

    .line 178
    const/4 v0, 0x1

    aput-object p0, p1, v0

    .line 179
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, v3, :cond_0

    .line 180
    const/4 v2, 0x0

    aput-object v2, p1, v0

    .line 179
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 182
    :cond_0
    sput-object p1, Lcom/google/android/gms/car/support/ab;->d:[Ljava/lang/Object;

    .line 183
    sget v0, Lcom/google/android/gms/car/support/ab;->e:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/car/support/ab;->e:I

    .line 187
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :cond_2
    :goto_1
    return-void

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 188
    :cond_3
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 189
    const-class v1, Lcom/google/android/gms/car/support/a;

    monitor-enter v1

    .line 190
    :try_start_1
    sget v0, Lcom/google/android/gms/car/support/ab;->c:I

    if-ge v0, v2, :cond_5

    .line 191
    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/gms/car/support/ab;->b:[Ljava/lang/Object;

    aput-object v2, p1, v0

    .line 192
    const/4 v0, 0x1

    aput-object p0, p1, v0

    .line 193
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-lt v0, v3, :cond_4

    .line 194
    const/4 v2, 0x0

    aput-object v2, p1, v0

    .line 193
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 196
    :cond_4
    sput-object p1, Lcom/google/android/gms/car/support/ab;->b:[Ljava/lang/Object;

    .line 197
    sget v0, Lcom/google/android/gms/car/support/ab;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/car/support/ab;->c:I

    .line 201
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private d(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 5

    .prologue
    .line 101
    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 104
    if-nez v2, :cond_1

    .line 105
    const/4 v0, -0x1

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/car/support/f;->a([III)I

    move-result v0

    .line 111
    if-ltz v0, :cond_0

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v1, v1, v3

    if-eqz v1, :cond_0

    .line 122
    add-int/lit8 v1, v0, 0x1

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    aget v3, v3, v1

    if-nez v3, :cond_3

    .line 123
    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 122
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 127
    :cond_3
    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    aget v2, v2, v0

    if-nez v2, :cond_4

    .line 128
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 127
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 135
    :cond_4
    xor-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method final a(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 278
    iget v1, p0, Lcom/google/android/gms/car/support/ab;->h:I

    mul-int/lit8 v1, v1, 0x2

    .line 279
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 280
    if-nez p1, :cond_2

    .line 281
    :goto_0
    if-ge v0, v1, :cond_3

    .line 282
    aget-object v3, v2, v0

    if-nez v3, :cond_0

    .line 283
    shr-int/lit8 v0, v0, 0x1

    .line 293
    :goto_1
    return v0

    .line 281
    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 287
    :cond_1
    add-int/lit8 v0, v0, 0x2

    :cond_2
    if-ge v0, v1, :cond_3

    .line 288
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289
    shr-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 293
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method final a(Ljava/lang/Object;I)I
    .locals 5

    .prologue
    .line 63
    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 66
    if-nez v2, :cond_1

    .line 67
    const/4 v0, -0x1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    invoke-static {v0, v2, p2}, Lcom/google/android/gms/car/support/f;->a([III)I

    move-result v0

    .line 73
    if-ltz v0, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v1, v1, v3

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    add-int/lit8 v1, v0, 0x1

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    aget v3, v3, v1

    if-ne v3, p2, :cond_3

    .line 85
    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 84
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 89
    :cond_3
    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    aget v2, v2, v0

    if-ne v2, p2, :cond_4

    .line 90
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 97
    :cond_4
    xor-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method final a(I)V
    .locals 5

    .prologue
    .line 139
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 140
    const-class v1, Lcom/google/android/gms/car/support/a;

    monitor-enter v1

    .line 141
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/support/ab;->d:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 142
    sget-object v2, Lcom/google/android/gms/car/support/ab;->d:[Ljava/lang/Object;

    .line 143
    iput-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 144
    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/gms/car/support/ab;->d:[Ljava/lang/Object;

    .line 145
    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 146
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v4, v2, v0

    .line 147
    sget v0, Lcom/google/android/gms/car/support/ab;->e:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/gms/car/support/ab;->e:I

    .line 150
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :goto_0
    return-void

    .line 152
    :cond_0
    monitor-exit v1

    .line 169
    :cond_1
    :goto_1
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 170
    shl-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 153
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 154
    const-class v1, Lcom/google/android/gms/car/support/a;

    monitor-enter v1

    .line 155
    :try_start_1
    sget-object v0, Lcom/google/android/gms/car/support/ab;->b:[Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 156
    sget-object v2, Lcom/google/android/gms/car/support/ab;->b:[Ljava/lang/Object;

    .line 157
    iput-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 158
    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/gms/car/support/ab;->b:[Ljava/lang/Object;

    .line 159
    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 160
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v4, v2, v0

    .line 161
    sget v0, Lcom/google/android/gms/car/support/ab;->c:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/gms/car/support/ab;->c:I

    .line 164
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 166
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    monitor-exit v1

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v0, 0x8

    const/4 v5, 0x0

    .line 462
    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    aget-object v1, v1, v2

    .line 463
    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/car/support/ab;->h:I

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/support/ab;->a([I[Ljava/lang/Object;I)V

    .line 467
    sget-object v0, Lcom/google/android/gms/car/support/f;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 468
    sget-object v0, Lcom/google/android/gms/car/support/f;->c:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 469
    iput v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 509
    :cond_0
    :goto_0
    return-object v1

    .line 471
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v2, v2

    if-le v2, v0, :cond_4

    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    if-ge v2, v3, :cond_4

    .line 475
    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-le v2, v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 479
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 480
    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 481
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/ab;->a(I)V

    .line 483
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 484
    if-lez p1, :cond_3

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    invoke-static {v2, v5, v0, v5, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 489
    :cond_3
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge p1, v0, :cond_0

    .line 492
    add-int/lit8 v0, p1, 0x1

    iget-object v4, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v5, p1

    invoke-static {v2, v0, v4, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 493
    add-int/lit8 v0, p1, 0x1

    shl-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v5, p1

    shl-int/lit8 v5, v5, 0x1

    invoke-static {v3, v0, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 497
    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 498
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge p1, v0, :cond_5

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget v4, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v4, p1

    invoke-static {v0, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    add-int/lit8 v2, p1, 0x1

    shl-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v5, p1

    shl-int/lit8 v5, v5, 0x1

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 505
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    shl-int/lit8 v2, v2, 0x1

    aput-object v6, v0, v2

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x1

    aput-object v6, v0, v2

    goto/16 :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 242
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/support/ab;->a([I[Ljava/lang/Object;I)V

    .line 244
    sget-object v0, Lcom/google/android/gms/car/support/f;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 245
    sget-object v0, Lcom/google/android/gms/car/support/f;->c:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 246
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 248
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 274
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->a()I

    move-result v2

    if-ltz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/gms/car/support/ab;->a(Ljava/lang/Object;I)I

    move-result v2

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 304
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/support/ab;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 529
    if-ne p0, p1, :cond_1

    .line 558
    :cond_0
    :goto_0
    return v0

    .line 532
    :cond_1
    instance-of v2, p1, Ljava/util/Map;

    if-eqz v2, :cond_6

    .line 533
    check-cast p1, Ljava/util/Map;

    .line 534
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 535
    goto :goto_0

    :cond_2
    move v2, v1

    .line 539
    :goto_1
    :try_start_0
    iget v3, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge v2, v3, :cond_0

    .line 540
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/support/ab;->c(I)Ljava/lang/Object;

    move-result-object v3

    .line 541
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/support/ab;->d(I)Ljava/lang/Object;

    move-result-object v4

    .line 542
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 543
    if-nez v4, :cond_4

    .line 544
    if-nez v5, :cond_3

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    move v0, v1

    .line 545
    goto :goto_0

    .line 547
    :cond_4
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_5

    move v0, v1

    .line 548
    goto :goto_0

    .line 539
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 552
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 554
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 558
    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 315
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->a()I

    move-result v0

    .line 316
    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    .line 315
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/support/ab;->a(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_0

    .line 316
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 566
    iget-object v5, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 567
    iget-object v6, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 569
    const/4 v0, 0x1

    iget v7, p0, Lcom/google/android/gms/car/support/ab;->h:I

    move v2, v0

    move v3, v1

    move v4, v1

    :goto_0
    if-ge v3, v7, :cond_1

    .line 570
    aget-object v0, v6, v2

    .line 571
    aget v8, v5, v3

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    xor-int/2addr v0, v8

    add-int/2addr v4, v0

    .line 569
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 571
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 573
    :cond_1
    return v4
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 357
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x4

    const/4 v4, 0x0

    .line 372
    if-nez p1, :cond_0

    .line 374
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->a()I

    move-result v2

    move v3, v4

    .line 379
    :goto_0
    if-ltz v2, :cond_1

    .line 380
    shl-int/lit8 v0, v2, 0x1

    add-int/lit8 v1, v0, 0x1

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 382
    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 417
    :goto_1
    return-object v0

    .line 376
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    .line 377
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/car/support/ab;->a(Ljava/lang/Object;I)I

    move-result v2

    goto :goto_0

    .line 386
    :cond_1
    xor-int/lit8 v2, v2, -0x1

    .line 387
    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    iget-object v6, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v6, v6

    if-lt v5, v6, :cond_4

    .line 388
    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-lt v5, v0, :cond_6

    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    iget v1, p0, Lcom/google/android/gms/car/support/ab;->h:I

    shr-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 393
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    .line 394
    iget-object v5, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    .line 395
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/ab;->a(I)V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v6, v1

    invoke-static {v1, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    array-length v6, v5

    invoke-static {v5, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 403
    :cond_3
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    invoke-static {v1, v5, v0}, Lcom/google/android/gms/car/support/ab;->a([I[Ljava/lang/Object;I)V

    .line 406
    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge v2, v0, :cond_5

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    add-int/lit8 v4, v2, 0x1

    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v5, v2

    invoke-static {v0, v2, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    iget-object v4, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    add-int/lit8 v5, v2, 0x1

    shl-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/google/android/gms/car/support/ab;->h:I

    sub-int/2addr v6, v2

    shl-int/lit8 v6, v6, 0x1

    invoke-static {v0, v1, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    aput v3, v0, v2

    .line 414
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    aput-object p1, v0, v1

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    aput-object p2, v0, v1

    .line 416
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    .line 417
    const/4 v0, 0x0

    goto :goto_1

    .line 388
    :cond_6
    iget v5, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge v5, v1, :cond_2

    move v0, v1

    goto :goto_2
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 447
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->a()I

    move-result v0

    .line 448
    :goto_0
    if-ltz v0, :cond_1

    .line 449
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/ab;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 452
    :goto_1
    return-object v0

    .line 447
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/support/ab;->a(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_0

    .line 452
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ab;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    const-string v0, "{}"

    .line 610
    :goto_0
    return-object v0

    .line 589
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    mul-int/lit8 v0, v0, 0x1c

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 590
    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 591
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-ge v0, v2, :cond_4

    .line 592
    if-lez v0, :cond_1

    .line 593
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/ab;->c(I)Ljava/lang/Object;

    move-result-object v2

    .line 596
    if-eq v2, p0, :cond_2

    .line 597
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 601
    :goto_2
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 602
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/ab;->d(I)Ljava/lang/Object;

    move-result-object v2

    .line 603
    if-eq v2, p0, :cond_3

    .line 604
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 591
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 599
    :cond_2
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 606
    :cond_3
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 609
    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 610
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
