.class public final Lcom/google/android/gms/games/ui/destination/players/ae;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Lcom/google/android/gms/games/ui/destination/players/ah;

.field m:Z

.field private final n:Landroid/view/View;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/view/ViewGroup;

.field private final q:Landroid/view/ViewGroup;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/view/ViewGroup;

.field private final u:Landroid/view/View;

.field private final v:Landroid/view/View;

.field private final w:Landroid/view/View;

.field private final x:Ljava/lang/StringBuilder;

.field private y:Z

.field private z:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 167
    const v0, 0x7f0c012b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->n:Landroid/view/View;

    .line 169
    const v0, 0x7f0c012d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->o:Landroid/widget/TextView;

    .line 171
    const v0, 0x7f0c012c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->p:Landroid/view/ViewGroup;

    .line 173
    const v0, 0x7f0c012e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 176
    const v0, 0x7f0c0203

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->r:Landroid/widget/TextView;

    .line 178
    const v0, 0x7f0c0204

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->s:Landroid/widget/TextView;

    .line 181
    const v0, 0x7f0c0200

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->t:Landroid/view/ViewGroup;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->t:Landroid/view/ViewGroup;

    const v1, 0x7f0c0201

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->u:Landroid/view/View;

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->t:Landroid/view/ViewGroup;

    const v1, 0x7f0c0202

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->v:Landroid/view/View;

    .line 189
    const v0, 0x7f0c00f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->x:Ljava/lang/StringBuilder;

    .line 193
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->y:Z

    .line 194
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/ae;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/ae;Lcom/google/android/gms/games/ui/destination/players/ah;)Lcom/google/android/gms/games/ui/destination/players/ah;
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->A:Lcom/google/android/gms/games/ui/destination/players/ah;

    return-object p1
.end method

.method private a(Landroid/content/res/Resources;FILandroid/view/View;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 388
    invoke-virtual {p4, p3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 390
    invoke-virtual {p5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 392
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 393
    invoke-virtual {p5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 398
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    const v0, 0x7f0d001a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v4, 0x0

    aput v1, v0, v4

    const/4 v4, 0x1

    aput p2, v0, v4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v4, Lcom/google/android/gms/games/ui/destination/players/ah;

    invoke-direct {v4, p0, p4, p5, v0}, Lcom/google/android/gms/games/ui/destination/players/ah;-><init>(Lcom/google/android/gms/games/ui/destination/players/ae;Landroid/view/View;Landroid/view/View;Landroid/animation/Animator;)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 403
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->z:Landroid/animation/Animator;

    if-nez v2, :cond_0

    .line 404
    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->z:Landroid/animation/Animator;

    :cond_0
    move p2, v1

    .line 408
    :cond_1
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 410
    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 411
    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 412
    return-void
.end method

.method private b(II)Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 491
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 492
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/ag;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/players/ag;-><init>(Lcom/google/android/gms/games/ui/destination/players/ae;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 501
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/ae;)Lcom/google/android/gms/games/ui/destination/players/ah;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->A:Lcom/google/android/gms/games/ui/destination/players/ah;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 26

    .prologue
    .line 217
    invoke-super/range {p0 .. p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    .line 219
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 221
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->n:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->o:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 223
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->s:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 224
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->v:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 226
    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->f(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 227
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    const v7, 0x7f020099

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 234
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->x:Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 235
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->m:Z

    if-nez v6, :cond_d

    .line 236
    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->g(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v6

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->h(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v11

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->i(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->j(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Landroid/view/LayoutInflater;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->l:Lcom/google/android/gms/games/ui/w;

    move-object v10, v4

    check-cast v10, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    const/4 v7, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-virtual {v12}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v6

    invoke-virtual {v12}, Lcom/google/android/gms/games/PlayerLevelInfo;->e()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v9

    invoke-virtual {v12}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v14

    invoke-static {v5, v9}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v7

    if-eq v6, v8, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v20

    sub-long v14, v14, v20

    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v22

    sub-long v20, v22, v20

    long-to-float v6, v14

    move-wide/from16 v0, v20

    long-to-float v8, v0

    div-float/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->s:Landroid/widget/TextView;

    const v13, 0x7f0f00b8

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v18, 0x0

    invoke-virtual {v4, v14, v15}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v16, v18

    const/4 v14, 0x1

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v14

    move-object/from16 v0, v16

    invoke-virtual {v5, v13, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->r:Landroid/widget/TextView;

    const v8, 0x7f0f00b6

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v13, v14

    invoke-virtual {v5, v8, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->u:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->v:Landroid/view/View;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/games/ui/destination/players/ae;->a(Landroid/content/res/Resources;FILandroid/view/View;Landroid/view/View;)V

    invoke-static {v10}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v10}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v4

    invoke-virtual {v4, v12}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)V

    :cond_0
    if-nez v11, :cond_7

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v11, v4

    :goto_2
    const/4 v10, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    const-wide/16 v8, 0x1

    move-wide v12, v8

    :goto_3
    const-wide/16 v14, 0x0

    const/4 v6, 0x1

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v16, v4

    :goto_4
    if-ltz v16, :cond_f

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    iget-wide v8, v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    const-wide/16 v20, 0x0

    cmp-long v4, v8, v20

    if-lez v4, :cond_9

    const v4, 0x3cf5c28f    # 0.03f

    long-to-float v6, v12

    mul-float/2addr v4, v6

    float-to-long v14, v4

    cmp-long v4, v8, v14

    if-gtz v4, :cond_1

    cmp-long v4, v8, v12

    if-nez v4, :cond_2

    :cond_1
    const-wide/16 v8, 0x0

    :cond_2
    add-int/lit8 v4, v16, 0x1

    move-wide v14, v8

    :goto_5
    const/16 v6, 0xa

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    const/high16 v4, 0x3f800000    # 1.0f

    const v8, 0x3f666666    # 0.9f

    int-to-float v6, v6

    div-float v20, v8, v6

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    move/from16 v16, v4

    move/from16 v17, v10

    :goto_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    move/from16 v0, v17

    if-ge v0, v11, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->p:Landroid/view/ViewGroup;

    move-object/from16 v18, v4

    :goto_7
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v22

    const v6, 0x7f040046

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v23

    const v6, 0x7f0c012d

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v8, v10, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->k(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    iget-wide v0, v10, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    move-wide/from16 v24, v0

    const v6, 0x7f0c0148

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->k(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v4, 0x7f0c0149

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x7f0c014d

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const-wide/16 v8, 0x0

    cmp-long v4, v24, v8

    if-nez v4, :cond_b

    const/4 v6, 0x0

    :goto_8
    const v4, 0x7f0c014b

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const v4, 0x7f0c014c

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/games/ui/destination/players/ae;->a(Landroid/content/res/Resources;FILandroid/view/View;Landroid/view/View;)V

    invoke-virtual {v8}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float v6, v6, v16

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0f00b5

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v10, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v4, v6, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->x:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->x:Ljava/lang/StringBuilder;

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sub-float v4, v16, v20

    const v6, 0x3dcccccd    # 0.1f

    cmpg-float v6, v4, v6

    if-gez v6, :cond_3

    const v4, 0x3dcccccd    # 0.1f

    :cond_3
    add-int/lit8 v6, v17, 0x1

    move/from16 v16, v4

    move/from16 v17, v6

    goto/16 :goto_6

    .line 228
    :cond_4
    const/16 v6, 0x10

    invoke-static {v6}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 229
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 231
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 236
    :cond_6
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->s:Landroid/widget/TextView;

    const v8, 0x7f0f00ae

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v13, v16

    invoke-virtual {v5, v8, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4, v11}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v11, v4

    goto/16 :goto_2

    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    iget-wide v8, v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    move-wide v12, v8

    goto/16 :goto_3

    :cond_9
    add-int/lit8 v4, v16, -0x1

    move/from16 v16, v4

    goto/16 :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    move-object/from16 v18, v4

    goto/16 :goto_7

    :cond_b
    sub-long v8, v24, v14

    long-to-float v4, v8

    sub-long v8, v12, v14

    long-to-float v6, v8

    div-float/2addr v4, v6

    const v6, 0x3f7851ec    # 0.97f

    mul-float/2addr v4, v6

    const v6, 0x3cf5c28f    # 0.03f

    add-float/2addr v6, v4

    goto/16 :goto_8

    :cond_c
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->m:Z

    .line 239
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->w:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->x:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->z:Landroid/animation/Animator;

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->z:Landroid/animation/Animator;

    invoke-virtual {v5}, Landroid/animation/Animator;->start()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->z:Landroid/animation/Animator;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v5

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->y:Z

    if-eqz v5, :cond_e

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/ae;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d001a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(J)V

    .line 242
    :cond_e
    return-void

    :cond_f
    move v4, v6

    goto/16 :goto_5
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0xb

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-static {v3}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0, v2}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/games/ui/destination/players/ae;->b(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 428
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 423
    goto :goto_0

    .line 426
    :cond_2
    invoke-static {v3}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/players/ae;->b(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/af;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/players/af;-><init>(Lcom/google/android/gms/games/ui/destination/players/ae;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ae;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method
