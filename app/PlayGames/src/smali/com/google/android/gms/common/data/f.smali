.class public final Lcom/google/android/gms/common/data/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/d;
.implements Lcom/google/android/gms/common/data/e;


# instance fields
.field a:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(II)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 70
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/common/data/d;->a_(II)V

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public final b_(II)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 77
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/common/data/d;->b_(II)V

    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method
