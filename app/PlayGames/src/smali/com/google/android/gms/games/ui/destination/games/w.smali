.class final Lcom/google/android/gms/games/ui/destination/games/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

.field private final b:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Z)V
    .locals 0

    .prologue
    .line 1433
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1434
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/destination/games/w;->b:Z

    .line 1435
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1428
    check-cast p1, Lcom/google/android/gms/games/m;

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->c()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/w;->b:Z

    if-eq v4, v3, :cond_0

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->al()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The mute status was not changed as it should have been. ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->b:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0f01d0

    :goto_1
    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->x(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v3, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->y(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0f01d2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/w;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/w;->b:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0f01d1

    :goto_4
    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    const v0, 0x7f0f01d3

    goto :goto_4

    :cond_4
    move v0, v2

    goto :goto_3
.end method
