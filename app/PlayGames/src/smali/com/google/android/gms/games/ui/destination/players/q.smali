.class public final Lcom/google/android/gms/games/ui/destination/players/q;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/destination/games/b;
.implements Lcom/google/android/gms/games/ui/destination/games/c;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/destination/games/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    return-void
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 190
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;

    move-result-object v0

    .line 191
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/gms/games/i;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 194
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 126
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/q;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/q;->an:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/games/a;->k()V

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/q;->an:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/q;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/q;->b(Lcom/google/android/gms/common/api/t;)V

    .line 153
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 2

    .prologue
    .line 263
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 267
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    .line 268
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 269
    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Lcom/google/android/gms/games/internal/game/ExtendedGame;Lcom/google/android/gms/games/Player;)V

    .line 271
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 236
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/q;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 243
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v1

    .line 244
    if-eqz v1, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 247
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v2, v0, v3, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/q;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 199
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    const-string v0, "PDGameComparisonA"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    .line 208
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;

    move-result-object v0

    .line 209
    sget-object v2, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/q;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/gms/games/i;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 137
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/a;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->an:Lcom/google/android/gms/games/ui/destination/games/a;

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->an:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 143
    const v0, 0x7f0200d3

    const v1, 0x7f0f0153

    const v2, 0x7f0f0152

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/q;->a(III)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/q;->an:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/q;->a(Landroid/support/v7/widget/bv;)V

    .line 148
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/q;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    const-string v0, "PDGameComparisonA"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/q;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 222
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/q;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
