.class public Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/h;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:[I

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/gms/search/global/h;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->c:I

    .line 100
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[I)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput p1, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->c:I

    .line 122
    iput-object p2, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 123
    iput-object p3, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->b:[I

    .line 124
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/h;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/h;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/h;->a(Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;Landroid/os/Parcel;I)V

    .line 137
    return-void
.end method
