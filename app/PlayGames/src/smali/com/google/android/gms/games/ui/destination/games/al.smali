.class final Lcom/google/android/gms/games/ui/destination/games/al;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->g(Z)V

    .line 97
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 10

    .prologue
    const v9, 0x7f0f014f

    const v8, 0x7f0f014e

    const v7, 0x7f0f0074

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 91
    check-cast p3, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/al;->c(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v1

    const v2, 0x7f020090

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/al;->a(Landroid/net/Uri;I)V

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3, v1}, Lcom/google/android/gms/games/ui/destination/games/al;->a(IF)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/al;->t()Landroid/database/CharArrayBuffer;

    move-result-object v1

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/al;->a(Landroid/database/CharArrayBuffer;)V

    iget v1, v0, Lcom/google/android/gms/games/ui/destination/games/aj;->g:I

    if-ne v1, v3, :cond_5

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v8}, Lcom/google/android/gms/games/ui/destination/games/al;->i(I)V

    :goto_0
    const v0, 0x7f11000a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->m(I)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/al;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0148

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v0, Lcom/google/android/gms/games/ui/destination/games/aj;->g:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->a(Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, v9}, Lcom/google/android/gms/games/ui/destination/games/al;->i(I)V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/al;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->f(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->f(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget v0, v0, Lcom/google/android/gms/games/ui/destination/games/aj;->g:I

    if-ne v0, v4, :cond_0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, v7}, Lcom/google/android/gms/games/ui/destination/games/al;->i(I)V

    :cond_6
    const v0, 0x7f110008

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/al;->m(I)V

    goto :goto_1

    :pswitch_0
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->k:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :pswitch_1
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_8
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/aj;->h:Lcom/google/android/gms/games/ui/destination/games/ak;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/al;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/games/ui/destination/games/ak;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;[Landroid/util/Pair;)V

    .line 180
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/al;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 185
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 199
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    .line 187
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v1, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/destination/games/aj;->h:Lcom/google/android/gms/games/ui/destination/games/ak;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/ak;->a_(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 201
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 190
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v1, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/destination/games/aj;->h:Lcom/google/android/gms/games/ui/destination/games/ak;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/ak;->a(Lcom/google/android/gms/games/Game;)V

    goto :goto_1

    .line 193
    :pswitch_2
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/al;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/aj;->h:Lcom/google/android/gms/games/ui/destination/games/ak;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/games/ui/destination/games/ak;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 185
    :pswitch_data_0
    .packed-switch 0x7f0c0287
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
