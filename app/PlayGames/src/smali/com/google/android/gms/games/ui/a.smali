.class public final Lcom/google/android/gms/games/ui/a;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/b;

.field private final h:Ljava/lang/Object;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/gms/i;->d:I

    sput v0, Lcom/google/android/gms/games/ui/a;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/b;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/a;->g:Lcom/google/android/gms/games/ui/b;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/a;->h:Ljava/lang/Object;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/a;)I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/gms/games/ui/a;->i:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a;->h:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/a;)Lcom/google/android/gms/games/ui/b;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a;->g:Lcom/google/android/gms/games/ui/b;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/games/ui/a;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/google/android/gms/games/ui/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/c;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/games/ui/a;->e:I

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f0f0069

    iput v0, p0, Lcom/google/android/gms/games/ui/a;->i:I

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/a;->d()V

    .line 60
    return-void
.end method
