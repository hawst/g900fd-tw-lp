.class public Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final b:J

.field final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 638
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/ad;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 599
    iput-wide p1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    .line 600
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    .line 601
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 594
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    .line 595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    .line 596
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;)I
    .locals 4

    .prologue
    .line 605
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    iget-wide v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 606
    const/4 v0, -0x1

    .line 610
    :goto_0
    return v0

    .line 607
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    iget-wide v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 608
    const/4 v0, 0x1

    goto :goto_0

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 588
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 629
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 616
    instance-of v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    if-nez v2, :cond_1

    .line 623
    :cond_0
    :goto_0
    return v0

    .line 619
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 620
    goto :goto_0

    .line 622
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    .line 623
    iget-wide v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 634
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 635
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 636
    return-void
.end method
