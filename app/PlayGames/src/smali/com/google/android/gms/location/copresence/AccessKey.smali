.class public Lcom/google/android/gms/location/copresence/AccessKey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/location/copresence/a;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/AccessKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/google/android/gms/location/copresence/AccessKey;->a:I

    .line 41
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/AccessKey;->b:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessKey;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessKey;->b:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessKey;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "N/A"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessKey;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/a;->a(Lcom/google/android/gms/location/copresence/AccessKey;Landroid/os/Parcel;)V

    .line 65
    return-void
.end method
