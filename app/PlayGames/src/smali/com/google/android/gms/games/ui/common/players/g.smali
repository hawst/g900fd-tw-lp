.class public final Lcom/google/android/gms/games/ui/common/players/g;
.super Landroid/support/v4/app/x;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private aj:Lcom/google/android/gms/games/Player;

.field private ak:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/x;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/Player;Z)Lcom/google/android/gms/games/ui/common/players/g;
    .locals 4

    .prologue
    .line 52
    new-instance v1, Lcom/google/android/gms/games/ui/common/players/g;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/common/players/g;-><init>()V

    .line 53
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string v3, "com.google.android.gms.games.PLAYER"

    invoke-interface {p0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 55
    const-string v0, "isSelf"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/common/players/g;->g(Landroid/os/Bundle;)V

    .line 57
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 65
    sget v0, Lcom/google/android/gms/i;->F:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 66
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/g;->aj:Lcom/google/android/gms/games/Player;

    sget v0, Lcom/google/android/gms/g;->bp:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sget v0, Lcom/google/android/gms/g;->bo:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v0, Lcom/google/android/gms/g;->bE:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    sget v0, Lcom/google/android/gms/g;->m:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->bs:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->br:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/g;->aX:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v9, Lcom/google/android/gms/g;->bg:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->j()Landroid/content/res/Resources;

    move-result-object v10

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    sget v1, Lcom/google/android/gms/e;->u:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    sget v1, Lcom/google/android/gms/e;->v:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v10, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v0

    const/4 v1, 0x3

    new-array v1, v1, [F

    invoke-static {v0, v1}, Landroid/graphics/Color;->colorToHSV(I[F)V

    const/4 v0, 0x2

    aget v2, v1, v0

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v2, v11

    aput v2, v1, v0

    invoke-static {v1}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/google/android/gms/e;->U:I

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/e;->S:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->ak:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/l;->bN:I

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/google/android/gms/e;->U:I

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/e;->T:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/gms/e;->R:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/gms/e;->S:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x106000b

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 67
    return-object v4

    .line 66
    :cond_0
    const/16 v11, 0x8

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    sget v0, Lcom/google/android/gms/l;->bJ:I

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/x;->a_(Landroid/os/Bundle;)V

    .line 73
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 74
    const-string v0, "com.google.android.gms.games.PLAYER"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->aj:Lcom/google/android/gms/games/Player;

    .line 75
    const-string v0, "isSelf"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->ak:Z

    .line 76
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/x;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 81
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 82
    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 88
    invoke-static {p1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    instance-of v0, v0, Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 92
    if-nez v0, :cond_1

    .line 93
    const-string v0, "ProfileSummaryDFrag"

    const-string v1, "onClick: getActivity() returned null"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    instance-of v1, v0, Lcom/google/android/gms/games/ui/n;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 98
    check-cast v0, Lcom/google/android/gms/games/ui/n;

    .line 100
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 102
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/players/g;->ak:Z

    if-eqz v2, :cond_3

    .line 103
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;)V

    const/16 v2, 0x44d

    invoke-static {v2, v1}, Lcom/google/android/gms/games/ui/a/c;->a(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 106
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/g;->aj:Lcom/google/android/gms/games/Player;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;)V

    const/16 v3, 0x44e

    invoke-static {v3, v1}, Lcom/google/android/gms/games/ui/a/c;->a(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.gms.games.OTHER_PLAYER"

    const-string v1, "The given player cannot be null"

    invoke-static {v2, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v6

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/PlayerEntity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5, v4, v1, v6, v2}, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "GamesDestApi"

    const-string v2, "Failed to downgrade player safely! Aborting."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_5

    const-string v0, "GamesDestApi"

    const-string v1, "viewProfileComparison - Failed to add the player to the Intent"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v3, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    invoke-static {v0, v3}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 110
    :cond_6
    const-string v0, "ProfileSummaryDFrag"

    const-string v1, "onClick: Unhandled games configuration"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
