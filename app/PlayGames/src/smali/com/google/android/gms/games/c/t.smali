.class public final Lcom/google/android/gms/games/c/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/c/b;

.field public c:Lcom/google/android/gms/games/c/z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4173
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4174
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/t;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    iput-object v1, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/t;->G:I

    .line 4175
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 4246
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 4247
    iget v1, p0, Lcom/google/android/gms/games/c/t;->a:I

    if-eqz v1, :cond_0

    .line 4248
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/t;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4251
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-eqz v1, :cond_1

    .line 4252
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4255
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-eqz v1, :cond_2

    .line 4256
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4259
    :cond_2
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/t;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/z;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4232
    iget v0, p0, Lcom/google/android/gms/games/c/t;->a:I

    if-eqz v0, :cond_0

    .line 4233
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/t;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4235
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-eqz v0, :cond_1

    .line 4236
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4238
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-eqz v0, :cond_2

    .line 4239
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4241
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 4242
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4187
    if-ne p1, p0, :cond_1

    .line 4215
    :cond_0
    :goto_0
    return v0

    .line 4190
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/t;

    if-nez v2, :cond_2

    move v0, v1

    .line 4191
    goto :goto_0

    .line 4193
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/t;

    .line 4194
    iget v2, p0, Lcom/google/android/gms/games/c/t;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/t;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4195
    goto :goto_0

    .line 4197
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-nez v2, :cond_4

    .line 4198
    iget-object v2, p1, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4199
    goto :goto_0

    .line 4202
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    iget-object v3, p1, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4203
    goto :goto_0

    .line 4206
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-nez v2, :cond_6

    .line 4207
    iget-object v2, p1, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-eqz v2, :cond_0

    move v0, v1

    .line 4208
    goto :goto_0

    .line 4211
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    iget-object v3, p1, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4212
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4220
    iget v0, p0, Lcom/google/android/gms/games/c/t;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4222
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4224
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4226
    return v0

    .line 4222
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/t;->b:Lcom/google/android/gms/games/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4224
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/t;->c:Lcom/google/android/gms/games/c/z;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/z;->hashCode()I

    move-result v1

    goto :goto_1
.end method
