.class public abstract Lcom/google/android/gms/games/ui/destination/games/af;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/destination/games/ak;
.implements Lcom/google/android/gms/games/ui/destination/games/c;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/e/e;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    .line 287
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/af;->a(Lcom/google/android/gms/games/j;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 0

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/af;->b(Lcom/google/android/gms/common/api/t;)V

    .line 77
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 172
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->au()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/app/a;->b(I)V

    .line 213
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 134
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 165
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/j;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    .line 87
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    .line 91
    const/4 v1, 0x1

    .line 93
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    .line 110
    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    .line 96
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    .line 110
    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/af;->an:Lcom/google/android/gms/games/ui/e;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/e;->k()V

    .line 103
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/af;->an:Lcom/google/android/gms/games/ui/e;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 106
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/af;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPG_overflowMenu"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public final am()V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/games/ShopGamesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 192
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/af;->a(Landroid/content/Intent;)V

    .line 193
    return-void
.end method

.method public final aq()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public final ar()V
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/af;->a(Lcom/google/android/gms/games/app/a;)V

    .line 203
    return-void
.end method

.method protected at()Lcom/google/android/gms/games/ui/e;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/gms/games/ui/destination/games/aj;-><init>(Lcom/google/android/gms/games/ui/n;IILcom/google/android/gms/games/ui/destination/games/ak;)V

    return-object v0
.end method

.method protected abstract au()I
.end method

.method protected abstract b(Lcom/google/android/gms/common/api/t;)V
.end method

.method public final b(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 143
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 146
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 147
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/Game;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->at()Lcom/google/android/gms/games/ui/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->an:Lcom/google/android/gms/games/ui/e;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->an:Lcom/google/android/gms/games/ui/e;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 56
    const v0, 0x7f0b01ed

    iput v0, p0, Lcom/google/android/gms/games/ui/p;->al:I

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/af;->an:Lcom/google/android/gms/games/ui/e;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/af;->a(Landroid/support/v7/widget/bv;)V

    .line 60
    const v0, 0x7f0200fd

    const v1, 0x7f0f0153

    const v2, 0x7f0f0152

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/games/af;->a(III)V

    .line 67
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/af;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    const-string v0, "GameListMyGamesFragment"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/af;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 125
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/af;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
