.class public Lcom/google/android/gms/cloudsave/internal/QueryResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/g;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/QueryResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/QueryResponse;->a:I

    .line 53
    iput p2, p0, Lcom/google/android/gms/cloudsave/internal/QueryResponse;->b:I

    .line 54
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/internal/QueryResponse;->c:Lcom/google/android/gms/common/data/DataHolder;

    .line 55
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/g;->a(Lcom/google/android/gms/cloudsave/internal/QueryResponse;Landroid/os/Parcel;I)V

    .line 40
    return-void
.end method
