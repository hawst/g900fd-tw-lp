.class final Lcom/google/android/gms/games/ui/destination/quests/b;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 42
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 8

    .prologue
    .line 38
    check-cast p3, Lcom/google/android/gms/games/quest/Quest;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/quest/Quest;->h()Landroid/net/Uri;

    move-result-object v0

    const v1, 0x7f020090

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/quests/b;->a(Landroid/net/Uri;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/b;->t()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/games/quest/Quest;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/b;->a(Landroid/database/CharArrayBuffer;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p3}, Lcom/google/android/gms/games/quest/Quest;->q()J

    move-result-wide v0

    sub-long v4, v2, v0

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    const-wide/16 v4, 0x3e8

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/b;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/b;->k:Landroid/content/Context;

    const v2, 0x7f0f01ac

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p3}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/b;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/b;->k:Landroid/content/Context;

    const v1, 0x7f0f01ad

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
