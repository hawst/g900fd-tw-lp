.class public final Lcom/google/android/gms/games/internal/a/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/gms/games/internal/a/ap;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/ap;-><init>(Lcom/google/android/gms/games/internal/a/ao;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 114
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->s()V

    .line 115
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;I)V
    .locals 1

    .prologue
    .line 99
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->b(I)V

    .line 100
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 108
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;I)V

    .line 110
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/games/internal/a/ar;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/ar;-><init>(Lcom/google/android/gms/games/internal/a/ao;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;)Z
    .locals 1

    .prologue
    .line 119
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->t()Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 125
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/internal/c;->b(Z)V

    .line 127
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/google/android/gms/games/internal/a/at;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/internal/a/at;-><init>(Lcom/google/android/gms/games/internal/a/ao;Lcom/google/android/gms/common/api/t;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method
