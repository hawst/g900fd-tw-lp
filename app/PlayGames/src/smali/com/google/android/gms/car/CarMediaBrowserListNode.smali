.class public Lcom/google/android/gms/car/CarMediaBrowserListNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

.field public c:I

.field public d:I

.field public e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/car/g;

    invoke-direct {v0}, Lcom/google/android/gms/car/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    .line 100
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->a:I

    .line 101
    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;II[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    .line 110
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->a:I

    .line 111
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    .line 112
    iput p3, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->c:I

    .line 113
    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->d:I

    .line 114
    iput-object p5, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    .line 115
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 124
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/g;->a(Lcom/google/android/gms/car/CarMediaBrowserListNode;Landroid/os/Parcel;I)V

    .line 125
    return-void
.end method
