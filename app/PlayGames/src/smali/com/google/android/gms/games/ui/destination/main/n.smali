.class final Lcom/google/android/gms/games/ui/destination/main/n;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 79
    const v0, 0x7f0c00b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->m:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0c00e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->n:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0c00e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->o:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->o:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->m:Landroid/widget/TextView;

    const v1, 0x7f0f0084

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->n:Landroid/widget/TextView;

    const v1, 0x7f0f0083

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 92
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/n;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/main/m;

    .line 99
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/m;->a(Lcom/google/android/gms/games/ui/destination/main/m;)Lcom/google/android/gms/games/ui/destination/main/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/destination/main/o;->at()V

    .line 100
    return-void
.end method
