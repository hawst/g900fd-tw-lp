.class public Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

.field public final c:Lcom/google/android/gms/location/copresence/MessageFilter;

.field public final d:Lcom/google/android/gms/location/copresence/internal/d;

.field public final e:Landroid/app/PendingIntent;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/j;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/MessageFilter;Landroid/os/IBinder;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->f:I

    .line 61
    iput p2, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    .line 63
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    .line 64
    if-nez p5, :cond_0

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/d;

    .line 70
    :goto_0
    iput-object p6, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    .line 71
    return-void

    .line 67
    :cond_0
    invoke-static {p5}, Lcom/google/android/gms/location/copresence/internal/e;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/d;

    goto :goto_0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->f:I

    return v0
.end method

.method final b()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/d;

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/d;

    invoke-interface {v0}, Lcom/google/android/gms/location/copresence/internal/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SubscribeOperation[unknown type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SubscribeOperation[listener="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->b()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SubscribeOperation[pendingIntent="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 122
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/j;->a(Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Landroid/os/Parcel;I)V

    .line 123
    return-void
.end method
