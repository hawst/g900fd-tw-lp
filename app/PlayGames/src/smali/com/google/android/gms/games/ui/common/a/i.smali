.class public final Lcom/google/android/gms/games/ui/common/a/i;
.super Lcom/google/android/gms/games/ui/common/a/h;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/a/h;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/android/gms/games/ui/common/a/h;->c()V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/i;->P()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const-string v0, "QuestInboxFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/a/h;->d(Landroid/os/Bundle;)V

    .line 23
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/i;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    .line 24
    sget v0, Lcom/google/android/gms/l;->U:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 27
    sget v0, Lcom/google/android/gms/l;->bl:I

    .line 28
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/i;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v2

    .line 29
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/z;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 31
    :cond_0
    sget v0, Lcom/google/android/gms/l;->M:I

    .line 33
    :cond_1
    sget v2, Lcom/google/android/gms/f;->y:I

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lcom/google/android/gms/games/ui/common/a/i;->a(III)V

    .line 36
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 37
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/i;->am:Lcom/google/android/gms/games/ui/common/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/a/i;->a(Landroid/support/v7/widget/bv;)V

    .line 41
    return-void
.end method
