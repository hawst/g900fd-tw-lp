.class public final Lcom/google/android/gms/games/ui/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/b/a/i;


# static fields
.field private static final a:[I


# instance fields
.field private b:I

.field private c:I

.field private final d:Landroid/app/Activity;

.field private final e:Landroid/view/ViewConfiguration;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:Lcom/google/android/gms/games/ui/b/a/f;

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Landroid/app/AlertDialog;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/games/ui/b/a;->a:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x1
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    .line 58
    sget-object v0, Lcom/google/android/gms/games/ui/b/a;->a:[I

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->h:I

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b/a;->d:Landroid/app/Activity;

    .line 82
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->e:Landroid/view/ViewConfiguration;

    .line 83
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->f:Landroid/view/LayoutInflater;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->d:Landroid/app/Activity;

    invoke-static {v0, p0}, Lcom/google/android/gms/games/ui/b/a/f;->a(Landroid/app/Activity;Lcom/google/android/gms/games/ui/b/a/i;)Lcom/google/android/gms/games/ui/b/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->g:Lcom/google/android/gms/games/ui/b/a/f;

    .line 86
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    .line 324
    sget-object v0, Lcom/google/android/gms/games/ui/b/a;->a:[I

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 327
    iput-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    .line 328
    iput-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->n:Landroid/view/View;

    .line 329
    iput-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->o:Landroid/view/View;

    .line 330
    iput-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->p:Landroid/view/View;

    .line 332
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 297
    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    if-ne p1, v0, :cond_0

    .line 299
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a;->a()V

    .line 320
    :goto_0
    return-void

    .line 304
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    .line 307
    sget-object v0, Lcom/google/android/gms/games/ui/b/a;->a:[I

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    .line 310
    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 312
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f04002b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c00f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->n:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0c00f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->o:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0c00f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->p:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->d:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    :cond_1
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 316
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->g:Lcom/google/android/gms/games/ui/b/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/b/a/f;->a()V

    .line 317
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a;->a()V

    goto :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/KeyEvent;)V
    .locals 3

    .prologue
    .line 196
    :try_start_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 196
    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->n:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 199
    const-string v1, "CodeListener"

    const-string v2, "Exception in code listener"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 196
    :pswitch_2
    :try_start_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->o:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x6c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->p:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->p:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    packed-switch v1, :pswitch_data_2

    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b/a;->a(I)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x13

    if-ne v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-ne v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_7
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    goto :goto_1

    :pswitch_8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/high16 v4, 0x3fc00000    # 1.5f

    .line 90
    :try_start_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->h:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->h:I

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    packed-switch v1, :pswitch_data_0

    .line 95
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    .line 90
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->h:I

    if-ne v1, v0, :cond_0

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/google/android/gms/games/ui/b/a;->i:I

    iput v0, p0, Lcom/google/android/gms/games/ui/b/a;->j:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a;->k:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 91
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "CodeListener"

    const-string v2, "Exception in code listener"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 90
    :pswitch_2
    :try_start_1
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a;->k:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->i:I

    sub-int/2addr v1, v2

    int-to-double v2, v1

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->j:I

    sub-int v0, v1, v0

    int-to-double v0, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/b/a;->e:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-double v2, v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a;->k:Z

    goto :goto_1

    :pswitch_3
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a;->k:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->i:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/b/a;->j:I

    sub-int/2addr v2, v0

    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    iget v3, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    packed-switch v3, :pswitch_data_1

    :cond_2
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b/a;->a(I)V

    goto :goto_1

    :pswitch_4
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v1, v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v3, v1

    if-lez v1, :cond_2

    if-lez v2, :cond_2

    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_5
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v1, v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v3, v1

    if-lez v1, :cond_2

    if-gez v2, :cond_2

    goto :goto_3

    :pswitch_6
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_2

    if-lez v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    goto :goto_2

    :pswitch_7
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    if-nez v2, :cond_5

    if-gtz v1, :cond_6

    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    if-eqz v2, :cond_2

    if-gez v1, :cond_2

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_8
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z

    if-nez v2, :cond_7

    if-ltz v1, :cond_3

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/b/a;->l:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v2, :cond_2

    if-lez v1, :cond_2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/ui/b/a/g;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 358
    const-string v4, "com.google.android.gms.games.extra.name"

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v0, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_1

    move v0, v1

    :goto_0
    const/16 v6, 0xd

    invoke-static {v6}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget v5, v5, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v6, 0x258

    if-lt v5, v6, :cond_2

    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_3

    :cond_0
    const-string v0, "All your game are belong to us"

    :goto_2
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a;->d:Landroid/app/Activity;

    invoke-static {v0, p1, v3}, Lcom/google/android/gms/games/ui/b/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/games/ui/b/a/g;Landroid/os/Bundle;)V

    .line 360
    return-void

    :cond_1
    move v0, v2

    .line 358
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    const-string v0, "All your game\nare belong to us"

    goto :goto_2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 271
    iget v0, p0, Lcom/google/android/gms/games/ui/b/a;->b:I

    .line 273
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 293
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b/a;->a(I)V

    .line 294
    return-void

    .line 275
    :pswitch_0
    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :pswitch_1
    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :pswitch_2
    iget v1, p0, Lcom/google/android/gms/games/ui/b/a;->c:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x7f0c00f3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
