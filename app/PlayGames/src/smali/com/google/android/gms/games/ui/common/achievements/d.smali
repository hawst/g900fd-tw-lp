.class public abstract Lcom/google/android/gms/games/ui/common/achievements/d;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# static fields
.field private static final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/google/android/gms/i;->b:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/d;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 20

    .prologue
    .line 40
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/achievements/d;->requestWindowFeature(I)Z

    .line 41
    invoke-super/range {p0 .. p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 43
    sget v2, Lcom/google/android/gms/games/ui/common/achievements/d;->n:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/achievements/d;->setContentView(I)V

    .line 45
    invoke-super/range {p0 .. p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v2

    .line 46
    if-eqz v2, :cond_0

    .line 47
    invoke-virtual {v2}, Landroid/support/v7/app/a;->b()V

    .line 50
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 51
    const-string v3, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/achievement/Achievement;

    .line 52
    if-nez v2, :cond_1

    .line 53
    const-string v2, "AchieveDescrActivity"

    const-string v3, "Required achievement is missing."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->finish()V

    .line 259
    :goto_0
    return-void

    .line 58
    :cond_1
    sget v3, Lcom/google/android/gms/g;->bW:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 59
    sget v3, Lcom/google/android/gms/g;->aA:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 60
    sget v3, Lcom/google/android/gms/g;->cs:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 61
    sget v3, Lcom/google/android/gms/g;->cg:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 62
    sget v3, Lcom/google/android/gms/g;->aN:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 63
    sget v3, Lcom/google/android/gms/g;->bU:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 64
    sget v3, Lcom/google/android/gms/g;->b:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 65
    sget v9, Lcom/google/android/gms/g;->c:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    .line 68
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v16

    .line 69
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->d()I

    move-result v10

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 72
    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    const/4 v10, 0x1

    move/from16 v0, v16

    if-ne v0, v10, :cond_2

    const/4 v10, 0x1

    .line 76
    :goto_1
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->f()Ljava/lang/String;

    move-result-object v14

    .line 80
    const-string v13, ""

    .line 81
    const-string v12, ""

    .line 83
    if-nez v10, :cond_6

    .line 86
    packed-switch v16, :pswitch_data_0

    .line 107
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown achievement state "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 72
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 88
    :pswitch_0
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->g()Landroid/net/Uri;

    move-result-object v10

    sget v11, Lcom/google/android/gms/f;->j:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 90
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->q()J

    move-result-wide v10

    const/high16 v12, 0x80000

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v12}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v12

    .line 93
    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    sget v10, Lcom/google/android/gms/d;->q:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTextColor(I)V

    move-object v11, v12

    .line 116
    :goto_2
    if-nez v16, :cond_3

    .line 117
    const/16 v10, 0xff

    .line 123
    :goto_3
    const/16 v12, 0x10

    invoke-static {v12}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 124
    invoke-virtual {v3, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageAlpha(I)V

    .line 129
    :goto_4
    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    .line 130
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 132
    if-nez v16, :cond_5

    .line 133
    sget v3, Lcom/google/android/gms/l;->bF:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v11

    .line 173
    :goto_5
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_a

    .line 174
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v10

    .line 178
    const-wide/16 v18, 0x0

    cmp-long v12, v10, v18

    if-lez v12, :cond_9

    .line 179
    sget v12, Lcom/google/android/gms/l;->z:I

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v16

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 181
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :goto_6
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    move-object v2, v14

    .line 200
    :goto_7
    sget v12, Lcom/google/android/gms/d;->d:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    .line 201
    sget v13, Lcom/google/android/gms/d;->d:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 202
    sget v14, Lcom/google/android/gms/d;->v:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 203
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 204
    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 205
    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 207
    sget v12, Lcom/google/android/gms/l;->bD:I

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v11, v13, v14

    const/4 v11, 0x1

    aput-object v2, v13, v11

    const/4 v2, 0x2

    aput-object v9, v13, v2

    const/4 v2, 0x3

    aput-object v10, v13, v2

    const/4 v2, 0x4

    aput-object v3, v13, v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 221
    invoke-virtual {v4}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    .line 222
    new-instance v2, Lcom/google/android/gms/games/ui/common/achievements/e;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/games/ui/common/achievements/e;-><init>(Lcom/google/android/gms/games/ui/common/achievements/d;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v9, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    .line 98
    :pswitch_1
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->i()Landroid/net/Uri;

    move-result-object v10

    sget v11, Lcom/google/android/gms/f;->i:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 100
    const/4 v10, 0x4

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v11, v12

    .line 101
    goto/16 :goto_2

    .line 103
    :pswitch_2
    const/4 v10, 0x0

    sget v11, Lcom/google/android/gms/f;->h:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 104
    const/4 v10, 0x4

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v11, v12

    .line 105
    goto/16 :goto_2

    .line 119
    :cond_3
    sget v10, Lcom/google/android/gms/d;->r:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 120
    invoke-static {v10}, Landroid/graphics/Color;->alpha(I)I

    move-result v10

    goto/16 :goto_3

    .line 126
    :cond_4
    invoke-virtual {v3, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setAlpha(I)V

    goto/16 :goto_4

    .line 136
    :cond_5
    sget v3, Lcom/google/android/gms/l;->bE:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v11

    .line 139
    goto/16 :goto_5

    .line 142
    :cond_6
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->o()I

    move-result v10

    .line 143
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->k()I

    move-result v11

    .line 146
    if-gtz v11, :cond_7

    .line 149
    const-string v16, "AchieveDescrActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Inconsistent achievement "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": TYPE_INCREMENTAL, but totalSteps = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-static {v0, v11}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v11, 0x1

    .line 155
    :cond_7
    if-lt v10, v11, :cond_8

    .line 159
    const-string v16, "AchieveDescrActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Inconsistent achievement "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": STATE_REVEALED, but steps = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v18, " / "

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v10, v11

    .line 165
    :cond_8
    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b(II)V

    .line 167
    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 168
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    .line 170
    mul-int/lit8 v3, v10, 0x64

    div-int/2addr v3, v11

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/gms/l;->aX:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v11, v16

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v12

    goto/16 :goto_5

    .line 184
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v10, v13

    goto/16 :goto_6

    .line 190
    :cond_a
    sget v2, Lcom/google/android/gms/l;->u:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    sget v2, Lcom/google/android/gms/l;->t:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const/16 v2, 0x8

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    sget v2, Lcom/google/android/gms/l;->u:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 195
    sget v2, Lcom/google/android/gms/l;->t:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v11, v10

    move-object v10, v13

    goto/16 :goto_7

    .line 86
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
