.class public final Lcom/google/android/gms/games/ui/card/aa;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/card/ab;

.field private final h:Lcom/google/android/gms/games/ui/card/ac;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/google/android/gms/i;->t:I

    sput v0, Lcom/google/android/gms/games/ui/card/aa;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 35
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/card/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;B)V

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/card/ab;Lcom/google/android/gms/games/ui/card/ac;Ljava/lang/String;B)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/games/ui/card/aa;->g:Lcom/google/android/gms/games/ui/card/ab;

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/games/ui/card/aa;->h:Lcom/google/android/gms/games/ui/card/ac;

    .line 44
    iput-object p4, p0, Lcom/google/android/gms/games/ui/card/aa;->i:Ljava/lang/String;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/card/aa;)Lcom/google/android/gms/games/ui/card/ab;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/aa;->g:Lcom/google/android/gms/games/ui/card/ab;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/card/aa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/aa;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/card/aa;)Lcom/google/android/gms/games/ui/card/ac;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/aa;->h:Lcom/google/android/gms/games/ui/card/ac;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/games/ui/card/ad;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/aa;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/card/aa;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/card/ad;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/google/android/gms/games/ui/card/aa;->e:I

    return v0
.end method
