.class public final Lcom/google/android/gms/games/ui/common/players/d;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private final g:I

.field private final h:Lcom/google/android/gms/games/ui/common/players/e;

.field private i:Lcom/google/android/gms/games/Player;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/gms/i;->E:I

    sput v0, Lcom/google/android/gms/games/ui/common/players/d;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/e;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->g:I

    .line 72
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/players/d;->h:Lcom/google/android/gms/games/ui/common/players/e;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->i:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 86
    if-nez p0, :cond_1

    .line 87
    const-string v1, "ProfileSummaryAdapter"

    const-string v2, "Player to mark as added is null. Let\'s do nothing in this case."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v1

    .line 95
    invoke-interface {v1}, Lcom/google/android/gms/common/a/a/c;->d()Ljava/util/ArrayList;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/players/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->g:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/players/d;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->j:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->h:Lcom/google/android/gms/games/ui/common/players/e;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/f;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/d;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/players/d;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/players/f;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 1

    .prologue
    .line 76
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->i:Lcom/google/android/gms/games/Player;

    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/d;->c(Z)V

    .line 78
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 101
    sget v0, Lcom/google/android/gms/games/ui/common/players/d;->e:I

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/d;->j:Z

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/d;->d()V

    .line 83
    return-void
.end method
