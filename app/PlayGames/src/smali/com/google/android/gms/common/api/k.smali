.class public abstract Lcom/google/android/gms/common/api/k;
.super Lcom/google/android/gms/common/api/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/af;


# instance fields
.field private final b:Lcom/google/android/gms/common/api/h;

.field private final c:Lcom/google/android/gms/common/api/t;

.field private d:Lcom/google/android/gms/common/api/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/h;Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 101
    invoke-interface {p2}, Lcom/google/android/gms/common/api/t;->a()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/j;-><init>(Landroid/os/Looper;)V

    .line 102
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/h;

    iput-object v0, p0, Lcom/google/android/gms/common/api/k;->b:Lcom/google/android/gms/common/api/h;

    .line 103
    iput-object p2, p0, Lcom/google/android/gms/common/api/k;->c:Lcom/google/android/gms/common/api/t;

    .line 104
    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    .prologue
    .line 206
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 208
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/k;->b(Lcom/google/android/gms/common/api/Status;)V

    .line 209
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/ad;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/gms/common/api/k;->d:Lcom/google/android/gms/common/api/ad;

    .line 180
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/f;)V
    .locals 1

    .prologue
    .line 155
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/k;->b(Lcom/google/android/gms/common/api/f;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :goto_0
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/k;->a(Landroid/os/RemoteException;)V

    .line 159
    throw v0

    .line 160
    :catch_1
    move-exception v0

    .line 161
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/k;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/k;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/k;->a(Lcom/google/android/gms/common/api/am;)V

    .line 172
    return-void

    .line 170
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Lcom/google/android/gms/common/api/f;)V
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Lcom/google/android/gms/common/api/j;->d()V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/common/api/k;->d:Lcom/google/android/gms/common/api/ad;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/common/api/k;->d:Lcom/google/android/gms/common/api/ad;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/ad;->a(Lcom/google/android/gms/common/api/af;)V

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/k;->d:Lcom/google/android/gms/common/api/ad;

    .line 203
    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/gms/common/api/h;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/common/api/k;->b:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method
