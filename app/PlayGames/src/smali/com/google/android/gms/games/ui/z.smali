.class public final Lcom/google/android/gms/games/ui/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field private final b:Lcom/google/android/gms/games/ui/n;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    .line 46
    iput p2, p0, Lcom/google/android/gms/games/ui/z;->a:I

    .line 47
    if-eq p2, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid UI Type"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 50
    iput p3, p0, Lcom/google/android/gms/games/ui/z;->c:I

    .line 51
    if-nez p3, :cond_2

    :goto_1
    const-string v0, "Invalid Device Type"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 52
    return-void

    :cond_1
    move v0, v1

    .line 47
    goto :goto_0

    :cond_2
    move v2, v1

    .line 51
    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 55
    iget v1, p0, Lcom/google/android/gms/games/ui/z;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 71
    iget v1, p0, Lcom/google/android/gms/games/ui/z;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 86
    iget-object v2, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    const-string v0, "GamesUiConfig"

    const-string v2, "getCurrentGameId: not connected; ignoring..."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 108
    :goto_0
    return-object v0

    .line 90
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/z;->a:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 92
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 96
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/aa;

    if-eqz v0, :cond_6

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/aa;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/aa;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_5

    .line 99
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_5
    const-string v0, "GamesUiConfig"

    const-string v2, "Trying to get a current game Id from a CurrentGameProvider but we don\'t have a current game yet."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 103
    goto :goto_0

    .line 106
    :cond_6
    const-string v0, "GamesUiConfig"

    const-string v2, "Trying to get a current game Id in a destination context. Returning null as we are not game specific here"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 108
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    const-string v0, "GamesUiConfig"

    const-string v1, "getCurrentPlayerId: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    .line 120
    :cond_1
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    const-string v0, "GamesUiConfig"

    const-string v1, "getCurrentAccountName: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    .line 134
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
