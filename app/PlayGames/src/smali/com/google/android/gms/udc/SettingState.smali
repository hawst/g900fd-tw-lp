.class public Lcom/google/android/gms/udc/SettingState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/udc/b;


# instance fields
.field private final a:I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/gms/udc/b;

    invoke-direct {v0}, Lcom/google/android/gms/udc/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    .line 120
    return-void
.end method

.method constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput p1, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    .line 132
    iput p2, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    .line 133
    iput p3, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    .line 134
    iput p4, p0, Lcom/google/android/gms/udc/SettingState;->d:I

    .line 135
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/udc/b;->a(Lcom/google/android/gms/udc/SettingState;Landroid/os/Parcel;)V

    .line 158
    return-void
.end method
