.class public final Lcom/google/android/gms/games/internal/a/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/google/android/gms/games/internal/a/ax;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/ax;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 290
    new-instance v0, Lcom/google/android/gms/games/internal/a/bi;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/bi;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 474
    new-instance v0, Lcom/google/android/gms/games/internal/a/bc;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/bc;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/api/aj;
    .locals 7

    .prologue
    .line 455
    new-instance v0, Lcom/google/android/gms/games/internal/a/bb;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/internal/a/bb;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Z)V
    .locals 1

    .prologue
    .line 284
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->a(Z)V

    .line 285
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 193
    new-instance v0, Lcom/google/android/gms/games/internal/a/az;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/games/internal/a/az;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 411
    new-instance v0, Lcom/google/android/gms/games/internal/a/ay;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ay;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Z)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 553
    new-instance v0, Lcom/google/android/gms/games/internal/a/bh;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bh;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 176
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->l()Lcom/google/android/gms/games/Player;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 542
    new-instance v0, Lcom/google/android/gms/games/internal/a/bg;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/bg;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 497
    new-instance v0, Lcom/google/android/gms/games/internal/a/bd;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bd;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 423
    new-instance v0, Lcom/google/android/gms/games/internal/a/ba;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/ba;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 447
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/aw;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/internal/a/aw;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 509
    new-instance v0, Lcom/google/android/gms/games/internal/a/be;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/be;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 521
    new-instance v0, Lcom/google/android/gms/games/internal/a/bf;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/bf;-><init>(Lcom/google/android/gms/games/internal/a/aw;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method
