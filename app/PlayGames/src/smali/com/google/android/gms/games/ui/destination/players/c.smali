.class final Lcom/google/android/gms/games/ui/destination/players/c;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# instance fields
.field private final m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/c;->w()Landroid/view/View;

    move-result-object v1

    .line 67
    const v0, 0x7f0c00d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 69
    const v0, 0x7f0c00da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 71
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V
    .locals 1

    .prologue
    .line 166
    const v0, 0x7f0b0048

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 168
    const v0, 0x7f0b004a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 170
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 10

    .prologue
    .line 56
    check-cast p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->h:Lcom/google/android/gms/games/Player;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->j:Lcom/google/android/gms/games/Player;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;Z)V

    const-string v6, ""

    iget v0, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    if-nez v0, :cond_0

    iget v0, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    const v0, 0x7f0f01a9

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->e(Z)V

    iget v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown achievement state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x3e19999a    # 0.15f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    const v0, 0x7f0f01ab

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->k:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x3e19999a    # 0.15f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    const v0, 0x7f0f01aa

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->m:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x3e19999a    # 0.15f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->n:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x3e19999a    # 0.15f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setAlpha(F)V

    const v0, 0x7f0f01a8

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->f:Landroid/net/Uri;

    const v2, 0x7f02009f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Landroid/net/Uri;I)V

    :goto_1
    iget v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->d(Ljava/lang/String;)V

    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->e(Ljava/lang/String;)V

    iget-wide v2, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->d:J

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-lez v1, :cond_3

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0f0034

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v5, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->g(Ljava/lang/String;)V

    :cond_3
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget v2, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->i:I

    if-nez v2, :cond_5

    const v4, 0x7f0a0033

    const v3, 0x7f0a0031

    const v2, 0x7f0a00d3

    :goto_3
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/destination/players/c;->f(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/players/c;->h(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/players/c;->l(I)V

    const v2, 0x7f0f01a7

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v7, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->a:Ljava/lang/String;

    aput-object v7, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    iget-object v4, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->b:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object v1, v3, v0

    const/4 v0, 0x4

    aput-object v6, v3, v0

    invoke-virtual {v5, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Ljava/lang/String;)V

    return-void

    :pswitch_1
    iget-object v1, p3, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;->g:Landroid/net/Uri;

    const v2, 0x7f02009d

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Landroid/net/Uri;I)V

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x0

    const v2, 0x7f02009c

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/c;->a(Landroid/net/Uri;I)V

    goto :goto_1

    :cond_4
    const v1, 0x7f0f003a

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->e(I)V

    const v1, 0x7f0f0039

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/c;->g(I)V

    const-string v1, ""

    goto :goto_2

    :cond_5
    const v4, 0x7f0a0032

    const v3, 0x7f0a0030

    const v2, 0x7f0a0034

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final varargs a([Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;

    .line 188
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter;)Lcom/google/android/gms/games/ui/destination/players/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/c;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/b;->a(Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonAdapter$AchievementComparison;)V

    .line 189
    return-void
.end method

.method protected final s(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 174
    const v0, 0x7f0c000a

    if-ne p1, v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/c;->k:Landroid/content/Context;

    const v1, 0x7f04001b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 179
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown image view type received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
