.class final Lcom/google/android/gms/games/ui/destination/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/p;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/b;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/destination/b;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/destination/b;B)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/e;-><init>(Lcom/google/android/gms/games/ui/destination/b;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/games/ui/destination/b;)Landroid/support/v4/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/a;->a(I)V

    .line 472
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/games/ui/destination/b;)Landroid/support/v4/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/a;->a(Landroid/view/View;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "drawerHasBeenOpened"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 446
    if-nez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, "drawerHasBeenOpened"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->I()V

    .line 452
    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/games/ui/destination/b;)Landroid/support/v4/app/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/a;->a(Landroid/view/View;F)V

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/b;->K()V

    .line 466
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/b;->a(Lcom/google/android/gms/games/ui/destination/b;)Landroid/support/v4/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/a;->b(Landroid/view/View;)V

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/e;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->J()V

    .line 459
    return-void
.end method
