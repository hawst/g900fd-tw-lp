.class public abstract Lcom/google/android/gms/games/ui/p;
.super Lcom/google/android/gms/games/ui/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;
.implements Lcom/google/android/gms/common/api/w;
.implements Lcom/google/android/gms/games/ui/e/e;
.implements Lcom/google/android/gms/games/ui/e/p;
.implements Lcom/google/android/gms/games/ui/e/q;
.implements Lcom/google/android/gms/games/ui/e/r;


# instance fields
.field protected aj:Landroid/support/v4/widget/SwipeRefreshLayout;

.field protected ak:Landroid/support/v4/widget/bk;

.field public al:I

.field private final am:Landroid/os/Handler;

.field private final an:Ljava/lang/Runnable;

.field private ao:Z

.field private ap:Landroid/support/v7/widget/cg;

.field private final aq:Lcom/google/android/gms/games/ui/ay;

.field private ar:Landroid/database/ContentObserver;

.field private as:Z

.field private at:Z

.field private final au:Landroid/support/v7/widget/cg;

.field protected d:Lcom/google/android/gms/games/ui/n;

.field protected e:Landroid/view/LayoutInflater;

.field protected f:Landroid/view/View;

.field public g:Landroid/support/v7/widget/RecyclerView;

.field protected h:Lcom/google/android/gms/games/ui/e/o;

.field protected i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    sget v0, Lcom/google/android/gms/i;->H:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 107
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/m;-><init>(I)V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->am:Landroid/os/Handler;

    .line 73
    new-instance v0, Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/q;-><init>(Lcom/google/android/gms/games/ui/p;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->an:Ljava/lang/Runnable;

    .line 89
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->i:Z

    .line 93
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->ao:Z

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->at:Z

    .line 103
    iput v1, p0, Lcom/google/android/gms/games/ui/p;->al:I

    .line 495
    new-instance v0, Lcom/google/android/gms/games/ui/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/t;-><init>(Lcom/google/android/gms/games/ui/p;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->au:Landroid/support/v7/widget/cg;

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->aq:Lcom/google/android/gms/games/ui/ay;

    .line 113
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/p;)Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/ui/ax;)Lcom/google/android/gms/games/ui/az;
    .locals 4

    .prologue
    .line 749
    new-instance v0, Lcom/google/android/gms/games/ui/az;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-interface {p1, v2}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/az;-><init>(Landroid/content/Context;IZ)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/p;Z)Z
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/p;->ao:Z

    return p1
.end method

.method private as()Lcom/google/android/gms/games/ui/w;
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 306
    if-nez v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 310
    :goto_0
    return-object v0

    .line 309
    :cond_0
    instance-of v1, v0, Lcom/google/android/gms/games/ui/w;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 310
    check-cast v0, Lcom/google/android/gms/games/ui/w;

    goto :goto_0
.end method

.method private at()V
    .locals 3

    .prologue
    .line 321
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->V:Z

    if-nez v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 329
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->as()Lcom/google/android/gms/games/ui/w;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 334
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/w;->b(Z)V

    .line 335
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->as:Z

    if-nez v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/google/android/gms/games/ui/r;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/r;-><init>(Lcom/google/android/gms/games/ui/p;Lcom/google/android/gms/games/ui/w;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private au()V
    .locals 3

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Landroid/view/View;

    .line 393
    if-nez v0, :cond_1

    .line 394
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_1
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_4

    .line 397
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    .line 411
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->a()V

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/view/View;)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->R()Z

    move-result v0

    if-nez v0, :cond_2

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->au:Landroid/support/v7/widget/cg;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cg;)V

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/cd;

    move-result-object v0

    if-nez v0, :cond_3

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/am;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v2}, Landroid/support/v7/widget/am;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cd;)V

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/gms/games/ui/s;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/s;-><init>(Lcom/google/android/gms/games/ui/p;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cb;)V

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->am:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->an:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 399
    :cond_4
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 400
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_6

    .line 401
    if-nez v0, :cond_5

    .line 402
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a RecyclerView whose id attribute is android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a RecyclerView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_6
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/p;)Landroid/support/v7/widget/cg;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->ap:Landroid/support/v7/widget/cg;

    return-object v0
.end method

.method private b(II)Lcom/google/android/gms/games/ui/az;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 763
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->j()Landroid/content/res/Resources;

    move-result-object v2

    .line 767
    if-eqz p1, :cond_1

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 770
    :goto_0
    if-eqz p2, :cond_0

    .line 771
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 772
    sub-int v0, v2, v0

    .line 775
    :cond_0
    new-instance v2, Lcom/google/android/gms/games/ui/az;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/gms/games/ui/az;-><init>(Landroid/content/Context;IZ)V

    return-object v2

    :cond_1
    move v0, v1

    .line 767
    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 167
    sget v0, Lcom/google/android/gms/g;->ck:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->i:Z

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/gms/d;->l:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/d;->n:I

    aput v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/gms/d;->o:I

    aput v3, v1, v2

    const/4 v2, 0x3

    sget v3, Lcom/google/android/gms/d;->m:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a([I)V

    .line 173
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->i:Z

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->ak:Landroid/support/v4/widget/bk;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->ak:Landroid/support/v4/widget/bk;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bk;)V

    .line 180
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_1

    .line 182
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 183
    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v0

    .line 184
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 185
    sget v2, Lcom/google/android/gms/e;->N:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 187
    iget-object v2, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)V

    .line 189
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/p;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->ao:Z

    return v0
.end method


# virtual methods
.method protected final C_()V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 573
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    .line 575
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/m;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 211
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;->b(Landroid/view/View;)V

    .line 212
    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/gms/games/ui/p;->e:Landroid/view/LayoutInflater;

    .line 139
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/m;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;->b(Landroid/view/View;)V

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/View;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->h:Lcom/google/android/gms/games/ui/e/o;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    return-object v0
.end method

.method protected a(Landroid/view/View;)Lcom/google/android/gms/games/ui/e/o;
    .locals 11

    .prologue
    .line 153
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v7

    .line 154
    if-eqz v7, :cond_0

    .line 155
    new-instance v0, Lcom/google/android/gms/games/ui/e/o;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    const v2, 0x102000a

    sget v3, Lcom/google/android/gms/g;->aP:I

    sget v4, Lcom/google/android/gms/g;->K:I

    sget v5, Lcom/google/android/gms/g;->bb:I

    sget v6, Lcom/google/android/gms/g;->ao:I

    iget-object v8, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-interface {v7, v8}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v10

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/e/o;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/e/q;Lcom/google/android/gms/games/ui/e/p;Lcom/google/android/gms/games/ui/e/r;I)V

    .line 161
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/e/o;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    invoke-direct {v0, v1, p0, p0, p0}, Lcom/google/android/gms/games/ui/e/o;-><init>(Landroid/view/View;Lcom/google/android/gms/games/ui/e/q;Lcom/google/android/gms/games/ui/e/p;Lcom/google/android/gms/games/ui/e/r;)V

    goto :goto_0
.end method

.method protected final a(II)V
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->K:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 638
    sget v0, Lcom/google/android/gms/g;->J:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 639
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 640
    sget v0, Lcom/google/android/gms/g;->I:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 641
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 644
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->bb:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 645
    sget v0, Lcom/google/android/gms/g;->ba:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 646
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 647
    sget v0, Lcom/google/android/gms/g;->M:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 648
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 651
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->ao:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 652
    sget v0, Lcom/google/android/gms/g;->an:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 653
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 654
    sget v0, Lcom/google/android/gms/g;->am:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 655
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 656
    return-void
.end method

.method protected final a(III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->J:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 613
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 614
    invoke-virtual {v0, v2, p1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 617
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->I:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 618
    if-lez p3, :cond_1

    .line 619
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 620
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 621
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/widget/bk;)V
    .locals 2

    .prologue
    .line 693
    iput-object p1, p0, Lcom/google/android/gms/games/ui/p;->ak:Landroid/support/v4/widget/bk;

    .line 694
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->ak:Landroid/support/v4/widget/bk;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bk;)V

    .line 697
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bv;)V
    .locals 4

    .prologue
    .line 251
    instance-of v0, p1, Lcom/google/android/gms/games/ui/w;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 252
    check-cast p1, Lcom/google/android/gms/games/ui/w;

    .line 253
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->au()V

    .line 255
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/w;->t()I

    move-result v1

    .line 259
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v2

    .line 260
    if-eqz v2, :cond_2

    .line 262
    instance-of v0, p1, Lcom/google/android/gms/games/ui/ak;

    if-eqz v0, :cond_4

    .line 263
    if-nez v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/p;->al:I

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 264
    check-cast v0, Lcom/google/android/gms/games/ui/ak;

    iget v3, p0, Lcom/google/android/gms/games/ui/p;->al:I

    invoke-direct {p0, v1, v3}, Lcom/google/android/gms/games/ui/p;->b(II)Lcom/google/android/gms/games/ui/az;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ak;->b(Lcom/google/android/gms/games/ui/w;)V

    :cond_1
    move-object v0, p1

    .line 267
    check-cast v0, Lcom/google/android/gms/games/ui/ak;

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/p;->a(Lcom/google/android/gms/games/ui/ax;)Lcom/google/android/gms/games/ui/az;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ak;->b(Lcom/google/android/gms/games/ui/w;)V

    .line 284
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/cd;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/am;

    .line 285
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/w;->r()I

    move-result v1

    .line 286
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(I)V

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/bv;)V

    .line 288
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/w;->q()Landroid/support/v7/widget/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(Landroid/support/v7/widget/ap;)V

    .line 291
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    new-instance v0, Lcom/google/android/gms/games/ui/bc;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/bc;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/by;)V

    .line 294
    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/w;->a(Landroid/support/v7/widget/by;)V

    .line 295
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->at()V

    .line 297
    :cond_3
    return-void

    .line 270
    :cond_4
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 271
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/p;->a(Lcom/google/android/gms/games/ui/ax;)Lcom/google/android/gms/games/ui/az;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 272
    if-nez v1, :cond_5

    iget v2, p0, Lcom/google/android/gms/games/ui/p;->al:I

    if-eqz v2, :cond_6

    .line 273
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/ui/p;->al:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/ui/p;->b(II)Lcom/google/android/gms/games/ui/az;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 276
    :cond_6
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 277
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object p1

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/m;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->au()V

    .line 219
    return-void
.end method

.method public final a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/p;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 194
    return-void
.end method

.method public final ah()V
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->at:Z

    if-eqz v0, :cond_0

    .line 351
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->as:Z

    .line 353
    :cond_0
    return-void
.end method

.method public final ai()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 362
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->at:Z

    .line 363
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->as:Z

    if-eqz v0, :cond_1

    .line 364
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->as()Lcom/google/android/gms/games/ui/w;

    move-result-object v0

    .line 365
    if-nez v0, :cond_0

    .line 372
    :goto_0
    return-void

    .line 368
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/w;->b(Z)V

    .line 371
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->as:Z

    goto :goto_0
.end method

.method protected final aj()V
    .locals 4

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 564
    :goto_0
    return-void

    .line 548
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/u;-><init>(Lcom/google/android/gms/games/ui/p;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 562
    sget-object v1, Lcom/google/android/gms/games/internal/b/a;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/p;->ar:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected al()V
    .locals 0

    .prologue
    .line 586
    return-void
.end method

.method public am()V
    .locals 0

    .prologue
    .line 595
    return-void
.end method

.method public final an()V
    .locals 0

    .prologue
    .line 599
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->at()V

    .line 600
    return-void
.end method

.method public final ao()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 669
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/p;->i:Z

    .line 670
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 673
    :cond_0
    return-void
.end method

.method public final ap()V
    .locals 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->aj:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    .line 710
    :cond_0
    return-void
.end method

.method public aq()V
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/o;->a()I

    move-result v0

    .line 781
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 784
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_1

    .line 786
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ax;->ag()F

    move-result v0

    .line 787
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->h:Lcom/google/android/gms/games/ui/e/o;

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/o;->a(I)V

    .line 790
    :cond_1
    return-void
.end method

.method public ar()V
    .locals 0

    .prologue
    .line 795
    return-void
.end method

.method public final b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->au:Landroid/support/v7/widget/cg;

    invoke-virtual {p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/support/v7/widget/cg;)V

    .line 206
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->d(Landroid/os/Bundle;)V

    .line 118
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/n;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    .line 119
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 472
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->d(Z)V

    .line 477
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 478
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->au()V

    .line 479
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 480
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/ui/af;

    if-eqz v1, :cond_0

    .line 481
    check-cast v0, Lcom/google/android/gms/games/ui/af;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/ui/af;->a(ZZ)V

    .line 485
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->am:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->an:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/ui/d;

    if-eqz v1, :cond_0

    .line 227
    check-cast v0, Lcom/google/android/gms/games/ui/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d;->b()V

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    .line 231
    invoke-super {p0}, Lcom/google/android/gms/games/ui/m;->f()V

    .line 232
    return-void
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 376
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/m;->h(Landroid/os/Bundle;)V

    .line 380
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;->as()Lcom/google/android/gms/games/ui/w;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/w;->r()I

    move-result v1

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/cd;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/am;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(I)V

    .line 385
    :cond_0
    return-void
.end method

.method public final p_()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/google/android/gms/games/ui/m;->p_()V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->a(Lcom/google/android/gms/common/api/v;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->a(Lcom/google/android/gms/common/api/w;)V

    .line 126
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->b(Lcom/google/android/gms/common/api/v;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/n;->b(Lcom/google/android/gms/common/api/w;)V

    .line 132
    invoke-super {p0}, Lcom/google/android/gms/games/ui/m;->q_()V

    .line 133
    return-void
.end method
