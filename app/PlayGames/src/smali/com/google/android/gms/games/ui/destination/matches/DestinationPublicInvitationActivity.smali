.class public final Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/g;
.implements Lcom/google/android/gms/games/ui/common/matches/ab;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# instance fields
.field private A:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private z:Lcom/google/android/gms/games/ui/destination/matches/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    const v0, 0x7f040049

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 35
    return-void
.end method


# virtual methods
.method public final Q()Lcom/google/android/gms/games/ui/c/a/f;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->A:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->A:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->B:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->C:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/google/android/gms/games/ui/destination/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/matches/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->z:Lcom/google/android/gms/games/ui/destination/matches/a;

    .line 50
    const v0, 0x7f0f00ee

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->setTitle(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->A:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/matches/DestinationPublicInvitationActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
