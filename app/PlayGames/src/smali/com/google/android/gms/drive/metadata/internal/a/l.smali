.class public final Lcom/google/android/gms/drive/metadata/internal/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/drive/metadata/internal/a/m;

.field public static final b:Lcom/google/android/gms/drive/metadata/internal/a/n;

.field public static final c:Lcom/google/android/gms/drive/metadata/internal/a/p;

.field public static final d:Lcom/google/android/gms/drive/metadata/internal/a/o;

.field public static final e:Lcom/google/android/gms/drive/metadata/internal/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/m;

    const-string v1, "created"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->a:Lcom/google/android/gms/drive/metadata/internal/a/m;

    .line 29
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/n;

    const-string v1, "lastOpenedTime"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/p;

    const-string v1, "modified"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    .line 33
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/o;

    const-string v1, "modifiedByMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->d:Lcom/google/android/gms/drive/metadata/internal/a/o;

    .line 35
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/q;

    const-string v1, "sharedWithMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    return-void
.end method
