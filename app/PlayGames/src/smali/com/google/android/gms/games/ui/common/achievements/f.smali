.class public Lcom/google/android/gms/games/ui/common/achievements/f;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/common/achievements/b;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/achievements/g;

.field private an:Lcom/google/android/gms/games/ui/common/achievements/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method


# virtual methods
.method public R()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 163
    sget v1, Lcom/google/android/gms/e;->P:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/e;->O:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    .line 166
    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 24
    check-cast p1, Lcom/google/android/gms/games/achievement/d;

    invoke-interface {p1}, Lcom/google/android/gms/games/achievement/d;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/achievement/d;->c()Lcom/google/android/gms/games/achievement/a;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/f;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->an:Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/common/achievements/a;->a(Lcom/google/android/gms/common/data/b;)V

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->a()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/achievement/a;->b(I)Lcom/google/android/gms/games/achievement/Achievement;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->am:Lcom/google/android/gms/games/ui/common/achievements/g;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->am:Lcom/google/android/gms/games/ui/common/achievements/g;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/games/ui/common/achievements/g;->d(II)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v4, v1}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    sget-object v0, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v0, v3}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/achievement/Achievement;)V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/games/achievement/Achievement;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/AchievementEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.destination.ACTION_VIEW_ACHIEVEMENT_DESCRIPTION"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void

    .line 133
    :cond_0
    sget-object v3, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v3, v1, v0}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 37
    new-instance v0, Lcom/google/android/gms/games/ui/common/achievements/a;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/achievements/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/achievements/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->an:Lcom/google/android/gms/games/ui/common/achievements/a;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v1

    .line 41
    if-eqz v1, :cond_1

    sget v0, Lcom/google/android/gms/f;->r:I

    :goto_0
    sget v2, Lcom/google/android/gms/l;->A:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/achievements/f;->a(III)V

    .line 44
    if-eqz v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->a(II)V

    .line 50
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 51
    new-instance v1, Lcom/google/android/gms/games/ui/common/achievements/g;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/f;->R()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/achievements/g;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->am:Lcom/google/android/gms/games/ui/common/achievements/g;

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->am:Lcom/google/android/gms/games/ui/common/achievements/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 54
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->an:Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 56
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->a(Landroid/support/v7/widget/bv;)V

    .line 57
    return-void

    .line 41
    :cond_1
    sget v0, Lcom/google/android/gms/f;->q:I

    goto :goto_0
.end method

.method public final o_()V
    .locals 5

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 149
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    sget-object v1, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 156
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    goto :goto_0

    .line 153
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_1
.end method
