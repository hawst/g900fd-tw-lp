.class public abstract Lcom/google/android/gms/games/ui/n;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;
.implements Lcom/google/android/gms/common/api/w;
.implements Lcom/google/android/gms/games/multiplayer/g;
.implements Lcom/google/android/gms/games/multiplayer/turnbased/b;
.implements Lcom/google/android/gms/games/quest/d;
.implements Lcom/google/android/gms/games/request/c;
.implements Lcom/google/android/gms/games/ui/a/b;
.implements Lcom/google/android/gms/games/ui/ab;
.implements Lcom/google/android/gms/games/ui/ax;
.implements Lcom/google/android/gms/games/ui/c/a/d;
.implements Lcom/google/android/gms/games/ui/common/players/i;
.implements Lcom/google/android/gms/games/ui/e/j;


# static fields
.field private static N:Landroid/graphics/Bitmap;

.field private static final u:Landroid/content/IntentFilter;


# instance fields
.field private A:Z

.field private B:Landroid/app/Dialog;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Landroid/support/v4/app/Fragment;

.field private I:Lcom/google/android/gms/games/ui/a/d;

.field private J:Lcom/google/android/gms/games/ui/o;

.field private K:Landroid/graphics/drawable/ColorDrawable;

.field private L:Lcom/google/android/gms/games/ui/common/players/h;

.field private M:Ljava/lang/CharSequence;

.field protected n:Z

.field public o:Lcom/google/android/gms/games/ui/z;

.field public p:Z

.field public q:Ljava/lang/String;

.field protected r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected s:I

.field protected t:Z

.field private final v:I

.field private w:I

.field private x:Lcom/google/android/gms/common/api/t;

.field private y:Z

.field private z:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 95
    sput-object v0, Lcom/google/android/gms/games/ui/n;->u:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 6

    .prologue
    .line 185
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/n;-><init>(IIIIZ)V

    .line 187
    return-void
.end method

.method public constructor <init>(IIIIZ)V
    .locals 7

    .prologue
    .line 205
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/n;-><init>(IIIIZZ)V

    .line 207
    return-void
.end method

.method public constructor <init>(IIIIZZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 228
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 166
    const/16 v1, 0xff

    iput v1, p0, Lcom/google/android/gms/games/ui/n;->s:I

    .line 168
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->t:Z

    .line 229
    new-instance v1, Lcom/google/android/gms/games/ui/z;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/ui/z;-><init>(Lcom/google/android/gms/games/ui/n;II)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    .line 230
    iput-boolean p5, p0, Lcom/google/android/gms/games/ui/n;->y:Z

    .line 232
    if-gtz p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->R()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->k()I

    move-result v1

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 235
    iput p3, p0, Lcom/google/android/gms/games/ui/n;->v:I

    .line 236
    iput p4, p0, Lcom/google/android/gms/games/ui/n;->w:I

    .line 237
    iput-boolean p6, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    .line 238
    return-void
.end method

.method private K()V
    .locals 1

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/ar;

    if-eqz v0, :cond_1

    .line 1116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/ar;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ar;->n_()V

    .line 1121
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->u()V

    .line 1122
    return-void

    .line 1117
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/ar;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1119
    check-cast v0, Lcom/google/android/gms/games/ui/ar;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ar;->n_()V

    goto :goto_0
.end method

.method private N()V
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/at;

    if-eqz v0, :cond_1

    .line 1130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/at;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/at;->e()V

    .line 1135
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->u()V

    .line 1136
    return-void

    .line 1131
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/at;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1133
    check-cast v0, Lcom/google/android/gms/games/ui/at;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/at;->e()V

    goto :goto_0
.end method

.method private O()V
    .locals 4

    .prologue
    .line 1240
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1241
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/d;->w:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1242
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1243
    iget v2, p0, Lcom/google/android/gms/games/ui/n;->s:I

    const/16 v3, 0xff

    if-eq v2, v3, :cond_1

    .line 1245
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/d;->t:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1246
    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1251
    :cond_0
    :goto_0
    return-void

    .line 1248
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/n;)Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f()V

    .line 1227
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 1228
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 1229
    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->M:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 1230
    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->M:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 1232
    :cond_1
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/gms/games/ui/n;->s:I

    .line 1233
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->O()V

    .line 1234
    return-void
.end method

.method public final B()I
    .locals 4

    .prologue
    .line 1279
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1280
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/google/android/gms/c;->a:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1281
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    return v0
.end method

.method public final C()Lcom/google/android/gms/games/ui/common/players/h;
    .locals 1

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->L:Lcom/google/android/gms/games/ui/common/players/h;

    return-object v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/j;

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/e/j;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/j;->D()Z

    move-result v0

    .line 1469
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 1291
    const/4 v0, 0x0

    return v0
.end method

.method public S()Z
    .locals 1

    .prologue
    .line 1296
    const/4 v0, 0x0

    return v0
.end method

.method public T()F
    .locals 1

    .prologue
    .line 1323
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method public U()Z
    .locals 1

    .prologue
    .line 1328
    const/4 v0, 0x1

    return v0
.end method

.method public final V()I
    .locals 1

    .prologue
    .line 1343
    const/4 v0, 0x0

    return v0
.end method

.method public W()I
    .locals 1

    .prologue
    .line 1348
    const/4 v0, 0x2

    return v0
.end method

.method public X()I
    .locals 1

    .prologue
    .line 1354
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Y()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 1365
    return-object p0
.end method

.method public Z()I
    .locals 1

    .prologue
    .line 1360
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 1385
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->W()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1301
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->k()I

    move-result v0

    .line 1302
    if-eqz v0, :cond_0

    .line 1303
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->k()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 1305
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getWrappableContentResId() can\'t return 0 when hasPlayHeader() returns true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 520
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->v()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->w()V

    .line 514
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->x()V

    .line 515
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->y()V

    .line 517
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 488
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->a(Landroid/support/v4/app/Fragment;)V

    .line 489
    instance-of v0, p1, Landroid/support/v4/app/x;

    if-nez v0, :cond_0

    .line 493
    iput-object p1, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    .line 495
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/games/ui/c/b;

    if-eqz v0, :cond_1

    .line 497
    check-cast p1, Lcom/google/android/gms/games/ui/c/b;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/games/ui/c/b;->a(Landroid/content/Context;)V

    .line 499
    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/gms/common/a;)V
    .locals 4

    .prologue
    .line 524
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->c()I

    move-result v0

    .line 525
    const-string v1, "GamesFragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection to service apk failed with error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 529
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    .line 530
    const/16 v0, 0x385

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/a;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 532
    :catch_0
    move-exception v0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->finish()V

    goto :goto_0

    .line 537
    :cond_1
    invoke-static {v0, p0}, Lcom/google/android/gms/common/h;->a(ILandroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->B:Landroid/app/Dialog;

    .line 539
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->B:Landroid/app/Dialog;

    if-nez v0, :cond_2

    .line 540
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->finish()V

    goto :goto_0

    .line 544
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->B:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/common/api/u;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 659
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :goto_0
    return-void

    .line 662
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 693
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :goto_0
    return-void

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/w;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1183
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->i()I

    move-result v0

    .line 1184
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1185
    sget v0, Lcom/google/android/gms/l;->Q:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1192
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->N()V

    .line 1193
    return-void

    .line 1187
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1188
    sget v0, Lcom/google/android/gms/l;->T:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 1381
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 776
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 777
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 779
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    .line 783
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 785
    sget v0, Lcom/google/android/gms/l;->B:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 788
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/ui/n;->N:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 789
    sget v1, Lcom/google/android/gms/f;->F:I

    .line 790
    sget-object v0, Lcom/google/android/gms/games/ui/l;->e:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 791
    sget v0, Lcom/google/android/gms/f;->E:I

    .line 793
    :goto_0
    invoke-static {v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->N:Landroid/graphics/Bitmap;

    .line 797
    :cond_2
    new-instance v0, Landroid/app/ActivityManager$TaskDescription;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/games/ui/n;->N:Landroid/graphics/Bitmap;

    sget v4, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v1, v3, v2}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 799
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 801
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 300
    const-string v0, "GPG_shareGame"

    if-nez p1, :cond_0

    const-string v0, "UiUtils"

    const-string v1, "shareGame: null game name"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :goto_0
    return-void

    .line 300
    :cond_0
    if-nez p2, :cond_1

    const-string v0, "UiUtils"

    const-string v1, "shareGame: null game package name"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/common/internal/s;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pcampaignid"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "UiUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "shareGame: couldn\'t get shareGame for game: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/gms/l;->bB:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v0, Lcom/google/android/gms/l;->bC:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public aa()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1396
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/ax;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public final ab()V
    .locals 2

    .prologue
    .line 1406
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Z)V

    .line 1407
    return-void
.end method

.method public final ac()V
    .locals 0

    .prologue
    .line 1411
    return-void
.end method

.method public final ad()Z
    .locals 1

    .prologue
    .line 1415
    const/4 v0, 0x0

    return v0
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 1420
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->t:Z

    return v0
.end method

.method public final ag()F
    .locals 1

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 1449
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v0

    .line 1451
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 1391
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 831
    iget v0, p0, Lcom/google/android/gms/games/ui/n;->w:I

    if-ne v0, p1, :cond_0

    .line 836
    :goto_0
    return-void

    .line 834
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/n;->w:I

    .line 835
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->s_()V

    goto :goto_0
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1313
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 676
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :goto_0
    return-void

    .line 679
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 732
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :goto_0
    return-void

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/w;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/android/gms/games/ui/n;->M:Ljava/lang/CharSequence;

    .line 808
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 809
    if-eqz v0, :cond_0

    .line 810
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 812
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 998
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 815
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/n;->y:Z

    .line 816
    return-void
.end method

.method public final b_(Z)V
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/j;

    if-eqz v0, :cond_0

    .line 1476
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/e/j;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/e/j;->b_(Z)V

    .line 1479
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 4

    .prologue
    const/16 v3, 0xe

    .line 1258
    iput p1, p0, Lcom/google/android/gms/games/ui/n;->s:I

    .line 1259
    shl-int/lit8 v0, p1, 0x18

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->z()I

    move-result v1

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 1261
    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->K:Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_0

    invoke-static {v3}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1262
    :cond_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/n;->K:Landroid/graphics/drawable/ColorDrawable;

    .line 1265
    :cond_1
    invoke-static {v3}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1266
    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->K:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1269
    :cond_2
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->K:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1270
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->O()V

    .line 1271
    return-void
.end method

.method public c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1318
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1007
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1197
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->N()V

    .line 1198
    return-void
.end method

.method protected abstract h()Lcom/google/android/gms/common/api/t;
.end method

.method public final h_()V
    .locals 2

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1155
    sget v0, Lcom/google/android/gms/l;->S:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1158
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->K()V

    .line 1159
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method public final i_()V
    .locals 0

    .prologue
    .line 1163
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->K()V

    .line 1164
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->finish()V

    .line 312
    return-void
.end method

.method public final j_()V
    .locals 2

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169
    sget v0, Lcom/google/android/gms/l;->R:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1172
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->K()V

    .line 1173
    return-void
.end method

.method protected k()I
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public final k_()V
    .locals 0

    .prologue
    .line 1177
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/n;->K()V

    .line 1178
    return-void
.end method

.method protected final l()V
    .locals 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->h()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 614
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to instantiate GoogleApiClient; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->finish()V

    .line 619
    :cond_0
    return-void
.end method

.method public final l_()V
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/as;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/as;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/as;->d()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->u()V

    .line 1203
    return-void

    .line 1202
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/as;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/games/ui/as;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/as;->d()V

    goto :goto_0
.end method

.method public final m()Lcom/google/android/gms/common/api/t;
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-nez v0, :cond_0

    .line 639
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient instance not created yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 713
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 564
    sparse-switch p1, :sswitch_data_0

    .line 603
    :cond_0
    :goto_0
    const-string v0, "GamesFragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: unhandled request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 605
    :goto_1
    :sswitch_0
    return-void

    .line 566
    :sswitch_1
    if-ne p2, v0, :cond_1

    .line 567
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    goto :goto_1

    .line 569
    :cond_1
    const/16 v0, 0x2712

    if-ne p2, v0, :cond_2

    .line 572
    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE resulted in SIGN_IN_FAILED"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    .line 574
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->j()V

    goto :goto_1

    .line 579
    :cond_2
    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE failed, bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->finish()V

    goto :goto_1

    .line 589
    :sswitch_2
    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/z;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->c(Ljava/lang/String;)V

    .line 592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    goto :goto_0

    .line 594
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 564
    nop

    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x7d0 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 316
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    new-instance v0, Lcom/google/android/gms/games/ui/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/au;-><init>(Lcom/google/android/gms/games/ui/ax;)V

    .line 323
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/au;->a(Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 324
    iget-object v2, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/au;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->A()V

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->k()I

    move-result v2

    .line 330
    iget v0, p0, Lcom/google/android/gms/games/ui/n;->v:I

    if-eqz v0, :cond_4

    .line 332
    iget v0, p0, Lcom/google/android/gms/games/ui/n;->v:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->setContentView(I)V

    .line 333
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 338
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->R()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 342
    iget-object v1, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 355
    :cond_1
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/a/d;-><init>(Lcom/google/android/gms/games/ui/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->I:Lcom/google/android/gms/games/ui/a/d;

    .line 356
    new-instance v0, Lcom/google/android/gms/games/ui/e/i;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/games/ui/e/i;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/j;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->L:Lcom/google/android/gms/games/ui/common/players/h;

    .line 358
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->l()V

    .line 360
    if-eqz p1, :cond_2

    .line 361
    const-string v0, "savedStateResolutionInProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    .line 363
    const-string v0, "savedStateWaitingForInstall"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    .line 366
    :cond_2
    return-void

    .line 343
    :cond_3
    if-eqz v2, :cond_1

    .line 344
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 346
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->R()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->setContentView(Landroid/view/View;)V

    goto :goto_0

    .line 348
    :cond_5
    if-eqz v2, :cond_6

    .line 349
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/n;->setContentView(I)V

    goto :goto_0

    .line 351
    :cond_6
    const-string v0, "We need to either have a layout res id, play header, or a wrappable content res id to ensure we have a content view."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 851
    iget v0, p0, Lcom/google/android/gms/games/ui/n;->w:I

    if-nez v0, :cond_0

    .line 852
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 907
    :goto_0
    return v0

    .line 861
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 863
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 864
    iget v2, p0, Lcom/google/android/gms/games/ui/n;->w:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 874
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 876
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->y:Z

    if-eqz v0, :cond_2

    .line 880
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 881
    goto :goto_0

    .line 886
    :cond_1
    sget v0, Lcom/google/android/gms/g;->aW:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 887
    const-string v0, "You need an item menu_progress_bar in your menu if you are enabling the ProgressBar in the ActionBar"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 896
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/gms/i;->a:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 898
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 901
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 902
    sget v0, Lcom/google/android/gms/g;->bF:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    .line 903
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->A:Z

    const/4 v4, 0x4

    new-array v5, v1, [Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    aput-object v6, v5, v7

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 904
    invoke-static {v2, v3}, Landroid/support/v4/view/ad;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    :cond_2
    move v0, v1

    .line 907
    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 425
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 427
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 392
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 394
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "GamesFragmentActivity"

    const-string v1, "onResume with a resolutionIntentInProgress. This should never have happened ..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/z;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 406
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    if-eqz v1, :cond_1

    .line 407
    if-eqz v0, :cond_3

    .line 408
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->c(Ljava/lang/String;)V

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    .line 416
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    if-nez v0, :cond_2

    .line 417
    new-instance v0, Lcom/google/android/gms/games/ui/o;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/ui/o;-><init>(Lcom/google/android/gms/games/ui/n;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    sget-object v1, Lcom/google/android/gms/games/ui/n;->u:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/n;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 421
    :cond_2
    return-void

    .line 412
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 503
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 505
    const-string v0, "savedStateResolutionInProgress"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 506
    const-string v0, "savedStateWaitingForInstall"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 507
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 384
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 385
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->n:Z

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->b()V

    .line 388
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 431
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    .line 434
    invoke-interface {v2}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 435
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->D:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    if-eqz v0, :cond_1

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 437
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;)V

    .line 445
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->E:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    if-eqz v0, :cond_3

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 447
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/t;)V

    .line 455
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->F:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    if-eqz v0, :cond_5

    .line 456
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 457
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;)V

    .line 464
    :cond_5
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->G:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->C:Z

    if-eqz v0, :cond_7

    .line 465
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 466
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;)V

    .line 474
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->x:Lcom/google/android/gms/common/api/t;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->c()V

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->B:Landroid/app/Dialog;

    if-eqz v0, :cond_8

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->B:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 480
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    if-eqz v0, :cond_9

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 482
    iput-object v1, p0, Lcom/google/android/gms/games/ui/n;->J:Lcom/google/android/gms/games/ui/o;

    .line 484
    :cond_9
    return-void

    .line 439
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 441
    :goto_4
    sget-object v3, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 449
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 451
    :goto_5
    sget-object v3, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 459
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    move-object v0, v1

    .line 461
    :goto_6
    sget-object v3, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    goto :goto_2

    .line 459
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 468
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_11

    move-object v0, v1

    .line 470
    :goto_7
    sget-object v3, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V

    goto :goto_3

    .line 468
    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method public final p()Lcom/google/android/gms/games/ui/z;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    return-object v0
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 912
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->y:Z

    const-string v1, "This method can only be called if we have a progressbar in the actionbar"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 914
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->A:Z

    .line 915
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 918
    :cond_0
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 922
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->y:Z

    const-string v1, "This method can only be called if we have a progressbar in the actionbar"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 924
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->A:Z

    .line 925
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 928
    :cond_0
    return-void
.end method

.method public final s()Lcom/google/android/gms/games/ui/a/a;
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->I:Lcom/google/android/gms/games/ui/a/d;

    return-object v0
.end method

.method public final s_()V
    .locals 1

    .prologue
    .line 841
    invoke-super {p0}, Landroid/support/v7/app/d;->s_()V

    .line 842
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->z:Landroid/widget/ProgressBar;

    .line 843
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 756
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/n;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/n;->setTitle(Ljava/lang/CharSequence;)V

    .line 757
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 748
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->setTitle(Ljava/lang/CharSequence;)V

    .line 749
    invoke-super {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_0

    .line 751
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 753
    :cond_0
    return-void
.end method

.method public final t()Lcom/google/android/gms/games/ui/c/a/c;
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->I:Lcom/google/android/gms/games/ui/a/d;

    return-object v0
.end method

.method public u()V
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v0, :cond_1

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->H:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1022
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v0, :cond_0

    .line 1024
    check-cast p0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {p0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    goto :goto_0
.end method

.method public final v()V
    .locals 3

    .prologue
    .line 1035
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 1036
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1037
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->D:Z

    .line 1038
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1039
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/turnbased/b;)V

    .line 1047
    :cond_0
    :goto_0
    return-void

    .line 1041
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1043
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/turnbased/b;Ljava/lang/String;)V

    goto :goto_0

    .line 1041
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final w()V
    .locals 3

    .prologue
    .line 1056
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 1057
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1058
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->E:Z

    .line 1059
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1060
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/g;)V

    .line 1068
    :cond_0
    :goto_0
    return-void

    .line 1062
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1064
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/g;Ljava/lang/String;)V

    goto :goto_0

    .line 1062
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 1077
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 1078
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1079
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->F:Z

    .line 1080
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1081
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/request/c;)V

    .line 1088
    :cond_0
    :goto_0
    return-void

    .line 1083
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1085
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/request/c;Ljava/lang/String;)V

    goto :goto_0

    .line 1083
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 1097
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 1098
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->G:Z

    .line 1100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1101
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/quest/d;)V

    .line 1108
    :cond_0
    :goto_0
    return-void

    .line 1103
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1105
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/quest/d;Ljava/lang/String;)V

    goto :goto_0

    .line 1103
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/games/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected z()I
    .locals 2

    .prologue
    .line 1213
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/d;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method
