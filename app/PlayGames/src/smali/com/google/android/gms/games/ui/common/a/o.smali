.class public final Lcom/google/android/gms/games/ui/common/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;JJZ)Ljava/lang/String;
    .locals 7

    .prologue
    .line 25
    sub-long v0, p1, p3

    .line 26
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 27
    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v1

    .line 29
    if-eqz p5, :cond_0

    sget v0, Lcom/google/android/gms/l;->bV:I

    .line 31
    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 35
    :goto_1
    return-object v0

    .line 29
    :cond_0
    sget v0, Lcom/google/android/gms/l;->bR:I

    goto :goto_0

    .line 33
    :cond_1
    if-eqz p5, :cond_2

    sget v0, Lcom/google/android/gms/l;->bW:I

    .line 35
    :goto_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 33
    :cond_2
    sget v0, Lcom/google/android/gms/l;->bS:I

    goto :goto_2
.end method
