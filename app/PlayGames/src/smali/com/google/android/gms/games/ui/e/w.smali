.class public final Lcom/google/android/gms/games/ui/e/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/e/v;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/e/v;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/w;->a:Lcom/google/android/gms/games/ui/e/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 226
    check-cast p1, Lcom/google/android/gms/people/e;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/w;->a:Lcom/google/android/gms/games/ui/e/v;

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->D_()Lcom/google/android/gms/people/model/c;

    move-result-object v2

    iget-boolean v3, v1, Lcom/google/android/gms/games/ui/e/v;->g:Z

    if-eqz v3, :cond_0

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onCirclesLoaded: Canceled! Ignoring this callback..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v2, "ManageCirclesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCirclesLoaded: error status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/e/v;->b()V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/games/ui/e/v;->j:Lcom/google/android/gms/games/ui/e/y;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/gms/games/ui/e/v;->j:Lcom/google/android/gms/games/ui/e/y;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/y;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ManageCirclesHelper"

    const-string v2, "onCirclesLoaded: processing halted at client\'s request"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/e/v;->b()V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/c;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/b;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/people/model/b;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v4, v1, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/c;->f_()V

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/c;->f_()V

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/gms/games/ui/e/v;->c:Z

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/e/v;->a()V

    goto :goto_0
.end method
