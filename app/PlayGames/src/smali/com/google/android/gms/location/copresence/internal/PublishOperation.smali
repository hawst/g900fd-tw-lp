.class public final Lcom/google/android/gms/location/copresence/internal/PublishOperation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

.field private final d:Lcom/google/android/gms/location/copresence/Message;

.field private final e:Lcom/google/android/gms/location/copresence/AccessPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/h;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/Message;Lcom/google/android/gms/location/copresence/AccessPolicy;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->a:I

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->b:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->c:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    .line 56
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->d:Lcom/google/android/gms/location/copresence/Message;

    .line 57
    iput-object p5, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e:Lcom/google/android/gms/location/copresence/AccessPolicy;

    .line 58
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->a:I

    return v0
.end method

.method final b()Lcom/google/android/gms/location/copresence/internal/StrategyImpl;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->c:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/location/copresence/Message;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->d:Lcom/google/android/gms/location/copresence/Message;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/location/copresence/AccessPolicy;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e:Lcom/google/android/gms/location/copresence/AccessPolicy;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PublishOperation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->d:Lcom/google/android/gms/location/copresence/Message;

    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/Message;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 108
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/h;->a(Lcom/google/android/gms/location/copresence/internal/PublishOperation;Landroid/os/Parcel;I)V

    .line 109
    return-void
.end method
