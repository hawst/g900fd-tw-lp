.class public final Lcom/google/android/gms/games/ui/common/a/b;
.super Lcom/google/android/gms/games/ui/common/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/l;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/a/j;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    .line 32
    return-void
.end method

.method private c(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 36
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    const-string v0, "DestQuestHelper"

    const-string v1, "launchGameForQuest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "quest"

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v1}, Lcom/google/android/gms/games/internal/az;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 52
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    const-string v0, "DestQuestHelper"

    const-string v1, "switchAccountForQuest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 64
    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v3

    .line 65
    invoke-static {v1, v3, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/a/b;->c(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 73
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const-string v0, "DestQuestHelper"

    const-string v1, "onPlayQuestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 85
    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v1, v3}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    if-nez v1, :cond_1

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/a/b;->a(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 98
    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/ui/c/a/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/quest/Quest;)Lcom/google/android/gms/games/ui/c/a/k;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/b;->a:Lcom/google/android/gms/games/ui/n;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/a/b;->c(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0
.end method
