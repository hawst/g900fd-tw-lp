.class public final Lcom/google/android/gms/games/ui/az;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private g:I

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/gms/i;->j:I

    sput v0, Lcom/google/android/gms/games/ui/az;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 27
    iput p2, p0, Lcom/google/android/gms/games/ui/az;->g:I

    .line 28
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/az;->h:Z

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/az;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/az;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/az;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/az;->g:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/az;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/games/ui/az;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/google/android/gms/games/ui/ba;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/ba;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/games/ui/az;->e:I

    return v0
.end method
