.class public final Lcom/google/android/gms/games/internal/d/c;
.super Lcom/google/android/gms/common/data/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/internal/d/a;


# instance fields
.field private final c:Lcom/google/android/gms/games/GameRef;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/g;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 27
    const-string v0, "external_game_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/d/c;->c:Lcom/google/android/gms/games/GameRef;

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/GameRef;

    iget-object v1, p0, Lcom/google/android/gms/games/internal/d/c;->a_:Lcom/google/android/gms/common/data/DataHolder;

    iget v2, p0, Lcom/google/android/gms/games/internal/d/c;->b_:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/GameRef;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/d/c;->c:Lcom/google/android/gms/games/GameRef;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/internal/d/c;->c:Lcom/google/android/gms/games/GameRef;

    return-object v0
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "display_title"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/internal/d/c;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    .line 52
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "display_title"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 61
    const-string v0, "display_description"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/internal/d/c;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    .line 62
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "display_description"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 71
    const-string v0, "icon_uri"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->i(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 76
    const-string v0, "created_timestamp"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 81
    const-string v0, "xp_earned"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 91
    const-string v0, "type"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 96
    const-string v0, "newLevel"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/d/c;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
