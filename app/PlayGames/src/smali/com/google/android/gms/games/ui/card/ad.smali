.class final Lcom/google/android/gms/games/ui/card/ad;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/view/View;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/view/View;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/view/View;

.field private final t:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 110
    sget v0, Lcom/google/android/gms/g;->C:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->m:Landroid/view/View;

    .line 111
    sget v0, Lcom/google/android/gms/g;->az:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->n:Landroid/widget/ImageView;

    .line 112
    sget v0, Lcom/google/android/gms/g;->cs:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->o:Landroid/widget/TextView;

    .line 113
    sget v0, Lcom/google/android/gms/g;->F:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->p:Landroid/widget/TextView;

    .line 115
    sget v0, Lcom/google/android/gms/g;->aM:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->q:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    sget v0, Lcom/google/android/gms/g;->aL:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->r:Landroid/widget/TextView;

    .line 119
    sget v0, Lcom/google/android/gms/g;->aq:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->s:Landroid/view/View;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->s:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    sget v0, Lcom/google/android/gms/g;->ap:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->t:Landroid/widget/TextView;

    .line 122
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 126
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/card/aa;

    .line 129
    invoke-static {v0}, Lcom/google/android/gms/games/ui/card/aa;->a(Lcom/google/android/gms/games/ui/card/aa;)Lcom/google/android/gms/games/ui/card/ab;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->m:Landroid/view/View;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->a:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->n:Landroid/widget/ImageView;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->b:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    iget v1, v0, Lcom/google/android/gms/games/ui/card/ab;->c:I

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->o:Landroid/widget/TextView;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->c:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 139
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->p:Landroid/widget/TextView;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->d:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget v1, v0, Lcom/google/android/gms/games/ui/card/ab;->e:I

    if-nez v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->q:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->t:Landroid/widget/TextView;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->f:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->t:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/ad;->t:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, v0, Lcom/google/android/gms/games/ui/card/ab;->f:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    return-void

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->q:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->r:Landroid/widget/TextView;

    iget v2, v0, Lcom/google/android/gms/games/ui/card/ab;->e:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/ad;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/card/aa;

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->q:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 156
    invoke-static {v0}, Lcom/google/android/gms/games/ui/card/aa;->c(Lcom/google/android/gms/games/ui/card/aa;)Lcom/google/android/gms/games/ui/card/ac;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/card/aa;->b(Lcom/google/android/gms/games/ui/card/aa;)Ljava/lang/String;

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/ad;->s:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 158
    invoke-static {v0}, Lcom/google/android/gms/games/ui/card/aa;->c(Lcom/google/android/gms/games/ui/card/aa;)Lcom/google/android/gms/games/ui/card/ac;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/card/aa;->b(Lcom/google/android/gms/games/ui/card/aa;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/card/ac;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
