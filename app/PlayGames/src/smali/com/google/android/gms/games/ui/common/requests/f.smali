.class final Lcom/google/android/gms/games/ui/common/requests/f;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 113
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/d;

    .line 177
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->b(Lcom/google/android/gms/games/ui/common/requests/d;)Lcom/google/android/gms/games/ui/common/requests/e;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/games/request/GameRequest;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/ui/common/requests/e;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 178
    return-void
.end method

.method public final B()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/d;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/request/GameRequest;

    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->p()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/q;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/q;->b(Ljava/lang/Object;)V

    .line 186
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->b(Lcom/google/android/gms/games/ui/common/requests/d;)Lcom/google/android/gms/games/ui/common/requests/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/e;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 187
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 109
    check-cast p3, Lcom/google/android/gms/games/request/GameRequest;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/d;

    sget v1, Lcom/google/android/gms/l;->bo:I

    invoke-interface {p3}, Lcom/google/android/gms/games/request/GameRequest;->i()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    sget v1, Lcom/google/android/gms/l;->bp:I

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/requests/f;->e(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/common/requests/f;->e(Z)V

    invoke-interface {p3}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Lcom/google/android/gms/games/ui/common/requests/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/common/requests/f;->c(Z)V

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->g:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/common/requests/f;->a(Landroid/net/Uri;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->u()Landroid/database/CharArrayBuffer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/Player;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->b(Landroid/database/CharArrayBuffer;)V

    :goto_0
    sget v0, Lcom/google/android/gms/l;->bm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->n(I)V

    sget v0, Lcom/google/android/gms/l;->bn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->o(I)V

    sget v0, Lcom/google/android/gms/l;->br:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->p(I)V

    sget v0, Lcom/google/android/gms/l;->bs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->q(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->b(Z)V

    sget v0, Lcom/google/android/gms/j;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/f;->m(I)V

    return-void

    :cond_1
    invoke-interface {p3}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->f:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/games/ui/common/requests/f;->a(Landroid/net/Uri;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->u()Landroid/database/CharArrayBuffer;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/requests/f;->b(Landroid/database/CharArrayBuffer;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/gms/f;->g:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/requests/f;->a(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/d;

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/request/GameRequest;

    .line 194
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 195
    sget v4, Lcom/google/android/gms/g;->aU:I

    if-ne v2, v4, :cond_0

    .line 196
    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 197
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->b(Lcom/google/android/gms/games/ui/common/requests/d;)Lcom/google/android/gms/games/ui/common/requests/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/e;->a(Lcom/google/android/gms/games/Game;)V

    move v0, v3

    .line 204
    :goto_0
    return v0

    .line 199
    :cond_0
    sget v4, Lcom/google/android/gms/g;->aT:I

    if-ne v2, v4, :cond_1

    .line 200
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/f;->p()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/q;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/q;->b(Ljava/lang/Object;)V

    .line 201
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->b(Lcom/google/android/gms/games/ui/common/requests/d;)Lcom/google/android/gms/games/ui/common/requests/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/e;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    move v0, v3

    .line 202
    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
