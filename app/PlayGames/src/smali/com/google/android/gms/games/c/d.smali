.class public final Lcom/google/android/gms/games/c/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 5047
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5048
    iput v0, p0, Lcom/google/android/gms/games/c/d;->a:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    iput v0, p0, Lcom/google/android/gms/games/c/d;->c:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/d;->G:I

    .line 5049
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 5116
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 5117
    iget v1, p0, Lcom/google/android/gms/games/c/d;->a:I

    if-eqz v1, :cond_0

    .line 5118
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/d;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5121
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 5122
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5125
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/d;->c:I

    if-eqz v1, :cond_2

    .line 5126
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/d;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5129
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 5130
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5133
    :cond_3
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 4994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/d;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/d;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/d;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/d;->d:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 5099
    iget v0, p0, Lcom/google/android/gms/games/c/d;->a:I

    if-eqz v0, :cond_0

    .line 5100
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/d;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5102
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/c/d;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 5103
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 5105
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/d;->c:I

    if-eqz v0, :cond_2

    .line 5106
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5108
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/games/c/d;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 5109
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 5111
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 5112
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5062
    if-ne p1, p0, :cond_1

    .line 5081
    :cond_0
    :goto_0
    return v0

    .line 5065
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 5066
    goto :goto_0

    .line 5068
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/d;

    .line 5069
    iget v2, p0, Lcom/google/android/gms/games/c/d;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/d;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5070
    goto :goto_0

    .line 5072
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/d;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 5073
    goto :goto_0

    .line 5075
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/c/d;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/d;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 5076
    goto :goto_0

    .line 5078
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/d;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 5079
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 5086
    iget v0, p0, Lcom/google/android/gms/games/c/d;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5088
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/d;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 5090
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/d;->c:I

    add-int/2addr v0, v1

    .line 5091
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/d;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/d;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 5093
    return v0
.end method
