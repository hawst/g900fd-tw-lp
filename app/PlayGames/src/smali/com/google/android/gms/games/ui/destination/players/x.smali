.class public final Lcom/google/android/gms/games/ui/destination/players/x;
.super Lcom/google/android/gms/games/ui/destination/players/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/destination/games/ak;
.implements Lcom/google/android/gms/games/ui/destination/games/b;
.implements Lcom/google/android/gms/games/ui/destination/players/k;
.implements Lcom/google/android/gms/games/ui/e/h;


# instance fields
.field private aA:Lcom/google/android/gms/games/ui/destination/games/aj;

.field private aB:Ljava/util/ArrayList;

.field private aC:Ljava/util/ArrayList;

.field private aD:[Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

.field private aw:Lcom/google/android/gms/games/ui/destination/players/j;

.field private ax:Lcom/google/android/gms/games/ui/ac;

.field private ay:Lcom/google/android/gms/games/ui/destination/games/a;

.field private az:Lcom/google/android/gms/games/ui/ac;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/m;-><init>(Z)V

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->h:Lcom/google/android/gms/games/ui/e/o;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/x;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aB:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;ILcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 72
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    if-nez v0, :cond_0

    if-nez v4, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p4, :cond_3

    invoke-interface {p4}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v3}, Lcom/google/android/gms/games/internal/game/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    :goto_1
    new-instance v5, Lcom/google/android/gms/common/data/p;

    invoke-direct {v5}, Lcom/google/android/gms/common/data/p;-><init>()V

    sub-int v0, v4, v1

    if-gtz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    :goto_2
    if-ge v3, p3, :cond_5

    add-int v0, v3, v1

    if-ge v0, v4, :cond_5

    add-int v0, v3, v1

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/internal/game/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ay:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    add-int v0, p3, v1

    if-le v4, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "moreGamesInCommon"

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/w;Lcom/google/android/gms/games/w;)V
    .locals 10

    .prologue
    .line 72
    invoke-interface {p1}, Lcom/google/android/gms/games/w;->b()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    new-instance v9, Ljava/util/TreeSet;

    invoke-direct {v9}, Ljava/util/TreeSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {p1, v6}, Lcom/google/android/gms/games/w;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {p2, v6}, Lcom/google/android/gms/games/w;->a(Ljava/lang/String;)J

    move-result-wide v4

    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;-><init>(JJLjava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0019

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/x;->an:Lcom/google/android/gms/games/Player;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->a(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;Ljava/util/ArrayList;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->c(Z)V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/x;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aD:[Ljava/lang/String;

    return-object p1
.end method

.method private at()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 507
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    .line 515
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aD:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aB:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aD:[Ljava/lang/String;

    array-length v4, v0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 517
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aD:[Ljava/lang/String;

    aget-object v5, v0, v3

    .line 519
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_2

    .line 520
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aB:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 521
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 522
    iget-object v7, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 516
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 528
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    if-eqz v0, :cond_4

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/u;->a(Ljava/util/ArrayList;)V

    .line 533
    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->h:Lcom/google/android/gms/games/ui/e/o;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;ILcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 72
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    if-nez v1, :cond_0

    if-nez v4, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p4, :cond_3

    move v1, v2

    :goto_1
    new-instance v5, Lcom/google/android/gms/common/data/p;

    invoke-direct {v5}, Lcom/google/android/gms/common/data/p;-><init>()V

    sub-int v3, v4, v1

    if-gtz v3, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    move v3, v0

    :goto_2
    if-ge v3, p3, :cond_5

    add-int v0, v3, v1

    if-ge v0, v4, :cond_5

    add-int v0, v3, v1

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/internal/game/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aA:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/destination/games/aj;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    add-int v0, p3, v1

    if-le v4, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "moreOtherPlayerGames"

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/destination/players/j;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aw:Lcom/google/android/gms/games/ui/destination/players/j;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/x;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aB:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/players/x;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->at()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->h:Lcom/google/android/gms/games/ui/e/o;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/players/m;->a(IILandroid/content/Intent;)V

    .line 141
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 143
    invoke-static {p3}, Lcom/google/android/gms/common/a/a/b;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/a/a/c;

    move-result-object v2

    .line 145
    invoke-interface {v2}, Lcom/google/android/gms/common/a/a/c;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Lcom/google/android/gms/common/a/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 147
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v4, ""

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aq:Ljava/lang/String;

    const/4 v6, 0x4

    invoke-static {v3, v4, v5, v6, v0}, Lcom/google/android/gms/games/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 150
    invoke-interface {v2}, Lcom/google/android/gms/common/a/a/c;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 151
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 152
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    :goto_1
    if-ge v1, v3, :cond_2

    .line 154
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 145
    goto :goto_0

    .line 156
    :cond_2
    new-array v0, v3, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aD:[Ljava/lang/String;

    .line 157
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->at()V

    .line 159
    :cond_3
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 591
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->as()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 593
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 594
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 1

    .prologue
    .line 209
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 210
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 2

    .prologue
    .line 577
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Lcom/google/android/gms/games/internal/game/ExtendedGame;Lcom/google/android/gms/games/Player;)V

    .line 580
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/internal/game/ExtendedGame;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V

    .line 587
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 641
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 608
    invoke-static {p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 609
    instance-of v2, v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v2, :cond_0

    .line 610
    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 611
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 612
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    .line 613
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 622
    :goto_0
    return v0

    .line 615
    :pswitch_0
    const-string v1, "GPG_overflowMenu"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 622
    goto :goto_0

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0289
        :pswitch_0
    .end packed-switch
.end method

.method public final a_(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 0

    .prologue
    .line 604
    return-void
.end method

.method protected final av()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 163
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->av()V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->j()Landroid/content/res/Resources;

    move-result-object v0

    .line 167
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->c(Z)V

    .line 170
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/j;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/players/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/k;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aw:Lcom/google/android/gms/games/ui/destination/players/j;

    .line 173
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    const v2, 0x7f0f0066

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Ljava/lang/String;)V

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 178
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0, p0, v3}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;Lcom/google/android/gms/games/ui/destination/games/b;B)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ay:Lcom/google/android/gms/games/ui/destination/games/a;

    .line 183
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    const v2, 0x7f0f0068

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 188
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v2, 0x2

    const v3, 0x7f0d0018

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/gms/games/ui/destination/games/aj;-><init>(Lcom/google/android/gms/games/ui/n;IILcom/google/android/gms/games/ui/destination/games/ak;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aA:Lcom/google/android/gms/games/ui/destination/games/aj;

    .line 192
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 193
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->as:Lcom/google/android/gms/games/ui/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 198
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aw:Lcom/google/android/gms/games/ui/destination/players/j;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ax:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ay:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->az:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aA:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 204
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Landroid/support/v7/widget/bv;)V

    .line 205
    return-void
.end method

.method public final aw()V
    .locals 4

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aq:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aC:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 540
    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 542
    return-void
.end method

.method public final ax()V
    .locals 3

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/s;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 547
    :goto_0
    return-void

    .line 546
    :catch_0
    move-exception v0

    const-string v1, "UiUtils"

    const-string v2, "Unable to launch show profile intent"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 12

    .prologue
    const v8, 0x3f99999a    # 1.2f

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    :goto_0
    return-void

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->j()Landroid/content/res/Resources;

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->an:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/google/android/gms/common/api/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/api/p;-><init>(Lcom/google/android/gms/common/api/t;)V

    .line 227
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    move-result-object v2

    if-nez v2, :cond_2

    .line 229
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    invoke-interface {v2, p1, v3}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v2

    .line 236
    :goto_1
    sget-object v3, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v3, p1, v0}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v3

    .line 241
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    invoke-interface {v0, p1, v4}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v4

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/h;->m:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    sget v6, Lcom/google/android/gms/h;->g:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/2addr v0, v5

    int-to-float v0, v0

    mul-float/2addr v0, v8

    float-to-int v9, v0

    .line 249
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    add-int/lit8 v6, v9, 0x2

    invoke-interface {v0, p1, v5, v6}, Lcom/google/android/gms/games/i;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v5

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lcom/google/android/gms/h;->l:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    sget v7, Lcom/google/android/gms/h;->h:I

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/2addr v0, v6

    int-to-float v0, v0

    mul-float/2addr v0, v8

    float-to-int v10, v0

    .line 255
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    add-int/lit8 v7, v10, 0x2

    invoke-interface {v0, p1, v6, v7}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v6

    .line 260
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    .line 261
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 262
    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/people/internal/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/h;->a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;

    .line 266
    sget-object v7, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aq:Ljava/lang/String;

    invoke-interface {v7, p1, v8, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v7

    .line 271
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    .line 272
    invoke-virtual {v0}, Lcom/google/android/gms/people/d;->e()Lcom/google/android/gms/people/d;

    .line 275
    sget-object v8, Lcom/google/android/gms/people/p;->e:Lcom/google/android/gms/people/c;

    iget-object v11, p0, Lcom/google/android/gms/games/ui/destination/players/x;->aq:Ljava/lang/String;

    invoke-interface {v8, p1, v11, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v8

    .line 280
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v11

    .line 281
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/y;

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/destination/players/y;-><init>(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;II)V

    invoke-virtual {v11, v0}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/an;)V

    goto/16 :goto_0

    .line 232
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/destination/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 556
    if-eqz p1, :cond_0

    .line 557
    const-string v0, "moreGamesInCommon"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 560
    const-string v0, "com.google.android.gms.games.TAB"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 562
    const-string v2, "com.google.android.gms.games.PLAYER"

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Landroid/content/Intent;)V

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    const-string v0, "moreOtherPlayerGames"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567
    const-string v0, "com.google.android.gms.games.TAB"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 569
    const-string v2, "com.google.android.gms.games.PLAYER"

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 570
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/m;->d(Landroid/os/Bundle;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 122
    const-string v0, "ProfileCompareFrag"

    const-string v1, "We don\'t have another player to compare to, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->finish()V

    .line 135
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/b;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->au:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/x;->as:Lcom/google/android/gms/games/ui/ai;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ai;->c(Z)V

    goto :goto_0
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/x;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 628
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 630
    const-string v0, "ProfileCompareFrag"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :goto_0
    return-void

    .line 633
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/x;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 634
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/x;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
