.class final Lcom/google/android/gms/common/api/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/g;

.field final synthetic b:Lcom/google/android/gms/common/api/x;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/g;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iput-object p2, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 428
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/g;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/g;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget v1, v1, Lcom/google/android/gms/common/api/x;->e:I

    if-ge v0, v1, :cond_1

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iput-object p1, v0, Lcom/google/android/gms/common/api/x;->d:Lcom/google/android/gms/common/a;

    .line 432
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/g;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/g;->a()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/common/api/x;->e:I

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    invoke-static {v0}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/x;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 438
    return-void

    .line 437
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->b:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
