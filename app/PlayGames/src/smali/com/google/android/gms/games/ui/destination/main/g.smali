.class final Lcom/google/android/gms/games/ui/destination/main/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/main/d;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;)V
    .locals 0

    .prologue
    .line 1236
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;B)V
    .locals 0

    .prologue
    .line 1236
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/main/g;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1236
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/main/d;->au()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FeaturedGamesLoadedListener... statusCode = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", data = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->s(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->t(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v5

    if-nez v5, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->u(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->v(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/main/a;->b()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->i(Lcom/google/android/gms/games/ui/destination/main/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    goto :goto_0

    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->u(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->v(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/main/a;->c()I

    move-result v6

    if-gt v5, v6, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->v(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/a;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/destination/main/a;->a(Lcom/google/android/gms/common/data/b;)V

    move v1, v0

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/google/android/gms/games/ui/destination/main/j;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-direct {v2, v3, v4, v6}, Lcom/google/android/gms/games/ui/destination/main/j;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;Lcom/google/android/gms/common/data/b;I)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/b;->a()I

    move-result v3

    if-ge v3, v6, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->w(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v2

    const-string v3, "shuffleHomePageSeed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v2, v3, v8, v9}, Lcom/google/android/gms/games/ui/destination/b/a;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v2, Lcom/google/android/gms/games/ui/destination/c/a;

    invoke-direct {v2, v4, v6, v8, v9}, Lcom/google/android/gms/games/ui/destination/c/a;-><init>(Lcom/google/android/gms/common/data/b;IJ)V

    move-object v3, v2

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->u(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/ac;

    move-result-object v7

    if-le v5, v6, :cond_4

    move v2, v1

    :goto_3
    invoke-virtual {v7, v2}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/main/g;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/main/d;->v(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/main/a;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/destination/main/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_3

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_5
    throw v0

    :cond_6
    move-object v3, v2

    goto :goto_2
.end method
