.class public final Lcom/google/android/gms/people/internal/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Map;

.field public static b:Ljava/lang/Iterable;

.field public static final c:Landroid/os/Handler;

.field public static final d:[Ljava/lang/String;

.field public static final e:Ljava/util/regex/Pattern;

.field public static final f:Ljava/util/regex/Pattern;

.field public static final g:Ljava/util/regex/Pattern;

.field public static final h:Ljava/util/regex/Pattern;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:Ljava/security/SecureRandom;

.field private static final l:Ljava/lang/ThreadLocal;

.field private static final m:Ljava/lang/ThreadLocal;

.field private static final n:Ljava/lang/ThreadLocal;

.field private static final o:Ljava/lang/ThreadLocal;

.field private static final p:Ljava/lang/ThreadLocal;

.field private static final q:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/people/internal/z;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/z;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->a:Ljava/util/Map;

    .line 70
    new-instance v0, Lcom/google/android/gms/people/internal/c;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->b:Ljava/lang/Iterable;

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->c:Landroid/os/Handler;

    .line 81
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/people/internal/y;->d:[Ljava/lang/String;

    .line 89
    const-string v0, "\\,"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/y;->e:Ljava/util/regex/Pattern;

    .line 92
    const-string v0, "[\u2028\u2029 \u00a0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\t\u000b\u000c\u001c\u001d\u001e\u001f\n\r]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/y;->f:Ljava/util/regex/Pattern;

    .line 112
    const-string v0, "\u0001"

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/y;->g:Ljava/util/regex/Pattern;

    .line 114
    const-string v0, "\u0002"

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/y;->h:Ljava/util/regex/Pattern;

    .line 117
    const-string v0, "\u0001"

    sput-object v0, Lcom/google/android/gms/people/internal/y;->i:Ljava/lang/String;

    .line 120
    const-string v0, "\u0002"

    sput-object v0, Lcom/google/android/gms/people/internal/y;->j:Ljava/lang/String;

    .line 128
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->k:Ljava/security/SecureRandom;

    .line 542
    new-instance v0, Lcom/google/android/gms/people/internal/aa;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->l:Ljava/lang/ThreadLocal;

    .line 556
    new-instance v0, Lcom/google/android/gms/people/internal/ab;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/ab;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->m:Ljava/lang/ThreadLocal;

    .line 573
    new-instance v0, Lcom/google/android/gms/people/internal/ac;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/ac;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->n:Ljava/lang/ThreadLocal;

    .line 591
    new-instance v0, Lcom/google/android/gms/people/internal/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->o:Ljava/lang/ThreadLocal;

    .line 610
    new-instance v0, Lcom/google/android/gms/people/internal/ae;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->p:Ljava/lang/ThreadLocal;

    .line 631
    new-instance v0, Lcom/google/android/gms/people/internal/af;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/af;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/internal/y;->q:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 242
    invoke-static {p0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 266
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 267
    const-string v0, "g:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "e:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Expecting qualified-id, not gaia-id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 271
    return-void

    .line 267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
