.class public Lcom/google/android/gms/search/queries/QueryCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/queries/h;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:Lcom/google/android/gms/appdatasearch/SearchResults;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/google/android/gms/search/queries/h;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/QueryCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/QueryCall$Response;->c:I

    .line 135
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;Lcom/google/android/gms/appdatasearch/SearchResults;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput p1, p0, Lcom/google/android/gms/search/queries/QueryCall$Response;->c:I

    .line 152
    iput-object p2, p0, Lcom/google/android/gms/search/queries/QueryCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 153
    iput-object p3, p0, Lcom/google/android/gms/search/queries/QueryCall$Response;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    .line 154
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/search/queries/QueryCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/h;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$Response;->CREATOR:Lcom/google/android/gms/search/queries/h;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/queries/h;->a(Lcom/google/android/gms/search/queries/QueryCall$Response;Landroid/os/Parcel;I)V

    .line 166
    return-void
.end method
