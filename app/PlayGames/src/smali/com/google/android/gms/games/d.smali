.class public final Lcom/google/android/gms/games/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/common/api/h;

.field public static final b:Lcom/google/android/gms/common/api/Scope;

.field public static final c:Lcom/google/android/gms/common/api/a;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/common/api/a;

.field public static final f:Lcom/google/android/gms/games/i;

.field public static final g:Lcom/google/android/gms/games/achievement/c;

.field public static final h:Lcom/google/android/gms/games/appcontent/m;

.field public static final i:Lcom/google/android/gms/games/event/b;

.field public static final j:Lcom/google/android/gms/games/a/m;

.field public static final k:Lcom/google/android/gms/games/multiplayer/d;

.field public static final l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

.field public static final m:Lcom/google/android/gms/games/multiplayer/realtime/b;

.field public static final n:Lcom/google/android/gms/games/multiplayer/f;

.field public static final o:Lcom/google/android/gms/games/t;

.field public static final p:Lcom/google/android/gms/games/l;

.field public static final q:Lcom/google/android/gms/games/quest/e;

.field public static final r:Lcom/google/android/gms/games/request/d;

.field public static final s:Lcom/google/android/gms/games/snapshot/f;

.field public static final t:Lcom/google/android/gms/games/internal/game/a;

.field private static final u:Lcom/google/android/gms/common/api/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 313
    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/h;

    .line 320
    new-instance v0, Lcom/google/android/gms/games/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/g;

    .line 349
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/d;->b:Lcom/google/android/gms/common/api/Scope;

    .line 357
    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/h;

    new-array v3, v6, [Lcom/google/android/gms/common/api/Scope;

    sget-object v4, Lcom/google/android/gms/games/d;->b:Lcom/google/android/gms/common/api/Scope;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/d;->c:Lcom/google/android/gms/common/api/a;

    .line 366
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/d;->d:Lcom/google/android/gms/common/api/Scope;

    .line 377
    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/h;

    new-array v3, v6, [Lcom/google/android/gms/common/api/Scope;

    sget-object v4, Lcom/google/android/gms/games/d;->d:Lcom/google/android/gms/common/api/Scope;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/d;->e:Lcom/google/android/gms/common/api/a;

    .line 383
    new-instance v0, Lcom/google/android/gms/games/internal/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    .line 388
    new-instance v0, Lcom/google/android/gms/games/internal/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    .line 395
    new-instance v0, Lcom/google/android/gms/games/internal/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->h:Lcom/google/android/gms/games/appcontent/m;

    .line 400
    new-instance v0, Lcom/google/android/gms/games/internal/a/h;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->i:Lcom/google/android/gms/games/event/b;

    .line 405
    new-instance v0, Lcom/google/android/gms/games/internal/a/ad;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/a/m;

    .line 410
    new-instance v0, Lcom/google/android/gms/games/internal/a/y;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/y;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    .line 415
    new-instance v0, Lcom/google/android/gms/games/internal/a/ch;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ch;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    .line 420
    new-instance v0, Lcom/google/android/gms/games/internal/a/bx;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/bx;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    .line 427
    new-instance v0, Lcom/google/android/gms/games/internal/a/an;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/an;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->n:Lcom/google/android/gms/games/multiplayer/f;

    .line 432
    new-instance v0, Lcom/google/android/gms/games/internal/a/aw;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/aw;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    .line 437
    new-instance v0, Lcom/google/android/gms/games/internal/a/ao;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    .line 442
    new-instance v0, Lcom/google/android/gms/games/internal/a/bs;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/bs;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    .line 447
    new-instance v0, Lcom/google/android/gms/games/internal/a/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/by;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    .line 452
    new-instance v0, Lcom/google/android/gms/games/internal/a/cg;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/cg;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->s:Lcom/google/android/gms/games/snapshot/f;

    .line 459
    new-instance v0, Lcom/google/android/gms/games/internal/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->t:Lcom/google/android/gms/games/internal/game/a;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 468
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "GoogleApiClient parameter is required."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 470
    invoke-interface {p0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    const-string v3, "GoogleApiClient must be connected."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    .line 472
    sget-object v0, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/c;

    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/ag;->a(ZLjava/lang/Object;)V

    return-object v0

    :cond_0
    move v0, v2

    .line 468
    goto :goto_0

    :cond_1
    move v1, v2

    .line 472
    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 692
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/internal/c;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 715
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    return-void
.end method

.method public static b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 567
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
