.class public final Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/as;


# static fields
.field private static final am:[I


# instance fields
.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Lcom/google/android/gms/games/ui/destination/quests/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->am:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/t;Z)V
    .locals 7

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 65
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->an:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->ao:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->am:[I

    const/4 v5, 0x0

    move-object v1, p1

    move v6, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[IIZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 69
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/gms/games/quest/f;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->c()Lcom/google/android/gms/games/quest/b;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->r()V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->ap:Lcom/google/android/gms/games/ui/destination/quests/a;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/destination/quests/a;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->y()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->a(Lcom/google/android/gms/common/api/t;Z)V

    .line 61
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->a(Lcom/google/android/gms/common/api/t;Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->F:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    goto :goto_0

    :cond_2
    const-string v0, "CompletedQuestFrag"

    const-string v1, "No valid listener to update the inbox counts"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f0200fa

    const v1, 0x7f0f0050

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->a(III)V

    .line 47
    new-instance v0, Lcom/google/android/gms/games/ui/destination/quests/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/destination/quests/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->ap:Lcom/google/android/gms/games/ui/destination/quests/a;

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->ap:Lcom/google/android/gms/games/ui/destination/quests/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 49
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->e(Landroid/os/Bundle;)V

    .line 54
    const-string v0, "savedStateGameId"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->an:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "savedStatePlayerId"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->ao:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->b(Z)V

    .line 110
    return-void
.end method
