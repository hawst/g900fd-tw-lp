.class public Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/o;


# instance fields
.field final a:I

.field final b:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field public final c:I

.field final d:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field private final transient i:Ljava/util/Map;

.field private final transient j:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/appdatasearch/o;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/o;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/CorpusId;I[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;IIILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a:I

    .line 98
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b:[Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 99
    iput p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->c:I

    .line 100
    iput p5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->e:I

    .line 101
    iput p6, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    .line 102
    iput p7, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->g:I

    .line 103
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->h:Ljava/lang/String;

    .line 105
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->d:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    .line 106
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_4

    .line 107
    :cond_0
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    .line 122
    :cond_1
    if-eqz p4, :cond_2

    array-length v0, p4

    if-nez v0, :cond_7

    .line 123
    :cond_2
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    .line 131
    :cond_3
    return-void

    .line 109
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    move v1, v2

    .line 110
    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 112
    if-nez v0, :cond_5

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 114
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    aget-object v4, p2, v1

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_5
    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 117
    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125
    :cond_7
    new-instance v0, Ljava/util/HashMap;

    array-length v1, p4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    .line 127
    :goto_1
    array-length v0, p4

    if-ge v2, v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    aget-object v1, p4, v2

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    aget-object v3, p4, v2

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/o;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/o;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/o;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Landroid/os/Parcel;I)V

    .line 196
    return-void
.end method
