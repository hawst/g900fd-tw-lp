.class final Lcom/google/android/gms/games/internal/ag;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/ag;)V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 775
    iput-object p1, p0, Lcom/google/android/gms/games/internal/ag;->a:Lcom/google/android/gms/common/api/ag;

    .line 776
    return-void
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ag;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v1, Lcom/google/android/gms/games/internal/af;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/af;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 800
    return-void
.end method

.method public final r(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 781
    new-instance v1, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 782
    const/4 v0, 0x0

    .line 784
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 785
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 788
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->f_()V

    .line 792
    if-eqz v0, :cond_1

    .line 793
    iget-object v1, p0, Lcom/google/android/gms/games/internal/ag;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v2, Lcom/google/android/gms/games/internal/ah;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/ah;-><init>(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 795
    :cond_1
    return-void

    .line 788
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->f_()V

    throw v0
.end method
