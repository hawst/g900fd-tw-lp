.class public final Lcom/google/android/gms/games/ui/common/a/f;
.super Lcom/google/android/gms/games/ui/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Lcom/google/android/gms/games/ui/common/a/c;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/g;-><init>(Landroid/view/View;)V

    .line 55
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/a/c;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/f;->m:Lcom/google/android/gms/games/ui/common/a/c;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/f;->m:Lcom/google/android/gms/games/ui/common/a/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/a/c;->a(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 48
    check-cast p3, Lcom/google/android/gms/games/quest/Quest;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/g;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/e;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/f;->m:Lcom/google/android/gms/games/ui/common/a/c;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/f;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/e;->a(Lcom/google/android/gms/games/ui/common/a/e;)Lcom/google/android/gms/games/ui/n;

    move-result-object v0

    invoke-virtual {v1, v2, p0, p3, v0}, Lcom/google/android/gms/games/ui/common/a/c;->a(Landroid/content/Context;Lcom/google/android/gms/games/ui/ag;Lcom/google/android/gms/games/quest/Quest;Lcom/google/android/gms/games/ui/n;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/e;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/quest/Quest;

    .line 72
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/e;->b(Lcom/google/android/gms/games/ui/common/a/e;)Lcom/google/android/gms/games/ui/common/a/g;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/a/g;->b(Lcom/google/android/gms/games/quest/Quest;)V

    .line 73
    return-void
.end method
