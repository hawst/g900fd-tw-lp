.class public final Lcom/google/android/gms/games/ui/destination/games/ai;
.super Lcom/google/android/gms/games/ui/destination/games/af;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/af;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 218
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/af;->a(Lcom/google/android/gms/games/j;)V

    return-void
.end method

.method protected final at()Lcom/google/android/gms/games/ui/e;
    .locals 2

    .prologue
    .line 223
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;)V

    return-object v0
.end method

.method protected final au()I
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x3

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 228
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aa;->a(Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 231
    return-void
.end method

.method public final b_(I)V
    .locals 3

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ai;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 241
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    const-string v0, "GameListMyGamesFragment"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/i;->b(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
