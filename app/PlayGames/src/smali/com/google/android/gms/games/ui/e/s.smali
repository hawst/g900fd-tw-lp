.class public final Lcom/google/android/gms/games/ui/e/s;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/gms/games/ui/e/u;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/gms/games/ui/e/u;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 86
    iput v2, p0, Lcom/google/android/gms/games/ui/e/s;->a:I

    .line 137
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 138
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Must be called from UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 144
    const v0, 0x7f0c016c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 147
    const v0, 0x7f0c016a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/ui/e/s;->a(Landroid/view/View;I)V

    .line 151
    const v0, 0x7f0c0119

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/ui/e/s;->a(Landroid/view/View;I)V

    .line 155
    const v0, 0x7f0c0164

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/ui/e/s;->a(Landroid/view/View;I)V

    .line 159
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/e/u;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->f:Lcom/google/android/gms/games/ui/e/u;

    .line 160
    sget v0, Lcom/google/android/gms/g;->M:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 161
    if-eqz v0, :cond_6

    :goto_5
    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 162
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    sget v0, Lcom/google/android/gms/g;->I:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/e/s;->a(I)V

    .line 171
    return-void

    :cond_1
    move v0, v2

    .line 138
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 145
    goto :goto_1

    :cond_3
    move v0, v2

    .line 148
    goto :goto_2

    :cond_4
    move v0, v2

    .line 152
    goto :goto_3

    :cond_5
    move v0, v2

    .line 156
    goto :goto_4

    :cond_6
    move v1, v2

    .line 161
    goto :goto_5
.end method

.method private static a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 275
    if-eqz p1, :cond_0

    .line 276
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 277
    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v1, :cond_1

    .line 278
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 279
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 280
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    instance-of v1, v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_2

    .line 282
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 283
    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 284
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 285
    :cond_2
    instance-of v1, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_0

    .line 286
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 287
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 288
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v4, 0x8

    .line 190
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must be called from UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 193
    iput p1, p0, Lcom/google/android/gms/games/ui/e/s;->a:I

    .line 199
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/e/s;->removeMessages(I)V

    .line 201
    packed-switch p1, :pswitch_data_0

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid state!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 190
    goto :goto_0

    .line 203
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 247
    :goto_1
    return-void

    .line 210
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 215
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/s;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 218
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 224
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->aZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->ba:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->an:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 233
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->aZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->ba:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->an:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 243
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 333
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 352
    const-string v0, "LoadingDataViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage: unexpected code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :goto_0
    return-void

    .line 340
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/an;->a(Landroid/view/View;)V

    goto :goto_0

    .line 340
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 333
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/g;->M:I

    if-ne v0, v1, :cond_1

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->f:Lcom/google/android/gms/games/ui/e/u;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/u;->d()V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/g;->I:I

    if-ne v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->f:Lcom/google/android/gms/games/ui/e/u;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/t;

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/s;->f:Lcom/google/android/gms/games/ui/e/u;

    goto :goto_0

    .line 369
    :cond_2
    const-string v0, "LoadingDataViewManager"

    const-string v1, "onClick: empty_action_message text clicked without an ActionTextListener!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
