.class public Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/people/identity/models/PersonRef;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/aj;


# instance fields
.field final a:I

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/aj;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/aj;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/aj;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;->a:I

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;->b:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;->c:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;->d:Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;

    .line 46
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 86
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/people/identity/internal/models/aj;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonRefImpl;Landroid/os/Parcel;I)V

    .line 87
    return-void
.end method
