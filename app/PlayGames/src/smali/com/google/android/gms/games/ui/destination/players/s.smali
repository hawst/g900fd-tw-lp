.class public final Lcom/google/android/gms/games/ui/destination/players/s;
.super Lcom/google/android/gms/games/ui/destination/players/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/destination/main/o;
.implements Lcom/google/android/gms/games/ui/destination/players/ak;
.implements Lcom/google/android/gms/games/ui/destination/players/ar;
.implements Lcom/google/android/gms/games/ui/e/j;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private aA:Lcom/google/android/gms/games/ui/destination/players/aj;

.field private aB:Z

.field private au:Lcom/google/android/gms/games/ui/destination/main/m;

.field private av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

.field private aw:Lcom/google/android/gms/games/ui/ac;

.field private ax:Lcom/google/android/gms/games/ui/destination/games/a;

.field private ay:Lcom/google/android/gms/games/ui/ac;

.field private az:Lcom/google/android/gms/games/ui/destination/players/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/m;-><init>(Z)V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/e/o;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->h:Lcom/google/android/gms/games/ui/e/o;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/s;Lcom/google/android/gms/games/w;)V
    .locals 8

    .prologue
    .line 63
    invoke-interface {p1}, Lcom/google/android/gms/games/w;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    new-instance v4, Ljava/util/TreeSet;

    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/gms/games/w;->a(Ljava/lang/String;)J

    move-result-wide v6

    new-instance v5, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    invoke-direct {v5, v6, v7, v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;-><init>(JLjava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/players/u;->c()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/gms/games/ui/destination/players/s;->at:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->a(Lcom/google/android/gms/games/Player;Ljava/util/ArrayList;ILcom/google/android/gms/games/ui/widget/MetagameAvatarView;Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->c(Z)V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/s;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aB:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/s;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aB:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/destination/main/m;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->au:Lcom/google/android/gms/games/ui/destination/main/m;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/s;)Lcom/google/android/gms/games/ui/e/o;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->h:Lcom/google/android/gms/games/ui/e/o;

    return-object v0
.end method


# virtual methods
.method public final D()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aB:Z

    return v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Lcom/google/android/gms/games/x;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Lcom/google/android/gms/games/x;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 153
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/d/a;)V
    .locals 3

    .prologue
    .line 423
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/d/a;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_0

    .line 425
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/16 v2, 0xa

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;I)V

    .line 427
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 325
    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v3

    .line 326
    invoke-virtual {p2}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    .line 328
    new-instance v5, Lcom/google/android/gms/common/data/p;

    invoke-direct {v5}, Lcom/google/android/gms/common/data/p;-><init>()V

    move v1, v2

    .line 329
    :goto_0
    if-ge v1, v4, :cond_0

    if-ge v1, p3, :cond_0

    .line 330
    invoke-virtual {p2, v1}, Lcom/google/android/gms/games/internal/game/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/data/p;->a(Ljava/lang/Object;)V

    .line 329
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333
    :cond_0
    if-nez v3, :cond_2

    if-lez v4, :cond_2

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 335
    if-le v4, p3, :cond_1

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "moreRecentGames"

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ax:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/common/data/b;)V

    .line 344
    return-void

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ax:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Z)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/u;)V
    .locals 3

    .prologue
    .line 248
    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    .line 249
    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    .line 254
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->a()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 255
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/o;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->au()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    .line 260
    return-void

    .line 259
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/x;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 363
    invoke-interface {p1}, Lcom/google/android/gms/games/x;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    .line 364
    invoke-interface {p1}, Lcom/google/android/gms/games/x;->b()Lcom/google/android/gms/games/internal/d/b;

    move-result-object v3

    .line 370
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/gms/games/ui/destination/players/aq;->a(Z)V

    .line 372
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    .line 400
    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/d/b;->f_()V

    :goto_0
    return-void

    .line 375
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    .line 400
    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 380
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/players/aq;->k()V

    .line 383
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/destination/players/aq;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 385
    :try_start_2
    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/d/b;->a()I

    move-result v1

    if-nez v1, :cond_4

    .line 386
    if-eqz v0, :cond_3

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/aj;->c(Z)V

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 401
    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/d/b;->f_()V

    :cond_2
    throw v0

    .line 391
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/aj;->c(Z)V

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    goto :goto_0

    .line 396
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/aj;->c(Z)V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 401
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final at()V
    .locals 3

    .prologue
    .line 269
    new-instance v0, Lcom/google/android/gms/games/ui/c/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/e;-><init>()V

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-string v2, "com.google.android.gms.games.ui.dialog.privateLevelDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    .line 272
    return-void
.end method

.method protected final av()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/players/m;->av()V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->j()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/google/android/gms/games/ui/destination/main/m;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/main/m;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/destination/main/o;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->au:Lcom/google/android/gms/games/ui/destination/main/m;

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->au:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/destination/main/m;->c(Z)V

    .line 107
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->c(Z)V

    .line 110
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    const v2, 0x7f0f00af

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 115
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/games/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/c;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ax:Lcom/google/android/gms/games/ui/destination/games/a;

    .line 118
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    const v2, 0x7f0f0072

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 122
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/aq;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/destination/players/aq;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/ar;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/players/aq;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/destination/players/aq;->a(Z)V

    .line 131
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/aj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/destination/players/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/players/ak;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    .line 134
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 135
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ar:Lcom/google/android/gms/games/ui/destination/players/u;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->as:Lcom/google/android/gms/games/ui/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->au:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->av:Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aw:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ax:Lcom/google/android/gms/games/ui/destination/games/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ay:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 145
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 147
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Landroid/support/v7/widget/bv;)V

    .line 148
    return-void
.end method

.method public final ay()V
    .locals 0

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->o_()V

    .line 432
    return-void
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 9

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->j()Landroid/content/res/Resources;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    .line 161
    new-instance v8, Lcom/google/android/gms/common/api/p;

    invoke-direct {v8, p1}, Lcom/google/android/gms/common/api/p;-><init>(Lcom/google/android/gms/common/api/t;)V

    .line 164
    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v2, p1}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v3

    .line 169
    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v2, p1, v1}, Lcom/google/android/gms/games/t;->c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v4

    .line 174
    const v2, 0x7f0d0031

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 176
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    add-int/lit8 v2, v7, 0x1

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/i;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v5

    .line 181
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->b(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/t;->f(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v6

    .line 189
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/aj;)Lcom/google/android/gms/common/api/r;

    move-result-object v2

    .line 194
    invoke-virtual {v8}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v8

    .line 195
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/t;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/ui/destination/players/t;-><init>(Lcom/google/android/gms/games/ui/destination/players/s;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;I)V

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/an;)V

    .line 245
    return-void
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 349
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 350
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 352
    const-string v0, "MyProfileFragment"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :goto_0
    return-void

    .line 356
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->b(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->g(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final b_(Z)V
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->au:Lcom/google/android/gms/games/ui/destination/main/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/destination/main/m;->a(Z)V

    goto :goto_0
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 408
    if-eqz p1, :cond_0

    .line 409
    const-string v0, "moreRecentGames"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->am:Lcom/google/android/gms/games/ui/destination/b;

    const-class v2, Lcom/google/android/gms/games/ui/destination/games/MyGamesListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 411
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 413
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->a(Landroid/content/Intent;)V

    .line 419
    :goto_0
    return-void

    .line 418
    :cond_0
    const-string v0, "MyProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onHeaderClicked: unexpected tag \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/m;->d(Landroid/os/Bundle;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->as:Lcom/google/android/gms/games/ui/ai;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ai;->c(Z)V

    .line 95
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/s;->ao:Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/s;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 438
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 440
    const-string v0, "MyProfileFragment"

    const-string v1, "onRetryClick: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :goto_0
    return-void

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->aA:Lcom/google/android/gms/games/ui/destination/players/aj;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/destination/players/aj;->c(Z)V

    .line 444
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/s;->az:Lcom/google/android/gms/games/ui/destination/players/aq;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/players/aq;->i()V

    .line 445
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/s;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0

    .line 447
    :cond_1
    const-string v0, "MyProfileFragment"

    const-string v1, "onRetryClick: can\'t load recently played games, player is null."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
