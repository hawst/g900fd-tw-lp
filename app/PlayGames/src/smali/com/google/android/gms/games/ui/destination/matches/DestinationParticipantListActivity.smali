.class public Lcom/google/android/gms/games/ui/destination/matches/DestinationParticipantListActivity;
.super Lcom/google/android/gms/games/ui/common/matches/w;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/w;-><init>(I)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/multiplayer/Participant;)V
    .locals 3

    .prologue
    .line 27
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    const-string v0, "DestPartListActvity"

    const-string v1, "participant.getPlayer() returned null. Ignoring click"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/matches/DestinationParticipantListActivity;->u:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;)V

    goto :goto_0

    .line 36
    :cond_1
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;)V

    goto :goto_0
.end method
