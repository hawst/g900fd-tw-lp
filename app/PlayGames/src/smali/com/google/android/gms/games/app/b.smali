.class public final Lcom/google/android/gms/games/app/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/ArrayList;

.field private static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/gms/games/ui/l;->c:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/app/b;->b:Landroid/net/Uri;

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/app/b;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(I)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 432
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 433
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 434
    const v1, 0x3fa66666    # 1.3f

    aget v2, v0, v3

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 435
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/app/Activity;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 442
    .line 444
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.play.games"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :goto_0
    return v0

    .line 447
    :catch_0
    move-exception v1

    const-string v1, "PowerUpUtils"

    const-string v2, "isNewerVersion: failed to get package name when getting versionCode."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static varargs a(Landroid/app/Activity;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 78
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    move-result-object v0

    .line 79
    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Ljava/lang/String;I[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public static varargs a(Landroid/app/Activity;Lcom/google/android/gms/games/internal/game/ExtendedGame;I[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 133
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;Lcom/google/android/gms/games/Game;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    array-length v0, p3

    if-lez v0, :cond_0

    invoke-static {p0, p3}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static varargs a(Landroid/app/Activity;Ljava/lang/String;I[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 169
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v1, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 172
    const-string v1, "com.google.android.gms.games.TAB"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 175
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    array-length v1, p3

    if-lez v1, :cond_0

    .line 176
    invoke-static {p0, p3}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 229
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;I)V

    .line 91
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Game;I)V
    .locals 3

    .prologue
    .line 97
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    const-string v2, "com.google.android.gms.games.GAME"

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 99
    const-string v2, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;Lcom/google/android/gms/games/Game;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 101
    if-lez p2, :cond_0

    .line 102
    const-string v0, "com.google.android.gms.games.TAB"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 104
    :cond_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 105
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/internal/game/ExtendedGame;Lcom/google/android/gms/games/Player;)V
    .locals 3

    .prologue
    .line 219
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/games/ui/destination/players/AchievementComparisonListActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    const-string v2, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 221
    const-string v2, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 222
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 223
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 124
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 125
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    if-lez p2, :cond_0

    .line 191
    const-string v1, "com.google.android.gms.games.TAB"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 195
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 196
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/ui/n;)V
    .locals 4

    .prologue
    .line 267
    instance-of v0, p0, Lcom/google/android/gms/games/ui/destination/q;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 268
    check-cast v0, Lcom/google/android/gms/games/ui/destination/q;

    .line 269
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/destination/q;->w_()Ljava/lang/String;

    move-result-object v0

    .line 275
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 276
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 278
    new-instance v2, Landroid/accounts/Account;

    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.google"

    invoke-direct {v2, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-static {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/app/b;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    .line 292
    :goto_1
    new-instance v1, Lcom/google/android/gms/googlehelp/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/b;-><init>(Landroid/app/Activity;)V

    .line 293
    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/b;->a(Landroid/content/Intent;)V

    .line 294
    return-void

    .line 271
    :cond_0
    const-string v0, "mobile_games_default"

    goto :goto_0

    .line 286
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/app/b;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 237
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    .line 238
    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.gms.games.ui.extras.tab"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 241
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/DestinationPlayerSearchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    aput-object v1, v0, v4

    .line 244
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/support/v4/a/a;->a(Landroid/content/Context;[Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v1

    .line 247
    if-nez v1, :cond_0

    .line 248
    aget-object v0, v0, v4

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 250
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/games/Game;)V
    .locals 3

    .prologue
    .line 204
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    const-string v2, "com.google.android.gms.games.GAME"

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 206
    const-string v0, "com.google.android.gms.games.TAB"

    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    const-string v2, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/app/b;->c(Landroid/content/Context;Lcom/google/android/gms/games/Game;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 209
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 210
    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/games/Game;)I
    .locals 6

    .prologue
    .line 400
    sget-object v0, Lcom/google/android/gms/games/ui/l;->n:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->z()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, -0xffba95

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x10

    invoke-static {v2, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    const-wide/16 v0, 0x456b

    :cond_1
    const-wide/32 v2, -0x1000000

    const-wide/32 v4, 0xffffff

    and-long/2addr v0, v4

    or-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 256
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/DestinationGameSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 257
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method public static d(Landroid/content/Context;)Ljava/util/List;
    .locals 2

    .prologue
    .line 367
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.play.games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 369
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 370
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 371
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/games/app/b;->a:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
