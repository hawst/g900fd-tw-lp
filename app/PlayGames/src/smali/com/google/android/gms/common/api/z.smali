.class final Lcom/google/android/gms/common/api/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/x;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/x;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 285
    packed-switch p1, :pswitch_data_0

    .line 319
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 320
    :goto_1
    return-void

    .line 288
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/x;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/x;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 319
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 294
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-boolean v0, v0, Lcom/google/android/gms/common/api/x;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 298
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/common/api/x;->g:Z

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    new-instance v1, Lcom/google/android/gms/common/api/ac;

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/ac;-><init>(Lcom/google/android/gms/common/api/x;)V

    iput-object v1, v0, Lcom/google/android/gms/common/api/x;->k:Landroid/content/BroadcastReceiver;

    .line 301
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 303
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v2, v2, Lcom/google/android/gms/common/api/x;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-wide v2, v2, Lcom/google/android/gms/common/api/x;->h:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->j:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-wide v2, v2, Lcom/google/android/gms/common/api/x;->i:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/x;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 285
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget v0, v0, Lcom/google/android/gms/common/api/x;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 271
    if-eqz p1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->l:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    invoke-static {v0}, Lcom/google/android/gms/common/api/x;->a(Lcom/google/android/gms/common/api/x;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v0, v0, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 278
    return-void

    .line 277
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Lcom/google/android/gms/common/api/x;

    iget-object v1, v1, Lcom/google/android/gms/common/api/x;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
