.class public final Lcom/google/android/gms/games/ui/destination/s;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/destination/b;

.field private b:Ljava/util/ArrayList;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 49
    check-cast p1, Lcom/google/android/gms/games/ui/destination/b;

    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/s;->a:Lcom/google/android/gms/games/ui/destination/b;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/s;)Lcom/google/android/gms/games/ui/destination/b;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->a:Lcom/google/android/gms/games/ui/destination/b;

    return-object v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/google/android/gms/games/ui/destination/s;->c:I

    .line 55
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/destination/s;->d:Z

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/s;->notifyDataSetChanged()V

    .line 57
    return-void
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 101
    const-wide/16 v0, -0x1

    .line 111
    :goto_0
    return-wide v0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/w;

    .line 104
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/x;

    if-eqz v1, :cond_1

    .line 105
    const-class v0, Lcom/google/android/gms/games/ui/destination/x;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 106
    :cond_1
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/v;

    if-eqz v1, :cond_2

    .line 107
    const-class v0, Lcom/google/android/gms/games/ui/destination/v;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 109
    :cond_2
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/u;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 110
    check-cast v0, Lcom/google/android/gms/games/ui/destination/u;

    .line 111
    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 118
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/games/ui/destination/x;

    if-eqz v2, :cond_2

    .line 121
    const/4 v0, 0x2

    goto :goto_0

    .line 122
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/games/ui/destination/v;

    if-eqz v2, :cond_3

    .line 123
    const/4 v0, 0x3

    goto :goto_0

    .line 124
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/games/ui/destination/u;

    if-eqz v2, :cond_0

    .line 125
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/c/b;->b()I

    move-result v2

    if-ne p1, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/s;->a:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v2, v2, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    if-eqz v2, :cond_4

    move v2, v1

    .line 127
    :goto_1
    if-eqz v2, :cond_0

    move v0, v1

    .line 128
    goto :goto_0

    :cond_4
    move v2, v0

    .line 125
    goto :goto_1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->a:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/s;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/w;

    .line 64
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/x;

    if-eqz v1, :cond_1

    .line 65
    invoke-static {p2, v2}, Lcom/google/android/gms/games/ui/destination/x;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object p2

    .line 95
    :cond_0
    :goto_0
    return-object p2

    .line 67
    :cond_1
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/v;

    if-eqz v1, :cond_2

    .line 68
    invoke-static {p2, v2}, Lcom/google/android/gms/games/ui/destination/v;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 70
    :cond_2
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/u;

    if-eqz v1, :cond_0

    .line 71
    check-cast v0, Lcom/google/android/gms/games/ui/destination/u;

    .line 74
    new-instance v1, Lcom/google/android/gms/games/ui/destination/t;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/ui/destination/t;-><init>(Lcom/google/android/gms/games/ui/destination/s;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/u;->a(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-static {}, Lcom/google/android/gms/games/ui/destination/c/b;->b()I

    move-result v1

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/s;->a:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v1, v1, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    if-eqz v1, :cond_3

    const/4 v3, 0x1

    .line 91
    :goto_1
    iget v4, p0, Lcom/google/android/gms/games/ui/destination/s;->c:I

    iget-boolean v5, p0, Lcom/google/android/gms/games/ui/destination/s;->d:Z

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/u;->a(Landroid/view/View;Landroid/view/LayoutInflater;ZIZ)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 88
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x4

    return v0
.end method
