.class public final Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private g:Z

.field private h:Lcom/google/android/gms/games/Player;

.field private i:Ljava/util/ArrayList;

.field private j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 65
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->k:I

    .line 66
    const v1, 0x7f0a009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->l:I

    .line 67
    const v1, 0x7f0a004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->m:I

    .line 68
    const v1, 0x7f0a009f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->n:I

    .line 69
    const v1, 0x7f0a0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->o:I

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->p:Z

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->e:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->k:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->l:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->n:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->o:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->p:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->h:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->j:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->m:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 127
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/ae;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f04008e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->g:Z

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/ae;-><init>(Landroid/view/View;Z)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/Player;Ljava/util/ArrayList;ILcom/google/android/gms/games/ui/widget/MetagameAvatarView;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 110
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 111
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 112
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->h:Lcom/google/android/gms/games/Player;

    .line 113
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->i:Ljava/util/ArrayList;

    .line 114
    iput p3, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->j:I

    .line 115
    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->e:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 116
    iput-boolean p5, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->g:Z

    .line 117
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter;->c(Z)V

    .line 118
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 122
    const v0, 0x7f04008e

    return v0
.end method
