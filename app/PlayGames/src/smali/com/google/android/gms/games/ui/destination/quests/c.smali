.class public final Lcom/google/android/gms/games/ui/destination/quests/c;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/games/ui/n;

.field private final g:Lcom/google/android/gms/games/ui/common/a/m;

.field private final h:Lcom/google/android/gms/games/ui/common/a/g;

.field private final i:Lcom/google/android/gms/games/ui/destination/quests/e;

.field private j:Landroid/support/v4/view/ViewPager;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Lcom/google/android/gms/games/ui/common/a/m;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/quests/c;->c(Z)V

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->e:Lcom/google/android/gms/games/ui/n;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->h:Lcom/google/android/gms/games/ui/common/a/g;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->g:Lcom/google/android/gms/games/ui/common/a/m;

    .line 52
    new-instance v0, Lcom/google/android/gms/games/ui/destination/quests/e;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/destination/quests/e;-><init>(Lcom/google/android/gms/games/ui/destination/quests/c;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/quests/c;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/ViewPager;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/quests/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    .line 158
    if-nez p1, :cond_0

    if-eqz p2, :cond_5

    .line 161
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v5

    move v1, v3

    move v2, v3

    :goto_0
    if-ge v1, v5, :cond_4

    .line 162
    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 175
    iput-object v6, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->k:Ljava/lang/String;

    .line 176
    iput-object v6, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->l:Ljava/lang/String;

    .line 183
    :goto_2
    return-void

    .line 166
    :cond_1
    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 161
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 180
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->k:Ljava/lang/String;

    .line 181
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->l:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/quests/c;)Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/quests/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/quests/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->e:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/common/a/g;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->h:Lcom/google/android/gms/games/ui/common/a/g;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/common/a/m;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->g:Lcom/google/android/gms/games/ui/common/a/m;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/games/ui/destination/quests/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f04004b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/destination/quests/d;-><init>(Lcom/google/android/gms/games/ui/destination/quests/c;Landroid/view/View;Lcom/google/android/gms/games/ui/destination/quests/e;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;Lcom/google/android/gms/games/ui/y;)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    const v1, 0x7f0b00f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 71
    neg-int v0, v2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 72
    neg-int v0, v2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 78
    iget-object v0, p2, Lcom/google/android/gms/games/ui/y;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 79
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/cd;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/am;

    invoke-virtual {v1}, Landroid/support/v7/widget/am;->b()I

    move-result v1

    .line 80
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 81
    div-int v2, v0, v1

    .line 82
    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 83
    iget v1, p1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 84
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/quest/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    :cond_1
    iput-object p1, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/quests/e;->d()V

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ao;)V

    .line 147
    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f04004b

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    .line 187
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    .line 189
    invoke-virtual {v0}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    .line 190
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->i:Lcom/google/android/gms/games/ui/destination/quests/e;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    .line 198
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/c;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 200
    if-ltz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    .line 201
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/String;

    move-result-object v0

    .line 204
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
