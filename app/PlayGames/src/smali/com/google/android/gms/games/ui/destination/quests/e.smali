.class final Lcom/google/android/gms/games/ui/destination/quests/e;
.super Landroid/support/v4/view/ao;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/games/quest/b;

.field b:Lcom/google/android/gms/games/ui/ag;

.field final synthetic c:Lcom/google/android/gms/games/ui/destination/quests/c;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/destination/quests/c;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-direct {p0}, Landroid/support/v4/view/ao;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/destination/quests/c;B)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/quests/e;-><init>(Lcom/google/android/gms/games/ui/destination/quests/c;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const v6, 0x7f0c0215

    const/4 v5, 0x0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->e(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->f(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400a0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 241
    const v0, 0x7f09000a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/quests/e;->c()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    .line 247
    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 249
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 250
    const v3, 0x7f0c0221

    invoke-virtual {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 253
    :cond_0
    const v0, 0x7f09000b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 254
    if-eqz v0, :cond_1

    .line 257
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 259
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 260
    const v3, 0x7f0b015d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 261
    const v3, 0x7f0b015b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 263
    const v0, 0x7f0c022b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 264
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 265
    const v3, 0x7f0b015a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 268
    const v0, 0x7f0c022f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 271
    const v3, 0x7f0b015c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 275
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/ui/destination/quests/f;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->b:Lcom/google/android/gms/games/ui/ag;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/quests/c;->e(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/n;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/quests/c;->g(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/common/a/g;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v5}, Lcom/google/android/gms/games/ui/destination/quests/c;->h(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/common/a/m;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/quests/f;-><init>(Lcom/google/android/gms/games/ui/ag;Landroid/view/View;Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Lcom/google/android/gms/games/ui/common/a/m;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/quest/Quest;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->c:Lcom/google/android/gms/games/ui/destination/quests/c;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/quests/c;->e(Lcom/google/android/gms/games/ui/destination/quests/c;)Lcom/google/android/gms/games/ui/n;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/games/ui/destination/quests/f;->a(Landroid/content/Context;Lcom/google/android/gms/games/quest/Quest;)V

    .line 276
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 277
    return-object v2
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 282
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 283
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 292
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/e;->a:Lcom/google/android/gms/games/quest/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
