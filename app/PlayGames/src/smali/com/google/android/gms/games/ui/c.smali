.class final Lcom/google/android/gms/games/ui/c;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 76
    sget v0, Lcom/google/android/gms/g;->x:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c;->m:Landroid/widget/Button;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/a;

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c;->m:Landroid/widget/Button;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/a;->a(Lcom/google/android/gms/games/ui/a;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 86
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/a;

    .line 94
    invoke-static {v0}, Lcom/google/android/gms/games/ui/a;->c(Lcom/google/android/gms/games/ui/a;)Lcom/google/android/gms/games/ui/b;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/a;->b(Lcom/google/android/gms/games/ui/a;)Ljava/lang/Object;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/b;->a()V

    .line 95
    return-void
.end method
