.class public final Lcom/google/android/gms/games/ui/destination/players/ai;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/common/players/b;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/common/players/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/h;-><init>()V

    return-void
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v1, "circled"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->c(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/t;->d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 136
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 30
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/ai;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/common/players/a;->k()V

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/o;->f_()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/ai;->b(Lcom/google/android/gms/common/api/t;)V

    .line 65
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 147
    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 154
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public final am()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;)V

    .line 130
    return-void
.end method

.method public final varargs b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 159
    return-void
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/ai;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    const-string v0, "PlayerFriendsPlay"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v2, "circled"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->c(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f0200ed

    const v1, 0x7f0f00ba

    const v2, 0x7f0f00b9

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/players/ai;->a(III)V

    .line 50
    new-instance v0, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->an:Lcom/google/android/gms/games/ui/common/players/a;

    .line 55
    const v0, 0x7f0b01ed

    iput v0, p0, Lcom/google/android/gms/games/ui/p;->al:I

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/ai;->a(Landroid/support/v7/widget/bv;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 60
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/ai;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    const-string v0, "PlayerFriendsPlay"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ai;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 121
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/ai;->b(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0
.end method
