.class public final Lcom/google/android/gms/games/ui/destination/onboarding/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/onboard/p;


# instance fields
.field protected final a:Landroid/content/Context;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->a:Landroid/content/Context;

    .line 23
    iput p2, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->b:I

    .line 24
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->c:I

    .line 25
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/play/onboard/n;

    invoke-direct {v0}, Lcom/google/android/play/onboard/n;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->a:Landroid/content/Context;

    const v2, 0x7f0f00a2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/n;->a(Landroid/content/Context;I)Lcom/google/android/play/onboard/n;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/onboarding/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/ui/destination/onboarding/f;-><init>(Lcom/google/android/gms/games/ui/destination/onboarding/e;Lcom/google/android/play/onboard/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/n;->a(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 68
    const v0, 0x7f0f01f9

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    iget v2, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 70
    const v0, 0x7f0f015a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_0
    new-instance v1, Lcom/google/android/play/onboard/n;

    invoke-direct {v1}, Lcom/google/android/play/onboard/n;-><init>()V

    const v2, 0x7f02012a

    invoke-virtual {v1, v2}, Lcom/google/android/play/onboard/n;->a(I)Lcom/google/android/play/onboard/n;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/play/onboard/n;->a(Ljava/lang/String;)Lcom/google/android/play/onboard/n;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/onboarding/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/ui/destination/onboarding/g;-><init>(Lcom/google/android/gms/games/ui/destination/onboarding/e;Lcom/google/android/play/onboard/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/n;->a(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->c:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->b:I

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/e;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method
