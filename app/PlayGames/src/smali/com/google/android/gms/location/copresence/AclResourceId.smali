.class public Lcom/google/android/gms/location/copresence/AclResourceId;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/location/copresence/d;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/AclResourceId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->a:I

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->b:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->c:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->d:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 88
    const-string v0, "AclResourceId{application = %s, id = %s, part = %s}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/location/copresence/AclResourceId;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/d;->a(Lcom/google/android/gms/location/copresence/AclResourceId;Landroid/os/Parcel;)V

    .line 84
    return-void
.end method
