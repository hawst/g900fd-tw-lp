.class public final Lcom/google/android/gms/games/ui/c/a/e;
.super Lcom/google/android/gms/games/ui/c/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a/a;-><init>()V

    .line 20
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/Invitation;)Lcom/google/android/gms/games/ui/c/a/e;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/games/ui/c/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/a/e;-><init>()V

    .line 39
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 40
    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v2, "invitation"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 43
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/a/e;->g(Landroid/os/Bundle;)V

    .line 45
    return-object v0
.end method


# virtual methods
.method protected final P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 51
    const-string v1, "invitation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 52
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final Q()V
    .locals 3

    .prologue
    .line 57
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 58
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 59
    const-string v2, "invitation"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 62
    instance-of v2, v1, Lcom/google/android/gms/games/ui/c/a/f;

    if-eqz v2, :cond_0

    .line 63
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/f;

    .line 74
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/c/a/f;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 75
    return-void

    .line 64
    :cond_0
    instance-of v2, v1, Lcom/google/android/gms/games/ui/c/a/g;

    if-eqz v2, :cond_1

    .line 65
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/g;

    .line 67
    invoke-interface {v1}, Lcom/google/android/gms/games/ui/c/a/g;->Q()Lcom/google/android/gms/games/ui/c/a/f;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/c/a/f;

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "InvitationChangeAccountDialogFragment must be used with a parent Activity which implements InvitationAccountSwitcher or InvitationAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
