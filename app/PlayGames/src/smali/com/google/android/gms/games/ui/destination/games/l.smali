.class final Lcom/google/android/gms/games/ui/destination/games/l;
.super Landroid/app/SharedElementCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:Landroid/content/res/Resources;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;Landroid/content/res/Resources;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 473
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->b:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/games/l;->c:Landroid/view/ViewGroup;

    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 477
    const-string v0, "splash"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/l;->a:Landroid/view/ViewGroup;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    return-void
.end method

.method public final onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 485
    .line 486
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 487
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 490
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->a:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/play/c/i;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    new-array v4, v10, [I

    invoke-virtual {v2, v4}, Landroid/view/View;->getLocationInWindow([I)V

    aget v5, v4, v8

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    aget v4, v4, v9

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v4, v6

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    sub-int v5, v6, v5

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v3, v6

    sub-int/2addr v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v2, v4}, Landroid/view/View;->setRight(I)V

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v2, v4}, Landroid/view/View;->setBottom(I)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setLeft(I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setTop(I)V

    .line 496
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-nez v2, :cond_0

    .line 497
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 498
    if-nez v2, :cond_1

    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 499
    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 501
    :goto_1
    if-eqz v0, :cond_0

    .line 502
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 509
    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->d(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    aput-object v2, v0, v9

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->e(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v10

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c010d

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v3, 0x7f0c0110

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v11

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c0114

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c010b

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v4, 0x7f0c0251

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v8, v11, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v2, 0x7f0c010a

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->b:Landroid/content/res/Resources;

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->f(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    invoke-static {v0, v1, v2, v9}, Lcom/google/android/play/c/g;->a(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 523
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->g(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v3

    invoke-static {v2, v1, v3, v8}, Lcom/google/android/play/c/g;->a(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;

    move-result-object v1

    .line 527
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setEnterTransition(Landroid/transition/Transition;)V

    .line 530
    new-instance v0, Landroid/transition/Slide;

    const/16 v2, 0x50

    invoke-direct {v0, v2}, Landroid/transition/Slide;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/l;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 531
    new-array v2, v10, [Landroid/transition/Transition;

    aput-object v1, v2, v8

    aput-object v0, v2, v9

    invoke-static {v2}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->h(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 533
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/l;->d:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setReturnTransition(Landroid/transition/Transition;)V

    .line 534
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/games/m;-><init>(Lcom/google/android/gms/games/ui/destination/games/l;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 539
    return-void

    :cond_1
    move-object v0, v2

    goto/16 :goto_1

    :cond_2
    move-object v0, v1

    goto/16 :goto_0
.end method
