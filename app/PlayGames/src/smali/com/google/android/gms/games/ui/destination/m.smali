.class public abstract Lcom/google/android/gms/games/ui/destination/m;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"


# instance fields
.field protected A:Lcom/google/android/gms/games/ui/e/b;

.field private final B:Lcom/google/android/gms/games/ui/e/ah;

.field private C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

.field private D:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

.field public z:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/e/ah;II)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/destination/m;-><init>(Lcom/google/android/gms/games/ui/e/ah;IIZ)V

    .line 72
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/e/ah;IIZ)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 96
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 100
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/m;->B:Lcom/google/android/gms/games/ui/e/ah;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/m;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/m;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/m;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->b(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 249
    instance-of v0, p1, Lcom/google/android/gms/games/ui/p;

    if-eqz v0, :cond_0

    .line 250
    check-cast p1, Lcom/google/android/gms/games/ui/p;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/p;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 254
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 245
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/m;->b(Landroid/support/v4/app/Fragment;)V

    .line 246
    return-void
.end method


# virtual methods
.method protected final Q()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/b;->f()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected final a(IZ)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 229
    const-string v0, "DestMultiFragActivity"

    const-string v1, "setCurrentTab(): new tab index is bigger than the number of tabs. We must be in a rotation without any tabs. disregarding it."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/m;->R()Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    goto :goto_0

    .line 237
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->d(I)V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public final b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/gms/games/ui/destination/p;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/destination/p;-><init>(Lcom/google/android/gms/games/ui/destination/m;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->D:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->D:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 108
    const v0, 0x7f0c016c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    const-string v1, "layout resource did not include include a ViewPager with id \'pager\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 123
    new-instance v0, Lcom/google/android/gms/games/ui/e/b;

    iget-object v1, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/m;->B:Lcom/google/android/gms/games/ui/e/ah;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/e/ah;->a:[Lcom/google/android/gms/games/ui/e/ai;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/b;-><init>(Landroid/content/Context;Landroid/support/v4/app/ag;[Lcom/google/android/gms/games/ui/e/ai;Lcom/google/android/gms/games/ui/e/f;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/m;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    new-instance v1, Lcom/google/android/gms/games/ui/destination/n;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/n;-><init>(Lcom/google/android/gms/games/ui/destination/m;)V

    iput-object v1, v0, Lcom/google/android/gms/games/ui/e/b;->d:Lcom/google/android/gms/games/ui/e/d;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ao;)V

    .line 136
    const v0, 0x7f0c01a4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/m;->R()Z

    move-result v0

    if-nez v0, :cond_2

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/m;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c(I)V

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Landroid/support/v4/view/ViewPager;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/android/gms/games/ui/e/c;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/games/ui/e/c;-><init>(Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/e/b;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/cc;)V

    .line 162
    :goto_0
    if-nez p1, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->B:Lcom/google/android/gms/games/ui/e/ah;

    iget v0, v0, Lcom/google/android/gms/games/ui/e/ah;->b:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/m;->a(IZ)V

    .line 169
    :cond_1
    return-void

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i()V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/gms/games/ui/destination/o;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/m;->C:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/gms/games/ui/destination/o;-><init>(Lcom/google/android/gms/games/ui/destination/m;Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/e/b;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/support/v4/view/cc;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/b;->onResume()V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/m;->A:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 177
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/r;

    if-eqz v1, :cond_0

    .line 178
    check-cast v0, Lcom/google/android/gms/games/ui/destination/r;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/destination/r;->a(Lcom/google/android/gms/games/app/a;)V

    .line 180
    :cond_0
    return-void
.end method
