.class final Lcom/google/android/gms/games/ui/f;
.super Lcom/google/android/gms/games/ui/g;
.source "SourceFile"


# instance fields
.field private final m:Landroid/view/View;

.field private final n:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 730
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/g;-><init>(Landroid/view/View;)V

    .line 732
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->a:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->bF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/f;->m:Landroid/view/View;

    .line 733
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->a:Landroid/view/View;

    sget v1, Lcom/google/android/gms/g;->bT:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/f;->n:Landroid/view/View;

    .line 734
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 738
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/g;->a(Lcom/google/android/gms/games/ui/w;ILjava/lang/Object;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/e;

    .line 742
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/f;->s()I

    move-result v4

    .line 743
    invoke-static {v0, v4}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/games/ui/e;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 744
    iget-object v5, p0, Lcom/google/android/gms/games/ui/f;->m:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/games/ui/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 745
    iget-object v1, p0, Lcom/google/android/gms/games/ui/f;->n:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e;->a(Lcom/google/android/gms/games/ui/e;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 754
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/f;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->n:Landroid/view/View;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 756
    return-void

    :cond_0
    move v1, v3

    .line 744
    goto :goto_0

    :cond_1
    move v3, v2

    .line 745
    goto :goto_1

    .line 746
    :cond_2
    invoke-static {v0, v4}, Lcom/google/android/gms/games/ui/e;->b(Lcom/google/android/gms/games/ui/e;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 747
    iget-object v5, p0, Lcom/google/android/gms/games/ui/f;->m:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e;->b(Lcom/google/android/gms/games/ui/e;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 748
    iget-object v1, p0, Lcom/google/android/gms/games/ui/f;->n:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e;->b(Lcom/google/android/gms/games/ui/e;)Z

    move-result v5

    if-eqz v5, :cond_4

    :goto_4
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v1, v3

    .line 747
    goto :goto_3

    :cond_4
    move v3, v2

    .line 748
    goto :goto_4

    .line 750
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Given position is not a footer or a header: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
