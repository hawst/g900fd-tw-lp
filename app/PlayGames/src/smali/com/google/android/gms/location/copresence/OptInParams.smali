.class public Lcom/google/android/gms/location/copresence/OptInParams;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/location/copresence/i;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/OptInParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/google/android/gms/location/copresence/OptInParams;->a:I

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/OptInParams;->b:Ljava/lang/String;

    .line 65
    iput p3, p0, Lcom/google/android/gms/location/copresence/OptInParams;->c:I

    .line 66
    iput p4, p0, Lcom/google/android/gms/location/copresence/OptInParams;->d:I

    .line 67
    iput p5, p0, Lcom/google/android/gms/location/copresence/OptInParams;->e:I

    .line 68
    iput p6, p0, Lcom/google/android/gms/location/copresence/OptInParams;->f:I

    .line 69
    iput-object p7, p0, Lcom/google/android/gms/location/copresence/OptInParams;->g:Ljava/lang/String;

    .line 70
    iput-object p8, p0, Lcom/google/android/gms/location/copresence/OptInParams;->h:Ljava/lang/String;

    .line 71
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->c:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->e:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->f:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->h:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 125
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/i;->a(Lcom/google/android/gms/location/copresence/OptInParams;Landroid/os/Parcel;)V

    .line 126
    return-void
.end method
