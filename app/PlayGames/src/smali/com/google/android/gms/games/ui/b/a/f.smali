.class public Lcom/google/android/gms/games/ui/b/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/app/Activity;

.field protected b:Lcom/google/android/gms/games/ui/b/a/g;

.field protected final c:Lcom/google/android/gms/games/ui/b/a/i;


# direct methods
.method private constructor <init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b/a/f;->a:Landroid/app/Activity;

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/games/ui/b/a/f;->c:Lcom/google/android/gms/games/ui/b/a/i;

    .line 53
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/ui/b/a/f;->a(I)V

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;B)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/b/a/f;-><init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/games/ui/b/a/i;)Lcom/google/android/gms/games/ui/b/a/f;
    .locals 2

    .prologue
    const/16 v1, 0x31

    .line 42
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/h;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/games/ui/b/a/h;-><init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;)V

    .line 45
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/f;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/games/ui/b/a/f;-><init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/f;->c:Lcom/google/android/gms/games/ui/b/a/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/f;->b:Lcom/google/android/gms/games/ui/b/a/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/b/a/i;->a(Lcom/google/android/gms/games/ui/b/a/g;)V

    .line 72
    return-void
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/g;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/games/ui/b/a/g;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/f;->b:Lcom/google/android/gms/games/ui/b/a/g;

    .line 62
    return-void
.end method
