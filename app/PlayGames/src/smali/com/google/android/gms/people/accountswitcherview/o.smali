.class public Lcom/google/android/gms/people/accountswitcherview/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static volatile a:Ljava/util/concurrent/Executor;

.field private static d:I

.field private static l:Z


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/google/android/gms/common/api/t;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;

.field private f:Z

.field private g:Lcom/google/android/gms/people/accountswitcherview/p;

.field private final h:Ljava/util/LinkedList;

.field private i:Z

.field private j:F

.field private k:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Ljava/util/concurrent/Executor;

    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    sput-object v0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Ljava/util/concurrent/Executor;

    .line 65
    :cond_0
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/people/accountswitcherview/o;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Z)V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    .line 103
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/content/Context;

    .line 104
    iput-object p2, p0, Lcom/google/android/gms/people/accountswitcherview/o;->c:Lcom/google/android/gms/common/api/t;

    .line 105
    iput-boolean p3, p0, Lcom/google/android/gms/people/accountswitcherview/o;->i:Z

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 107
    sget v1, Lcom/google/android/gms/people/accountswitcherview/ac;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->k:F

    .line 108
    sget v1, Lcom/google/android/gms/people/accountswitcherview/ac;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->j:F

    .line 109
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 111
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/people/accountswitcherview/o;->l:Z

    .line 112
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IIII)I
    .locals 4

    .prologue
    .line 307
    sget-boolean v0, Lcom/google/android/gms/people/accountswitcherview/o;->l:Z

    if-eqz v0, :cond_1

    .line 308
    const/4 v0, 0x2

    .line 326
    :cond_0
    return v0

    .line 310
    :cond_1
    const/4 v0, 0x1

    .line 312
    if-gt p0, p3, :cond_2

    if-le p1, p2, :cond_0

    .line 314
    :cond_2
    div-int/lit8 v1, p0, 0x2

    .line 315
    div-int/lit8 v2, p1, 0x2

    .line 321
    :goto_0
    div-int v3, v1, v0

    if-lt v3, p3, :cond_0

    div-int v3, v2, v0

    if-lt v3, p2, :cond_0

    .line 322
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 375
    int-to-float v0, p1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne p1, v1, :cond_0

    if-ne v0, v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v3, p1

    int-to-float v4, v1

    div-float/2addr v3, v4

    int-to-float v4, v0

    int-to-float v6, v2

    div-float/2addr v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v5, v4, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    int-to-float v3, p1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v0, v1

    mul-float/2addr v0, v7

    div-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    sub-float/2addr v0, v6

    float-to-int v0, v0

    int-to-float v6, v2

    mul-float/2addr v6, v7

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int v0, v2, v4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/os/ParcelFileDescriptor;IIII)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 331
    if-nez p0, :cond_0

    .line 352
    :goto_0
    return-object v0

    .line 334
    :cond_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 335
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/people/accountswitcherview/o;->a(IIII)I

    move-result v1

    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 336
    const/4 v1, 0x0

    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 337
    iput p1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 338
    iput p2, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 339
    sget-boolean v1, Lcom/google/android/gms/people/accountswitcherview/o;->l:Z

    if-eqz v1, :cond_1

    .line 340
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 345
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 348
    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 349
    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Ljava/io/InputStream;)V

    :cond_2
    throw v0

    .line 348
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static a(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 357
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :goto_0
    return-void

    .line 359
    :catch_0
    move-exception v0

    const-string v0, "AvatarManager"

    const-string v1, "Exception closing the cover photo input stream."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/people/accountswitcherview/o;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/people/accountswitcherview/o;)F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->j:F

    return v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/p;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/p;->a()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/people/accountswitcherview/o;)F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->k:F

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/people/accountswitcherview/o;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    iput-boolean v1, v0, Lcom/google/android/gms/people/accountswitcherview/p;->d:Z

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 122
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    .line 123
    return-void
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 161
    const/4 v0, 0x0

    move v1, v0

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 167
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_2

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/people/accountswitcherview/p;->d:Z

    .line 174
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;IIILcom/google/android/gms/people/accountswitcherview/p;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 195
    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    if-eq v0, p6, :cond_2

    .line 198
    const-string v0, "AvatarManager"

    const-string v1, "Got a different request than we\'re waiting for!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    if-nez v0, :cond_0

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    .line 226
    :cond_0
    if-eqz p2, :cond_1

    .line 228
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 233
    :cond_1
    :goto_0
    return-void

    .line 203
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->g:Lcom/google/android/gms/people/accountswitcherview/p;

    .line 204
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    .line 223
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    if-nez v0, :cond_3

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    .line 226
    :cond_3
    if-eqz p2, :cond_1

    .line 228
    :try_start_3
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 207
    :cond_4
    :try_start_4
    iget-object v0, p6, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    .line 208
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p6, :cond_5

    iget-boolean v0, p6, Lcom/google/android/gms/people/accountswitcherview/p;->d:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_7

    .line 223
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    if-nez v0, :cond_6

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    .line 226
    :cond_6
    if-eqz p2, :cond_1

    .line 228
    :try_start_5
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    .line 213
    :cond_7
    :try_start_6
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez p2, :cond_9

    .line 214
    :cond_8
    const-string v0, "AvatarManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Avatar loaded: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_9
    if-eqz p2, :cond_a

    .line 218
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/q;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/accountswitcherview/q;-><init>(Lcom/google/android/gms/people/accountswitcherview/o;Lcom/google/android/gms/people/accountswitcherview/p;Landroid/os/ParcelFileDescriptor;III)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_c

    sget-object v1, Lcom/google/android/gms/people/accountswitcherview/o;->a:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/accountswitcherview/q;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_1
    move-object p2, v7

    .line 223
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    if-nez v0, :cond_b

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    .line 226
    :cond_b
    if-eqz p2, :cond_1

    .line 228
    :try_start_7
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    .line 230
    :catch_2
    move-exception v0

    goto/16 :goto_0

    .line 218
    :cond_c
    const/4 v1, 0x0

    :try_start_8
    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 223
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->f:Z

    if-nez v1, :cond_d

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    .line 226
    :cond_d
    if-eqz p2, :cond_e

    .line 228
    :try_start_9
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 230
    :cond_e
    :goto_2
    throw v0

    :catch_3
    move-exception v0

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/people/accountswitcherview/p;)V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lcom/google/android/gms/people/accountswitcherview/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, Lcom/google/android/gms/people/accountswitcherview/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 132
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Landroid/widget/ImageView;)V

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Landroid/widget/ImageView;)V

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->c:Lcom/google/android/gms/common/api/t;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "AvatarManager"

    const-string v1, "Client not connected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->b()V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 450
    if-eqz p2, :cond_0

    .line 451
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 453
    :cond_0
    return-void
.end method
