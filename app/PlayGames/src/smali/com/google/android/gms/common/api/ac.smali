.class final Lcom/google/android/gms/common/api/ac;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/x;)V
    .locals 1

    .prologue
    .line 347
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 348
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/ac;->a:Ljava/lang/ref/WeakReference;

    .line 349
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 353
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 354
    const/4 v0, 0x0

    .line 355
    if-eqz v1, :cond_0

    .line 356
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 358
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 369
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/ac;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/x;

    .line 363
    if-eqz v0, :cond_1

    .line 366
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/x;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/x;->e()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/gms/common/api/x;->g:Z

    if-eqz v1, :cond_1

    .line 367
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/x;->b()V

    goto :goto_0
.end method
