.class public final Lcom/google/android/gms/games/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/games/ui/l;->h:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/b/a;->a:Z

    .line 181
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/games/b/a;->b:Z

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/c/af;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 561
    new-instance v0, Lcom/google/android/gms/games/c/af;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/af;-><init>()V

    .line 562
    iput-boolean v1, v0, Lcom/google/android/gms/games/c/af;->a:Z

    .line 563
    iput-object p2, v0, Lcom/google/android/gms/games/c/af;->b:Ljava/lang/String;

    .line 564
    iput-object p1, v0, Lcom/google/android/gms/games/c/af;->d:Ljava/lang/String;

    .line 565
    const-string v1, "6586000"

    iput-object v1, v0, Lcom/google/android/gms/games/c/af;->e:Ljava/lang/String;

    .line 571
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 572
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 573
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/google/android/gms/games/c/af;->f:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 577
    :goto_0
    return-object v0

    .line 575
    :catch_0
    move-exception v1

    const-string v1, "GamesPlayLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find package info for package: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/gms/games/c/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/r;-><init>()V

    .line 402
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p1}, Lcom/google/android/gms/games/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/c/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->a:Lcom/google/android/gms/games/c/af;

    .line 404
    new-instance v1, Lcom/google/android/gms/games/c/ac;

    invoke-direct {v1}, Lcom/google/android/gms/games/c/ac;-><init>()V

    iput p3, v1, Lcom/google/android/gms/games/c/ac;->a:I

    iput-boolean p4, v1, Lcom/google/android/gms/games/c/ac;->b:Z

    iput-object v1, v0, Lcom/google/android/gms/games/c/r;->h:Lcom/google/android/gms/games/c/ac;

    .line 405
    sget-boolean v1, Lcom/google/android/gms/games/b/a;->b:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/google/android/gms/games/b/a;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "GamesPlayLogger"

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    new-instance v1, Lcom/google/android/gms/playlog/a;

    const/4 v4, 0x5

    invoke-direct {v1, p0, v4, p2}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    const/4 v4, 0x0

    invoke-static {v0}, Lcom/google/android/gms/games/c/r;->a(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v1, v4, v0, v5}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 406
    :cond_1
    return-void

    .line 405
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method
