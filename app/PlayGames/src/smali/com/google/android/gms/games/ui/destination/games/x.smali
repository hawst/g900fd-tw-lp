.class final Lcom/google/android/gms/games/ui/destination/games/x;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 1470
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;B)V
    .locals 0

    .prologue
    .line 1470
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/x;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1474
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1500
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1479
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1480
    if-eqz v0, :cond_0

    .line 1481
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 1482
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1483
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    .line 1486
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->z(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    .line 1487
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->b()V

    goto :goto_0

    .line 1490
    :cond_2
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1491
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1492
    if-eqz v0, :cond_0

    .line 1493
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 1494
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1495
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    .line 1496
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/x;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->b()V

    goto :goto_0
.end method
