.class final Lcom/google/android/gms/games/internal/ac;
.super Lcom/google/android/gms/common/api/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/o;


# instance fields
.field private final c:Lcom/google/android/gms/games/a/c;

.field private final d:Lcom/google/android/gms/games/a/f;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 2367
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/api/s;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2368
    new-instance v1, Lcom/google/android/gms/games/a/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2370
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 2371
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/a/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/c;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ac;->c:Lcom/google/android/gms/games/a/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2376
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->f_()V

    .line 2378
    new-instance v0, Lcom/google/android/gms/games/a/f;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/a/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ac;->d:Lcom/google/android/gms/games/a/f;

    .line 2379
    return-void

    .line 2373
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/gms/games/internal/ac;->c:Lcom/google/android/gms/games/a/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2376
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/b;->f_()V

    throw v0
.end method


# virtual methods
.method public final c()Lcom/google/android/gms/games/a/a;
    .locals 1

    .prologue
    .line 2383
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ac;->c:Lcom/google/android/gms/games/a/c;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/games/a/f;
    .locals 1

    .prologue
    .line 2388
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ac;->d:Lcom/google/android/gms/games/a/f;

    return-object v0
.end method
