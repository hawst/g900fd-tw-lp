.class public abstract Lcom/google/android/gms/games/ui/common/a/a;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/bk;
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/as;


# instance fields
.field protected am:Lcom/google/android/gms/games/ui/common/a/e;

.field private an:Lcom/google/android/gms/games/ui/common/a/n;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/t;Z)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->an:Lcom/google/android/gms/games/ui/common/a/n;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/a/n;->r_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 70
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/games/ui/common/a/a;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/z;Z)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 72
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/games/quest/f;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->c()Lcom/google/android/gms/games/quest/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/a;->ap()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/a;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->an:Lcom/google/android/gms/games/ui/common/a/n;

    invoke-interface {v2}, Lcom/google/android/gms/games/ui/common/a/n;->r_()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->r()V

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->am:Lcom/google/android/gms/games/ui/common/a/e;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/a/e;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->a()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->y()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/common/a/a;->a(Lcom/google/android/gms/common/api/t;Z)V

    .line 63
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/z;Z)V
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/a;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->F:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/e/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/m;->a()V

    goto :goto_0

    :cond_2
    const-string v0, "BaseQuestFragment"

    const-string v1, "No valid listener to update the inbox counts"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/a/k;

    const-string v1, "Parent activity is not a QuestInboxHelperProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/a/k;->z_()Lcom/google/android/gms/games/ui/common/a/j;

    move-result-object v1

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/a/n;

    const-string v2, "Parent activity did not implement QuestUiConfiguration"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/n;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->an:Lcom/google/android/gms/games/ui/common/a/n;

    .line 50
    sget v0, Lcom/google/android/gms/f;->y:I

    sget v2, Lcom/google/android/gms/l;->bl:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/a/a;->a(III)V

    .line 53
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/e;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/a/a;->an:Lcom/google/android/gms/games/ui/common/a/n;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/gms/games/ui/common/a/e;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Z)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->am:Lcom/google/android/gms/games/ui/common/a/e;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/a;->am:Lcom/google/android/gms/games/ui/common/a/e;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/a/a;->a(Landroid/support/v7/widget/bv;)V

    .line 57
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/common/a/a;->a(Landroid/support/v4/widget/bk;)V

    .line 58
    return-void
.end method

.method public final o_()V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/a;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/a/a;->a(Lcom/google/android/gms/common/api/t;Z)V

    .line 125
    :cond_0
    return-void
.end method

.method public final v_()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/a/a;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/a;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/common/a/a;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/z;Z)V

    goto :goto_0
.end method
