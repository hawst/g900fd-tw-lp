.class public final Lcom/google/android/gms/games/c/as;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/games/c/as;


# instance fields
.field public a:I

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 8169
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8170
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/c/as;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/as;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/as;->G:I

    .line 8171
    return-void
.end method

.method public static b()[Lcom/google/android/gms/games/c/as;
    .locals 2

    .prologue
    .line 8152
    sget-object v0, Lcom/google/android/gms/games/c/as;->c:[Lcom/google/android/gms/games/c/as;

    if-nez v0, :cond_1

    .line 8153
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8155
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/c/as;->c:[Lcom/google/android/gms/games/c/as;

    if-nez v0, :cond_0

    .line 8156
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/c/as;

    sput-object v0, Lcom/google/android/gms/games/c/as;->c:[Lcom/google/android/gms/games/c/as;

    .line 8158
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8160
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/c/as;->c:[Lcom/google/android/gms/games/c/as;

    return-object v0

    .line 8158
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 8221
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 8222
    iget v1, p0, Lcom/google/android/gms/games/c/as;->a:I

    if-eq v1, v2, :cond_0

    .line 8223
    iget v1, p0, Lcom/google/android/gms/games/c/as;->a:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8226
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/c/as;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 8227
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/c/as;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8230
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/as;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/as;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 8210
    iget v0, p0, Lcom/google/android/gms/games/c/as;->a:I

    if-eq v0, v1, :cond_0

    .line 8211
    iget v0, p0, Lcom/google/android/gms/games/c/as;->a:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8213
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/c/as;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 8214
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/c/as;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 8216
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 8217
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8182
    if-ne p1, p0, :cond_1

    .line 8195
    :cond_0
    :goto_0
    return v0

    .line 8185
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/as;

    if-nez v2, :cond_2

    move v0, v1

    .line 8186
    goto :goto_0

    .line 8188
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/as;

    .line 8189
    iget v2, p0, Lcom/google/android/gms/games/c/as;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/as;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 8190
    goto :goto_0

    .line 8192
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/games/c/as;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/as;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 8193
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 8200
    iget v0, p0, Lcom/google/android/gms/games/c/as;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8202
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/c/as;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/c/as;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 8204
    return v0
.end method
