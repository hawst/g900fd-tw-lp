.class public Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/play/onboard/o;
.implements Lcom/google/android/play/onboard/p;


# instance fields
.field private d:Landroid/app/Activity;

.field private e:Landroid/content/Context;

.field private f:Lcom/google/android/gms/games/ui/destination/onboarding/c;

.field private g:Lcom/google/android/play/onboard/d;

.field private h:Lcom/google/android/gms/games/ui/destination/onboarding/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->e:Landroid/content/Context;

    .line 77
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    const v1, 0x7f040079

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 81
    const v0, 0x7f0c01af

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    new-instance v0, Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/destination/onboarding/a;-><init>(Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    .line 85
    invoke-static {p1}, Lcom/google/android/gms/games/app/b;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 86
    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 87
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/destination/onboarding/a;->add(Ljava/lang/Object;)V

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    const v0, 0x7f0c00d2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/a;->clear()V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/a;->addAll(Ljava/util/Collection;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->h:Lcom/google/android/gms/games/ui/destination/onboarding/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/a;->notifyDataSetChanged()V

    .line 110
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->d:Landroid/app/Activity;

    .line 101
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/destination/onboarding/c;)V
    .locals 1

    .prologue
    .line 96
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/c;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->f:Lcom/google/android/gms/games/ui/destination/onboarding/c;

    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/play/onboard/d;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->g:Lcom/google/android/play/onboard/d;

    .line 200
    return-void
.end method

.method public final b(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 1

    .prologue
    .line 252
    new-instance v0, Lcom/google/android/play/onboard/n;

    invoke-direct {v0}, Lcom/google/android/play/onboard/n;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/play/onboard/n;->a()Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/play/onboard/p;
    .locals 0

    .prologue
    .line 204
    return-object p0
.end method

.method public final c(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 1

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/play/onboard/n;

    invoke-direct {v0}, Lcom/google/android/play/onboard/n;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/play/onboard/n;->a()Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x3

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->f:Lcom/google/android/gms/games/ui/destination/onboarding/c;

    if-nez v0, :cond_0

    .line 167
    const-string v0, "OnboardAccountsPage"

    const-string v1, "onClick with null listener, ignoring."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :goto_0
    return-void

    .line 171
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 172
    const v1, 0x7f0c01af

    if-ne v0, v1, :cond_1

    .line 173
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->d:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 175
    :cond_1
    const-string v0, "OnboardAccountsPage"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled onClick view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->f:Lcom/google/android/gms/games/ui/destination/onboarding/c;

    if-nez v0, :cond_1

    .line 182
    const-string v0, "OnboardAccountsPage"

    const-string v1, "onItemClick with null listener, ignoring."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/onboarding/b;->b:Landroid/accounts/Account;

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->f:Lcom/google/android/gms/games/ui/destination/onboarding/c;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/c;->a(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->g:Lcom/google/android/play/onboard/d;

    invoke-interface {v0}, Lcom/google/android/play/onboard/d;->b()V

    goto :goto_0
.end method
