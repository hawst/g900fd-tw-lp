.class public final Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;
.super Lcom/google/android/gms/games/ui/destination/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/e/af;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private an:Lcom/google/android/gms/games/ui/destination/games/aj;

.field private ao:Landroid/view/View;

.field private ap:Z

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    const v0, 0x7f04005a

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/h;-><init>(I)V

    .line 47
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ap:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->as:Z

    .line 67
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->at:Z

    .line 71
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/h;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 77
    const v1, 0x7f0c0168

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ao:Landroid/view/View;

    .line 79
    const v1, 0x7f0200d3

    const v2, 0x7f0f0196

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->a(III)V

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ao:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 88
    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 36
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->Q()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->as:Z

    if-eqz v0, :cond_1

    const-string v0, "GameSearchResFrag"

    const-string v1, "onExtendedGamesLoaded: discarding stray result"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/aj;->k()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/aj;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->at:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->at:Z

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Landroid/view/View;

    const v3, 0x102000a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ap:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->aq:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "GameSearchResFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGoogleApiClientConnected: running pending query \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->aq:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->aq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->a(Ljava/lang/String;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->aq:Ljava/lang/String;

    .line 128
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 142
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ap:Z

    if-nez v0, :cond_1

    .line 143
    const-string v0, "GameSearchResFrag"

    const-string v1, "doSearch: not connected yet! Stashing away mPendingQuery..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->aq:Ljava/lang/String;

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/aj;->b()V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ao:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 165
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->as:Z

    goto :goto_0

    .line 170
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ar:Ljava/lang/String;

    .line 171
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ar:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->f(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/i;->e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ao:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 181
    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->as:Z

    .line 185
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->at:Z

    goto :goto_0
.end method

.method final b(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 279
    const-string v1, "getSuggestions must not be called from the main thread!"

    invoke-static {v1}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/String;)V

    .line 281
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-object v0

    .line 285
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 286
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 288
    const-string v1, "GameSearchResFrag"

    const-string v2, "getSuggestions: not connected; ignoring..."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 292
    :cond_2
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/games/i;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 297
    invoke-interface {v0}, Lcom/google/android/gms/common/api/aj;->a()Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/k;

    .line 298
    invoke-interface {v0}, Lcom/google/android/gms/games/k;->c()Lcom/google/android/gms/games/internal/game/h;

    move-result-object v2

    .line 299
    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/h;->a()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 300
    const/4 v1, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/game/h;->a()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_0

    .line 301
    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/internal/game/h;->b(I)Lcom/google/android/gms/games/internal/game/g;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/internal/game/g;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 300
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final b_(I)V
    .locals 4

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    const-string v0, "GameSearchResFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :goto_0
    return-void

    .line 257
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ar:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->f(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/i;->f(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/h;->d(Landroid/os/Bundle;)V

    .line 96
    new-instance v1, Lcom/google/android/gms/games/ui/ac;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    .line 97
    const v0, 0x7f0f0194

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/games/ak;

    const-string v2, "Parent must implement GameSmallCardEventListener."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 103
    new-instance v2, Lcom/google/android/gms/games/ui/destination/games/aj;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    const/4 v4, 0x2

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->am:Lcom/google/android/gms/games/ui/destination/b;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/ak;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/android/gms/games/ui/destination/games/aj;-><init>(Lcom/google/android/gms/games/ui/n;IILcom/google/android/gms/games/ui/destination/games/ak;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/aj;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 108
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 109
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->an:Lcom/google/android/gms/games/ui/destination/games/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    .line 112
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 113
    return-void
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->ar:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameSearchResultsFragment;->a(Ljava/lang/String;)V

    .line 269
    return-void
.end method
