.class public final Lcom/google/android/gms/games/ui/destination/main/m;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/gms/games/ui/n;

.field private g:Lcom/google/android/gms/games/ui/destination/main/o;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/destination/main/o;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/main/m;->e:Lcom/google/android/gms/games/ui/n;

    .line 44
    const-string v0, "ProfileVisibilityButterbarEventListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/main/o;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/m;->g:Lcom/google/android/gms/games/ui/destination/main/o;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/main/m;)Lcom/google/android/gms/games/ui/destination/main/o;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/m;->g:Lcom/google/android/gms/games/ui/destination/main/o;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/games/ui/destination/main/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/main/m;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f040029

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/destination/main/n;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 54
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/main/m;->c(Z)V

    .line 55
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f040029

    return v0
.end method
