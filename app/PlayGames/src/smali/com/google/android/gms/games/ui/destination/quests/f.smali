.class public final Lcom/google/android/gms/games/ui/destination/quests/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/common/a/c;

.field private final b:Lcom/google/android/gms/games/ui/n;

.field private final c:Lcom/google/android/gms/games/ui/common/a/g;

.field private final d:Lcom/google/android/gms/games/ui/common/a/m;

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/gms/games/ui/ag;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/ag;Landroid/view/View;Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/a/g;Lcom/google/android/gms/games/ui/common/a/m;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/c;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/ui/common/a/c;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->a:Lcom/google/android/gms/games/ui/common/a/c;

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->a:Lcom/google/android/gms/games/ui/common/a/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/a/c;->a(Landroid/view/View$OnClickListener;)V

    .line 35
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->b:Lcom/google/android/gms/games/ui/n;

    .line 36
    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->c:Lcom/google/android/gms/games/ui/common/a/g;

    .line 37
    iput-object p5, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->d:Lcom/google/android/gms/games/ui/common/a/m;

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->a:Lcom/google/android/gms/games/ui/common/a/c;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->b:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->e:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->d:Lcom/google/android/gms/games/ui/common/a/m;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->e:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->f:Lcom/google/android/gms/games/ui/ag;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/quest/Quest;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->a:Lcom/google/android/gms/games/ui/common/a/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->f:Lcom/google/android/gms/games/ui/ag;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->b:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0, p1, v1, p2, v2}, Lcom/google/android/gms/games/ui/common/a/c;->a(Landroid/content/Context;Lcom/google/android/gms/games/ui/ag;Lcom/google/android/gms/games/quest/Quest;Lcom/google/android/gms/games/ui/n;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->e:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 53
    invoke-static {p1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    instance-of v1, v0, Lcom/google/android/gms/games/quest/Quest;

    if-eqz v1, :cond_1

    .line 56
    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->e:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->d:Lcom/google/android/gms/games/ui/common/a/m;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/a/m;->a(Lcom/google/android/gms/games/quest/Quest;)V

    .line 67
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/f;->c:Lcom/google/android/gms/games/ui/common/a/g;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/a/g;->b(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0

    .line 65
    :cond_1
    const-string v1, "QuestCarouselVH"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
