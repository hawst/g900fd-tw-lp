.class public abstract Lcom/google/android/gms/common/api/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/al;
.implements Lcom/google/android/gms/common/api/am;


# instance fields
.field protected final a:Lcom/google/android/gms/common/api/Status;

.field protected final b:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->e()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/s;->a:Lcom/google/android/gms/common/api/Status;

    .line 17
    iput-object p1, p0, Lcom/google/android/gms/common/api/s;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/common/api/s;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/common/api/s;->b:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/common/api/s;->b:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    .line 30
    :cond_0
    return-void
.end method
