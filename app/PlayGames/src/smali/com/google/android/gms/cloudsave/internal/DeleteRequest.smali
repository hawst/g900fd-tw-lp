.class public Lcom/google/android/gms/cloudsave/internal/DeleteRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/common/data/DataHolder;

.field public final c:Lcom/google/android/gms/cloudsave/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/DeleteRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/cloudsave/Query;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/DeleteRequest;->a:I

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/cloudsave/internal/DeleteRequest;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/internal/DeleteRequest;->c:Lcom/google/android/gms/cloudsave/Query;

    .line 57
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/b;->a(Lcom/google/android/gms/cloudsave/internal/DeleteRequest;Landroid/os/Parcel;I)V

    .line 42
    return-void
.end method
