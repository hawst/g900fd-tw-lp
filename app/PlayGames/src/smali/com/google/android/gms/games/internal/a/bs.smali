.class public final Lcom/google/android/gms/games/internal/a/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/quest/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[IIZ)Lcom/google/android/gms/common/api/aj;
    .locals 8

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/gms/games/internal/a/bu;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/bu;-><init>(Lcom/google/android/gms/games/internal/a/bs;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[IIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;[IZ)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/gms/games/internal/a/bt;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/bt;-><init>(Lcom/google/android/gms/games/internal/a/bs;Lcom/google/android/gms/common/api/t;[IIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 129
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->p()V

    .line 130
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/quest/d;)V
    .locals 2

    .prologue
    .line 123
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 124
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->c(Lcom/google/android/gms/common/api/ag;)V

    .line 125
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/quest/d;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 214
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 215
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->c(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 222
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->c(Ljava/lang/String;)V

    .line 223
    return-void
.end method
