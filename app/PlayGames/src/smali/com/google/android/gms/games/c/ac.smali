.class public final Lcom/google/android/gms/games/c/ac;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5241
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5242
    iput v0, p0, Lcom/google/android/gms/games/c/ac;->a:I

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ac;->G:I

    .line 5243
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 5292
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 5293
    iget v1, p0, Lcom/google/android/gms/games/c/ac;->a:I

    if-eqz v1, :cond_0

    .line 5294
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/ac;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5297
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    if-eqz v1, :cond_1

    .line 5298
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5301
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5209
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/ac;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5281
    iget v0, p0, Lcom/google/android/gms/games/c/ac;->a:I

    if-eqz v0, :cond_0

    .line 5282
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/ac;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5284
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    if-eqz v0, :cond_1

    .line 5285
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5287
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 5288
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5254
    if-ne p1, p0, :cond_1

    .line 5267
    :cond_0
    :goto_0
    return v0

    .line 5257
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ac;

    if-nez v2, :cond_2

    move v0, v1

    .line 5258
    goto :goto_0

    .line 5260
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ac;

    .line 5261
    iget v2, p0, Lcom/google/android/gms/games/c/ac;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/ac;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5262
    goto :goto_0

    .line 5264
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ac;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5265
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5272
    iget v0, p0, Lcom/google/android/gms/games/c/ac;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5274
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ac;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 5275
    return v0

    .line 5274
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method
