.class public Lcom/google/android/gms/car/support/a;
.super Lcom/google/android/gms/car/support/ab;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map;


# instance fields
.field a:Lcom/google/android/gms/car/support/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/car/support/ab;-><init>()V

    .line 56
    return-void
.end method

.method private b()Lcom/google/android/gms/car/support/u;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/car/support/a;->a:Lcom/google/android/gms/car/support/u;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/google/android/gms/car/support/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/b;-><init>(Lcom/google/android/gms/car/support/a;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/a;->a:Lcom/google/android/gms/car/support/u;

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/a;->a:Lcom/google/android/gms/car/support/u;

    return-object v0
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/car/support/a;->b()Lcom/google/android/gms/car/support/u;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/w;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/car/support/w;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/support/w;-><init>(Lcom/google/android/gms/car/support/u;)V

    iput-object v1, v0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/w;

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/w;

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/google/android/gms/car/support/a;->b()Lcom/google/android/gms/car/support/u;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/car/support/u;->c:Lcom/google/android/gms/car/support/x;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/car/support/x;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/support/x;-><init>(Lcom/google/android/gms/car/support/u;)V

    iput-object v1, v0, Lcom/google/android/gms/car/support/u;->c:Lcom/google/android/gms/car/support/x;

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/car/support/u;->c:Lcom/google/android/gms/car/support/x;

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 140
    iget v0, p0, Lcom/google/android/gms/car/support/a;->h:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    array-length v1, v1

    if-ge v1, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget-object v2, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    invoke-super {p0, v0}, Lcom/google/android/gms/car/support/ab;->a(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->f:[I

    iget v3, p0, Lcom/google/android/gms/car/support/ab;->h:I

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ab;->g:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/car/support/ab;->h:I

    shl-int/lit8 v3, v3, 0x1

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/ab;->h:I

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/car/support/ab;->a([I[Ljava/lang/Object;I)V

    .line 141
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 142
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/car/support/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 144
    :cond_2
    return-void
.end method

.method public values()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/gms/car/support/a;->b()Lcom/google/android/gms/car/support/u;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/car/support/u;->d:Lcom/google/android/gms/car/support/z;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/car/support/z;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/support/z;-><init>(Lcom/google/android/gms/car/support/u;)V

    iput-object v1, v0, Lcom/google/android/gms/car/support/u;->d:Lcom/google/android/gms/car/support/z;

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/car/support/u;->d:Lcom/google/android/gms/car/support/z;

    return-object v0
.end method
