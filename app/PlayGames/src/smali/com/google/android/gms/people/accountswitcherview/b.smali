.class public final Lcom/google/android/gms/people/accountswitcherview/b;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/gms/people/accountswitcherview/an;
.implements Lcom/google/android/gms/people/accountswitcherview/aq;


# instance fields
.field public a:Lcom/google/android/gms/people/accountswitcherview/af;

.field private b:Lcom/google/android/gms/people/accountswitcherview/c;

.field private c:Lcom/google/android/gms/people/accountswitcherview/e;

.field private d:Lcom/google/android/gms/people/accountswitcherview/d;

.field private e:Lcom/google/android/gms/people/model/e;

.field private f:Ljava/util/List;

.field private g:Landroid/widget/FrameLayout;

.field private h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

.field private i:Lcom/google/android/gms/people/accountswitcherview/r;

.field private j:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

.field private k:Z

.field private l:Landroid/view/ViewGroup;

.field private m:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

.field private n:Lcom/google/android/gms/people/accountswitcherview/g;

.field private o:Z

.field private p:Lcom/google/android/gms/people/accountswitcherview/f;

.field private q:I

.field private r:I

.field private s:Z

.field private t:Landroid/view/View;


# direct methods
.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 400
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->q:I

    .line 402
    return-void
.end method

.method private a(Lcom/google/android/gms/people/model/e;Z)V
    .locals 3

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    .line 485
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    .line 486
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->f:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 487
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->f:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/people/accountswitcherview/r;->a(Ljava/util/List;Lcom/google/android/gms/people/model/e;Lcom/google/android/gms/people/model/e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->f:Ljava/util/List;

    .line 488
    if-nez p2, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/e;)V

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->i:Lcom/google/android/gms/people/accountswitcherview/r;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/r;->a(Ljava/util/List;)V

    .line 496
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/e;)V

    goto :goto_0
.end method

.method private a(ZLandroid/view/animation/Interpolator;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 635
    if-eqz p1, :cond_0

    move v0, v1

    move v3, v2

    .line 643
    :goto_0
    const/16 v4, 0xb

    invoke-static {v4}, Lcom/google/android/gms/people/accountswitcherview/b;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 644
    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/b;->j:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const-string v5, "animatedHeightFraction"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v3, v3

    aput v3, v6, v2

    int-to-float v0, v0

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 646
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 647
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 648
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 652
    :goto_1
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 640
    goto :goto_0

    .line 650
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->j:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->a(F)V

    goto :goto_1
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 772
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(I)Z

    move-result v0

    return v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 765
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->r:I

    .line 407
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 846
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->a:Lcom/google/android/gms/people/accountswitcherview/af;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/af;->a(I)V

    .line 849
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(I)V

    .line 850
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    const v5, 0x3f4ccccd    # 0.8f

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 596
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->p:Lcom/google/android/gms/people/accountswitcherview/f;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->p:Lcom/google/android/gms/people/accountswitcherview/f;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    .line 600
    :cond_0
    return-void

    .line 595
    :pswitch_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v4, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(ZLandroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->j:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const-wide/16 v2, 0x85

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    const/4 v0, 0x1

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(ZLandroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->j:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/people/model/e;)V
    .locals 1

    .prologue
    .line 688
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Lcom/google/android/gms/people/model/e;Z)V

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->b:Lcom/google/android/gms/people/accountswitcherview/c;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->b:Lcom/google/android/gms/people/accountswitcherview/c;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    .line 692
    :cond_0
    return-void
.end method

.method public final getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x2

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 705
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_1

    .line 706
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->n:Lcom/google/android/gms/people/accountswitcherview/g;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->n:Lcom/google/android/gms/people/accountswitcherview/g;

    .line 719
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->m:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    if-ne p1, v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b(I)V

    .line 714
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->m:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v3

    if-ne v3, v2, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->a(Z)V

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 710
    goto :goto_1

    :cond_3
    move v2, v1

    .line 714
    goto :goto_2
.end method

.method protected final onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 289
    iput-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->t:Landroid/view/View;

    .line 291
    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->i:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->i:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/r;->a(I)Lcom/google/android/gms/people/model/e;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Lcom/google/android/gms/people/model/e;Z)V

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->b:Lcom/google/android/gms/people/accountswitcherview/c;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->b:Lcom/google/android/gms/people/accountswitcherview/c;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->e:Lcom/google/android/gms/people/model/e;

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->i:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 663
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->d:Lcom/google/android/gms/people/accountswitcherview/d;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->d:Lcom/google/android/gms/people/accountswitcherview/d;

    invoke-interface {v0}, Lcom/google/android/gms/people/accountswitcherview/d;->L()V

    goto :goto_0

    .line 666
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->i:Lcom/google/android/gms/people/accountswitcherview/r;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/r;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->c:Lcom/google/android/gms/people/accountswitcherview/e;

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->c:Lcom/google/android/gms/people/accountswitcherview/e;

    invoke-interface {v0}, Lcom/google/android/gms/people/accountswitcherview/e;->M()V

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 315
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 320
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    .line 321
    :goto_0
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->q:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 322
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->q:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 324
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->r:I

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->r:I

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 327
    :cond_1
    return-void

    .line 320
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 295
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    move v0, v1

    .line 296
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/b;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 297
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 298
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 299
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 301
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 305
    iget-boolean v2, p0, Lcom/google/android/gms/people/accountswitcherview/b;->o:Z

    if-eqz v2, :cond_2

    .line 306
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/b;->getHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1, p1, v0}, Landroid/widget/FrameLayout;->measure(II)V

    .line 311
    :cond_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getMeasuredHeight()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 305
    goto :goto_2

    .line 296
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    .line 335
    :goto_0
    if-nez p4, :cond_1

    cmpg-float v1, p3, v2

    if-gez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gez v1, :cond_1

    .line 337
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    neg-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    .line 338
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    .line 339
    const/4 v0, 0x1

    .line 351
    :goto_1
    return v0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 340
    :cond_1
    if-eqz p4, :cond_3

    cmpl-float v1, p3, v2

    if-lez v1, :cond_3

    .line 341
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_2

    .line 345
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    .line 347
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_3

    .line 348
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    .line 351
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 364
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    .line 365
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 396
    :cond_0
    :goto_1
    return-void

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 370
    :cond_2
    if-lez p3, :cond_6

    .line 372
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-lez v1, :cond_6

    .line 374
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v1, p3, :cond_3

    .line 375
    neg-int v1, p3

    .line 381
    :goto_2
    if-eqz v1, :cond_0

    .line 383
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_4

    .line 384
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    .line 388
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_5

    .line 389
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    .line 393
    :goto_4
    aput v2, p4, v2

    .line 394
    aput v1, p4, v5

    goto :goto_1

    .line 377
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    goto :goto_2

    .line 386
    :cond_4
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    goto :goto_3

    .line 391
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 417
    const/4 v0, 0x0

    .line 418
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->k:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->l:Landroid/view/ViewGroup;

    .line 419
    :goto_0
    if-gez p5, :cond_5

    .line 420
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gez v2, :cond_5

    .line 421
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    if-gt p5, v0, :cond_0

    .line 422
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result p5

    .line 428
    :cond_0
    :goto_1
    if-eqz p5, :cond_1

    .line 430
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    if-lez v0, :cond_3

    .line 431
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    .line 435
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v0, v2, :cond_4

    .line 436
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    .line 441
    :cond_1
    :goto_3
    return-void

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/b;->h:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 433
    :cond_3
    neg-int v0, p5

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 438
    :cond_4
    neg-int v0, p5

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/b;->b(I)V

    goto :goto_3

    :cond_5
    move p5, v0

    goto :goto_1
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->o:Z

    return v0
.end method

.method public final setPadding(IIII)V
    .locals 1

    .prologue
    .line 822
    .line 823
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->s:Z

    if-eqz v0, :cond_0

    .line 824
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/accountswitcherview/b;->c(I)V

    .line 825
    const/4 p2, 0x0

    .line 827
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 828
    return-void
.end method

.method public final setPaddingRelative(IIII)V
    .locals 1

    .prologue
    .line 832
    .line 833
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/b;->s:Z

    if-eqz v0, :cond_0

    .line 834
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/accountswitcherview/b;->c(I)V

    .line 835
    const/4 p2, 0x0

    .line 837
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->setPaddingRelative(IIII)V

    .line 838
    return-void
.end method
