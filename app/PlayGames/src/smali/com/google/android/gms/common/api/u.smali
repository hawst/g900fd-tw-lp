.class public final Lcom/google/android/gms/common/api/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Map;

.field private d:I

.field private e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private final g:Landroid/content/Context;

.field private h:Landroid/support/v4/app/ab;

.field private i:I

.field private j:Lcom/google/android/gms/common/api/w;

.field private k:Landroid/os/Looper;

.field private final l:Ljava/util/Set;

.field private final m:Ljava/util/Set;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->b:Ljava/util/Set;

    .line 413
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    .line 415
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/u;->i:I

    .line 419
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->l:Ljava/util/Set;

    .line 421
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->m:Ljava/util/Set;

    .line 430
    iput-object p1, p0, Lcom/google/android/gms/common/api/u;->g:Landroid/content/Context;

    .line 431
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->k:Landroid/os/Looper;

    .line 432
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/u;->f:Ljava/lang/String;

    .line 433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/w;)V
    .locals 1

    .prologue
    .line 446
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/u;-><init>(Landroid/content/Context;)V

    .line 447
    const-string v0, "Must provide a connected listener"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->l:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 449
    const-string v0, "Must provide a connection failed listener"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->m:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 452
    return-void
.end method

.method private b()Lcom/google/android/gms/common/internal/ClientSettings;
    .locals 6

    .prologue
    .line 663
    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v1, p0, Lcom/google/android/gms/common/api/u;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/api/u;->b:Ljava/util/Set;

    iget v3, p0, Lcom/google/android/gms/common/api/u;->d:I

    iget-object v4, p0, Lcom/google/android/gms/common/api/u;->e:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gms/common/api/u;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/t;
    .locals 9

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 691
    iget v0, p0, Lcom/google/android/gms/common/api/u;->i:I

    if-ltz v0, :cond_2

    .line 692
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->h:Landroid/support/v4/app/ab;

    invoke-static {v0}, Lcom/google/android/gms/common/api/aq;->a(Landroid/support/v4/app/ab;)Lcom/google/android/gms/common/api/aq;

    move-result-object v8

    iget v0, p0, Lcom/google/android/gms/common/api/u;->i:I

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/aq;->b(I)Lcom/google/android/gms/common/api/t;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/x;

    iget-object v1, p0, Lcom/google/android/gms/common/api/u;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/u;->k:Landroid/os/Looper;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/u;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/gms/common/api/u;->l:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gms/common/api/u;->m:Ljava/util/Set;

    iget v7, p0, Lcom/google/android/gms/common/api/u;->i:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/api/x;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    :cond_0
    iget v1, p0, Lcom/google/android/gms/common/api/u;->i:I

    iget-object v2, p0, Lcom/google/android/gms/common/api/u;->j:Lcom/google/android/gms/common/api/w;

    invoke-virtual {v8, v1, v0, v2}, Lcom/google/android/gms/common/api/aq;->a(ILcom/google/android/gms/common/api/t;Lcom/google/android/gms/common/api/w;)V

    .line 695
    :goto_1
    return-object v0

    .line 688
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 695
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/x;

    iget-object v1, p0, Lcom/google/android/gms/common/api/u;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/u;->k:Landroid/os/Looper;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/u;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/gms/common/api/u;->l:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gms/common/api/u;->m:Ljava/util/Set;

    const/4 v7, -0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/api/x;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/u;
    .locals 5

    .prologue
    .line 566
    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    iget-object v2, p1, Lcom/google/android/gms/common/api/a;->c:Ljava/util/ArrayList;

    .line 569
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 570
    iget-object v4, p0, Lcom/google/android/gms/common/api/u;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 572
    :cond_0
    return-object p0
.end method
