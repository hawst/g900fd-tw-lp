.class public final Lcom/google/android/gms/games/ui/e/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/Player;

.field public b:Ljava/util/ArrayList;

.field public c:Z

.field public d:[Ljava/lang/String;

.field public e:Z

.field public f:Ljava/util/ArrayList;

.field public g:Z

.field public final h:Ljava/lang/String;

.field public final i:Lcom/google/android/gms/common/api/t;

.field final j:Lcom/google/android/gms/games/ui/e/y;

.field private final k:Landroid/support/v4/app/Fragment;

.field private final l:Landroid/app/Activity;

.field private final m:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/e/y;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/v;->a:Lcom/google/android/gms/games/Player;

    .line 115
    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->j:Lcom/google/android/gms/games/ui/e/y;

    .line 116
    iput-object p3, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    .line 117
    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->l:Landroid/app/Activity;

    .line 118
    iput-object p4, p0, Lcom/google/android/gms/games/ui/e/v;->i:Lcom/google/android/gms/common/api/t;

    .line 119
    iput-object p5, p0, Lcom/google/android/gms/games/ui/e/v;->h:Ljava/lang/String;

    .line 120
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/e/v;->m:I

    .line 121
    return-void
.end method


# virtual methods
.method final a()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/v;->g:Z

    if-eqz v0, :cond_1

    .line 348
    const-string v0, "ManageCirclesHelper"

    const-string v1, "computeBelongingCircles: Canceled! Bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/v;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/v;->e:Z

    if-eqz v0, :cond_0

    .line 360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->f:Ljava/util/ArrayList;

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->d:[Ljava/lang/String;

    array-length v4, v0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_4

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->d:[Ljava/lang/String;

    aget-object v5, v0, v3

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_3

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 368
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 369
    iget-object v7, p0, Lcom/google/android/gms/games/ui/e/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 363
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 375
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/v;->g:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/v;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/e/v;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/e/v;->f:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    iget v2, p0, Lcom/google/android/gms/games/ui/e/v;->m:I

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->l:Landroid/app/Activity;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->l:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/v;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/e/v;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/e/v;->f:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/v;->l:Landroid/app/Activity;

    iget v2, p0, Lcom/google/android/gms/games/ui/e/v;->m:I

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mFragment and mActivity cannot both be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final b()V
    .locals 3

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->k:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/ab;

    move-result-object v0

    :goto_0
    sget v1, Lcom/google/android/gms/l;->aY:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 406
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/v;->l:Landroid/app/Activity;

    goto :goto_0
.end method
