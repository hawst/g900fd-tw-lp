.class public Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;-><init>(I)V

    .line 38
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;->a:I

    .line 34
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/realtime/internal/b;->a(Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;Landroid/os/Parcel;)V

    .line 28
    return-void
.end method
