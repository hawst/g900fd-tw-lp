.class public final Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;
.super Lcom/google/android/gms/games/ui/destination/m;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/aa;
.implements Lcom/google/android/gms/games/ui/c/a/m;
.implements Lcom/google/android/gms/games/ui/common/a/k;
.implements Lcom/google/android/gms/games/ui/common/a/l;
.implements Lcom/google/android/gms/games/ui/common/a/n;
.implements Lcom/google/android/play/headerlist/m;


# static fields
.field private static final B:Ljava/lang/String;

.field private static final C:Landroid/content/IntentFilter;

.field private static final D:Landroid/content/IntentFilter;

.field private static final E:Lcom/google/android/gms/games/ui/e/ai;

.field private static final F:Lcom/google/android/gms/games/ui/e/ai;

.field private static final G:Lcom/google/android/gms/games/ui/e/ai;

.field private static final H:Lcom/google/android/gms/games/ui/e/ai;

.field private static final I:Lcom/google/android/gms/games/ui/e/ai;

.field private static final J:Lcom/google/android/gms/games/ui/e/ah;

.field private static final K:[I


# instance fields
.field private final L:Lcom/google/android/gms/games/ui/destination/games/t;

.field private M:Landroid/view/Menu;

.field private N:Z

.field private O:Lcom/google/android/gms/games/Game;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:I

.field private S:I

.field private T:Lcom/google/android/gms/games/ui/destination/games/x;

.field private U:Z

.field private V:Lcom/google/android/gms/games/ui/e/s;

.field private W:Landroid/view/View;

.field private X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private Y:Landroid/widget/ImageView;

.field private Z:Landroid/view/View;

.field private aa:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private ab:Landroid/view/View;

.field private ac:Landroid/view/View;

.field private ad:Landroid/widget/Button;

.field private ae:Landroid/view/View;

.field private af:Landroid/view/View;

.field private ag:Landroid/view/View;

.field private ah:Landroid/widget/TextView;

.field private ai:Landroid/widget/TextView;

.field private aj:Lcom/google/android/gms/games/ui/common/a/b;

.field private ak:Z

.field private al:Z

.field private am:I

.field private an:Z

.field private ao:Z

.field private ap:I

.field private aq:J

.field private ar:J

.field private as:J

.field private at:Ljava/util/ArrayList;

.field private au:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 140
    const-class v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B:Ljava/lang/String;

    .line 149
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->C:Landroid/content/IntentFilter;

    .line 151
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->D:Landroid/content/IntentFilter;

    .line 154
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->C:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->D:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/google/android/gms/games/ui/e/ai;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/e;

    const v2, 0x7f0f005f

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->E:Lcom/google/android/gms/games/ui/e/ai;

    .line 163
    new-instance v0, Lcom/google/android/gms/games/ui/e/ai;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/k;

    const v2, 0x7f0f0061

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->F:Lcom/google/android/gms/games/ui/e/ai;

    .line 167
    new-instance v0, Lcom/google/android/gms/games/ui/e/ai;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/z;

    const v2, 0x7f0f006e

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->G:Lcom/google/android/gms/games/ui/e/ai;

    .line 171
    new-instance v0, Lcom/google/android/gms/games/ui/e/ai;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/y;

    const v2, 0x7f0f006b

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->H:Lcom/google/android/gms/games/ui/e/ai;

    .line 175
    new-instance v0, Lcom/google/android/gms/games/ui/e/ai;

    const-class v1, Lcom/google/android/gms/games/ui/destination/games/aa;

    const v2, 0x7f0f0071

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->I:Lcom/google/android/gms/games/ui/e/ai;

    .line 181
    new-instance v0, Lcom/google/android/gms/games/ui/e/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/e/ah;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->J:Lcom/google/android/gms/games/ui/e/ah;

    .line 197
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->K:[I

    return-void

    :array_0
    .array-data 4
        0x6
        0x4
        0x3
        0x2
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 266
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->J:Lcom/google/android/gms/games/ui/e/ah;

    const v1, 0x7f04002f

    const v2, 0x7f11000b

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/m;-><init>(Lcom/google/android/gms/games/ui/e/ah;II)V

    .line 213
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    .line 244
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ak:Z

    .line 245
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->al:Z

    .line 252
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->an:Z

    .line 253
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ao:Z

    .line 260
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as:J

    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->at:Ljava/util/ArrayList;

    .line 267
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/t;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    .line 268
    return-void
.end method

.method static synthetic A(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/s;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->V:Lcom/google/android/gms/games/ui/e/s;

    return-object v0
.end method

.method static synthetic B(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic C(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->av()V

    return-void
.end method

.method static synthetic D(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aw()V

    return-void
.end method

.method static synthetic E(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic F(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic G(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic H(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic I(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic J(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic K(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->R:I

    return v0
.end method

.method static synthetic L(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic M(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic N(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->S:I

    return v0
.end method

.method static synthetic O(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/e/b;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A:Lcom/google/android/gms/games/ui/e/b;

    return-object v0
.end method

.method static synthetic P(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/res/Resources;)I
    .locals 3

    .prologue
    .line 1379
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1380
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1381
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1382
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    const/high16 v2, 0x43fa0000    # 500.0f

    mul-float/2addr v0, v2

    const/high16 v2, 0x44800000    # 1024.0f

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 1386
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v2, v1, :cond_3

    const/4 v1, 0x1

    .line 1387
    :goto_0
    if-eqz v1, :cond_0

    .line 1388
    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1392
    :cond_0
    const v1, 0x7f09000c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 1393
    if-eqz v1, :cond_1

    .line 1394
    int-to-float v0, v0

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1399
    :cond_1
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1400
    int-to-float v0, v0

    const v1, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1403
    :cond_2
    return v0

    .line 1386
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)I
    .locals 0

    .prologue
    .line 135
    iput p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->R:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;J)Landroid/animation/Animator;
    .locals 7

    .prologue
    const/high16 v5, 0x40800000    # 4.0f

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getWidth()I

    move-result v1

    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/Context;)I

    move-result v2

    div-int/lit8 v3, v2, 0x2

    div-int/lit8 v4, v1, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v2, v3

    mul-int/2addr v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    mul-int/2addr v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0040

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/2addr v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v3, v4, v0, v2, v1}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    return-object v0
.end method

.method private static a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 969
    const v0, 0x7f040030

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;JLjava/util/ArrayList;)Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 135
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(ZJ[I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;J[I)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(ZJ[I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(ZJ[I)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 916
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v2, 0x10a0000

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 918
    invoke-virtual {v2, p2, p3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 920
    array-length v3, p4

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, p4, v0

    .line 921
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 922
    if-eqz v4, :cond_0

    .line 923
    invoke-virtual {v4, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 924
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 920
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 927
    :cond_1
    return-object v2
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/Game;
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/images/internal/LoadingImageView;)V
    .locals 2

    .prologue
    .line 980
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 982
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 983
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 984
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 985
    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 986
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/graphics/drawable/Drawable;J)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 135
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v1, v0, v3

    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    long-to-int v0, p2

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Z)Z
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    return p1
.end method

.method static synthetic al()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic am()Lcom/google/android/gms/games/ui/e/ai;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->E:Lcom/google/android/gms/games/ui/e/ai;

    return-object v0
.end method

.method static synthetic an()Lcom/google/android/gms/games/ui/e/ai;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->F:Lcom/google/android/gms/games/ui/e/ai;

    return-object v0
.end method

.method static synthetic ao()Lcom/google/android/gms/games/ui/e/ai;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->G:Lcom/google/android/gms/games/ui/e/ai;

    return-object v0
.end method

.method static synthetic ap()Lcom/google/android/gms/games/ui/e/ai;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->H:Lcom/google/android/gms/games/ui/e/ai;

    return-object v0
.end method

.method static synthetic aq()Lcom/google/android/gms/games/ui/e/ai;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->I:Lcom/google/android/gms/games/ui/e/ai;

    return-object v0
.end method

.method private ar()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 417
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 422
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ak:Z

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    const-string v1, "playFab"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/c/i;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAllowEnterTransitionOverlap(Z)V

    .line 432
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    if-ne v2, v0, :cond_2

    .line 433
    const-wide/16 v0, 0x1c2

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq:J

    .line 434
    const-wide/16 v0, 0x15e

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ar:J

    .line 440
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 435
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    if-ne v0, v1, :cond_1

    .line 436
    sget-wide v0, Lcom/google/android/gms/games/ui/d/a;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq:J

    .line 437
    sget-wide v0, Lcom/google/android/gms/games/ui/d/a;->b:J

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ar:J

    goto :goto_1
.end method

.method private as()Lcom/google/android/gms/common/images/internal/LoadingImageView;
    .locals 2

    .prologue
    .line 974
    new-instance v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;-><init>(Landroid/content/Context;)V

    .line 975
    const v1, 0x7f0c0024

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setId(I)V

    .line 976
    return-object v0
.end method

.method private at()Landroid/view/View;
    .locals 5

    .prologue
    .line 989
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 990
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 991
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    const v4, 0x7f0a0045

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const v4, 0x106000d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v2, v3

    .line 993
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v1, v3, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 995
    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 996
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1000
    :goto_0
    const v1, 0x7f0c0026

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 1001
    return-object v0

    .line 998
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private au()V
    .locals 2

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->M:Landroid/view/Menu;

    if-nez v0, :cond_0

    .line 1100
    :goto_0
    return-void

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->M:Landroid/view/Menu;

    const v1, 0x7f0c028b

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1095
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    if-eqz v1, :cond_1

    .line 1096
    const v1, 0x7f0f0070

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1098
    :cond_1
    const v1, 0x7f0f006f

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private av()V
    .locals 4

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    if-eqz v0, :cond_5

    .line 1184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 1185
    const/4 v0, 0x0

    .line 1186
    if-eqz v1, :cond_0

    .line 1187
    invoke-interface {v1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->m()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    .line 1190
    :cond_0
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1192
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1193
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Ljava/lang/CharSequence;)V

    .line 1194
    if-eqz v0, :cond_1

    .line 1195
    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Ljava/lang/CharSequence;)V

    .line 1199
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v1, :cond_2

    .line 1200
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v2

    .line 1201
    if-eqz v0, :cond_6

    .line 1202
    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->f()Landroid/net/Uri;

    move-result-object v1

    .line 1203
    if-eqz v1, :cond_6

    .line 1207
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v3, 0x7f02015c

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 1209
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aa:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v1, :cond_3

    .line 1210
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aa:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v2

    const v3, 0x7f020090

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 1213
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ah:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 1214
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ah:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1216
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 1217
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1219
    if-eqz v0, :cond_5

    .line 1220
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai:Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1224
    :cond_5
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_0
.end method

.method private aw()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    if-eqz v0, :cond_1

    .line 1228
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ak:Z

    if-eqz v0, :cond_0

    .line 1229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1278
    :goto_0
    return-void

    .line 1236
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 1239
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    if-eqz v1, :cond_2

    .line 1241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1242
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1243
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1245
    :cond_2
    if-nez v0, :cond_3

    .line 1248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1250
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1252
    :cond_3
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->e()I

    move-result v1

    if-nez v1, :cond_6

    .line 1254
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1255
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1256
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1257
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1260
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1261
    const v0, 0x7f0f006d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1270
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1263
    :cond_5
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->j()Ljava/lang/String;

    move-result-object v0

    .line 1264
    if-eqz v0, :cond_4

    .line 1265
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1266
    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1272
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1273
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1274
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aa:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1005
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1006
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1007
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1009
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;I)V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(IZ)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1137
    const v0, 0x7f0c010c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aa:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 1138
    const v0, 0x7f0c0111

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    .line 1140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1144
    const v0, 0x7f0c0112

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ae:Landroid/view/View;

    .line 1145
    const v0, 0x7f0c0113

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->af:Landroid/view/View;

    .line 1146
    const v0, 0x7f0c0114

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ag:Landroid/view/View;

    .line 1147
    const v0, 0x7f0c010e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ah:Landroid/widget/TextView;

    .line 1148
    const v0, 0x7f0c010f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai:Landroid/widget/TextView;

    .line 1150
    const v0, 0x7f0c010b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1151
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1152
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1153
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->h()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1154
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1158
    :goto_0
    return-void

    .line 1156
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/common/images/internal/LoadingImageView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    return-object v0
.end method

.method private d(I)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x7f0b0078

    const/4 v2, 0x0

    .line 2025
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2026
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/q;

    .line 2030
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v1

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    .line 2035
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v1, v5

    const v5, 0x7f0b0077

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v1, v5

    .line 2041
    if-gez v1, :cond_0

    move v1, v2

    .line 2047
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->al:Z

    if-eqz v5, :cond_1

    .line 2048
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 2051
    :cond_1
    sub-int v1, v4, p1

    .line 2052
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/support/v4/widget/q;->setMargins(IIII)V

    .line 2053
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2055
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2057
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    .line 2059
    if-gez v1, :cond_3

    .line 2060
    int-to-float v1, v1

    add-float/2addr v1, v0

    div-float v0, v1, v0

    .line 2061
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    cmpg-float v1, v0, v7

    if-gez v1, :cond_2

    .line 2062
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2068
    :cond_2
    :goto_0
    return-void

    .line 2065
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ar:J

    return-wide v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ak:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq:J

    return-wide v0
.end method

.method static synthetic m(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->au:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->at:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    instance-of v2, v0, Lcom/google/android/gms/games/ui/p;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/gms/games/ui/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/p;->ai()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->an:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->an:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ao:Z

    return v0
.end method

.method static synthetic v(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ao:Z

    return v0
.end method

.method static synthetic w(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    return v0
.end method

.method static synthetic y(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->au()V

    return-void
.end method

.method static synthetic z(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    return v0
.end method


# virtual methods
.method protected final E()V
    .locals 3

    .prologue
    .line 1067
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LAUNCHED_VIA_API"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1070
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.MAIN_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1071
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1072
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 1073
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->finish()V

    .line 1078
    :goto_0
    return-void

    .line 1076
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/m;->E()V

    goto :goto_0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 1344
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    return v0
.end method

.method public final S()Z
    .locals 1

    .prologue
    .line 1349
    const/4 v0, 0x1

    return v0
.end method

.method public final T()F
    .locals 1

    .prologue
    .line 1425
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 1409
    const/4 v0, 0x0

    return v0
.end method

.method public final W()I
    .locals 1

    .prologue
    .line 1359
    const/4 v0, 0x0

    return v0
.end method

.method public final X()I
    .locals 1

    .prologue
    .line 1415
    const/4 v0, 0x1

    return v0
.end method

.method public final Z()I
    .locals 1

    .prologue
    .line 1420
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)I
    .locals 4

    .prologue
    const v3, 0x7f0b0076

    .line 1364
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1365
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v1

    .line 1366
    const/16 v2, 0xb

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1367
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 1371
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1162
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->a(Landroid/os/Bundle;)V

    .line 1164
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q:Ljava/lang/String;

    .line 1167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ad:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->a()V

    .line 1170
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2071
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->a(Landroid/support/v4/app/Fragment;)V

    .line 2072
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->au:Z

    if-eqz v0, :cond_1

    .line 2074
    instance-of v0, p1, Lcom/google/android/gms/games/ui/p;

    if-eqz v0, :cond_0

    .line 2075
    check-cast p1, Lcom/google/android/gms/games/ui/p;

    .line 2077
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/p;->ai()V

    .line 2083
    :cond_0
    :goto_0
    return-void

    .line 2081
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->at:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final aa()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1354
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public final af()Lcom/google/android/gms/games/ui/c/a/l;
    .locals 1

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aj:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method

.method final ah()Lcom/google/android/gms/games/internal/game/ExtendedGame;
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    return-object v0
.end method

.method final ai()Lcom/google/android/gms/games/ui/destination/games/t;
    .locals 1

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    return-object v0
.end method

.method final aj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final ak()V
    .locals 8

    .prologue
    const/high16 v7, 0x437f0000    # 255.0f

    .line 1938
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1939
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->h()I

    move-result v0

    .line 1940
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g()I

    move-result v2

    .line 1941
    const v3, 0x7f0b0076

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1943
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/Context;)I

    move-result v4

    sub-int/2addr v4, v0

    sub-int/2addr v4, v2

    sub-int/2addr v4, v3

    .line 1945
    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v5

    int-to-float v0, v0

    sub-float v0, v5, v0

    int-to-float v2, v2

    sub-float/2addr v0, v2

    int-to-float v2, v3

    sub-float/2addr v0, v2

    .line 1948
    int-to-float v2, v4

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 1949
    int-to-float v0, v4

    .line 1952
    :cond_0
    float-to-int v2, v0

    sub-int v2, v4, v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->d(I)V

    .line 1956
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 1957
    const/high16 v3, -0x1000000

    iget v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    const v6, 0x3e19999a    # 0.15f

    invoke-static {v3, v5, v6}, Lcom/google/android/gms/games/ui/e/aj;->a(IIF)I

    move-result v3

    .line 1958
    const v5, 0x7f0a00d0

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1959
    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-lez v5, :cond_2

    .line 1960
    int-to-float v4, v4

    div-float/2addr v0, v4

    mul-float/2addr v0, v7

    sub-float v0, v7, v0

    .line 1961
    float-to-int v4, v0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(I)V

    .line 1964
    float-to-int v0, v0

    .line 1965
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1966
    int-to-float v0, v0

    div-float/2addr v0, v7

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(IIF)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1975
    :cond_1
    :goto_0
    return-void

    .line 1970
    :cond_2
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(I)V

    .line 1971
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1972
    invoke-virtual {v2, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 932
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 933
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 935
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Landroid/view/View;)V

    .line 936
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    .line 1507
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aw()V

    .line 1509
    :cond_0
    return-void
.end method

.method public final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 941
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 945
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 946
    const v0, 0x7f0c0022

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setId(I)V

    .line 947
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTransitionGroup(Z)V

    .line 950
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    .line 951
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    const v2, 0x7f0c0023

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 952
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 953
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 954
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 955
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 956
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 958
    const/4 v2, -0x1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 959
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 960
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->at()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    .line 961
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 962
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 963
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;)V

    .line 964
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Landroid/view/View;)V

    .line 965
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->av()V

    .line 966
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1514
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    .line 1515
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    .line 1516
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aw()V

    .line 1518
    :cond_0
    return-void
.end method

.method public final c()[I
    .locals 1

    .prologue
    .line 1320
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->K:[I

    return-object v0
.end method

.method protected final k()I
    .locals 1

    .prologue
    .line 272
    const v0, 0x7f040031

    return v0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 834
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    if-ne v0, v1, :cond_0

    .line 838
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024d

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 847
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/m;->onBackPressed()V

    .line 848
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1283
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1304
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->onClick(Landroid/view/View;)V

    .line 1307
    :goto_0
    return-void

    .line 1285
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/g;->u:Lcom/google/android/gms/games/app/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;IZ)V

    .line 1287
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1290
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1291
    sget-object v0, Lcom/google/android/gms/games/ui/l;->p:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1292
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B:Ljava/lang/String;

    const-string v2, "OnClick: Launch the finsky buy/install process"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Q:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/n;->p:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    const-string v1, "Cannot try to install a game while already waiting for another install"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    const-string v0, "currentAccountName cannot be null"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v0, "game package to install cannot be null"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/n;->q:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/internal/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/n;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1296
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GPG_gameDetail_about"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1300
    :cond_2
    sget-object v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->B:Ljava/lang/String;

    const-string v1, "onClick: mCurrentAccountName is null!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1283
    :sswitch_data_0
    .sparse-switch
        0x7f0c0111 -> :sswitch_1
        0x7f0c0162 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v9, 0xb

    const v7, 0x7f0c0115

    const/4 v6, -0x1

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 282
    const-string v0, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a00d3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    .line 285
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->onCreate(Landroid/os/Bundle;)V

    .line 288
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/common/a/b;-><init>(Lcom/google/android/gms/games/ui/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aj:Lcom/google/android/gms/games/ui/common/a/b;

    .line 290
    invoke-static {v9}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d()I

    move-result v0

    .line 291
    :goto_0
    new-instance v2, Lcom/google/android/gms/games/ui/e/s;

    invoke-virtual {p0, v7}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-direct {v2, v4, v5, v0}, Lcom/google/android/gms/games/ui/e/s;-><init>(Landroid/view/View;Lcom/google/android/gms/games/ui/e/u;I)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->V:Lcom/google/android/gms/games/ui/e/s;

    .line 296
    if-eqz p1, :cond_4

    .line 297
    const-string v0, "savedStateGame"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    .line 298
    const-string v0, "savedStateGameId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    .line 299
    const-string v0, "savedStateExtendedGame"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 300
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iput-object v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 301
    const-string v0, "savedStateIsMuted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    .line 303
    const-string v0, "savedStateWaitingForInstall"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    .line 306
    const-string v0, "savedStateSelectedTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->R:I

    .line 334
    :cond_0
    :goto_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Ljava/lang/CharSequence;)V

    .line 342
    :cond_1
    invoke-static {v9}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 348
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 349
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 350
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v2

    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->as()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v4

    .line 352
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->at()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    .line 353
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->av()V

    .line 355
    iput-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->X:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 361
    invoke-virtual {p0, v7}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 362
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 363
    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 364
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 365
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 366
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 367
    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 368
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 369
    invoke-direct {p0, v4}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;)V

    .line 370
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->W:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->b(Landroid/view/View;)V

    .line 371
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Landroid/view/View;)V

    .line 374
    const v0, 0x7f0c010a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 375
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 377
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v2

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 378
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 383
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 384
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 385
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 388
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->A()V

    .line 397
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(I)V

    .line 401
    const v0, 0x7f0c010a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 402
    const v0, 0x7f0c010b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 404
    const v0, 0x7f0c00fb

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040057

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    const v2, 0x7f0c0162

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    invoke-static {v1}, Lcom/google/android/gms/games/app/b;->a(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0098

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    invoke-virtual {v4, v8}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    invoke-virtual {v4, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    invoke-static {v4, v2, v8}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0c027d

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ab:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/widget/DrawerLayout;->addView(Landroid/view/View;I)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->d(I)V

    .line 406
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    if-ne v8, v0, :cond_9

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ar()V

    .line 408
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v0, 0x7f0c010c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024e

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v7}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v6, 0x7f0c024b

    invoke-virtual {v2, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    invoke-static {p0, v5, v2, v8, v8}, Lcom/google/android/play/c/g;->a(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    invoke-static {p0, v5, v2, v8, v3}, Lcom/google/android/play/c/g;->a(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    new-instance v3, Lcom/google/android/gms/games/ui/destination/games/l;

    invoke-direct {v3, p0, v2, v4, v1}, Lcom/google/android/gms/games/ui/destination/games/l;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;Landroid/content/res/Resources;Landroid/view/ViewGroup;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/ui/destination/games/n;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;)V

    invoke-virtual {v6, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 413
    :cond_2
    :goto_4
    return-void

    :cond_3
    move v0, v3

    .line 290
    goto/16 :goto_0

    .line 308
    :cond_4
    const-string v0, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 309
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iput-object v0, v2, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 310
    if-nez v0, :cond_5

    .line 312
    const-string v0, "com.google.android.gms.games.GAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    .line 317
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    if-nez v0, :cond_6

    .line 318
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    .line 323
    :goto_6
    iput v6, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->R:I

    .line 324
    const-string v0, "com.google.android.gms.games.TAB"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->S:I

    .line 325
    iput v6, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    .line 326
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    goto/16 :goto_1

    .line 314
    :cond_5
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    goto :goto_5

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    goto :goto_6

    .line 392
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/m;)V

    goto/16 :goto_2

    .line 404
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ac:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 409
    :cond_9
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ap:I

    if-ne v0, v1, :cond_2

    .line 410
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ar()V

    .line 411
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v0, 0x7f0c0107

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p0, v5}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Landroid/content/res/Resources;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    const v2, 0x7f0b0040

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->Y:Landroid/widget/ImageView;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/content/Context;Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aq:J

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024e

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {p0, v7}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024b

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->r:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0c024f

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "tab_bar"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/o;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/destination/games/o;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/res/Resources;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/games/p;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    invoke-virtual {v6, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    goto/16 :goto_4
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1082
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1083
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->M:Landroid/view/Menu;

    .line 1084
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->au()V

    .line 1085
    const/4 v0, 0x1

    return v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->f_()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    .line 1062
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/m;->onDestroy()V

    .line 1063
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 1106
    const v3, 0x7f0c028b

    if-ne v2, v3, :cond_3

    .line 1107
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_2

    sget-object v3, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    invoke-interface {v3, v2, v4}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    :goto_0
    new-instance v3, Lcom/google/android/gms/games/ui/destination/games/w;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/games/ui/destination/games/w;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 1120
    :cond_1
    :goto_1
    return v1

    .line 1107
    :cond_2
    sget-object v3, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    invoke-interface {v3, v2, v4}, Lcom/google/android/gms/games/l;->b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    goto :goto_0

    .line 1109
    :cond_3
    const v3, 0x7f0c028a

    if-ne v2, v3, :cond_5

    .line 1110
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    if-eqz v2, :cond_4

    .line 1111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1115
    :cond_4
    const v2, 0x7f0f00dc

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1116
    invoke-static {p0, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1120
    :cond_5
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 1036
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/m;->onPause()V

    .line 1038
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->R:I

    .line 1040
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    if-eqz v0, :cond_0

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1042
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    .line 1044
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 1013
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/m;->onResume()V

    .line 1017
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    if-nez v0, :cond_0

    .line 1018
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/x;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/destination/games/x;-><init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->T:Lcom/google/android/gms/games/ui/destination/games/x;

    sget-object v1, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->D:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1022
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    if-eqz v0, :cond_1

    .line 1025
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/e/z;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 1027
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iput-boolean v0, v1, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    .line 1030
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->au()V

    .line 1031
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->b()V

    .line 1032
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1048
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1050
    const-string v0, "savedStateGame"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->O:Lcom/google/android/gms/games/Game;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1051
    const-string v0, "savedStateGameId"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const-string v0, "savedStateExtendedGame"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->L:Lcom/google/android/gms/games/ui/destination/games/t;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/destination/games/t;->b:Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1053
    const-string v0, "savedStateIsMuted"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1055
    const-string v0, "savedStateWaitingForInstall"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->U:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1056
    const-string v0, "savedStateSelectedTab"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/m;->z:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1057
    return-void
.end method

.method public final r_()Z
    .locals 1

    .prologue
    .line 1330
    const/4 v0, 0x0

    return v0
.end method

.method protected final z()I
    .locals 1

    .prologue
    .line 1335
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->am:I

    return v0
.end method

.method public final z_()Lcom/google/android/gms/games/ui/common/a/j;
    .locals 1

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->aj:Lcom/google/android/gms/games/ui/common/a/b;

    return-object v0
.end method
