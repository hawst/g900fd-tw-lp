.class public final Lcom/google/android/gms/games/ui/e/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/games/ui/n;

.field public b:Landroid/support/v7/widget/SearchView;

.field c:Ljava/lang/String;

.field public d:Z

.field e:Landroid/os/Handler;

.field f:Z

.field public g:Z

.field protected h:Ljava/lang/Runnable;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/gms/games/ui/e/af;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/af;)V
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f0f0198

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/e/ab;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/af;I)V

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/af;I)V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lcom/google/android/gms/games/ui/e/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/e/ac;-><init>(Lcom/google/android/gms/games/ui/e/ab;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->h:Ljava/lang/Runnable;

    .line 118
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    .line 119
    iput-object p2, p0, Lcom/google/android/gms/games/ui/e/ab;->k:Lcom/google/android/gms/games/ui/e/af;

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->e:Landroid/os/Handler;

    .line 121
    iput p3, p0, Lcom/google/android/gms/games/ui/e/ab;->i:I

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/e/ab;->f:Z

    .line 123
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/e/ab;->d:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 160
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 372
    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    .line 378
    :goto_0
    return-void

    .line 376
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNewIntent: Unexpected intent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/n;->setTitle(Ljava/lang/CharSequence;)V

    .line 136
    if-eqz p1, :cond_0

    .line 140
    const-string v0, "savedStatePreviousQuery"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    .line 151
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->k:Lcom/google/android/gms/games/ui/e/af;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/e/af;->a(Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 178
    new-instance v0, Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->x_()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/support/v7/app/a;->a(II)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->x_()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Landroid/view/View;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    sget v1, Lcom/google/android/gms/g;->bX:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 186
    if-eqz v0, :cond_0

    .line 187
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/d;->z:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_2

    const-string v0, "SearchHelper"

    const-string v1, "got passed a null searchView!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 195
    :cond_2
    new-instance v1, Lcom/google/android/gms/games/ui/e/ad;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/e/ad;-><init>(Lcom/google/android/gms/games/ui/e/ab;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Landroid/support/v7/widget/dm;)V

    new-instance v1, Lcom/google/android/gms/games/ui/e/ae;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/ui/e/ae;-><init>(Lcom/google/android/gms/games/ui/e/ab;Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Landroid/support/v7/widget/dl;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->e()V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/ui/e/ab;->i:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->c()I

    move-result v1

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/e/ab;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    const-string v1, "search"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/n;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView;->a(Landroid/app/SearchableInfo;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 362
    const-string v0, "savedStatePreviousQuery"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    return-void
.end method

.method protected final c()V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->a:Lcom/google/android/gms/games/ui/n;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/n;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 318
    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 320
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 339
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/ab;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 395
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 396
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/e/ab;->a(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/e/ab;->c()V

    .line 399
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
