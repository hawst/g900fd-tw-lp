.class public final Lcom/google/android/gms/games/ui/common/matches/r;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/ah;
.implements Lcom/google/android/gms/games/ui/ar;
.implements Lcom/google/android/gms/games/ui/common/matches/b;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/common/matches/ac;

.field private an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

.field private ao:Z

.field private ap:I

.field private aq:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/google/android/gms/i;->r:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/r;I)I
    .locals 0

    .prologue
    .line 43
    iput p1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method private at()V
    .locals 5

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    const-string v0, "MultiplayerInboxFrag"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->q()V

    .line 246
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    .line 247
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0

    .line 250
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v4, v1}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private au()V
    .locals 3

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 375
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/r;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/common/matches/ac;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/r;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/n;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/View;)Lcom/google/android/gms/games/ui/e/o;
    .locals 11

    .prologue
    .line 112
    invoke-static {p0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/ax;

    move-result-object v0

    .line 113
    const/4 v10, 0x0

    .line 114
    if-eqz v0, :cond_0

    .line 115
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ax;->a(Landroid/content/Context;)I

    move-result v10

    .line 117
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/e/o;

    const v2, 0x102000a

    sget v3, Lcom/google/android/gms/g;->aP:I

    sget v4, Lcom/google/android/gms/g;->aY:I

    sget v5, Lcom/google/android/gms/g;->bb:I

    sget v6, Lcom/google/android/gms/g;->ao:I

    move-object v1, p1

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/e/o;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/e/q;Lcom/google/android/gms/games/ui/e/p;Lcom/google/android/gms/games/ui/e/r;I)V

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->r()V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->w()V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->v()V

    .line 172
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 101
    sget v0, Lcom/google/android/gms/g;->aH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    .line 103
    sget-object v0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->aq:Ljava/lang/String;

    .line 152
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Lcom/google/android/gms/common/api/t;)V

    .line 161
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->aj()V

    .line 162
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 276
    :cond_0
    if-eqz p1, :cond_3

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/aq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->aq:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/ap;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/r;->b(Z)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/t;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/t;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/c/a;->a(Landroid/support/v4/app/Fragment;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    const-string v2, "confirmationDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->as()V

    goto :goto_0
.end method

.method protected final al()V
    .locals 1

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->at()V

    .line 229
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    if-eqz v0, :cond_0

    .line 230
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/r;->b(Z)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->as()V

    goto :goto_0
.end method

.method public final am()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;)V

    .line 264
    :goto_0
    return-void

    .line 261
    :cond_0
    const-string v0, "onEmptyActionTextClicked - Trying to show popular multiplayer when not in destination app"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final as()V
    .locals 3

    .prologue
    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Z)V

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 364
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->au()V

    .line 365
    return-void
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 307
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Z)V

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 311
    if-eqz p1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->aq:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/ap;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 317
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 318
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Z)V

    .line 321
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-static {v0}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/e/aa;->d(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/s;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/common/matches/s;-><init>(Lcom/google/android/gms/games/ui/common/matches/r;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 356
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->c()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->C_()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->P()Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    const-string v0, "MultiplayerInboxFrag"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->au()V

    goto :goto_0
.end method

.method public final c_(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->r()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ap:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->w()V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->v()V

    .line 184
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/ad;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/v;->a()Lcom/google/android/gms/games/ui/common/matches/u;

    move-result-object v2

    .line 73
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    sget v0, Lcom/google/android/gms/l;->ad:I

    .line 78
    :cond_0
    sget v1, Lcom/google/android/gms/f;->x:I

    sget v3, Lcom/google/android/gms/l;->ae:I

    invoke-virtual {p0, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/r;->a(III)V

    .line 81
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v6, Lcom/google/android/gms/games/ui/ad;

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/ui/common/matches/ac;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;Lcom/google/android/gms/games/ui/common/matches/i;Lcom/google/android/gms/games/ui/common/matches/o;Lcom/google/android/gms/games/ui/ah;Lcom/google/android/gms/games/ui/ad;Lcom/google/android/gms/games/ui/common/matches/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->am:Lcom/google/android/gms/games/ui/common/matches/ac;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/r;->a(Landroid/support/v7/widget/bv;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 94
    :cond_1
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->at()V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->at()V

    .line 222
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->an:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    if-ne p1, v0, :cond_0

    .line 383
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/r;->a(Z)V

    .line 385
    :cond_0
    return-void

    .line 383
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q_()V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/r;->ao:Z

    if-eqz v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/r;->au()V

    .line 143
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->q_()V

    .line 144
    return-void
.end method
