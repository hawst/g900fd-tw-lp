.class final Lcom/google/android/gms/games/ui/destination/games/i;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field final synthetic e:Lcom/google/android/gms/games/ui/destination/games/e;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/games/e;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    .line 307
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 308
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->h:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->g:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->j:Landroid/view/View;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->i:Landroid/view/View;

    return-object p1
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->k:Landroid/view/View;

    return-object p1
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->l:Landroid/view/View;

    return-object p1
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/i;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 320
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/j;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/i;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f04002e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/destination/games/j;-><init>(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 312
    const v0, 0x7f04002e

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 316
    const v0, 0x7f0b00d6

    return v0
.end method
