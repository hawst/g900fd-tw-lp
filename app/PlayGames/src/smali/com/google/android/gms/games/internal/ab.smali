.class final Lcom/google/android/gms/games/internal/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/request/e;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2848
    iput-object p1, p0, Lcom/google/android/gms/games/internal/ab;->a:Lcom/google/android/gms/common/api/Status;

    .line 2849
    iput-object p2, p0, Lcom/google/android/gms/games/internal/ab;->b:Landroid/os/Bundle;

    .line 2850
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2854
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ab;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/games/request/a;
    .locals 3

    .prologue
    .line 2859
    packed-switch p1, :pswitch_data_0

    const-string v0, "RequestType"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNKNOWN_TYPE"

    .line 2860
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/internal/ab;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2861
    const/4 v0, 0x0

    .line 2864
    :goto_1
    return-object v0

    .line 2859
    :pswitch_0
    const-string v0, "GIFT"

    goto :goto_0

    :pswitch_1
    const-string v0, "WISH"

    goto :goto_0

    .line 2863
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/internal/ab;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 2864
    new-instance v1, Lcom/google/android/gms/games/request/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    move-object v0, v1

    goto :goto_1

    .line 2859
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f_()V
    .locals 3

    .prologue
    .line 2869
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ab;->b:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2870
    iget-object v2, p0, Lcom/google/android/gms/games/internal/ab;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 2871
    if-eqz v0, :cond_0

    .line 2872
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    goto :goto_0

    .line 2875
    :cond_1
    return-void
.end method
