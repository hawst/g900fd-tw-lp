.class final Lcom/google/android/gms/games/ui/common/players/f;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/view/View;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/ProgressBar;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 132
    sget v0, Lcom/google/android/gms/g;->bo:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->m:Landroid/view/View;

    .line 134
    sget v0, Lcom/google/android/gms/g;->br:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->n:Landroid/widget/TextView;

    .line 135
    sget v0, Lcom/google/android/gms/g;->bs:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    .line 137
    sget v0, Lcom/google/android/gms/g;->bc:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->p:Landroid/widget/TextView;

    .line 138
    sget v0, Lcom/google/android/gms/g;->bd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->q:Landroid/widget/ProgressBar;

    .line 140
    sget v0, Lcom/google/android/gms/g;->f:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/widget/TextView;F)V

    .line 144
    sget v0, Lcom/google/android/gms/g;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    .line 146
    sget v0, Lcom/google/android/gms/g;->m:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->u:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->v:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/e;->t:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e(I)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->c(Landroid/view/View;)V

    .line 155
    sget v0, Lcom/google/android/gms/g;->w:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    .line 156
    sget v0, Lcom/google/android/gms/g;->A:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->v:Landroid/widget/TextView;

    .line 157
    sget v0, Lcom/google/android/gms/g;->B:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->w:Landroid/widget/TextView;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 164
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/d;

    .line 167
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 169
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v6

    .line 171
    if-eqz v6, :cond_8

    .line 172
    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    .line 174
    :goto_0
    invoke-static {v5, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/res/Resources;I)I

    move-result v1

    .line 175
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/f;->m:Landroid/view/View;

    invoke-virtual {v5, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 180
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v1

    .line 181
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    .line 189
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "avatar"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTransitionName(Ljava/lang/String;)V

    .line 191
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->b(Lcom/google/android/gms/games/ui/common/players/d;)I

    move-result v0

    if-nez v0, :cond_5

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v6, :cond_4

    :goto_2
    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->q:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/f;->p:Landroid/widget/TextView;

    invoke-static {v6, v0, v1, v3}, Lcom/google/android/gms/games/ui/common/players/j;->a(Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Landroid/widget/TextView;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz v2, :cond_2

    invoke-static {v6}, Lcom/google/android/gms/games/ui/common/players/j;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->v:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/f;->w:Landroid/widget/TextView;

    invoke-static {v0, v6, v1, v2}, Lcom/google/android/gms/games/ui/common/players/j;->a(Landroid/content/res/Resources;Lcom/google/android/gms/games/PlayerLevelInfo;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 196
    :cond_2
    :goto_3
    return-void

    .line 184
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v2, v4

    .line 192
    goto :goto_2

    .line 194
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->c(Lcom/google/android/gms/games/ui/common/players/d;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v3

    :goto_4
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->c(Lcom/google/android/gms/games/ui/common/players/d;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_5
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_4

    :cond_7
    move v4, v3

    goto :goto_5

    :cond_8
    move v1, v2

    goto/16 :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/d;

    .line 248
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v1

    if-nez v1, :cond_0

    .line 260
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    if-ne p1, v1, :cond_1

    .line 253
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/players/e;->a(Lcom/google/android/gms/games/Player;)V

    goto :goto_0

    .line 255
    :cond_1
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    const-string v3, "avatar"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 258
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/util/Pair;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/games/ui/common/players/e;->a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method
