.class public final Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestListActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f04002d

    const v1, 0x7f11000c

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 28
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f0f0058

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestListActivity;->setTitle(I)V

    .line 34
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 39
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f0c0045

    if-ne v0, v2, :cond_0

    .line 40
    iget-object v0, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    const v2, 0x7f0c00fc

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ag;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/quests/CompletedQuestFragment;->b(Z)V

    move v0, v1

    .line 43
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
