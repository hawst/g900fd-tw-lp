.class public final Lcom/google/android/gms/games/ui/d/e;
.super Landroid/transition/Transition;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 62
    return-void
.end method

.method private static a(Landroid/transition/TransitionValues;)V
    .locals 7

    .prologue
    .line 57
    iget-object v0, p0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 58
    iget-object v1, p0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "playgames:changeBoundsCentered:bounds"

    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method


# virtual methods
.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 53
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/e;->a(Landroid/transition/TransitionValues;)V

    .line 54
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 48
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/e;->a(Landroid/transition/TransitionValues;)V

    .line 49
    return-void
.end method

.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 33
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 43
    :goto_0
    return-object v0

    .line 37
    :cond_1
    iget-object v3, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 38
    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "playgames:changeBoundsCentered:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 39
    iget-object v1, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "playgames:changeBoundsCentered:bounds"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 41
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 42
    new-instance v4, Lcom/google/android/gms/games/ui/d/f;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d/e;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v5

    invoke-direct {v4, v3, v0, v1, v5}, Lcom/google/android/gms/games/ui/d/f;-><init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/transition/PathMotion;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object v0, v2

    .line 43
    goto :goto_0

    .line 41
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
