.class public final Lcom/google/android/gms/games/ui/common/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field private c:Z

.field private final d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private final e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ProgressBar;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/database/CharArrayBuffer;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/database/CharArrayBuffer;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/Button;

.field private final p:Landroid/widget/Button;

.field private final q:Landroid/view/View;

.field private final r:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget v0, Lcom/google/android/gms/g;->bL:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 55
    sget v0, Lcom/google/android/gms/g;->v:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->a:Landroid/view/View;

    .line 56
    sget v0, Lcom/google/android/gms/g;->bN:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 57
    sget v0, Lcom/google/android/gms/g;->bQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->f:Landroid/widget/TextView;

    .line 58
    sget v0, Lcom/google/android/gms/g;->bP:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->g:Landroid/widget/ProgressBar;

    .line 59
    sget v0, Lcom/google/android/gms/g;->bO:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->h:Landroid/widget/TextView;

    .line 60
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->i:Landroid/database/CharArrayBuffer;

    .line 61
    sget v0, Lcom/google/android/gms/g;->bM:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->j:Landroid/widget/TextView;

    .line 62
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/database/CharArrayBuffer;

    .line 63
    sget v0, Lcom/google/android/gms/g;->bR:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->l:Landroid/widget/TextView;

    .line 64
    sget v0, Lcom/google/android/gms/g;->bK:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->m:Landroid/widget/TextView;

    .line 66
    sget v0, Lcom/google/android/gms/g;->bJ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    .line 67
    sget v0, Lcom/google/android/gms/g;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->o:Landroid/widget/Button;

    .line 68
    sget v0, Lcom/google/android/gms/g;->y:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->p:Landroid/widget/Button;

    .line 69
    sget v0, Lcom/google/android/gms/g;->G:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->q:Landroid/view/View;

    .line 70
    sget v0, Lcom/google/android/gms/g;->bm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->r:Landroid/widget/Button;

    .line 72
    sget v0, Lcom/google/android/gms/g;->cp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->b:Landroid/view/View;

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/ui/ag;Lcom/google/android/gms/games/quest/Quest;Lcom/google/android/gms/games/ui/n;)V
    .locals 18

    .prologue
    .line 77
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/games/ui/common/a/c;->c:Z

    .line 79
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/z;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/a/c;->j:Landroid/widget/TextView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 86
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 87
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->m()I

    move-result v2

    .line 91
    const/4 v12, 0x0

    .line 92
    const/4 v11, 0x0

    .line 93
    const/4 v3, 0x0

    .line 94
    const/4 v10, 0x0

    .line 95
    const/4 v9, 0x0

    .line 97
    packed-switch v2, :pswitch_data_0

    .line 142
    const-string v4, "InternalQuestVH"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "populateViews: unexpected state: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v4, ""

    .line 144
    const-string v2, ""

    move v5, v11

    move v6, v12

    move-object v7, v2

    move-object v8, v4

    move v4, v3

    move v2, v9

    move v3, v10

    .line 148
    :goto_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->f()Landroid/net/Uri;

    move-result-object v9

    .line 149
    if-nez v9, :cond_1

    .line 151
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v9

    .line 153
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/ui/common/a/c;->a:Landroid/view/View;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 154
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/ui/common/a/c;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v11, Lcom/google/android/gms/f;->B:I

    new-instance v12, Lcom/google/android/gms/games/ui/common/a/d;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/google/android/gms/games/ui/common/a/d;-><init>(Lcom/google/android/gms/games/ui/common/a/c;)V

    move-object/from16 v0, p2

    invoke-interface {v0, v10, v9, v11, v12}, Lcom/google/android/gms/games/ui/ag;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/f;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->h()Landroid/net/Uri;

    move-result-object v10

    sget v11, Lcom/google/android/gms/f;->f:I

    move-object/from16 v0, p2

    invoke-interface {v0, v9, v10, v11}, Lcom/google/android/gms/games/ui/ag;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    .line 168
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->j()Lcom/google/android/gms/games/quest/Milestone;

    move-result-object v9

    .line 169
    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Milestone;->d()J

    move-result-wide v12

    .line 170
    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Milestone;->g()J

    move-result-wide v14

    .line 171
    sget v9, Lcom/google/android/gms/l;->bP:I

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v10, v11

    const/4 v11, 0x1

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v10, v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 173
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/ui/common/a/c;->f:Landroid/widget/TextView;

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    if-eqz v3, :cond_7

    .line 179
    const/16 v10, 0x64

    .line 180
    sget v9, Lcom/google/android/gms/f;->D:I

    .line 188
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/games/ui/common/a/c;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v11, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 190
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/games/ui/common/a/c;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v10, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v9}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->i:Landroid/database/CharArrayBuffer;

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Lcom/google/android/gms/games/quest/Quest;->a(Landroid/database/CharArrayBuffer;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->h:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/games/ui/common/a/c;->i:Landroid/database/CharArrayBuffer;

    iget-object v11, v11, Landroid/database/CharArrayBuffer;->data:[C

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->i:Landroid/database/CharArrayBuffer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v9, v11, v0, v1}, Landroid/widget/TextView;->setText([CII)V

    .line 195
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/database/CharArrayBuffer;

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Lcom/google/android/gms/games/quest/Quest;->b(Landroid/database/CharArrayBuffer;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->j:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/database/CharArrayBuffer;

    iget-object v11, v11, Landroid/database/CharArrayBuffer;->data:[C

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/database/CharArrayBuffer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v9, v11, v0, v1}, Landroid/widget/TextView;->setText([CII)V

    .line 199
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->l:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->m:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->c:Z

    if-eqz v9, :cond_8

    .line 204
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->m:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    const/16 v9, 0x8

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/view/View;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    move-object/from16 v17, v0

    aput-object v17, v11, v16

    invoke-static {v2, v9, v11}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 212
    :goto_3
    if-eqz v3, :cond_9

    .line 213
    sget v3, Lcom/google/android/gms/d;->p:I

    .line 218
    :goto_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->l:Landroid/widget/TextView;

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    if-nez v6, :cond_2

    if-nez v5, :cond_2

    if-eqz v4, :cond_a

    :cond_2
    const/4 v3, 0x1

    .line 221
    :goto_5
    const/16 v9, 0x8

    const/4 v10, 0x1

    new-array v10, v10, [Landroid/view/View;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->q:Landroid/view/View;

    move-object/from16 v16, v0

    aput-object v16, v10, v11

    invoke-static {v3, v9, v10}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 222
    const/16 v3, 0x8

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/View;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/games/ui/common/a/c;->o:Landroid/widget/Button;

    aput-object v11, v9, v10

    invoke-static {v6, v3, v9}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/a/c;->o:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 224
    const/16 v3, 0x8

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/view/View;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/ui/common/a/c;->p:Landroid/widget/Button;

    aput-object v10, v6, v9

    invoke-static {v5, v3, v6}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/a/c;->p:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 226
    const/16 v3, 0x8

    const/4 v5, 0x1

    new-array v5, v5, [Landroid/view/View;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->r:Landroid/widget/Button;

    aput-object v9, v5, v6

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/a/c;->r:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 230
    if-eqz v2, :cond_b

    .line 231
    sget v2, Lcom/google/android/gms/l;->bG:I

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v7, v3, v4

    const/4 v4, 0x5

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 242
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/a/c;->b:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 243
    return-void

    .line 79
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 99
    :pswitch_0
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->s()J

    move-result-wide v4

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/a/o;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v4

    .line 101
    const-string v3, ""

    .line 102
    const/4 v2, 0x0

    move v5, v11

    move v6, v12

    move-object v7, v3

    move-object v8, v4

    move v3, v10

    move v4, v2

    move v2, v9

    .line 103
    goto/16 :goto_1

    .line 105
    :pswitch_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->p()J

    move-result-wide v4

    const/4 v8, 0x1

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/a/o;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v5

    .line 107
    const-string v4, ""

    .line 108
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/games/ui/common/a/c;->c:Z

    .line 109
    if-nez v3, :cond_4

    const/4 v2, 0x1

    :goto_7
    move v6, v3

    move-object v7, v4

    move-object v8, v5

    move v5, v11

    move v3, v10

    move v4, v2

    move v2, v9

    .line 110
    goto/16 :goto_1

    .line 109
    :cond_4
    const/4 v2, 0x0

    goto :goto_7

    .line 112
    :pswitch_2
    sget v2, Lcom/google/android/gms/l;->bQ:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 113
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->p()J

    move-result-wide v4

    const/4 v8, 0x1

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/a/o;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v4

    .line 115
    const/4 v3, 0x1

    .line 116
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/games/ui/common/a/c;->c:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_8
    move v5, v11

    move v6, v12

    move-object v7, v4

    move-object v8, v9

    move v4, v2

    move v2, v3

    move v3, v10

    .line 117
    goto/16 :goto_1

    .line 116
    :cond_5
    const/4 v2, 0x0

    goto :goto_8

    .line 119
    :pswitch_3
    const/4 v2, 0x3

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->j()Lcom/google/android/gms/games/quest/Milestone;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/quest/Milestone;->f()I

    move-result v4

    if-ne v2, v4, :cond_6

    .line 121
    sget v2, Lcom/google/android/gms/l;->bT:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 122
    const-string v4, ""

    .line 123
    const/4 v2, 0x1

    move v6, v12

    move-object v7, v4

    move-object v8, v5

    move v5, v2

    move v4, v3

    move v3, v10

    move v2, v9

    goto/16 :goto_1

    .line 125
    :cond_6
    sget v2, Lcom/google/android/gms/l;->bT:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 126
    const-string v2, ""

    move v5, v11

    move v6, v12

    move-object v7, v2

    move-object v8, v4

    move v4, v3

    move v2, v9

    move v3, v10

    .line 128
    goto/16 :goto_1

    .line 130
    :pswitch_4
    sget v2, Lcom/google/android/gms/l;->bU:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 131
    const-string v4, ""

    .line 132
    const/4 v2, 0x1

    move v6, v12

    move-object v7, v4

    move-object v8, v5

    move v5, v11

    move v4, v3

    move v3, v2

    move v2, v9

    .line 133
    goto/16 :goto_1

    .line 135
    :pswitch_5
    sget v2, Lcom/google/android/gms/l;->bU:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 136
    const-string v4, ""

    .line 137
    const/4 v2, 0x1

    move v6, v12

    move-object v7, v4

    move-object v8, v5

    move v5, v11

    move v4, v3

    move v3, v2

    move v2, v9

    .line 138
    goto/16 :goto_1

    .line 185
    :cond_7
    const-wide/16 v10, 0x64

    mul-long/2addr v10, v12

    div-long/2addr v10, v14

    long-to-int v10, v10

    .line 186
    sget v9, Lcom/google/android/gms/f;->C:I

    goto/16 :goto_2

    .line 207
    :cond_8
    const/16 v9, 0x8

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/view/View;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/a/c;->m:Landroid/widget/TextView;

    move-object/from16 v17, v0

    aput-object v17, v11, v16

    invoke-static {v2, v9, v11}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 215
    :cond_9
    sget v3, Lcom/google/android/gms/d;->v:I

    goto/16 :goto_4

    .line 220
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 236
    :cond_b
    sget v2, Lcom/google/android/gms/l;->bO:I

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v8, v3, v4

    const/4 v4, 0x5

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->o:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->p:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->r:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    return-void
.end method
