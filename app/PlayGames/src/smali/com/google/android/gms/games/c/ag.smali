.class public final Lcom/google/android/gms/games/c/ag;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:I

.field public F:Lcom/google/android/gms/games/c/ah;

.field public a:Ljava/lang/String;

.field public b:I

.field public c:J

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:I

.field public j:J

.field public k:J

.field public l:I

.field public m:Z

.field public n:J

.field public o:J

.field public p:Z

.field public q:I

.field public r:I

.field public s:J

.field public t:J

.field public u:I

.field public v:J

.field public w:J

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 7246
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->b:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->c:J

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->g:I

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->h:I

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->i:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->j:J

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->k:J

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->n:J

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->o:J

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->q:I

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->r:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->s:J

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->t:J

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->u:I

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->v:J

    iput-wide v2, p0, Lcom/google/android/gms/games/c/ag;->w:J

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    iput v1, p0, Lcom/google/android/gms/games/c/ag;->E:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->G:I

    .line 7248
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7568
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 7569
    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7570
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7573
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->b:I

    if-eqz v1, :cond_1

    .line 7574
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7577
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 7578
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7581
    :cond_2
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->d:I

    if-eqz v1, :cond_3

    .line 7582
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7585
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 7586
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7589
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7590
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7593
    :cond_5
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->g:I

    if-eqz v1, :cond_6

    .line 7594
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7597
    :cond_6
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->h:I

    if-eqz v1, :cond_7

    .line 7598
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7601
    :cond_7
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->i:I

    if-eqz v1, :cond_8

    .line 7602
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7605
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->j:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 7606
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7609
    :cond_9
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->k:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 7610
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->k:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7613
    :cond_a
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->l:I

    if-eqz v1, :cond_b

    .line 7614
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7617
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    if-eqz v1, :cond_c

    .line 7618
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7621
    :cond_c
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->n:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_d

    .line 7622
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7625
    :cond_d
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->o:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_e

    .line 7626
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->o:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7629
    :cond_e
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    if-eqz v1, :cond_f

    .line 7630
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7633
    :cond_f
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->q:I

    if-eqz v1, :cond_10

    .line 7634
    const/16 v1, 0x11

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->q:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7637
    :cond_10
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->r:I

    if-eqz v1, :cond_11

    .line 7638
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->r:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7641
    :cond_11
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->s:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_12

    .line 7642
    const/16 v1, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->s:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7645
    :cond_12
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->t:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_13

    .line 7646
    const/16 v1, 0x14

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->t:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7649
    :cond_13
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->u:I

    if-eqz v1, :cond_14

    .line 7650
    const/16 v1, 0x15

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->u:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7653
    :cond_14
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->v:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 7654
    const/16 v1, 0x16

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->v:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7657
    :cond_15
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->w:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_16

    .line 7658
    const/16 v1, 0x17

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->w:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7661
    :cond_16
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    if-eqz v1, :cond_17

    .line 7662
    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7665
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    if-eqz v1, :cond_18

    .line 7666
    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7669
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    if-eqz v1, :cond_19

    .line 7670
    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7673
    :cond_19
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    if-eqz v1, :cond_1a

    .line 7674
    const/16 v1, 0x1b

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7677
    :cond_1a
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    if-eqz v1, :cond_1b

    .line 7678
    const/16 v1, 0x1c

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7681
    :cond_1b
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    if-eqz v1, :cond_1c

    .line 7682
    const/16 v1, 0x1d

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7685
    :cond_1c
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    if-eqz v1, :cond_1d

    .line 7686
    const/16 v1, 0x1e

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7689
    :cond_1d
    iget v1, p0, Lcom/google/android/gms/games/c/ag;->E:I

    if-eqz v1, :cond_1e

    .line 7690
    const/16 v1, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->E:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7693
    :cond_1e
    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-eqz v1, :cond_1f

    .line 7694
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7697
    :cond_1f
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/ag;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/games/c/ag;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->j:J

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->k:J

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->n:J

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->o:J

    goto :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->q:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->r:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->s:J

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->t:J

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/android/gms/games/c/ag;->u:I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->v:J

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/c/ag;->w:J

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ag;->E:I

    goto/16 :goto_0

    :sswitch_20
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x102 -> :sswitch_20
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7467
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7468
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7470
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->b:I

    if-eqz v0, :cond_1

    .line 7471
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7473
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 7474
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7476
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->d:I

    if-eqz v0, :cond_3

    .line 7477
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7479
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 7480
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7482
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 7483
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7485
    :cond_5
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->g:I

    if-eqz v0, :cond_6

    .line 7486
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7488
    :cond_6
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->h:I

    if-eqz v0, :cond_7

    .line 7489
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7491
    :cond_7
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->i:I

    if-eqz v0, :cond_8

    .line 7492
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7494
    :cond_8
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->j:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 7495
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7497
    :cond_9
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_a

    .line 7498
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7500
    :cond_a
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->l:I

    if-eqz v0, :cond_b

    .line 7501
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7503
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    if-eqz v0, :cond_c

    .line 7504
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7506
    :cond_c
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 7507
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7509
    :cond_d
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->o:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_e

    .line 7510
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->o:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7512
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    if-eqz v0, :cond_f

    .line 7513
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7515
    :cond_f
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->q:I

    if-eqz v0, :cond_10

    .line 7516
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7518
    :cond_10
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->r:I

    if-eqz v0, :cond_11

    .line 7519
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7521
    :cond_11
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->s:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_12

    .line 7522
    const/16 v0, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->s:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7524
    :cond_12
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->t:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_13

    .line 7525
    const/16 v0, 0x14

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->t:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7527
    :cond_13
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->u:I

    if-eqz v0, :cond_14

    .line 7528
    const/16 v0, 0x15

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->u:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7530
    :cond_14
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->v:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 7531
    const/16 v0, 0x16

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->v:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7533
    :cond_15
    iget-wide v0, p0, Lcom/google/android/gms/games/c/ag;->w:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_16

    .line 7534
    const/16 v0, 0x17

    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->w:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7536
    :cond_16
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    if-eqz v0, :cond_17

    .line 7537
    const/16 v0, 0x18

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7539
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    if-eqz v0, :cond_18

    .line 7540
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7542
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    if-eqz v0, :cond_19

    .line 7543
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7545
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    if-eqz v0, :cond_1a

    .line 7546
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7548
    :cond_1a
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    if-eqz v0, :cond_1b

    .line 7549
    const/16 v0, 0x1c

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7551
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    if-eqz v0, :cond_1c

    .line 7552
    const/16 v0, 0x1d

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7554
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    if-eqz v0, :cond_1d

    .line 7555
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7557
    :cond_1d
    iget v0, p0, Lcom/google/android/gms/games/c/ag;->E:I

    if-eqz v0, :cond_1e

    .line 7558
    const/16 v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/ag;->E:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7560
    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-eqz v0, :cond_1f

    .line 7561
    const/16 v0, 0x20

    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7563
    :cond_1f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 7564
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7289
    if-ne p1, p0, :cond_1

    .line 7410
    :cond_0
    :goto_0
    return v0

    .line 7292
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 7293
    goto :goto_0

    .line 7295
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ag;

    .line 7296
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 7297
    iget-object v2, p1, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 7298
    goto :goto_0

    .line 7300
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 7301
    goto :goto_0

    .line 7303
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->b:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 7304
    goto :goto_0

    .line 7306
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 7307
    goto :goto_0

    .line 7309
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->d:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 7310
    goto :goto_0

    .line 7312
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 7313
    iget-object v2, p1, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 7314
    goto :goto_0

    .line 7316
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 7317
    goto :goto_0

    .line 7319
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 7320
    iget-object v2, p1, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 7321
    goto :goto_0

    .line 7323
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 7324
    goto :goto_0

    .line 7326
    :cond_b
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->g:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 7327
    goto :goto_0

    .line 7329
    :cond_c
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->h:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 7330
    goto :goto_0

    .line 7332
    :cond_d
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->i:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->i:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 7333
    goto :goto_0

    .line 7335
    :cond_e
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->j:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 7336
    goto/16 :goto_0

    .line 7338
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->k:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 7339
    goto/16 :goto_0

    .line 7341
    :cond_10
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->l:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->l:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 7342
    goto/16 :goto_0

    .line 7344
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->m:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 7345
    goto/16 :goto_0

    .line 7347
    :cond_12
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->n:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->n:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_13

    move v0, v1

    .line 7348
    goto/16 :goto_0

    .line 7350
    :cond_13
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->o:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->o:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    move v0, v1

    .line 7351
    goto/16 :goto_0

    .line 7353
    :cond_14
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->p:Z

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 7354
    goto/16 :goto_0

    .line 7356
    :cond_15
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->q:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->q:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 7357
    goto/16 :goto_0

    .line 7359
    :cond_16
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->r:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->r:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 7360
    goto/16 :goto_0

    .line 7362
    :cond_17
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->s:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->s:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    move v0, v1

    .line 7363
    goto/16 :goto_0

    .line 7365
    :cond_18
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->t:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->t:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_19

    move v0, v1

    .line 7366
    goto/16 :goto_0

    .line 7368
    :cond_19
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->u:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->u:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 7369
    goto/16 :goto_0

    .line 7371
    :cond_1a
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->v:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->v:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1b

    move v0, v1

    .line 7372
    goto/16 :goto_0

    .line 7374
    :cond_1b
    iget-wide v2, p0, Lcom/google/android/gms/games/c/ag;->w:J

    iget-wide v4, p1, Lcom/google/android/gms/games/c/ag;->w:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1c

    move v0, v1

    .line 7375
    goto/16 :goto_0

    .line 7377
    :cond_1c
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->x:Z

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 7378
    goto/16 :goto_0

    .line 7380
    :cond_1d
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->y:Z

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 7381
    goto/16 :goto_0

    .line 7383
    :cond_1e
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->z:Z

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 7384
    goto/16 :goto_0

    .line 7386
    :cond_1f
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->A:Z

    if-eq v2, v3, :cond_20

    move v0, v1

    .line 7387
    goto/16 :goto_0

    .line 7389
    :cond_20
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->B:Z

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 7390
    goto/16 :goto_0

    .line 7392
    :cond_21
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->C:Z

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 7393
    goto/16 :goto_0

    .line 7395
    :cond_22
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/ag;->D:Z

    if-eq v2, v3, :cond_23

    move v0, v1

    .line 7396
    goto/16 :goto_0

    .line 7398
    :cond_23
    iget v2, p0, Lcom/google/android/gms/games/c/ag;->E:I

    iget v3, p1, Lcom/google/android/gms/games/c/ag;->E:I

    if-eq v2, v3, :cond_24

    move v0, v1

    .line 7399
    goto/16 :goto_0

    .line 7401
    :cond_24
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-nez v2, :cond_25

    .line 7402
    iget-object v2, p1, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-eqz v2, :cond_0

    move v0, v1

    .line 7403
    goto/16 :goto_0

    .line 7406
    :cond_25
    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/ah;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 7407
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v8, 0x20

    .line 7415
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7418
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->b:I

    add-int/2addr v0, v4

    .line 7419
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->c:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->c:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7421
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->d:I

    add-int/2addr v0, v4

    .line 7422
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 7424
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 7426
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->g:I

    add-int/2addr v0, v4

    .line 7427
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->h:I

    add-int/2addr v0, v4

    .line 7428
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->i:I

    add-int/2addr v0, v4

    .line 7429
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->j:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->j:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7431
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->k:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->k:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7433
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->l:I

    add-int/2addr v0, v4

    .line 7434
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->m:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 7435
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->n:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->n:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7437
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->o:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->o:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7439
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->p:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 7440
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->q:I

    add-int/2addr v0, v4

    .line 7441
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->r:I

    add-int/2addr v0, v4

    .line 7442
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->s:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->s:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7444
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->t:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->t:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7446
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/c/ag;->u:I

    add-int/2addr v0, v4

    .line 7447
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->v:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->v:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7449
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/c/ag;->w:J

    iget-wide v6, p0, Lcom/google/android/gms/games/c/ag;->w:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7451
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->x:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 7452
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->y:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 7453
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->z:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 7454
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->A:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v4

    .line 7455
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->B:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_9
    add-int/2addr v0, v4

    .line 7456
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/ag;->C:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    add-int/2addr v0, v4

    .line 7457
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/games/c/ag;->D:Z

    if-eqz v4, :cond_b

    :goto_b
    add-int/2addr v0, v2

    .line 7458
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/c/ag;->E:I

    add-int/2addr v0, v2

    .line 7459
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    if-nez v2, :cond_c

    :goto_c
    add-int/2addr v0, v1

    .line 7461
    return v0

    .line 7415
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 7422
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 7424
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ag;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_3
    move v0, v3

    .line 7434
    goto/16 :goto_3

    :cond_4
    move v0, v3

    .line 7439
    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 7451
    goto :goto_5

    :cond_6
    move v0, v3

    .line 7452
    goto :goto_6

    :cond_7
    move v0, v3

    .line 7453
    goto :goto_7

    :cond_8
    move v0, v3

    .line 7454
    goto :goto_8

    :cond_9
    move v0, v3

    .line 7455
    goto :goto_9

    :cond_a
    move v0, v3

    .line 7456
    goto :goto_a

    :cond_b
    move v2, v3

    .line 7457
    goto :goto_b

    .line 7459
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/games/c/ag;->F:Lcom/google/android/gms/games/c/ah;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/ah;->hashCode()I

    move-result v1

    goto :goto_c
.end method
