.class final Lcom/google/android/gms/people/internal/r;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/internal/m;

.field private final b:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/google/android/gms/people/internal/r;->a:Lcom/google/android/gms/people/internal/m;

    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 395
    iput-object p2, p0, Lcom/google/android/gms/people/internal/r;->b:Lcom/google/android/gms/common/api/m;

    .line 396
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    .prologue
    .line 400
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Owner callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/x;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/m;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 406
    if-nez p3, :cond_1

    const/4 v0, 0x0

    .line 408
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/internal/r;->a:Lcom/google/android/gms/people/internal/m;

    new-instance v3, Lcom/google/android/gms/people/internal/u;

    iget-object v4, p0, Lcom/google/android/gms/people/internal/r;->a:Lcom/google/android/gms/people/internal/m;

    iget-object v5, p0, Lcom/google/android/gms/people/internal/r;->b:Lcom/google/android/gms/common/api/m;

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/google/android/gms/people/internal/u;-><init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/f;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/internal/m;->a(Lcom/google/android/gms/common/internal/g;)V

    .line 409
    return-void

    .line 406
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/model/f;

    invoke-direct {v0, p3}, Lcom/google/android/gms/people/model/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
