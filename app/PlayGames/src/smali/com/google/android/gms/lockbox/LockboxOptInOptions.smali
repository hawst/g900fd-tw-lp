.class public Lcom/google/android/gms/lockbox/LockboxOptInOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/lockbox/a;


# instance fields
.field final a:I

.field b:I

.field c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/lockbox/a;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/lockbox/LockboxOptInOptions;->CREATOR:Lcom/google/android/gms/lockbox/a;

    return-void
.end method

.method constructor <init>(III)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/lockbox/LockboxOptInOptions;->a:I

    .line 42
    iput p2, p0, Lcom/google/android/gms/lockbox/LockboxOptInOptions;->b:I

    .line 43
    iput p3, p0, Lcom/google/android/gms/lockbox/LockboxOptInOptions;->c:I

    .line 44
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/google/android/gms/lockbox/a;->a(Lcom/google/android/gms/lockbox/LockboxOptInOptions;Landroid/os/Parcel;)V

    .line 66
    return-void
.end method
