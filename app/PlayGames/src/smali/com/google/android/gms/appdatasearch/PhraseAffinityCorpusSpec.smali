.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/t;


# instance fields
.field final a:I

.field public final b:Lcom/google/android/gms/appdatasearch/CorpusId;

.field final c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/appdatasearch/t;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/CorpusId;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a:I

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 41
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->c:Landroid/os/Bundle;

    .line 42
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/t;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;Landroid/os/Parcel;I)V

    .line 72
    return-void
.end method
