.class final Lcom/google/android/gms/games/ui/destination/games/p;
.super Lcom/google/android/play/c/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-direct {p0}, Lcom/google/android/play/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTransitionEnd(Landroid/transition/Transition;)V
    .locals 7

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 722
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->u(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->v(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    .line 727
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->t(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 728
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->t(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v3, 0x7f0c010a

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->j(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->k(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    .line 737
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->p(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    if-eqz v0, :cond_2

    .line 741
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->i(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 743
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v5, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 745
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 746
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 747
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 748
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 752
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 753
    const v1, 0x7f0c010d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    const v1, 0x7f0c0110

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    const v1, 0x7f0c0114

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 756
    const v1, 0x7f0c010b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    const v1, 0x7f0c0251

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 758
    const v1, 0x7f0c010c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    const v1, 0x7f0c0026

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 760
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;JLjava/util/ArrayList;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 762
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/q;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/games/q;-><init>(Lcom/google/android/gms/games/ui/destination/games/p;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 811
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/p;->a:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;J)Landroid/animation/Animator;

    move-result-object v0

    .line 812
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/s;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/games/s;-><init>(Lcom/google/android/gms/games/ui/destination/games/p;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 825
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0
.end method
