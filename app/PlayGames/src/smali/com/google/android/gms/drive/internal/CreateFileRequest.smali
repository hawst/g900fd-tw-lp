.class public Lcom/google/android/gms/drive/internal/CreateFileRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field final d:Lcom/google/android/gms/drive/Contents;

.field final e:Ljava/lang/Integer;

.field final f:Z

.field final g:Ljava/lang/String;

.field final h:I

.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/drive/internal/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;Ljava/lang/Integer;ZLjava/lang/String;II)V
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    if-eqz p4, :cond_0

    if-eqz p9, :cond_0

    .line 115
    invoke-virtual {p4}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v0

    if-ne v0, p9, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "inconsistent contents reference"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 118
    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    if-nez p4, :cond_3

    if-nez p9, :cond_3

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need a valid contents"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 124
    :cond_3
    iput p1, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->a:I

    .line 125
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b:Lcom/google/android/gms/drive/DriveId;

    .line 126
    invoke-static {p3}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 127
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->d:Lcom/google/android/gms/drive/Contents;

    .line 128
    iput-object p5, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->e:Ljava/lang/Integer;

    .line 129
    iput-object p7, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->g:Ljava/lang/String;

    .line 130
    iput p8, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->h:I

    .line 131
    iput-boolean p6, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->f:Z

    .line 132
    iput p9, p0, Lcom/google/android/gms/drive/internal/CreateFileRequest;->i:I

    .line 133
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 95
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/i;->a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Landroid/os/Parcel;I)V

    .line 96
    return-void
.end method
