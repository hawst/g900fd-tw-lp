.class public final Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"


# instance fields
.field private z:Lcom/google/android/gms/games/Player;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    const v0, 0x7f040040

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/b;-><init>(II)V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->z:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->z:Lcom/google/android/gms/games/Player;

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 61
    const-string v1, "com.google.android.gms.games.PLAYER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->z:Lcom/google/android/gms/games/Player;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->z:Lcom/google/android/gms/games/Player;

    const-string v1, "otherPlayer cannot be null in game comparison screen."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->z:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailGameComparisonActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method
