.class final Lcom/google/android/gms/games/ui/d/r;
.super Landroid/util/Property;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/d/p;

.field private b:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/d/p;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/r;->a:Lcom/google/android/gms/games/ui/d/p;

    .line 122
    const-class v0, Ljava/lang/Integer;

    invoke-direct {p0, v0, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 123
    return-void
.end method


# virtual methods
.method final a(Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/r;->b:Landroid/animation/ObjectAnimator;

    .line 127
    return-void
.end method

.method public final bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 118
    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/r;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getAnimatedFraction()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/r;->a:Lcom/google/android/gms/games/ui/d/p;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/p;->a(Lcom/google/android/gms/games/ui/d/p;)Z

    move-result v1

    if-eqz v1, :cond_1

    sub-float v0, v2, v0

    :cond_1
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    new-instance v1, Lcom/google/android/gms/games/ui/d/s;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/ui/d/s;-><init>(Lcom/google/android/gms/games/ui/d/r;F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClipToOutline(Z)V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method
