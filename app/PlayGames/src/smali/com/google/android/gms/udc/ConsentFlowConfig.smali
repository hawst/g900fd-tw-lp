.class public Lcom/google/android/gms/udc/ConsentFlowConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/udc/a;


# instance fields
.field private final a:I

.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/udc/a;

    invoke-direct {v0}, Lcom/google/android/gms/udc/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/gms/udc/ConsentFlowConfig;-><init>(IZZ)V

    .line 38
    return-void
.end method

.method constructor <init>(IZZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    .line 49
    iput-boolean p2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    .line 50
    iput-boolean p3, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    .line 51
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/udc/a;->a(Lcom/google/android/gms/udc/ConsentFlowConfig;Landroid/os/Parcel;)V

    .line 67
    return-void
.end method
