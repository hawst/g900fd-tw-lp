.class public Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ab;


# instance fields
.field final a:I

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Z

.field public final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/appdatasearch/ab;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ab;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->a:I

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->b:Ljava/lang/String;

    .line 49
    iput-wide p3, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->c:J

    .line 50
    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->d:Z

    .line 51
    iput-wide p6, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->e:J

    .line 52
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ab;->a(Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;Landroid/os/Parcel;)V

    .line 69
    return-void
.end method
