.class public final Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/u;

.field private static final e:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:J

.field d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 8137
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/u;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/u;

    .line 8152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8155
    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->e:Ljava/util/HashMap;

    const-string v1, "incomingAnyCircleCount"

    const-string v2, "incomingAnyCircleCount"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8156
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->e:Ljava/util/HashMap;

    const-string v1, "viewCount"

    const-string v2, "viewCount"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8157
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8188
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 8189
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->b:I

    .line 8190
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a:Ljava/util/Set;

    .line 8191
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IJJ)V
    .locals 1

    .prologue
    .line 8199
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 8200
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a:Ljava/util/Set;

    .line 8201
    iput p2, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->b:I

    .line 8202
    iput-wide p3, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->c:J

    .line 8203
    iput-wide p5, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->d:J

    .line 8204
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 8293
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 8298
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 8304
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8300
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 8302
    :goto_0
    return-object v0

    :pswitch_1
    iget-wide v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 8298
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 8161
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8282
    const/4 v0, 0x0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 8271
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/u;

    const/4 v0, 0x0

    return v0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 8288
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 8354
    instance-of v0, p1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;

    if-nez v0, :cond_0

    move v0, v1

    .line 8385
    :goto_0
    return v0

    .line 8359
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 8360
    goto :goto_0

    .line 8363
    :cond_1
    check-cast p1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;

    .line 8364
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 8365
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 8366
    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 8368
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 8370
    goto :goto_0

    :cond_3
    move v0, v1

    .line 8375
    goto :goto_0

    .line 8378
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 8380
    goto :goto_0

    :cond_5
    move v0, v2

    .line 8385
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 8341
    const/4 v0, 0x0

    .line 8342
    sget-object v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 8343
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8344
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v3

    add-int/2addr v1, v3

    .line 8345
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 8347
    goto :goto_0

    .line 8348
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 8276
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/u;

    invoke-static {p0, p1}, Lcom/google/android/gms/people/identity/internal/models/u;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata$ProfileOwnerStats;Landroid/os/Parcel;)V

    .line 8277
    return-void
.end method
