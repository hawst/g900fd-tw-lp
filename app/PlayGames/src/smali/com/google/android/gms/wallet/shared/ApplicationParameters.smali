.class public final Lcom/google/android/gms/wallet/shared/ApplicationParameters;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:I

.field c:Landroid/accounts/Account;

.field d:Landroid/os/Bundle;

.field e:Z

.field f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/wallet/shared/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e:Z

    .line 68
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a:I

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b:I

    .line 70
    iput v1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f:I

    .line 71
    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Landroid/os/Bundle;ZI)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e:Z

    .line 56
    iput p1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a:I

    .line 57
    iput p2, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b:I

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c:Landroid/accounts/Account;

    .line 59
    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d:Landroid/os/Bundle;

    .line 60
    iput-boolean p5, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e:Z

    .line 61
    iput p6, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f:I

    .line 62
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 183
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/shared/a;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;Landroid/os/Parcel;I)V

    .line 184
    return-void
.end method
