.class public final Lcom/google/android/gms/games/ui/destination/players/w;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Landroid/view/View;

.field private final n:Landroid/view/View;

.field private final o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/view/View;

.field private final t:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 146
    const v0, 0x7f0c01e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const v0, 0x7f0c01f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->n:Landroid/view/View;

    .line 151
    const v0, 0x7f0c0187

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00b1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00b2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const v1, 0x7f0b00af

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e(I)V

    .line 159
    const v0, 0x7f0c01e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    .line 160
    const v0, 0x7f0c0188

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->q:Landroid/widget/TextView;

    .line 162
    const v0, 0x7f0c01e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    const v0, 0x7f0c01ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->s:Landroid/view/View;

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->s:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    const v0, 0x7f0c01e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->t:Landroid/view/View;

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/w;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 10

    .prologue
    .line 177
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/u;

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 182
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->a(Lcom/google/android/gms/games/ui/destination/players/u;)F

    move-result v1

    .line 187
    float-to-double v4, v1

    const-wide v6, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_5

    .line 188
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 193
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 197
    :cond_0
    const/4 v2, 0x0

    .line 198
    const/4 v1, 0x0

    .line 199
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 200
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v2

    .line 201
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v1

    .line 203
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->c(Lcom/google/android/gms/games/ui/destination/players/u;)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;Z)V

    move-object v9, v1

    move-object v1, v2

    move-object v2, v9

    .line 207
    :goto_1
    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    :goto_2
    const/4 v1, -0x1

    .line 220
    if-nez v2, :cond_2

    .line 221
    const v1, 0x7f0a0040

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 223
    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->q:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 224
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 226
    const/4 v1, 0x1

    .line 227
    if-eqz v2, :cond_3

    .line 228
    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    .line 230
    :cond_3
    invoke-static {v3, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v2

    .line 232
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->d(Lcom/google/android/gms/games/ui/destination/players/u;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 233
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v7

    const v8, 0x7f0b0147

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 244
    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->e(Lcom/google/android/gms/games/ui/destination/players/u;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 245
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->e(Lcom/google/android/gms/games/ui/destination/players/u;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 246
    if-nez v4, :cond_8

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const v2, 0x7f0f00a7

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/w;->k:Landroid/content/Context;

    const v4, 0x7f0f00a7

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const v2, 0x7f02010c

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 270
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->s:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 277
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 292
    :goto_5
    return-void

    .line 190
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 212
    :cond_6
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->p:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 239
    :cond_7
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->s:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 255
    :cond_8
    const/4 v5, 0x1

    if-ne v4, v5, :cond_9

    .line 257
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->e(Lcom/google/android/gms/games/ui/destination/players/u;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 264
    :goto_6
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/w;->k:Landroid/content/Context;

    const v5, 0x7f0f00ad

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    const v2, 0x7f02010d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_4

    .line 260
    :cond_9
    const v0, 0x7f0e0001

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 285
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->t:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->t:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/w;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f020035

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 289
    invoke-static {v3, v1}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/content/res/Resources;I)I

    move-result v0

    .line 290
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_5

    :cond_b
    move-object v9, v1

    move-object v1, v2

    move-object v2, v9

    goto/16 :goto_1
.end method

.method public final o()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->o:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/w;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/players/u;

    .line 297
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->f(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/ui/destination/players/v;

    move-result-object v1

    if-nez v1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->r:Landroid/widget/TextView;

    if-ne p1, v1, :cond_2

    .line 302
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->f(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/ui/destination/players/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/destination/players/v;->aw()V

    goto :goto_0

    .line 303
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->s:Landroid/view/View;

    if-ne p1, v1, :cond_3

    .line 304
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->f(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/ui/destination/players/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/destination/players/v;->ax()V

    goto :goto_0

    .line 305
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/w;->m:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 306
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/u;->f(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/ui/destination/players/v;

    goto :goto_0
.end method
