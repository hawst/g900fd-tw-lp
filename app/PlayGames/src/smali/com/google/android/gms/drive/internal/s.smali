.class public final Lcom/google/android/gms/drive/internal/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/s;->e:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/s;->G:I

    .line 37
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 4

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->a()I

    move-result v0

    .line 104
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/internal/s;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 95
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 96
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 97
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 98
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->a(Lcom/google/protobuf/nano/b;)V

    .line 99
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 52
    const/4 v0, 0x1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/drive/internal/s;

    if-eqz v1, :cond_0

    .line 57
    check-cast p1, Lcom/google/android/gms/drive/internal/s;

    .line 58
    iget v1, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    iget v2, p1, Lcom/google/android/gms/drive/internal/s;->a:I

    if-ne v1, v2, :cond_0

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 62
    iget-object v1, p1, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 68
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/s;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 71
    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/s;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/internal/s;->a(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 65
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 79
    iget v0, p0, Lcom/google/android/gms/drive/internal/s;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 81
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/s;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/s;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/s;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/s;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
