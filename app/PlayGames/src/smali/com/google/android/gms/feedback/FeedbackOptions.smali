.class public Lcom/google/android/gms/feedback/FeedbackOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public b:Ljava/lang/String;

.field public c:Landroid/os/Bundle;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/app/ApplicationErrorReport;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/android/gms/common/data/BitmapTeleporter;

.field public i:Ljava/lang/String;

.field public j:Ljava/util/ArrayList;

.field public k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/gms/feedback/d;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    new-instance v6, Landroid/app/ApplicationErrorReport;

    invoke-direct {v6}, Landroid/app/ApplicationErrorReport;-><init>()V

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    move v11, v1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/feedback/FeedbackOptions;-><init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 122
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->a:I

    .line 106
    iput-object p2, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    .line 107
    iput-object p3, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    .line 108
    iput-object p4, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->d:Ljava/lang/String;

    .line 109
    iput-object p5, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    .line 110
    iput-object p6, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    .line 111
    iput-object p7, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    .line 112
    iput-object p8, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    .line 113
    iput-object p9, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    .line 114
    iput-object p10, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    .line 115
    iput-boolean p11, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->k:Z

    .line 116
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 89
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/feedback/d;->a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/os/Parcel;I)V

    .line 90
    return-void
.end method
