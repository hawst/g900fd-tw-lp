.class public Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/internal/b;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->CREATOR:Lcom/google/android/gms/location/copresence/internal/b;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 50
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    .line 52
    iput-boolean p3, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    .line 53
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    iget v1, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;-><init>(ILjava/lang/String;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->CREATOR:Lcom/google/android/gms/location/copresence/internal/b;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    if-ne p0, p1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 98
    goto :goto_0

    .line 101
    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    .line 102
    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FeatureOptIn[id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isOptedIn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->CREATOR:Lcom/google/android/gms/location/copresence/internal/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/internal/b;->a(Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;Landroid/os/Parcel;)V

    .line 79
    return-void
.end method
