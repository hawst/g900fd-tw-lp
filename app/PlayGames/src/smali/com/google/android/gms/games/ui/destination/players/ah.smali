.class final Lcom/google/android/gms/games/ui/destination/players/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/players/ae;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private d:Landroid/animation/Animator;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/players/ae;Landroid/view/View;Landroid/view/View;Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 539
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->a:Lcom/google/android/gms/games/ui/destination/players/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->b:Landroid/view/View;

    .line 541
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->c:Landroid/view/View;

    .line 543
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->d:Landroid/animation/Animator;

    .line 544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->e:Z

    .line 548
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/players/ae;->b(Lcom/google/android/gms/games/ui/destination/players/ae;)Lcom/google/android/gms/games/ui/destination/players/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 549
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/players/ae;->b(Lcom/google/android/gms/games/ui/destination/players/ae;)Lcom/google/android/gms/games/ui/destination/players/ah;

    move-result-object v0

    iput-object p4, v0, Lcom/google/android/gms/games/ui/destination/players/ah;->d:Landroid/animation/Animator;

    .line 552
    :cond_0
    invoke-static {p1, p0}, Lcom/google/android/gms/games/ui/destination/players/ae;->a(Lcom/google/android/gms/games/ui/destination/players/ae;Lcom/google/android/gms/games/ui/destination/players/ah;)Lcom/google/android/gms/games/ui/destination/players/ah;

    .line 553
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 557
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 561
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 562
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 566
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v1, v2, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 567
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->d:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->e:Z

    if-nez v0, :cond_0

    .line 572
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v0

    long-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    long-to-float v2, v2

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->e:Z

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/ah;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 578
    :cond_0
    return-void
.end method
