.class public Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public b:I

.field public final c:Lcom/google/android/gms/cloudsave/Entity;

.field public d:Z

.field public e:Lcom/google/android/gms/cloudsave/Entity$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/cloudsave/internal/i;

    invoke-direct {v0}, Lcom/google/android/gms/cloudsave/internal/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/cloudsave/Entity;ZLcom/google/android/gms/cloudsave/Entity$Key;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput p1, p0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->a:I

    .line 61
    iput p2, p0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->b:I

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->c:Lcom/google/android/gms/cloudsave/Entity;

    .line 63
    iput-boolean p4, p0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->d:Z

    .line 64
    iput-object p5, p0, Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;->e:Lcom/google/android/gms/cloudsave/Entity$Key;

    .line 65
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 45
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cloudsave/internal/i;->a(Lcom/google/android/gms/cloudsave/internal/SaveEntityResponse;Landroid/os/Parcel;I)V

    .line 46
    return-void
.end method
