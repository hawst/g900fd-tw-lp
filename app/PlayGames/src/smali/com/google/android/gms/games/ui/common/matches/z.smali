.class final Lcom/google/android/gms/games/ui/common/matches/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/widget/TextView;

.field final b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field final c:Landroid/widget/ImageView;

.field final d:Landroid/view/View;

.field final synthetic e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/z;->e:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    sget v0, Lcom/google/android/gms/g;->bk:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/z;->a:Landroid/widget/TextView;

    .line 240
    sget v0, Lcom/google/android/gms/g;->m:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d(I)V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/z;->b:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b(I)V

    .line 244
    sget v0, Lcom/google/android/gms/g;->aE:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/z;->c:Landroid/widget/ImageView;

    .line 246
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/z;->d:Landroid/view/View;

    .line 247
    return-void
.end method
