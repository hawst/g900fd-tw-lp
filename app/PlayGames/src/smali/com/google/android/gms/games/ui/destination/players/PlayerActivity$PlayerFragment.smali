.class public final Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;
.super Lcom/google/android/gms/games/ui/destination/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bb;


# static fields
.field private static final g:[Lcom/google/android/gms/games/ui/e/ai;

.field private static final h:Lcom/google/android/gms/games/ui/e/ah;

.field private static final i:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/games/ui/e/ai;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/games/ui/e/ai;

    const-class v3, Lcom/google/android/gms/games/ui/destination/players/ai;

    const v4, 0x7f0f00bb

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/gms/games/ui/e/ai;

    const-class v2, Lcom/google/android/gms/games/ui/destination/players/am;

    const v3, 0x7f0f00be

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/ui/e/ai;-><init>(Ljava/lang/Class;I)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    .line 85
    new-instance v0, Lcom/google/android/gms/games/ui/e/ah;

    sget-object v1, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->g:[Lcom/google/android/gms/games/ui/e/ai;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/e/ah;-><init>([Lcom/google/android/gms/games/ui/e/ai;)V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    .line 92
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 94
    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 97
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->h:Lcom/google/android/gms/games/ui/e/ah;

    const v1, 0x7f040096

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/destination/i;-><init>(Lcom/google/android/gms/games/ui/e/ah;I)V

    .line 101
    return-void
.end method


# virtual methods
.method public final R()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public final V()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 133
    sget-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 134
    if-eq v0, v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->f:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 136
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 141
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/destination/i;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/games/app/b;->a(Landroid/content/Context;)V

    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 165
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->a(Landroid/view/ViewGroup;)V

    .line 166
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/i;->d(Landroid/os/Bundle;)V

    .line 107
    if-nez p1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->d:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 109
    const-string v1, "com.google.android.gms.games.ui.extras.tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/i;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 112
    :cond_0
    return-void
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/i;->v()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/PlayerActivity$PlayerFragment;->ah()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 129
    return-void
.end method
