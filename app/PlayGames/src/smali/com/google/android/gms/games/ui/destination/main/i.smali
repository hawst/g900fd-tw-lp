.class final Lcom/google/android/gms/games/ui/destination/main/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/destination/main/d;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;)V
    .locals 0

    .prologue
    .line 980
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/destination/main/d;B)V
    .locals 0

    .prologue
    .line 980
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/main/i;-><init>(Lcom/google/android/gms/games/ui/destination/main/d;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 980
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/games/ui/destination/main/d;->au()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RecentlyPlayedGamesLoaded... statusCode = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", data = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/games/internal/ba;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->e(Lcom/google/android/gms/games/ui/destination/main/d;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->f(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->o()Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->g(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/destination/games/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/destination/games/a;->a(Lcom/google/android/gms/common/data/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->h(Lcom/google/android/gms/games/ui/destination/main/d;)Lcom/google/android/gms/games/ui/card/aa;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->a()I

    move-result v4

    if-nez v4, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/card/aa;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/main/i;->a:Lcom/google/android/gms/games/ui/destination/main/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/main/d;->i(Lcom/google/android/gms/games/ui/destination/main/d;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    :cond_1
    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method
