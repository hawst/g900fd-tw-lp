.class public final Lcom/google/android/gms/games/ui/common/leaderboards/d;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private g:[I

.field private h:Z

.field private i:Z

.field private j:Z

.field private final k:Lcom/google/android/gms/games/ui/common/leaderboards/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/gms/i;->c:I

    sput v0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/e;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->k:Lcom/google/android/gms/games/ui/common/leaderboards/e;

    .line 72
    return-void

    .line 51
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->j:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Lcom/google/android/gms/games/ui/common/leaderboards/e;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->k:Lcom/google/android/gms/games/ui/common/leaderboards/e;

    return-object v0
.end method

.method private f(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 304
    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Z)V

    move v0, v2

    .line 307
    :goto_1
    if-ge v0, p1, :cond_3

    .line 308
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aget v3, v3, v0

    if-ne v3, v1, :cond_2

    .line 314
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 304
    goto :goto_0

    .line 307
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 314
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aget v0, v0, p1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    move v2, v1

    goto :goto_2
.end method

.method private h(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 350
    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/ag;->a(Z)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aput v1, v0, p1

    .line 352
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->j()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 354
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 356
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 350
    goto :goto_0
.end method

.method private j()I
    .locals 4

    .prologue
    .line 330
    const/4 v1, -0x1

    .line 331
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 332
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aget v2, v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 337
    :goto_1
    return v0

    .line 331
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/f;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/leaderboards/d;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/f;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 86
    const-string v0, "BUTTERBAR_STATES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "BUTTERBAR_STATES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    .line 89
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :goto_0
    return-void

    .line 91
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c()V

    goto :goto_0

    .line 94
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->e()V

    goto :goto_0

    .line 97
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h()V

    goto :goto_0

    .line 89
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->e:I

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 321
    const-string v0, "BUTTERBAR_STATES"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 322
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 190
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 194
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->i:Z

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aput v1, v0, v2

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 231
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h:Z

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aput v1, v0, v1

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h(I)V

    .line 253
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 263
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 267
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->j:Z

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/d;->g:[I

    aput v1, v0, v2

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h(I)V

    .line 289
    return-void
.end method
