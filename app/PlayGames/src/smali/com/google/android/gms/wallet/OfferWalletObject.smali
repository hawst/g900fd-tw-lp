.class public final Lcom/google/android/gms/wallet/OfferWalletObject;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/wallet/n;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/OfferWalletObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    .line 89
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/wobs/CommonWalletObject;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->b:Ljava/lang/String;

    .line 78
    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    .line 79
    invoke-static {}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->a()Lcom/google/android/gms/wallet/wobs/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/wallet/wobs/a;->a:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    iput-object p2, v1, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/wallet/wobs/a;->a:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    iput-object v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iput-object p4, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 56
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/n;->a(Lcom/google/android/gms/wallet/OfferWalletObject;Landroid/os/Parcel;I)V

    .line 57
    return-void
.end method
