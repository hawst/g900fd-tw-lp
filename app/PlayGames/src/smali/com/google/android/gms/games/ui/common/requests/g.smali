.class public final Lcom/google/android/gms/games/ui/common/requests/g;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/requests/h;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/requests/g;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;I)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/h;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->g:Lcom/google/android/gms/games/ui/common/requests/h;

    .line 65
    sget v0, Lcom/google/android/gms/h;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/requests/g;->d(II)V

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/requests/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/requests/g;)Lcom/google/android/gms/games/ui/common/requests/h;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->g:Lcom/google/android/gms/games/ui/common/requests/h;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/b;)V
    .locals 1

    .prologue
    .line 79
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/q;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/b;)V

    .line 81
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/requests/g;->h:Ljava/lang/String;

    .line 75
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/i;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/i;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 149
    sget v0, Lcom/google/android/gms/g;->ae:I

    return v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x3

    return v0
.end method
