.class public Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/car/l;

    invoke-direct {v0}, Lcom/google/android/gms/car/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->a:I

    .line 66
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->a:I

    .line 58
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->b:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->c:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->d:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->e:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 75
    invoke-static {p0, p1}, Lcom/google/android/gms/car/l;->a(Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;Landroid/os/Parcel;)V

    .line 76
    return-void
.end method
