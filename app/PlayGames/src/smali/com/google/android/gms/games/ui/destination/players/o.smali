.class final Lcom/google/android/gms/games/ui/destination/players/o;
.super Lcom/google/android/play/c/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Lcom/google/android/gms/games/ui/destination/players/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/m;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/o;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/play/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTransitionEnd(Landroid/transition/Transition;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/m;->j(Lcom/google/android/gms/games/ui/destination/players/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/o;->a:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 282
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 283
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/o;->a:Landroid/widget/TextView;

    const-string v2, "scaleX"

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 284
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/o;->a:Landroid/widget/TextView;

    const-string v3, "scaleY"

    new-array v4, v5, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 285
    new-array v3, v5, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    aput-object v2, v3, v6

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 286
    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->k(Lcom/google/android/gms/games/ui/destination/players/m;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 288
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/destination/players/m;->at:Z

    if-nez v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/m;->l(Lcom/google/android/gms/games/ui/destination/players/m;)Lcom/google/android/gms/games/ui/destination/players/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/players/w;->o()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/players/m;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(J)V

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/o;->b:Lcom/google/android/gms/games/ui/destination/players/m;

    iput-boolean v6, v0, Lcom/google/android/gms/games/ui/destination/players/m;->at:Z

    .line 301
    :cond_1
    return-void

    .line 283
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 284
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
