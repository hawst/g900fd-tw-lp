.class public Lcom/android/b/a/a;
.super Lcom/android/b/d;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final b:Lcom/android/b/i;

.field private final c:Landroid/graphics/Bitmap$Config;

.field private final d:I

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/b/a/a;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/b/i;IILandroid/graphics/Bitmap$Config;Lcom/android/b/h;)V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0, p1, p6}, Lcom/android/b/d;-><init>(Ljava/lang/String;Lcom/android/b/h;)V

    .line 72
    new-instance v0, Lcom/android/b/c;

    const/16 v1, 0x3e8

    const/4 v2, 0x2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v2, v3}, Lcom/android/b/c;-><init>(IIF)V

    iput-object v0, p0, Lcom/android/b/d;->a:Lcom/android/b/j;

    .line 74
    iput-object p2, p0, Lcom/android/b/a/a;->b:Lcom/android/b/i;

    .line 75
    iput-object p5, p0, Lcom/android/b/a/a;->c:Landroid/graphics/Bitmap$Config;

    .line 76
    iput p3, p0, Lcom/android/b/a/a;->d:I

    .line 77
    iput p4, p0, Lcom/android/b/a/a;->e:I

    .line 78
    return-void
.end method


# virtual methods
.method public final d()Lcom/android/b/e;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/android/b/e;->a:Lcom/android/b/e;

    return-object v0
.end method
