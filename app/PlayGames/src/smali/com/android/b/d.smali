.class public abstract Lcom/android/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Lcom/android/b/j;

.field private final b:Lcom/android/b/l;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Lcom/android/b/h;

.field private g:Ljava/lang/Integer;

.field private h:Lcom/android/b/f;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:J

.field private m:Lcom/android/b/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/b/h;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-boolean v0, Lcom/android/b/l;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/b/l;

    invoke-direct {v0}, Lcom/android/b/l;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/android/b/d;->b:Lcom/android/b/l;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/b/d;->i:Z

    .line 88
    iput-boolean v2, p0, Lcom/android/b/d;->j:Z

    .line 91
    iput-boolean v2, p0, Lcom/android/b/d;->k:Z

    .line 94
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/b/d;->l:J

    .line 107
    iput-object v1, p0, Lcom/android/b/d;->m:Lcom/android/b/b;

    .line 132
    iput v2, p0, Lcom/android/b/d;->c:I

    .line 133
    iput-object p1, p0, Lcom/android/b/d;->d:Ljava/lang/String;

    .line 134
    iput-object p2, p0, Lcom/android/b/d;->f:Lcom/android/b/h;

    .line 135
    new-instance v0, Lcom/android/b/c;

    invoke-direct {v0}, Lcom/android/b/c;-><init>()V

    iput-object v0, p0, Lcom/android/b/d;->a:Lcom/android/b/j;

    .line 137
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/android/b/d;->e:I

    .line 138
    return-void

    :cond_0
    move-object v0, v1

    .line 61
    goto :goto_0

    :cond_1
    move v0, v2

    .line 137
    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lcom/android/b/d;
    .locals 1

    .prologue
    .line 262
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/b/d;->g:Ljava/lang/Integer;

    .line 263
    return-object p0
.end method

.method public final a(Lcom/android/b/f;)Lcom/android/b/d;
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/b/d;->h:Lcom/android/b/f;

    .line 253
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/android/b/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 203
    sget-boolean v0, Lcom/android/b/l;->a:Z

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/android/b/d;->b:Lcom/android/b/l;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/android/b/l;->a(Ljava/lang/String;J)V

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-wide v0, p0, Lcom/android/b/d;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 206
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/b/d;->l:J

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/b/d;->j:Z

    .line 313
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/android/b/d;->i:Z

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 38
    check-cast p1, Lcom/android/b/d;

    invoke-virtual {p0}, Lcom/android/b/d;->d()Lcom/android/b/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/b/d;->d()Lcom/android/b/e;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/b/d;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lcom/android/b/d;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/android/b/e;->ordinal()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/b/e;->ordinal()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public d()Lcom/android/b/e;
    .locals 1

    .prologue
    .line 490
    sget-object v0, Lcom/android/b/e;->b:Lcom/android/b/e;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/b/d;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 586
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lcom/android/b/d;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "[X] "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/b/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/b/d;->d()Lcom/android/b/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/b/d;->g:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[ ] "

    goto :goto_0
.end method
