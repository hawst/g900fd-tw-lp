.class public final Lcom/samsung/android/sdk/spenv10/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/spenv10/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final stms_appgroup:I = 0x7f050000

.field public static final string_add_preset:I = 0x7f050001

.field public static final string_align:I = 0x7f050002

.field public static final string_already_exists:I = 0x7f050003

.field public static final string_back:I = 0x7f050004

.field public static final string_beutify:I = 0x7f050005

.field public static final string_bold:I = 0x7f050006

.field public static final string_brush:I = 0x7f050007

.field public static final string_button:I = 0x7f050008

.field public static final string_calligraphy_pen:I = 0x7f050009

.field public static final string_chinese_brush:I = 0x7f05000a

.field public static final string_clear_all:I = 0x7f05000b

.field public static final string_close:I = 0x7f05000c

.field public static final string_color_black:I = 0x7f05000d

.field public static final string_color_blue:I = 0x7f05000e

.field public static final string_color_blue_violet:I = 0x7f05000f

.field public static final string_color_burgundy:I = 0x7f050010

.field public static final string_color_burnt_umber:I = 0x7f050011

.field public static final string_color_chartreuse:I = 0x7f050012

.field public static final string_color_coral:I = 0x7f050013

.field public static final string_color_crimson:I = 0x7f050014

.field public static final string_color_dark_blue:I = 0x7f050015

.field public static final string_color_dark_cyan:I = 0x7f050016

.field public static final string_color_dark_grey:I = 0x7f050017

.field public static final string_color_dark_salmon:I = 0x7f050018

.field public static final string_color_dim_grey:I = 0x7f050019

.field public static final string_color_forest_green:I = 0x7f05001a

.field public static final string_color_gold:I = 0x7f05001b

.field public static final string_color_green:I = 0x7f05001c

.field public static final string_color_grey:I = 0x7f05001d

.field public static final string_color_hot_pink:I = 0x7f05001e

.field public static final string_color_indigo:I = 0x7f05001f

.field public static final string_color_light_pink:I = 0x7f050020

.field public static final string_color_light_salmon:I = 0x7f050021

.field public static final string_color_lilac:I = 0x7f050022

.field public static final string_color_lime_green:I = 0x7f050023

.field public static final string_color_magenta:I = 0x7f050024

.field public static final string_color_maroon:I = 0x7f050025

.field public static final string_color_medium_orchid:I = 0x7f050026

.field public static final string_color_olive_drab:I = 0x7f050027

.field public static final string_color_orange:I = 0x7f050028

.field public static final string_color_palette:I = 0x7f050029

.field public static final string_color_peach_puff:I = 0x7f05002a

.field public static final string_color_peacock_blue:I = 0x7f05002b

.field public static final string_color_permanent_violet:I = 0x7f05002c

.field public static final string_color_picker_tts:I = 0x7f05002d

.field public static final string_color_pitch_black:I = 0x7f05002e

.field public static final string_color_plum:I = 0x7f05002f

.field public static final string_color_purple:I = 0x7f050030

.field public static final string_color_red:I = 0x7f050031

.field public static final string_color_royal_blue:I = 0x7f050032

.field public static final string_color_saddle_brown:I = 0x7f050033

.field public static final string_color_sap_green:I = 0x7f050034

.field public static final string_color_sea_green:I = 0x7f050035

.field public static final string_color_sky_blue:I = 0x7f050036

.field public static final string_color_steel_blue:I = 0x7f050037

.field public static final string_color_tap_to_apply:I = 0x7f050038

.field public static final string_color_teal:I = 0x7f050039

.field public static final string_color_tomato:I = 0x7f05003a

.field public static final string_color_vandyke_brown:I = 0x7f05003b

.field public static final string_color_viridian:I = 0x7f05003c

.field public static final string_color_white:I = 0x7f05003d

.field public static final string_color_yellow:I = 0x7f05003e

.field public static final string_color_yellow_ochre:I = 0x7f05003f

.field public static final string_copied_to_clipboard:I = 0x7f050040

.field public static final string_correction_pen:I = 0x7f050041

.field public static final string_cursive:I = 0x7f050042

.field public static final string_delete_preset:I = 0x7f050043

.field public static final string_delete_text:I = 0x7f050044

.field public static final string_dialog_cancel:I = 0x7f050045

.field public static final string_dialog_ok:I = 0x7f050046

.field public static final string_drag_to_resize:I = 0x7f050047

.field public static final string_dummy:I = 0x7f050048

.field public static final string_enter:I = 0x7f050049

.field public static final string_erase_by_stroke:I = 0x7f05004a

.field public static final string_erase_line_by_line:I = 0x7f05004b

.field public static final string_eraser_settings:I = 0x7f05004c

.field public static final string_fountain_pen:I = 0x7f05004d

.field public static final string_gradation:I = 0x7f05004e

.field public static final string_indent:I = 0x7f05004f

.field public static final string_italic:I = 0x7f050050

.field public static final string_lasso:I = 0x7f050051

.field public static final string_line_spacing:I = 0x7f050052

.field public static final string_marker:I = 0x7f050053

.field public static final string_minus:I = 0x7f050054

.field public static final string_modulation:I = 0x7f050055

.field public static final string_move_down:I = 0x7f050056

.field public static final string_move_left:I = 0x7f050057

.field public static final string_move_right:I = 0x7f050058

.field public static final string_move_up:I = 0x7f050059

.field public static final string_next:I = 0x7f05005a

.field public static final string_no_pen_history:I = 0x7f05005b

.field public static final string_no_preset:I = 0x7f05005c

.field public static final string_not_selected:I = 0x7f05005d

.field public static final string_palette:I = 0x7f05005e

.field public static final string_pasted_to_clipboard:I = 0x7f05005f

.field public static final string_pen:I = 0x7f050060

.field public static final string_pen_preset:I = 0x7f050061

.field public static final string_pen_settings:I = 0x7f050062

.field public static final string_pen_tab:I = 0x7f050063

.field public static final string_pencil:I = 0x7f050064

.field public static final string_plus:I = 0x7f050065

.field public static final string_preset:I = 0x7f050066

.field public static final string_preset_tab:I = 0x7f050067

.field public static final string_reached_maximum_input:I = 0x7f050068

.field public static final string_reached_maximum_preset:I = 0x7f050069

.field public static final string_rectangle:I = 0x7f05006a

.field public static final string_reset:I = 0x7f05006b

.field public static final string_resize:I = 0x7f05006c

.field public static final string_selected:I = 0x7f05006d

.field public static final string_selection_mode:I = 0x7f05006e

.field public static final string_special:I = 0x7f05006f

.field public static final string_sustenance:I = 0x7f050070

.field public static final string_switch_to_front_camera:I = 0x7f050071

.field public static final string_switch_to_rear_camera:I = 0x7f050072

.field public static final string_tab_selected_tts:I = 0x7f050073

.field public static final string_text_settings:I = 0x7f050074

.field public static final string_title:I = 0x7f050075

.field public static final string_transform_back_to_original_shape:I = 0x7f050076

.field public static final string_transform_into_auto_shape:I = 0x7f050077

.field public static final string_unable_to_erase_heavy_lines:I = 0x7f050078

.field public static final string_underline:I = 0x7f050079

.field public static final string_upgrade_notification:I = 0x7f05007a

.field public static final string_upgrade_recommended:I = 0x7f05007b

.field public static final string_upgrade_required:I = 0x7f05007c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
