.class public final Lcom/samsung/android/sdk/spenv10/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/spenv10/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final beautify_switch_off:I = 0x7f020000

.field public static final beautify_switch_on:I = 0x7f020001

.field public static final beautify_switch_thumb:I = 0x7f020002

.field public static final chinabrush_effect_btn_hold:I = 0x7f020003

.field public static final chinabrush_effect_btn_normal:I = 0x7f020004

.field public static final chinabrush_effect_btn_press:I = 0x7f020005

.field public static final chinabrush_mode_01:I = 0x7f020006

.field public static final chinabrush_mode_01_press:I = 0x7f020007

.field public static final chinabrush_mode_02:I = 0x7f020008

.field public static final chinabrush_mode_02_press:I = 0x7f020009

.field public static final chinabrush_mode_03:I = 0x7f02000a

.field public static final chinabrush_mode_03_press:I = 0x7f02000b

.field public static final chinabrush_mode_04:I = 0x7f02000c

.field public static final chinabrush_mode_04_press:I = 0x7f02000d

.field public static final chinabrush_mode_05:I = 0x7f02000e

.field public static final chinabrush_mode_05_press:I = 0x7f02000f

.field public static final chinabrush_mode_06:I = 0x7f020010

.field public static final chinabrush_mode_06_press:I = 0x7f020011

.field public static final contextmenu_popup:I = 0x7f020012

.field public static final contextmenu_seperator:I = 0x7f020013

.field public static final eraser_bar:I = 0x7f020014

.field public static final eraser_handel:I = 0x7f020015

.field public static final eraser_handel_press:I = 0x7f020016

.field public static final expand_icon_01:I = 0x7f020017

.field public static final expand_icon_02:I = 0x7f020018

.field public static final handler_icon:I = 0x7f020019

.field public static final handler_icon_rotate:I = 0x7f02001a

.field public static final handler_icon_rotate_chagall:I = 0x7f02001b

.field public static final hover_pointer_text:I = 0x7f02001c

.field public static final ic_launcher:I = 0x7f02001d

.field public static final pen_preset_alpha:I = 0x7f02001e

.field public static final pen_preset_bg:I = 0x7f02001f

.field public static final pen_preset_bg_focus:I = 0x7f020020

.field public static final pen_preset_bg_press:I = 0x7f020021

.field public static final pen_preset_bg_selected:I = 0x7f020022

.field public static final pen_preset_brush:I = 0x7f020023

.field public static final pen_preset_calligraphypen:I = 0x7f020024

.field public static final pen_preset_chinabrush:I = 0x7f020025

.field public static final pen_preset_delete_focus:I = 0x7f020026

.field public static final pen_preset_delete_focus_chagall:I = 0x7f020027

.field public static final pen_preset_delete_normal:I = 0x7f020028

.field public static final pen_preset_delete_normal_chagall:I = 0x7f020029

.field public static final pen_preset_delete_press:I = 0x7f02002a

.field public static final pen_preset_delete_press_chagall:I = 0x7f02002b

.field public static final pen_preset_fountainpen:I = 0x7f02002c

.field public static final pen_preset_marker:I = 0x7f02002d

.field public static final pen_preset_montblanc_calligraphypen:I = 0x7f02002e

.field public static final pen_preset_montblanc_fountainpen:I = 0x7f02002f

.field public static final pen_preset_montblanc_marker:I = 0x7f020030

.field public static final pen_preset_montblanc_pen:I = 0x7f020031

.field public static final pen_preset_montblanc_pencil:I = 0x7f020032

.field public static final pen_preset_montblancpen:I = 0x7f020033

.field public static final pen_preset_pen:I = 0x7f020034

.field public static final pen_preset_pencil:I = 0x7f020035

.field public static final pensetting_btn_center_focus:I = 0x7f020036

.field public static final pensetting_btn_center_normal:I = 0x7f020037

.field public static final pensetting_btn_center_press:I = 0x7f020038

.field public static final pensetting_btn_center_select:I = 0x7f020039

.field public static final pensetting_btn_left_focus:I = 0x7f02003a

.field public static final pensetting_btn_left_normal:I = 0x7f02003b

.field public static final pensetting_btn_left_press:I = 0x7f02003c

.field public static final pensetting_btn_left_select:I = 0x7f02003d

.field public static final pensetting_btn_right_focus:I = 0x7f02003e

.field public static final pensetting_btn_right_normal:I = 0x7f02003f

.field public static final pensetting_btn_right_press:I = 0x7f020040

.field public static final pensetting_btn_right_select:I = 0x7f020041

.field public static final popup_title_ic_delete_focus:I = 0x7f020042

.field public static final popup_title_ic_delete_normal:I = 0x7f020043

.field public static final popup_title_ic_delete_press:I = 0x7f020044

.field public static final preview_alpha:I = 0x7f020045

.field public static final progress_bg:I = 0x7f020046

.field public static final progress_bg_alpha:I = 0x7f020047

.field public static final progress_handle_focus:I = 0x7f020048

.field public static final progress_handle_normal:I = 0x7f020049

.field public static final progress_handle_press:I = 0x7f02004a

.field public static final progress_shadow:I = 0x7f02004b

.field public static final quick_popup_bg:I = 0x7f02004c

.field public static final quick_popup_bg_press:I = 0x7f02004d

.field public static final quick_popup_bg_press_vienna:I = 0x7f02004e

.field public static final quick_popup_dv:I = 0x7f02004f

.field public static final quick_popup_dv_vienna:I = 0x7f020050

.field public static final selector_change_stroke_frame_bg:I = 0x7f020051

.field public static final shadow_border:I = 0x7f020052

.field public static final shadow_border_n1:I = 0x7f020053

.field public static final shadow_border_vienna:I = 0x7f020054

.field public static final snote_btn_check_off:I = 0x7f020055

.field public static final snote_btn_check_off_focused:I = 0x7f020056

.field public static final snote_btn_check_off_ll:I = 0x7f020057

.field public static final snote_btn_check_off_pressed:I = 0x7f020058

.field public static final snote_btn_check_on:I = 0x7f020059

.field public static final snote_btn_check_on_focused:I = 0x7f02005a

.field public static final snote_btn_check_on_ll:I = 0x7f02005b

.field public static final snote_btn_check_on_pressed:I = 0x7f02005c

.field public static final snote_color_box:I = 0x7f02005d

.field public static final snote_color_box_grayscale:I = 0x7f02005e

.field public static final snote_color_select_kit:I = 0x7f02005f

.field public static final snote_color_spoid_dim:I = 0x7f020060

.field public static final snote_color_spoid_focus:I = 0x7f020061

.field public static final snote_color_spoid_normal:I = 0x7f020062

.field public static final snote_color_spoid_press:I = 0x7f020063

.field public static final snote_colorbox_mini:I = 0x7f020064

.field public static final snote_colorchip:I = 0x7f020065

.field public static final snote_colorchip_select_box:I = 0x7f020066

.field public static final snote_colorchip_select_box_focus:I = 0x7f020067

.field public static final snote_colorchip_shadow:I = 0x7f020068

.field public static final snote_dropdown_focused:I = 0x7f020069

.field public static final snote_dropdown_normal:I = 0x7f02006a

.field public static final snote_dropdown_pressed:I = 0x7f02006b

.field public static final snote_eraser_popup_draw:I = 0x7f02006c

.field public static final snote_eraser_popup_draw_press:I = 0x7f02006d

.field public static final snote_eraser_popup_text:I = 0x7f02006e

.field public static final snote_eraser_popup_text_press:I = 0x7f02006f

.field public static final snote_insert_video_icon_cue:I = 0x7f020070

.field public static final snote_insert_video_rec_01:I = 0x7f020071

.field public static final snote_insert_video_rec_02:I = 0x7f020072

.field public static final snote_option_in_bg:I = 0x7f020073

.field public static final snote_photoframe_refine:I = 0x7f020074

.field public static final snote_photoframe_toggle:I = 0x7f020075

.field public static final snote_photoframe_undo:I = 0x7f020076

.field public static final snote_popup_add:I = 0x7f020077

.field public static final snote_popup_add_focus:I = 0x7f020078

.field public static final snote_popup_add_press:I = 0x7f020079

.field public static final snote_popup_arrow_left_dim:I = 0x7f02007a

.field public static final snote_popup_arrow_left_focus:I = 0x7f02007b

.field public static final snote_popup_arrow_left_normal:I = 0x7f02007c

.field public static final snote_popup_arrow_left_press:I = 0x7f02007d

.field public static final snote_popup_arrow_max_normal:I = 0x7f02007e

.field public static final snote_popup_arrow_min_normal:I = 0x7f02007f

.field public static final snote_popup_arrow_right_dim:I = 0x7f020080

.field public static final snote_popup_arrow_right_focus:I = 0x7f020081

.field public static final snote_popup_arrow_right_normal:I = 0x7f020082

.field public static final snote_popup_arrow_right_press:I = 0x7f020083

.field public static final snote_popup_bg02_left:I = 0x7f020084

.field public static final snote_popup_bg02_right:I = 0x7f020085

.field public static final snote_popup_bg_expand:I = 0x7f020086

.field public static final snote_popup_bg_expand_preset:I = 0x7f020087

.field public static final snote_popup_bg_expand_preset_press:I = 0x7f020088

.field public static final snote_popup_bg_expand_press:I = 0x7f020089

.field public static final snote_popup_bg_left:I = 0x7f02008a

.field public static final snote_popup_bg_right:I = 0x7f02008b

.field public static final snote_popup_btn_focus:I = 0x7f02008c

.field public static final snote_popup_btn_normal:I = 0x7f02008d

.field public static final snote_popup_btn_press:I = 0x7f02008e

.field public static final snote_popup_close:I = 0x7f02008f

.field public static final snote_popup_close_focus:I = 0x7f020090

.field public static final snote_popup_close_press:I = 0x7f020091

.field public static final snote_popup_delete:I = 0x7f020092

.field public static final snote_popup_delete_focus:I = 0x7f020093

.field public static final snote_popup_delete_press:I = 0x7f020094

.field public static final snote_popup_divider:I = 0x7f020095

.field public static final snote_popup_handler:I = 0x7f020096

.field public static final snote_popup_icon_close:I = 0x7f020097

.field public static final snote_popup_icon_close_2:I = 0x7f020098

.field public static final snote_popup_icon_lasso:I = 0x7f020099

.field public static final snote_popup_icon_rectangle:I = 0x7f02009a

.field public static final snote_popup_line:I = 0x7f02009b

.field public static final snote_popup_option_btn_center_focus:I = 0x7f02009c

.field public static final snote_popup_option_btn_center_normal:I = 0x7f02009d

.field public static final snote_popup_option_btn_center_press:I = 0x7f02009e

.field public static final snote_popup_option_btn_focus:I = 0x7f02009f

.field public static final snote_popup_option_btn_left_focus:I = 0x7f0200a0

.field public static final snote_popup_option_btn_left_normal:I = 0x7f0200a1

.field public static final snote_popup_option_btn_left_press:I = 0x7f0200a2

.field public static final snote_popup_option_btn_left_press_1:I = 0x7f0200a3

.field public static final snote_popup_option_btn_normal:I = 0x7f0200a4

.field public static final snote_popup_option_btn_press:I = 0x7f0200a5

.field public static final snote_popup_option_btn_right_focus:I = 0x7f0200a6

.field public static final snote_popup_option_btn_right_normal:I = 0x7f0200a7

.field public static final snote_popup_option_btn_right_press:I = 0x7f0200a8

.field public static final snote_popup_option_btn_right_press_1:I = 0x7f0200a9

.field public static final snote_popup_pensetting_alpha:I = 0x7f0200aa

.field public static final snote_popup_pensetting_alpha_focus:I = 0x7f0200ab

.field public static final snote_popup_pensetting_alpha_select:I = 0x7f0200ac

.field public static final snote_popup_pensetting_brush:I = 0x7f0200ad

.field public static final snote_popup_pensetting_brush_focus:I = 0x7f0200ae

.field public static final snote_popup_pensetting_brush_select:I = 0x7f0200af

.field public static final snote_popup_pensetting_calligraphypen:I = 0x7f0200b0

.field public static final snote_popup_pensetting_calligraphypen_focus:I = 0x7f0200b1

.field public static final snote_popup_pensetting_calligraphypen_select:I = 0x7f0200b2

.field public static final snote_popup_pensetting_chinabrush:I = 0x7f0200b3

.field public static final snote_popup_pensetting_chinabrush_focus:I = 0x7f0200b4

.field public static final snote_popup_pensetting_chinabrush_select:I = 0x7f0200b5

.field public static final snote_popup_pensetting_fountainpen:I = 0x7f0200b6

.field public static final snote_popup_pensetting_fountainpen_focus:I = 0x7f0200b7

.field public static final snote_popup_pensetting_fountainpen_select:I = 0x7f0200b8

.field public static final snote_popup_pensetting_marker:I = 0x7f0200b9

.field public static final snote_popup_pensetting_marker_focus:I = 0x7f0200ba

.field public static final snote_popup_pensetting_marker_select:I = 0x7f0200bb

.field public static final snote_popup_pensetting_montblanc_calligraphypen:I = 0x7f0200bc

.field public static final snote_popup_pensetting_montblanc_calligraphypen_focus:I = 0x7f0200bd

.field public static final snote_popup_pensetting_montblanc_calligraphypen_select:I = 0x7f0200be

.field public static final snote_popup_pensetting_montblanc_fountainpen:I = 0x7f0200bf

.field public static final snote_popup_pensetting_montblanc_fountainpen_focus:I = 0x7f0200c0

.field public static final snote_popup_pensetting_montblanc_fountainpen_select:I = 0x7f0200c1

.field public static final snote_popup_pensetting_montblanc_marker:I = 0x7f0200c2

.field public static final snote_popup_pensetting_montblanc_marker_focus:I = 0x7f0200c3

.field public static final snote_popup_pensetting_montblanc_marker_select:I = 0x7f0200c4

.field public static final snote_popup_pensetting_montblanc_pen:I = 0x7f0200c5

.field public static final snote_popup_pensetting_montblanc_pen_focus:I = 0x7f0200c6

.field public static final snote_popup_pensetting_montblanc_pen_select:I = 0x7f0200c7

.field public static final snote_popup_pensetting_montblanc_pencil:I = 0x7f0200c8

.field public static final snote_popup_pensetting_montblanc_pencil_focus:I = 0x7f0200c9

.field public static final snote_popup_pensetting_montblanc_pencil_select:I = 0x7f0200ca

.field public static final snote_popup_pensetting_montblancpen:I = 0x7f0200cb

.field public static final snote_popup_pensetting_montblancpen_focus:I = 0x7f0200cc

.field public static final snote_popup_pensetting_montblancpen_select:I = 0x7f0200cd

.field public static final snote_popup_pensetting_pen:I = 0x7f0200ce

.field public static final snote_popup_pensetting_pen_focus:I = 0x7f0200cf

.field public static final snote_popup_pensetting_pen_select:I = 0x7f0200d0

.field public static final snote_popup_pensetting_pencil:I = 0x7f0200d1

.field public static final snote_popup_pensetting_pencil_focus:I = 0x7f0200d2

.field public static final snote_popup_pensetting_pencil_select:I = 0x7f0200d3

.field public static final snote_popup_pensetting_preview_alpha:I = 0x7f0200d4

.field public static final snote_popup_preview_bg:I = 0x7f0200d5

.field public static final snote_popup_progress_btn_minus_dim:I = 0x7f0200d6

.field public static final snote_popup_progress_btn_minus_focus:I = 0x7f0200d7

.field public static final snote_popup_progress_btn_minus_normal:I = 0x7f0200d8

.field public static final snote_popup_progress_btn_minus_press:I = 0x7f0200d9

.field public static final snote_popup_progress_btn_plus_dim:I = 0x7f0200da

.field public static final snote_popup_progress_btn_plus_focus:I = 0x7f0200db

.field public static final snote_popup_progress_btn_plus_normal:I = 0x7f0200dc

.field public static final snote_popup_progress_btn_plus_press:I = 0x7f0200dd

.field public static final snote_popup_scroll_handle_f:I = 0x7f0200de

.field public static final snote_popup_scroll_handle_n:I = 0x7f0200df

.field public static final snote_popup_scroll_handle_p:I = 0x7f0200e0

.field public static final snote_popup_textoption_bold:I = 0x7f0200e1

.field public static final snote_popup_textoption_italic:I = 0x7f0200e2

.field public static final snote_popup_textoption_left:I = 0x7f0200e3

.field public static final snote_popup_textoption_right:I = 0x7f0200e4

.field public static final snote_popup_textoption_underline:I = 0x7f0200e5

.field public static final snote_popup_title_bended:I = 0x7f0200e6

.field public static final snote_popup_title_center:I = 0x7f0200e7

.field public static final snote_popup_title_left:I = 0x7f0200e8

.field public static final snote_popup_title_right:I = 0x7f0200e9

.field public static final snote_save_btn_focus:I = 0x7f0200ea

.field public static final snote_save_btn_normal:I = 0x7f0200eb

.field public static final snote_save_btn_press:I = 0x7f0200ec

.field public static final snote_sub_tab_bg:I = 0x7f0200ed

.field public static final snote_sub_tab_bg_focus:I = 0x7f0200ee

.field public static final snote_sub_tab_line:I = 0x7f0200ef

.field public static final snote_sub_tab_selected:I = 0x7f0200f0

.field public static final snote_sub_tab_selected_focus:I = 0x7f0200f1

.field public static final snote_text_all_left:I = 0x7f0200f2

.field public static final snote_text_all_right:I = 0x7f0200f3

.field public static final snote_text_center:I = 0x7f0200f4

.field public static final snote_text_center_press:I = 0x7f0200f5

.field public static final snote_text_icon_bold:I = 0x7f0200f6

.field public static final snote_text_icon_bold_press:I = 0x7f0200f7

.field public static final snote_text_icon_italic:I = 0x7f0200f8

.field public static final snote_text_icon_italic_press:I = 0x7f0200f9

.field public static final snote_text_icon_underline:I = 0x7f0200fa

.field public static final snote_text_icon_underline_press:I = 0x7f0200fb

.field public static final snote_text_left:I = 0x7f0200fc

.field public static final snote_text_left_press:I = 0x7f0200fd

.field public static final snote_text_right:I = 0x7f0200fe

.field public static final snote_text_right_press:I = 0x7f0200ff

.field public static final snote_toolbar_bg_center2_normal:I = 0x7f020100

.field public static final snote_toolbar_bg_center_normal_2:I = 0x7f020101

.field public static final snote_toolbar_bg_right_focus:I = 0x7f020102

.field public static final snote_toolbar_bg_right_focus_2:I = 0x7f020103

.field public static final snote_toolbar_bg_right_normal:I = 0x7f020104

.field public static final snote_toolbar_bg_right_normal_2:I = 0x7f020105

.field public static final snote_toolbar_bg_right_selectedl:I = 0x7f020106

.field public static final snote_toolbar_bg_right_selectedl_2:I = 0x7f020107

.field public static final snote_toolbar_handle:I = 0x7f020108

.field public static final snote_toolbar_handle_2:I = 0x7f020109

.field public static final snote_toolbar_icon_eraser_cancel:I = 0x7f02010a

.field public static final snote_toolbar_icon_eraser_normal:I = 0x7f02010b

.field public static final snote_toolbar_icon_spoid_hover:I = 0x7f02010c

.field public static final text_cue_bottom_press:I = 0x7f02010d

.field public static final text_select_handle_left_2_browser:I = 0x7f02010e

.field public static final text_select_handle_left_browser:I = 0x7f02010f

.field public static final text_select_handle_middle:I = 0x7f020110

.field public static final text_select_handle_reverse:I = 0x7f020111

.field public static final text_select_handle_right_2_browser:I = 0x7f020112

.field public static final text_select_handle_right_browser:I = 0x7f020113

.field public static final tw_list_divider_holo_light:I = 0x7f020114

.field public static final tw_list_selected_holo_light:I = 0x7f020115

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f020116

.field public static final tw_quick_bubble_divider_holo_light:I = 0x7f020117

.field public static final tw_scrollbar_holo_light:I = 0x7f020118

.field public static final tw_spinner_list_focused_holo_light:I = 0x7f020119

.field public static final tw_spinner_list_pressed_holo_light:I = 0x7f02011a

.field public static final vienna_popup_bg:I = 0x7f02011b

.field public static final vienna_popup_bg02:I = 0x7f02011c

.field public static final vienna_popup_title_bg:I = 0x7f02011d

.field public static final vienna_progress_bg:I = 0x7f02011e

.field public static final vienna_progress_bg_alpha:I = 0x7f02011f

.field public static final vienna_progress_shadow:I = 0x7f020120

.field public static final vienna_subtitle_line:I = 0x7f020121

.field public static final zen:I = 0x7f020122

.field public static final zoompad_bg:I = 0x7f020123

.field public static final zoompad_button:I = 0x7f020124

.field public static final zoompad_handle_bottom:I = 0x7f020125

.field public static final zoompad_menu_close:I = 0x7f020126

.field public static final zoompad_menu_down:I = 0x7f020127

.field public static final zoompad_menu_down_dim:I = 0x7f020128

.field public static final zoompad_menu_enter:I = 0x7f020129

.field public static final zoompad_menu_enter_dim:I = 0x7f02012a

.field public static final zoompad_menu_focus:I = 0x7f02012b

.field public static final zoompad_menu_focus_hd:I = 0x7f02012c

.field public static final zoompad_menu_left:I = 0x7f02012d

.field public static final zoompad_menu_left_dim:I = 0x7f02012e

.field public static final zoompad_menu_press:I = 0x7f02012f

.field public static final zoompad_menu_press_hd:I = 0x7f020130

.field public static final zoompad_menu_right:I = 0x7f020131

.field public static final zoompad_menu_right_dim:I = 0x7f020132

.field public static final zoompad_menu_up:I = 0x7f020133

.field public static final zoompad_menu_up_dim:I = 0x7f020134

.field public static final zoompad_selected_handle_normal:I = 0x7f020135

.field public static final zoompad_selected_handle_normal_arabic:I = 0x7f020136

.field public static final zoompad_selected_handle_press:I = 0x7f020137


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
