.class public Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
.super Landroid/widget/RelativeLayout;
.source "DownloadUIItem.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDownloadId:Ljava/lang/String;

.field private mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

.field private mFileName:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    return v0
.end method


# virtual methods
.method public getCheckBox()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 66
    const v0, 0x7f0a000c

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    .line 67
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    return-void
.end method

.method public setChecked(Z)V
    .locals 6
    .param p1, "checked"    # Z

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->isChecked()Z

    move-result v1

    if-ne v1, p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 109
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v0

    .line 113
    .local v0, "curruntView":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 114
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method public setCheckedWithData(Z)V
    .locals 6
    .param p1, "checked"    # Z

    .prologue
    const/4 v4, 0x1

    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->isChecked()Z

    move-result v1

    if-ne v1, p1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v0

    .line 125
    .local v0, "curruntView":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getSelectedIdsCount()I

    move-result v1

    if-ne v1, v4, :cond_2

    const-string v1, "NotFound"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    iget v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCheckedDownloadID(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    iget v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCheckedDownloadID(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez p1, :cond_2

    .line 130
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 134
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method public setData(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "downloadId"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;

    .line 86
    iput p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I

    .line 87
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;

    .line 88
    iput-object p4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;

    .line 89
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->isDownloadSelected(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setCheckedWithData(Z)V

    .line 92
    :cond_0
    return-void
.end method

.method public setDownloadListObj(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V
    .locals 0
    .param p1, "downloadList"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .line 96
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setChecked(Z)V

    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 144
    :cond_0
    return-void

    .line 141
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
