.class Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;
.super Ljava/lang/Object;
.source "DownloadActivity.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->setupViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPopupItemClick(I)Z
    .locals 9
    .param p1, "itemId"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 281
    packed-switch p1, :pswitch_data_0

    .line 326
    :cond_0
    :goto_0
    return v7

    .line 284
    :pswitch_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 285
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 286
    .local v0, "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Landroid/widget/ListView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v4, v1, v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .line 288
    .local v2, "uiItem":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v3

    if-eq v3, v7, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v3

    if-nez v3, :cond_3

    .line 290
    :cond_1
    if-eqz v2, :cond_2

    .line 291
    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setChecked(Z)V

    .line 284
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 293
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getMediaType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v7, v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_2

    .line 298
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_2

    .line 302
    .end local v0    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .end local v2    # "uiItem":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    goto :goto_0

    .line 305
    .end local v1    # "i":I
    :pswitch_1
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 306
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 307
    .restart local v0    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Landroid/widget/ListView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v4, v1, v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .line 309
    .restart local v2    # "uiItem":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v3

    if-eq v3, v7, :cond_5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v3

    if-nez v3, :cond_7

    .line 311
    :cond_5
    if-eqz v2, :cond_6

    .line 312
    invoke-virtual {v2, v8}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setChecked(Z)V

    .line 305
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 314
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getMediaType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v8, v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_4

    .line 319
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_4

    .line 281
    :pswitch_data_0
    .packed-switch 0x7f0a0026
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
