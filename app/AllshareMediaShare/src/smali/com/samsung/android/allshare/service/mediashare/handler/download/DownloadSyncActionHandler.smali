.class public Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.source "DownloadSyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler$1;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "com.sec.android.allshare.action.ACTION_DOWNLOAD_REQUEST"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method protected finiHandler()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method getItemType(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item$MediaType;
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 116
    :goto_0
    return-object v1

    .line 113
    :cond_0
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "typeStr":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 115
    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {v0}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v1

    goto :goto_0
.end method

.method protected initHandler()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 12
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "actionID":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 72
    .local v2, "bundle":Landroid/os/Bundle;
    const/4 v7, 0x0

    .line 74
    .local v7, "result":Z
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.sec.android.allshare.action.ACTION_DOWNLOAD_REQUEST"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 75
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v1, "b":Landroid/os/Bundle;
    const-string v10, "BUNDLE_STRING_DEVICE_NAME"

    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 77
    .local v8, "serverName":Ljava/lang/String;
    const-string v10, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 80
    .local v3, "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v6, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v9, 0x0

    .line 83
    .local v9, "type":Lcom/samsung/android/allshare/Item$MediaType;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 84
    .local v5, "item":Landroid/os/Bundle;
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler;->getItemType(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v9

    .line 85
    if-eqz v9, :cond_0

    .line 88
    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 90
    :pswitch_0
    new-instance v10, Lcom/samsung/android/allshare/extension/impl/SimpleAudioItem;

    invoke-direct {v10, v5}, Lcom/samsung/android/allshare/extension/impl/SimpleAudioItem;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :pswitch_1
    new-instance v10, Lcom/samsung/android/allshare/extension/impl/SimpleImageItem;

    invoke-direct {v10, v5}, Lcom/samsung/android/allshare/extension/impl/SimpleImageItem;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :pswitch_2
    new-instance v10, Lcom/samsung/android/allshare/extension/impl/SimpleVideoItem;

    invoke-direct {v10, v5}, Lcom/samsung/android/allshare/extension/impl/SimpleVideoItem;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    .end local v5    # "item":Landroid/os/Bundle;
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getDownlaodManager()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-result-object v10

    invoke-virtual {v10, v8, v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->addDownloadList(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v7

    .line 102
    const-string v10, "BUNDLE_BOOLEAN_RESULT"

    invoke-virtual {v1, v10, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v10

    .line 107
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v3    # "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    .end local v8    # "serverName":Ljava/lang/String;
    .end local v9    # "type":Lcom/samsung/android/allshare/Item$MediaType;
    :goto_1
    return-object v10

    :cond_2
    const/4 v10, 0x0

    invoke-virtual {p0, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/download/DownloadSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v10

    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
