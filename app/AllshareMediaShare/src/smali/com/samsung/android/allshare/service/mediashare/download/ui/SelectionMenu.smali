.class public Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;
.super Ljava/lang/Object;
.source "SelectionMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectionMenu"


# instance fields
.field private final mButton:Landroid/widget/Button;

.field private mContentSize:I

.field private final mContext:Landroid/content/Context;

.field private final mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Button;Landroid/widget/LinearLayout;ILcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "button"    # Landroid/widget/Button;
    .param p3, "linearLayout"    # Landroid/widget/LinearLayout;
    .param p4, "contentSize"    # I
    .param p5, "listener"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContentSize:I

    .line 42
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    .line 44
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0, p5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->setOnPopupItemClickListener(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;)V

    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    iput p4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContentSize:I

    .line 48
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->dismiss()V

    .line 65
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->show()V

    .line 53
    return-void
.end method

.method public setContextSize(I)V
    .locals 0
    .param p1, "contentSize"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContentSize:I

    .line 61
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public updateItem(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const v5, 0x7f0a0027

    const v4, 0x7f0a0026

    const v3, 0x7f070015

    const v2, 0x7f070014

    .line 68
    if-nez p1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->clearItems()V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->addItem(ILjava/lang/String;)V

    .line 79
    :goto_0
    return-void

    .line 71
    :cond_0
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContentSize:I

    if-ne p1, v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->clearItems()V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->addItem(ILjava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->clearItems()V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->addItem(ILjava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mPopupList:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->addItem(ILjava/lang/String;)V

    goto :goto_0
.end method
