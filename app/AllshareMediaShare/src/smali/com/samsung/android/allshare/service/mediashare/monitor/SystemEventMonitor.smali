.class public Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
.super Ljava/lang/Object;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$3;
    }
.end annotation


# static fields
.field private static final AIRPLANE_MODE_OFF:I = 0x0

.field private static final AIRPLANE_MODE_ON:I = 0x1


# instance fields
.field private handler:Landroid/os/Handler;

.field private handlerThread:Landroid/os/HandlerThread;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBssid:Ljava/lang/String;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mEth0InterfaceName:Ljava/lang/String;

.field private mFeatureSupportEthernet:Z

.field private mIsEthernetAvailable:Z

.field private mIsP2pAvailable:Z

.field private mP2pInterfaceName:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field private mStarted:Z

.field private final mTag:Ljava/lang/String;

.field private mWifiInterfaceName:Ljava/lang/String;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 62
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mEth0InterfaceName:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBssid:Ljava/lang/String;

    .line 74
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    .line 80
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    .line 292
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    .line 352
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 354
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    .line 452
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 626
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 86
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->init()V

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyByHandler(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkWifiState(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkP2PState(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Context;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getAirplaneModeState(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkAirplaneModeState(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkScreenState(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkEthernetState(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkHotspotState(Landroid/content/Intent;)V

    return-void
.end method

.method private checkAirplaneModeState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 232
    packed-switch p1, :pswitch_data_0

    .line 246
    :goto_0
    return-void

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkAirplaneModeState"

    const-string v2, "AIRPLANE MODE: OFF"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v0, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_OFF"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 239
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkAirplaneModeState"

    const-string v2, "AIRPLANE MODE: ON"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 243
    const-string v0, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_ON"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkEthernetState(Z)Z
    .locals 9
    .param p1, "isNotify"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 295
    iget-boolean v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-nez v4, :cond_0

    .line 346
    :goto_0
    return v3

    .line 299
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v5, "checkEthernetState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "is notify: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    .line 302
    .local v2, "state":Landroid/net/NetworkInfo$DetailedState;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 304
    .local v0, "netInfoEthernet":Landroid/net/NetworkInfo;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 307
    .local v1, "netInfoWifi":Landroid/net/NetworkInfo;
    if-nez v0, :cond_2

    .line 308
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    .line 315
    :goto_1
    sget-object v4, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    invoke-virtual {v2}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 346
    :cond_1
    :goto_2
    iget-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    goto :goto_0

    .line 310
    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    goto :goto_1

    .line 317
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkEthernetState"

    const-string v5, "Ethernet Interface (eth0): CONNECTED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iput-boolean v8, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    .line 319
    if-eqz p1, :cond_1

    .line 321
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 322
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkEthernetState"

    const-string v5, "WIFI STATE: CONNECTED, disable it!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v3, "com.sec.android.allshare.event.EVENT_WIFI_DISABLED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    .line 326
    :cond_3
    const-string v3, "com.sec.android.allshare.event.EVENT_ETHERNET_CONNECTED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_2

    .line 331
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v5, "checkEthernetState"

    const-string v6, "Ethernet Interface (eth0): DISCONNECTED"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iput-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    .line 333
    if-eqz p1, :cond_1

    .line 334
    const-string v3, "com.sec.android.allshare.event.EVENT_ETHERNET_DISCONNECTED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    .line 337
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 338
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkEthernetState"

    const-string v5, "WIFI STATE: CONNECTED, enable it!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v3, "com.sec.android.allshare.event.EVENT_WIFI_ENABLED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_2

    .line 315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkHotspotState(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 270
    const-string v1, "wifi_state"

    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 273
    .local v0, "state":I
    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "checkHotspotState"

    const-string v3, "WIFI AP STATE: DISABLED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_DISABLED"

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    .line 282
    :goto_0
    return-void

    .line 276
    :cond_0
    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "checkHotspotState"

    const-string v3, "WIFI AP STATE: ENABLED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_ENABLED"

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 280
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v2, "checkHotspotState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WIFI AP STATE: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkP2PState(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 201
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 202
    .local v1, "state":Landroid/net/NetworkInfo$State;
    const-string v2, "networkInfo"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 205
    .local v0, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    .line 210
    :goto_0
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 225
    :goto_1
    return-void

    .line 208
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    goto :goto_0

    .line 212
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: CONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 216
    const-string v2, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 220
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: DISCONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 222
    const-string v2, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 210
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkScreenState(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 255
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: ON"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v0, "com.sec.android.allshare.event.EVENT_SCREEN_ON"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: OFF"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v0, "com.sec.android.allshare.event.EVENT_SCREEN_OFF"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "checkScreenState"

    const-string v2, "SCREEN STATE: USER_PRESENT"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v0, "com.sec.android.allshare.event.EVENT_USER_PRESENT"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkWifiState(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    const-string v3, "wifi_state"

    const/4 v4, 0x4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 153
    .local v2, "wifiState":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 154
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkWifiState"

    const-string v5, "WIFI STATE: DISABLED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v3, "com.sec.android.allshare.event.EVENT_WIFI_DISABLED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 161
    .local v1, "state":Landroid/net/NetworkInfo$State;
    const-string v3, "networkInfo"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 164
    .local v0, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_2

    .line 165
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    .line 167
    :cond_2
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 171
    :pswitch_0
    iget-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkEthernetState(Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 172
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkWifiState"

    const-string v5, "The eth0 is connected, so use eth0"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getIpAddress()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getBssid()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->isAPChanged()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 182
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkWifiState"

    const-string v5, "WIFI STATE: AP CHANGED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v3, "com.sec.android.allshare.event.EVENT_AP_CHANGED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkWifiState"

    const-string v5, "WIFI STATE: CONNECTED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v3, "com.sec.android.allshare.event.EVENT_WIFI_ENABLED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v4, "checkWifiState"

    const-string v5, "WIFI STATE: DISCONNECTED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v3, "com.sec.android.allshare.event.EVENT_WIFI_DISABLED"

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->notifyEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAirplaneModeState(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "airplane_mode_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getBssid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIpAddress()I
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 94
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 96
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 98
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    .line 102
    const-string v0, "wlan0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    .line 106
    const-string v0, "p2p-wlan0-0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 111
    const-string v0, "eth0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mEth0InterfaceName:Ljava/lang/String;

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    .line 114
    return-void
.end method

.method private isAPChanged()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 533
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBssid:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 534
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->setBssid()V

    .line 544
    :cond_0
    :goto_0
    return v0

    .line 538
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBssid:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getBssid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 542
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->setBssid()V

    .line 544
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private notifyByHandler(Ljava/lang/String;)V
    .locals 3
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 386
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 387
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "com.sec.android.allshare.event.EVENT_PARAMETER_KEY1"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 389
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 394
    :cond_0
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 399
    :cond_1
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 400
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 404
    :cond_2
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.sec.android.allshare.event.EVENT_AP_CHANGED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 406
    :cond_3
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 410
    :cond_4
    const-string v1, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_ON"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.sec.android.allshare.event.EVENT_AIRPLANE_MODE_OFF"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 412
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 415
    :cond_6
    const-string v1, "com.sec.android.allshare.event.EVENT_SCREEN_ON"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 416
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 417
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->setScreenStatus(Z)V

    .line 420
    :cond_7
    const-string v1, "com.sec.android.allshare.event.EVENT_SCREEN_OFF"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 421
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 422
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->setScreenStatus(Z)V

    .line 425
    :cond_8
    const-string v1, "com.sec.android.allshare.event.EVENT_USER_PRESENT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 426
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 429
    :cond_9
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v1, :cond_a

    const-string v1, "com.sec.android.allshare.event.EVENT_ETHERNET_CONNECTED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 430
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mEth0InterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 434
    :cond_a
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v1, :cond_b

    const-string v1, "com.sec.android.allshare.event.EVENT_ETHERNET_DISCONNECTED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 435
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mEth0InterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 439
    :cond_b
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 441
    :cond_c
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 444
    :cond_d
    return-void
.end method

.method private notifyEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 362
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 363
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "SystemEventMonitorHandlerThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 364
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 365
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$1;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    .line 379
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 380
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 381
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 382
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 383
    return-void
.end method

.method private setBssid()V
    .locals 1

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getBssid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBssid:Ljava/lang/String;

    .line 525
    return-void
.end method


# virtual methods
.method public fini()V
    .locals 2

    .prologue
    .line 561
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v0, :cond_0

    .line 562
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 563
    :cond_0
    return-void
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNetworkAvailable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 629
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiInterfaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    .line 637
    :goto_0
    return v0

    .line 631
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mEth0InterfaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    goto :goto_0

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 635
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    goto :goto_0

    .line 637
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNetworkConnect()Z
    .locals 8

    .prologue
    .line 645
    const/4 v0, 0x0

    .line 646
    .local v0, "isWifiConnect":Z
    const/4 v1, 0x0

    .line 649
    .local v1, "isWifiP2pConnect":Z
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 650
    .local v2, "mWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    .line 651
    if-eqz v0, :cond_0

    .line 682
    .end local v0    # "isWifiConnect":Z
    :goto_0
    return v0

    .line 654
    .restart local v0    # "isWifiConnect":Z
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v5, "isWifiConnect"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isWifiConnect is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    iget-boolean v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v4, :cond_2

    .line 658
    iget-boolean v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    if-eqz v4, :cond_1

    .line 659
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    goto :goto_0

    .line 661
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v5, "isEthernetConnect"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isEthernetConnect is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mIsEthernetAvailable:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    :cond_2
    sget-object v3, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 667
    .local v3, "state":Landroid/net/NetworkInfo$State;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 668
    sget-object v4, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v3}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 678
    :goto_1
    if-nez v1, :cond_3

    .line 679
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v5, "isWifiConnect"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isWifiP2pConnect is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v1

    .line 682
    goto :goto_0

    .line 670
    :pswitch_0
    const/4 v1, 0x1

    .line 671
    goto :goto_1

    .line 674
    :pswitch_1
    const/4 v1, 0x0

    goto :goto_1

    .line 668
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isScreenOn()Z
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public startMonitor()V
    .locals 3

    .prologue
    .line 569
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v1, :cond_0

    .line 607
    :goto_0
    return-void

    .line 572
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 575
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 576
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 577
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 578
    const-string v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 581
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 582
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 583
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 584
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 587
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 590
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z

    if-eqz v1, :cond_1

    .line 591
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 595
    :cond_1
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 598
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 599
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 602
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 604
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 606
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    goto :goto_0
.end method

.method public stopMonitor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 613
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    if-nez v0, :cond_1

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 617
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mStarted:Z

    .line 619
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 621
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 622
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    goto :goto_0
.end method
