.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "PlaylistPlayerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaylistPlayerActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->resumePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->stopPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->getPlaylistPosition(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "x2"    # Z

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->setAutoFlipMode(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "x2"    # Z

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->setQuickNavigate(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "x4"    # I

    .prologue
    .line 165
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playPlaylistPlayer(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "x2"    # I

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->seekPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playNextPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playPerviousPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->pausePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method private getPlaylistPosition(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 4
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 363
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayPositionInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 365
    .local v0, "result":I
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responseBundle(I)V

    .line 367
    return-void
.end method

.method private pausePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 5
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 299
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->pause(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v1

    .line 301
    .local v1, "result":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 303
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 304
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 309
    return-void

    .line 306
    :cond_0
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private playNextPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 6
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 357
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const-string v3, ""

    const/4 v4, 0x1

    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 358
    .local v0, "result":I
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responseBundle(I)V

    .line 360
    return-void
.end method

.method private playPerviousPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 6
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 351
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const-string v3, ""

    const/4 v4, -0x1

    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 352
    .local v0, "result":I
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responseBundle(I)V

    .line 354
    return-void
.end method

.method private playPlaylistPlayer(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    .locals 15
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "trackNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "playlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    if-nez p1, :cond_0

    .line 250
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responsePlayFail(Ljava/util/ArrayList;ILjava/lang/String;)V

    .line 296
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1502(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 254
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/Bundle;

    .line 255
    .local v10, "bundle":Landroid/os/Bundle;
    if-eqz v10, :cond_1

    .line 257
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v2, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v10, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 263
    .end local v10    # "bundle":Landroid/os/Bundle;
    :cond_2
    if-ltz p4, :cond_3

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p4

    if-lt v0, v2, :cond_4

    .line 264
    :cond_3
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responsePlayFail(Ljava/util/ArrayList;ILjava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    move/from16 v0, p4

    # setter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I
    invoke-static {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1602(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;I)I

    .line 269
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$202(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Z)Z

    .line 270
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # setter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$102(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;

    .line 272
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 273
    .local v13, "uri":Landroid/net/Uri;
    const-string v2, "URI"

    invoke-virtual {v13, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 274
    .local v4, "uriStr":Ljava/lang/String;
    new-instance v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    invoke-direct {v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;-><init>()V

    .line 276
    .local v9, "albumDetails":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 277
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    const-wide/16 v6, 0x0

    new-instance v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual/range {v2 .. v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    move-result v12

    .line 280
    .local v12, "result":I
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 282
    .restart local v10    # "bundle":Landroid/os/Bundle;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 283
    .local v14, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    move-object/from16 v0, p1

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 286
    const-string v2, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {v10, v2, v14}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 287
    const-string v2, "BUNDLE_STRING_MIME_TYPE"

    move-object/from16 v0, p2

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v2, "BUNDLE_INT_PLAYLIST_TRACKNUMBER"

    move/from16 v0, p4

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 290
    if-nez v12, :cond_5

    .line 291
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :goto_2
    invoke-virtual {p0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 293
    :cond_5
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private responseBundle(I)V
    .locals 3
    .param p1, "result"    # I

    .prologue
    .line 376
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 378
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez p1, :cond_0

    .line 379
    const-string v1, "BUNDLE_ENUM_ERROR"

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 384
    return-void

    .line 381
    :cond_0
    const-string v1, "BUNDLE_ENUM_ERROR"

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private responsePlayFail(Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 2
    .param p2, "trackNumber"    # I
    .param p3, "errMsg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "playlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 389
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 390
    const-string v1, "BUNDLE_INT_TRACKNUM"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 391
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 395
    return-void
.end method

.method private resumePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 5
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 312
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->resume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v1

    .line 314
    .local v1, "result":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 317
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 322
    return-void

    .line 319
    :cond_0
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private seekPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    .locals 7
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "trackNumber"    # I

    .prologue
    .line 339
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const-string v4, ""

    const/4 v5, 0x0

    new-instance v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v1

    .line 340
    .local v1, "result":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 341
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_INT_TRACKNUM"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 343
    if-nez v1, :cond_0

    .line 344
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 348
    return-void

    .line 346
    :cond_0
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setAutoFlipMode(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    .locals 0
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "onoff"    # Z

    .prologue
    .line 370
    return-void
.end method

.method private setQuickNavigate(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    .locals 0
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "onoff"    # Z

    .prologue
    .line 373
    return-void
.end method

.method private stopPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 5
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 325
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v1

    .line 326
    .local v1, "result":I
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$202(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Z)Z

    .line 328
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 330
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 331
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 336
    return-void

    .line 333
    :cond_0
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 169
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 236
    return-void
.end method
