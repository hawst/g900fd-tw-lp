.class public Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
.super Ljava/lang/Object;
.source "DownloadItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$1;,
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;,
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$FailReason;,
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$DownloadStatus;,
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$DownloadLocation;
    }
.end annotation


# instance fields
.field private mContentItem:Lcom/samsung/android/allshare/Item;

.field private mCreatedFile:Ljava/io/File;

.field private mDate:Ljava/util/Date;

.field private mDownloadSize:J

.field private mDownloadStatus:I

.field private mDownloadTo:I

.field private mFailReason:I

.field private mIsCanceled:Z

.field private mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

.field private mSavePath:Ljava/lang/String;

.field private mServerName:Ljava/lang/String;

.field private thumbnail:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item;I)V
    .locals 4
    .param p1, "server"    # Ljava/lang/String;
    .param p2, "savePath"    # Ljava/lang/String;
    .param p3, "item"    # Lcom/samsung/android/allshare/Item;
    .param p4, "downloadTo"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mCreatedFile:Ljava/io/File;

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    .line 28
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    .line 30
    iput-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mIsCanceled:Z

    .line 32
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDate:Ljava/util/Date;

    .line 34
    iput v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mFailReason:I

    .line 40
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->thumbnail:Landroid/graphics/Bitmap;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mServerName:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mSavePath:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    .line 77
    iput p4, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadTo:I

    .line 78
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDate:Ljava/util/Date;

    .line 79
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mIsCanceled:Z

    .line 147
    return-void
.end method

.method public getContentItem()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public getCreatedFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mCreatedFile:Ljava/io/File;

    return-object v0
.end method

.method public getCreatedFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mCreatedFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mCreatedFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 254
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mServerName:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadSize()J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    return-wide v0
.end method

.method public getDownloadTo()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadTo:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 208
    const-string v0, ""

    :goto_0
    return-object v0

    .line 202
    :pswitch_0
    const-string v0, "audio/*"

    goto :goto_0

    .line 204
    :pswitch_1
    const-string v0, "image/*"

    goto :goto_0

    .line 206
    :pswitch_2
    const-string v0, "video/*"

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getReason()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mFailReason:I

    return v0
.end method

.method public getServerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mServerName:Ljava/lang/String;

    return-object v0
.end method

.method public getSevePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    .line 122
    const-wide/16 v0, 0x0

    .line 123
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    return v0
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->thumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    .line 110
    const-string v0, "No Content"

    .line 111
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType()Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    .line 104
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    goto :goto_0
.end method

.method public getURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mContentItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method isCanceled()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mIsCanceled:Z

    return v0
.end method

.method notifyCancelled()V
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->cancel()V

    .line 191
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    .line 192
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 193
    return-void
.end method

.method notifyError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 167
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 168
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;->onError(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Ljava/lang/Exception;)V

    .line 170
    :cond_0
    return-void
.end method

.method notifyFinish(Ljava/io/File;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const-wide/16 v4, 0x0

    .line 173
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 174
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mCreatedFile:Ljava/io/File;

    .line 176
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getContentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_LONG_ITEM_FILE_SIZE"

    iget-wide v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v2

    invoke-interface {v0, p0, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;->onFinish(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;J)V

    .line 186
    :cond_0
    :goto_0
    iput-wide v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    .line 187
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;->onFinish(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    goto :goto_0
.end method

.method notifyProgress(J)V
    .locals 3
    .param p1, "readBytes"    # J

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    .line 162
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;->onProgress(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    .line 164
    :cond_0
    return-void
.end method

.method notifyStart(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 154
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 155
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadSize:J

    .line 156
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;->onStart(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    .line 158
    :cond_0
    return-void
.end method

.method public setReason(I)V
    .locals 0
    .param p1, "reason"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mFailReason:I

    .line 226
    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 216
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mDownloadStatus:I

    .line 217
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->thumbnail:Landroid/graphics/Bitmap;

    .line 244
    return-void
.end method

.method setTransportListener(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;

    .line 143
    return-void
.end method
