.class public Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;
.super Ljava/lang/Thread;
.source "MessageAllocateTask.java"


# static fields
.field private static final mActionFINI:Ljava/lang/String; = "com.samsung.android.allshare.framework.io.ACTION_FINI"


# instance fields
.field private mQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/sec/android/allshare/iface/CVMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field private mStopFlag:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 34
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mStopFlag:Z

    .line 38
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 44
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 46
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 47
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mStopFlag:Z

    .line 49
    return-void
.end method

.method private handleAllocateMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getMsgType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 104
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Received finalize message..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :goto_0
    const/4 v0, 0x0

    .line 113
    :goto_1
    return v0

    .line 98
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Oops~~. Undefined Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 101
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->handleRequestMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z

    goto :goto_2

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v1, "handleAllocateMessage"

    const-string v2, "Oops~~. Invalid Context Variable type..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleRequestMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 7
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "action_id":Ljava/lang/String;
    const/4 v1, 0x0

    .line 72
    .local v1, "handler":Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v3, :cond_1

    .line 73
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getAsyncActionHandlerMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;

    .line 74
    .restart local v1    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
    if-nez v1, :cond_0

    .line 75
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v4, "handleRequestMessage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " No such action handler :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :goto_0
    return v2

    .line 79
    :cond_0
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->responseAsyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 81
    const/4 v2, 0x1

    goto :goto_0

    .line 83
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v4, "handleRequestMessage"

    const-string v5, "mServiceManager is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 4
    .param p1, "msg"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    const/4 v1, 0x1

    :goto_0
    monitor-exit p0

    return v1

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v2, "putQ"

    const-string v3, "putQ InterruptedException "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    const/4 v1, 0x0

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 5

    .prologue
    .line 165
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/iface/CVMessage;

    .line 166
    .local v1, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->handleAllocateMessage(Lcom/sec/android/allshare/iface/CVMessage;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    .end local v1    # "msg":Lcom/sec/android/allshare/iface/CVMessage;
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mStopFlag:Z

    if-eqz v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 179
    return-void

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v3, "run"

    const-string v4, "run InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setStopFlag()V
    .locals 5

    .prologue
    .line 130
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mTag:Ljava/lang/String;

    const-string v2, "setStopFlag"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mStopFlag = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mStopFlag:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->mStopFlag:Z

    .line 132
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 133
    .local v0, "fin":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v1, "com.samsung.android.allshare.framework.io.ACTION_FINI"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z

    .line 136
    return-void
.end method
