.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.source "PlaylistPlayerSyncActionHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_IMAGE_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 46
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 47
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method protected finiHandler()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method protected initHandler()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 8
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v7, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "actionID":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 77
    .local v4, "reqb":Landroid/os/Bundle;
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "id":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v2

    .line 86
    .local v2, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v2, :cond_0

    .line 87
    invoke-virtual {p0, v0, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    .line 113
    :goto_0
    return-object v5

    .line 90
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 92
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v1, "b":Landroid/os/Bundle;
    const-string v5, "BUNDLE_BOOLEAN_SUPPORT_AUDIO_PLAYLIST_PLAYER"

    iget-boolean v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportAudioPlayList:Z

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 96
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    goto :goto_0

    .line 97
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 99
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 101
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v5, "BUNDLE_BOOLEAN_SUPPORT_VIDEO_PLAYLIST_PLAYER"

    iget-boolean v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportVideoPlayList:Z

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    goto :goto_0

    .line 104
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_IMAGE_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 106
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 108
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v5, "BUNDLE_BOOLEAN_SUPPORT_PLAYLIST_PLAYER"

    iget-boolean v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportImagePlayList:Z

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    goto :goto_0

    .line 113
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_3
    invoke-virtual {p0, v0, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    goto :goto_0
.end method
