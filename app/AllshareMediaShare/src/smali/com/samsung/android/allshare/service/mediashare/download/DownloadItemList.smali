.class public Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
.super Ljava/util/ArrayList;
.source "DownloadItemList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mCurrentIndex:I

.field private mCurrentSize:J

.field private mDownloadedSize:J

.field private mIsThumbnailMax:Z

.field private mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

.field private mTag:Ljava/lang/String;

.field private mTotalCancelledCount:I

.field private mTotalFailedCount:I

.field private mTotalSize:J

.field private mTotalSuccessCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTag:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mIsThumbnailMax:Z

    .line 34
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 138
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTag:Ljava/lang/String;

    const-string v3, "clear"

    const-string v4, "init DownloadItemList"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iput v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSuccessCount:I

    .line 141
    iput v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalFailedCount:I

    .line 142
    iput v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalCancelledCount:I

    .line 143
    iput-wide v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSize:J

    .line 144
    iput-wide v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mDownloadedSize:J

    .line 145
    iput-wide v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentSize:J

    .line 146
    iput v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    .line 147
    iput-boolean v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mIsThumbnailMax:Z

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 149
    .local v1, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 150
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 153
    .end local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :cond_1
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 154
    return-void
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    return v0
.end method

.method public getCurrentSize()J
    .locals 4

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDownloadSize()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentSize:J

    .line 106
    iget-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentSize:J

    return-wide v0
.end method

.method public getDownloadedSize()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mDownloadedSize:J

    return-wide v0
.end method

.method public getTotalCancelledCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalCancelledCount:I

    return v0
.end method

.method public getTotalFailedCount()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalFailedCount:I

    return v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSize:J

    return-wide v0
.end method

.method public getTotalSuccessCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSuccessCount:I

    return v0
.end method

.method public isComplete()Z
    .locals 2

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSuccessCount:I

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalFailedCount:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalCancelledCount:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 123
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isThumbnailMax()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mIsThumbnailMax:Z

    return v0
.end method

.method public moveToNextItem()Z
    .locals 3

    .prologue
    .line 158
    :cond_0
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    .line 160
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 161
    :cond_1
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    .line 162
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTag:Ljava/lang/String;

    const-string v1, "moveToNextItem"

    const-string v2, "return false, download complete!!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    .line 168
    :goto_0
    return v0

    .line 166
    :cond_2
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setCurrentIndex(I)V
    .locals 0
    .param p1, "currentIndex"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentIndex:I

    .line 119
    return-void
.end method

.method public setCurrentSize(J)V
    .locals 1
    .param p1, "currentSize"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mCurrentSize:J

    .line 111
    return-void
.end method

.method public setDownloadListListener(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    .line 44
    return-void
.end method

.method public setDownloadedSize(J)V
    .locals 1
    .param p1, "downloadedSize"    # J

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mDownloadedSize:J

    .line 102
    return-void
.end method

.method public setThumbnailMax()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mIsThumbnailMax:Z

    .line 134
    return-void
.end method

.method public setTotalCancelledCount(I)V
    .locals 0
    .param p1, "TotalCancelledCount"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalCancelledCount:I

    .line 86
    return-void
.end method

.method public setTotalFailedCount(I)V
    .locals 0
    .param p1, "TotalFailedCount"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalFailedCount:I

    .line 78
    return-void
.end method

.method public setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSize:J

    .line 94
    return-void
.end method

.method public setTotalSuccessCount(I)V
    .locals 0
    .param p1, "TotalSuccessCount"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mTotalSuccessCount:I

    .line 70
    return-void
.end method

.method public updateList()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;->updateList()V

    .line 56
    :cond_0
    return-void
.end method

.method public updateProgress()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;->updateProgress()V

    .line 50
    :cond_0
    return-void
.end method

.method public updateSelectAll(Ljava/lang/String;)V
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->mListener:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;

    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;->updateSelectAll(Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method
