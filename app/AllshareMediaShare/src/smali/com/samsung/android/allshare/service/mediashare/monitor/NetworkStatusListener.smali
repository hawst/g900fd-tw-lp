.class public Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;
.super Ljava/lang/Object;
.source "NetworkStatusListener.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;
    }
.end annotation


# static fields
.field private static final MAX_NETWORK_INFO_RETRY:I = 0x2

.field private static final MAX_SUBMASK_LENGTH:S = 0x20s

.field private static final TAG:Ljava/lang/String; = "NetworkStatusListener"


# instance fields
.field private getNetworkInfo_retrying:I

.field private mHotspotEnabled:Z

.field private mWifiEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    .line 31
    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mWifiEnabled:Z

    .line 33
    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mHotspotEnabled:Z

    .line 231
    return-void
.end method

.method private getNetworkInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;
    .locals 11
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 138
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    .line 139
    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 140
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 141
    .local v4, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 142
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "finding NetworkInterface "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "... "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    :catch_0
    move-exception v2

    .line 176
    .local v2, "ex":Ljava/net/SocketException;
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SocketException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .end local v2    # "ex":Ljava/net/SocketException;
    :cond_1
    iget v7, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    .line 180
    iget v7, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    const/4 v8, 0x2

    if-le v7, v8, :cond_6

    .line 181
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    const-string v9, "FAIL to getNetworkInfo"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v7, 0x0

    .line 186
    :goto_1
    return-object v7

    .line 146
    .restart local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v4    # "intf":Ljava/net/NetworkInterface;
    :cond_2
    :try_start_1
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NetworkInterface found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " == "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 150
    .local v1, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 151
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 152
    .local v3, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 153
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    const-string v9, "skip InetAddress LoopbackAddress"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 158
    :cond_3
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    .line 159
    .local v5, "ip":Ljava/lang/String;
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "InetAddress found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "%"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 161
    :cond_4
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "skip...this is IPv6: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "||"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v5, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ||"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%"

    invoke-virtual {v5, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 169
    :cond_5
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "this is IPv4 we want: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0, v5, v4}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getSubnetMask(Ljava/lang/String;Ljava/net/NetworkInterface;)Ljava/lang/String;

    move-result-object v6

    .line 172
    .local v6, "subnetMask":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;

    invoke-direct {v7, p0, v5, v6}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;-><init>(Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 185
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v1    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    .end local v5    # "ip":Ljava/lang/String;
    .end local v6    # "subnetMask":Ljava/lang/String;
    :cond_6
    const-string v7, "NetworkStatusListener"

    const-string v8, "getNetworkInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "retry getNetworkInfo again... "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;

    move-result-object v7

    goto/16 :goto_1
.end method

.method private getSubnetMask(Ljava/lang/String;Ljava/net/NetworkInterface;)Ljava/lang/String;
    .locals 12
    .param p1, "ip"    # Ljava/lang/String;
    .param p2, "networkInterface"    # Ljava/net/NetworkInterface;

    .prologue
    const/4 v7, 0x0

    .line 191
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 192
    :cond_0
    const-string v8, "NetworkStatusListener"

    const-string v9, "getSubnetMask"

    const-string v10, "the parameter is null !"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :goto_0
    return-object v7

    .line 197
    :cond_1
    const/4 v5, 0x0

    .line 198
    .local v5, "networkPrefixLength":I
    invoke-virtual {p2}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v4

    .line 200
    .local v4, "intfAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InterfaceAddress;

    .line 201
    .local v3, "interAddr":Ljava/net/InterfaceAddress;
    invoke-virtual {v3}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 202
    .local v2, "inetAddr":Ljava/net/InetAddress;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 203
    invoke-virtual {v3}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    move-result v5

    .line 208
    .end local v2    # "inetAddr":Ljava/net/InetAddress;
    .end local v3    # "interAddr":Ljava/net/InterfaceAddress;
    :cond_3
    const/16 v8, 0x20

    if-lt v5, v8, :cond_4

    .line 209
    const-string v8, "NetworkStatusListener"

    const-string v9, "getSubnetMask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid subnet mask length: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_4
    const/high16 v6, -0x80000000

    .line 216
    .local v6, "shiftby":I
    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_5

    .line 220
    shr-int/lit8 v6, v6, 0x1

    .line 216
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 225
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v8, v6, 0x18

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    shr-int/lit8 v8, v6, 0x10

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    shr-int/lit8 v8, v6, 0x8

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    and-int/lit16 v8, v6, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0
.end method

.method private notifyNICAdded(Ljava/lang/String;)V
    .locals 6
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 98
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NIC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo_retrying:I

    .line 103
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->getNetworkInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;

    move-result-object v0

    .line 104
    .local v0, "info":Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->ip:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->subnetMask:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 105
    :cond_0
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    const-string v4, "The network info is null !"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ip: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->ip:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", subnet mask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->subnetMask:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->ip:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener$NetworkInfo;->subnetMask:Ljava/lang/String;

    invoke-virtual {v2, p1, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 114
    .local v1, "result":I
    if-eqz v1, :cond_1

    .line 115
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify to core failed, return value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyNICRemoved(Ljava/lang/String;)V
    .locals 6
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICRemoved"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NIC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->notifyNICRemoved(Ljava/lang/String;)I

    move-result v1

    .line 125
    .local v1, "result":I
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance()Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v0

    .line 126
    .local v0, "deviceStateHandler":Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopNICTask(Ljava/lang/String;)V

    .line 130
    :cond_0
    if-eqz v1, :cond_1

    .line 131
    const-string v2, "NetworkStatusListener"

    const-string v3, "notifyNICRemoved"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify to core failed, return value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_1
    return-void
.end method


# virtual methods
.method public eventNotifyReceived(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 38
    :cond_0
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "the parameter is null !"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_1
    :goto_0
    return-void

    .line 42
    :cond_2
    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "nic":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 44
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "this is not NIC related event!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 49
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "invalid empty NIC name!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_4
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.sec.android.allshare.event.EVENT_AP_CHANGED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 55
    :cond_5
    iput-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mWifiEnabled:Z

    .line 56
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_6
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 58
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mWifiEnabled:Z

    .line 60
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mHotspotEnabled:Z

    if-nez v1, :cond_7

    .line 61
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICRemoved(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_7
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "Hotspot mode is enabled, so NIC needn\'t remove."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_8
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 67
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_9
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_P2P_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 69
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICRemoved(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_a
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_ENABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 71
    iput-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mHotspotEnabled:Z

    .line 72
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_b
    const-string v1, "com.sec.android.allshare.event.EVENT_WIFI_AP_DISABLED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mHotspotEnabled:Z

    .line 76
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->mWifiEnabled:Z

    if-nez v1, :cond_c

    .line 77
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;->notifyNICRemoved(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 79
    :cond_c
    const-string v1, "NetworkStatusListener"

    const-string v2, "eventNotifyReceived"

    const-string v3, "WiFi connected, so NIC needn\'t remove."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
