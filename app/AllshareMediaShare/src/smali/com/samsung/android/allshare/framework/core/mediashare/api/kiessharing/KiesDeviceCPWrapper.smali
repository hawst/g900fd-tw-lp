.class public Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
.super Ljava/lang/Object;
.source "KiesDeviceCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;


# instance fields
.field private kiesActionListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;",
            ">;"
        }
    .end annotation
.end field

.field private kiesDeviceNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private kiesEventListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private profileID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->TAG:Ljava/lang/String;

    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->profileID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    .line 27
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    .locals 2

    .prologue
    .line 30
    const-class v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    .line 34
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addKiesActionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .prologue
    .line 38
    if-eqz p1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    monitor-exit v1

    .line 43
    :cond_0
    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addKiesEventListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;

    .prologue
    .line 46
    if-eqz p1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    monitor-exit v1

    .line 51
    :cond_0
    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addkiesDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;

    .prologue
    .line 54
    if-eqz p1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    monitor-exit v1

    .line 59
    :cond_0
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDeviceDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;-><init>()V

    .line 127
    .local v0, "message":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;
    invoke-static {p1, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getKiesDeviceDescription(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I

    move-result v1

    .line 129
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 130
    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;->value:Ljava/lang/String;

    .line 132
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getServiceDescription(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 136
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;-><init>()V

    .line 137
    .local v0, "message":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;
    invoke-static {p1, p2, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getKiesServiceDescription(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I

    move-result v1

    .line 140
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 141
    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;->value:Ljava/lang/String;

    .line 143
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;-><init>()V

    .line 99
    .local v0, "message":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;
    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getKiesUserAgent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I

    move-result v1

    .line 101
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 102
    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;->value:Ljava/lang/String;

    .line 104
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 211
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 212
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;

    .line 213
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;->onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    return-void
.end method

.method public onErrorResponse(IILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 179
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 180
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .line 181
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;->onErrorResponse(IILjava/lang/String;)V

    goto :goto_0

    .line 183
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    return-void
.end method

.method public onEventNotify(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 164
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;

    .line 165
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;
    invoke-interface {v1, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;->onEventNotify(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 167
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    return-void
.end method

.method public onGetKiesDeviceInfo(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 172
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .line 173
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;->onGetKiesDeviceInfo(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 175
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    return-void
.end method

.method public onNetworkException(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "exceptionType"    # Ljava/lang/String;
    .param p2, "exceptionValue"    # Ljava/lang/String;
    .param p3, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 155
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;

    .line 157
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;->onNetworkExcetption(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    return-void
.end method

.method public onOptionalCommandResponse(IILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 203
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 204
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .line 205
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;->onOptionalCommandResponse(IILjava/lang/String;)V

    goto :goto_0

    .line 207
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    return-void
.end method

.method public onSetAutoSyncConnectionInfoResponse(IILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 196
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .line 197
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;->onSetAutoSyncConnectionInfoResponse(IILjava/lang/String;)V

    goto :goto_0

    .line 199
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    return-void
.end method

.method public onSetSyncConnectionInfoResponse(IILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 187
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 188
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .line 189
    .local v1, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;->onSetSyncConnectionInfoResponse(IILjava/lang/String;)V

    goto :goto_0

    .line 191
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    return-void
.end method

.method public optionalCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 1
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;
    .param p3, "command"    # Ljava/lang/String;
    .param p4, "arg1"    # Ljava/lang/String;
    .param p5, "arg2"    # Ljava/lang/String;
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 109
    invoke-static/range {p1 .. p6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->kiesOptionalCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    return v0
.end method

.method public removeKiesActionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;

    .prologue
    .line 62
    if-eqz p1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesActionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 65
    monitor-exit v1

    .line 67
    :cond_0
    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeKiesEventListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;

    .prologue
    .line 70
    if-eqz p1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesEventListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 73
    monitor-exit v1

    .line 75
    :cond_0
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removekiesDeviceNotifyListeners(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;

    .prologue
    .line 78
    if-eqz p1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->kiesDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 81
    monitor-exit v1

    .line 83
    :cond_0
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAutoSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 1
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "uniqueID"    # Ljava/lang/String;
    .param p3, "ip"    # Ljava/lang/String;
    .param p4, "port"    # Ljava/lang/String;
    .param p5, "model"    # Ljava/lang/String;
    .param p6, "connectionType"    # Ljava/lang/String;
    .param p7, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 121
    invoke-static/range {p1 .. p7}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setKiesAutoSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    return v0
.end method

.method public setSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 1
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;
    .param p3, "port"    # Ljava/lang/String;
    .param p4, "model"    # Ljava/lang/String;
    .param p5, "connectionType"    # Ljava/lang/String;
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 115
    invoke-static/range {p1 .. p6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setKiesSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    return v0
.end method

.method public setUserAgent(Ljava/lang/String;)I
    .locals 1
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setKiesUserAgent(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->profileID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->startKiesCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v0

    return v0
.end method

.method public stop()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->profileID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-static {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stopKiesCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v0

    return v0
.end method

.method public subscribe(Ljava/lang/String;)I
    .locals 1
    .param p1, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 147
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->subscribeKiesDevice(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public unsubscribe(Ljava/lang/String;)I
    .locals 1
    .param p1, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->unsubscribeKiesDevice(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
