.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "ViewControllerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewControllerActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$1;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->response(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->requestGetZoomPort(Ljava/lang/String;)V

    return-void
.end method

.method private requestAddAVSListener(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 174
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$700()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 175
    return-void
.end method

.method private requestGetZoomPort(Ljava/lang/String;)V
    .locals 14
    .param p1, "dmrUDN"    # Ljava/lang/String;

    .prologue
    .line 111
    new-instance v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 112
    .local v7, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 113
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v9, 0x0

    .line 115
    .local v9, "result":I
    new-instance v5, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 118
    .local v5, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v10, "BUNDLE_INT_ZOOM_PORT_NUMBER"

    const/4 v11, -0x1

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    const-string v10, "X_GetZoomPort"

    invoke-virtual {v5, v10}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 121
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$700()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v10

    invoke-virtual {v10, p1, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getZoomPort(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v9

    .line 124
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    iget v11, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v10, v9, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;II)Z

    move-result v10

    if-nez v10, :cond_0

    .line 125
    const-string v10, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v10, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v10, "BUNDLE_ENUM_ERROR"

    sget-object v11, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->response(Landroid/os/Bundle;)V

    .line 171
    :goto_0
    return-void

    .line 133
    :cond_0
    iget v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 134
    .local v4, "key":I
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->requestAddAVSListener(I)V

    .line 135
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-wide/16 v10, 0x7530

    invoke-virtual {v5, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v8

    .line 141
    .local v8, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$1000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v3

    .line 145
    .local v3, "errorCode":I
    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 148
    .local v0, "avsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    if-eqz v8, :cond_1

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 149
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "requestAVControl"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mRequestID :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "X_GetZoomPort"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " has wrong response"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v10, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v2

    .line 154
    .local v2, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v10, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v10, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v10, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .end local v2    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 163
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    iget-object v11, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->port:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->getPortNumber(Ljava/lang/String;)I
    invoke-static {v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;Ljava/lang/String;)I

    move-result v6

    .line 165
    .local v6, "port":I
    const-string v10, "BUNDLE_INT_ZOOM_PORT_NUMBER"

    invoke-virtual {v1, v10, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v10, "BUNDLE_STRING_ID"

    iget-object v11, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->dmrUDN:Ljava/lang/String;

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v10, "BUNDLE_ENUM_ERROR"

    sget-object v11, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 73
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 108
    return-void
.end method
