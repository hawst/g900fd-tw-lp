.class Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "ProviderAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProviderWorker"
.end annotation


# instance fields
.field mFolderItem:Landroid/os/Bundle;

.field mUDN:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$1;

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/ArrayList;
    .param p4, "x4"    # I
    .param p5, "x5"    # I

    .prologue
    .line 333
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->searchItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 333
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->browseItems(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method private browseItems(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 17
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "startIndex"    # I
    .param p4, "requestCount"    # I

    .prologue
    .line 547
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "browseItems"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " objectID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    new-instance v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 550
    .local v7, "requestId":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$500()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v1

    const/4 v6, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->browse(Ljava/lang/String;Ljava/lang/String;IIILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 551
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/util/Map;

    move-result-object v1

    iget v2, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v8, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;

    const-string v10, "Browse"

    const-string v13, ""

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mFolderItem:Landroid/os/Bundle;

    move-object/from16 v9, p1

    move/from16 v11, p3

    move/from16 v12, p4

    move-object/from16 v16, p0

    invoke-direct/range {v8 .. v16}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;)V

    invoke-interface {v1, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    return-void
.end method

.method private convertSearchCriteriaFromSearchString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 14
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "searchCap"    # Ljava/lang/String;
    .param p3, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 435
    .local p4, "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    const-string v10, "dc:title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "upnp:artist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "upnp:album"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 438
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "convertSearchCriteriaFromSearchString"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "convertSearchCriteriaFromSearchString filed! searchCap is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const/4 v10, 0x0

    .line 502
    :goto_0
    return-object v10

    .line 443
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_4

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_4

    .line 445
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "convertSearchCriteriaFromSearchString"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "convertSearchCriteriaFromSearchString filed! searchString is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v10, "@refID exists false"

    goto :goto_0

    .line 451
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 452
    .local v6, "searchCriteriaKey":Ljava/lang/StringBuilder;
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 454
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 455
    .local v2, "itemType":Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 456
    goto :goto_1

    .line 457
    .end local v2    # "itemType":Ljava/lang/String;
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 458
    .local v7, "searchCriteriaKeyString":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1000()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 459
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1000()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    goto :goto_0

    .line 462
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 463
    .local v5, "searchCriteria":Ljava/lang/StringBuilder;
    const-string v4, ""

    .line 464
    .local v4, "refID":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    .local v9, "upnpClass":Ljava/lang/StringBuilder;
    if-eqz p3, :cond_d

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_d

    .line 466
    const/16 v10, 0x28

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 467
    const-string v10, "dc:title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 468
    :cond_7
    const-string v10, "dc:title contains &quot;"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "&quot; or "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 470
    :cond_8
    const-string v10, "upnp:artist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 471
    :cond_9
    const-string v10, "upnp:artist contains &quot;"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "&quot; or "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 473
    :cond_a
    const-string v10, "upnp:album"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 474
    :cond_b
    const-string v10, "upnp:album contains &quot;"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "&quot; or "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 476
    :cond_c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 477
    .local v3, "length":I
    add-int/lit8 v10, v3, -0x4

    invoke-virtual {v5, v10, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 478
    const/16 v10, 0x29

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 481
    .end local v3    # "length":I
    :cond_d
    const-string v10, "@refID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 482
    :cond_e
    const-string v4, "false"

    .line 483
    :cond_f
    const-string v10, "upnp:class"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_10

    const-string v10, "*"

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    :cond_10
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_11

    .line 484
    move-object/from16 v0, p4

    invoke-direct {p0, v9, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->createUPnPClassString(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 487
    :cond_11
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_12

    .line 488
    if-eqz p3, :cond_15

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_15

    .line 489
    const-string v10, " and "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 494
    :cond_12
    :goto_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_13

    .line 495
    const-string v10, " and @refID exists false"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 496
    :cond_13
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 497
    .local v8, "searchCriteriaStr":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1000()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    const/16 v11, 0x64

    if-le v10, v11, :cond_14

    .line 498
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1000()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    .line 499
    :cond_14
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1000()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "convertSearchCriteriaFromSearchString"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "searchCriteriaStr is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v8

    .line 502
    goto/16 :goto_0

    .line 491
    .end local v8    # "searchCriteriaStr":Ljava/lang/String;
    :cond_15
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto :goto_2
.end method

.method private convertUPnPClassFromItemType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 535
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    const-string v0, "object.item.videoItem"

    .line 543
    :goto_0
    return-object v0

    .line 537
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    const-string v0, "object.item.audioItem"

    goto :goto_0

    .line 539
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 540
    const-string v0, "object.item.imageItem"

    goto :goto_0

    .line 541
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 542
    const-string v0, "object.container"

    goto :goto_0

    .line 543
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createUPnPClassString(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)Ljava/lang/StringBuilder;
    .locals 5
    .param p1, "upnpClass"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .local p2, "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v4, 0x22

    const/4 v3, 0x1

    .line 507
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v3, :cond_3

    .line 508
    const-string v2, "("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 509
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 510
    .local v1, "itemType":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 511
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 514
    :cond_0
    const-string v2, "upnp:class derivedfrom \""

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 515
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->convertUPnPClassFromItemType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 517
    const-string v2, " or "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 518
    goto :goto_0

    .line 519
    .end local v1    # "itemType":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 520
    const-string v2, ")"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    move-object v2, p1

    .line 531
    :goto_1
    return-object v2

    .line 521
    :cond_3
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 522
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 523
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 525
    :cond_4
    const-string v2, "upnp:class derivedfrom \""

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 526
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 527
    .restart local v1    # "itemType":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->convertUPnPClassFromItemType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 529
    goto :goto_2
.end method

.method private responseFailForSearch(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;II)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;
    .param p2, "searchString"    # Ljava/lang/String;
    .param p3, "startIndex"    # I
    .param p4, "requestCount"    # I

    .prologue
    .line 425
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 426
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const-string v1, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 428
    const-string v1, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 429
    const-string v1, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->responseAction(Landroid/os/Bundle;)V

    .line 431
    return-void
.end method

.method private searchItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V
    .locals 23
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "searchString"    # Ljava/lang/String;
    .param p4, "startIndex"    # I
    .param p5, "requestCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p3, "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 390
    .local v11, "requestId":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$400()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v5

    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v5, v6, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v21

    .line 391
    .local v21, "dms":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v21, :cond_0

    .line 392
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->responseFailForSearch(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;II)V

    .line 421
    :goto_0
    return-void

    .line 396
    :cond_0
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->searchCapability:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 397
    .local v22, "searchCapa":Ljava/lang/String;
    if-eqz v22, :cond_1

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 398
    :cond_1
    move-object/from16 v0, v21

    iget-boolean v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSearchable:Z

    if-eqz v5, :cond_3

    .line 399
    const-string v22, "*"

    .line 407
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->convertSearchCriteriaFromSearchString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v8

    .line 410
    .local v8, "searchCriteria":Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 411
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$500()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v5

    const-string v7, "0"

    move-object/from16 v6, p1

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-virtual/range {v5 .. v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 413
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/util/Map;

    move-result-object v5

    iget v6, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v12, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;

    const-string v14, "Search"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mFolderItem:Landroid/os/Bundle;

    move-object/from16 v19, v0

    move-object/from16 v13, p1

    move/from16 v15, p4

    move/from16 v16, p5

    move-object/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v20, p0

    invoke-direct/range {v12 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;)V

    invoke-interface {v5, v6, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 401
    .end local v8    # "searchCriteria":Ljava/lang/String;
    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->responseFailForSearch(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;II)V

    goto :goto_0

    .line 416
    .restart local v8    # "searchCriteria":Ljava/lang/String;
    :cond_4
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$500()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v12

    const-string v14, "0"

    const-string v15, "*"

    move-object/from16 v13, p1

    move/from16 v16, p4

    move/from16 v17, p5

    move-object/from16 v18, v11

    invoke-virtual/range {v12 .. v18}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 417
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/util/Map;

    move-result-object v5

    iget v6, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v12, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;

    const-string v14, "Search"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mFolderItem:Landroid/os/Bundle;

    move-object/from16 v19, v0

    move-object/from16 v13, p1

    move/from16 v15, p4

    move/from16 v16, p5

    move-object/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v20, p0

    invoke-direct/range {v12 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;)V

    invoke-interface {v5, v6, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "searchItems"

    const-string v7, "failed for SearchCapility"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public responseAction(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 556
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->response(Landroid/os/Bundle;)V

    .line 557
    return-void
.end method

.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 346
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProviderWorker"

    const-string v2, "excutor is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 349
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
