.class Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$1;
.super Landroid/os/Handler;
.source "AsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 341
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 342
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "REQUEST_MESSAGE_DATA"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/iface/CVMessage;

    .line 344
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;

    move-result-object v2

    .line 345
    .local v2, "worker":Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getMsgID()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mResId:J

    .line 346
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mActionId:Ljava/lang/String;

    .line 347
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mReplyMessenger:Landroid/os/Messenger;

    .line 349
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 351
    return-void
.end method
