.class public Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
.super Landroid/app/Service;
.source "ServiceManager.java"


# static fields
.field private static final AWAIT_TERMINATION_TIME_SEC:I = 0x9

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final LENGTH_DEVICE_NAME_MAX:I = 0x37

.field private static final TEXT_DEVICE_TYPE_CAMERA:Ljava/lang/String; = "[Camera]"

.field private static final TEXT_DEVICE_TYPE_MOBILE:Ljava/lang/String; = "[Mobile]"

.field private static final TEXT_DEVICE_TYPE_SPC:Ljava/lang/String; = "[HomeSync]"

.field private static final TEXT_DEVICE_TYPE_TABLET:Ljava/lang/String; = "[Tablet]"

.field private static mContext:Landroid/content/Context;

.field private static mContextMutex:Ljava/lang/Object;

.field private static mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

.field private static mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

.field private static mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

.field private static mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

.field private static mSystemEventListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mSystemEventListenerMutex:Ljava/lang/Object;

.field private static mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

.field private static profieID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;


# instance fields
.field private isBind:Z

.field private mAsyncActionHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field mBindActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

.field private mFlagRunning:Z

.field private mHandlerTimer:Landroid/os/Handler;

.field private mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

.field private mModelConfig:Lcom/samsung/android/allshare/service/mediashare/ModelConfig;

.field private mRunnableTimer:Ljava/lang/Runnable;

.field private mSyncActionHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;

.field private serviceMutex:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContext:Landroid/content/Context;

    .line 90
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    .line 93
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    .line 102
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 105
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    .line 121
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    .line 129
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    .line 131
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    .line 133
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->profieID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    .line 137
    const-string v0, "content://com.samsung.android.nearby.mediaserver.config/config"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->CONTENT_URI:Landroid/net/Uri;

    .line 145
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 99
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    .line 124
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mFlagRunning:Z

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    .line 135
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mModelConfig:Lcom/samsung/android/allshare/service/mediashare/ModelConfig;

    .line 139
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->serviceMutex:Ljava/lang/Object;

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mBindActionList:Ljava/util/ArrayList;

    .line 553
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContentObserver:Landroid/database/ContentObserver;

    .line 562
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    .line 820
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    .line 919
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->updateFriendlyName()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->serviceMutex:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object p1
.end method

.method private static arrangeDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 657
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DLNADOC/1.50 SEC_HHP_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1.0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bindServiceManager(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 785
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 787
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 788
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "bindServiceManager"

    const-string v4, "intent.getAction() == null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    :goto_0
    return-object v1

    .line 792
    :cond_0
    const-string v2, "com.samsung.android.allshare.service.mediashare.SUBSCRIBE_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.samsung.android.allshare.framework.SUBSCRIBE_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 794
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "bindServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SubscriberID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "com.sec.android.allshare.iface.subscriber"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mISubscriberStub:Lcom/sec/android/allshare/iface/ISubscriber$Stub;

    goto :goto_0

    .line 798
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "bindServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bindServiceManager Ignore bind request..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 595
    if-nez p1, :cond_0

    .line 596
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    const-string v6, "use Build.MODEL"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 652
    :goto_0
    return-object v4

    .line 601
    :cond_0
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 602
    const-string v4, "/"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 603
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remove all \'/\': "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    :cond_1
    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 608
    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 609
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "replace line.separator : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_2
    :try_start_0
    const-string v1, "[Mobile]"

    .line 614
    .local v1, "deviceType":Ljava/lang/String;
    const/4 v0, 0x0

    .line 616
    .local v0, "deviceCategory":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    .line 617
    .local v3, "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    if-eqz v3, :cond_3

    .line 618
    const-string v4, "SEC_FLOATING_FEATURE_ALLSHARE_DEVICE_CATEGORY"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 620
    :cond_3
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "NONE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 621
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    const-string v6, "refer SEC_FLOATING_FEATURE_ALLSHARE_DEVICE_CATEGORY"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 634
    :goto_1
    if-nez p1, :cond_8

    .line 635
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "use Build.MODEL with "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 644
    :cond_4
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x37

    if-lt v4, v5, :cond_5

    .line 645
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    const-string v6, "deviceName length is over LENGTH_DEVICE_NAME_MAX"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const/4 v4, 0x0

    const/16 v5, 0x36

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .end local v0    # "deviceCategory":Ljava/lang/String;
    .end local v1    # "deviceType":Ljava/lang/String;
    .end local v3    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    :cond_5
    :goto_3
    move-object v4, p1

    .line 652
    goto/16 :goto_0

    .line 627
    .restart local v0    # "deviceCategory":Ljava/lang/String;
    .restart local v1    # "deviceType":Ljava/lang/String;
    .restart local v3    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v5, 0x258

    if-lt v4, v5, :cond_7

    .line 628
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    const-string v6, "set TABLET Device"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const-string v1, "[Tablet]"

    goto :goto_1

    .line 631
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    const-string v6, "set default MOBILE Device"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 648
    .end local v0    # "deviceCategory":Ljava/lang/String;
    .end local v1    # "deviceType":Ljava/lang/String;
    .end local v3    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    :catch_0
    move-exception v2

    .line 649
    .local v2, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 638
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "deviceCategory":Ljava/lang/String;
    .restart local v1    # "deviceType":Ljava/lang/String;
    .restart local v3    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    :cond_8
    :try_start_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 639
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p1

    goto :goto_2
.end method

.method private finiActionHandlers()V
    .locals 2

    .prologue
    .line 855
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 914
    return-void
.end method

.method private finiServiceManager()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 364
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "finiServiceManager"

    const-string v2, "Finalize AllShare Service Manager..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->finiActionHandlers()V

    .line 369
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 370
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->shutdownAndAwaitTermination()V

    .line 373
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 374
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 375
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 376
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 378
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    sput-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    .line 382
    sput-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    .line 384
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->terminate()I

    .line 386
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    if-eqz v0, :cond_2

    .line 387
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->fini()V

    .line 388
    sput-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    .line 391
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    if-eqz v0, :cond_3

    .line 392
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->fini()V

    .line 393
    sput-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    .line 397
    :cond_3
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    if-eqz v0, :cond_4

    .line 398
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->fini()V

    .line 399
    sput-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .line 402
    :cond_4
    return-void

    .line 378
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized getContext()Landroid/content/Context;
    .locals 3

    .prologue
    .line 995
    const-class v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 996
    :try_start_1
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContext:Landroid/content/Context;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0

    .line 997
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 995
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getDownlaodManager()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .locals 1

    .prologue
    .line 1019
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    return-object v0
.end method

.method public static getSubscriberManager()Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;
    .locals 1

    .prologue
    .line 1007
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    return-object v0
.end method

.method public static getSystemEventMonitor()Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
    .locals 1

    .prologue
    .line 1032
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    return-object v0
.end method

.method private initActionHandlers()V
    .locals 4

    .prologue
    .line 844
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionHandlerLoader;->loadHandlers(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    :goto_0
    return-void

    .line 845
    :catch_0
    move-exception v0

    .line 846
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "initActionHandlers"

    const-string v3, "Exception "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 847
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->stopSelf()V

    goto :goto_0
.end method

.method private initServiceManager()V
    .locals 10

    .prologue
    .line 300
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "initServiceManager"

    const-string v8, "Initialize AllShare ServiceManager..."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 304
    .local v0, "beginInitServiceTime":J
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    .line 305
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    .line 306
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 307
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    .line 309
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 310
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    .line 311
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;-><init>()V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    .line 322
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .line 325
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    .line 330
    sget-object v6, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-nez v6, :cond_1

    .line 331
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutorMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 332
    :try_start_1
    sget-object v6, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    if-nez v6, :cond_0

    .line 333
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    .line 335
    :cond_0
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 343
    :cond_1
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 344
    :try_start_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v6

    sget-object v8, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->initialize(Landroid/content/Context;)I

    .line 345
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 348
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->initActionHandlers()V

    .line 351
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/mediashare/monitor/NetworkStatusListener;-><init>()V

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->addSystemEventListener(Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;)V

    .line 352
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->addSystemEventListener(Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;)V

    .line 354
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 355
    .local v2, "endInitServiceTime":J
    sub-long v4, v2, v0

    .line 356
    .local v4, "spendInitServiceTime":J
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "initServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Initialized AllShare ServiceManager...taking msec of "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    return-void

    .line 311
    .end local v2    # "endInitServiceTime":J
    .end local v4    # "spendInitServiceTime":J
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 335
    :catchall_1
    move-exception v6

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v6

    .line 345
    :catchall_2
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v6
.end method

.method public static sendBroadcastMsg(Ljava/lang/String;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 1399
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1400
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1401
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1403
    return-void
.end method

.method private shutdownAndAwaitTermination()V
    .locals 5

    .prologue
    .line 408
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 413
    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 418
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "shutdownAndAwaitTermination"

    const-string v3, "AsyncActionHandler excutor Pool did not terminate"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_0
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    .line 431
    return-void

    .line 423
    :catch_0
    move-exception v0

    .line 425
    .local v0, "ie":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 427
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private declared-synchronized startServiceManager(Landroid/content/Intent;II)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 438
    monitor-enter p0

    :try_start_0
    iget-boolean v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mFlagRunning:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 439
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "AllShare Service is ALREADY running..."

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :goto_0
    monitor-exit p0

    return-void

    .line 442
    :cond_0
    :try_start_1
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "Start AllShare Service..."

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    sget-object v11, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContextMutex:Ljava/lang/Object;

    monitor-enter v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    sput-object v10, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContext:Landroid/content/Context;

    .line 448
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 451
    :try_start_3
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->initServiceManager()V

    .line 454
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 457
    .local v0, "beginStartServiceTime":J
    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;->start()V

    .line 460
    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->start()V

    .line 463
    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mContext:Landroid/content/Context;

    const-string v11, "power"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 464
    .local v6, "pm":Landroid/os/PowerManager;
    invoke-virtual {v6}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v5

    .line 466
    .local v5, "isScreenOn":Z
    if-nez v5, :cond_1

    .line 467
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "screen is off, turn off MCAST"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    .line 475
    :goto_1
    const-string v10, "com.samsung.android.allshare.service.mediashare.ServiceManager.START_COMPLETED"

    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->sendBroadcastMsg(Ljava/lang/String;)V

    .line 478
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mFlagRunning:Z

    .line 480
    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->startMonitor()V

    .line 483
    new-instance v10, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;

    invoke-direct {v10, p0}, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    iput-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mModelConfig:Lcom/samsung/android/allshare/service/mediashare/ModelConfig;

    .line 484
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->updateFriendlyName()V

    .line 489
    new-instance v4, Landroid/content/IntentFilter;

    const-string v10, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-direct {v4, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 490
    .local v4, "filter":Landroid/content/IntentFilter;
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v10, v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 498
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->startMRCP()I

    move-result v7

    .line 499
    .local v7, "result":I
    if-nez v7, :cond_2

    .line 500
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start MRCP successfully"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :goto_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->profieID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-virtual {v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->startMscp(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v7

    .line 506
    if-nez v7, :cond_3

    .line 507
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start MSCP successfully"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :goto_3
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->start()I

    move-result v7

    .line 522
    if-nez v7, :cond_4

    .line 523
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start kies cp successfully!"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 545
    .local v2, "endStartServiceTime":J
    sub-long v8, v2, v0

    .line 546
    .local v8, "spendStartServiceTime":J
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Started AllShare Service...taking msec of "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 438
    .end local v0    # "beginStartServiceTime":J
    .end local v2    # "endStartServiceTime":J
    .end local v4    # "filter":Landroid/content/IntentFilter;
    .end local v5    # "isScreenOn":Z
    .end local v6    # "pm":Landroid/os/PowerManager;
    .end local v7    # "result":I
    .end local v8    # "spendStartServiceTime":J
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10

    .line 448
    :catchall_1
    move-exception v10

    :try_start_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v10

    .line 470
    .restart local v0    # "beginStartServiceTime":J
    .restart local v5    # "isScreenOn":Z
    .restart local v6    # "pm":Landroid/os/PowerManager;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "screen is on, turn on MCAST"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    goto/16 :goto_1

    .line 502
    .restart local v4    # "filter":Landroid/content/IntentFilter;
    .restart local v7    # "result":I
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start MRCP failed!"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 509
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start MSCP failed!"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 525
    :cond_4
    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v11, "startServiceManager"

    const-string v12, "start kies cp failed!"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4
.end method

.method private declared-synchronized stopServiceManager()V
    .locals 4

    .prologue
    .line 696
    monitor-enter p0

    :try_start_0
    const-string v1, "com.samsung.android.allshare.service.mediashare.ServiceManager.STOP_COMPLETED"

    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->sendBroadcastMsg(Ljava/lang/String;)V

    .line 697
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "Stop AllShare Service..."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->clear()V

    .line 714
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->profieID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->stopMscp(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v0

    .line 715
    .local v0, "result":I
    if-nez v0, :cond_3

    .line 716
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop mscp successfully!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :goto_0
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stopMRCP()I

    move-result v0

    .line 722
    if-nez v0, :cond_4

    .line 723
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop mrcp successfully!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    :goto_1
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->stop()I

    move-result v0

    .line 729
    if-nez v0, :cond_5

    .line 730
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop kies cp successfully!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    :goto_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mFlagRunning:Z

    .line 747
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    if-eqz v1, :cond_0

    .line 748
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->stopMonitor()V

    .line 751
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    if-eqz v1, :cond_1

    .line 752
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->setStopFlag()V

    .line 755
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    if-eqz v1, :cond_2

    .line 756
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;->setStopFlag()V

    .line 758
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->stopForeground(Z)V

    .line 766
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mDeviceNameReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 774
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->finiServiceManager()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    monitor-exit p0

    return-void

    .line 718
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop mscp failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 696
    .end local v0    # "result":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 725
    .restart local v0    # "result":I
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop mrcp failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 732
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "stopServiceManager"

    const-string v3, "stop kies cp failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private unbindServiceManager(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 811
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "unbindServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unbind method executed....  ACTION            : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    const/4 v0, 0x1

    return v0
.end method

.method private updateFriendlyName()V
    .locals 3

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 582
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->checkDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 588
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->arrangeDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setDefaultUserAgent(Ljava/lang/String;)V

    .line 591
    return-void
.end method


# virtual methods
.method public addSystemEventListener(Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;

    .prologue
    .line 1293
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 1294
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1295
    monitor-exit v1

    .line 1296
    return-void

    .line 1295
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public allocateCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1084
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    if-eqz v0, :cond_0

    .line 1085
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessageAllocateTask:Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/io/MessageAllocateTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)Z

    .line 1091
    :goto_0
    return-void

    .line 1087
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "allocateCVMessage"

    const-string v2, "allocateCVMessage NullPointerException AllocateTask is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1068
    const-string v0, "ACTION_TYPE_ASYNC"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    .line 1074
    :goto_0
    return-object v0

    .line 1070
    :cond_0
    const-string v0, "ACTION_TYPE_SYNC"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    goto :goto_0

    .line 1074
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAsyncActionHandlerMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSyncActionHandlerMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public notifyEvent2Operator(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1316
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1317
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "notifyEvent2Operator"

    const-string v3, "mSystemEventListenerList is null, AllShare service has stopped"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    :goto_0
    return-void

    .line 1321
    :cond_0
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 1322
    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventListenerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1323
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1324
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;

    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;->eventNotifyReceived(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 1326
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;>;"
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1304
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 1306
    .local v0, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 1307
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "onBind"

    const-string v3, "[ TRACE ] onBind"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mBindActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 240
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->serviceMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 241
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z

    .line 242
    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v3}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->startServiceManager(Landroid/content/Intent;II)V

    .line 243
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->bindServiceManager(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 244
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 174
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 175
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "[ TRACE ] onCreate"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 211
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "[ TRACE ] onDestroy"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 214
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->stopServiceManager()V

    .line 222
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 249
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z

    .line 251
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "onRebind"

    const-string v3, "[ TRACE ] onRebind"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mBindActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 259
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 260
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 196
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 197
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "onStartCommand"

    const-string v2, "[ TRACE ] onStartCommand"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->startServiceManager(Landroid/content/Intent;II)V

    .line 201
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 268
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z

    .line 270
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mBindActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 273
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mBindActionList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "onUnbind"

    const-string v3, "[ TRACE ] binding intent-filter was left."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v1, 0x1

    .line 283
    :goto_0
    return v1

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v2, "onUnbind"

    const-string v3, "[ TRACE ] onUnbind"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    .line 281
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mHandlerTimer:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mRunnableTimer:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 283
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->unbindServiceManager(Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method

.method public publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1099
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    if-eqz v0, :cond_0

    .line 1100
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mMessagePublishTask:Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/io/MessagePublishTask;->putQ(Lcom/sec/android/allshare/iface/CVMessage;)I

    .line 1106
    :goto_0
    return-void

    .line 1102
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v1, "publishCVMessage"

    const-string v2, "publishCVMessage NullPointerException PublishTask is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)Z
    .locals 10
    .param p1, "handler"    # Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;

    .prologue
    const/4 v5, 0x0

    .line 1115
    if-nez p1, :cond_0

    .line 1116
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    const-string v8, "Oops~~. Null Action Handler warning"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    :goto_0
    return v5

    .line 1120
    :cond_0
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getActionType()Ljava/lang/String;

    move-result-object v0

    .line 1122
    .local v0, "action_type":Ljava/lang/String;
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getActionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1123
    .local v1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 1124
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    const-string v8, "Oops~~. NULL Action ID list..."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1128
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 1129
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;"
    if-nez v4, :cond_2

    .line 1130
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v7, "registerActionHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "map == null for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1134
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1135
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1136
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1137
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_4

    .line 1138
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "registerActionHandler"

    const-string v7, "Oops~~. Action ID failure.."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1142
    :cond_4
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1143
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "registerActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Oops~~. Couldn\'t register Action handler for duplicated Action ID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1149
    :cond_5
    invoke-virtual {v4, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1152
    .end local v2    # "id":Ljava/lang/String;
    :cond_6
    const/4 v5, 0x1

    goto/16 :goto_0
.end method

.method public requestCVAsyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 6
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v1, 0x0

    .line 1211
    if-nez p2, :cond_0

    .line 1225
    :goto_0
    return v1

    .line 1215
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 1216
    .local v0, "action_id":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1217
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v3, "requestCVAsyncToHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No such Action Handler with [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1223
    :cond_1
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->allocateCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 1225
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public requestCVSyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 7
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1237
    if-nez p2, :cond_0

    .line 1238
    const/4 v2, 0x0

    .line 1251
    :goto_0
    return-object v2

    .line 1240
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 1241
    .local v0, "action_id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1242
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v4, "requestCVSyncToHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No such Action Handler with [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(I)V

    .line 1245
    .local v2, "m":Lcom/sec/android/allshare/iface/CVMessage;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1249
    .end local v2    # "m":Lcom/sec/android/allshare/iface/CVMessage;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;

    .line 1251
    .local v1, "handler":Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
    invoke-virtual {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->responseSyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v2

    goto :goto_0
.end method

.method public requestEventSubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 4
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1263
    if-nez p2, :cond_0

    .line 1264
    const/4 v3, 0x0

    .line 1270
    :goto_0
    return v3

    .line 1266
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v1

    .line 1267
    .local v1, "evt":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1268
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v2

    .line 1270
    .local v2, "messenger":Landroid/os/Messenger;
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    invoke-virtual {v3, v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->subscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v3

    goto :goto_0
.end method

.method public requestEventUnsubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 4
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 1281
    if-nez p2, :cond_0

    .line 1290
    :goto_0
    return-void

    .line 1285
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v1

    .line 1286
    .local v1, "evt":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1287
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p2}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v2

    .line 1288
    .local v2, "messenger":Landroid/os/Messenger;
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSubscriberManager:Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;

    invoke-virtual {v3, v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->unsubscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setScreenStatus(Z)V
    .locals 8
    .param p1, "on_off"    # Z

    .prologue
    const/4 v7, 0x1

    .line 1332
    if-eqz p1, :cond_2

    .line 1333
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    move-result v1

    .line 1334
    .local v1, "res":I
    if-eqz v1, :cond_0

    .line 1336
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1341
    :goto_0
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSystemEventkMonitor:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->isNetworkConnect()Z

    move-result v2

    .line 1342
    .local v2, "res_net":Z
    if-eqz v2, :cond_1

    .line 1343
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    .line 1352
    .end local v1    # "res":I
    .end local v2    # "res_net":Z
    :cond_0
    :goto_1
    return-void

    .line 1337
    .restart local v1    # "res":I
    :catch_0
    move-exception v0

    .line 1338
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v4, "setScreenStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1345
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "res_net":Z
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v4, "setScreenStatus"

    const-string v5, "The network can not connect, so can not set screen status!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1350
    .end local v1    # "res":I
    .end local v2    # "res_net":Z
    :cond_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->setScreenStatus(I)I

    goto :goto_1
.end method

.method public unregisterActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)V
    .locals 9
    .param p1, "handler"    # Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;

    .prologue
    .line 1163
    if-nez p1, :cond_1

    .line 1164
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. Null Action Handler warning"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    :cond_0
    :goto_0
    return-void

    .line 1168
    :cond_1
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getActionType()Ljava/lang/String;

    move-result-object v0

    .line 1170
    .local v0, "action_type":Ljava/lang/String;
    invoke-interface {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getActionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1171
    .local v1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_2

    .line 1172
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. NULL Action ID list..."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1176
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getActionHandlerMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 1177
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;"
    if-nez v4, :cond_3

    .line 1178
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "map == null for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1182
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1183
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1184
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1185
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_5

    .line 1186
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    const-string v7, "Oops~~. Action ID failure.."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1190
    :cond_5
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1191
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;

    const-string v6, "unregisterActionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Oops~~. Such ActionHandler[ "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ] doesn\'t exist."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1196
    :cond_6
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
