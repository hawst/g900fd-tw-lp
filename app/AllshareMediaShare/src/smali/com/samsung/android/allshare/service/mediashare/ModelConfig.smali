.class public Lcom/samsung/android/allshare/service/mediashare/ModelConfig;
.super Ljava/lang/Object;
.source "ModelConfig.java"


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field protected static final PROJECTION_CONTACT:[Ljava/lang/String;


# instance fields
.field protected mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-string v0, "content://com.samsung.android.nearby.mediaserver.config/config"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->CONTENT_URI:Landroid/net/Uri;

    .line 33
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "display_name_alt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sort_key"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "starred"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "contact_presence"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "contact_status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "photo_thumb_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "lookup"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "phonetic_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "has_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sort_key_alt"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "link"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "is_user_profile"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->PROJECTION_CONTACT:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 54
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 55
    return-void
.end method


# virtual methods
.method public getFriendlyName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 114
    const-string v5, "friendlyName"

    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->isContainAttribute(Ljava/lang/String;)Z

    move-result v3

    .line 116
    .local v3, "isExist":Z
    const-string v4, "[Mobile]Samsung Mobile"

    .line 118
    .local v4, "name":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 119
    const-string v5, "friendlyName"

    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->queryValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "friendlyName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 121
    move-object v4, v2

    .line 164
    .end local v2    # "friendlyName":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 123
    .restart local v2    # "friendlyName":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 126
    .end local v2    # "friendlyName":Ljava/lang/String;
    :cond_1
    :try_start_0
    const-string v0, "[Mobile]"

    .line 130
    .local v0, "deviceType":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v6, 0x258

    if-lt v5, v6, :cond_2

    .line 131
    const-string v0, "[Tablet]"

    .line 134
    :cond_2
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 135
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 137
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Samsung Mobile"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 159
    .end local v0    # "deviceType":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected isContainAttribute(Ljava/lang/String;)Z
    .locals 11
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "attribute"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "value"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 95
    .local v7, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 97
    .local v9, "ret":Z
    if-eqz v7, :cond_2

    .line 98
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 100
    .local v6, "attr":Ljava/lang/String;
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const/4 v9, 0x1

    .line 105
    .end local v6    # "attr":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 107
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v9    # "ret":Z
    :cond_2
    :goto_0
    return v9

    .line 91
    :catch_0
    move-exception v8

    .line 92
    .local v8, "e":Landroid/database/SQLException;
    goto :goto_0
.end method

.method protected queryValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "attribute"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 62
    const/4 v8, 0x0

    .line 64
    .local v8, "val":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/ModelConfig;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const-string v4, "attribute"

    aput-object v4, v2, v9

    const-string v4, "value"

    aput-object v4, v2, v10

    const-string v5, "_id ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 68
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 70
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "attr":Ljava/lang/String;
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 76
    .end local v6    # "attr":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 78
    :cond_2
    return-object v8
.end method
