.class public interface abstract Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;
.super Ljava/lang/Object;
.source "KiesDeviceCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KiesActionListener"
.end annotation


# virtual methods
.method public abstract onErrorResponse(IILjava/lang/String;)V
.end method

.method public abstract onGetKiesDeviceInfo(ILjava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onOptionalCommandResponse(IILjava/lang/String;)V
.end method

.method public abstract onSetAutoSyncConnectionInfoResponse(IILjava/lang/String;)V
.end method

.method public abstract onSetSyncConnectionInfoResponse(IILjava/lang/String;)V
.end method
