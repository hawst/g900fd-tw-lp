.class public Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
.super Ljava/lang/Object;
.source "MSCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "MSCPWrapper"

.field private static instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

.field private static instanceMutex:Ljava/lang/Object;


# instance fields
.field private mscpDeviceNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpErrorResponseListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpEventNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpGetInfoListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpGetObjListListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpGetProtocolInfoListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpGetTransferProgressListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpGetTransportStatusListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpNetworkExceptionListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpObjOPerationListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mscpResponseListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instanceMutex:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetProtocolInfoListeners:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    .line 1103
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    if-nez v0, :cond_1

    .line 71
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    .line 75
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addMSCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;

    .prologue
    .line 707
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 708
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 709
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPErrorResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;

    .prologue
    .line 659
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 660
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 661
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;

    .prologue
    .line 611
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 612
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 613
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPGetInfoListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;

    .prologue
    .line 491
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 492
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 493
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;

    .prologue
    .line 515
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 516
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 517
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPGetTransferProgressListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;

    .prologue
    .line 563
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 564
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 565
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPGetTransportStatusListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;

    .prologue
    .line 587
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 588
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 589
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPNetworkExceptionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;

    .prologue
    .line 635
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 636
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPObjectOperationListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;

    .prologue
    .line 539
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 540
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 541
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMSCPResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;

    .prologue
    .line 683
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 684
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 685
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public browse(Ljava/lang/String;Ljava/lang/String;IIILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "containerID"    # Ljava/lang/String;
    .param p3, "startIndex"    # I
    .param p4, "maxCount"    # I
    .param p5, "browseFlag"    # I
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 173
    invoke-static/range {p1 .. p6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->browse(Ljava/lang/String;Ljava/lang/String;IIILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 175
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 176
    const-string v1, "MSCPWrapper"

    const-string v2, "browse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to browse DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    return v0
.end method

.method public creatObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "containerID"    # Ljava/lang/String;
    .param p3, "object"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 217
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->creatObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 219
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 220
    const-string v1, "MSCPWrapper"

    const-string v2, "creatObject"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to create object on a remote DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_0
    return v0
.end method

.method public deleteResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "resourceURI"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 314
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->deleteResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 315
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 316
    const-string v1, "MSCPWrapper"

    const-string v2, "deleteResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to delete a resource["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] on target DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_0
    return v0
.end method

.method public destroyObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "containerID"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 236
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->destroyObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 237
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 238
    const-string v1, "MSCPWrapper"

    const-string v2, "destroyObject"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to destroy an object on a remote DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    return v0
.end method

.method public exportResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "exportURI"    # Ljava/lang/String;
    .param p3, "sourceURI"    # Ljava/lang/String;
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 295
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->exportResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 297
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 298
    const-string v1, "MSCPWrapper"

    const-string v2, "exportResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to export a resource["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] to DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_0
    return v0
.end method

.method public getProtocolInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 4
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 425
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getProtocolInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 426
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 427
    const-string v1, "MSCPWrapper"

    const-string v2, "getProtocolInfo"

    const-string v3, "MSCP request to get protocol info failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_0
    return v0
.end method

.method public getSearchCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 117
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getSearchCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 118
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 119
    const-string v1, "MSCPWrapper"

    const-string v2, "getSearchCapabilities"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP get DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] search capabilites failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    return v0
.end method

.method public getSortCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 134
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getSortCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 135
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 136
    const-string v1, "MSCPWrapper"

    const-string v2, "getSortCapabilities"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP get DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] sort capabilites failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    return v0
.end method

.method public getSystemUpdateID(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 151
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getSystemUpdateID(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 152
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 153
    const-string v1, "MSCPWrapper"

    const-string v2, "getSystemUpdateID"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP get DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] update ID failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_0
    return v0
.end method

.method public getTransferProgress(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 4
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "transferID"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 406
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getTransferProgress(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 408
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 409
    const-string v1, "MSCPWrapper"

    const-string v2, "getTransferProgress"

    const-string v3, "MSCP request to get transfer progress failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    return v0
.end method

.method public importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "importURI"    # Ljava/lang/String;
    .param p3, "sourceURI"    # Ljava/lang/String;
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 255
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 257
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 258
    const-string v1, "MSCPWrapper"

    const-string v2, "importResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to import a resource["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] from DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    return v0
.end method

.method public mscpGetUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 480
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mscpGetUserAgent()Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "userAgent":Ljava/lang/String;
    return-object v0
.end method

.method public mscpSetUserAgent(Ljava/lang/String;)V
    .locals 4
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 470
    const-string v0, "MSCPWrapper"

    const-string v1, "mscpSetUserAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSCP request to set user agent ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mscpSetUserAgent(Ljava/lang/String;)V

    .line 472
    return-void
.end method

.method public onCommonResponse(II)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 878
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 879
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;

    .line 880
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;->onCommonResponse(II)V

    goto :goto_0

    .line 882
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 883
    return-void
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 894
    const-string v3, "MSCPWrapper"

    const-string v4, "onDeviceNotify"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    iget-object v4, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v4

    .line 896
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;

    .line 897
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;->onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 899
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906
    const-string v3, "MS-AVCP-MS:1-CM:1-1.1"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "UPnP Forum"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 907
    :cond_1
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 908
    .local v2, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    invoke-virtual {p0, p2, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getProtocolInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 910
    .end local v2    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    :cond_2
    return-void
.end method

.method public onErrorResponse(IILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 922
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 923
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;

    .line 924
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;->onErrorResponse(IILjava/lang/String;)V

    goto :goto_0

    .line 926
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 927
    return-void
.end method

.method public onEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 746
    .local p2, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 747
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;

    .line 748
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;->onEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 750
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 751
    return-void
.end method

.method public onGetDMSInfoResponse(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    .line 765
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 766
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;

    .line 767
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;->onGetDMSInfoResponse(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 769
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 770
    return-void
.end method

.method public onGetObjectListResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;

    .prologue
    .line 784
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 785
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;

    .line 786
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;->onGetObjectListResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;)V

    goto :goto_0

    .line 788
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 789
    return-void
.end method

.method public onGetProtocolInfoResponse(IILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "source"    # Ljava/lang/String;
    .param p4, "sink"    # Ljava/lang/String;

    .prologue
    .line 823
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetProtocolInfoListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 824
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetProtocolInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;

    .line 825
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;->onGetProtocolInfoResponse(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 827
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetProtocolInfoListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 828
    return-void
.end method

.method public onGetTransferProgressResponse(IILjava/lang/String;II)V
    .locals 8
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "status"    # Ljava/lang/String;
    .param p4, "transferLength"    # I
    .param p5, "totalLength"    # I

    .prologue
    .line 804
    iget-object v7, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    monitor-enter v7

    .line 805
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;

    .local v0, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;
    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 806
    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;->onGetTransferProgressResponse(IILjava/lang/String;II)V

    goto :goto_0

    .line 809
    .end local v0    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;
    .end local v6    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v1

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 810
    return-void
.end method

.method public onGetTransportStatusResponse(ILjava/lang/String;IJJ)V
    .locals 10
    .param p1, "transportId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "status"    # I
    .param p4, "size"    # J
    .param p6, "sentSize"    # J

    .prologue
    .line 843
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    monitor-enter v9

    .line 844
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .local v8, "i":I
    :goto_0
    if-ltz v8, :cond_0

    .line 845
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    invoke-interface/range {v0 .. v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;->onGetTransportStatusResponse(ILjava/lang/String;IJJ)V

    .line 844
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 848
    :cond_0
    monitor-exit v9

    .line 849
    return-void

    .line 848
    .end local v8    # "i":I
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "exceptionType"    # Ljava/lang/String;
    .param p2, "exceptionValue"    # Ljava/lang/String;
    .param p3, "udn"    # Ljava/lang/String;

    .prologue
    .line 732
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 733
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;

    .line 734
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;->onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 736
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 737
    return-void
.end method

.method public onObjectOperationResponse(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    .line 863
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 864
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;

    .line 865
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;->onObjectOperationResponse(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 867
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 868
    return-void
.end method

.method public postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "desUri"    # Ljava/lang/String;
    .param p2, "srcUri"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 274
    const-string v1, "MSCPWrapper"

    const-string v2, "postObject"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to post a resource["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] to ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 277
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 278
    const-string v1, "MSCPWrapper"

    const-string v2, "postObject"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to post a resource["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] to ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_0
    return v0
.end method

.method public postObjectStopTransfer(I)I
    .locals 5
    .param p1, "transferID"    # I

    .prologue
    .line 350
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->postObjectStopTransfer(I)I

    move-result v0

    .line 351
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 352
    const-string v1, "MSCPWrapper"

    const-string v2, "postObjectStopTransfer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to stop a transfer[ID ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] process failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    return v0
.end method

.method public removeMSCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;

    .prologue
    .line 719
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 720
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 721
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPErrorResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;

    .prologue
    .line 671
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 672
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpErrorResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;

    .prologue
    .line 623
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 624
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 625
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPGetInfoListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;

    .prologue
    .line 503
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 504
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;

    .prologue
    .line 527
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 528
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetObjListListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 529
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPGetTransferProgressListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;

    .prologue
    .line 575
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 576
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransferProgressListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPGetTransportStatusListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;

    .prologue
    .line 599
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 600
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpGetTransportStatusListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 601
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPNetworkExceptionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPNetworkExceptionListener;

    .prologue
    .line 647
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 648
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpNetworkExceptionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPObjectOperationListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;

    .prologue
    .line 551
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 552
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpObjOPerationListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 553
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMSCPResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;

    .prologue
    .line 695
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 696
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->mscpResponseListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "containerID"    # Ljava/lang/String;
    .param p3, "keyword"    # Ljava/lang/String;
    .param p4, "startIndex"    # I
    .param p5, "maxCount"    # I
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 195
    invoke-static/range {p1 .. p6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 197
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 198
    const-string v1, "MSCPWrapper"

    const-string v2, "search"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to search items from remote DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    return v0
.end method

.method public startMscp(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
    .locals 5
    .param p1, "profileID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    .prologue
    .line 88
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->startMSCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v0

    .line 89
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 90
    const-string v1, "MSCPWrapper"

    const-string v2, "startMscp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP start failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    return v0
.end method

.method public stopMscp(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
    .locals 5
    .param p1, "profileID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;

    .prologue
    .line 101
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stopMSCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I

    move-result v0

    .line 102
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 103
    const-string v1, "MSCPWrapper"

    const-string v2, "startMscp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP stop failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    return v0
.end method

.method public stopTransResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;
    .param p2, "transferID"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 333
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stopTransResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 335
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 336
    const-string v1, "MSCPWrapper"

    const-string v2, "stopTransResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to stop a transfer[ID ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] process on target DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_0
    return v0
.end method

.method public subscribeDMS(Ljava/lang/String;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;

    .prologue
    .line 439
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->subscribeDMS(Ljava/lang/String;)I

    move-result v0

    .line 440
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 441
    const-string v1, "MSCPWrapper"

    const-string v2, "subscribeDMS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to subscribe a target DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    return v0
.end method

.method public unSubscribeDMS(Ljava/lang/String;)I
    .locals 5
    .param p1, "dmsUDN"    # Ljava/lang/String;

    .prologue
    .line 454
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->unSubscribeDMS(Ljava/lang/String;)I

    move-result v0

    .line 455
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 456
    const-string v1, "MSCPWrapper"

    const-string v2, "unSubscribeDMS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSCP request to unsubscribe a target DMS["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_0
    return v0
.end method
