.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "SlideShowPlayerAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$1;,
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    }
.end annotation


# static fields
.field private static final MAX_COUNT_PER_PAGE:I = 0xa

.field private static final MAX_SPC_VOL:I = 0xf

.field private static final MIN_SPC_VOL:I

.field private static deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    .line 54
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 58
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->createAllShareObjectList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->dividePageFromBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method private createAllShareObjectList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 532
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 534
    .local v15, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    const-string v11, ""

    .line 536
    .local v11, "itemConstructorKey":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    .line 538
    .local v7, "bundle":Landroid/os/Bundle;
    const-string v21, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 540
    const-string v21, "LOCAL_CONTENT"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 541
    const-string v21, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    .line 542
    .local v20, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->convertUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v12

    .line 545
    .local v12, "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    if-eqz v12, :cond_0

    .line 548
    iget-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    iget-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    .line 549
    iget-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    const/16 v22, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    .line 552
    :cond_1
    const-string v9, ""

    .line 554
    .local v9, "filePath":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v21

    const-string v22, "content"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 555
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 562
    :goto_1
    iget-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-nez v21, :cond_2

    .line 563
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v21

    iput-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    .line 566
    :cond_2
    new-instance v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct {v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 568
    .local v6, "attr":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v21, "sec:localFilePath"

    move-object/from16 v0, v21

    iput-object v0, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 569
    iput-object v9, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 571
    iget-object v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 556
    .end local v6    # "attr":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    :cond_3
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v21

    const-string v22, "file"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 557
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x7

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 559
    :cond_4
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 577
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v12    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v20    # "uri":Landroid/net/Uri;
    :cond_5
    new-instance v16, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;-><init>()V

    .line 578
    .local v16, "simpleObject":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    .line 579
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    .line 582
    const-string v21, "BUNDLE_STRING_ITEM_TITLE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 583
    .local v17, "title":Ljava/lang/String;
    if-eqz v17, :cond_6

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v21

    if-gtz v21, :cond_7

    .line 584
    :cond_6
    const-string v17, "title"

    .line 585
    :cond_7
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    .line 588
    const-string v21, "BUNDLE_STRING_ITEM_DATE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 589
    .local v8, "date":Ljava/lang/String;
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v21

    if-gtz v21, :cond_9

    .line 590
    :cond_8
    const-string v8, "0000-00-00_00:00:00"

    .line 591
    :cond_9
    move-object/from16 v0, v16

    iput-object v8, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->date:Ljava/lang/String;

    .line 596
    new-instance v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    invoke-direct {v14}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;-><init>()V

    .line 599
    .local v14, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    const-string v21, "BUNDLE_LONG_ITEM_FILE_SIZE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 600
    .local v18, "size":J
    move-wide/from16 v0, v18

    iput-wide v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J

    .line 603
    const-string v21, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 604
    .local v13, "mimeType":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 606
    iput-object v13, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    .line 609
    const-string v21, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    .line 610
    .restart local v20    # "uri":Landroid/net/Uri;
    if-eqz v20, :cond_0

    .line 612
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    .line 615
    const-string v21, ""

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->bitrate:Ljava/lang/String;

    .line 616
    const-string v21, "0:00:00"

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    .line 617
    const-string v21, ""

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->extension:Ljava/lang/String;

    .line 618
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "http-get:*:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ":*"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    .line 619
    const-string v21, "  x  "

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    .line 620
    const-string v21, "http"

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->transport:Ljava/lang/String;

    .line 622
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    const-string v21, "BUNDLE_STRING_AUDIO_ITEM_ALBUM_TITLE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 629
    .local v3, "albumTitle":Ljava/lang/String;
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_a

    .line 630
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 631
    .local v2, "abt":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v21, "upnp:album"

    move-object/from16 v0, v21

    iput-object v0, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 632
    iput-object v3, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 633
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    .end local v2    # "abt":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    :cond_a
    const-string v21, "BUNDLE_STRING_AUDIO_ITEM_ARTIST"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 638
    .local v4, "artist":Ljava/lang/String;
    if-eqz v4, :cond_b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v21

    if-gtz v21, :cond_b

    .line 639
    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 640
    .local v5, "at":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v21, "upnp:artist"

    move-object/from16 v0, v21

    iput-object v0, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 641
    iput-object v4, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 642
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    .end local v5    # "at":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    :cond_b
    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 648
    .end local v3    # "albumTitle":Ljava/lang/String;
    .end local v4    # "artist":Ljava/lang/String;
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v8    # "date":Ljava/lang/String;
    .end local v13    # "mimeType":Ljava/lang/String;
    .end local v14    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v16    # "simpleObject":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v17    # "title":Ljava/lang/String;
    .end local v18    # "size":J
    .end local v20    # "uri":Landroid/net/Uri;
    :cond_c
    return-object v15
.end method

.method private dividePageFromBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 507
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Landroid/os/Bundle;>;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 508
    .local v4, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 509
    move v6, v2

    .line 510
    .local v6, "start":I
    add-int/lit8 v1, v2, 0xa

    .line 512
    .local v1, "end":I
    if-lt v1, v4, :cond_0

    move v1, v4

    .line 515
    :cond_0
    :try_start_0
    invoke-virtual {p1, v6, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v7

    .line 516
    .local v7, "temp":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 518
    .local v8, "tempArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 519
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 522
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "temp":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .end local v8    # "tempArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :catch_0
    move-exception v9

    .line 525
    :goto_2
    add-int/lit8 v2, v2, 0xa

    .line 526
    goto :goto_0

    .line 521
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "temp":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v8    # "tempArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_1
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 528
    .end local v1    # "end":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "start":I
    .end local v7    # "temp":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .end local v8    # "tempArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_2
    return-object v5
.end method

.method public static getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 652
    if-nez p0, :cond_1

    .line 666
    :cond_0
    :goto_0
    return-object v2

    .line 655
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 656
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 659
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 660
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 661
    .local v7, "filePath":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 662
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 663
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 664
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v2, v7

    .line 666
    goto :goto_0
.end method

.method private validateReturnValue(II)Z
    .locals 1
    .param p1, "returnValue"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 501
    if-nez p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 86
    return-void
.end method

.method public convertVolumeLevelForSPC(I)I
    .locals 10
    .param p1, "volume"    # I

    .prologue
    const/16 v4, 0x64

    const/16 v5, 0xf

    const/4 v1, 0x0

    .line 484
    const/4 v0, 0x0

    .line 487
    .local v0, "result":I
    if-gez p1, :cond_1

    move v0, v1

    .line 490
    :goto_0
    mul-int/lit8 v4, p1, 0xf

    int-to-double v6, v4

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double v2, v6, v8

    .line 491
    .local v2, "temp":D
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    .line 494
    if-gez v0, :cond_3

    move v0, v1

    .line 497
    :cond_0
    :goto_1
    return v0

    .line 487
    .end local v2    # "temp":D
    :cond_1
    if-le p1, v4, :cond_2

    move v0, v4

    goto :goto_0

    :cond_2
    move v0, p1

    goto :goto_0

    .line 494
    .restart local v2    # "temp":D
    :cond_3
    if-le v0, v5, :cond_0

    move v0, v5

    goto :goto_1
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_START"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 63
    const-string v0, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_STOP"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 64
    const-string v0, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETLIST"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 65
    const-string v0, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMLIST"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 66
    const-string v0, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMVOLUME"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 77
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 72
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 464
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onAVTControlResponse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " actionName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " errorCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 469
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 470
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 472
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 473
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 474
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 475
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 478
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method
