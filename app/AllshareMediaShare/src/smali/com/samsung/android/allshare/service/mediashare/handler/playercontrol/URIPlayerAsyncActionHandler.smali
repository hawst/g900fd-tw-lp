.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "URIPlayerAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;
    }
.end annotation


# static fields
.field public static final AS_URIPLAY_ACTION_TIMEOUT:I = 0x321

.field public static final AS_URIPLAY_PLAYER_NOT_FOUND:I = 0x2329

.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$1900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;IZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->isOnlyDirectlyMode(Ljava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->requestAddAVSListener(I)V

    return-void
.end method

.method private getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "uriOrPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 504
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object p1, v2

    .line 532
    .end local p1    # "uriOrPath":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 507
    .restart local p1    # "uriOrPath":Ljava/lang/String;
    :cond_2
    const-string v3, "http://"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 510
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 513
    const-string v3, "file://"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 514
    const-string v2, "file://"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 516
    :cond_3
    const-string v3, "content://"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 517
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 518
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_4

    .line 519
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "getAbsFilepath"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parse uri failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v2

    .line 520
    goto :goto_0

    .line 523
    :cond_4
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->parseUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/util/HashMap;

    move-result-object v0

    .line 525
    .local v0, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_5

    move-object p1, v2

    .line 526
    goto :goto_0

    .line 528
    :cond_5
    const-string v2, "_data"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object p1, v2

    goto :goto_0

    .line 531
    .end local v0    # "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "getAbsFilepath"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid file uri or path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v2

    .line 532
    goto :goto_0
.end method

.method private isOnlyDirectlyMode(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "udnType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 479
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v3

    invoke-virtual {v3, v2, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v0

    .line 482
    .local v0, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v0, :cond_1

    .line 489
    :cond_0
    :goto_0
    return v1

    .line 486
    :cond_1
    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    const-string v4, "Windows Media Player"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 487
    goto :goto_0
.end method

.method private isOnlyDirectlyMode(Ljava/lang/String;IZ)Z
    .locals 5
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "udnType"    # I
    .param p3, "isHttps"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 461
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v3

    invoke-virtual {v3, v2, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v0

    .line 464
    .local v0, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v0, :cond_1

    .line 474
    :cond_0
    :goto_0
    return v1

    .line 468
    :cond_1
    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    const-string v4, "Windows Media Player"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p3, :cond_0

    .line 470
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v3, "isOnlyDirectlyMode"

    const-string v4, "force Windows Media Player redirect to http"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 471
    goto :goto_0
.end method

.method private putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 493
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 494
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "putResourceUriListToBundle"

    const-string v3, "(bundle == null || uri == null)"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :goto_0
    return-void

    .line 498
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private requestAddAVSListener(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 454
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 455
    return-void
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 123
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 65
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 84
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 75
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 89
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onAVTControlResponse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", actionName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 94
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 97
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 98
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 99
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 100
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 103
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method
