.class public Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
.super Ljava/lang/Object;
.source "ConcurrentBlockingHashMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final BLOCKING_UNIT_TIME:J = 0x32L

.field private static final TAG:Ljava/lang/String; = "ConcurrentBlockingHashMap"


# instance fields
.field private concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private unitSleep(J)J
    .locals 7
    .param p1, "remainingTime"    # J

    .prologue
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    const-wide/16 v2, 0x32

    .line 47
    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 50
    .local v2, "waitingTime":J
    :goto_0
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_1
    return-wide v2

    .end local v2    # "waitingTime":J
    :cond_0
    move-wide v2, p1

    .line 47
    goto :goto_0

    .line 51
    .restart local v2    # "waitingTime":J
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "ConcurrentBlockingHashMap"

    const-string v4, "unitSleep"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 4
    .param p2, "timeout"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    const/4 v0, 0x0

    .line 22
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 24
    .local v0, "value":Ljava/lang/Object;, "TV;"
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-gtz v1, :cond_1

    .line 30
    :cond_0
    return-object v0

    .line 27
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->unitSleep(J)J

    move-result-wide v2

    sub-long/2addr p2, v2

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;, "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->concurrentMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
