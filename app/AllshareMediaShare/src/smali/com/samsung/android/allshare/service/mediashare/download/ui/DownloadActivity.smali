.class public Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
.super Landroid/app/Activity;
.source "DownloadActivity.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;
    }
.end annotation


# static fields
.field private static final BUNDLE_SAVED_DOWNLOAD_IDS:Ljava/lang/String; = "download_ids"

.field private static final BUNDLE_SAVED_FILENAMES:Ljava/lang/String; = "filenames"

.field private static final BUNDLE_SAVED_MIMETYPES:Ljava/lang/String; = "mimetypes"


# instance fields
.field private deleteMenu:Landroid/view/MenuItem;

.field private mCompleteLayout:Landroid/widget/LinearLayout;

.field private mCompleteStatus:Landroid/widget/TextView;

.field private mCompleteTotalSize:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mCurrentView:Landroid/widget/ListView;

.field private mDownloadCount:Landroid/widget/TextView;

.field private mDownloadFiles:Landroid/widget/TextView;

.field private mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

.field private mDownloadProgress:Landroid/widget/ProgressBar;

.field private mEmptyView:Landroid/view/View;

.field private mFileNameText:Landroid/widget/TextView;

.field private mInProgressDeleted:Z

.field private mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

.field private mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

.field private mListView:Landroid/widget/ListView;

.field private mProgressLayout:Landroid/widget/RelativeLayout;

.field private mProgressTotalSize:Landroid/widget/TextView;

.field private mRootView:Landroid/widget/LinearLayout;

.field private mSelectAllLayout:Landroid/widget/LinearLayout;

.field mSelectedCountFormat:Ljava/lang/String;

.field private final mSelectedIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

.field private mTabletSideMargin:I

.field private final mTag:Ljava/lang/String;

.field private mTransferInfo:Landroid/widget/TextView;

.field private mTransferRate:Landroid/widget/TextView;

.field powermanager:Landroid/os/PowerManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 73
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    .line 104
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    .line 105
    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    .line 130
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->powermanager:Landroid/os/PowerManager;

    .line 132
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->handleItemClick(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteDownload(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->showStatus()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    return-object v0
.end method

.method private activeListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    .line 565
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    return-object v0
.end method

.method private chooseListToShow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 541
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 543
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 544
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 545
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 546
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 547
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadFiles:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 556
    :goto_0
    return-void

    .line 550
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadFiles:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 551
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 552
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->activeListView()Landroid/widget/ListView;

    move-result-object v0

    .line 553
    .local v0, "lv":Landroid/widget/ListView;
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 554
    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0
.end method

.method private deleteDownload(Ljava/lang/String;)V
    .locals 8
    .param p1, "downloadId"    # Ljava/lang/String;

    .prologue
    .line 729
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 730
    .local v1, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 731
    const-string v2, "DownloadActivity"

    const-string v3, "deleteDownload"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalSize(J)V

    .line 734
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 746
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->remove(Ljava/lang/Object;)Z

    .line 747
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->notifyDataSetChanged()V

    .line 751
    .end local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :cond_1
    return-void

    .line 736
    .restart local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setCurrentIndex(I)V

    .line 737
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalCancelledCount(I)V

    .line 738
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadedSize(J)V

    goto :goto_0

    .line 741
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setCurrentIndex(I)V

    .line 742
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalFailedCount(I)V

    .line 743
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadedSize(J)V

    goto :goto_0

    .line 734
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private deleteSelectedDownload()V
    .locals 8

    .prologue
    .line 754
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-nez v2, :cond_0

    .line 784
    :goto_0
    return-void

    .line 757
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 759
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 761
    .local v1, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 762
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    const-string v2, "DownloadActivity"

    const-string v3, "deleteSelectedDownload()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 766
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    .line 768
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalSize(J)V

    .line 769
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->cancel()V

    .line 770
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->remove(Ljava/lang/Object;)Z

    .line 771
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->notifyDataSetChanged()V

    .line 775
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 779
    .end local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 780
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 781
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 782
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->itemDownload()V

    .line 783
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    goto/16 :goto_0

    .line 773
    .restart local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getConvertSize(J)Ljava/lang/String;
    .locals 5
    .param p1, "size"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 432
    const-string v0, ""

    .line 433
    .local v0, "retSize":Ljava/lang/String;
    cmp-long v1, p1, v2

    if-gez v1, :cond_0

    .line 434
    const-string v0, "Wrong size"

    .line 440
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v2, "getConvertSize"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    return-object v0

    .line 435
    :cond_0
    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 436
    const-string v0, "0"

    goto :goto_0

    .line 438
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDeleteClickHandler(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .param p1, "downloadId"    # Ljava/lang/String;

    .prologue
    .line 573
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$4;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$4;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getDeviceInch(Landroid/content/Context;)D
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 829
    const-wide/16 v0, 0x0

    .line 831
    .local v0, "device_inch":D
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 832
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 833
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 835
    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v6, v6

    iget v7, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float v4, v6, v7

    .line 836
    .local v4, "xInch":F
    iget v6, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    iget v7, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v5, v6, v7

    .line 838
    .local v5, "yInch":F
    mul-float v6, v4, v4

    mul-float v7, v5, v5

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 840
    return-wide v0
.end method

.method private getErrorMessage(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Ljava/lang/String;
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 634
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getReason()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 644
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getUnknownErrorMessage()Ljava/lang/String;

    move-result-object v0

    .line 646
    :goto_0
    return-object v0

    .line 636
    :pswitch_0
    const v0, 0x7f07001a

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 638
    :pswitch_1
    const v0, 0x7f070016

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 646
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getUnknownErrorMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 634
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getUnknownErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 659
    const v0, 0x7f070020

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleItemClick(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 609
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 610
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 627
    :goto_0
    return-void

    .line 613
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .line 615
    .local v1, "uiItem":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->toggle()V

    goto :goto_0

    .line 618
    .end local v1    # "uiItem":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->openCurrentDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    goto :goto_0

    .line 621
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getErrorMessage(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->showFailedDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 624
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->showCancelledDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 610
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private openCurrentDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 8
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 587
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getCreatedFile()Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 590
    .local v3, "localUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 598
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 599
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getMediaType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    const v4, 0x10000001

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 602
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 606
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 591
    :catch_0
    move-exception v1

    .line 592
    .local v1, "exc":Ljava/io/FileNotFoundException;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v5, "openCurrentDownload"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to open download "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 593
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f070019

    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->showFailedDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 603
    .end local v1    # "exc":Ljava/io/FileNotFoundException;
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 604
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    const v4, 0x7f07001b

    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 595
    .end local v0    # "ex":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v4

    goto :goto_0
.end method

.method private setupViews()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    .line 220
    .local v10, "titleBar":Landroid/app/ActionBar;
    if-eqz v10, :cond_0

    .line 221
    invoke-virtual {v10, v11}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 222
    const v0, 0x7f07001d

    invoke-virtual {v10, v0}, Landroid/app/ActionBar;->setTitle(I)V

    .line 224
    :cond_0
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->setContentView(I)V

    .line 225
    const/high16 v0, 0x7f0a0000

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    .line 227
    const v0, 0x7f0a0001

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 228
    const v0, 0x7f0a0002

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    .line 229
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadCount:Landroid/widget/TextView;

    .line 230
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0021

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mFileNameText:Landroid/widget/TextView;

    .line 231
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadProgress:Landroid/widget/ProgressBar;

    .line 232
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferRate:Landroid/widget/TextView;

    .line 233
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressTotalSize:Landroid/widget/TextView;

    .line 235
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0a001b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteStatus:Landroid/widget/TextView;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0a001d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferInfo:Landroid/widget/TextView;

    .line 237
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteTotalSize:Landroid/widget/TextView;

    .line 239
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadFiles:Landroid/widget/TextView;

    .line 241
    const v0, 0x7f0a0007

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    .line 242
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 243
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 267
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 275
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    .line 277
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    const v1, 0x7f0a0006

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v4

    new-instance v5, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$3;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;-><init>(Landroid/content/Context;Landroid/widget/Button;Landroid/widget/LinearLayout;ILcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    .line 330
    const v0, 0x7f0a0008

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    .line 331
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->showStatus()V

    .line 335
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getDeviceInch(Landroid/content/Context;)D

    move-result-wide v8

    .line 336
    .local v8, "inch":D
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v1, "setupViews"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device inch: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getDeviceInch(Landroid/content/Context;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    .line 339
    new-instance v6, Landroid/util/DisplayMetrics;

    invoke-direct {v6}, Landroid/util/DisplayMetrics;-><init>()V

    .line 340
    .local v6, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 342
    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    .line 343
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 344
    .local v7, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 345
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 346
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 351
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 353
    .end local v6    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v7    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    return-void

    .line 348
    .restart local v6    # "displayMetrics":Landroid/util/DisplayMetrics;
    .restart local v7    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    iput v11, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 349
    iput v11, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method

.method private showCancelledDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "downloadId"    # Ljava/lang/String;

    .prologue
    .line 670
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070020

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000b

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getDeleteClickHandler(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 675
    return-void
.end method

.method private showFailedDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "downloadId"    # Ljava/lang/String;
    .param p2, "dialogBody"    # Ljava/lang/String;

    .prologue
    .line 663
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070009

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000b

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getDeleteClickHandler(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 667
    return-void
.end method

.method private showStatus()V
    .locals 12

    .prologue
    const v11, 0x7f070018

    const v10, 0x7f070011

    const/4 v9, 0x1

    const/16 v5, 0x8

    const/4 v8, 0x0

    .line 356
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 358
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 359
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 360
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadFiles:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 361
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 429
    :cond_1
    :goto_0
    return-void

    .line 363
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadFiles:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 367
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->invalidateOptionsMenu()V

    .line 369
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    .line 370
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 371
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 374
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v1

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v4

    if-ne v1, v4, :cond_3

    .line 375
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteStatus:Landroid/widget/TextView;

    const v4, 0x7f070009

    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSuccessCount()I

    move-result v1

    if-nez v1, :cond_5

    .line 382
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferInfo:Landroid/widget/TextView;

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p0, v11, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteTotalSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getConvertSize(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->dismiss()V

    goto/16 :goto_0

    .line 376
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 377
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteStatus:Landroid/widget/TextView;

    const v4, 0x7f07000a

    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 379
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteStatus:Landroid/widget/TextView;

    const v4, 0x7f070013

    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 384
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSuccessCount()I

    move-result v1

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v4

    if-eq v1, v4, :cond_6

    .line 385
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferInfo:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f07001f

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSuccessCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v11, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 391
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferInfo:Landroid/widget/TextView;

    const v4, 0x7f07001f

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSuccessCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 398
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v1

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "iDataRate":I
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_8

    .line 402
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 403
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 405
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 406
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 408
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v2

    .line 409
    .local v2, "totalSize":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 410
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDownloadSize()J

    move-result-wide v4

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    div-long/2addr v4, v2

    long-to-int v0, v4

    .line 412
    :cond_9
    if-nez v0, :cond_a

    .line 413
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v9}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 414
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferRate:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    :goto_3
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadCount:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mProgressTotalSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getConvertSize(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mFileNameText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 416
    :cond_a
    const/16 v1, 0x64

    if-le v0, v1, :cond_b

    .line 417
    const/16 v0, 0x64

    .line 418
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v8}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 419
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 420
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTransferRate:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method


# virtual methods
.method public getCheckedDownloadID(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 694
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 695
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->isDownloadSelected(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 696
    const-string v0, "NotFound"

    .line 698
    .end local v0    # "id":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getContentSize()I
    .locals 6

    .prologue
    .line 702
    const/4 v0, 0x0

    .line 704
    .local v0, "count":I
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 705
    .local v3, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v4

    if-nez v4, :cond_1

    .line 707
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 709
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 711
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :catch_0
    move-exception v1

    .line 712
    .local v1, "ex":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v1}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    .line 714
    .end local v1    # "ex":Ljava/util/ConcurrentModificationException;
    :cond_2
    return v0
.end method

.method getCurrentView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mCurrentView:Landroid/widget/ListView;

    return-object v0
.end method

.method public getSelectedIdsCount()I
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public isCheckable(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 824
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloadSelected(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 468
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    if-lez v1, :cond_1

    .line 469
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 470
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 471
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 472
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTabletSideMargin:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 477
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mRootView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 479
    .end local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 480
    return-void

    .line 473
    .restart local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 474
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 475
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mContext:Landroid/content/Context;

    .line 139
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getDownlaodManager()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .line 140
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getDownloadItemList()Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "mList is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->finish()V

    .line 151
    :cond_0
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->setTheme(I)V

    .line 153
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->setFinishOnTouchOutside(Z)V

    .line 154
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->setupViews()V

    .line 156
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->powermanager:Landroid/os/PowerManager;

    .line 158
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v0, :cond_1

    .line 159
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mContext:Landroid/content/Context;

    const v2, 0x7f030001

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;-><init>(Landroid/content/Context;Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    .line 161
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mListAdapter:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadListListener(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList$IDownloadListListenr;)V

    .line 165
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->chooseListToShow()V

    .line 166
    const v0, 0x7f07001c

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedCountFormat:Ljava/lang/String;

    .line 168
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 174
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    .line 175
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    const v1, 0x7f07000b

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 176
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->isComplete()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 180
    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mInProgressDeleted:Z

    .line 181
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 458
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getNotiType()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-eq v0, v1, :cond_1

    .line 461
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "STOP_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 463
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 464
    return-void
.end method

.method public onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "downloadId"    # Ljava/lang/String;
    .param p2, "isSelected"    # Z
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 680
    if-eqz p2, :cond_0

    .line 681
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;

    invoke-direct {v1, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 685
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->invalidateOptionsMenu()V

    .line 686
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 687
    return-void

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 186
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 215
    :goto_0
    return v7

    .line 190
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 192
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteSelectedDownload()V

    .line 195
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v3

    .line 196
    .local v3, "lv":Landroid/widget/ListView;
    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 197
    .local v0, "checkedPositionList":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    .line 198
    .local v1, "checkedPositionListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_2

    .line 199
    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    .line 200
    .local v4, "position":I
    invoke-virtual {v0, v4, v6}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 201
    invoke-virtual {v3, v4, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 198
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 204
    .end local v4    # "position":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v5

    if-nez v5, :cond_5

    .line 205
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->chooseListToShow()V

    .line 207
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    if-eqz v5, :cond_4

    .line 208
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->deleteMenu:Landroid/view/MenuItem;

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 211
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 210
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateProgress()V

    goto :goto_2

    .line 190
    :pswitch_data_0
    .packed-switch 0x7f0a0025
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->finish()V

    .line 487
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 488
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 522
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 523
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 524
    const-string v4, "download_ids"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 525
    .local v3, "selectedIds":[Ljava/lang/String;
    const-string v4, "filenames"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "fileNames":[Ljava/lang/String;
    const-string v4, "mimetypes"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "mimeTypes":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v4, v3

    if-lez v4, :cond_0

    .line 528
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 529
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    aget-object v5, v3, v1

    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;

    aget-object v7, v0, v1

    aget-object v8, v2, v1

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 532
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 533
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->chooseListToShow()V

    .line 534
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mTag:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getNotiType()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v0, v1, :cond_0

    .line 449
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mDownloadManager:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->setNotiType(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;)V

    .line 451
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->chooseListToShow()V

    .line 452
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 453
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 454
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 498
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v4

    .line 499
    .local v4, "len":I
    if-nez v4, :cond_0

    .line 518
    :goto_0
    return-void

    .line 502
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 504
    new-array v7, v4, [Ljava/lang/String;

    .line 505
    .local v7, "selectedIds":[Ljava/lang/String;
    new-array v0, v4, [Ljava/lang/String;

    .line 506
    .local v0, "fileNames":[Ljava/lang/String;
    new-array v5, v4, [Ljava/lang/String;

    .line 507
    .local v5, "mimeTypes":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 508
    .local v1, "i":I
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 509
    .local v3, "id":Ljava/lang/String;
    aput-object v3, v7, v1

    .line 510
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;

    .line 511
    .local v6, "obj":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->getFileName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v1

    .line 512
    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->getMimeType()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v1

    .line 513
    add-int/lit8 v1, v1, 0x1

    .line 514
    goto :goto_1

    .line 515
    .end local v3    # "id":Ljava/lang/String;
    .end local v6    # "obj":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;
    :cond_1
    const-string v8, "download_ids"

    invoke-virtual {p1, v8, v7}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 516
    const-string v8, "filenames"

    invoke-virtual {p1, v8, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 517
    const-string v8, "mimetypes"

    invoke-virtual {p1, v8, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateList()V
    .locals 1

    .prologue
    .line 804
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$6;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 810
    return-void
.end method

.method public updateProgress()V
    .locals 1

    .prologue
    .line 794
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$5;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 801
    return-void
.end method

.method public updateSelectAll(Ljava/lang/String;)V
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 813
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$7;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$7;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 821
    return-void
.end method

.method public updateSelectAllMenu()V
    .locals 5

    .prologue
    .line 718
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    const v1, 0x7f070028

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->setTitle(Ljava/lang/CharSequence;)V

    .line 719
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getContentSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->setContextSize(I)V

    .line 720
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectionMenu:Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->mSelectedIds:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/SelectionMenu;->updateItem(I)V

    .line 721
    return-void
.end method
