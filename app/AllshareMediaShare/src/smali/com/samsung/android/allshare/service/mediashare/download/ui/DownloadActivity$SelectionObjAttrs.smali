.class Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;
.super Ljava/lang/Object;
.source "DownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectionObjAttrs"
.end annotation


# instance fields
.field private mFileName:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->mFileName:Ljava/lang/String;

    .line 114
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->mMimeType:Ljava/lang/String;

    .line 115
    return-void
.end method


# virtual methods
.method getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity$SelectionObjAttrs;->mMimeType:Ljava/lang/String;

    return-object v0
.end method
