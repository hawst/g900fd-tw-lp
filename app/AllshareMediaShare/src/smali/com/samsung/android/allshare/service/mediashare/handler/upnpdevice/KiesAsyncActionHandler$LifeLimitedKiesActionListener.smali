.class Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;
.super Ljava/util/TimerTask;
.source "KiesAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LifeLimitedKiesActionListener"
.end annotation


# static fields
.field private static final MAX_WAIT_ID_TIME:I = 0x1388

.field private static final NOT_AVAILABLE_ID:I = -0x1


# instance fields
.field private hasReceived:Z

.field private hasWaited:Z

.field private kiesWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

.field private requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

.field private resBundle:Landroid/os/Bundle;

.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

.field private timer:Ljava/util/Timer;

.field private worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;J)V
    .locals 2
    .param p2, "wrapper"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    .param p3, "worker"    # Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;
    .param p4, "lifeTime"    # J

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 248
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 233
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->kiesWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    .line 235
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    .line 237
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .line 239
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    .line 241
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->timer:Ljava/util/Timer;

    .line 243
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasWaited:Z

    .line 245
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasReceived:Z

    .line 249
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->kiesWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    .line 250
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    .line 251
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .line 252
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 254
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->timer:Ljava/util/Timer;

    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->timer:Ljava/util/Timer;

    invoke-virtual {v0, p0, p4, p5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 256
    return-void
.end method

.method private getMonitorID()I
    .locals 6

    .prologue
    .line 337
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    monitor-enter v2

    .line 338
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    iget v1, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasWaited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 340
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 341
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasWaited:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    iget v1, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    monitor-exit v2

    return v1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "getMonitorID"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 347
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public onErrorResponse(IILjava/lang/String;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->getMonitorID()I

    move-result v1

    .line 281
    .local v1, "monitorID":I
    if-eq v1, p1, :cond_0

    .line 289
    :goto_0
    return-void

    .line 284
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasReceived:Z

    .line 286
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 287
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseMessage(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onGetKiesDeviceInfo(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    .line 294
    return-void
.end method

.method public onOptionalCommandResponse(IILjava/lang/String;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->getMonitorID()I

    move-result v1

    .line 299
    .local v1, "monitorID":I
    if-eq v1, p1, :cond_0

    .line 308
    :goto_0
    return-void

    .line 302
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasReceived:Z

    .line 304
    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 305
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_KIES_DEVICE_RESULT"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseMessage(Landroid/os/Bundle;)V

    goto :goto_0

    .line 304
    .end local v0    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_1
.end method

.method public onSetAutoSyncConnectionInfoResponse(IILjava/lang/String;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->getMonitorID()I

    move-result v1

    .line 327
    .local v1, "monitorID":I
    if-eq v1, p1, :cond_0

    .line 334
    :goto_0
    return-void

    .line 330
    :cond_0
    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 331
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_KIES_DEVICE_RESULT"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseMessage(Landroid/os/Bundle;)V

    goto :goto_0

    .line 330
    .end local v0    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_1
.end method

.method public onSetSyncConnectionInfoResponse(IILjava/lang/String;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->getMonitorID()I

    move-result v1

    .line 313
    .local v1, "monitorID":I
    if-eq v1, p1, :cond_0

    .line 322
    :goto_0
    return-void

    .line 316
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasReceived:Z

    .line 318
    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 319
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_KIES_DEVICE_RESULT"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseMessage(Landroid/os/Bundle;)V

    goto :goto_0

    .line 318
    .end local v0    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_1
.end method

.method public run()V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->kiesWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->removeKiesActionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;)V

    .line 272
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->hasReceived:Z

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_ENUM_ERROR"

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseMessage(Landroid/os/Bundle;)V

    .line 276
    :cond_0
    return-void
.end method

.method public setMonitorRequestID(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 259
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    iput p1, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 262
    monitor-exit v1

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setResponseBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->resBundle:Landroid/os/Bundle;

    .line 267
    return-void
.end method
