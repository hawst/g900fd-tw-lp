.class Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;
.super Ljava/lang/Object;
.source "ProviderAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v6

    .line 353
    .local v6, "bundle":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mUDN:Ljava/lang/String;

    .line 354
    const-string v0, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 355
    .local v4, "startIndex":I
    const-string v0, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 357
    .local v5, "requestCount":I
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    const-string v0, "BUNDLE_PARCELABLE_FOLDERITEM"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mFolderItem:Landroid/os/Bundle;

    .line 360
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    const-string v0, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "searchCriteria":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 365
    .local v3, "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mUDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->searchItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V

    .line 381
    .end local v2    # "searchCriteria":Ljava/lang/String;
    .end local v3    # "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_ITEMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 369
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mUDN:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v7, v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mFolderItem:Landroid/os/Bundle;

    const-string v8, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->browseItems(Ljava/lang/String;Ljava/lang/String;II)V
    invoke-static {v0, v1, v7, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    .line 372
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCHCRITERIA_ITEMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 376
    .restart local v2    # "searchCriteria":Ljava/lang/String;
    const-string v0, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 378
    .restart local v3    # "itemTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    iget-object v1, v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->mUDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->searchItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V

    goto :goto_0
.end method
