.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;
.super Ljava/lang/Object;
.source "SlideShowPlayerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 97
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    if-nez v9, :cond_1

    .line 98
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v9, v9, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "run"

    const-string v11, "cvm is null"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v9}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 103
    .local v3, "bundle":Landroid/os/Bundle;
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v9}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "actionID":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-nez v3, :cond_3

    .line 105
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v9, v9, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "run"

    const-string v11, "bundle/action id is null"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_3
    const-class v9, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 110
    const-string v9, "BUNDLE_STRING_ID"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 112
    .local v8, "udn":Ljava/lang/String;
    const-string v9, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 114
    .local v7, "slideUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v9, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 116
    .local v2, "bgmUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v9, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "albumTitle":Ljava/lang/String;
    const-string v9, "BUNDLE_INT_SLIDESHOW_INTERVAL"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 119
    .local v5, "interval":I
    const-string v9, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 121
    .local v6, "level":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v4

    .line 122
    .local v4, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-eqz v4, :cond_4

    iget-object v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    if-nez v9, :cond_5

    .line 123
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v9, v9, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "run"

    const-string v11, "deviceItem == null"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->responseFail(Ljava/lang/String;)V
    invoke-static {v9, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_5
    const-string v9, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_START"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 129
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v10, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestShow(Ljava/lang/String;I)V
    invoke-static {v9, v10, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 130
    :cond_6
    const-string v9, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_STOP"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 131
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v10, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestStop(Ljava/lang/String;)V
    invoke-static {v9, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    :cond_7
    const-string v9, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETLIST"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 133
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v10, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    invoke-static {v9, v10, v1, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 134
    :cond_8
    const-string v9, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMLIST"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 135
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v10, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetBGMList(Ljava/lang/String;Ljava/util/ArrayList;)V
    invoke-static {v9, v10, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 136
    :cond_9
    const-string v9, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMVOLUME"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 137
    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;

    iget-object v10, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetBGMVolume(Ljava/lang/String;I)V
    invoke-static {v9, v10, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
