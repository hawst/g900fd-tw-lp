.class public Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
.super Ljava/lang/Object;
.source "ActionResponseMonitor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ActionResponseMonitor"


# instance fields
.field private actionName:Ljava/lang/String;

.field private deviceUdn:Ljava/lang/String;

.field private errorCode:I

.field private isUnknowError:Z

.field private notified:Z

.field private received:Z

.field private requestID:I

.field private response:Ljava/lang/Object;

.field private signal:Ljava/lang/Object;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->signal:Ljava/lang/Object;

    .line 12
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->response:Ljava/lang/Object;

    .line 14
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    .line 16
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->deviceUdn:Ljava/lang/String;

    .line 18
    iput v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->requestID:I

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->errorCode:I

    .line 22
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->isUnknowError:Z

    .line 24
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->received:Z

    .line 26
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notified:Z

    .line 28
    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->timer:Ljava/util/Timer;

    return-void
.end method


# virtual methods
.method public cancelTimer()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 114
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 116
    :cond_0
    return-void
.end method

.method public getActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->errorCode:I

    return v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->requestID:I

    return v0
.end method

.method public getResponse()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public hasWrongResponse()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 77
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->errorCode:I

    if-eqz v1, :cond_0

    .line 78
    const-string v1, "ActionResponseMonitor"

    const-string v2, "hasWrongResponse"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->response:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 82
    const-string v1, "ActionResponseMonitor"

    const-string v2, "hasWrongResponse"

    const-string v3, "response is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->isUnknowError:Z

    if-eqz v1, :cond_2

    .line 86
    const-string v1, "ActionResponseMonitor"

    const-string v2, "hasWrongResponse"

    const-string v3, "isUnknowError"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotified()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notified:Z

    return v0
.end method

.method public isUnknowError()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->isUnknowError:Z

    return v0
.end method

.method public notifyResponse()V
    .locals 2

    .prologue
    .line 101
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->signal:Ljava/lang/Object;

    monitor-enter v1

    .line 102
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->received:Z

    .line 103
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->signal:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 104
    monitor-exit v1

    .line 105
    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setActionName(Ljava/lang/String;)V
    .locals 0
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setDeviceUdn(Ljava/lang/String;)V
    .locals 0
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->deviceUdn:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setErrorCode(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->errorCode:I

    .line 70
    return-void
.end method

.method public setNotified(Z)V
    .locals 0
    .param p1, "notified"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notified:Z

    .line 94
    return-void
.end method

.method public setRequestID(I)V
    .locals 0
    .param p1, "requestID"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->requestID:I

    .line 62
    return-void
.end method

.method public setResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/Object;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->response:Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public setTimer(Ljava/util/Timer;)V
    .locals 0
    .param p1, "timer"    # Ljava/util/Timer;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->timer:Ljava/util/Timer;

    .line 109
    return-void
.end method

.method public setUnknowError(Z)V
    .locals 0
    .param p1, "isUnknowError"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->isUnknowError:Z

    .line 38
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActionResponseMonitor action["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] requestID["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->requestID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] errorCode["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateActionName(Ljava/lang/String;)Z
    .locals 2
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 151
    :cond_0
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    const-string v1, "Play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SetAVTransportURI"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    const/4 v0, 0x1

    goto :goto_0

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->actionName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public validateDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->deviceUdn:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 160
    :cond_0
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public waitNotified(J)Z
    .locals 11
    .param p1, "time"    # J

    .prologue
    const-wide/16 v8, 0x32

    .line 134
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notified:Z

    if-eqz v1, :cond_0

    .line 135
    const/4 v1, 0x1

    .line 146
    :goto_0
    return v1

    .line 137
    :cond_0
    const-wide/16 v6, 0x31

    add-long/2addr v6, p1

    div-long v4, v6, v8

    .line 138
    .local v4, "n":J
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_1
    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 140
    const-wide/16 v6, 0x32

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_2
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_1

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ActionResponseMonitor"

    const-string v6, "waitNotified"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exception : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 146
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notified:Z

    goto :goto_0
.end method

.method public waitResponse(J)Z
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 119
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->received:Z

    if-eqz v1, :cond_0

    .line 120
    const/4 v1, 0x1

    .line 130
    :goto_0
    return v1

    .line 122
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->signal:Ljava/lang/Object;

    monitor-enter v2

    .line 124
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->signal:Ljava/lang/Object;

    invoke-virtual {v1, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->received:Z

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "ActionResponseMonitor"

    const-string v3, "waitResponse"

    const-string v4, "InterruptedException : "

    invoke-static {v1, v3, v4, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 128
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
