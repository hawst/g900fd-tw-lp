.class public Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;
.super Ljava/lang/Object;
.source "BundleCreator.java"


# static fields
.field private static final ALBUM_NAME:Ljava/lang/String; = "upnp:album"

.field private static final ARTIST_NAME:Ljava/lang/String; = "upnp:artist"

.field private static final CHANNELNR_NAME:Ljava/lang/String; = "upnp:channelNr"

.field private static final GENRE_NAME:Ljava/lang/String; = "upnp:genre"

.field private static final TAG:Ljava/lang/String; = "BundleCreator"

.field private static sDeviceList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->sDeviceList:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 476
    return-void
.end method

.method public static createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;
    .locals 18
    .param p0, "item"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 66
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v3, "b":Landroid/os/Bundle;
    const-string v14, "BUNDLE_STRING_ITEM_TITLE"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v14, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v15, "MEDIA_SERVER"

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    if-nez v14, :cond_9

    .line 73
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :goto_0
    const/4 v8, 0x0

    .line 85
    .local v8, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-eqz v14, :cond_4

    .line 86
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 87
    .local v7, "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v14, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    const-string v15, "DLNA.ORG_CI=1"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 88
    .local v4, "bOptionalCI":Z
    if-nez v4, :cond_0

    .line 89
    move-object v8, v7

    .line 93
    .end local v4    # "bOptionalCI":Z
    .end local v7    # "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_1
    if-nez v8, :cond_2

    .line 94
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    check-cast v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 96
    .restart local v8    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_2
    const-string v14, "BUNDLE_PARCELABLE_ITEM_URI"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 97
    const-string v14, "BUNDLE_STRING_ITEM_DATE"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->date:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v14, "BUNDLE_STRING_ITEM_MIMETYPE"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->extension:Ljava/lang/String;

    if-eqz v14, :cond_3

    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->extension:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 100
    :cond_3
    const-string v14, "BUNDLE_STRING_ITEM_EXTENSION"

    const-string v15, ".bin"

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_1
    iget-wide v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 104
    .local v9, "size":Ljava/lang/Long;
    const-string v14, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 106
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v9    # "size":Ljava/lang/Long;
    :cond_4
    const-string v14, "BUNDLE_PARCELABLE_ITEM_RESOURCE_LIST"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createResourceBundleList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 108
    const-string v14, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v14, "BUNDLE_INT_ITEM_CHANNELNR"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v16, "upnp:channelNr"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_10

    .line 114
    const/4 v2, 0x0

    .line 115
    .local v2, "audioSnap":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    if-eqz v14, :cond_5

    .line 116
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v2, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 117
    :cond_5
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_e

    .line 118
    const-string v14, "BUNDLE_PARCELABLE_AUDIO_ITEM_ALBUMART"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v15, v15, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 122
    :goto_2
    const-string v14, "BUNDLE_STRING_AUDIO_ITEM_ARTIST"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v16, "upnp:artist"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v14, "BUNDLE_STRING_AUDIO_ITEM_GENRE"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v16, "upnp:genre"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v14, "BUNDLE_STRING_AUDIO_ITEM_ALBUM_TITLE"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v16, "upnp:album"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    if-eqz v8, :cond_7

    .line 132
    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    if-eqz v14, :cond_6

    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_6

    .line 134
    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    invoke-static {v14}, Lcom/samsung/android/allshare/service/mediashare/utility/TimeUtil;->getTimeLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 135
    .local v12, "time":J
    const-wide/16 v14, 0x0

    cmp-long v14, v14, v12

    if-nez v14, :cond_f

    .line 136
    const-string v14, "BUNDLE_LONG_AUDIO_ITEM_DURATION"

    const-wide/16 v16, -0x1

    move-wide/from16 v0, v16

    invoke-virtual {v3, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 141
    .end local v12    # "time":J
    :cond_6
    :goto_3
    const-string v14, "BUNDLE_STRING_AUDIO_ITEM_SEEKMODE"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getSeekModeFromProtocalInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getModeByBit(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$SeekMode;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v14, "BUNDLE_STRING_AUDIO_ITEM_BITRATE"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->bitrate:Ljava/lang/String;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->parseInt(Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    :cond_7
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .end local v2    # "audioSnap":Ljava/lang/String;
    :cond_8
    :goto_4
    return-object v3

    .line 74
    .end local v8    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_9
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_a

    .line 75
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 76
    :cond_a
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_b

    .line 77
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 78
    :cond_b
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/4 v15, 0x3

    if-ne v14, v15, :cond_c

    .line 79
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    :cond_c
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v8    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_d
    const-string v14, "BUNDLE_STRING_ITEM_EXTENSION"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->extension:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 121
    .end local v5    # "i$":Ljava/util/Iterator;
    .restart local v2    # "audioSnap":Ljava/lang/String;
    :cond_e
    const-string v14, "BUNDLE_PARCELABLE_AUDIO_ITEM_ALBUMART"

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_2

    .line 138
    .restart local v12    # "time":J
    :cond_f
    const-string v14, "BUNDLE_LONG_AUDIO_ITEM_DURATION"

    invoke-virtual {v3, v14, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_3

    .line 146
    .end local v2    # "audioSnap":Ljava/lang/String;
    .end local v12    # "time":J
    :cond_10
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_17

    .line 148
    const/4 v11, 0x0

    .line 149
    .local v11, "videoSnap":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    if-eqz v14, :cond_11

    .line 150
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v11, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 151
    :cond_11
    if-eqz v11, :cond_14

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_14

    .line 152
    const-string v14, "BUNDLE_PARCELABLE_VIDEO_ITEM_THUMBNAIL"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v15, v15, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 156
    :goto_5
    if-eqz v8, :cond_13

    .line 157
    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    if-eqz v14, :cond_12

    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_12

    .line 158
    iget-object v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    invoke-static {v14}, Lcom/samsung/android/allshare/service/mediashare/utility/TimeUtil;->getTimeLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 159
    .restart local v12    # "time":J
    const-wide/16 v14, 0x0

    cmp-long v14, v14, v12

    if-nez v14, :cond_15

    .line 160
    const-string v14, "BUNDLE_LONG_VIDEO_ITEM_DURATION"

    const-wide/16 v16, -0x1

    move-wide/from16 v0, v16

    invoke-virtual {v3, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 165
    .end local v12    # "time":J
    :cond_12
    :goto_6
    const-string v14, "BUNDLE_STRING_VIDEO_ITEM_RESOLUTION"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v14, "BUNDLE_STRING_VIDEO_ITEM_SEEKMODE"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getSeekModeFromProtocalInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getModeByBit(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$SeekMode;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v14, "BUNDLE_STRING_VIDEO_ITEM_BITRATE"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->bitrate:Ljava/lang/String;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->parseInt(Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    :cond_13
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    .line 173
    .local v10, "subTitle":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_16

    .line 174
    const-string v14, "BUNDLE_PARCELABLE_VIDEO_ITEM_SUBTITLE"

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 178
    :goto_7
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 155
    .end local v10    # "subTitle":Ljava/lang/String;
    :cond_14
    const-string v14, "BUNDLE_PARCELABLE_VIDEO_ITEM_THUMBNAIL"

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_5

    .line 162
    .restart local v12    # "time":J
    :cond_15
    const-string v14, "BUNDLE_LONG_VIDEO_ITEM_DURATION"

    invoke-virtual {v3, v14, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_6

    .line 176
    .end local v12    # "time":J
    .restart local v10    # "subTitle":Ljava/lang/String;
    :cond_16
    const-string v14, "BUNDLE_PARCELABLE_VIDEO_ITEM_SUBTITLE"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_7

    .line 179
    .end local v10    # "subTitle":Ljava/lang/String;
    .end local v11    # "videoSnap":Ljava/lang/String;
    :cond_17
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    if-nez v14, :cond_8

    .line 181
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-static {v15}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->valueOf(Lcom/samsung/android/allshare/Item$MediaType;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v6, 0x0

    .line 183
    .local v6, "imageSnap":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    if-eqz v14, :cond_18

    .line 184
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v6, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 185
    :cond_18
    if-eqz v6, :cond_1a

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_1a

    .line 186
    const-string v14, "BUNDLE_PARCELABLE_IMAGE_ITEM_THUMBNAIL"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v15, v15, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 191
    :goto_8
    if-eqz v8, :cond_19

    .line 192
    const-string v14, "BUNDLE_STRING_IMAGE_ITEM_RESOLUTION"

    iget-object v15, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_19
    const-string v14, "BUNDLE_PARCELABLE_IMAGE_ITEM_LOCATION"

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 195
    const-string v14, "BUNDLE_STRING_ITEM_TYPE"

    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 189
    :cond_1a
    const-string v14, "BUNDLE_PARCELABLE_IMAGE_ITEM_THUMBNAIL"

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_8
.end method

.method public static createDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;
    .locals 14
    .param p0, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;

    .prologue
    const/16 v13, 0x78

    .line 219
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 220
    :cond_0
    const/4 v0, 0x0

    .line 360
    :cond_1
    :goto_0
    return-object v0

    .line 224
    :cond_2
    sget-object v9, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->sDeviceList:Ljava/util/HashMap;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 225
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 228
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 231
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 232
    .local v5, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v1, 0x0

    .line 233
    .local v1, "deviceIconUrl":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 234
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createIconBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 235
    if-eqz v5, :cond_8

    .line 236
    const-string v9, "BundleCreator"

    const-string v10, "createDeviceBundle"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "iconList.size = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_3

    .line 240
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

    .line 241
    .local v4, "icon":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;
    if-eqz v4, :cond_7

    iget v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->height:I

    if-ne v9, v13, :cond_7

    iget v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->width:I

    if-ne v9, v13, :cond_7

    iget v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->depth:I

    const/16 v10, 0x18

    if-ne v9, v10, :cond_7

    iget-object v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->mimeType:Ljava/lang/String;

    const-string v10, "image/png"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 243
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

    iget-object v1, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    .line 257
    .end local v3    # "i":I
    .end local v4    # "icon":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;
    :cond_3
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-static {v9}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->isLocalAddress(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 258
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isLocalDevice:Z

    .line 261
    :cond_4
    iget-boolean v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isLocalDevice:Z

    if-eqz v9, :cond_9

    .line 262
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, "deviceLocation":Ljava/lang/String;
    :goto_3
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->name:Ljava/lang/String;

    if-eqz v9, :cond_a

    .line 268
    const-string v9, "BUNDLE_STRING_DEVICE_NAME"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->name:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :goto_4
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->ID:Ljava/lang/String;

    if-eqz v9, :cond_b

    .line 273
    const-string v9, "BUNDLE_STRING_DEVICE_MODELNAME"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->ID:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :goto_5
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    if-eqz v9, :cond_c

    .line 278
    const-string v9, "BUNDLE_STRING_BOUND_INTERFACE"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_6
    const-string v9, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    if-eqz v9, :cond_d

    .line 285
    const-string v9, "BUNDLE_STRING_ID"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    :goto_7
    const-string v9, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    if-eqz v9, :cond_e

    .line 291
    const-string v9, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :goto_8
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v9, :cond_f

    .line 296
    const-string v9, "BUNDLE_STRING_DEVICE_MODELNAME"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :goto_9
    const-string v9, "BUNDLE_BOOLEAN_SMSC_IS_WHOLE_HOME_AUDIO"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isWholeHomeAudio:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 304
    const-string v9, "BUNDLE_BOOLEAN_SMSC_iS_SEEKABLE_ON_PAUSE"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSeekOnPaused:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 309
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->productCap:Ljava/lang/String;

    if-eqz v9, :cond_10

    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->productCap:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "ScreenMirroringP2PMAC="

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 312
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->productCap:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 313
    .local v7, "secProductCap":Ljava/lang/String;
    const-string v9, "ScreenMirroringP2PMAC="

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const-string v10, "ScreenMirroringP2PMAC="

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int v8, v9, v10

    .line 315
    .local v8, "start":I
    add-int/lit8 v9, v8, 0x11

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 316
    .local v6, "macAddress":Ljava/lang/String;
    const-string v9, "BUNDLE_STRING_MIRRORING_MAC"

    invoke-virtual {v0, v9, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    .end local v6    # "macAddress":Ljava/lang/String;
    .end local v7    # "secProductCap":Ljava/lang/String;
    .end local v8    # "start":I
    :goto_a
    if-eqz v5, :cond_11

    .line 322
    const-string v9, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICONLIST"

    invoke-virtual {v0, v9, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 328
    :goto_b
    if-eqz v1, :cond_5

    .line 329
    const-string v9, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICON"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 334
    :cond_5
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 335
    const-string v9, "BUNDLE_BOOLEAN_SEARCHABLE"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSearchable:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 336
    const-string v9, "BUNDLE_BOOLEAN_RECEIVERABLE"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isDownloadable:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 359
    :cond_6
    :goto_c
    sget-object v9, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->sDeviceList:Ljava/util/HashMap;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 246
    .end local v2    # "deviceLocation":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "icon":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->iconList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

    iget-object v1, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    .line 239
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 250
    .end local v3    # "i":I
    .end local v4    # "icon":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;
    :cond_8
    const-string v9, "BundleCreator"

    const-string v10, "createDeviceBundle"

    const-string v11, "iconList == null"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 264
    :cond_9
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "deviceLocation":Ljava/lang/String;
    goto/16 :goto_3

    .line 270
    :cond_a
    const-string v9, "BUNDLE_STRING_DEVICE_NAME"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 275
    :cond_b
    const-string v9, "BUNDLE_STRING_DEVICE_MODELNAME"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 281
    :cond_c
    const-string v9, "BUNDLE_STRING_BOUND_INTERFACE"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 287
    :cond_d
    const-string v9, "BUNDLE_STRING_ID"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 293
    :cond_e
    const-string v9, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 298
    :cond_f
    const-string v9, "BUNDLE_STRING_DEVICE_MODELNAME"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 318
    :cond_10
    const-string v9, "BUNDLE_STRING_MIRRORING_MAC"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 324
    :cond_11
    const-string v9, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICONLIST"

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_b

    .line 337
    :cond_12
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 338
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_PLAYLIST_PLAYER"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportImagePlayList:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    const-string v9, "BUNDLE_BOOLEAN_SEEKABLE"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSeekable:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 341
    const-string v9, "BUNDLE_STRING_DEVICE_ID"

    iget-object v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->deviceID:Ljava/lang/String;

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v9, "BUNDLE_BOOLEAN_NAVIGATE_IN_PAUSE"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isNavigateInPause:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 344
    const-string v9, "BUNDLE_BOOLEAN_AUTO_SLIDE_SHOW"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSlideshowSupport:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_c

    .line 346
    :cond_13
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 347
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_AUDIO_PLAYLIST_PLAYER"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportAudioPlayList:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 349
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_VIDEO_PLAYLIST_PLAYER"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportVideoPlayList:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 351
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_AUDIO"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportAudio:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_VIDEO"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportVideo:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_c

    .line 355
    :cond_14
    sget-object v9, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/Device$DeviceType;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 356
    const-string v9, "BUNDLE_BOOLEAN_SUPPORT_TVCONTROLLER"

    iget-boolean v10, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isTVControlable:Z

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_c
.end method

.method private static createIconBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;)Landroid/os/Bundle;
    .locals 3
    .param p0, "iconItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

    .prologue
    .line 380
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 382
    .local v0, "iconBundle":Landroid/os/Bundle;
    const-string v1, "ICON_URI"

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->URL:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 383
    const-string v1, "ICON_DEPTH"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->depth:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 384
    const-string v1, "ICON_WIDTH"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->width:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 385
    const-string v1, "ICON_HEIGHT"

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->height:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 386
    const-string v1, "ICON_MIMETYPE"

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    return-object v0
.end method

.method private static createIconBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    .local p0, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v2, "iconbundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 372
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createIconBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;)Landroid/os/Bundle;

    move-result-object v1

    .line 373
    .local v1, "iconBundle":Landroid/os/Bundle;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    .end local v1    # "iconBundle":Landroid/os/Bundle;
    :cond_0
    return-object v2
.end method

.method private static createResourceBundleList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 505
    .local p0, "resourceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;>;"
    if-nez p0, :cond_1

    .line 506
    const/4 v7, 0x0

    .line 570
    :cond_0
    :goto_0
    return-object v7

    .line 508
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 511
    .local v7, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 513
    .local v6, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    if-eqz v6, :cond_2

    .line 519
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 522
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v5, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    .line 523
    .local v5, "mimeType":Ljava/lang/String;
    sget-object v11, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v4

    .line 525
    .local v4, "itemType":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 526
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "audio"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 527
    sget-object v11, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v4

    .line 536
    :cond_3
    :goto_2
    iget-object v10, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    .line 537
    .local v10, "time":Ljava/lang/String;
    const-wide/16 v2, -0x1

    .line 539
    .local v2, "duration":J
    const-string v11, "00:00:00"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 540
    invoke-static {v10}, Lcom/samsung/android/allshare/service/mediashare/utility/TimeUtil;->getTimeLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 542
    :cond_4
    const-wide/16 v12, 0x0

    cmp-long v11, v12, v2

    if-nez v11, :cond_5

    .line 543
    const-wide/16 v2, -0x1

    .line 546
    :cond_5
    iget-wide v8, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J

    .line 547
    .local v8, "size":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-gtz v11, :cond_6

    .line 548
    const-wide/16 v8, -0x1

    .line 552
    :cond_6
    const-string v11, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {v0, v11, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const-string v11, "BUNDLE_STRING_RESOURCE_ITEM_SEEKMODE"

    iget-object v12, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    invoke-static {v12}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getSeekModeFromProtocalInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getModeByBit(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item$SeekMode;->enumToString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v11, "BUNDLE_PARCELABLE_ITEM_URI"

    iget-object v12, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 556
    const-string v11, "BUNDLE_LONG_RESOURCE_ITEM_DURATION"

    invoke-virtual {v0, v11, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 557
    const-string v11, "BUNDLE_STRING_RESOURCE_ITEM_RESOLUTION"

    iget-object v12, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string v11, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v0, v11, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 560
    const-string v11, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v11, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v11, "BUNDLE_STRING_RESOURCE_ITEM_BITRATE"

    iget-object v12, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->bitrate:Ljava/lang/String;

    invoke-static {v12}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 564
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 528
    .end local v2    # "duration":J
    .end local v8    # "size":J
    .end local v10    # "time":Ljava/lang/String;
    :cond_7
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "video"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 529
    sget-object v11, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 530
    :cond_8
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 531
    sget-object v11, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 567
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v4    # "itemType":Ljava/lang/String;
    .end local v5    # "mimeType":Ljava/lang/String;
    .end local v6    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-gtz v11, :cond_0

    .line 568
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method public static getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "artistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 202
    .local p0, "attributes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 203
    :cond_0
    const/4 v0, 0x0

    .line 215
    :cond_1
    return-object v0

    .line 206
    :cond_2
    const/4 v0, 0x0

    .line 207
    .local v0, "artistvalue":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 208
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    .line 210
    .local v1, "curAttr":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 211
    iget-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 207
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static final getHostAddress(I)Ljava/lang/String;
    .locals 10
    .param p0, "n"    # I

    .prologue
    .line 392
    const/4 v4, 0x0

    .line 394
    .local v4, "hostAddrCnt":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v6

    .line 395
    .local v6, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-nez v6, :cond_0

    .line 396
    const-string v3, ""

    .line 420
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    return-object v3

    .line 399
    .restart local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 400
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 401
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 402
    .local v1, "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 403
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 404
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 408
    if-ge v4, p0, :cond_2

    .line 409
    add-int/lit8 v4, v4, 0x1

    .line 410
    goto :goto_1

    .line 413
    :cond_2
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 414
    .local v3, "host":Ljava/lang/String;
    goto :goto_0

    .line 417
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "host":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v2

    .line 418
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "BundleCreator"

    const-string v8, "getHostAddress"

    const-string v9, ""

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 420
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v3, ""

    goto :goto_0
.end method

.method public static getModeByBit(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1
    .param p0, "bitmode"    # Ljava/lang/String;

    .prologue
    .line 574
    if-nez p0, :cond_0

    .line 575
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 587
    :goto_0
    return-object v0

    .line 578
    :cond_0
    const-string v0, "01"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 580
    :cond_1
    const-string v0, "10"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->TIME:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 582
    :cond_2
    const-string v0, "11"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 583
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->ANY:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 584
    :cond_3
    const-string v0, "00"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 585
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->NONE:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 587
    :cond_4
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0
.end method

.method private static getSeekModeFromProtocalInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "protocalInfo"    # Ljava/lang/String;

    .prologue
    .line 479
    if-nez p0, :cond_1

    .line 480
    sget-object v2, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$SeekMode;->enumToString()Ljava/lang/String;

    move-result-object v1

    .line 487
    :cond_0
    :goto_0
    return-object v1

    .line 482
    :cond_1
    const-string v2, "DLNA.ORG_OP=\\d{2}"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 483
    .local v0, "matcher":Ljava/util/regex/Matcher;
    sget-object v2, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$SeekMode;->enumToString()Ljava/lang/String;

    move-result-object v1

    .line 484
    .local v1, "seekMode":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 485
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "DLNA.ORG_OP="

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static final isLocalAddress(Ljava/lang/String;)Z
    .locals 10
    .param p0, "targetAdd"    # Ljava/lang/String;

    .prologue
    .line 424
    const/4 v4, 0x0

    .line 427
    .local v4, "isLocalAddress":Z
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v6

    .line 428
    .local v6, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-nez v6, :cond_0

    .line 429
    const/4 v7, 0x0

    .line 450
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    return v7

    .line 432
    .restart local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 433
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 434
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 435
    .local v1, "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 436
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 437
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 441
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    .line 442
    .local v3, "host":Ljava/lang/String;
    if-eqz p0, :cond_1

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_1

    .line 443
    const/4 v4, 0x1

    goto :goto_1

    .line 447
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "host":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v2

    .line 448
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "BundleCreator"

    const-string v8, "isLocalAddress"

    const-string v9, ""

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    move v7, v4

    .line 450
    goto :goto_0
.end method

.method private static isUsableAddress(Ljava/net/InetAddress;)Z
    .locals 1
    .param p0, "addr"    # Ljava/net/InetAddress;

    .prologue
    .line 454
    instance-of v0, p0, Ljava/net/Inet6Address;

    if-eqz v0, :cond_0

    .line 455
    const/4 v0, 0x0

    .line 457
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static parseInt(Ljava/lang/String;)I
    .locals 6
    .param p0, "bitrate"    # Ljava/lang/String;

    .prologue
    .line 491
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 492
    :cond_0
    const/4 v1, -0x1

    .line 501
    :goto_0
    return v1

    .line 495
    :cond_1
    const/4 v1, -0x1

    .line 497
    .local v1, "result":I
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BundleCreator"

    const-string v3, "parseInt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static removeDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 3
    .param p0, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 364
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 365
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->sDeviceList:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    :cond_0
    return-void
.end method

.method private static valueOf(Lcom/samsung/android/allshare/Item$MediaType;)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # Lcom/samsung/android/allshare/Item$MediaType;

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
