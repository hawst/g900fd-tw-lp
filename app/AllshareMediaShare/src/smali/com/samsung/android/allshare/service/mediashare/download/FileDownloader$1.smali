.class Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;
.super Ljava/lang/Object;
.source "FileDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->doDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

.field final synthetic val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "Thread Start!!!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v22, v0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->isDownloadableSize(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Z
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "Thread End, not enough memory!!!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    new-instance v22, Ljava/io/IOException;

    const-string v23, "not_enough_memory"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyError(Ljava/lang/Exception;)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    const/4 v13, 0x0

    .line 79
    .local v13, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 80
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    const/4 v15, 0x0

    .line 81
    .local v15, "is":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 84
    .local v12, "file":Ljava/io/File;
    const/high16 v21, 0x80000

    :try_start_0
    move/from16 v0, v21

    new-array v4, v0, [B

    .line 87
    .local v4, "buffer":[B
    new-instance v20, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getURI()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 90
    .local v20, "url":Ljava/net/URL;
    sget-object v16, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .line 91
    .local v16, "proxy":Ljava/net/Proxy;
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 92
    .local v5, "conn":Ljava/net/HttpURLConnection;
    const/16 v21, 0x2710

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 93
    const-string v21, "GET"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 94
    const-string v21, "USER-AGENT"

    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/16 v21, 0x61a8

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 99
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->getArbitaryPath(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;
    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;

    move-result-object v12

    .line 102
    if-eqz v12, :cond_2

    .line 103
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "Item cancelled. Do not download"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_24
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_22
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_20
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1e
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 239
    if-eqz v15, :cond_3

    .line 240
    :try_start_1
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 245
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 247
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 252
    :cond_4
    :goto_2
    if-eqz v13, :cond_5

    .line 254
    :try_start_3
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 260
    :cond_5
    :goto_3
    if-eqz v12, :cond_0

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 241
    :catch_0
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 249
    .local v11, "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 112
    .end local v10    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyStart(Ljava/io/File;)V

    .line 113
    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_24
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_22
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_20
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1e
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 114
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .local v14, "fos":Ljava/io/FileOutputStream;
    :try_start_5
    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-direct {v3, v14}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_25
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_23
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_21
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1f
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 117
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v22

    const-wide/16 v24, 0x64

    div-long v18, v22, v24

    .line 118
    .local v18, "updateThreshhold":J
    const-wide/16 v6, 0x0

    .line 119
    .local v6, "cumReadCount":J
    const/16 v17, 0x0

    .line 121
    .local v17, "readCount":I
    const-wide/16 v8, 0x0

    .line 124
    .local v8, "downloadedSize":J
    :cond_7
    invoke-virtual {v15, v4}, Ljava/io/InputStream;->read([B)I

    move-result v17

    .line 126
    if-gez v17, :cond_12

    .line 127
    const-wide/16 v22, 0x0

    cmp-long v21, v8, v22

    if-lez v21, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v22

    const-wide/16 v24, 0x0

    cmp-long v21, v22, v24

    if-nez v21, :cond_d

    .line 160
    :cond_8
    :goto_4
    const-wide/16 v22, 0x0

    cmp-long v21, v6, v22

    if-lez v21, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-nez v21, :cond_9

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyProgress(J)V

    .line 167
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Z
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v21

    if-eqz v21, :cond_1b

    .line 239
    if-eqz v15, :cond_a

    .line 240
    :try_start_7
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 245
    :cond_a
    :goto_5
    if-eqz v3, :cond_b

    .line 247
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 252
    :cond_b
    :goto_6
    if-eqz v14, :cond_c

    .line 254
    :try_start_9
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 260
    :cond_c
    :goto_7
    if-eqz v12, :cond_0

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 130
    :cond_d
    :try_start_a
    new-instance v21, Ljava/io/IOException;

    const-string v22, "readCount -1"

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 198
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    :catch_3
    move-exception v10

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .line 199
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :goto_8
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "OutOfMemoryError - total memory:  "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v24

    const-wide/16 v26, 0x400

    div-long v24, v24, v26

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "KB, max memory: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v24

    const-wide/16 v26, 0x400

    div-long v24, v24, v26

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "KB"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 239
    if-eqz v15, :cond_e

    .line 240
    :try_start_c
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_f

    .line 245
    .end local v10    # "e":Ljava/lang/OutOfMemoryError;
    :cond_e
    :goto_9
    if-eqz v2, :cond_f

    .line 247
    :try_start_d
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_10

    .line 252
    :cond_f
    :goto_a
    if-eqz v13, :cond_10

    .line 254
    :try_start_e
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_11

    .line 260
    :cond_10
    :goto_b
    if-eqz v12, :cond_11

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 264
    :cond_11
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "Thread End!!!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_12
    if-lez v17, :cond_15

    .line 133
    const/16 v21, 0x0

    :try_start_f
    move/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v3, v4, v0, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 135
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v6, v6, v22

    .line 137
    cmp-long v21, v6, v18

    if-lez v21, :cond_14

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-nez v21, :cond_13

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyProgress(J)V

    .line 144
    :cond_13
    const-wide/16 v6, 0x0

    .line 146
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDownloadSize()J

    move-result-wide v22

    add-long v8, v22, v6

    .line 151
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v22

    cmp-long v21, v8, v22

    if-nez v21, :cond_1a

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "downloaded-size is equal to total-size, force break.."

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_4

    .line 203
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    :catch_4
    move-exception v10

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .line 204
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    .local v10, "e":Ljava/net/SocketTimeoutException;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :goto_d
    if-eqz v12, :cond_16

    .line 205
    :try_start_10
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 206
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Exception - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyError(Ljava/lang/Exception;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 239
    if-eqz v15, :cond_17

    .line 240
    :try_start_11
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_12

    .line 245
    .end local v10    # "e":Ljava/net/SocketTimeoutException;
    :cond_17
    :goto_e
    if-eqz v2, :cond_18

    .line 247
    :try_start_12
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_13

    .line 252
    :cond_18
    :goto_f
    if-eqz v13, :cond_19

    .line 254
    :try_start_13
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_14

    .line 260
    :cond_19
    :goto_10
    if-eqz v12, :cond_11

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 157
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_1a
    if-lez v17, :cond_8

    :try_start_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-nez v21, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Z
    :try_end_14
    .catch Ljava/lang/OutOfMemoryError; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_14 .. :try_end_14} :catch_4
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result v21

    if-eqz v21, :cond_7

    goto/16 :goto_4

    .line 241
    :catch_5
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_7
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    .line 170
    .end local v10    # "e":Ljava/io/IOException;
    :cond_1b
    if-eqz v3, :cond_1c

    .line 172
    :try_start_15
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_15 .. :try_end_15} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_15 .. :try_end_15} :catch_4
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_b
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 177
    :cond_1c
    :goto_11
    if-eqz v14, :cond_1d

    .line 179
    :try_start_16
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_16 .. :try_end_16} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_16 .. :try_end_16} :catch_4
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_b
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 185
    :cond_1d
    :goto_12
    :try_start_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-nez v21, :cond_2a

    .line 187
    const/16 v21, 0x0

    sput v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "retryCount reset to 0"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyFinish(Ljava/io/File;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_1e

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v22, v0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->downloadSubtitle(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$400(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    :try_end_17
    .catch Ljava/lang/OutOfMemoryError; {:try_start_17 .. :try_end_17} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_17 .. :try_end_17} :catch_4
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_9
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_b
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 239
    :cond_1e
    :goto_13
    if-eqz v15, :cond_1f

    .line 240
    :try_start_18
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_c

    .line 245
    :cond_1f
    :goto_14
    if-eqz v3, :cond_20

    .line 247
    :try_start_19
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_d

    .line 252
    :cond_20
    :goto_15
    if-eqz v14, :cond_21

    .line 254
    :try_start_1a
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_e

    .line 260
    :cond_21
    :goto_16
    if-eqz v12, :cond_34

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_c

    .line 173
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v11

    .line 174
    .restart local v11    # "e1":Ljava/io/IOException;
    :try_start_1b
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1b .. :try_end_1b} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_1b .. :try_end_1b} :catch_4
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto :goto_11

    .line 208
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v11    # "e1":Ljava/io/IOException;
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    :catch_9
    move-exception v10

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .line 210
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    .restart local v10    # "e":Ljava/io/IOException;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :goto_17
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->isCanceled()Z

    move-result v21

    if-nez v21, :cond_32

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Exception - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_30

    .line 214
    sget v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    add-int/lit8 v21, v21, 0x1

    sput v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "retryCount: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget v24, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    if-eqz v12, :cond_22

    .line 217
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 218
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->doDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 239
    :goto_18
    if-eqz v15, :cond_23

    .line 240
    :try_start_1d
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_15

    .line 245
    :cond_23
    :goto_19
    if-eqz v2, :cond_24

    .line 247
    :try_start_1e
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_16

    .line 252
    :cond_24
    :goto_1a
    if-eqz v13, :cond_25

    .line 254
    :try_start_1f
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_17

    .line 260
    :cond_25
    :goto_1b
    if-eqz v12, :cond_11

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 180
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    .restart local v20    # "url":Ljava/net/URL;
    :catch_a
    move-exception v10

    .line 181
    .restart local v10    # "e":Ljava/io/IOException;
    :try_start_20
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_20
    .catch Ljava/lang/OutOfMemoryError; {:try_start_20 .. :try_end_20} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_20 .. :try_end_20} :catch_4
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_9
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_b
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    goto/16 :goto_12

    .line 232
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v10    # "e":Ljava/io/IOException;
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    :catch_b
    move-exception v10

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .line 233
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    .local v10, "e":Ljava/lang/Exception;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :goto_1c
    if-eqz v12, :cond_26

    .line 234
    :try_start_21
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 235
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Exception - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyError(Ljava/lang/Exception;)V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    .line 239
    if-eqz v15, :cond_27

    .line 240
    :try_start_22
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_18

    .line 245
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_27
    :goto_1d
    if-eqz v2, :cond_28

    .line 247
    :try_start_23
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_19

    .line 252
    :cond_28
    :goto_1e
    if-eqz v13, :cond_29

    .line 254
    :try_start_24
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_1a

    .line 260
    :cond_29
    :goto_1f
    if-eqz v12, :cond_11

    .line 261
    sget-object v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 194
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_2a
    if-eqz v12, :cond_2b

    .line 195
    :try_start_25
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 196
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "ITEM Canceled"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/lang/OutOfMemoryError; {:try_start_25 .. :try_end_25} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_25 .. :try_end_25} :catch_4
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_9
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_b
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    goto/16 :goto_13

    .line 238
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    :catchall_0
    move-exception v21

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .line 239
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :goto_20
    if-eqz v15, :cond_2c

    .line 240
    :try_start_26
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_1b

    .line 245
    :cond_2c
    :goto_21
    if-eqz v2, :cond_2d

    .line 247
    :try_start_27
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_1c

    .line 252
    :cond_2d
    :goto_22
    if-eqz v13, :cond_2e

    .line 254
    :try_start_28
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_1d

    .line 260
    :cond_2e
    :goto_23
    if-eqz v12, :cond_2f

    .line 261
    sget-object v22, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2f
    throw v21

    .line 241
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    .restart local v20    # "url":Ljava/net/URL;
    :catch_c
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_14

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_15

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_e
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_16

    .line 241
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v6    # "cumReadCount":J
    .end local v8    # "downloadedSize":J
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v17    # "readCount":I
    .end local v18    # "updateThreshhold":J
    .end local v20    # "url":Ljava/net/URL;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :catch_f
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_9

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_a

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_11
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_b

    .line 241
    .local v10, "e":Ljava/net/SocketTimeoutException;
    :catch_12
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_e

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_13
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_f

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_14
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_10

    .line 221
    :cond_30
    const/16 v21, 0x0

    :try_start_29
    sput v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 222
    if-eqz v12, :cond_31

    .line 223
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 224
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "retryCount: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget v24, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->val$item:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyError(Ljava/lang/Exception;)V

    goto/16 :goto_18

    .line 238
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v21

    goto/16 :goto_20

    .line 228
    .restart local v10    # "e":Ljava/io/IOException;
    :cond_32
    if-eqz v12, :cond_33

    .line 229
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 230
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "doDownload"

    const-string v23, "ITEM Canceled"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_1

    goto/16 :goto_18

    .line 241
    :catch_15
    move-exception v10

    .line 242
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_19

    .line 248
    :catch_16
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1a

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_17
    move-exception v10

    .line 256
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1b

    .line 241
    .local v10, "e":Ljava/lang/Exception;
    :catch_18
    move-exception v10

    .line 242
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1d

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_19
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1e

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_1a
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1f

    .line 241
    .end local v10    # "e":Ljava/io/IOException;
    :catch_1b
    move-exception v10

    .line 242
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_21

    .line 248
    .end local v10    # "e":Ljava/io/IOException;
    :catch_1c
    move-exception v11

    .line 249
    .restart local v11    # "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_22

    .line 255
    .end local v11    # "e1":Ljava/io/IOException;
    :catch_1d
    move-exception v10

    .line 256
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_23

    .line 238
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v20    # "url":Ljava/net/URL;
    :catchall_2
    move-exception v21

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_20

    .line 232
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    :catch_1e
    move-exception v10

    goto/16 :goto_1c

    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v20    # "url":Ljava/net/URL;
    :catch_1f
    move-exception v10

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_1c

    .line 208
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    :catch_20
    move-exception v10

    goto/16 :goto_17

    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v20    # "url":Ljava/net/URL;
    :catch_21
    move-exception v10

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_17

    .line 203
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    :catch_22
    move-exception v10

    goto/16 :goto_d

    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v20    # "url":Ljava/net/URL;
    :catch_23
    move-exception v10

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .line 198
    .end local v4    # "buffer":[B
    .end local v5    # "conn":Ljava/net/HttpURLConnection;
    .end local v16    # "proxy":Ljava/net/Proxy;
    .end local v20    # "url":Ljava/net/URL;
    :catch_24
    move-exception v10

    goto/16 :goto_8

    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "proxy":Ljava/net/Proxy;
    .restart local v20    # "url":Ljava/net/URL;
    :catch_25
    move-exception v10

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v6    # "cumReadCount":J
    .restart local v8    # "downloadedSize":J
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v17    # "readCount":I
    .restart local v18    # "updateThreshhold":J
    :cond_34
    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_c
.end method
