.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;
.super Ljava/lang/Object;
.source "ViewControllerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 78
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    if-nez v4, :cond_1

    .line 79
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "run"

    const-string v6, "cvm is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 84
    .local v1, "bundle":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "actionID":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 86
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "run"

    const-string v6, "bundle/action id is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    const-class v4, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 91
    const-string v4, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "udn":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v2

    .line 94
    .local v2, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    if-nez v4, :cond_5

    .line 95
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "run"

    const-string v6, "deviceItem == null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v4, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Landroid/os/Bundle;)V

    goto :goto_0

    .line 102
    :cond_5
    const-string v4, "com.sec.android.allshare.action.ACTION_VIEWCONTROLLER_REQUEST_GET_ZOOM_PORT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;

    iget-object v5, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->requestGetZoomPort(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ViewControllerAsyncActionHandler$ViewControllerActionWorker;Ljava/lang/String;)V

    goto :goto_0
.end method
