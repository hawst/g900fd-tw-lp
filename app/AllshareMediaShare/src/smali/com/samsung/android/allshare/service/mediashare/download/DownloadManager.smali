.class public Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$7;,
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    }
.end annotation


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mDeviceType:Ljava/lang/String;

.field private mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

.field private mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

.field private mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

.field private mIsMemoryFull:Z

.field private mIsUpdate:Z

.field private mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

.field private mLastUpdatedTime:J

.field private mNotiManager:Landroid/app/NotificationManager;

.field private mNotification:Landroid/app/Notification;

.field private mProgressView:Landroid/widget/RemoteViews;

.field private mServiceConn:Landroid/content/ServiceConnection;

.field private mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field private final mTag:Ljava/lang/String;

.field private mhandler:Landroid/os/Handler;

.field private scanItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;",
            ">;"
        }
    .end annotation
.end field

.field private thumbnailMax:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mProgressView:Landroid/widget/RemoteViews;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;

    .line 86
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->thumbnailMax:I

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z

    .line 98
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    .line 100
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastUpdatedTime:J

    .line 119
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceConn:Landroid/content/ServiceConnection;

    .line 570
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 106
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 114
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->init()V

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->saveThumbnail(Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Landroid/app/NotificationManager;)Landroid/app/NotificationManager;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Landroid/app/NotificationManager;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getFileSize(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z

    return p1
.end method

.method static synthetic access$702(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsUpdate:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceConn:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private static getExifOrientation(Ljava/lang/String;)I
    .locals 7
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 536
    const/4 v0, 0x0

    .line 537
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 540
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 544
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 545
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 546
    .local v4, "orientation":I
    if-eq v4, v6, :cond_0

    .line 548
    packed-switch v4, :pswitch_data_0

    .line 563
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 541
    :catch_0
    move-exception v1

    .line 542
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 550
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 551
    goto :goto_1

    .line 553
    :pswitch_2
    const/16 v0, 0xb4

    .line 554
    goto :goto_1

    .line 556
    :pswitch_3
    const/16 v0, 0x10e

    .line 557
    goto :goto_1

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getFileSize(Ljava/lang/String;)J
    .locals 10
    .param p1, "FilePathOrWebUri"    # Ljava/lang/String;

    .prologue
    .line 200
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 203
    .local v3, "url":Ljava/net/URL;
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .line 205
    .local v2, "proxy":Ljava/net/Proxy;
    invoke-virtual {v3, v2}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 206
    .local v0, "conn":Ljava/net/HttpURLConnection;
    const-string v6, "HEAD"

    invoke-virtual {v0, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 208
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v6

    int-to-long v4, v6

    .line 209
    .local v4, "result":J
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v7, "getFileSize"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "result = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 219
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .end local v2    # "proxy":Ljava/net/Proxy;
    .end local v3    # "url":Ljava/net/URL;
    .end local v4    # "result":J
    :goto_0
    return-wide v4

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/net/SocketTimeoutException;
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v7, "getFileSize"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :goto_1
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 213
    :catch_1
    move-exception v1

    .line 214
    .local v1, "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v7, "getFileSize"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 215
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 216
    .local v1, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v7, "getFileSize"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getRunningTasks()Ljava/lang/String;
    .locals 5

    .prologue
    .line 777
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 779
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 781
    .local v1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 782
    const-string v3, ""

    .line 785
    :goto_0
    return-object v3

    .line 783
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 785
    .local v2, "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v3, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private init()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    .line 165
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    .line 167
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    .line 169
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "STOP_SERVICE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "CANCEL_DOWNLOAD"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 171
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 173
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 175
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    div-long/2addr v2, v6

    div-long v0, v2, v6

    .line 176
    .local v0, "maxMemory":J
    const-wide/16 v2, 0x8

    mul-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->thumbnailMax:I

    .line 178
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app max memory: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "max thumbnail number: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->thumbnailMax:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method private static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 519
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 520
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 521
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 523
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 524
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    move-object p0, v7

    .line 532
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 528
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 529
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private saveThumbnail(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 20
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 434
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 438
    .local v17, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    if-eqz v17, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->isThumbnailMax()Z

    move-result v3

    if-nez v3, :cond_0

    .line 442
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->thumbnailMax:I

    if-lt v3, v4, :cond_2

    .line 443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v4, "saveThumbnail"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Download list exceed max.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->thumbnailMax:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") do not save thumbnail"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setThumbnailMax()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 509
    :catch_0
    move-exception v12

    .line 510
    .local v12, "e":Ljava/lang/OutOfMemoryError;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v4, "saveThumbnail"

    const-string v5, "OutOfMemoryError occured. do not save thumbnail"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setThumbnailMax()V

    .line 512
    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 447
    .end local v12    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 448
    .local v2, "cr":Landroid/content/ContentResolver;
    const/16 v19, 0x0

    .line 449
    .local v19, "thumbnail":Landroid/graphics/Bitmap;
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$7;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 505
    :cond_3
    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 506
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    goto/16 :goto_0

    .line 451
    :pswitch_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 452
    .local v10, "audioCursor":Landroid/database/Cursor;
    const-wide/16 v8, -0x1

    .line 453
    .local v8, "albumID":J
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 454
    const-string v3, "album_id"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 457
    :cond_4
    if-eqz v10, :cond_5

    .line 458
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 459
    :cond_5
    const/16 v18, 0x0

    .line 461
    .local v18, "tempThumbnail":Landroid/graphics/Bitmap;
    :try_start_2
    const-string v3, "content://media/external/audio/albumart"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 465
    if-eqz v18, :cond_6

    .line 466
    const/16 v3, 0xa5

    const/16 v4, 0xa5

    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v19

    .line 473
    :cond_6
    if-eqz v18, :cond_3

    .line 474
    :try_start_3
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 468
    :catch_1
    move-exception v12

    .line 469
    .local v12, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 473
    if-eqz v18, :cond_3

    .line 474
    :try_start_5
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    .line 470
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v12

    .line 471
    .local v12, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 473
    if-eqz v18, :cond_3

    .line 474
    :try_start_7
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 473
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v18, :cond_7

    .line 474
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    :cond_7
    throw v3

    .line 478
    .end local v8    # "albumID":J
    .end local v10    # "audioCursor":Landroid/database/Cursor;
    .end local v18    # "tempThumbnail":Landroid/graphics/Bitmap;
    :pswitch_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 479
    .local v13, "imageCursor":Landroid/database/Cursor;
    const-wide/16 v14, -0x1

    .line 480
    .local v14, "imageId":J
    const/4 v11, 0x0

    .line 481
    .local v11, "degree":I
    if-eqz v13, :cond_8

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 482
    const-string v3, "_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 484
    const-string v3, "orientation"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 486
    .local v16, "index":I
    move/from16 v0, v16

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 488
    if-nez v11, :cond_8

    .line 489
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getExifOrientation(Ljava/lang/String;)I

    move-result v11

    .line 491
    .end local v16    # "index":I
    :cond_8
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v14, v15, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 493
    if-eqz v19, :cond_9

    if-eqz v11, :cond_9

    .line 494
    move-object/from16 v0, v19

    invoke-static {v0, v11}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 496
    :cond_9
    if-eqz v13, :cond_3

    .line 497
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 500
    .end local v11    # "degree":I
    .end local v13    # "imageCursor":Landroid/database/Cursor;
    .end local v14    # "imageId":J
    :pswitch_2
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0

    move-result-object v19

    goto/16 :goto_1

    .line 449
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showNotification()V
    .locals 14

    .prologue
    .line 606
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->isComplete()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 607
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v10, "showNotification()"

    const-string v11, "Invalid request: download is completed"

    invoke-static {v7, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v7, :cond_2

    .line 612
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-string v10, "notification"

    invoke-virtual {v7, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    iput-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    .line 615
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    sget-object v10, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v7, v10, :cond_3

    .line 616
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v7}, Landroid/app/NotificationManager;->cancelAll()V

    .line 617
    :cond_3
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    iput-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 619
    iget-boolean v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsUpdate:Z

    if-nez v7, :cond_4

    .line 621
    new-instance v7, Landroid/app/Notification;

    invoke-direct {v7}, Landroid/app/Notification;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    .line 623
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget v10, v7, Landroid/app/Notification;->flags:I

    or-int/lit8 v10, v10, 0xa

    iput v10, v7, Landroid/app/Notification;->flags:I

    .line 625
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget v10, v7, Landroid/app/Notification;->defaults:I

    or-int/lit8 v10, v10, 0x1

    iput v10, v7, Landroid/app/Notification;->defaults:I

    .line 626
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    const v10, 0x1080081

    iput v10, v7, Landroid/app/Notification;->icon:I

    .line 628
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-class v10, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-direct {v1, v7, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 629
    .local v1, "it":Landroid/content/Intent;
    const/high16 v7, 0x10800000

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 631
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    invoke-static {v7, v10, v1, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 633
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iput-object v0, v7, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 635
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v1    # "it":Landroid/content/Intent;
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastUpdatedTime:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x1f4

    cmp-long v7, v10, v12

    if-ltz v7, :cond_0

    .line 638
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastUpdatedTime:J

    .line 639
    new-instance v7, Landroid/widget/RemoteViews;

    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f030002

    invoke-direct {v7, v10, v11}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mProgressView:Landroid/widget/RemoteViews;

    .line 641
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v10, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mProgressView:Landroid/widget/RemoteViews;

    iput-object v10, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 642
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0013

    const v11, 0x1080081

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 644
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v6

    .line 645
    .local v6, "position":I
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7, v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 646
    .local v2, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    const/16 v3, 0x64

    .line 647
    .local v3, "max":I
    const/4 v4, 0x0

    .line 649
    .local v4, "percent":I
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v8

    .line 650
    .local v8, "totalSize":J
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 651
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDownloadSize()J

    move-result-wide v10

    const-wide/16 v12, 0x64

    mul-long/2addr v10, v12

    div-long/2addr v10, v8

    long-to-int v4, v10

    .line 653
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v7

    const/4 v10, 0x1

    if-le v7, v10, :cond_6

    .line 654
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0019

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v6, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 657
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0016

    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const v12, 0x7f07000e

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 659
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0015

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 660
    if-nez v4, :cond_7

    .line 661
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0017

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 662
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0018

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v7, v10, v11, v12, v13}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 670
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    new-instance v11, Landroid/content/Intent;

    const-string v12, "CANCEL_DOWNLOAD"

    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v12, 0x8000000

    invoke-static {v7, v10, v11, v12}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 672
    .local v5, "pi":Landroid/app/PendingIntent;
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0014

    invoke-virtual {v7, v10, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 674
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    const-string v10, "ASFDownload"

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v7, v10, v11, v12}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 664
    .end local v5    # "pi":Landroid/app/PendingIntent;
    :cond_7
    const/16 v7, 0x64

    if-le v4, v7, :cond_8

    .line 665
    const/16 v4, 0x64

    .line 666
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0018

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v3, v4, v11}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 667
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotification:Landroid/app/Notification;

    iget-object v7, v7, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v10, 0x7f0a0017

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public addDownloadList(Ljava/lang/String;Ljava/util/ArrayList;)Z
    .locals 2
    .param p1, "serverName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 223
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 224
    :cond_0
    const/4 v0, 0x0

    .line 312
    :goto_0
    return v0

    .line 226
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 312
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cancelDownload()V
    .locals 8

    .prologue
    .line 744
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 745
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    .line 747
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v2}, Landroid/app/NotificationManager;->cancelAll()V

    .line 749
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 750
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 751
    .local v1, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v2

    if-nez v2, :cond_2

    .line 753
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->notifyCancelled()V

    .line 754
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    if-eqz v2, :cond_2

    .line 755
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalCancelledCount(I)V

    .line 757
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadedSize(J)V

    .line 761
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setCurrentIndex(I)V

    .line 762
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateSelectAll(Ljava/lang/String;)V

    goto :goto_0

    .line 765
    .end local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    .line 766
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateProgress()V

    .line 767
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;

    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$6;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 773
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    .line 774
    return-void
.end method

.method public completeNotification()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 679
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v5, :cond_0

    .line 680
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-string v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    iput-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    .line 682
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v5}, Landroid/app/NotificationManager;->cancelAll()V

    .line 683
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    if-eqz v5, :cond_1

    .line 684
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->releaseLock()V

    .line 686
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    sget-object v6, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v5, v6, :cond_2

    .line 687
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v5}, Landroid/app/NotificationManager;->cancelAll()V

    .line 688
    sget-object v5, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    iput-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 691
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    sget-object v6, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v5, v6, :cond_4

    .line 692
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getRunningTasks()Ljava/lang/String;

    move-result-object v1

    .line 694
    .local v1, "classname":Ljava/lang/String;
    const-string v5, "com.samsung.android.allshare.service.mediashare.download.ui"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 695
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 696
    .local v0, "builder":Landroid/app/Notification$Builder;
    const v5, 0x1080082

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 697
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 698
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const v6, 0x7f070013

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 702
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalCancelledCount()I

    move-result v5

    if-lez v5, :cond_5

    .line 703
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const v6, 0x7f07000a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 707
    :cond_3
    :goto_0
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 708
    .local v3, "it":Landroid/content/Intent;
    const/high16 v5, 0x10800000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 711
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const/high16 v6, 0x8000000

    invoke-static {v5, v8, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 714
    .local v2, "contentIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 715
    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 716
    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 718
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 719
    .local v4, "notification":Landroid/app/Notification;
    iget v5, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x8

    iput v5, v4, Landroid/app/Notification;->flags:I

    .line 720
    iget v5, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Landroid/app/Notification;->defaults:I

    .line 722
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "STOP_SERVICE"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v8, v6, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, v4, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 724
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    const-string v6, "ASFDownload"

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 725
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;

    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$5;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$5;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 736
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;

    const-string v6, "completeNotification"

    const-string v7, "notify completeNotification"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v1    # "classname":Ljava/lang/String;
    .end local v2    # "contentIntent":Landroid/app/PendingIntent;
    .end local v3    # "it":Landroid/content/Intent;
    .end local v4    # "notification":Landroid/app/Notification;
    :cond_4
    :goto_1
    return-void

    .line 704
    .restart local v0    # "builder":Landroid/app/Notification$Builder;
    .restart local v1    # "classname":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v5

    if-lez v5, :cond_3

    .line 705
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const v6, 0x7f070009

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 738
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    :cond_6
    sget-object v5, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    iput-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    goto :goto_1
.end method

.method public fini()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 187
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->terminate()V

    .line 192
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    .line 193
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 196
    return-void
.end method

.method public getDownloadItemList()Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    return-object v0
.end method

.method public getNotiType()Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-object v0
.end method

.method public isTablet()Z
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 146
    :goto_0
    return v0

    .line 145
    :cond_0
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDeviceType:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public itemDownload()V
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getCurrentIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .line 318
    .local v0, "item":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->setTransportListener(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;)V

    .line 430
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mFileDownloader:Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->doDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    .line 431
    return-void
.end method

.method public setNotiType(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;)V
    .locals 2
    .param p1, "notiType"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .prologue
    .line 793
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 794
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v0, v1, :cond_1

    .line 795
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    .line 798
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 800
    :cond_1
    return-void
.end method
