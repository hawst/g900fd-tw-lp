.class public Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
.super Ljava/lang/Object;
.source "Metadata.java"


# instance fields
.field public album_art_path:Ljava/lang/String;

.field public mimeType:Ljava/lang/String;

.field public subtitlePath:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "subPath"    # Ljava/lang/String;
    .param p4, "artPath"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->subtitlePath:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->album_art_path:Ljava/lang/String;

    .line 37
    return-void
.end method
