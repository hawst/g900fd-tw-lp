.class Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;
.super Ljava/lang/Object;
.source "ServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0

    .prologue
    .line 820
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 825
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mRunnableTimer"

    const-string v3, "[ TRACE ] Stop Service"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 828
    .local v0, "stop_service":Landroid/content/Intent;
    const-string v1, "com.samsung.android.allshare.service.mediashare"

    const-string v2, "com.samsung.android.allshare.service.mediashare.ServiceManager"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 830
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->serviceMutex:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 831
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->isBind:Z
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$300(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 832
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->stopService(Landroid/content/Intent;)Z

    .line 834
    :cond_0
    monitor-exit v2

    .line 835
    return-void

    .line 834
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
