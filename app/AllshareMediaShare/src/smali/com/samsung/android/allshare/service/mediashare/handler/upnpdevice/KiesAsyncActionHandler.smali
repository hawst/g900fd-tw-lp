.class public Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "KiesAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;,
        Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;
    }
.end annotation


# static fields
.field private static final DEVICE_STATE_ADD:I = 0x0

.field private static final DEVICE_STATE_REMOVE:I = 0x1

.field private static final KIES_DEVICE_TYPE:I = 0x8

.field private static final MAX_KIES_ACTION_WAITING_TIME:J = 0x1d4c0L

.field private static deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

.field private static kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    .line 61
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 65
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method static synthetic access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 114
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 72
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_SYNC_CONNECTION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 73
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_AUTO_SYNC_CONNECTION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 74
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_OPTIONAL_COMMAND"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->removeKiesEventListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;)V

    .line 95
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->removekiesDeviceNotifyListeners(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;)V

    .line 96
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->addKiesEventListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesEventListener;)V

    .line 84
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->addkiesDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesDeviceNotifyListener;)V

    .line 85
    return-void
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 358
    :cond_0
    if-nez p1, :cond_1

    .line 359
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->subscribe(Ljava/lang/String;)I

    goto :goto_0

    .line 360
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 361
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->unsubscribe(Ljava/lang/String;)I

    goto :goto_0

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onEventNotify(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v3, "onEventNotify"

    const-string v4, "---events list---"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;

    .line 377
    .local v0, "event":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v3, "onEventNotify"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    .end local v0    # "event":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    :cond_0
    return-void
.end method

.method public onNetworkExcetption(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "exceptionType"    # Ljava/lang/String;
    .param p2, "exceptionValue"    # Ljava/lang/String;
    .param p3, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 369
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onNetworkExcetption"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    return-void
.end method
