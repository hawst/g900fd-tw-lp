.class public abstract Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.super Ljava/lang/Object;
.source "AsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "ActionWorker"
.end annotation


# instance fields
.field protected mActionId:Ljava/lang/String;

.field protected mReplyMessenger:Landroid/os/Messenger;

.field protected mResId:J

.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    return-void
.end method


# virtual methods
.method protected response(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 386
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 388
    .local v0, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mActionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 389
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgType(I)V

    .line 390
    iget-wide v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mResId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgID(J)V

    .line 391
    if-nez p1, :cond_0

    .line 392
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "bundle":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 394
    .restart local p1    # "bundle":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 395
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->mReplyMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setMessenger(Landroid/os/Messenger;)V

    .line 397
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->getManager()Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->publishCVMessage(Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 399
    return-void
.end method

.method protected responseFailMsg(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 402
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 403
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;->response(Landroid/os/Bundle;)V

    .line 405
    return-void
.end method

.method public abstract work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end method
