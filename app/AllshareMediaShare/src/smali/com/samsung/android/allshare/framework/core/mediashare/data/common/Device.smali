.class public Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
.super Ljava/lang/Object;
.source "Device.java"


# static fields
.field public static final TYPE_AVPLAYER:I = 0x4

.field public static final TYPE_DMPR:I = 0xb

.field public static final TYPE_FILERECEIVER:I = 0x7

.field public static final TYPE_IMAGEVIEWER:I = 0x1

.field public static final TYPE_KIES:I = 0x8

.field public static final TYPE_PMR:I = 0xa

.field public static final TYPE_PROVIDER:I = 0x2

.field public static final TYPE_RCR:I = 0x9

.field public static final TYPE_RECEIVER:I = 0x3

.field public static final TYPE_SMARTCONTROLLER_CE:I = 0x6

.field public static final TYPE_SMARTCONTROLLER_TV:I = 0x5

.field public static final TYPE_UNKNOWN:I = 0xff


# instance fields
.field public ID:Ljava/lang/String;

.field public IPAddr:Ljava/lang/String;

.field public TVHeightResolution:I

.field public TVWidthResolution:I

.field public UDN:Ljava/lang/String;

.field public boundInterfaceAddress:Ljava/lang/String;

.field public boundInterfaceName:Ljava/lang/String;

.field public defaultIcon:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;

.field public deviceID:Ljava/lang/String;

.field public firmwareVer:Ljava/lang/String;

.field public iconList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceIcon;",
            ">;"
        }
    .end annotation
.end field

.field public isCEControlable:Z

.field public isDownloadable:Z

.field public isLocalDevice:Z

.field public isMovable:Z

.field public isNavigateInPause:Z

.field public isRotatable:Z

.field public isSPCImageZoomable:Z

.field public isSPCSlideShowSupport:Z

.field public isSamsungTV:Z

.field public isSearchable:Z

.field public isSeekOnPaused:Z

.field public isSeekable:Z

.field public isSlideshowSupport:Z

.field public isSupportAudio:Z

.field public isSupportAudioPlayList:Z

.field public isSupportImage:Z

.field public isSupportImagePlayList:Z

.field public isSupportVideo:Z

.field public isSupportVideoPlayList:Z

.field public isTVControlable:Z

.field public isWebURLPlayable:Z

.field public isWholeHomeAudio:Z

.field public isZoomable:Z

.field public modelName:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public portNumber:Ljava/lang/String;

.field public productCap:Ljava/lang/String;

.field public productType:Ljava/lang/String;

.field public samsungTVYear:I

.field public searchCapability:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] name["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] IPAddr["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
