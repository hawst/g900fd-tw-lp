.class Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;
.super Ljava/lang/Object;
.source "DownloadUIItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 70
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 71
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mFileName:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mMimeType:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$400(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->onDownloadSelectionChanged(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getCurrentView()Landroid/widget/ListView;

    move-result-object v0

    .line 75
    .local v0, "curruntView":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 76
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mPosition:I
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 78
    .end local v0    # "curruntView":Landroid/widget/ListView;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 79
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;)Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->updateSelectAllMenu()V

    .line 80
    :cond_1
    return-void
.end method
