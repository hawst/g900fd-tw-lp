.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;
.super Ljava/lang/Object;
.source "AVPlayerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventNotifier"
.end annotation


# instance fields
.field private deviceUdn:Ljava/lang/String;

.field private eventName:Ljava/lang/String;

.field private eventValue:Ljava/lang/String;

.field private manager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "sm"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .param p3, "udn"    # Ljava/lang/String;
    .param p4, "eventName"    # Ljava/lang/String;
    .param p5, "eventValue"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 194
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    .line 188
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    .line 190
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    .line 192
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->manager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 195
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    .line 196
    iput-object p4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    .line 197
    iput-object p5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    .line 198
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->manager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 199
    return-void
.end method

.method private notifyCurrentTrackURI()V
    .locals 6

    .prologue
    .line 215
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 216
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ResourceUriHelper;->unEscapeUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "playingUri":Ljava/lang/String;
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v2, "BUNDLE_STRING_APP_ITEM_ID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notifyCurrentTrackURI"

    const-string v4, "--------> Notify change ~~~~, state = content change"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notifyCurrentTrackURI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playingUri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    const-string v4, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_CONTENT_CHANGED"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->notifyPlayerStatus(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 227
    return-void
.end method

.method private notifyTransportState()V
    .locals 6

    .prologue
    .line 251
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    const-string v3, "TRANSITIONING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 252
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notifyTransportState"

    const-string v4, "receive TRANSITIONING, start polling task"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->manager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 261
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 262
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertToAllShareEventString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "playState":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->notifyPlayerStatus(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v2, v3, v1, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 268
    return-void

    .line 256
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "playState":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notifyTransportState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", stop polling task"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->manager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopBufferingStatePolling(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyTransportStatus()V
    .locals 8

    .prologue
    .line 230
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "notifyTransportStatus"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    const-string v5, "ERROR_OCCURRED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 234
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->queryPlayActionMonitor(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    move-result-object v3

    .line 235
    .local v3, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const/4 v1, 0x0

    .line 236
    .local v1, "index":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    invoke-direct {p0, v4, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->queryActionMonitor(Ljava/lang/String;I)Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 237
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getRequestID()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "notifyTransportStatus"

    const-string v6, "stop GetTransportInfoThread "

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 242
    .end local v1    # "index":I
    .restart local v2    # "index":I
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 243
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_CATEGORY"

    const-string v5, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v4, "BUNDLE_STRING_ID"

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    const-string v6, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->notifyPlayerStatus(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 248
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "index":I
    .end local v3    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_1
    return-void
.end method

.method private queryActionMonitor(Ljava/lang/String;I)Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .locals 7
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 271
    const/4 v4, 0x0

    .line 272
    .local v4, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const/4 v0, 0x0

    .line 274
    .local v0, "curIndex":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 275
    .local v5, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 276
    .local v1, "enties":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 277
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 278
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;"
    if-eqz v2, :cond_0

    .line 279
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 280
    .local v3, "m":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateDevice(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-ne v0, p2, :cond_2

    .line 281
    move-object v4, v3

    .line 289
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;"
    .end local v3    # "m":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_1
    return-object v4

    .line 284
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;"
    .restart local v3    # "m":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private queryPlayActionMonitor(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .locals 7
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 293
    const/4 v3, 0x0

    .line 295
    .local v3, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 296
    .local v4, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 297
    .local v0, "enties":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 298
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 299
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;"
    if-eqz v1, :cond_0

    .line 300
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 301
    .local v2, "m":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getActionName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Play"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateDevice(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 302
    move-object v3, v2

    .line 308
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;>;"
    .end local v2    # "m":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_1
    return-object v3
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->deviceUdn:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventValue:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    const-string v0, "TransportState"

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->notifyTransportState()V

    goto :goto_0

    .line 207
    :cond_2
    const-string v0, "TransportStatus"

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->notifyTransportStatus()V

    goto :goto_0

    .line 209
    :cond_3
    const-string v0, "CurrentTrackURI"

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->eventName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;->notifyCurrentTrackURI()V

    goto :goto_0
.end method
