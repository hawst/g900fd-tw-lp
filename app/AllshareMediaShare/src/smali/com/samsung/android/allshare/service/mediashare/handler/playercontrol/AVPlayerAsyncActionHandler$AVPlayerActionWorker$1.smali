.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;
.super Ljava/lang/Object;
.source "AVPlayerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 57

    .prologue
    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    if-nez v4, :cond_1

    .line 450
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "AVPlayerActionWorker"

    const-string v10, "cvm is null"

    invoke-static {v4, v6, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v43

    .line 454
    .local v43, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v20

    .line 455
    .local v20, "actionID":Ljava/lang/String;
    if-eqz v20, :cond_2

    if-nez v43, :cond_3

    .line 456
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "AVPlayerActionWorker"

    const-string v10, "bundle/action id is null"

    invoke-static {v4, v6, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_3
    const-class v4, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 460
    const-string v4, "BUNDLE_STRING_ID"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 462
    .local v14, "udn":Ljava/lang/String;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 463
    .local v5, "itemBundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_TITLE"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 464
    .local v35, "title":Ljava/lang/String;
    const-string v4, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/media/ContentInfo;

    .line 466
    .local v7, "ci":Lcom/samsung/android/allshare/media/ContentInfo;
    const-wide/16 v8, 0x0

    .line 468
    .local v8, "startingPosition":J
    if-eqz v7, :cond_5

    .line 469
    invoke-virtual {v7}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v8

    .line 477
    :goto_1
    const-string v4, "BUNDLE_LONG_POSITION"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v50

    .line 478
    .local v50, "position":J
    const-string v4, "BUNDLE_INT_VOLUME"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v56

    .line 479
    .local v56, "volume":I
    const-string v4, "BUNDLE_BOOLEAN_MUTE"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v46

    .line 482
    .local v46, "isMute":Z
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_PLAY_POSITION"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 483
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "AVPlayerActionWorker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "actionID = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v6, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :cond_4
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 487
    if-eqz v5, :cond_0

    .line 488
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v4

    invoke-virtual {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 490
    const-string v4, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 492
    .local v48, "objectID":Ljava/lang/String;
    const-string v4, "BUNDLE_STRING_ID"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    .line 494
    .local v55, "serverUdn":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/os/Bundle;

    .line 495
    .local v54, "sItemBundle":Landroid/os/Bundle;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const/4 v6, 0x0

    move-object/from16 v0, v54

    move-object/from16 v1, v48

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v4, v14, v0, v1, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v54

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 473
    .end local v46    # "isMute":Z
    .end local v48    # "objectID":Ljava/lang/String;
    .end local v50    # "position":J
    .end local v54    # "sItemBundle":Landroid/os/Bundle;
    .end local v55    # "serverUdn":Ljava/lang/String;
    .end local v56    # "volume":I
    :cond_5
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    goto/16 :goto_1

    .line 501
    .restart local v46    # "isMute":Z
    .restart local v48    # "objectID":Ljava/lang/String;
    .restart local v50    # "position":J
    .restart local v54    # "sItemBundle":Landroid/os/Bundle;
    .restart local v55    # "serverUdn":Ljava/lang/String;
    .restart local v56    # "volume":I
    :cond_6
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v4

    const/4 v6, 0x2

    move-object/from16 v0, v55

    invoke-virtual {v4, v6, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v44

    .line 503
    .local v44, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const-string v19, ""

    .line 504
    .local v19, "deviceName":Ljava/lang/String;
    if-eqz v44, :cond_7

    .line 505
    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 508
    :cond_7
    new-instance v42, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;

    invoke-direct/range {v42 .. v42}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;-><init>()V

    .line 509
    .local v42, "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    move-object/from16 v0, v42

    move-object/from16 v1, v55

    move-object/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v13

    .line 511
    .local v13, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->discard()V

    .line 513
    if-nez v13, :cond_8

    .line 514
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    sget-object v6, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V
    invoke-static/range {v4 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 519
    :cond_8
    const/16 v53, 0x0

    .line 521
    .local v53, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v4, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    if-eqz v4, :cond_b

    iget-object v4, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_b

    .line 522
    iget-object v4, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v45

    .local v45, "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 523
    .local v52, "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    move-object/from16 v0, v52

    iget-object v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    const-string v6, "DLNA.ORG_CI=1"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v41

    .line 525
    .local v41, "bOptionalCI":Z
    if-nez v41, :cond_9

    .line 526
    move-object/from16 v53, v52

    .line 530
    .end local v41    # "bOptionalCI":Z
    .end local v52    # "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_a
    if-nez v53, :cond_b

    .line 531
    iget-object v4, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v53

    .end local v53    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    check-cast v53, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 535
    .end local v45    # "i$":Ljava/util/Iterator;
    .restart local v53    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_b
    if-eqz v53, :cond_c

    .line 536
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object v12, v5

    move-object v15, v7

    move-wide/from16 v16, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->playMediaServerItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v11 .. v19}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 539
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    sget-object v6, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V
    invoke-static/range {v4 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 544
    .end local v13    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v19    # "deviceName":Ljava/lang/String;
    .end local v42    # "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    .end local v44    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v48    # "objectID":Ljava/lang/String;
    .end local v53    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v54    # "sItemBundle":Landroid/os/Bundle;
    .end local v55    # "serverUdn":Ljava/lang/String;
    :cond_d
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_STOP"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "Stop"

    const-wide/16 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 546
    :cond_e
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_SEEK"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "Seek"

    move-wide/from16 v0, v50

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 548
    :cond_f
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PAUSE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 549
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "Pause"

    const-wide/16 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 550
    :cond_10
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_RESUME"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 551
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "Resume"

    const-wide/16 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 552
    :cond_11
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_VOLUME"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 553
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "SetVolume"

    move/from16 v0, v46

    move/from16 v1, v56

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V
    invoke-static {v4, v6, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 554
    :cond_12
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_MUTE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 555
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "SetMute"

    move/from16 v0, v46

    move/from16 v1, v56

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V
    invoke-static {v4, v6, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 556
    :cond_13
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_PLAY_POSITION"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 558
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "GetPlayPosition"

    const-wide/16 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 559
    :cond_14
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_VOLUME"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 560
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "GetVolume"

    move/from16 v0, v46

    move/from16 v1, v56

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V
    invoke-static {v4, v6, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 561
    :cond_15
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MUTE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 562
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "GetMute"

    move/from16 v0, v46

    move/from16 v1, v56

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V
    invoke-static {v4, v6, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 563
    :cond_16
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_URI"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 565
    if-eqz v5, :cond_0

    .line 566
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v4

    invoke-virtual {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 568
    const-string v4, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v30

    check-cast v30, Landroid/net/Uri;

    .line 571
    .local v30, "uri":Landroid/net/Uri;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/os/Bundle;

    .line 572
    .restart local v54    # "sItemBundle":Landroid/os/Bundle;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v54

    move-object/from16 v1, v30

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    invoke-static {v4, v14, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 574
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v54

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 577
    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v30

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v47

    .line 578
    .local v47, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v30

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v22, v5

    move-object/from16 v24, v14

    move-object/from16 v27, v7

    move-wide/from16 v28, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayLocalContent(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V
    invoke-static/range {v21 .. v30}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V

    goto/16 :goto_0

    .line 581
    .end local v30    # "uri":Landroid/net/Uri;
    .end local v47    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v54    # "sItemBundle":Landroid/os/Bundle;
    :cond_18
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 583
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v4

    invoke-virtual {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 585
    const-string v4, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 586
    .local v23, "filePath":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/os/Bundle;

    .line 587
    .restart local v54    # "sItemBundle":Landroid/os/Bundle;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const/4 v6, 0x1

    move-object/from16 v0, v54

    move-object/from16 v1, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v4, v14, v0, v1, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 589
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v54

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 592
    :cond_19
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-static {v4, v0, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v30

    .line 594
    .restart local v30    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v30

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v47

    .line 595
    .restart local v47    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v22, v5

    move-object/from16 v24, v14

    move-object/from16 v27, v7

    move-wide/from16 v28, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayLocalContent(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V
    invoke-static/range {v21 .. v30}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V

    goto/16 :goto_0

    .line 597
    .end local v23    # "filePath":Ljava/lang/String;
    .end local v30    # "uri":Landroid/net/Uri;
    .end local v47    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v54    # "sItemBundle":Landroid/os/Bundle;
    :cond_1a
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH_WITH_METADATA"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 599
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v4

    invoke-virtual {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 601
    const-string v4, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 602
    .restart local v23    # "filePath":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/os/Bundle;

    .line 603
    .restart local v54    # "sItemBundle":Landroid/os/Bundle;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const/4 v6, 0x1

    move-object/from16 v0, v54

    move-object/from16 v1, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v4, v14, v0, v1, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 605
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v0, v54

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 608
    :cond_1b
    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 610
    .local v36, "mimeType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    move-object/from16 v31, v0

    const/16 v40, 0x0

    move-object/from16 v32, v5

    move-object/from16 v33, v23

    move-object/from16 v34, v14

    move-object/from16 v37, v7

    move-wide/from16 v38, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayLocalContent(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V
    invoke-static/range {v31 .. v40}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V

    goto/16 :goto_0

    .line 612
    .end local v23    # "filePath":Ljava/lang/String;
    .end local v36    # "mimeType":Ljava/lang/String;
    .end local v54    # "sItemBundle":Landroid/os/Bundle;
    :cond_1c
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MEDIA_INFO"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 614
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "GetMediaInfo"

    const-wide/16 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v4, v6, v14, v10, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 615
    :cond_1d
    const-string v4, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_PLAYER_STATE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayerState(Ljava/lang/String;)V
    invoke-static {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 618
    :cond_1e
    const-string v4, "ACTION_AV_PLAYER_PREPARE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 619
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z
    invoke-static {v4, v14, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 620
    :cond_1f
    const-string v4, "ACTION_AV_PLAYER_SKIP_DYNAMIC_BUFFERING"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSkipDynamicBuffering(Ljava/lang/String;)V
    invoke-static {v4, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 625
    :cond_20
    const-string v4, "com.sec.android.allshare.action.ACTION_WHA_GET_DEVICE_STATUS_INFO"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "GetDeviceStatusInfo"

    const/4 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v6, v14, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 627
    :cond_21
    const-string v4, "com.sec.android.allshare.action.ACTION_WHA_CREATE_PARTY"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "CreateParty"

    const/4 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v6, v14, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 629
    :cond_22
    const-string v4, "com.sec.android.allshare.action.ACTION_WHA_JOIN_PARTY"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 630
    const-string v4, "BUNDLE_STRING_WHA_REQUEST_PARTY_ID"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    .line 632
    .local v49, "partyID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "JoinParty"

    move-object/from16 v0, v49

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v6, v14, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 633
    .end local v49    # "partyID":Ljava/lang/String;
    :cond_23
    const-string v4, "com.sec.android.allshare.action.ACTION_WHA_LEAVE_PARTY"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 634
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    const-string v6, "LeaveParty"

    const/4 v10, 0x0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v6, v14, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 637
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    iget-object v4, v4, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "AVPlayerActionWorker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " invalid action = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v6, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
