.class Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;
.super Ljava/lang/Object;
.source "DownloadManager.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem$IDownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->itemDownload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "itemDownload"

    const-string v2, "onError"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 390
    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "not_enough_memory"

    if-ne v0, v1, :cond_0

    .line 391
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$600(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$602(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z

    .line 393
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$3;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalFailedCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalFailedCount(I)V

    .line 404
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadedSize(J)V

    .line 406
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateProgress()V

    .line 408
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateSelectAll(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    .line 411
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->moveToNextItem()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->itemDownload()V

    .line 428
    :cond_1
    :goto_0
    return-void

    .line 414
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$4;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 425
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    goto :goto_0
.end method

.method public onFinish(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 341
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getCreatedFile()Ljava/io/File;

    move-result-object v0

    .line 342
    .local v0, "receivedFile":Ljava/io/File;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSuccessCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalSuccessCount(I)V

    .line 343
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getDownloadedSize()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setDownloadedSize(J)V

    .line 345
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    .line 346
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateProgress()V

    .line 347
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateSelectAll(Ljava/lang/String;)V

    .line 348
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    .line 350
    if-eqz v0, :cond_0

    .line 351
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "itemDownload"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!@#onFinish : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->scanItems:Ljava/util/Map;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x0

    new-instance v4, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;)V

    invoke-static {v1, v2, v3, v4}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 367
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->moveToNextItem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 368
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->itemDownload()V

    .line 384
    :goto_1
    return-void

    .line 364
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "itemDownload"

    const-string v3, "onFinish : invalid file "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 370
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mhandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 381
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    goto :goto_1
.end method

.method public onFinish(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;J)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .param p2, "downloadSize"    # J

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalSize(J)V

    .line 336
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->onFinish(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    .line 337
    return-void
.end method

.method public onProgress(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateProgress()V

    .line 330
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    .line 331
    return-void
.end method

.method public onStart(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "itemDownload"

    const-string v2, "onStart"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    .line 323
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateProgress()V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    .line 325
    return-void
.end method
