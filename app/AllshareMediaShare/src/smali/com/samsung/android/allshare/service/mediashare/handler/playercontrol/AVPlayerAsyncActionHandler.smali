.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "AVPlayerAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;,
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;
    }
.end annotation


# static fields
.field public static final AS_MRCP_INVALID_PARAM:I = 0x2328

.field public static final AS_MRCP_METADATA_CREATION_FAILED:I = 0x232c

.field public static final AS_MRCP_PLAYER_NOT_FOUND:I = 0x2329

.field private static final AVPLAYER_STATE_BUFFERING:I = 0x1

.field private static final AVPLAYER_STATE_PAUSED:I = 0x3

.field private static final AVPLAYER_STATE_PLAYING:I = 0x2

.field private static final AVPLAYER_STATE_STOPPED:I = 0x0

.field private static final AVPLAYER_STATE_UNKNOWN:I = 0x4

.field private static final AVPLAYER_STR_LOCALCONTENT:Ljava/lang/String; = "local content"

.field private static final AVPLAYER_STR_UPNP_ALBUMART:Ljava/lang/String; = "upnp:albumart"

.field public static final WHA_CREATE_PARTY:Ljava/lang/String; = "CreateParty"

.field public static final WHA_GET_DEVICE_STATUS_INFO:Ljava/lang/String; = "GetDeviceStatusInfo"

.field public static final WHA_JOIN_PARTY:Ljava/lang/String; = "JoinParty"

.field public static final WHA_LEAVE_PARTY:Ljava/lang/String; = "LeaveParty"

.field private static mPreparedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$10200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$10300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->notifyPlayerStatus(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V

    return-void
.end method

.method static synthetic access$3200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$3300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->validateReturnValue(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddRCSListener(I)V

    return-void
.end method

.method static synthetic access$5400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$6500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    return-void
.end method

.method static synthetic access$8100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$8300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->isSupportSetNextAVT(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$8600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$9000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$9100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$9700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$9800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method public static convertToAllShareEventString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "upnpPlayerState"    # Ljava/lang/String;

    .prologue
    .line 404
    const-string v0, "STOPPED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    .line 417
    :goto_0
    return-object v0

    .line 406
    :cond_0
    const-string v0, "TRANSITIONING"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    goto :goto_0

    .line 408
    :cond_1
    const-string v0, "PLAYING"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    goto :goto_0

    .line 410
    :cond_2
    const-string v0, "PAUSED_PLAYBACK"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 411
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    goto :goto_0

    .line 412
    :cond_3
    const-string v0, "NO_MEDIA_PRESENT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 413
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    goto :goto_0

    .line 414
    :cond_4
    const-string v0, "NOTHING"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 415
    const-string v0, ""

    goto :goto_0

    .line 417
    :cond_5
    const-string v0, ""

    goto :goto_0
.end method

.method public static getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "uriOrPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1575
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object p0, v2

    .line 1597
    .end local p0    # "uriOrPath":Ljava/lang/String;
    .local v1, "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-object p0

    .line 1578
    .end local v1    # "uri":Landroid/net/Uri;
    .restart local p0    # "uriOrPath":Ljava/lang/String;
    :cond_2
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1581
    const-string v3, "file://"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1582
    const-string v2, "file://"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1584
    :cond_3
    const-string v3, "content://"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1585
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1586
    .restart local v1    # "uri":Landroid/net/Uri;
    if-nez v1, :cond_4

    move-object p0, v2

    .line 1587
    goto :goto_0

    .line 1590
    :cond_4
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->parseUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/util/HashMap;

    move-result-object v0

    .line 1592
    .local v0, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_5

    move-object p0, v2

    .line 1593
    goto :goto_0

    .line 1595
    :cond_5
    const-string v2, "_data"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object p0, v2

    goto :goto_0

    .end local v0    # "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_6
    move-object p0, v2

    .line 1597
    goto :goto_0
.end method

.method private getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1631
    invoke-static {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1601
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 1603
    .local v2, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz p1, :cond_0

    .line 1604
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->convertUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v1

    .line 1607
    .local v1, "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    if-nez v1, :cond_1

    .line 1608
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v5, "getMetadata"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot convert uri to ItemNode for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1619
    .end local v1    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_0
    :goto_0
    return-object v2

    .line 1612
    .restart local v1    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_1
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 1613
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 1614
    .local v3, "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v4, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method private isSupportSetNextAVT(Ljava/lang/String;)Z
    .locals 1
    .param p1, "dmrUdn"    # Ljava/lang/String;

    .prologue
    .line 1635
    invoke-static {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->isSupportSetNextAVT(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private notifyPlayerStatus(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .param p2, "notifyState"    # Ljava/lang/String;
    .param p3, "notifyBundle"    # Landroid/os/Bundle;

    .prologue
    .line 421
    const/4 v0, 0x1

    .line 423
    .local v0, "bNeedNotify":Z
    if-eqz v0, :cond_0

    .line 424
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 426
    :cond_0
    return-void
.end method

.method private putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 1553
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1554
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "putResourceUriListToBundle"

    const-string v3, "(bundle == null || uri == null)"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    :goto_0
    return-void

    .line 1558
    :cond_1
    invoke-static {p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ResourceUriHelper;->getResourceUriListWithDeviceIP(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1560
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 1564
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1565
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "putResourceUriListToBundle"

    const-string v3, "(bundle == null || uri == null)"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    :goto_0
    return-void

    .line 1569
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1570
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1571
    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private requestAddAVSListener(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 1623
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 1624
    return-void
.end method

.method private requestAddRCSListener(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 1627
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPRCSControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)V

    .line 1628
    return-void
.end method

.method private validateReturnValue(II)Z
    .locals 1
    .param p1, "returnValue"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 1639
    if-nez p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 5
    .param p1, "req_id"    # J

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "cancelRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no operation for req_id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 112
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 113
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_STOP"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 114
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_SEEK"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 115
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PAUSE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 116
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_RESUME"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 117
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_VOLUME"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 118
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_MUTE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 119
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_VOLUME"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 120
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MUTE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 121
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 122
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MEDIA_INFO"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 123
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_PLAYER_STATE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 126
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 127
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 128
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH_WITH_METADATA"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 130
    const-string v0, "ACTION_AV_PLAYER_PREPARE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 131
    const-string v0, "ACTION_AV_PLAYER_SKIP_DYNAMIC_BUFFERING"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 134
    const-string v0, "com.sec.android.allshare.action.ACTION_WHA_GET_DEVICE_STATUS_INFO"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 135
    const-string v0, "com.sec.android.allshare.action.ACTION_WHA_CREATE_PARTY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 136
    const-string v0, "com.sec.android.allshare.action.ACTION_WHA_JOIN_PARTY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 137
    const-string v0, "com.sec.android.allshare.action.ACTION_WHA_LEAVE_PARTY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 160
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPRCSControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)Z

    .line 161
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z

    .line 162
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;)Z

    .line 163
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPErrorNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;)Z

    .line 164
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 147
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPRCSControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)Z

    .line 148
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z

    .line 149
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;)Z

    .line 150
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPErrorNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;)Z

    .line 151
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 315
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onAVTControlResponse"

    invoke-virtual {p4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 319
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 322
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 323
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 324
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 325
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 328
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 355
    sget v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceNotifyStatus;->DEVICE_REMOVED:I

    if-ne v0, p1, :cond_0

    .line 356
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_REMOVED: device name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", udn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->removeDeviceLocker(Ljava/lang/String;)V

    .line 365
    :goto_0
    return-void

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UNKNOW state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", device name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", udn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onErrorNotify(ILjava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onErrorNotify"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] requestID["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] errorCode["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] errorDescription["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 377
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 381
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 382
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 383
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 386
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method

.method public onMRCPEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 393
    .local p2, "arguments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 400
    :cond_0
    return-void

    .line 396
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;

    .line 397
    .local v6, "action":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    sget-object v8, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    iget-object v4, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionName:Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionValue:Ljava/lang/String;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$EventNotifier;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onRCSControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;

    .prologue
    .line 335
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onRCSControlResponse"

    invoke-virtual {p4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 339
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 341
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 342
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 343
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 344
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 345
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 348
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method

.method public onWHAControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;

    .prologue
    .line 1645
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onWHAControlResponse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->actionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->actionStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->deviceStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->dmrUDN:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->errorCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->friendlyName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->requestID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->volume:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 1652
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 1653
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1654
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 1655
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 1656
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 1657
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 1658
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 1664
    :cond_0
    :goto_0
    return-void

    .line 1660
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 1661
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method
