.class public Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.source "DeviceFinderDeviceSyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DeviceFinderDeviceSyncActionHandler"


# instance fields
.field private deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

.field private mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 41
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 45
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 46
    return-void
.end method

.method private formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p3, "changeType"    # Ljava/lang/String;

    .prologue
    .line 303
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 304
    .local v1, "eventBundle":Landroid/os/Bundle;
    invoke-static {p2, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 305
    .local v0, "changedDevice":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.GLOBAL"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v2, "BUNDLE_STRING_TYPE"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v2, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 310
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getSystemEventMonitor()Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    move-result-object v2

    if-nez v2, :cond_0

    .line 311
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :goto_0
    return-object v1

    .line 312
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getSystemEventMonitor()Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->isNetworkAvailable(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 314
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_1
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "boundInterfaceName"    # Ljava/lang/String;

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceByID(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 160
    .local v2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    if-nez v2, :cond_0

    .line 161
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    :goto_0
    return-object v0

    .line 163
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 165
    .local v1, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    iput-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->ID:Ljava/lang/String;

    .line 166
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->ID:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 167
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 169
    .local v0, "bundleDevice":Landroid/os/Bundle;
    goto :goto_0

    .line 172
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v1    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method private getDeviceByIPAddress(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 183
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 184
    .local v2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    if-nez v2, :cond_0

    .line 185
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 194
    :goto_0
    return-object v0

    .line 187
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 188
    .local v1, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->IPAddr:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 189
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 191
    .local v0, "bundleDevice":Landroid/os/Bundle;
    goto :goto_0

    .line 194
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v1    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method private getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "deviceType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 338
    .local v0, "deviceTypeID":I
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method private getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;
    .param p3, "deviceIface"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 206
    .local v3, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    if-nez v3, :cond_1

    .line 207
    const/4 v1, 0x0

    .line 229
    :cond_0
    return-object v1

    .line 210
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v1, "deviceBundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    if-eqz p3, :cond_7

    .line 212
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 214
    .local v2, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-boolean v5, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isLocalDevice:Z

    if-nez v5, :cond_5

    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-boolean v5, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isLocalDevice:Z

    if-eqz v5, :cond_5

    :cond_4
    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 219
    :cond_5
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_6

    iget-object v5, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->boundInterfaceName:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 221
    :cond_6
    invoke-static {p2}, Lcom/samsung/android/allshare/Device$DeviceType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/Device$DeviceType;)Landroid/os/Bundle;

    move-result-object v0

    .line 223
    .local v0, "bundleDevice":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 211
    .end local v0    # "bundleDevice":Landroid/os/Bundle;
    .end local v2    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_7
    const-string p3, ""

    goto :goto_0

    .line 224
    .restart local v2    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_8
    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 225
    const-string v5, "DeviceFinderDeviceSyncActionHandler"

    const-string v6, "[getDeviceList]: "

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The domain is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private getDeviceTypeID(Ljava/lang/String;)I
    .locals 2
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    const/16 v0, 0xff

    .line 347
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348
    const/4 v0, 0x2

    .line 362
    :cond_0
    :goto_0
    return v0

    .line 349
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 350
    const/4 v0, 0x1

    goto :goto_0

    .line 351
    :cond_2
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 352
    const/4 v0, 0x4

    goto :goto_0

    .line 353
    :cond_3
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 354
    const/4 v0, 0x5

    goto :goto_0

    .line 355
    :cond_4
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 356
    const/4 v0, 0x7

    goto :goto_0

    .line 357
    :cond_5
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 358
    const/16 v0, 0x8

    goto :goto_0

    .line 359
    :cond_6
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device$DeviceType;->enumToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1
    .param p1, "deviceTypeID"    # I

    .prologue
    .line 371
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 372
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 384
    :goto_0
    return-object v0

    .line 373
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 374
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 375
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 376
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 377
    :cond_3
    const/4 v0, 0x5

    if-ne p1, v0, :cond_4

    .line 378
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 379
    :cond_4
    const/4 v0, 0x7

    if-ne p1, v0, :cond_5

    .line 380
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 381
    :cond_5
    const/16 v0, 0x8

    if-ne p1, v0, :cond_6

    .line 382
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 384
    :cond_6
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0
.end method

.method private getEventStrByEventType(IZ)Ljava/lang/String;
    .locals 1
    .param p1, "deviceType"    # I
    .param p2, "isAdded"    # Z

    .prologue
    .line 388
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 389
    const-string v0, "com.sec.android.allshare.event.EVENT_PROVIDER_DISCOVERY"

    .line 401
    :goto_0
    return-object v0

    .line 390
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 391
    const-string v0, "com.sec.android.allshare.event.EVENT_IMAGE_VIEWER_DISCOVERY"

    goto :goto_0

    .line 392
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 393
    const-string v0, "com.sec.android.allshare.event.EVENT_AV_PLAYER_DISCOVERY"

    goto :goto_0

    .line 394
    :cond_2
    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    .line 395
    const-string v0, "com.sec.android.allshare.event.EVENT_SMARTCONTROL_DISCOVERY"

    goto :goto_0

    .line 396
    :cond_3
    const/4 v0, 0x7

    if-ne p1, v0, :cond_4

    .line 397
    const-string v0, "com.sec.android.allshare.event.EVENT_FILERECEIVER_DISCOVERY"

    goto :goto_0

    .line 398
    :cond_4
    const/16 v0, 0x8

    if-ne p1, v0, :cond_5

    .line 399
    const-string v0, "com.sec.android.allshare.event.EVENT_KIES_SYNC_SERVER_DISCOVERY"

    goto :goto_0

    .line 401
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 329
    return-void
.end method

.method private refreshByAppID(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 2
    .param p1, "appID"    # Ljava/lang/String;

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "result":I
    if-eqz p1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->rescanDeviceByAppID(Ljava/lang/String;)I

    move-result v0

    .line 241
    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    :goto_1
    return-object v1

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->rescanDevice()I

    move-result v0

    goto :goto_0

    .line 241
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_1
.end method

.method private refreshByType(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 3
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 248
    .local v0, "deviceTypeID":I
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->rescanDeviceByType(I)I

    move-result v1

    .line 249
    .local v1, "result":I
    if-nez v1, :cond_0

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method private registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/allshare/ERROR;"
        }
    .end annotation

    .prologue
    .line 408
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 409
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 418
    :goto_0
    return-object v5

    .line 410
    :cond_0
    if-nez p2, :cond_1

    .line 411
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 412
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 413
    .local v3, "searchTargetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 414
    .local v4, "target":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 415
    .local v0, "deviceType":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 417
    .end local v0    # "deviceType":I
    .end local v4    # "target":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v5, p1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    .line 418
    .local v2, "result":I
    if-nez v2, :cond_3

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method private unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/allshare/ERROR;"
        }
    .end annotation

    .prologue
    .line 421
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 422
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 431
    :goto_0
    return-object v5

    .line 423
    :cond_0
    if-nez p2, :cond_1

    .line 424
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 425
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 426
    .local v3, "searchTargetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 427
    .local v4, "target":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeID(Ljava/lang/String;)I

    move-result v0

    .line 428
    .local v0, "deviceType":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 430
    .end local v0    # "deviceType":I
    .end local v4    # "target":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v5, p1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    .line 431
    .local v2, "result":I
    if-nez v2, :cond_3

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    :cond_3
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 53
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH_TARGET"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 54
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_ID_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 55
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 56
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 57
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 58
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_SOURCE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 59
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 60
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_UNREGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->removeDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z

    .line 77
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->deviceListInstance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->addDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z

    .line 69
    return-void
.end method

.method public onDeviceAdded(ILcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 6
    .param p1, "deviceType"    # I
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 259
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    .line 260
    .local v0, "deviceTypeStr":Lcom/samsung/android/allshare/Device$DeviceType;
    const-string v3, "DeviceFinderDeviceSyncActionHandler"

    const-string v4, "onDeviceAdded"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    sget-object v3, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-eq v0, v3, :cond_0

    .line 264
    const-string v3, "ADDED"

    invoke-direct {p0, v0, p2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 265
    .local v1, "eventBundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getEventStrByEventType(IZ)Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, "eventType":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 268
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 271
    .end local v1    # "eventBundle":Landroid/os/Bundle;
    .end local v2    # "eventType":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onDeviceRemoved(ILcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 6
    .param p1, "deviceType"    # I
    .param p2, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceTypeStr(I)Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    .line 281
    .local v0, "deviceTypeStr":Lcom/samsung/android/allshare/Device$DeviceType;
    const-string v3, "DeviceFinderDeviceSyncActionHandler"

    const-string v4, "onDeviceRemoved"

    invoke-virtual {p2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    sget-object v3, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-eq v0, v3, :cond_0

    .line 286
    const-string v3, "REMOVED"

    invoke-direct {p0, v0, p2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->formEventBundle(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 287
    .local v1, "eventBundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getEventStrByEventType(IZ)Ljava/lang/String;

    move-result-object v2

    .line 288
    .local v2, "eventType":Ljava/lang/String;
    invoke-static {p2}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->removeDeviceBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    .line 289
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 290
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->notifyToServiceManager(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 293
    .end local v1    # "eventBundle":Landroid/os/Bundle;
    .end local v2    # "eventType":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 15
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "action":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v11

    .line 89
    .local v11, "reqb":Landroid/os/Bundle;
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 90
    .local v10, "name":Ljava/lang/String;
    const-string v13, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 91
    .local v7, "deviceType":Ljava/lang/String;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 92
    .local v3, "b":Landroid/os/Bundle;
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v3, v13, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 94
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 95
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "appId":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->refreshByAppID(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_1

    .line 97
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 148
    .end local v2    # "appId":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v13

    return-object v13

    .line 99
    .restart local v2    # "appId":Ljava/lang/String;
    :cond_1
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 100
    .end local v2    # "appId":Ljava/lang/String;
    :cond_2
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH_TARGET"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 101
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->refreshByType(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_3

    .line 103
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 105
    :cond_3
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 106
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v13

    const-string v14, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 107
    const-string v13, "0"

    const/4 v14, 0x0

    invoke-direct {p0, v13, v7, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 108
    .local v6, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 109
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_5
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_ID_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 111
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "ID":Ljava/lang/String;
    invoke-direct {p0, v0, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceByID(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 113
    .local v5, "device":Landroid/os/Bundle;
    const-string v13, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v3, v13, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 114
    .end local v0    # "ID":Ljava/lang/String;
    .end local v5    # "device":Landroid/os/Bundle;
    :cond_6
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 116
    const-string v13, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 117
    .local v4, "boundInterface":Ljava/lang/String;
    const-string v13, "0"

    invoke-direct {p0, v13, v7, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 118
    .restart local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 119
    .end local v4    # "boundInterface":Ljava/lang/String;
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_7
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_SOURCE_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 121
    const-string v13, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "ipAddress":Ljava/lang/String;
    invoke-direct {p0, v9, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceByIPAddress(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 123
    .restart local v5    # "device":Landroid/os/Bundle;
    const-string v13, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v3, v13, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_0

    .line 124
    .end local v5    # "device":Landroid/os/Bundle;
    .end local v9    # "ipAddress":Ljava/lang/String;
    :cond_8
    const-string v13, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 126
    const-string v13, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 127
    .local v8, "domain":Ljava/lang/String;
    const/4 v13, 0x0

    invoke-direct {p0, v8, v7, v13}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->getDeviceList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 128
    .restart local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v3, v13, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 129
    .end local v6    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v8    # "domain":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v13

    const-string v14, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 131
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .restart local v2    # "appId":Ljava/lang/String;
    const-string v13, "BUNDLE_STRINGARRAYLIST_DEVICE_TYPE_LIST"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 134
    .local v12, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_a

    .line 135
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 137
    :cond_a
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 138
    .end local v2    # "appId":Ljava/lang/String;
    .end local v12    # "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v13

    const-string v14, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_UNREGISTER_SEARCH_TARGET_SYNC"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 140
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    .restart local v2    # "appId":Ljava/lang/String;
    const-string v13, "BUNDLE_STRINGARRAYLIST_DEVICE_TYPE_LIST"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 143
    .restart local v12    # "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/devicefinder/DeviceFinderDeviceSyncActionHandler;->unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq v13, v14, :cond_c

    .line 144
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 146
    :cond_c
    const-string v13, "BUNDLE_BOOLEAN_RESULT"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
