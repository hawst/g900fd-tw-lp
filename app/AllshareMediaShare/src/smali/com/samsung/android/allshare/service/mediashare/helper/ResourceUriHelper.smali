.class public Lcom/samsung/android/allshare/service/mediashare/helper/ResourceUriHelper;
.super Ljava/lang/Object;
.source "ResourceUriHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResourceUriListWithDeviceIP(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 78
    if-nez p0, :cond_0

    .line 94
    :goto_0
    return-object v5

    .line 81
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v4, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    iget-object v2, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 84
    .local v2, "thumbnailResUri":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v6, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 87
    .local v1, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    if-eqz v1, :cond_1

    .line 88
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    .line 89
    .local v3, "uri":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 90
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    .end local v1    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v3    # "uri":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_3

    move-object v4, v5

    .end local v4    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    move-object v5, v4

    goto :goto_0
.end method

.method public static getResourceUriListWithoutDeviceIP(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 32
    if-nez p0, :cond_0

    .line 47
    :goto_0
    return-object v4

    .line 35
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 37
    .local v3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 38
    .local v1, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    if-eqz v1, :cond_1

    .line 39
    iget-object v2, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    .line 40
    .local v2, "uri":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ResourceUriHelper;->getResourceUriWithoutDeviceIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 43
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 47
    .end local v1    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v2    # "uri":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_3

    move-object v3, v4

    .end local v3    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    move-object v4, v3

    goto :goto_0
.end method

.method private static getResourceUriWithoutDeviceIP(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "fullUri"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 51
    if-nez p0, :cond_0

    .line 74
    :goto_0
    return-object v4

    .line 54
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .local v2, "resultUriBuider":Ljava/lang/StringBuilder;
    :try_start_0
    const-string v5, ":"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "sp":[Ljava/lang/String;
    if-eqz v3, :cond_2

    array-length v5, v3

    const/4 v6, 0x3

    if-lt v5, v6, :cond_2

    .line 61
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_1
    array-length v5, v3

    if-ge v1, v5, :cond_1

    .line 62
    aget-object v5, v3, v1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v5, ":"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 67
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 68
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 70
    .end local v3    # "sp":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static unEscapeUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "escapedUri"    # Ljava/lang/String;

    .prologue
    .line 98
    move-object v0, p0

    .line 101
    .local v0, "result":Ljava/lang/String;
    const-string v2, ".*[&]+(lt|gt|apos|nbsp|quot|amp)+[;]+.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    .line 117
    .end local v0    # "result":Ljava/lang/String;
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 105
    .end local v1    # "result":Ljava/lang/String;
    .restart local v0    # "result":Ljava/lang/String;
    :cond_0
    const-string v2, "&lt;"

    const-string v3, "<"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v2, "&gt;"

    const-string v3, ">"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string v2, "&apos;"

    const-string v3, "\'"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    const-string v2, "&nbsp;"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    const-string v2, "&quot;"

    const-string v3, "\""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string v2, "&amp;"

    const-string v3, "&"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 117
    .end local v0    # "result":Ljava/lang/String;
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0
.end method
