.class Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;
.super Ljava/lang/Object;
.source "ServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->finiActionHandlers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0

    .prologue
    .line 855
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 861
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 865
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;"
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$400(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 866
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$400(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 867
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;>;"
    if-eqz v2, :cond_1

    .line 868
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 869
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;

    .line 870
    .local v0, "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 871
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "finiActionHandlers"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found AsyncActionHandler : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 880
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;>;"
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 881
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 882
    .restart local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;>;"
    if-eqz v2, :cond_3

    .line 883
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 884
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;

    .line 885
    .restart local v0    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 886
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "finiActionHandlers"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found SyncActionHandler : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 894
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;>;"
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 895
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;>;"
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 896
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;

    .line 897
    .restart local v0    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "finiActionHandlers"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Finalize Action Handler : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->getHandlerID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    invoke-interface {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;->finiActionHandler()V

    goto :goto_2

    .line 902
    .end local v0    # "handler":Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$400(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 903
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$400(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 904
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # setter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mAsyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4, v8}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$402(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 907
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 908
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 909
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    # setter for: Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->mSyncActionHandlerMap:Ljava/util/HashMap;
    invoke-static {v4, v8}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->access$502(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 911
    :cond_6
    return-void
.end method
