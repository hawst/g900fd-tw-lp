.class public final enum Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
.super Ljava/lang/Enum;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotiType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

.field public static final enum COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

.field public static final enum NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

.field public static final enum PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    const-string v1, "PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 88
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->$VALUES:[Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    const-class v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->$VALUES:[Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    return-object v0
.end method
