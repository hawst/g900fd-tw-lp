.class public Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "ProviderAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$1;,
        Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;,
        Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;
    }
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "upnp:album"

.field public static final ARTIST:Ljava/lang/String; = "upnp:artist"

.field public static final BROWSE:Ljava/lang/String; = "Browse"

.field public static final OBJECT_CONTAINER:Ljava/lang/String; = "object.container"

.field public static final OBJECT_ITEM_AUDIOITEM:Ljava/lang/String; = "object.item.audioItem"

.field public static final OBJECT_ITEM_IMAGEITEM:Ljava/lang/String; = "object.item.imageItem"

.field public static final OBJECT_ITEM_VIDEOITEM:Ljava/lang/String; = "object.item.videoItem"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SEARCHCAPA:Ljava/lang/String; = "GetSearchCapabilities"

.field public static final TITLE:Ljava/lang/String; = "dc:title"

.field private static deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

.field private static mSearchCriteriaMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;


# instance fields
.field private requestArgumentMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;

    .line 72
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    .line 77
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;

    .line 79
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mSearchCriteriaMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method static synthetic access$500()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 136
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 86
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 87
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_ITEMS"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 88
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCHCRITERIA_ITEMS"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 89
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_ADD_EVENT_LISTENER"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 90
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_SET_BROWSE_ITEMS_RESPONSE_LISTENER"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 91
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_SET_SEARCH_ITEMS_RESPONSE_LISTENER"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z

    .line 115
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;)Z

    .line 116
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPGetInfoListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;)Z

    .line 117
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPErrorResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;)Z

    .line 118
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;)Z

    .line 119
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z

    .line 101
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPEventNotifyListener;)Z

    .line 102
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetInfoListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetInfoListener;)Z

    .line 103
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPErrorResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPErrorResponseListener;)Z

    .line 104
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPDeviceNotifyListener;)Z

    .line 105
    return-void
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 321
    sget v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceNotifyStatus;->DEVICE_ADDED:I

    if-ne v0, p1, :cond_0

    .line 322
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->subscribeDMS(Ljava/lang/String;)I

    .line 323
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeDMS : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") - DEVICE_ADDED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_0
    return-void
.end method

.method public onErrorResponse(IILjava/lang/String;)V
    .locals 7
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 253
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "[onErrorResponse]: "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " received,error code is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " descriptopn is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 256
    .local v1, "error":Lcom/samsung/android/allshare/ERROR;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 257
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;

    .line 258
    .local v2, "requestArgument":Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;
    if-nez v2, :cond_0

    .line 259
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "[onErrorResponse]: "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " There is no requestID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 262
    :cond_0
    iget-object v3, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->actionHandle:Ljava/lang/String;

    if-eqz v3, :cond_3

    const-string v3, "Browse"

    iget-object v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->actionHandle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Search"

    iget-object v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->actionHandle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 265
    :cond_1
    const-string v3, "BUNDLE_BOOLEAN_ENDOFITEM"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 266
    const-string v3, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v3, "BUNDLE_INT_STARTINDEX"

    iget v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->startIndex:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 268
    const-string v3, "BUNDLE_INT_REQUESTCOUNT"

    iget v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->requestCount:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 269
    const-string v3, "BUNDLE_STRING_SEARCHSTRING"

    iget-object v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->searchCriteria:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v3, "BUNDLE_PARCELABLE_CONTENT_BUNDLE_ARRAYLIST"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 272
    const-string v3, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    iget-object v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->itemTypeList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 275
    iget-object v3, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->folderItem:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    .line 276
    const-string v3, "BUNDLE_PARCELABLE_FOLDERITEM"

    iget-object v4, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->folderItem:Landroid/os/Bundle;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280
    :cond_2
    iget-object v3, v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->providerWorker:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->responseAction(Landroid/os/Bundle;)V

    .line 282
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p2, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 289
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez p2, :cond_1

    .line 290
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "onEventNotify"

    const-string v5, "get response filed!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    return-void

    .line 292
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;

    .line 294
    .local v0, "action":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    const-string v3, "SystemUpdateID"

    iget-object v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 295
    if-nez p1, :cond_3

    .line 296
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "onEventNotify"

    const-string v5, "deviceUDN is null "

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v3, "BUNDLE_STRING_ID"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :goto_1
    const-string v3, "BUNDLE_STRING_CATEGORY"

    const-string v4, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v3, "BUNDLE_ENUM_ERROR"

    sget-object v4, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v3, :cond_2

    .line 306
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v4, "com.sec.android.allshare.event.EVENT_PROVIDER_CONTENTS_UPDATED"

    invoke-virtual {v3, v4, v1}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 299
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v4, "onEventNotify"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deviceUDN is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onGetDMSInfoResponse(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    .line 317
    return-void
.end method

.method public onGetObjectListResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;)V
    .locals 19
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;

    .prologue
    .line 173
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;

    .line 174
    .local v13, "requestArgument":Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;
    if-nez v13, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "onGetObjectListResponse"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, " There is no requestID = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :goto_0
    return-void

    .line 181
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 183
    .local v3, "bundle":Landroid/os/Bundle;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v11, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v14, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 186
    .local v8, "hasImage":Z
    const/4 v6, 0x0

    .line 187
    .local v6, "hasAudio":Z
    const/4 v9, 0x0

    .line 188
    .local v9, "hasVideo":Z
    const/4 v7, 0x0

    .line 190
    .local v7, "hasFolder":Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p4

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->objects:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v10, v15, :cond_7

    .line 191
    move-object/from16 v0, p4

    iget-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->objects:Ljava/util/ArrayList;

    invoke-virtual {v15, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 192
    .local v12, "object":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    const-string v15, "Search"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    iget v15, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/16 v16, 0x4

    move/from16 v0, v16

    if-eq v15, v0, :cond_1

    iget v15, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    const/16 v16, 0x5

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 193
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "onGetObjectListResponse"

    const-string v17, "There is UNKNOWN or NOTSUPPORTED object!"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 198
    :cond_2
    iget v15, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    if-nez v15, :cond_4

    if-nez v8, :cond_4

    .line 199
    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    const/4 v8, 0x1

    .line 214
    :cond_3
    :goto_3
    invoke-static {v12}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;

    move-result-object v2

    .line 215
    .local v2, "b":Landroid/os/Bundle;
    const-string v15, "BUNDLE_STRING_ID"

    iget-object v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->dmsUDN:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 201
    .end local v2    # "b":Landroid/os/Bundle;
    :cond_4
    const/4 v15, 0x1

    iget v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    if-nez v6, :cond_5

    .line 202
    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const/4 v6, 0x1

    goto :goto_3

    .line 204
    :cond_5
    const/4 v15, 0x2

    iget v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    if-nez v9, :cond_6

    .line 205
    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const/4 v9, 0x1

    goto :goto_3

    .line 207
    :cond_6
    const/4 v15, 0x3

    iget v0, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    if-nez v7, :cond_3

    .line 208
    sget-object v15, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v15}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    const/4 v7, 0x1

    goto :goto_3

    .line 219
    .end local v12    # "object":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_7
    if-nez p3, :cond_9

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 223
    .local v5, "error":Lcom/samsung/android/allshare/ERROR;
    :goto_4
    move-object/from16 v0, p4

    iget v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->returnedCount:I

    if-eqz v15, :cond_b

    .line 224
    move-object/from16 v0, p4

    iget v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->returnedCount:I

    iget v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->startIndex:I

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p4

    iget v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->matchedCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-lt v15, v0, :cond_a

    const/4 v4, 0x1

    .line 233
    .local v4, "endOfItem":Z
    :goto_5
    const-string v15, "BUNDLE_INT_STARTINDEX"

    iget v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->startIndex:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 234
    const-string v15, "BUNDLE_INT_REQUESTCOUNT"

    iget v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->requestCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    const-string v15, "BUNDLE_STRING_SEARCHSTRING"

    iget-object v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->searchCriteria:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v15, "BUNDLE_BOOLEAN_ENDOFITEM"

    invoke-virtual {v3, v15, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 237
    const-string v15, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v15, "BUNDLE_PARCELABLE_CONTENT_BUNDLE_ARRAYLIST"

    invoke-virtual {v3, v15, v11}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 240
    const-string v15, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    invoke-virtual {v3, v15, v14}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 242
    iget-object v15, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->folderItem:Landroid/os/Bundle;

    if-eqz v15, :cond_8

    .line 243
    const-string v15, "BUNDLE_PARCELABLE_FOLDERITEM"

    iget-object v0, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->folderItem:Landroid/os/Bundle;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 247
    :cond_8
    iget-object v15, v13, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->providerWorker:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    invoke-virtual {v15, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;->responseAction(Landroid/os/Bundle;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;->requestArgumentMap:Ljava/util/Map;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 219
    .end local v4    # "endOfItem":Z
    .end local v5    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_9
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_4

    .line 224
    .restart local v5    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_a
    const/4 v4, 0x0

    goto :goto_5

    .line 226
    :cond_b
    const/4 v4, 0x1

    .restart local v4    # "endOfItem":Z
    goto :goto_5
.end method
