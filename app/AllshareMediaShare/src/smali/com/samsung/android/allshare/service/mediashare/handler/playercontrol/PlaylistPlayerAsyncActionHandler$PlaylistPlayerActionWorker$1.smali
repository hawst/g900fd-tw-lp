.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;
.super Ljava/lang/Object;
.source "PlaylistPlayerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 177
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 178
    .local v0, "bundle":Landroid/os/Bundle;
    const-class v7, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 179
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "udn":Ljava/lang/String;
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 182
    .local v4, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v7, "BUNDLE_INT_PLAYLIST_TRACKNUMBER"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 183
    .local v5, "trackNumber":I
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v1

    .line 185
    .local v1, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v1, :cond_1

    .line 186
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Ljava/lang/String;)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PLAY"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 191
    const-string v7, "BUNDLE_STRING_MIME_TYPE"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "mimeType":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playPlaylistPlayer(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    invoke-static {v7, v4, v2, v1, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V

    goto :goto_0

    .line 193
    .end local v2    # "mimeType":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SEEK"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 195
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->seekPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    invoke-static {v7, v1, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V

    goto :goto_0

    .line 197
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_NEXT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 199
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playNextPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0

    .line 201
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PERVIOUS"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 204
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->playPerviousPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0

    .line 206
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PAUSE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 209
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->pausePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0

    .line 211
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_RESUME"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 214
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->resumePlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0

    .line 216
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_STOP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 218
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->stopPlaylistPlayer(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto/16 :goto_0

    .line 219
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 221
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->getPlaylistPosition(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v7, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto/16 :goto_0

    .line 222
    :cond_9
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_AUTO_FLIP_MODE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 224
    const-string v7, "BUNDLE_BOOLEAN_AUTO_SLIDE_SHOW"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 226
    .local v3, "onoff":Z
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->setAutoFlipMode(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    invoke-static {v7, v1, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V

    goto/16 :goto_0

    .line 227
    .end local v3    # "onoff":Z
    :cond_a
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v7}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_QUICK_NAVIGATE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 229
    const-string v7, "BUNDLE_BOOLEAN_NAVIGATE_IN_PAUSE"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 231
    .restart local v3    # "onoff":Z
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->setQuickNavigate(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V
    invoke-static {v7, v1, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Z)V

    goto/16 :goto_0
.end method
