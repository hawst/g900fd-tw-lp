.class public Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
.super Ljava/lang/Object;
.source "TransferHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "TransferHandler"

.field private static mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;


# instance fields
.field private continue_get_transfer_progress:Z

.field private device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

.field private get_transfer_progress_request_id:I

.field private isSupportPostObject:Z

.field private mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

.field private service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field private transfer_id:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->isSupportPostObject:Z

    .line 46
    if-nez p1, :cond_0

    .line 47
    const-string v0, "TransferHandler"

    const-string v1, "TransferHandler"

    const-string v2, " the serviceManager is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->continue_get_transfer_progress:Z

    .line 52
    if-nez p2, :cond_1

    .line 53
    const-string v0, "TransferHandler"

    const-string v1, "TransferHandler"

    const-string v2, " the contentItem is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 57
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetTransferProgressListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;)Z

    .line 58
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;)Z

    .line 59
    return-void
.end method

.method private getOnceTransferProgress()V
    .locals 6

    .prologue
    .line 127
    const-string v2, "TransferHandler"

    const-string v3, "getOnceTransferProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dms udn: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    iget-object v5, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " transfer id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    new-instance v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 131
    .local v1, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    iget-object v3, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getTransferProgress(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 132
    .local v0, "error":I
    if-eqz v0, :cond_0

    .line 133
    const-string v2, "TransferHandler"

    const-string v3, "getOnceTransferProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTransferProgress filed! error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 138
    :cond_0
    iget v2, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    .line 139
    const-string v2, "TransferHandler"

    const-string v3, "getOnceTransferProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTransferProgress action has been sent! request id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyTransferProgress(JJZ)V
    .locals 5
    .param p1, "trans_size"    # J
    .param p3, "total_size"    # J
    .param p5, "is_completed"    # Z

    .prologue
    .line 145
    const-string v1, "TransferHandler"

    const-string v2, "notifyTransferProgress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transfered size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is completed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 149
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 150
    const-string v1, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 151
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 153
    const-string v1, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 155
    const-string v1, "BUNDLE_STRING_CATEGORY"

    const-string v2, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v1, "BUNDLE_STRING_ID"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    iget-object v2, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    if-nez p5, :cond_0

    .line 160
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v2, "com.sec.android.allshare.event.EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 167
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v2, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public discard()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPGetTransferProgressListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransferProgressListener;)Z

    .line 63
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPResponseListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPResponseListener;)Z

    .line 64
    return-void
.end method

.method public getTransferProgress(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "transfer_id"    # Ljava/lang/String;
    .param p3, "continuous"    # Z

    .prologue
    .line 104
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-nez v1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    if-eqz p1, :cond_0

    .line 110
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 114
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    .line 115
    iput-boolean p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->continue_get_transfer_progress:Z

    .line 117
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 118
    .local v0, "get_transfer_progress_thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public onCommonResponse(II)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 171
    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    if-eq p1, v1, :cond_1

    .line 172
    const-string v1, "TransferHandler"

    const-string v2, "onCommonResponse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "this response[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] is not expected response ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    if-nez p2, :cond_0

    .line 177
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 178
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 179
    const-string v1, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 180
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 182
    const-string v1, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 183
    const-string v1, "BUNDLE_STRING_CATEGORY"

    const-string v2, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v1, "BUNDLE_STRING_ID"

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    iget-object v2, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->service_mananger:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v2, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onGetTransferProgressResponse(IILjava/lang/String;II)V
    .locals 7
    .param p1, "requestID"    # I
    .param p2, "errorCode"    # I
    .param p3, "status"    # Ljava/lang/String;
    .param p4, "transferLength"    # I
    .param p5, "totalLength"    # I

    .prologue
    .line 68
    const-string v0, "TransferHandler"

    const-string v1, "onGetTransferProgressResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    if-eq p1, v0, :cond_1

    .line 72
    const-string v0, "TransferHandler"

    const-string v1, "onGetTransferProgressResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " this response[ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ] is not expected response ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->get_transfer_progress_request_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    const-string v0, "COMPLETED"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 78
    .local v6, "is_completed":Z
    int-to-long v2, p4

    int-to-long v4, p5

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->notifyTransferProgress(JJZ)V

    .line 80
    if-nez v6, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->continue_get_transfer_progress:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->getOnceTransferProgress()V

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->getOnceTransferProgress()V

    .line 124
    return-void
.end method

.method public setSupportPostObject(Z)V
    .locals 0
    .param p1, "isSupportPostObject"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->isSupportPostObject:Z

    .line 39
    return-void
.end method

.method public setTransfer_id(Ljava/lang/String;)V
    .locals 0
    .param p1, "transfer_id"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public stopTransfer()Z
    .locals 6

    .prologue
    .line 86
    new-instance v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 88
    .local v1, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    iget-boolean v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->isSupportPostObject:Z

    if-eqz v2, :cond_0

    .line 89
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->postObjectStopTransfer(I)I

    move-result v0

    .line 93
    .local v0, "error":I
    :goto_0
    if-eqz v0, :cond_1

    .line 94
    const-string v2, "TransferHandler"

    const-string v3, "stopTransfer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopTransResource filed! error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v2, 0x0

    .line 100
    :goto_1
    return v2

    .line 91
    .end local v0    # "error":I
    :cond_0
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->device:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    iget-object v3, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->transfer_id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->stopTransResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .restart local v0    # "error":I
    goto :goto_0

    .line 98
    :cond_1
    const-string v2, "TransferHandler"

    const-string v3, "stopTransfer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopTransResource action has been sent! request id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v2, 0x1

    goto :goto_1
.end method
