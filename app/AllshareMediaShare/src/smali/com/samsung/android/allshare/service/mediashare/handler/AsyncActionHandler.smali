.class public abstract Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.super Ljava/lang/Object;
.source "AsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/handler/IAsyncActionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    }
.end annotation


# static fields
.field protected static final AS_ACTION_TIMEOUT:I = 0x321

.field protected static final AS_PERMISSION_NOT_ALLOWED:I = 0x25e

.field protected static final AS_SUCCESS:I = 0x0

.field protected static final DEFAULT_WAIT_TIME:J = 0x1f4L

.field public static final GETINFO:Ljava/lang/String; = "GetMediaInfo"

.field public static final GETMUTE:Ljava/lang/String; = "GetMute"

.field public static final GETPOSITION:Ljava/lang/String; = "GetPlayPosition"

.field public static final GETTRANSPORTINFO:Ljava/lang/String; = "GetTransportInfo"

.field public static final GETVOL:Ljava/lang/String; = "GetVolume"

.field public static final GETZOOMPORT:Ljava/lang/String; = "X_GetZoomPort"

.field public static final NEXT:Ljava/lang/String; = "Next"

.field public static final PAUSE:Ljava/lang/String; = "Pause"

.field public static final PLAY:Ljava/lang/String; = "Play"

.field public static final PREVIOUS:Ljava/lang/String; = "Previous"

.field public static final RELTIME:Ljava/lang/String; = "REL_TIME"

.field public static final RESUME:Ljava/lang/String; = "Resume"

.field public static final SEEK:Ljava/lang/String; = "Seek"

.field public static final SETAVTURI:Ljava/lang/String; = "SetAVTransportURI"

.field public static final SETBGM:Ljava/lang/String; = "X_SetBGM"

.field public static final SETBGMVOLUME:Ljava/lang/String; = "X_SetBGMVolume"

.field public static final SETMUTE:Ljava/lang/String; = "SetMute"

.field public static final SETNEXTTRANSPORTURI:Ljava/lang/String; = "SetNextAVTransportURI"

.field public static final SETPLAYLIST:Ljava/lang/String; = "X_SetPlayList"

.field public static final SETVOL:Ljava/lang/String; = "SetVolume"

.field public static final SKIPDYNAMICBUFFER:Ljava/lang/String; = "X_SkipDynamicBuffer"

.field public static final STARTSLIDESHOW:Ljava/lang/String; = "X_StartSlideShow"

.field public static final STOP:Ljava/lang/String; = "Stop"

.field public static final STOPSLIDESHOW:Ljava/lang/String; = "X_StopSlideShow"

.field protected static actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;",
            ">;"
        }
    .end annotation
.end field

.field protected static deviceLockerMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/locks/ReentrantLock;",
            ">;"
        }
    .end annotation
.end field

.field public static excutor:Ljava/util/concurrent/ExecutorService;

.field public static excutorMutex:Ljava/lang/Object;

.field protected static sErrorNotifyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActionType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandlerID:Ljava/lang/String;

.field protected mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field protected final mTAG:Ljava/lang/String;

.field protected final mWorkerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-direct {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    .line 124
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->deviceLockerMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->sErrorNotifyMap:Ljava/util/HashMap;

    .line 128
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    .line 130
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutorMutex:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 2
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mTAG:Ljava/lang/String;

    .line 111
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 113
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    .line 115
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 117
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    .line 119
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 336
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    .line 134
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 135
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    .line 136
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 138
    return-void
.end method

.method private registerHandler()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->registerActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)Z

    .line 320
    :cond_0
    return-void
.end method

.method private unregisterHandler()V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->unregisterActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)V

    .line 331
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelActionRequest(J)V
    .locals 1
    .param p1, "req_id"    # J

    .prologue
    .line 299
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->cancelRequest(J)V

    .line 301
    return-void
.end method

.method protected abstract cancelRequest(J)V
.end method

.method protected convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p1, "ec"    # I

    .prologue
    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    sparse-switch p1, :sswitch_data_0

    .line 192
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 196
    :goto_0
    return-object v0

    .line 180
    :sswitch_0
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 181
    goto :goto_0

    .line 184
    :sswitch_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    .line 185
    goto :goto_0

    .line 188
    :sswitch_2
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->TIME_OUT:Lcom/samsung/android/allshare/ERROR;

    .line 189
    goto :goto_0

    .line 178
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x25e -> :sswitch_1
        0x321 -> :sswitch_2
    .end sparse-switch
.end method

.method protected abstract createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.end method

.method protected abstract defineHandler()V
.end method

.method public finiActionHandler()V
    .locals 0

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->finiHandler()V

    .line 235
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->unregisterHandler()V

    .line 237
    return-void
.end method

.method protected abstract finiHandler()V
.end method

.method public getActionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getActionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 160
    if-nez p1, :cond_1

    .line 161
    const/4 v0, 0x0

    .line 169
    :cond_0
    :goto_0
    return-object v0

    .line 163
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->deviceLockerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/locks/ReentrantLock;

    .line 164
    .local v0, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    if-nez v0, :cond_0

    .line 165
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    .end local v0    # "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 166
    .restart local v0    # "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->deviceLockerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getHandlerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    return-object v0
.end method

.method protected getManager()Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method public initActionHandler()V
    .locals 1

    .prologue
    .line 216
    const-string v0, "ACTION_TYPE_ASYNC"

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionType:Ljava/lang/String;

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->defineHandler()V

    .line 220
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->initHandler()V

    .line 221
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->registerHandler()V

    .line 223
    return-void
.end method

.method protected abstract initHandler()V
.end method

.method protected removeDeviceLocker(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 173
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->deviceLockerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    return-void
.end method

.method public responseAsyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 282
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 283
    .local v1, "m":Landroid/os/Message;
    if-eqz v1, :cond_0

    .line 284
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 285
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "REQUEST_MESSAGE_DATA"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 286
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 287
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mWorkerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 289
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected setActionID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_0
    return-void
.end method

.method protected setHandlerID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 151
    return-void
.end method
