.class public Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
.super Ljava/lang/Object;
.source "MRCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;,
        Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;
    }
.end annotation


# static fields
.field private static final AVTRESPONSE:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "MRCPWrapper"

.field private static final RCSRESPONSE:I = 0x1

.field private static final WHARESPONSE:I = 0x2

.field private static instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

.field private static instanceMutex:Ljava/lang/Object;

.field private static final mResponseHandler:Landroid/os/Handler;

.field private static sAVSListener:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sRCSListener:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sWHAListener:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mrcpAVSControlListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;",
            ">;"
        }
    .end annotation
.end field

.field private mrcpDeviceNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mrcpErrorNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mrcpEventNotifyListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mrcpRCSControlListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instanceMutex:Ljava/lang/Object;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;

    .line 78
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$1;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$1;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpRCSControlListeners:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpAVSControlListeners:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    .line 1000
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    if-nez v0, :cond_1

    .line 131
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 132
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 135
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0

    .line 135
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;

    .prologue
    .line 741
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;

    monitor-enter v1

    .line 742
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 743
    monitor-exit v1

    .line 744
    return-void

    .line 743
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;

    .prologue
    .line 735
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpAVSControlListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 736
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpAVSControlListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 737
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;

    .prologue
    .line 789
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 790
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 791
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPErrorNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;

    .prologue
    .line 813
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 814
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 815
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    .prologue
    .line 765
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 766
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 767
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPRCSControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;

    .prologue
    .line 702
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;

    monitor-enter v1

    .line 703
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    monitor-exit v1

    .line 705
    return-void

    .line 704
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPRCSControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;

    .prologue
    .line 696
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpRCSControlListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 697
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpRCSControlListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 698
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMRCPWHAControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;

    .prologue
    .line 709
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;

    monitor-enter v1

    .line 710
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    monitor-exit v1

    .line 712
    return-void

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public createParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 1025
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->createParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 1026
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 1027
    const-string v1, "MRCPWrapper"

    const-string v2, "createParty"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to createParty DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    :cond_0
    return v0
.end method

.method public getDeviceStatusInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 1016
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getDeviceStatusInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 1017
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 1018
    const-string v1, "MRCPWrapper"

    const-string v2, "getDeviceStatusInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to getDeviceStatusInfo DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    :cond_0
    return v0
.end method

.method public getMediaInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 608
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getMediaInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 609
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 610
    const-string v1, "MRCPWrapper"

    const-string v2, "getMediaInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get play state from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_0
    return v0
.end method

.method public getMute(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 262
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getMute(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 263
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 264
    const-string v1, "MRCPWrapper"

    const-string v2, "getMute"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get Mute from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_0
    return v0
.end method

.method public getPlayPositionInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 563
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getPlayPositionInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 564
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 565
    const-string v1, "MRCPWrapper"

    const-string v2, "getPlayPositionInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get play position from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    :cond_0
    return v0
.end method

.method public getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "state"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 581
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 582
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 583
    const-string v1, "MRCPWrapper"

    const-string v2, "getPlayState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get play state from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :cond_0
    return v0
.end method

.method public getStoppedReason(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 183
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mrcpGetStoppedReason(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 184
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 185
    const-string v1, "MRCPWrapper"

    const-string v2, "getStoppedReason"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to stopped reason from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_0
    return v0
.end method

.method public getTrnasportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 625
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getTransportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 626
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 627
    const-string v1, "MRCPWrapper"

    const-string v2, "getTrnasportInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP get trnasport info from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_0
    return v0
.end method

.method public getVolume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 297
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getVolume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 298
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 299
    const-string v1, "MRCPWrapper"

    const-string v2, "getVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get Volume from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_0
    return v0
.end method

.method public getZoomPort(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 245
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getZoomPort(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 246
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 247
    const-string v1, "MRCPWrapper"

    const-string v2, "getZoomPort"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get Zoom Port from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    return v0
.end method

.method public joinParty(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "partyID"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 1034
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->joinParty(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 1035
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 1036
    const-string v1, "MRCPWrapper"

    const-string v2, "joinParty"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to joinParty DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    :cond_0
    return v0
.end method

.method public leaveParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 1042
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->leaveParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 1043
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 1044
    const-string v1, "MRCPWrapper"

    const-string v2, "leaveParty"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to leaveParty DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    :cond_0
    return v0
.end method

.method public mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "serviceType"    # Ljava/lang/String;
    .param p3, "response"    # Landroid/os/Bundle;

    .prologue
    .line 591
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I

    move-result v0

    .line 593
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 594
    const-string v1, "MRCPWrapper"

    const-string v2, "mrcpGetServiceDescription"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to get service description from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_0
    return v0
.end method

.method public mrcpGetUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 686
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mrcpGetUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mrcpSetUserAgent(Ljava/lang/String;)V
    .locals 4
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 676
    const-string v0, "MRCPWrapper"

    const-string v1, "mrcpSetUserAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MRCP request to set user agent ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mrcpSetUserAgent(Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method public nextPlay(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 506
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->nextPlay(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 507
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 508
    const-string v1, "MRCPWrapper"

    const-string v2, "nextPlay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to nextPlay DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_0
    return v0
.end method

.method public onAVSControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 840
    iput-object p2, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->actionName:Ljava/lang/String;

    .line 841
    iput p3, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->errorCode:I

    .line 842
    iput p1, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->requestID:I

    .line 843
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 844
    .local v0, "msg":Landroid/os/Message;
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 845
    return-void
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 900
    const-string v2, "MRCPWrapper"

    const-string v3, "onDeviceNotify"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 902
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;

    .line 903
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;->onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 905
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906
    return-void
.end method

.method public onErrorNotify(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 919
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 920
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;

    .line 921
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;->onErrorNotify(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 923
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 924
    return-void
.end method

.method public onEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 884
    .local p2, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v3

    .line 885
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    .line 886
    .local v1, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;->onMRCPEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 888
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 889
    return-void
.end method

.method public onRCSControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;

    .prologue
    .line 857
    iput-object p2, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->actionName:Ljava/lang/String;

    .line 858
    iput p3, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->errorCode:I

    .line 859
    iput p1, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->requestID:I

    .line 860
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 861
    .local v0, "msg":Landroid/os/Message;
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 862
    return-void
.end method

.method public onWHAControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;

    .prologue
    .line 867
    iput-object p2, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->actionName:Ljava/lang/String;

    .line 868
    iput p3, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->errorCode:I

    .line 869
    iput p1, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->requestID:I

    .line 870
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 871
    .local v0, "msg":Landroid/os/Message;
    sget-object v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mResponseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 872
    return-void
.end method

.method public pause(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 440
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->pause(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 441
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 442
    const-string v1, "MRCPWrapper"

    const-string v2, "pause"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to pause DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    return v0
.end method

.method public play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I
    .locals 6
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "object"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "seekMode"    # Ljava/lang/String;
    .param p4, "seekTarget"    # J
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .param p7, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 337
    invoke-static/range {p1 .. p7}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I

    move-result v0

    .line 339
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 340
    const-string v1, "MRCPWrapper"

    const-string v2, "play"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to play DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    return v0
.end method

.method public playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p4, "response"    # Landroid/os/Bundle;

    .prologue
    .line 355
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I

    move-result v0

    .line 357
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 358
    const-string v1, "MRCPWrapper"

    const-string v2, "playLocalContent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to playLocalContent DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_0
    return v0
.end method

.method public playLocalContentEx(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;JLandroid/os/Bundle;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p4, "itemNode"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p5, "startTime"    # J
    .param p7, "response"    # Landroid/os/Bundle;

    .prologue
    .line 377
    invoke-static/range {p1 .. p7}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->playLocalContentEx(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;JLandroid/os/Bundle;)I

    move-result v0

    .line 379
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 380
    const-string v1, "MRCPWrapper"

    const-string v2, "playLocalContentEx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to playLocalContentEx DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_0
    return v0
.end method

.method public playRelayContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLandroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I
    .locals 6
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p4, "startTime"    # J
    .param p6, "response"    # Landroid/os/Bundle;
    .param p7, "albumDetails"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    .prologue
    .line 422
    invoke-static/range {p1 .. p7}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->playRelayContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLandroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    move-result v0

    .line 424
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 425
    const-string v1, "MRCPWrapper"

    const-string v2, "playRelayContent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to playRelayContent DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :cond_0
    return v0
.end method

.method public playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I
    .locals 6
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p4, "startTime"    # J
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .param p7, "albumDetails"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    .prologue
    .line 400
    invoke-static/range {p1 .. p7}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    move-result v0

    .line 402
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 403
    const-string v1, "MRCPWrapper"

    const-string v2, "playWebContent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to playWebContent DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_0
    return v0
.end method

.method public removeMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;

    .prologue
    .line 753
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpAVSControlListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 754
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpAVSControlListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 755
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMRCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;

    .prologue
    .line 801
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 802
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpDeviceNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 803
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMRCPErrorNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;

    .prologue
    .line 825
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 826
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpErrorNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 827
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    .prologue
    .line 777
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 778
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpEventNotifyListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 779
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMRCPRCSControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;

    .prologue
    .line 723
    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpRCSControlListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 724
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpRCSControlListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 725
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 456
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->resume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 457
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 458
    const-string v1, "MRCPWrapper"

    const-string v2, "resume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to resume DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_0
    return v0
.end method

.method public seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "seekUnit"    # Ljava/lang/String;
    .param p3, "seekPosition"    # I
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 490
    int-to-long v2, p3

    invoke-static {p1, p2, v2, v3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->seek(Ljava/lang/String;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 491
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 492
    const-string v1, "MRCPWrapper"

    const-string v2, "seek"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to seek DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :cond_0
    return v0
.end method

.method public setBGMListSlideshow(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;",
            ")I"
        }
    .end annotation

    .prologue
    .line 226
    .local p2, "slideBGMList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setBGMListSlideshow(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 228
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 229
    const-string v1, "MRCPWrapper"

    const-string v2, "setBGMListSlideshow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to set BGM list slide show from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_0
    return v0
.end method

.method public setBGMVolumeSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "level"    # I
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 236
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setBGMVolumeSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 237
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 238
    const-string v1, "MRCPWrapper"

    const-string v2, "setBGMVolumeSlideshow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to set BGM volume slide show from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    return v0
.end method

.method public setListSlideshow(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 8
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "albumTitle"    # Ljava/lang/String;
    .param p4, "currentPage"    # Ljava/lang/String;
    .param p5, "totalPages"    # Ljava/lang/String;
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;",
            ")I"
        }
    .end annotation

    .prologue
    .line 215
    .local p3, "slideImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setListSlideshow(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v7

    .line 217
    .local v7, "result":I
    if-eqz v7, :cond_0

    .line 218
    const-string v0, "MRCPWrapper"

    const-string v1, "setListSlideshow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MRCP request to set list slide show from DMR["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] failed!!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    return v7
.end method

.method public setListSlideshowLocal(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 8
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "albumTitle"    # Ljava/lang/String;
    .param p4, "currentPage"    # Ljava/lang/String;
    .param p5, "totalPages"    # Ljava/lang/String;
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;",
            ")I"
        }
    .end annotation

    .prologue
    .line 203
    .local p3, "slideImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    const/4 v5, 0x1

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setListSlideshow(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v7

    .line 205
    .local v7, "result":I
    if-eqz v7, :cond_0

    .line 206
    const-string v0, "MRCPWrapper"

    const-string v1, "setListSlideshow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MRCP request to set list slide show from DMR["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] failed!!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_0
    return v7
.end method

.method public setMute(Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "isMute"    # Z
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 280
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setMute(Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 281
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 282
    const-string v1, "MRCPWrapper"

    const-string v2, "setMute"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to set Mute on target DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_0
    return v0
.end method

.method public setNextLocalTranportUri(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p4, "subtitlePath"    # Ljava/lang/String;
    .param p5, "contentUri"    # Ljava/lang/String;
    .param p6, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 545
    invoke-static/range {p1 .. p6}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setNextLocalTranportUri(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 547
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 548
    const-string v1, "MRCPWrapper"

    const-string v2, "setNextLocalTranportUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to seek DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_0
    return v0
.end method

.method public setNextTranportUri(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "object"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 523
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setNextTranportUri(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 524
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 525
    const-string v1, "MRCPWrapper"

    const-string v2, "setNextTranportUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to seek DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_0
    return v0
.end method

.method public setVolume(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "volume"    # I
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 315
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->setVolume(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 316
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 317
    const-string v1, "MRCPWrapper"

    const-string v2, "setVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to set Volume to DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_0
    return v0
.end method

.method public skipDynamicBuffer(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 174
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->mrcpSkipDynamicBuffer(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 175
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 176
    const-string v1, "MRCPWrapper"

    const-string v2, "skipDynamicBuffer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to skip dynamic buffer from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    return v0
.end method

.method public startMRCP()I
    .locals 5

    .prologue
    .line 146
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->startMrcp()I

    move-result v0

    .line 147
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 148
    const-string v1, "MRCPWrapper"

    const-string v2, "startMRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP start failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    return v0
.end method

.method public startSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "interval"    # I
    .param p3, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 165
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->startSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 166
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 167
    const-string v1, "MRCPWrapper"

    const-string v2, "startSlideshow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to start slide show from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    return v0
.end method

.method public stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 472
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 473
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 474
    const-string v1, "MRCPWrapper"

    const-string v2, "stop"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to stop DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_0
    return v0
.end method

.method public stopMRCP()I
    .locals 5

    .prologue
    .line 157
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stopMrcp()I

    move-result v0

    .line 158
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 159
    const-string v1, "MRCPWrapper"

    const-string v2, "startMRCP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP stop failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    return v0
.end method

.method public stopSlideshow(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 192
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->stopSlideshow(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 193
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 194
    const-string v1, "MRCPWrapper"

    const-string v2, "stopSlideshow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to stop slide show from DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_0
    return v0
.end method

.method public subscribeDMR(Ljava/lang/String;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;

    .prologue
    .line 644
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->subscribeDMR(Ljava/lang/String;)I

    move-result v0

    .line 645
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 646
    const-string v1, "MRCPWrapper"

    const-string v2, "subscribeDMR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to subscribe DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    :cond_0
    return v0
.end method

.method public unSubscribeDMR(Ljava/lang/String;)I
    .locals 5
    .param p1, "dmrUDN"    # Ljava/lang/String;

    .prologue
    .line 659
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->unSubscribeDMR(Ljava/lang/String;)I

    move-result v0

    .line 660
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 661
    const-string v1, "MRCPWrapper"

    const-string v2, "unSubscribeDMR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MRCP request to cancel subscribe DMR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :goto_0
    return v0

    .line 664
    :cond_0
    const-string v1, "MRCPWrapper"

    const-string v2, "unSubscribeDMR"

    const-string v3, "MRCP request to remove this dms info from listener!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
