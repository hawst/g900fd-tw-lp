.class public Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;
.super Ljava/lang/Object;
.source "PopupList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$ItemDataAdapter;,
        Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;,
        Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;
    }
.end annotation


# instance fields
.field private final mAnchorLayout:Landroid/view/View;

.field private final mAnchorView:Landroid/view/View;

.field private mContentList:Landroid/widget/ListView;

.field private final mContext:Landroid/content/Context;

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;",
            ">;"
        }
    .end annotation
.end field

.field private mLongText:I

.field private mMaxWidth:I

.field private final mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private final mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnPopupItemClickListener:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;

.field private mPopupHeight:I

.field private mPopupWidth:I

.field private mPopupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "anchorLayout"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    .line 71
    const v0, 0x7f0a0027

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mLongText:I

    .line 92
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 108
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$2;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 120
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;

    .line 76
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorLayout:Landroid/view/View;

    .line 77
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->initListViewSize()V

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;
    .param p1, "x1"    # Landroid/widget/PopupWindow;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->updatePopupLayoutParams()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWidth:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupHeight:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createPopupWindow()Landroid/widget/PopupWindow;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    const/4 v4, 0x1

    .line 158
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 159
    .local v0, "popup":Landroid/widget/PopupWindow;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 160
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 162
    new-instance v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    const v3, 0x101006d

    invoke-direct {v1, v2, v6, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    .line 163
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 166
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    const v2, 0x7f02001b

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 167
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$ItemDataAdapter;

    invoke-direct {v2, p0, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$ItemDataAdapter;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 168
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 169
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContentList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 170
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 171
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 173
    return-object v0
.end method

.method private initListViewSize()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const v9, 0x7f0a0027

    const v8, 0x7f0a0026

    const/high16 v7, -0x80000000

    const/4 v6, 0x0

    .line 184
    const/4 v1, -0x1

    .line 185
    .local v1, "temp":I
    new-instance v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    const v3, 0x101006d

    invoke-direct {v0, v2, v4, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 187
    .local v0, "content":Landroid/widget/ListView;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$ItemDataAdapter;

    invoke-direct {v2, p0, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$ItemDataAdapter;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$1;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 189
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    const v5, 0x7f070015

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v9, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ListView;->measure(II)V

    .line 194
    invoke-virtual {v0}, Landroid/widget/ListView;->getMeasuredWidth()I

    move-result v1

    .line 196
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 198
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;

    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mContext:Landroid/content/Context;

    const v5, 0x7f070014

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v8, v4}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ListView;->measure(II)V

    .line 203
    invoke-virtual {v0}, Landroid/widget/ListView;->getMeasuredWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mMaxWidth:I

    .line 205
    iget v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mMaxWidth:I

    if-ge v1, v2, :cond_0

    .line 206
    iput v8, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mLongText:I

    .line 211
    :goto_0
    return-void

    .line 208
    :cond_0
    iput v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mMaxWidth:I

    .line 209
    iput v9, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mLongText:I

    goto :goto_0
.end method

.method private updatePopupLayoutParams()V
    .locals 2

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mLongText:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->findItem(I)Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mMaxWidth:I

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWidth:I

    .line 154
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupHeight:I

    .line 155
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWidth:I

    goto :goto_0
.end method


# virtual methods
.method public addItem(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;

    invoke-direct {v1, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public clearItems()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 90
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0
.end method

.method public findItem(I)Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 177
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;

    .line 178
    .local v1, "item":Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;
    iget v2, v1, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;->id:I

    if-ne v2, p1, :cond_0

    .line 180
    .end local v1    # "item":Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$Item;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOnPopupItemClickListener(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$OnPopupItemClickListener;

    .line 82
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 141
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 136
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->createPopupWindow()Landroid/widget/PopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 137
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->updatePopupLayoutParams()V

    .line 138
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWidth:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupHeight:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    goto :goto_0
.end method
