.class public Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
.super Ljava/lang/Object;
.source "DeviceStateHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;,
        Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;,
        Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;
    }
.end annotation


# static fields
.field private static final AS_MRCP_PLAYER_NOT_FOUND:I = 0x2329

.field private static final AVPLAYER_STATE_BUFFERING:I = 0x1

.field private static final GETTRANSPORTINFO:Ljava/lang/String; = "GetTransportInfo"

.field private static final SHORT_STATE_POLLING_TIME:J = 0x1770L

.field private static mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

.field private static mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

.field private static mPollingTaskMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;",
            ">;"
        }
    .end annotation
.end field

.field private static mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field private static mTAG:Ljava/lang/String;

.field private static mutex:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    const-string v0, "DeviceStateHandler"

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    .line 32
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    .line 34
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 36
    sput-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mutex:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "sm"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sput-object p1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 60
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 61
    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0
.end method

.method static synthetic access$600()Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-static {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->notifyPlayerStatus(Ljava/lang/String;ILandroid/os/Bundle;)V

    return-void
.end method

.method private static convertToAllShareEventString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "allsharePlayerState":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 130
    const-string v0, ""

    .line 133
    :goto_0
    return-object v0

    .line 118
    :pswitch_0
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    .line 119
    goto :goto_0

    .line 121
    :pswitch_1
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    .line 122
    goto :goto_0

    .line 124
    :pswitch_2
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    .line 125
    goto :goto_0

    .line 127
    :pswitch_3
    const-string v0, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    .line 128
    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getInstance()Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    return-object v0
.end method

.method public static getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
    .locals 2
    .param p0, "sm"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    if-nez v0, :cond_1

    .line 44
    const-class v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    .line 48
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mInstance:Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static notifyPlayerStatus(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2
    .param p0, "deviceUDN"    # Ljava/lang/String;
    .param p1, "notifyState"    # I
    .param p2, "notifyBundle"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-static {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->convertToAllShareEventString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "playState":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v1, v0, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 139
    return-void
.end method

.method public static stopBufferingStatePolling(Ljava/lang/String;)V
    .locals 4
    .param p0, "udn"    # Ljava/lang/String;

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v1, "stopBufferingStatePolling"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device udn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v2, "stopBufferingStatePolling"

    const-string v3, "there is no polling task for this devices"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    monitor-exit v1

    .line 93
    :goto_0
    return-void

    .line 88
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 89
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->stop()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;)V

    .line 90
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public startBufferingStatePolling(Ljava/lang/String;)V
    .locals 5
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 64
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v2, "startBufferingStatePolling"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "device udn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mutex:Ljava/lang/Object;

    monitor-enter v2

    .line 66
    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v3, "startBufferingStatePolling"

    const-string v4, "This device has been polling already!"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    monitor-exit v2

    .line 78
    :goto_0
    return-void

    .line 72
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;-><init>(Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;)V

    .line 73
    .local v0, "task":Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;
    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->start()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;)V

    .line 76
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    monitor-exit v2

    goto :goto_0

    .end local v0    # "task":Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stopNICTask(Ljava/lang/String;)V
    .locals 7
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 96
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v4, "stopNICTask"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stop all device\'s polling task in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mPollingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 102
    .local v1, "taskSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 103
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;

    const-string v4, "stopNICTask"

    const-string v5, "taskSet is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    return-void

    .line 107
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 108
    .local v2, "udn":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 109
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopBufferingStatePolling(Ljava/lang/String;)V

    goto :goto_0
.end method
