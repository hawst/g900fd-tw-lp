.class Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "ReceiverAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiverActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$1;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->createItemByFilePath(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V

    return-void
.end method

.method private createItemByFilePath(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 7
    .param p1, "itemBundle"    # Landroid/os/Bundle;

    .prologue
    .line 350
    const-string v6, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "mimeType":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 352
    const/4 v0, 0x0

    .line 371
    :goto_0
    return-object v0

    .line 353
    :cond_0
    const-string v6, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "itemType":Ljava/lang/String;
    const-string v6, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 355
    .local v4, "title":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 356
    .local v2, "mediaType":Lcom/samsung/android/allshare/Item$MediaType;
    const/4 v5, -0x1

    .line 357
    .local v5, "type":I
    if-eqz v1, :cond_1

    .line 358
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v2

    .line 359
    :cond_1
    if-nez v4, :cond_2

    .line 360
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v4

    .line 361
    :cond_2
    const-string v6, "audio"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 362
    const/4 v5, 0x1

    .line 368
    :cond_3
    :goto_1
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;-><init>()V

    .line 369
    .local v0, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    iput-object v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    .line 370
    iput v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    goto :goto_0

    .line 363
    .end local v0    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_4
    const-string v6, "image"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 364
    const/4 v5, 0x0

    goto :goto_1

    .line 365
    :cond_5
    const-string v6, "video"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 366
    const/4 v5, 0x2

    goto :goto_1
.end method

.method private notifyEvent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/ERROR;)V
    .locals 4
    .param p1, "targetDevice"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "result"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const-wide/16 v2, 0x0

    .line 376
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 377
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 378
    const-string v1, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 379
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-static {p2}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 381
    const-string v1, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 382
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v1, "BUNDLE_STRING_CATEGORY"

    const-string v2, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v1, "BUNDLE_STRING_ID"

    iget-object v2, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v1

    const-string v2, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 388
    return-void
.end method

.method private uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    .locals 12
    .param p1, "targetDevice"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    const/4 v9, 0x0

    .line 292
    const/4 v5, 0x0

    .line 293
    .local v5, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v8, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    if-eqz v8, :cond_0

    iget-object v8, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 294
    iget-object v8, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    check-cast v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 296
    .restart local v5    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_0
    if-nez v5, :cond_1

    .line 297
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, "There is no resource in object!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    .line 346
    :goto_0
    return-object v8

    .line 303
    :cond_1
    :try_start_0
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;

    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v8

    iget-object v9, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-direct {v0, v8, p2, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;)V

    .line 305
    .local v0, "createHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;
    iget-object v8, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v8, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->createObject(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "import_uri":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 307
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " create object failed!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 311
    :cond_2
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 313
    .local v4, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    iget-object v8, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    iget-object v9, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v3, v8, v9, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v2

    .line 316
    .local v2, "error":I
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;

    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v8

    invoke-direct {v6, v8, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    .line 318
    .local v6, "transferHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    if-nez v2, :cond_3

    .line 319
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setSupportPostObject(Z)V

    .line 320
    iget v8, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setTransfer_id(Ljava/lang/String;)V

    .line 321
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v8

    iget-object v9, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " postObject object successful! objectID = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 326
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " postObject failed,try to importResource !"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setSupportPostObject(Z)V

    .line 328
    iget-object v8, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    iget-object v9, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    invoke-virtual {v0, v8, v3, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 330
    .local v7, "transfer_id":Ljava/lang/String;
    if-nez v7, :cond_4

    .line 331
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " import resource failed!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 335
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v6, p1, v7, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->getTransferProgress(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Z)V

    .line 336
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v8

    iget-object v9, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " import resource successful! objectID = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-direct {p0, p1, p2, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->notifyEvent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 342
    .end local v0    # "createHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;
    .end local v2    # "error":I
    .end local v3    # "import_uri":Ljava/lang/String;
    .end local v4    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v6    # "transferHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    .end local v7    # "transfer_id":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 343
    .local v1, "e":Ljava/lang/Exception;
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, "uploadContent Exception: "

    invoke-static {v8, v9, v10, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 344
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0
.end method

.method private uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    .locals 11
    .param p1, "targetDevice"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p2, "sourceObjectUrl"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 236
    if-nez p2, :cond_0

    .line 237
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, "[ReceiverActionWorker][uploadContent] There is no resource in object!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    .line 288
    :goto_0
    return-object v8

    .line 242
    :cond_0
    move-object v4, p2

    .line 243
    .local v4, "originSourceObjectUri":Ljava/lang/String;
    const-string v8, "file://"

    invoke-virtual {p2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 244
    const/4 v8, 0x7

    invoke-virtual {p2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 248
    :cond_1
    :try_start_0
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;

    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v8

    iget-object v9, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-direct {v0, v8, p4, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;)V

    .line 250
    .local v0, "createHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;
    iget-object v8, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v8, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->createObject(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/lang/String;

    move-result-object v3

    .line 251
    .local v3, "import_uri":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 252
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " create object failed!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 256
    :cond_2
    new-instance v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 258
    .local v5, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    invoke-virtual {v0, v3, p2, p3, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v2

    .line 261
    .local v2, "error":I
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;

    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v8

    invoke-direct {v6, v8, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    .line 263
    .local v6, "transferHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    if-nez v2, :cond_3

    .line 264
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " postObject object successful!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setSupportPostObject(Z)V

    .line 266
    iget v8, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setTransfer_id(Ljava/lang/String;)V

    .line 267
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 270
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " postObject failed,try to importResource !"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->setSupportPostObject(Z)V

    .line 272
    iget-object v8, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v0, v8, v3, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 274
    .local v7, "transfer_id":Ljava/lang/String;
    if-nez v7, :cond_4

    .line 275
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, " import resource failed!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 279
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v6, p1, v7, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->getTransferProgress(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Z)V

    .line 280
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-direct {p0, p1, p4, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->notifyEvent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 284
    .end local v0    # "createHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;
    .end local v2    # "error":I
    .end local v3    # "import_uri":Ljava/lang/String;
    .end local v5    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v6    # "transferHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    .end local v7    # "transfer_id":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 285
    .local v1, "e":Ljava/lang/Exception;
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "uploadContent"

    const-string v10, "uploadContent Exception: "

    invoke-static {v8, v9, v10, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 286
    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 103
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 232
    return-void
.end method
