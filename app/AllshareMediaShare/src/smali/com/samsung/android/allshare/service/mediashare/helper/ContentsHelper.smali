.class public Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;
.super Ljava/lang/Object;
.source "ContentsHelper.java"


# static fields
.field private static MEDIA_STORE_THUMBNAIL_URIS_ALL:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_ALL:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri; = null

.field private static final TAG:Ljava/lang/String; = "ContentsHelper"


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri;

    .line 48
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri;

    .line 53
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri;

    .line 58
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_ALL:[Landroid/net/Uri;

    .line 67
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Images$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_THUMBNAIL_URIS_ALL:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertContentUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 50
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 169
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 170
    .local v18, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v17, 0x0

    .line 173
    .local v17, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 177
    :goto_0
    if-nez v17, :cond_0

    .line 178
    const/16 v36, 0x0

    .line 448
    :goto_1
    return-object v36

    .line 174
    :catch_0
    move-exception v25

    .line 175
    .local v25, "e":Ljava/lang/Exception;
    const-string v2, "ContentsHelper"

    const-string v4, "convertContentUriToItemNode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " exception :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    .line 181
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 182
    const/16 v36, 0x0

    goto :goto_1

    .line 184
    :cond_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 185
    const/16 v33, 0x0

    .local v33, "i":I
    :goto_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    move/from16 v0, v33

    if-ge v0, v2, :cond_1

    .line 186
    move-object/from16 v0, v17

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    add-int/lit8 v33, v33, 0x1

    goto :goto_2

    .line 189
    .end local v33    # "i":I
    :cond_2
    const-string v2, "_data"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 190
    .local v19, "data":Ljava/lang/String;
    const-string v2, "date_modified"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 191
    .local v21, "date_modified":Ljava/lang/String;
    const-string v2, "date_added"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 192
    .local v20, "date_created":Ljava/lang/String;
    const-string v2, "_size"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    .line 193
    .local v42, "size":Ljava/lang/String;
    const-string v2, "title"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Ljava/lang/String;

    .line 194
    .local v47, "title":Ljava/lang/String;
    const-string v2, "mime_type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Ljava/lang/String;

    .line 196
    .local v37, "mime":Ljava/lang/String;
    if-nez v37, :cond_3

    .line 197
    const-string v2, "ContentsHelper"

    const-string v4, "convertContentUriToItemNode"

    const-string v6, "mime is null"

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 199
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 203
    :cond_3
    const-string v2, "image/x-ms-bmp"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 204
    const-string v37, "image/bmp"

    .line 207
    :cond_4
    const-string v2, "image/jpg"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 208
    const-string v2, "ContentsHelper"

    const-string v4, "convertContentUriToItemNode"

    const-string v6, "fix to image/jpeg"

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v37, "image/jpeg"

    .line 213
    :cond_5
    const-string v2, "video/x-mkv"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "video/mkv"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "video/x-matroska"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 215
    :cond_6
    const-string v37, "video/x-mkv"

    .line 217
    :cond_7
    const-string v2, "audio/x-wav"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 218
    const-string v37, "audio/wav"

    .line 221
    :cond_8
    new-instance v36, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-direct/range {v36 .. v36}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;-><init>()V

    .line 222
    .local v36, "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    .line 223
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    .line 224
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;-><init>()V

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    .line 226
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 227
    const-string v2, "album"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 228
    .local v10, "album":Ljava/lang/String;
    new-instance v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct {v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 229
    .local v11, "albumAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v2, "upnp:album"

    iput-object v2, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 230
    iput-object v10, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 231
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    const/4 v2, 0x1

    move-object/from16 v0, v36

    iput v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    .line 233
    const-string v2, "album_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 234
    .local v13, "albumId":I
    if-lez v13, :cond_9

    .line 235
    sget-object v2, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v6, v13

    invoke-static {v2, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 237
    .local v3, "albumUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 239
    .local v12, "albumCur":Landroid/database/Cursor;
    if-eqz v12, :cond_10

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 240
    const-string v2, "album_art"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 241
    .local v26, "fileIdx":I
    move/from16 v0, v26

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 242
    .local v27, "filePath":Ljava/lang/String;
    if-eqz v27, :cond_f

    .line 243
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->loadBitmapImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v44

    .line 244
    .local v44, "thumbNail":Landroid/graphics/Bitmap;
    if-eqz v44, :cond_e

    .line 245
    const-string v45, ""

    .line 246
    .local v45, "thumbNailFilePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v35

    .line 247
    .local v35, "index":I
    const/4 v2, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v45

    .line 248
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/thumbnail.bmp"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    .line 249
    invoke-static/range {v44 .. v45}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 250
    new-instance v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct {v14}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 251
    .local v14, "albumartAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v2, "upnp:albumart"

    iput-object v2, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 252
    move-object/from16 v0, v45

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 253
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    .end local v14    # "albumartAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    .end local v35    # "index":I
    .end local v44    # "thumbNail":Landroid/graphics/Bitmap;
    .end local v45    # "thumbNailFilePath":Ljava/lang/String;
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 267
    .end local v3    # "albumUri":Landroid/net/Uri;
    .end local v10    # "album":Ljava/lang/String;
    .end local v11    # "albumAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    .end local v12    # "albumCur":Landroid/database/Cursor;
    .end local v13    # "albumId":I
    .end local v26    # "fileIdx":I
    .end local v27    # "filePath":Ljava/lang/String;
    :cond_9
    :goto_4
    move-object/from16 v0, v47

    move-object/from16 v1, v36

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    .line 270
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 271
    const-string v2, "year"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    .line 272
    .local v39, "recordingYear":Ljava/lang/String;
    const-string v2, "ContentsHelper"

    const-string v4, "convertContentUriToItemNode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "recordingYear = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v39

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "-00-00"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->date:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 286
    .end local v21    # "date_modified":Ljava/lang/String;
    .end local v39    # "recordingYear":Ljava/lang/String;
    :goto_5
    const/16 v48, 0x0

    .line 287
    .local v48, "width":I
    const/16 v31, 0x0

    .line 288
    .local v31, "height":I
    const/16 v24, 0x0

    .line 290
    .local v24, "duration":Ljava/lang/String;
    new-instance v38, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 291
    .local v38, "opt":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_13

    .line 293
    const/4 v2, 0x1

    move-object/from16 v0, v38

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 294
    const-string v2, "object.item.imageItem"

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->upnpClass:Ljava/lang/String;

    .line 295
    const/4 v2, 0x0

    move-object/from16 v0, v36

    iput v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    .line 296
    move-object/from16 v0, v19

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 297
    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v31, v0

    .line 298
    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v48, v0

    .line 346
    :cond_a
    :goto_6
    new-instance v41, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    invoke-direct/range {v41 .. v41}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;-><init>()V

    .line 348
    .local v41, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    if-eqz v37, :cond_b

    .line 349
    move-object/from16 v0, v37

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    .line 351
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http-get:*:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":*"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    .line 365
    :cond_b
    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    .line 368
    if-eqz v48, :cond_1b

    if-eqz v31, :cond_1b

    .line 369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 370
    .local v40, "resolution":Ljava/lang/String;
    move-object/from16 v0, v40

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    .line 375
    .end local v40    # "resolution":Ljava/lang/String;
    :goto_8
    if-eqz v42, :cond_1c

    .line 377
    :try_start_2
    invoke-static/range {v42 .. v42}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, v41

    iput-wide v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 386
    :goto_9
    if-eqz v24, :cond_1d

    .line 387
    const-wide/16 v22, 0x0

    .line 389
    .local v22, "dur":J
    :try_start_3
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v22, v0

    .line 394
    :goto_a
    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/service/mediashare/utility/TimeUtil;->getTimeString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    .line 399
    .end local v22    # "dur":J
    :goto_b
    const-string v2, "http"

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->transport:Ljava/lang/String;

    .line 400
    const-string v2, ""

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->bitrate:Ljava/lang/String;

    .line 401
    const-string v2, ""

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->extension:Ljava/lang/String;

    .line 402
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resource_attributes:Ljava/util/ArrayList;

    .line 403
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    move-object/from16 v0, v41

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    if-eqz v37, :cond_c

    .line 410
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 411
    const-string v2, "width"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Ljava/lang/String;

    .line 412
    .local v49, "width_thumbnail":Ljava/lang/String;
    const-string v2, "height"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    .line 414
    .local v32, "height_thumbnail":Ljava/lang/String;
    if-eqz v49, :cond_1e

    if-eqz v32, :cond_1e

    .line 415
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "x"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->resolution:Ljava/lang/String;

    .line 428
    .end local v32    # "height_thumbnail":Ljava/lang/String;
    .end local v49    # "width_thumbnail":Ljava/lang/String;
    :cond_c
    :goto_c
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    const/4 v4, 0x0

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 430
    if-eqz v37, :cond_d

    .line 431
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http-get:*:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":*"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->protocolinfo:Ljava/lang/String;

    .line 432
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "image"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 433
    const-string v2, "_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 434
    .local v34, "imageId":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-static {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryImageThumbnailUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v46

    .line 435
    .local v46, "thumbnailUri":Landroid/net/Uri;
    if-eqz v46, :cond_d

    .line 436
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    invoke-virtual/range {v46 .. v46}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->uri:Ljava/lang/String;

    .line 442
    .end local v34    # "imageId":Ljava/lang/String;
    .end local v46    # "thumbnailUri":Landroid/net/Uri;
    :cond_d
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    const-string v4, ""

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->size:Ljava/lang/String;

    .line 445
    const-string v2, "1"

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    .line 446
    const-string v2, "0"

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->parentID:Ljava/lang/String;

    .line 447
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 255
    .end local v24    # "duration":Ljava/lang/String;
    .end local v31    # "height":I
    .end local v38    # "opt":Landroid/graphics/BitmapFactory$Options;
    .end local v41    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v48    # "width":I
    .restart local v3    # "albumUri":Landroid/net/Uri;
    .restart local v10    # "album":Ljava/lang/String;
    .restart local v11    # "albumAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    .restart local v12    # "albumCur":Landroid/database/Cursor;
    .restart local v13    # "albumId":I
    .restart local v21    # "date_modified":Ljava/lang/String;
    .restart local v26    # "fileIdx":I
    .restart local v27    # "filePath":Ljava/lang/String;
    .restart local v44    # "thumbNail":Landroid/graphics/Bitmap;
    :cond_e
    const-string v2, "ContentsHelper"

    const-string v4, "createAlbumArt"

    const-string v6, "can not store albumArtBitMap from filePath"

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 259
    .end local v44    # "thumbNail":Landroid/graphics/Bitmap;
    :cond_f
    const-string v2, "ContentsHelper"

    const-string v4, "createAlbumArt"

    const-string v6, "album art file path is null"

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 263
    .end local v26    # "fileIdx":I
    .end local v27    # "filePath":Ljava/lang/String;
    :cond_10
    const-string v2, "ContentsHelper"

    const-string v4, "createAlbumArt"

    const-string v6, "there is no album info"

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 277
    .end local v3    # "albumUri":Landroid/net/Uri;
    .end local v10    # "album":Ljava/lang/String;
    .end local v11    # "albumAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    .end local v12    # "albumCur":Landroid/database/Cursor;
    .end local v13    # "albumId":I
    :cond_11
    :try_start_4
    const-string v2, "yyyy-MM-dd"

    new-instance v4, Ljava/util/Date;

    if-eqz v21, :cond_12

    .end local v21    # "date_modified":Ljava/lang/String;
    :goto_d
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->date:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_5

    .line 282
    :catch_1
    move-exception v25

    .line 283
    .restart local v25    # "e":Ljava/lang/Exception;
    const-string v2, "ContentsHelper"

    const-string v4, "convertContentUriToItemNode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " setDate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v21    # "date_modified":Ljava/lang/String;
    :cond_12
    move-object/from16 v21, v20

    .line 277
    goto :goto_d

    .line 299
    .end local v21    # "date_modified":Ljava/lang/String;
    .restart local v24    # "duration":Ljava/lang/String;
    .restart local v31    # "height":I
    .restart local v38    # "opt":Landroid/graphics/BitmapFactory$Options;
    .restart local v48    # "width":I
    :cond_13
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_15

    .line 301
    const-string v2, "object.item.videoItem"

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->upnpClass:Ljava/lang/String;

    .line 302
    const/4 v2, 0x2

    move-object/from16 v0, v36

    iput v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->type:I

    .line 303
    const-string v2, "resolution"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    .line 304
    .restart local v40    # "resolution":Ljava/lang/String;
    if-eqz v40, :cond_a

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_a

    .line 305
    new-instance v43, Ljava/util/StringTokenizer;

    const-string v2, "x"

    move-object/from16 v0, v43

    move-object/from16 v1, v40

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    .local v43, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_14

    .line 307
    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v48

    .line 308
    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v31

    .line 310
    :cond_14
    const-string v2, "duration"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "duration":Ljava/lang/String;
    check-cast v24, Ljava/lang/String;

    .restart local v24    # "duration":Ljava/lang/String;
    goto/16 :goto_6

    .line 312
    .end local v40    # "resolution":Ljava/lang/String;
    .end local v43    # "st":Ljava/util/StringTokenizer;
    :cond_15
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_a

    .line 313
    const-string v2, "object.item.audioItem"

    move-object/from16 v0, v36

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->upnpClass:Ljava/lang/String;

    .line 314
    const-string v2, "duration"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "duration":Ljava/lang/String;
    check-cast v24, Ljava/lang/String;

    .line 316
    .restart local v24    # "duration":Ljava/lang/String;
    const-string v2, "artist"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 317
    .local v15, "artist":Ljava/lang/String;
    if-eqz v15, :cond_16

    invoke-virtual {v15}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_16

    .line 318
    new-instance v16, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 319
    .local v16, "artistAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v2, "upnp:artist"

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 320
    move-object/from16 v0, v16

    iput-object v15, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 321
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    .end local v16    # "artistAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    :cond_16
    const-string v2, "genres"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 325
    .local v5, "genreUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "name"

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v30

    .line 329
    .local v30, "genreCursor":Landroid/database/Cursor;
    if-eqz v30, :cond_a

    .line 330
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 331
    const/4 v2, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 332
    .local v28, "genre":Ljava/lang/String;
    if-eqz v28, :cond_17

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_17

    .line 333
    new-instance v29, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;

    invoke-direct/range {v29 .. v29}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;-><init>()V

    .line 334
    .local v29, "genreAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    const-string v2, "upnp:genre"

    move-object/from16 v0, v29

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->name:Ljava/lang/String;

    .line 335
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;->value:Ljava/lang/String;

    .line 336
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    .end local v28    # "genre":Ljava/lang/String;
    .end local v29    # "genreAttrib":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareAttributes;
    :cond_17
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    goto/16 :goto_6

    .line 353
    .end local v5    # "genreUri":Landroid/net/Uri;
    .end local v15    # "artist":Ljava/lang/String;
    .end local v30    # "genreCursor":Landroid/database/Cursor;
    .restart local v41    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_18
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 354
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http-get:*:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":DLNA.ORG_OP=01;DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    goto/16 :goto_7

    .line 355
    :cond_19
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 356
    const-string v2, "audio/mpeg"

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http-get:*:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    goto/16 :goto_7

    .line 360
    :cond_1a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http-get:*:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":DLNA.ORG_OP=01;DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->protocolInfo:Ljava/lang/String;

    goto/16 :goto_7

    .line 372
    :cond_1b
    const-string v2, "  x  "

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->resolution:Ljava/lang/String;

    goto/16 :goto_8

    .line 378
    :catch_2
    move-exception v25

    .line 379
    .restart local v25    # "e":Ljava/lang/Exception;
    const-wide/16 v6, 0x0

    move-object/from16 v0, v41

    iput-wide v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J

    goto/16 :goto_9

    .line 382
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_1c
    const-wide/16 v6, 0x0

    move-object/from16 v0, v41

    iput-wide v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->size:J

    goto/16 :goto_9

    .line 390
    .restart local v22    # "dur":J
    :catch_3
    move-exception v25

    .line 391
    .restart local v25    # "e":Ljava/lang/Exception;
    const-wide/16 v22, 0x0

    goto/16 :goto_a

    .line 396
    .end local v22    # "dur":J
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_1d
    const-string v2, "0:00:00"

    move-object/from16 v0, v41

    iput-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->duration:Ljava/lang/String;

    goto/16 :goto_b

    .line 417
    .restart local v32    # "height_thumbnail":Ljava/lang/String;
    .restart local v49    # "width_thumbnail":Ljava/lang/String;
    :cond_1e
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    const-string v4, "  x  "

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->resolution:Ljava/lang/String;

    goto/16 :goto_c

    .line 419
    .end local v32    # "height_thumbnail":Ljava/lang/String;
    .end local v49    # "width_thumbnail":Ljava/lang/String;
    :cond_1f
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 420
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    const-string v4, "  x  "

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->resolution:Ljava/lang/String;

    goto/16 :goto_c

    .line 421
    :cond_20
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 422
    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->thumbnail:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;

    const-string v4, "  x  "

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareThumbnailInfo;->resolution:Ljava/lang/String;

    goto/16 :goto_c
.end method

.method public static convertUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 154
    if-nez p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-object v1

    .line 158
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "scheme":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 165
    invoke-static {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->convertContentUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v1

    goto :goto_0
.end method

.method public static deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 512
    if-eqz p0, :cond_0

    .line 513
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    .local v0, "cacheFile":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 519
    .end local v0    # "cacheFile":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private static loadBitmapImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 482
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 484
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v2, 0x280

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 485
    const/16 v2, 0x1e0

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 486
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 487
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static parseUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/util/HashMap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 452
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v7, v10

    .line 478
    :goto_0
    return-object v7

    .line 455
    :cond_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 456
    .local v7, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 459
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 464
    :goto_1
    if-nez v6, :cond_2

    move-object v7, v10

    .line 465
    goto :goto_0

    .line 460
    :catch_0
    move-exception v8

    .line 461
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "ContentsHelper"

    const-string v1, "parseUri"

    const-string v2, "convertContentUriToItemNode : cursor have problem "

    invoke-static {v0, v1, v2, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 467
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 468
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v7, v10

    .line 469
    goto :goto_0

    .line 472
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 473
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ge v9, v0, :cond_3

    .line 474
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 476
    .end local v9    # "i":I
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-static {p1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 74
    sget-object v12, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_ALL:[Landroid/net/Uri;

    .line 75
    .local v12, "mediaStoreUris":[Landroid/net/Uri;
    if-eqz p2, :cond_0

    .line 76
    const-string v0, "video"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    sget-object v12, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri;

    .line 85
    :cond_0
    :goto_0
    move-object v6, v12

    .local v6, "arr$":[Landroid/net/Uri;
    array-length v11, v6

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v11, :cond_5

    aget-object v1, v6, v9

    .line 88
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 95
    .local v7, "c":Landroid/database/Cursor;
    if-nez v7, :cond_3

    .line 96
    const-string v0, "ContentsHelper"

    const-string v2, "queryContentUri"

    const-string v3, "c == null"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .end local v7    # "c":Landroid/database/Cursor;
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 78
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "arr$":[Landroid/net/Uri;
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    :cond_1
    const-string v0, "audio"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    sget-object v12, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri;

    goto :goto_0

    .line 80
    :cond_2
    const-string v0, "image"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-object v12, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri;

    goto :goto_0

    .line 90
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "arr$":[Landroid/net/Uri;
    .restart local v9    # "i$":I
    .restart local v11    # "len$":I
    :catch_0
    move-exception v8

    .line 91
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "ContentsHelper"

    const-string v2, "queryContentUri"

    const-string v3, "SQLiteException"

    invoke-static {v0, v2, v3, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 100
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 101
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 102
    .local v10, "id":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 103
    const-string v0, "ContentsHelper"

    const-string v2, "queryContentUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    int-to-long v4, v10

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    int-to-long v2, v10

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 113
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v10    # "id":I
    :goto_3
    return-object v0

    .line 106
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_4
    const-string v0, "ContentsHelper"

    const-string v2, "queryContentUri"

    const-string v3, "c.moveToNext() == false"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 112
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_5
    const-string v0, "ContentsHelper"

    const-string v2, "queryContentUri"

    const-string v3, "return null"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static queryImageThumbnailUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageID"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x0

    .line 118
    sget-object v12, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->MEDIA_STORE_THUMBNAIL_URIS_ALL:[Landroid/net/Uri;

    .line 120
    .local v12, "mediaThumbnailUris":[Landroid/net/Uri;
    move-object v6, v12

    .local v6, "arr$":[Landroid/net/Uri;
    array-length v11, v6

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v11, :cond_2

    aget-object v1, v6, v9

    .line 123
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "image_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 128
    .local v7, "c":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 120
    .end local v7    # "c":Landroid/database/Cursor;
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 125
    :catch_0
    move-exception v8

    .line 126
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    goto :goto_1

    .line 131
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 132
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 133
    .local v10, "id":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 134
    int-to-long v2, v10

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 139
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v10    # "id":I
    :goto_2
    return-object v0

    .line 137
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_2
    move-object v0, v13

    .line 139
    goto :goto_2
.end method

.method private static saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 491
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 492
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 495
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 496
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 502
    if-eqz v3, :cond_0

    .line 503
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 509
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return-void

    .line 504
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 508
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 498
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 499
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 502
    if-eqz v2, :cond_1

    .line 503
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 504
    :catch_2
    move-exception v0

    .line 505
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 501
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 502
    :goto_2
    if-eqz v2, :cond_2

    .line 503
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 506
    :cond_2
    :goto_3
    throw v4

    .line 504
    :catch_3
    move-exception v0

    .line 505
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 501
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 498
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method
