.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;
.super Ljava/lang/Object;
.source "ImageViewerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 44

    .prologue
    .line 184
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    if-nez v5, :cond_0

    .line 185
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "cvm is null"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :goto_0
    return-void

    .line 189
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v34

    .line 190
    .local v34, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v32

    .line 191
    .local v32, "actionID":Ljava/lang/String;
    if-eqz v32, :cond_1

    if-nez v34, :cond_2

    .line 192
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "bundle/action id is null"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_2
    const-class v5, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 197
    const-string v5, "BUNDLE_STRING_ID"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 198
    .local v43, "udn":Ljava/lang/String;
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/os/Bundle;

    .line 199
    .local v6, "itemBundle":Landroid/os/Bundle;
    const-string v5, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/media/ContentInfo;

    .line 201
    .local v7, "ci":Lcom/samsung/android/allshare/media/ContentInfo;
    const-wide/16 v8, 0x0

    .line 203
    .local v8, "startingPosition":J
    if-eqz v7, :cond_4

    .line 204
    invoke-virtual {v7}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v8

    .line 212
    :goto_1
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v5

    const/4 v10, 0x1

    move-object/from16 v0, v43

    invoke-virtual {v5, v10, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v13

    .line 213
    .local v13, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-eqz v13, :cond_3

    iget-object v5, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 214
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "deviceItem == null"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 208
    .end local v13    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_4
    const-string v5, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    goto :goto_1

    .line 220
    .restart local v13    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "actionID = "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v32

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 223
    if-nez v6, :cond_6

    .line 224
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "itemBundle is null"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 229
    :cond_6
    const-string v5, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 230
    .local v38, "objectID":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 231
    .local v41, "serverUdn":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/Bundle;

    .line 232
    .local v40, "sItemBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v38

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v5, v10, v0, v1, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 233
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 234
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 238
    :cond_7
    new-instance v33, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;

    invoke-direct/range {v33 .. v33}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;-><init>()V

    .line 239
    .local v33, "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v41

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v12

    .line 241
    .local v12, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->discard()V

    .line 243
    if-nez v12, :cond_8

    .line 244
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v38

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "browse fail , contentItem is null"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 251
    :cond_8
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v5

    const/4 v10, 0x2

    move-object/from16 v0, v41

    invoke-virtual {v5, v10, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v35

    .line 253
    .local v35, "deviceProvider":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const-string v18, ""

    .line 254
    .local v18, "deviceName":Ljava/lang/String;
    if-eqz v35, :cond_9

    .line 255
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 258
    :cond_9
    const/16 v39, 0x0

    .line 259
    .local v39, "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v5, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    if-eqz v5, :cond_a

    iget-object v5, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_a

    .line 260
    iget-object v5, v12, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    .end local v39    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    check-cast v39, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 262
    .restart local v39    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    :cond_a
    if-eqz v39, :cond_b

    .line 263
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v0, v39

    iget-object v14, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->URI:Ljava/lang/String;

    move-object v11, v6

    move-object v15, v7

    move-wide/from16 v16, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->playItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v10 .. v18}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 266
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 270
    .end local v12    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v18    # "deviceName":Ljava/lang/String;
    .end local v33    # "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    .end local v35    # "deviceProvider":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v38    # "objectID":Ljava/lang/String;
    .end local v39    # "resource":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    .end local v40    # "sItemBundle":Landroid/os/Bundle;
    .end local v41    # "serverUdn":Ljava/lang/String;
    :cond_c
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 271
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->stopItem(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v5, v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto/16 :goto_0

    .line 272
    :cond_d
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 274
    if-nez v6, :cond_e

    .line 275
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "itemBundle is null"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    invoke-static/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 280
    :cond_e
    const-string v5, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v21

    check-cast v21, Landroid/net/Uri;

    .line 281
    .local v21, "uri":Landroid/net/Uri;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/Bundle;

    .line 282
    .restart local v40    # "sItemBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    invoke-static {v5, v10, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 283
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 284
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 287
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v19, v0

    move-object/from16 v20, v6

    move-object/from16 v22, v13

    move-object/from16 v23, v7

    move-wide/from16 v24, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    invoke-static/range {v19 .. v25}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    goto/16 :goto_0

    .line 288
    .end local v21    # "uri":Landroid/net/Uri;
    .end local v40    # "sItemBundle":Landroid/os/Bundle;
    :cond_10
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_FILEPATH"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 290
    const-string v5, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 291
    .local v26, "filePath":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/Bundle;

    .line 292
    .restart local v40    # "sItemBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const/4 v11, 0x1

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v5, v10, v0, v1, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 293
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 294
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 297
    :cond_11
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v10, 0x0

    move-object/from16 v0, v26

    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 299
    .restart local v21    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move-object/from16 v0, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v5, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v36

    .line 301
    .local v36, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v23, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v24, v6

    move-object/from16 v25, v13

    move-object/from16 v29, v7

    move-wide/from16 v30, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    invoke-static/range {v23 .. v31}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    goto/16 :goto_0

    .line 303
    .end local v21    # "uri":Landroid/net/Uri;
    .end local v26    # "filePath":Ljava/lang/String;
    .end local v36    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v40    # "sItemBundle":Landroid/os/Bundle;
    :cond_12
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 305
    const-string v5, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 306
    .restart local v26    # "filePath":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_TITLE"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 307
    .local v42, "title":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 309
    .local v37, "mimeType":Ljava/lang/String;
    move-object/from16 v4, v26

    .line 310
    .local v4, "absFilePath":Ljava/lang/String;
    const-string v5, "file://"

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 311
    const/4 v5, 0x7

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 314
    :cond_13
    if-nez v37, :cond_14

    .line 315
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/utility/HttpMimeUtils;->parseMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 318
    :cond_14
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, v37

    invoke-static {v5, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 320
    .restart local v21    # "uri":Landroid/net/Uri;
    if-eqz v21, :cond_16

    .line 323
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/Bundle;

    .line 324
    .restart local v40    # "sItemBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    invoke-static {v5, v10, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 325
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 326
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 330
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v19, v0

    move-object/from16 v20, v6

    move-object/from16 v22, v13

    move-object/from16 v23, v7

    move-wide/from16 v24, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    invoke-static/range {v19 .. v25}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    goto/16 :goto_0

    .line 333
    .end local v40    # "sItemBundle":Landroid/os/Bundle;
    :cond_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "ImageViewerActionWorker"

    const-string v11, "uri is null case"

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/Bundle;

    .line 335
    .restart local v40    # "sItemBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const/4 v11, 0x1

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    invoke-static {v5, v10, v0, v1, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 336
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v40

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 337
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    iget-object v10, v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 341
    :cond_17
    new-instance v36, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct/range {v36 .. v36}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 342
    .restart local v36    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz v42, :cond_18

    .end local v42    # "title":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, v42

    move-object/from16 v1, v36

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 343
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v23, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v24, v6

    move-object/from16 v25, v13

    move-object/from16 v29, v7

    move-wide/from16 v30, v8

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    invoke-static/range {v23 .. v31}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    goto/16 :goto_0

    .line 342
    .restart local v42    # "title":Ljava/lang/String;
    :cond_18
    const-string v42, "local content"

    goto :goto_2

    .line 347
    .end local v4    # "absFilePath":Ljava/lang/String;
    .end local v21    # "uri":Landroid/net/Uri;
    .end local v26    # "filePath":Ljava/lang/String;
    .end local v36    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v37    # "mimeType":Ljava/lang/String;
    .end local v40    # "sItemBundle":Landroid/os/Bundle;
    .end local v42    # "title":Ljava/lang/String;
    :cond_19
    const-string v5, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 349
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestPlayerState(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    invoke-static {v5, v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto/16 :goto_0

    .line 350
    :cond_1a
    const-string v5, "ACTION_IMAGE_VIEWER_PREPARE"

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 351
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    move-object/from16 v0, v43

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z
    invoke-static {v5, v0, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 353
    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    iget-object v5, v5, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " invalid action: "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v32

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
