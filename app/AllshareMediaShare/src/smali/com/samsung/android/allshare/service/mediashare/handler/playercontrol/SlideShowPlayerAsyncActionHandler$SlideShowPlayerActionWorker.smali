.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "SlideShowPlayerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SlideShowPlayerActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$1;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetBGMVolume(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->responseFail(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestShow(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestStop(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/ArrayList;

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestSetBGMList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method private requestAddAVSListener(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 455
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 456
    return-void
.end method

.method private requestSetBGMList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 202
    .local v4, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 203
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v6, 0x0

    .line 205
    .local v6, "result":I
    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 208
    .local v3, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->createAllShareObjectList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v7, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 210
    .local v0, "bgmList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_1

    .line 211
    :cond_0
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v1, v7, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 215
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 263
    :goto_0
    return-void

    .line 221
    :cond_1
    const-string v7, "X_SetBGM"

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 222
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v7

    invoke-virtual {v7, p1, v0, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setBGMListSlideshow(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    .line 225
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    iget v8, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v7, v6, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z

    move-result v7

    if-nez v7, :cond_2

    .line 226
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v1, v7, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 230
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 237
    :cond_2
    iget v2, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 238
    .local v2, "key":I
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestAddAVSListener(I)V

    .line 239
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-wide/16 v8, 0x7530

    invoke-virtual {v3, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v5

    .line 245
    .local v5, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 249
    :cond_3
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v1, v7, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 253
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :goto_1
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 255
    :cond_4
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v1, v7, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 259
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private requestSetBGMVolume(Ljava/lang/String;I)V
    .locals 10
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "level"    # I

    .prologue
    .line 153
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 154
    .local v3, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 157
    .local v5, "result":I
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 159
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v7, "X_SetBGMVolume"

    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 161
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    invoke-virtual {v7, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->convertVolumeLevelForSPC(I)I

    move-result v6

    .line 163
    .local v6, "volume":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v7

    invoke-virtual {v7, p1, v6, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setBGMVolumeSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v5

    .line 166
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    iget v8, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v7, v5, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z

    move-result v7

    if-nez v7, :cond_0

    .line 167
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v7, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v0, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 198
    :goto_0
    return-void

    .line 176
    :cond_0
    iget v1, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 177
    .local v1, "key":I
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestAddAVSListener(I)V

    .line 178
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-wide/16 v8, 0x7530

    invoke-virtual {v2, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v4

    .line 184
    .local v4, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 188
    :cond_1
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v7, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v0, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 192
    :cond_2
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v7, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v0, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private requestSetList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 18
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "albumTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 267
    .local v8, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    const/16 v17, 0x0

    .line 269
    .local v17, "result":I
    new-instance v13, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 272
    .local v13, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    move-object/from16 v0, p3

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->dividePageFromBundleList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v14

    .line 274
    .local v14, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Landroid/os/Bundle;>;>;"
    if-eqz v14, :cond_0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_2

    .line 275
    :cond_0
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 277
    .local v9, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v2, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 282
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 365
    .end local v9    # "bundle":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 288
    :cond_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 290
    .local v12, "len":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v12, :cond_1

    .line 291
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 292
    .restart local v9    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/ArrayList;

    .line 295
    .local v15, "pageUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->createAllShareObjectList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v2, v15}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 297
    .local v5, "slideList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_4

    .line 298
    :cond_3
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v2, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 303
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    if-nez v10, :cond_1

    .line 307
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 312
    :cond_4
    const-string v2, "X_SetPlayList"

    invoke-virtual {v13, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 314
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    add-int/lit8 v3, v10, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setListSlideshow(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v17

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    iget v3, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v0, v17

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v2, v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z

    move-result v2

    if-nez v2, :cond_5

    .line 319
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v2, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 324
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    if-nez v10, :cond_1

    .line 328
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 333
    :cond_5
    iget v11, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 334
    .local v11, "key":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestAddAVSListener(I)V

    .line 335
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    const-wide/16 v2, 0x7530

    invoke-virtual {v13, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v16

    .line 341
    .local v16, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    if-eqz v16, :cond_6

    invoke-virtual {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 346
    :cond_6
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v2, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 351
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :goto_2
    if-nez v10, :cond_7

    .line 363
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 290
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 353
    :cond_8
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v2, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    move-object/from16 v0, p3

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 358
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private requestShow(Ljava/lang/String;I)V
    .locals 8
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "interval"    # I

    .prologue
    .line 410
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 411
    .local v3, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 412
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 414
    .local v5, "result":I
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 416
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v6, "X_StartSlideShow"

    invoke-virtual {v2, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 417
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v6

    invoke-virtual {v6, p1, p2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->startSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v5

    .line 420
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v6, v5, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z

    move-result v6

    if-nez v6, :cond_0

    .line 421
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v6, "BUNDLE_INT_SLIDESHOW_INTERVAL"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 423
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 452
    :goto_0
    return-void

    .line 430
    :cond_0
    iget v1, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 431
    .local v1, "key":I
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestAddAVSListener(I)V

    .line 432
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    const-wide/16 v6, 0x7530

    invoke-virtual {v2, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v4

    .line 438
    .local v4, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 442
    :cond_1
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v6, "BUNDLE_INT_SLIDESHOW_INTERVAL"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 444
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 446
    :cond_2
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const-string v6, "BUNDLE_INT_SLIDESHOW_INTERVAL"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 448
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private requestStop(Ljava/lang/String;)V
    .locals 8
    .param p1, "dmrUDN"    # Ljava/lang/String;

    .prologue
    .line 368
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 369
    .local v3, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 370
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 372
    .local v5, "result":I
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 374
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v6, "X_StopSlideShow"

    invoke-virtual {v2, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 375
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1200()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v6

    invoke-virtual {v6, p1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stopSlideshow(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v5

    .line 378
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v6, v5, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;II)Z

    move-result v6

    if-nez v6, :cond_0

    .line 379
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 407
    :goto_0
    return-void

    .line 387
    :cond_0
    iget v1, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 388
    .local v1, "key":I
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->requestAddAVSListener(I)V

    .line 389
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    const-wide/16 v6, 0x7530

    invoke-virtual {v2, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v4

    .line 395
    .local v4, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 399
    :cond_1
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 402
    :cond_2
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private responseFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "responseFail"

    invoke-static {v1, v2, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 147
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 150
    return-void
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 92
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/SlideShowPlayerAsyncActionHandler$SlideShowPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 141
    return-void
.end method
