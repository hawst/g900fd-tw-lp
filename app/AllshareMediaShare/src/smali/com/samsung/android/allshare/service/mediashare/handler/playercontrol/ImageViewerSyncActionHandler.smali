.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.source "ImageViewerSyncActionHandler.java"


# static fields
.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# instance fields
.field private requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 36
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .line 40
    return-void
.end method

.method private getDeviceByID(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .locals 4
    .param p2, "deviceID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;"
        }
    .end annotation

    .prologue
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    const/4 v2, 0x0

    .line 185
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v2

    .line 194
    :goto_0
    return-object v0

    .line 189
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 190
    .local v0, "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->deviceID:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_3
    move-object v0, v2

    .line 194
    goto :goto_0
.end method

.method private getDeviceByUDN(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .locals 4
    .param p2, "udn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;"
        }
    .end annotation

    .prologue
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    const/4 v2, 0x0

    .line 198
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v2

    .line 207
    :goto_0
    return-object v0

    .line 202
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 203
    .local v0, "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_3
    move-object v0, v2

    .line 207
    goto :goto_0
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEWER_STATE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 48
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SET_VIEW_CONTROLLER_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 49
    const-string v0, "ACTION_IMAGE_VIEWER_IS_SUPPORT_REDIRECT_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 50
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEW_CONTROLLER2_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 51
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_SLIDESHOWPLAYER_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method protected finiHandler()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method protected initHandler()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 73
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 15
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 83
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "actionID":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v9

    .line 85
    .local v9, "reqb":Landroid/os/Bundle;
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 86
    .local v7, "name":Ljava/lang/String;
    const-string v13, "BUNDLE_STRING_ID"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 88
    .local v12, "udn":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v2, "b":Landroid/os/Bundle;
    const-string v13, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEWER_STATE_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_1

    .line 92
    const-string v13, "BUNDLE_STRING_NAME"

    invoke-virtual {v2, v13, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v13, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v13}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    iput-object v13, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .line 94
    new-instance v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct {v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 95
    .local v11, "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    sget-object v13, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    iget-object v14, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->requestID:Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-virtual {v13, v12, v11, v14}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v10

    .line 96
    .local v10, "result":I
    if-eqz v10, :cond_0

    .line 97
    const-string v13, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    sget-object v14, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v13, "BUNDLE_ENUM_ERROR"

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v13

    .line 181
    .end local v10    # "result":I
    .end local v11    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    :goto_0
    return-object v13

    .line 103
    .restart local v10    # "result":I
    .restart local v11    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    :cond_0
    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 104
    .local v8, "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    iget v13, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    packed-switch v13, :pswitch_data_0

    .line 117
    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 119
    :goto_1
    const-string v13, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    invoke-virtual {v8}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v13

    goto :goto_0

    .line 106
    :pswitch_0
    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 107
    goto :goto_1

    .line 110
    :pswitch_1
    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 111
    goto :goto_1

    .line 114
    :pswitch_2
    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 115
    goto :goto_1

    .line 121
    .end local v8    # "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .end local v10    # "result":I
    .end local v11    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    :cond_1
    const-string v13, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SET_VIEW_CONTROLLER_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_3

    .line 122
    const-string v13, "BUNDLE_STRING_DEVICE_ID"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, "deviceId":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v13

    const/16 v14, 0x9

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 125
    .local v0, "RCRList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-direct {p0, v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->getDeviceByID(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v4

    .line 126
    .local v4, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-eqz v4, :cond_2

    .line 127
    const-string v13, "BUNDLE_INT_TV_WIDTH_RESOLUTION"

    iget v14, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->TVWidthResolution:I

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    const-string v13, "BUNDLE_INT_TV_HEIGHT_RESOLUTION"

    iget v14, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->TVHeightResolution:I

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    const-string v13, "BUNDLE_BOOLEAN_ZOOMABLE"

    iget-boolean v14, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isZoomable:Z

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    const-string v13, "BUNDLE_BOOLEAN_ROTATABLE"

    iget-boolean v14, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isRotatable:Z

    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 181
    .end local v0    # "RCRList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    .end local v3    # "deviceId":Ljava/lang/String;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_2
    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v13

    goto :goto_0

    .line 134
    :cond_3
    const-string v13, "ACTION_IMAGE_VIEWER_IS_SUPPORT_REDIRECT_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 135
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 137
    .local v5, "imageviewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-direct {p0, v5, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->getDeviceByUDN(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v4

    .line 139
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v6, 0x0

    .line 141
    .local v6, "isSupport":Z
    if-eqz v4, :cond_4

    .line 142
    iget-boolean v13, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isWebURLPlayable:Z

    if-eqz v13, :cond_5

    .line 143
    const/4 v6, 0x1

    .line 150
    :cond_4
    :goto_3
    const-string v13, "BUNDLE_BOOLEAN_SUPPORT_REDIRECT"

    invoke-virtual {v2, v13, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 144
    :cond_5
    iget-object v13, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v13, :cond_4

    iget-object v13, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    const-string v14, "Windows Media Player"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 146
    const/4 v6, 0x1

    goto :goto_3

    .line 151
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v5    # "imageviewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    .end local v6    # "isSupport":Z
    :cond_6
    const-string v13, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEW_CONTROLLER2_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 152
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 154
    .restart local v5    # "imageviewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-direct {p0, v5, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->getDeviceByUDN(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v4

    .line 156
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v6, 0x0

    .line 158
    .restart local v6    # "isSupport":Z
    if-eqz v4, :cond_7

    iget-boolean v13, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSPCImageZoomable:Z

    if-eqz v13, :cond_7

    .line 159
    const/4 v6, 0x1

    .line 164
    :goto_4
    const-string v13, "BUNDLE_BOOLEAN_SUPPORT_SPC_IMAGEZOOM"

    invoke-virtual {v2, v13, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 161
    :cond_7
    const/4 v6, 0x0

    goto :goto_4

    .line 165
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v5    # "imageviewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    .end local v6    # "isSupport":Z
    :cond_8
    const-string v13, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_SLIDESHOWPLAYER_SYNC"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 166
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 168
    .restart local v5    # "imageviewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-direct {p0, v5, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerSyncActionHandler;->getDeviceByUDN(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v4

    .line 170
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v6, 0x0

    .line 172
    .restart local v6    # "isSupport":Z
    if-eqz v4, :cond_9

    iget-boolean v13, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSPCSlideShowSupport:Z

    if-eqz v13, :cond_9

    .line 173
    const/4 v6, 0x1

    .line 178
    :goto_5
    const-string v13, "BUNDLE_BOOLEAN_SUPPORT_SPC_SLIDESHOW"

    invoke-virtual {v2, v13, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 175
    :cond_9
    const/4 v6, 0x0

    goto :goto_5

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
