.class public Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
.super Ljava/lang/Object;
.source "SyncBrowseHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;


# static fields
.field public static final BROWSE:Ljava/lang/String; = "Browse"

.field private static final mTAG:Ljava/lang/String; = "SyncBrowseHandler"

.field private static mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;


# instance fields
.field private isReceived:Z

.field private item:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

.field private final mLock:Ljava/lang/Object;

.field private request_id:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->item:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mLock:Ljava/lang/Object;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->isReceived:Z

    .line 32
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z

    .line 33
    return-void
.end method

.method private waitForGetObjectListResponse()V
    .locals 6

    .prologue
    .line 62
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 63
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->isReceived:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 65
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mLock:Ljava/lang/Object;

    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 72
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "SyncBrowseHandler"

    const-string v3, "waitForGetObjectListResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Interrupted Exception in readAdnFileAndWait "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 9
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "object_id"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 75
    const-string v0, "SyncBrowseHandler"

    const-string v1, "browseMetadata"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "udn: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " object id: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iput-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->item:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 77
    iput-boolean v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->isReceived:Z

    .line 78
    new-instance v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 79
    .local v6, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->browse(Ljava/lang/String;Ljava/lang/String;IIILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v7

    .line 80
    .local v7, "error":I
    if-eqz v7, :cond_0

    .line 81
    const-string v0, "SyncBrowseHandler"

    const-string v1, "browseMetadata"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " browse metadata filed! error code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 88
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget v0, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->request_id:I

    .line 85
    const-string v0, "SyncBrowseHandler"

    const-string v1, "browseMetadata"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "browse metadata action has been sent! request id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->request_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->waitForGetObjectListResponse()V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->item:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    goto :goto_0
.end method

.method public discard()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPGetObjectListListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetObjectListListener;)Z

    .line 37
    return-void
.end method

.method public onGetObjectListResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;)V
    .locals 4
    .param p1, "request_id"    # I
    .param p2, "action_name"    # Ljava/lang/String;
    .param p3, "error_code"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;

    .prologue
    .line 42
    const-string v0, "SyncBrowseHandler"

    const-string v1, "onGetObjectListResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " action name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->request_id:I

    if-ne p1, v0, :cond_0

    const-string v0, "Browse"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 46
    :cond_0
    const-string v0, "SyncBrowseHandler"

    const-string v1, "onGetObjectListResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "this response is not expected response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->request_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_1
    :goto_0
    return-void

    .line 50
    :cond_2
    if-eqz p4, :cond_1

    iget-object v0, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->objects:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->objects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 52
    iget-object v0, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/GetObjectsResponse;->objects:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->item:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 54
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->isReceived:Z

    .line 56
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 57
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
