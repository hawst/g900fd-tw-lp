.class public Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;
.super Ljava/lang/Object;
.source "DmsObjectMap.java"


# static fields
.field private static instance:Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;


# instance fields
.field private dmsObjectMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;

    invoke-direct {v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->instance:Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->dmsObjectMap:Ljava/util/Map;

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->instance:Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;

    return-object v0
.end method


# virtual methods
.method public getObject(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "objectID"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->dmsObjectMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 40
    .local v0, "objectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    if-nez v0, :cond_0

    .line 41
    const/4 v1, 0x0

    .line 44
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    goto :goto_0
.end method

.method public updateObjectsMap(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "udn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->dmsObjectMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 27
    .local v2, "objectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    if-nez v2, :cond_0

    .line 28
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "objectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 29
    .restart local v2    # "objectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DmsObjectMap;->dmsObjectMap:Ljava/util/Map;

    invoke-interface {v3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 33
    .local v1, "item":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 36
    .end local v1    # "item":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_1
    return-void
.end method
