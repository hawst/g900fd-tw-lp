.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;
.super Ljava/lang/Object;
.source "RelayAuthentication.java"


# static fields
.field public static HTTP_HEADER:Ljava/lang/String;


# instance fields
.field private mHeader:Ljava/lang/String;

.field private mToken:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "HTTP_HEADER"

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->HTTP_HEADER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mType:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mHeader:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mToken:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mType:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mHeader:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mToken:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mHeader:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public setAuthenticationHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 32
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->HTTP_HEADER:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mType:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mHeader:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/instantplayhandler/RelayAuthentication;->mToken:Ljava/lang/String;

    .line 35
    return-void
.end method
