.class public Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field private static mNetworkInterfaceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->mNetworkInterfaceList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocalAddresses()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v3

    .line 37
    .local v3, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 50
    .end local v3    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_0
    :goto_0
    return-object v4

    .line 40
    .restart local v3    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/NetworkInterface;

    .line 41
    .local v2, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 43
    .local v1, "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 44
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 47
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v2    # "ni":Ljava/net/NetworkInterface;
    .end local v3    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private static getNetworkInterfaceList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->mNetworkInterfaceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 55
    const/4 v2, 0x0

    .line 57
    .local v2, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 61
    :goto_0
    if-nez v2, :cond_0

    .line 62
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->mNetworkInterfaceList:Ljava/util/ArrayList;

    .line 70
    :goto_1
    return-object v3

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/net/SocketException;
    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 65
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/NetworkInterface;

    .line 66
    .local v1, "ni":Ljava/net/NetworkInterface;
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->mNetworkInterfaceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 70
    .end local v1    # "ni":Ljava/net/NetworkInterface;
    :cond_1
    sget-object v3, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->mNetworkInterfaceList:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method public static isLocalIP(Ljava/lang/String;)Z
    .locals 5
    .param p0, "ip"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 16
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/utility/NetworkUtils;->getLocalAddresses()Ljava/util/Collection;

    move-result-object v2

    .line 17
    .local v2, "localAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-nez v2, :cond_1

    .line 28
    :cond_0
    :goto_0
    return v3

    .line 20
    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 23
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 24
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 25
    const/4 v3, 0x1

    goto :goto_0
.end method
