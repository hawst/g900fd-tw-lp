.class public Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;
.super Ljava/lang/Object;
.source "SyncCreateHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;


# static fields
.field public static final CREATEOBJECT:Ljava/lang/String; = "CreateObject"

.field public static final IMPORTRESOURCE:Ljava/lang/String; = "ImportResource"

.field private static final TAG:Ljava/lang/String; = "SyncCreateHandler"

.field private static actionMonitorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private static mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;


# instance fields
.field mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

.field mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field mUdn:Ljava/lang/String;

.field private transfer_id:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mUdn:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetTransportStatusListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;)Z

    .line 45
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPObjectOperationListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;)Z

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "udn"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mUdn:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .line 51
    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mUdn:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPGetTransportStatusListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;)Z

    .line 53
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->addMSCPObjectOperationListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;)Z

    .line 54
    return-void
.end method


# virtual methods
.method public createObject(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/lang/String;
    .locals 11
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "source_item"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    const/4 v6, 0x0

    .line 135
    if-nez p2, :cond_0

    .line 136
    const-string v7, "SyncCreateHandler"

    const-string v8, "createObject"

    const-string v9, "source object is null!"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    return-object v6

    .line 139
    :cond_0
    const-string v7, "SyncCreateHandler"

    const-string v8, "createObject"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " destination dms: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ource object id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 142
    .local v3, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 143
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v7, "CreateObject"

    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 145
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    const-string v8, "DLNA.ORG_AnyContainer"

    invoke-virtual {v7, p1, v8, p2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->creatObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 148
    .local v0, "error":I
    if-eqz v0, :cond_1

    .line 149
    const-string v7, "SyncCreateHandler"

    const-string v8, "createObject"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createObject  failed: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_1
    iget v1, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 153
    .local v1, "key":I
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const-wide/16 v8, 0x7530

    invoke-virtual {v2, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v5

    .line 155
    .local v5, "result":Z
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 157
    :cond_2
    const-string v7, "SyncCreateHandler"

    const-string v8, "createObject"

    const-string v9, "createObject timeout"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;

    .line 162
    .local v4, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;
    const-string v6, "SyncCreateHandler"

    const-string v7, "createObject"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "createObject response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v6, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->importUri:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public discard()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPGetTransportStatusListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPGetTransportStatusListener;)Z

    .line 58
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->removeMSCPObjectOperationListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper$IMSCPObjectOperationListener;)Z

    .line 59
    return-void
.end method

.method public importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "import_uri"    # Ljava/lang/String;
    .param p3, "source_uri"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 181
    const-string v7, "SyncCreateHandler"

    const-string v8, "importResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "destination dms: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " import uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  source uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 184
    .local v3, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 185
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v7, "ImportResource"

    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 186
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v7, p1, p2, p3, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 187
    .local v0, "error":I
    if-eqz v0, :cond_0

    .line 188
    const-string v7, "SyncCreateHandler"

    const-string v8, "importResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "importResource filed: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :goto_0
    return-object v6

    .line 191
    :cond_0
    iget v1, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 192
    .local v1, "key":I
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const-wide/16 v8, 0x7530

    invoke-virtual {v2, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v5

    .line 194
    .local v5, "result":Z
    sget-object v7, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 196
    :cond_1
    const-string v7, "SyncCreateHandler"

    const-string v8, "importResource"

    const-string v9, "importResource timeout"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;

    .line 201
    .local v4, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;
    const-string v6, "SyncCreateHandler"

    const-string v7, "importResource"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "importResource response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v6, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->importUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public onGetTransportStatusResponse(ILjava/lang/String;IJJ)V
    .locals 6
    .param p1, "transportId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "status"    # I
    .param p4, "size"    # J
    .param p6, "sentSize"    # J

    .prologue
    .line 86
    iget v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->transfer_id:I

    if-eq v2, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    const-string v2, "SyncCreateHandler"

    const-string v3, "onGetTransportStatusResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " transfered size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " total size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const/4 v1, 0x0

    .line 94
    .local v1, "isComplete":Z
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-eqz v2, :cond_0

    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v0, v2, p6, p7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 99
    const-string v2, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 100
    const/4 v2, 0x2

    if-ne p3, v2, :cond_3

    .line 101
    cmp-long v2, p6, p4

    if-ltz v2, :cond_2

    .line 102
    const/4 v1, 0x1

    .line 103
    const-string v2, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    :goto_1
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mContentItem:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->createAllShareObjectBundle(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 122
    const-string v2, "BUNDLE_STRING_CATEGORY"

    const-string v3, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mUdn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    if-nez v1, :cond_6

    .line 125
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v3, "com.sec.android.allshare.event.EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 105
    :cond_2
    const/4 v1, 0x0

    .line 106
    const-string v2, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 108
    :cond_3
    const/4 v2, 0x5

    if-eq p3, v2, :cond_4

    const/4 v2, 0x4

    if-ne p3, v2, :cond_5

    .line 111
    :cond_4
    const/4 v1, 0x0

    .line 112
    const-string v2, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 116
    :cond_5
    const/4 v1, 0x1

    .line 117
    const-string v2, "BUNDLE_BOOLEAN_RECEIVE_COMPLETED"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 128
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    const-string v3, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->notifyEvent2Subscriber(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->discard()V

    goto/16 :goto_0
.end method

.method public onObjectOperationResponse(ILjava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "info"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 63
    const-string v2, "SyncCreateHandler"

    const-string v3, "onObjectOperationResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " request id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " acion name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;

    invoke-direct {v1, v6, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;-><init>(ILjava/lang/String;)V

    .line 66
    .local v1, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->actionMonitorMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 67
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 70
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 71
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 72
    invoke-virtual {v0, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 73
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 76
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method

.method public postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "desUri"    # Ljava/lang/String;
    .param p2, "srcUri"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 168
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 169
    .local v0, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v2, p1, p2, p3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v1

    .line 170
    .local v1, "result":I
    iget v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    iput v2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->transfer_id:I

    .line 171
    return v1
.end method

.method public postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .locals 2
    .param p1, "desUri"    # Ljava/lang/String;
    .param p2, "srcUri"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "requestID"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    .prologue
    .line 175
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->mscpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MSCPWrapper;->postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v0

    .line 176
    .local v0, "result":I
    iget v1, p4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    iput v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncCreateHandler;->transfer_id:I

    .line 177
    return v0
.end method
