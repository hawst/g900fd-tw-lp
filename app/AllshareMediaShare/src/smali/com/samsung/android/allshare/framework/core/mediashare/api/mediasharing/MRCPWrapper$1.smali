.class final Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$1;
.super Landroid/os/Handler;
.source "MRCPWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 81
    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 83
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 84
    .local v1, "avsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    if-eqz v1, :cond_0

    .line 86
    iget v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->requestID:I

    .line 87
    .local v0, "avsrequestID":I
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$000()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;

    .line 88
    .local v3, "ls":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;
    if-eqz v3, :cond_0

    .line 89
    iget-object v9, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->actionName:Ljava/lang/String;

    iget v10, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->errorCode:I

    invoke-interface {v3, v0, v9, v10, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;->onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V

    .line 91
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sAVSListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$000()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 95
    .end local v0    # "avsrequestID":I
    .end local v1    # "avsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    .end local v3    # "ls":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;
    :pswitch_1
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;

    .line 96
    .local v6, "rcsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;
    if-eqz v6, :cond_0

    .line 98
    iget v5, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->requestID:I

    .line 99
    .local v5, "rcsrequestID":I
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$100()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;

    .line 100
    .local v2, "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;
    if-eqz v2, :cond_0

    .line 101
    iget-object v9, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->actionName:Ljava/lang/String;

    iget v10, v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->errorCode:I

    invoke-interface {v2, v5, v9, v10, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;->onRCSControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;)V

    .line 103
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sRCSListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$100()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 108
    .end local v2    # "l":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPRCSControlListener;
    .end local v5    # "rcsrequestID":I
    .end local v6    # "rcsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;
    :pswitch_2
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;

    .line 109
    .local v8, "wharesponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;
    if-eqz v8, :cond_0

    .line 111
    iget v7, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->requestID:I

    .line 112
    .local v7, "wharequestID":I
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$200()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;

    .line 113
    .local v4, "lw":Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;
    if-eqz v4, :cond_0

    .line 114
    iget-object v9, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->actionName:Ljava/lang/String;

    iget v10, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->errorCode:I

    invoke-interface {v4, v7, v9, v10, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;->onWHAControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;)V

    .line 116
    # getter for: Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->sWHAListener:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->access$200()Ljava/util/HashMap;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
