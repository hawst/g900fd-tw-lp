.class Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;
.super Ljava/lang/Object;
.source "PopupList.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->updatePopupLayoutParams()V
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$400(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mAnchorView:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupWidth:I
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList$3;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->mPopupHeight:I
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;->access$600(Lcom/samsung/android/allshare/service/mediashare/download/ui/PopupList;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    goto :goto_0
.end method
