.class public Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "ReceiverAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$1;,
        Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;
    }
.end annotation


# instance fields
.field mIsCancel:Z

.field private mUploadThreadlList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;

    .line 48
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 49
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 92
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 57
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 58
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 59
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method protected finiHandler()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method protected initHandler()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
