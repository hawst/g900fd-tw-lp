.class public Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
.super Ljava/lang/Object;
.source "AlbumDetails.java"


# instance fields
.field public album_title:Ljava/lang/String;

.field public artist:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public duration:Ljava/lang/String;

.field public genre:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "artist"    # Ljava/lang/String;
    .param p2, "genre"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;
    .param p5, "duration"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;->artist:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;->genre:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;->album_title:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;->date:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;->duration:Ljava/lang/String;

    .line 36
    return-void
.end method
