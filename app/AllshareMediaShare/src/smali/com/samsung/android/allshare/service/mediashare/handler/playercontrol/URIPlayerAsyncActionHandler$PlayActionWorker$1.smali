.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;
.super Ljava/lang/Object;
.source "URIPlayerAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v2}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v22

    .line 147
    .local v22, "bundle":Landroid/os/Bundle;
    if-nez v22, :cond_0

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "PlayActionWorker"

    const-string v12, "cvm bundle is null,return"

    invoke-static {v2, v9, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_0
    return-void

    .line 152
    :cond_0
    const-string v2, "BUNDLE_STRING_ID"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "udn":Ljava/lang/String;
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    .line 154
    .local v4, "itemBundle":Landroid/os/Bundle;
    if-nez v4, :cond_1

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "PlayActionWorker"

    const-string v12, "itemBundle is null,return"

    invoke-static {v2, v9, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_1
    const-string v2, "BUNDLE_STRING_WEB_PLAY_MODE"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 161
    .local v25, "playModeStr":Ljava/lang/String;
    if-eqz v25, :cond_5

    const-string v2, "REDIRECT"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v10, 0x1

    .line 163
    .local v10, "isRedirect":Z
    :goto_1
    const-string v2, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;

    .line 164
    .local v24, "itemUrl":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "PlayActionWorker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "itemUrl is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v9, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/16 v23, 0x0

    .line 166
    .local v23, "isHttps":Z
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v9, "https"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 167
    const/16 v23, 0x1

    .line 170
    :cond_2
    const-wide/16 v26, 0x0

    .line 171
    .local v26, "startingPosition":J
    const-string v2, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/media/ContentInfo;

    .line 174
    .local v5, "ci":Lcom/samsung/android/allshare/media/ContentInfo;
    if-eqz v5, :cond_6

    .line 175
    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v26

    .line 183
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->getMetadata(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v2, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v8

    .line 184
    .local v8, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->getAlbumDetails(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    invoke-static {v2, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    move-result-object v11

    .line 185
    .local v11, "albumDetails":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    const-wide/16 v6, 0x0

    .line 186
    .local v6, "position":J
    const-wide/16 v12, 0x0

    cmp-long v2, v26, v12

    if-lez v2, :cond_3

    .line 187
    const-wide/16 v12, 0x3e8

    div-long v6, v26, v12

    .line 189
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v2

    const-string v9, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_URI"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    const/4 v9, 0x4

    move/from16 v0, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->isOnlyDirectlyMode(Ljava/lang/String;IZ)Z
    invoke-static {v2, v3, v9, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 191
    const/4 v10, 0x1

    .line 193
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "PlayActionWorker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ACTION_AV_PLAYER_PLAY_URI : isRedirect = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v9, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->play(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;ZZLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)V

    goto/16 :goto_0

    .line 161
    .end local v5    # "ci":Lcom/samsung/android/allshare/media/ContentInfo;
    .end local v6    # "position":J
    .end local v8    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v10    # "isRedirect":Z
    .end local v11    # "albumDetails":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    .end local v23    # "isHttps":Z
    .end local v24    # "itemUrl":Landroid/net/Uri;
    .end local v26    # "startingPosition":J
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 179
    .restart local v5    # "ci":Lcom/samsung/android/allshare/media/ContentInfo;
    .restart local v10    # "isRedirect":Z
    .restart local v23    # "isHttps":Z
    .restart local v24    # "itemUrl":Landroid/net/Uri;
    .restart local v26    # "startingPosition":J
    :cond_6
    const-string v2, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v26

    goto :goto_2

    .line 198
    .restart local v6    # "position":J
    .restart local v8    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .restart local v11    # "albumDetails":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v2}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v2

    const-string v9, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    const/4 v9, 0x1

    move/from16 v0, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->isOnlyDirectlyMode(Ljava/lang/String;IZ)Z
    invoke-static {v2, v3, v9, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;IZ)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 201
    const/4 v10, 0x1

    .line 203
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    iget-object v2, v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "PlayActionWorker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ACTION_IMAGE_VIEWER_SHOW_URI : isRedirect = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v9, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    const/4 v9, 0x1

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->play(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;ZZLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)V

    goto/16 :goto_0

    .line 209
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object v13, v3

    move-object v14, v4

    move-object v15, v5

    move-wide/from16 v16, v6

    move-object/from16 v18, v8

    move-object/from16 v21, v11

    invoke-virtual/range {v12 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->play(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;ZZLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)V

    goto/16 :goto_0
.end method
