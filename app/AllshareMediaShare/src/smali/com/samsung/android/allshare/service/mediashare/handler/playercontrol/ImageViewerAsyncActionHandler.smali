.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "ImageViewerAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$1;,
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    }
.end annotation


# static fields
.field private static final AS_ACTION_ERROR_SETAVT:I = 0x2c1

.field private static deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

.field private static mPreparedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    .line 57
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 70
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$3800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$4900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    return-void
.end method

.method static synthetic access$5700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$5900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$6500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;

    return-object v0
.end method

.method private getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "uriOrPath"    # Ljava/lang/String;

    .prologue
    .line 954
    invoke-static {p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 958
    if-nez p0, :cond_1

    .line 972
    :cond_0
    :goto_0
    return-object v2

    .line 961
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 962
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 965
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 966
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 967
    .local v7, "filePath":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 968
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 969
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 970
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v2, v7

    .line 972
    goto :goto_0
.end method

.method private getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 933
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 935
    .local v2, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz p1, :cond_0

    .line 936
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->convertUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v1

    .line 939
    .local v1, "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    if-nez v1, :cond_1

    .line 950
    .end local v1    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_0
    :goto_0
    return-object v2

    .line 943
    .restart local v1    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_1
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->objectName:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 944
    iget-object v4, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->resources:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;

    .line 945
    .local v3, "res":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;
    iget-object v4, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareResource;->mimeType:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static isSupportSetNextAVT(Ljava/lang/String;)Z
    .locals 4
    .param p0, "dmrUdn"    # Ljava/lang/String;

    .prologue
    .line 976
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 977
    .local v0, "response":Landroid/os/Bundle;
    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    const-string v3, "AVTransport"

    invoke-virtual {v2, p0, v3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I

    .line 978
    const-string v2, "BUNDLE_STRING_SERVICE_DESCRIPTION"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 981
    .local v1, "serviceDescription":Ljava/lang/String;
    const-string v2, "SetNextAVTransportURI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 982
    const/4 v2, 0x1

    .line 984
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    .prologue
    .line 911
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 912
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "putResourceUriListToBundle"

    const-string v3, "(bundle == null || uri == null)"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    :goto_0
    return-void

    .line 916
    :cond_1
    invoke-static {p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ResourceUriHelper;->getResourceUriListWithDeviceIP(Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 918
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 922
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 923
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "putResourceUriListToBundle"

    const-string v3, "(bundle == null || uri == null)"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :goto_0
    return-void

    .line 927
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 928
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 929
    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 5
    .param p1, "req_id"    # J

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "cancelRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no operation for req_id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 77
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 78
    const-string v0, "ACTION_IMAGE_VIEWER_PREPARE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 79
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 80
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 81
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_FILEPATH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 82
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 85
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method protected finiHandler()V
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 103
    return-void
.end method

.method protected initHandler()V
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)Z

    .line 94
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 6
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v2, "onAVTControlResponse"

    invoke-virtual {p4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->get(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .line 130
    .local v0, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->validateActionName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setRequestID(I)V

    .line 133
    invoke-virtual {v0, p3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setErrorCode(I)V

    .line 134
    invoke-virtual {v0, p4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setResponse(Ljava/lang/Object;)V

    .line 135
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 136
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setUnknowError(Z)V

    .line 139
    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->notifyResponse()V

    goto :goto_0
.end method
