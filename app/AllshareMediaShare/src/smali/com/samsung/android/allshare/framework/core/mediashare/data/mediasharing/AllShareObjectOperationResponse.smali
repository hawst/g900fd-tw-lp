.class public Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;
.super Ljava/lang/Object;
.source "AllShareObjectOperationResponse.java"


# instance fields
.field public importUri:Ljava/lang/String;

.field public objectID:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->objectID:I

    .line 28
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->importUri:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " objectID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->objectID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " importUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObjectOperationResponse;->importUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
