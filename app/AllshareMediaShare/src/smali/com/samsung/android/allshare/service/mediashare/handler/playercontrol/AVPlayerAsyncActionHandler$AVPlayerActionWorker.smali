.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "AVPlayerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AVPlayerActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    .line 436
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    .line 437
    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 433
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p4, "x4"    # J
    .param p6, "x5"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 433
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p5, "x5"    # J
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-direct/range {p0 .. p8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->playMediaServerItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J

    .prologue
    .line 433
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # I

    .prologue
    .line 433
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Landroid/net/Uri;

    .prologue
    .line 433
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p7, "x7"    # J
    .param p9, "x8"    # Landroid/net/Uri;

    .prologue
    .line 433
    invoke-direct/range {p0 .. p9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayLocalContent(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestPlayerState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSkipDynamicBuffering(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    .locals 3
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "itemBundle"    # Landroid/os/Bundle;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 1511
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1523
    :cond_0
    :goto_0
    return v1

    .line 1514
    :cond_1
    if-eqz p2, :cond_0

    .line 1515
    const-string v2, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1517
    .local v0, "prepareUri":Landroid/net/Uri;
    invoke-virtual {p3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1518
    const/4 p2, 0x0

    .line 1519
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private canRequestNext(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "itemBundle"    # Landroid/os/Bundle;
    .param p3, "itemInfo"    # Ljava/lang/String;
    .param p4, "isLocal"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1528
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1542
    :cond_0
    :goto_0
    return v1

    .line 1531
    :cond_1
    const/4 v0, 0x0

    .line 1532
    .local v0, "prepareInfo":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 1533
    const-string v2, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1537
    :goto_1
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1538
    const/4 p2, 0x0

    .line 1539
    const/4 v1, 0x1

    goto :goto_0

    .line 1535
    :cond_2
    const-string v2, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 701
    if-nez p1, :cond_1

    .line 715
    :cond_0
    :goto_0
    return-object v2

    .line 704
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 705
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 708
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 709
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 712
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 713
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 714
    .local v7, "str":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v2, v7

    .line 715
    goto :goto_0
.end method

.method private playMediaServerItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;Ljava/lang/String;)V
    .locals 29
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "dmrUDN"    # Ljava/lang/String;
    .param p4, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p5, "startingPosition"    # J
    .param p7, "path"    # Ljava/lang/String;
    .param p8, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "playMediaServerItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " filepath : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " deviceName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, p3

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v22

    .line 1223
    .local v22, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1226
    :try_start_0
    new-instance v25, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v25 .. v25}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1227
    .local v25, "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v28, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct/range {v28 .. v28}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 1228
    .local v28, "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v0, p3

    move-object/from16 v1, v28

    move-object/from16 v2, v25

    invoke-virtual {v4, v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 1229
    move-object/from16 v0, v28

    iget v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    if-eqz v4, :cond_0

    .line 1230
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSyncStop(Ljava/lang/String;)V

    .line 1232
    :cond_0
    sget-object v24, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 1233
    .local v24, "error":Lcom/samsung/android/allshare/ERROR;
    const-wide/16 v4, 0x0

    cmp-long v4, p5, v4

    if-lez v4, :cond_1

    move-wide/from16 v8, p5

    .line 1236
    .local v8, "position":J
    :goto_0
    new-instance v20, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1237
    .local v20, "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1238
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 1241
    new-instance v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1242
    .local v10, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    const-string v7, "REL_TIME"

    move-object/from16 v5, p3

    move-object/from16 v6, p2

    move-object/from16 v11, p8

    invoke-virtual/range {v4 .. v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I

    move-result v27

    .line 1244
    .local v27, "retValue":I
    if-eqz v27, :cond_2

    .line 1245
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "playMediaServerItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "play to dmr failed, result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    new-instance v21, Landroid/os/Bundle;

    invoke-direct/range {v21 .. v21}, Landroid/os/Bundle;-><init>()V

    .line 1247
    .local v21, "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1248
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1318
    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1320
    :goto_1
    return-void

    .line 1233
    .end local v8    # "position":J
    .end local v10    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v20    # "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .end local v21    # "bundle":Landroid/os/Bundle;
    .end local v27    # "retValue":I
    :cond_1
    const-wide/16 v8, 0x0

    goto :goto_0

    .line 1254
    .restart local v8    # "position":J
    .restart local v10    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .restart local v20    # "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .restart local v27    # "retValue":I
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v5, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1255
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    iget v5, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1256
    const-wide/32 v4, 0xea60

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v26

    .line 1257
    .local v26, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    iget v5, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 1263
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v23

    .line 1264
    .local v23, "ec":I
    if-eqz v26, :cond_4

    if-eqz v23, :cond_4

    .line 1265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "playMediaServerItem"

    const-string v6, "try again....."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    new-instance v20, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .end local v20    # "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1269
    .restart local v20    # "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1270
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 1273
    new-instance v18, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1274
    .local v18, "retryRequestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSyncStop(Ljava/lang/String;)V

    .line 1275
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v12

    const-string v15, "REL_TIME"

    move-object/from16 v13, p3

    move-object/from16 v14, p2

    move-wide/from16 v16, v8

    move-object/from16 v19, p8

    invoke-virtual/range {v12 .. v19}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I

    move-result v27

    .line 1278
    if-eqz v27, :cond_3

    .line 1279
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "playMediaServerItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "play to dmr failed, result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    new-instance v21, Landroid/os/Bundle;

    invoke-direct/range {v21 .. v21}, Landroid/os/Bundle;-><init>()V

    .line 1281
    .restart local v21    # "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1282
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318
    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1

    .line 1288
    .end local v21    # "bundle":Landroid/os/Bundle;
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v18

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1289
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    move-object/from16 v0, v18

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1290
    const-wide/32 v4, 0xea60

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 1291
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    move-object/from16 v0, v18

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 1297
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v23

    .line 1301
    .end local v18    # "retryRequestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$7900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v24

    .line 1304
    new-instance v21, Landroid/os/Bundle;

    invoke-direct/range {v21 .. v21}, Landroid/os/Bundle;-><init>()V

    .line 1305
    .restart local v21    # "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1306
    const-string v4, "BUNDLE_ENUM_ERROR"

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    if-eqz p4, :cond_5

    .line 1308
    const-string v4, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1314
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    invoke-static {v4, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    .line 1315
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1316
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setNotified(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1318
    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1

    .line 1310
    :cond_5
    :try_start_3
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v21

    move-wide/from16 v1, p5

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1318
    .end local v8    # "position":J
    .end local v10    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v20    # "actionResponseMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .end local v21    # "bundle":Landroid/os/Bundle;
    .end local v23    # "ec":I
    .end local v24    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v25    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v26    # "result":Z
    .end local v27    # "retValue":I
    .end local v28    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4
.end method

.method private requestAVControl(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 17
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "dmrUDN"    # Ljava/lang/String;
    .param p3, "position"    # J

    .prologue
    .line 1104
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 1106
    new-instance v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1107
    .local v9, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1108
    .local v3, "bundle":Landroid/os/Bundle;
    const/4 v11, 0x0

    .line 1110
    .local v11, "result":I
    new-instance v8, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1112
    .local v8, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v13, "GetMediaInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1114
    const-string v13, "BUNDLE_LONG_DURATION"

    const-wide/16 v14, -0x1

    invoke-virtual {v3, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1115
    const-string v13, "GetMediaInfo"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1116
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getMediaInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    .line 1159
    :cond_0
    :goto_0
    const-string v13, "GetPlayPosition"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 1160
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "requestAVControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "complete..action["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] position["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-wide/from16 v0, p3

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] to "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " then result["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] requestID["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v14, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v13, v11, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;II)Z

    move-result v13

    if-nez v13, :cond_a

    .line 1168
    const-string v13, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v3, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    const-string v13, "BUNDLE_ENUM_ERROR"

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1215
    :goto_1
    return-void

    .line 1117
    :cond_2
    const-string v13, "GetPlayPosition"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1119
    const-string v13, "BUNDLE_LONG_POSITION"

    const-wide/16 v14, -0x1

    invoke-virtual {v3, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1120
    const-string v13, "GetPlayPosition"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1121
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayPositionInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1122
    :cond_3
    const-string v13, "Stop"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1123
    const-string v13, "Stop"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1124
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1125
    :cond_4
    const-string v13, "Pause"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1126
    const-string v13, "Pause"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1127
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->pause(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1128
    :cond_5
    const-string v13, "Resume"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1130
    const-string v13, "Play"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1132
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->resume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1133
    :cond_6
    const-string v13, "Seek"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1134
    const-string v13, "Seek"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1135
    const-wide/16 v14, 0x0

    cmp-long v13, v14, p3

    if-eqz v13, :cond_7

    .line 1136
    const-wide/16 v14, 0x3e8

    div-long p3, p3, v14

    .line 1140
    :cond_7
    const-string v13, "BUNDLE_LONG_POSITION"

    move-wide/from16 v0, p3

    invoke-virtual {v3, v13, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1143
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    const-string v14, "AVTransport"

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I

    move-result v11

    .line 1144
    const-string v13, "BUNDLE_STRING_SERVICE_DESCRIPTION"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1147
    .local v12, "serviceDescription":Ljava/lang/String;
    const-string v13, "A_ARG_TYPE_SeekMode"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1148
    const-string v13, "REL_TIME"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 1149
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    const-string v14, "REL_TIME"

    move-wide/from16 v0, p3

    long-to-int v15, v0

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v15, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1150
    :cond_8
    const-string v13, "ABS_TIME"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 1151
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    const-string v14, "ABS_TIME"

    move-wide/from16 v0, p3

    long-to-int v15, v0

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v15, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1152
    :cond_9
    const-string v13, "TRACK_NR"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1153
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v13

    const-string v14, "TRACK_NR"

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v15, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->seek(Ljava/lang/String;Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v11

    goto/16 :goto_0

    .line 1176
    .end local v12    # "serviceDescription":Ljava/lang/String;
    :cond_a
    iget v7, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 1177
    .local v7, "key":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v13, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1178
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v13

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1181
    const-wide/16 v14, 0x7530

    invoke-virtual {v8, v14, v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v10

    .line 1184
    .local v10, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v13

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1187
    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v6

    .line 1188
    .local v6, "errorCode":I
    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 1191
    .local v2, "avsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v13

    const/16 v14, 0x25e

    if-ne v13, v14, :cond_b

    .line 1192
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "requestAVControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mRequestID :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " return value: 606 (Permission not allowed)"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    const-string v13, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v3, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const-string v13, "BUNDLE_ENUM_ERROR"

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1196
    :cond_b
    if-eqz v10, :cond_c

    invoke-virtual {v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v13

    if-eqz v13, :cond_d

    .line 1197
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "requestAVControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mRequestID :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " fail"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    const-string v13, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v3, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    const-string v13, "BUNDLE_ENUM_ERROR"

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1202
    :cond_d
    const-string v13, "GetMediaInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 1203
    iget-object v13, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->duration:Ljava/lang/String;

    invoke-static {v13}, Lcom/samsung/android/allshare/service/mediashare/utility/TimeUtil;->getTimeLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1204
    .local v4, "duration":J
    const-string v13, "BUNDLE_LONG_DURATION"

    invoke-virtual {v3, v13, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1211
    .end local v4    # "duration":J
    :cond_e
    :goto_3
    const-string v13, "BUNDLE_STRING_ID"

    iget-object v14, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->dmrUDN:Ljava/lang/String;

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    const-string v13, "BUNDLE_ENUM_ERROR"

    sget-object v14, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1205
    :cond_f
    const-string v13, "GetPlayPosition"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 1206
    const-string v13, "BUNDLE_LONG_POSITION"

    iget v14, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->position:I

    int-to-long v14, v14

    invoke-virtual {v3, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_3

    .line 1207
    :cond_10
    const-string v13, "Seek"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 1208
    const-string v13, "BUNDLE_LONG_POSITION"

    move-wide/from16 v0, p3

    invoke-virtual {v3, v13, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_3
.end method

.method private requestAddWhaListener(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 1547
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPWHAControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPWHAControlListener;)V

    .line 1548
    return-void
.end method

.method private requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 9
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "dmrUdn"    # Ljava/lang/String;

    .prologue
    .line 1442
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1443
    .local v2, "id":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1444
    .local v3, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v5, "Next"

    invoke-virtual {v3, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1445
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    invoke-virtual {v5, p2, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->nextPlay(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 1446
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestNextPlay"

    const-string v7, "Next action"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1448
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    iget v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    const-wide/16 v6, 0x7530

    invoke-virtual {v3, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v4

    .line 1454
    .local v4, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    iget v6, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1456
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1457
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestNextPlay"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Next"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has wrong response"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v6

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v1

    .line 1462
    .local v1, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    const-string v5, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470
    .end local v1    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_0
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1472
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1473
    return-void

    .line 1465
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$10000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestNextPlay"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Next"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestPlayLocalContent(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLandroid/net/Uri;)V
    .locals 29
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "dmrUDN"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "mimeType"    # Ljava/lang/String;
    .param p6, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p7, "startingPosition"    # J
    .param p9, "uri"    # Landroid/net/Uri;

    .prologue
    .line 721
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestPlayLocalContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "filepath["

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v27, "] uri["

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p9

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v27, "]"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    if-nez p2, :cond_0

    .line 723
    sget-object v6, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v4 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    .line 913
    .end local p4    # "title":Ljava/lang/String;
    :goto_0
    return-void

    .line 727
    .restart local p4    # "title":Ljava/lang/String;
    :cond_0
    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V

    .line 729
    .local v14, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, p3

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v16

    .line 730
    .local v16, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 733
    :try_start_0
    new-instance v23, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 734
    .local v23, "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v25, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct/range {v25 .. v25}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 735
    .local v25, "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-virtual {v4, v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 736
    move-object/from16 v0, v25

    iget v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    if-eqz v4, :cond_1

    .line 737
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSyncStop(Ljava/lang/String;)V

    .line 740
    :cond_1
    move-object/from16 v7, p2

    .line 741
    .local v7, "absFilePath":Ljava/lang/String;
    const-string v4, "file://"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 742
    const/4 v4, 0x7

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 745
    :cond_2
    const/16 v20, 0x0

    .line 746
    .local v20, "originMimeType":Ljava/lang/String;
    if-nez p5, :cond_7

    .line 747
    const/16 v20, 0x0

    .line 771
    :cond_3
    :goto_1
    const-wide/16 v10, 0x0

    .line 772
    .local v10, "position":J
    const-wide/16 v4, 0x0

    cmp-long v4, p7, v4

    if-lez v4, :cond_4

    .line 773
    move-wide/from16 v10, p7

    .line 777
    :cond_4
    if-nez p9, :cond_5

    .line 778
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {v4, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p9

    .line 781
    :cond_5
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 783
    .local v18, "error":Lcom/samsung/android/allshare/ERROR;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->convertUriToItemNode(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v9

    .line 786
    .local v9, "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    new-instance v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 787
    .local v8, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz p4, :cond_e

    move-object/from16 v4, p4

    :goto_2
    iput-object v4, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 788
    const-string v4, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 790
    .local v26, "subtitlePath":Ljava/lang/String;
    if-eqz v26, :cond_f

    .end local v26    # "subtitlePath":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v26

    iput-object v0, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->subtitlePath:Ljava/lang/String;

    .line 793
    new-instance v13, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 794
    .local v13, "actionMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    invoke-virtual {v13, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 795
    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 798
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 799
    .local v12, "result":Landroid/os/Bundle;
    const/16 v24, 0x0

    .line 800
    .local v24, "result_local":I
    if-eqz v9, :cond_10

    .line 801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestPlayLocalContent"

    const-string v6, "play local content with itemnode"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    move-object/from16 v0, p5

    iput-object v0, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 805
    iget-object v4, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    if-eqz v4, :cond_6

    .line 806
    iget-object v4, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v5, "upnp:albumart"

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->album_art_path:Ljava/lang/String;

    .line 808
    :cond_6
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v6, p3

    invoke-virtual/range {v5 .. v12}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContentEx(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;JLandroid/os/Bundle;)I

    move-result v24

    .line 810
    const-string v4, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, p1

    move-object/from16 v1, p9

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 825
    .end local p4    # "title":Ljava/lang/String;
    :goto_4
    const-string v4, "BUNDLE_INT_REQUEST_ID"

    invoke-virtual {v12, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 826
    .local v22, "requestID":I
    const-string v4, "BUNDLE_STRING_PLAYING_URI"

    invoke-virtual {v12, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 827
    .local v15, "contentUri":Ljava/lang/String;
    const/16 v4, 0x232c

    move/from16 v0, v24

    if-ne v0, v4, :cond_13

    .line 828
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    .line 893
    :goto_5
    const-string v4, "BUNDLE_ENUM_ERROR"

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    const-string v4, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v5, "LOCAL_CONTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v14, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 898
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v14, v15}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 900
    if-eqz p6, :cond_1a

    .line 901
    const-string v4, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, p6

    invoke-virtual {v14, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 907
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 909
    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setNotified(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 911
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 748
    .end local v8    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v9    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v10    # "position":J
    .end local v12    # "result":Landroid/os/Bundle;
    .end local v13    # "actionMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .end local v15    # "contentUri":Ljava/lang/String;
    .end local v18    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v22    # "requestID":I
    .end local v24    # "result_local":I
    .restart local p4    # "title":Ljava/lang/String;
    :cond_7
    :try_start_1
    const-string v4, "audio"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 749
    move-object/from16 v20, p5

    .line 750
    const-string v4, "audio/x-wav"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 751
    const-string p5, "audio/wav"

    goto/16 :goto_1

    .line 753
    :cond_8
    const-string v4, "image"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 754
    move-object/from16 v20, p5

    .line 755
    const-string v4, "image/x-ms-bmp"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 756
    const-string p5, "image/bmp"

    goto/16 :goto_1

    .line 757
    :cond_9
    const-string v4, "image/jpg"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 758
    const-string p5, "image/jpeg"

    goto/16 :goto_1

    .line 760
    :cond_a
    const-string v4, "video"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 761
    move-object/from16 v20, p5

    .line 762
    const-string v4, "video/mkv"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    const-string v4, "video/x-matroska"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 763
    :cond_b
    const-string p5, "video/x-mkv"

    goto/16 :goto_1

    .line 764
    :cond_c
    const-string v4, "video/flv"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 765
    const-string p5, "video/x-flv"

    goto/16 :goto_1

    .line 768
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestPlayLocalContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "unknown mimeType = "

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 911
    .end local v7    # "absFilePath":Ljava/lang/String;
    .end local v20    # "originMimeType":Ljava/lang/String;
    .end local v23    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v25    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .end local p4    # "title":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    .line 787
    .restart local v7    # "absFilePath":Ljava/lang/String;
    .restart local v8    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .restart local v9    # "itemNode":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .restart local v10    # "position":J
    .restart local v18    # "error":Lcom/samsung/android/allshare/ERROR;
    .restart local v20    # "originMimeType":Ljava/lang/String;
    .restart local v23    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .restart local v25    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .restart local p4    # "title":Ljava/lang/String;
    :cond_e
    :try_start_2
    const-string v4, "local content"

    goto/16 :goto_2

    .line 790
    .restart local v26    # "subtitlePath":Ljava/lang/String;
    :cond_f
    const-string v26, ""

    goto/16 :goto_3

    .line 813
    .end local v26    # "subtitlePath":Ljava/lang/String;
    .restart local v12    # "result":Landroid/os/Bundle;
    .restart local v13    # "actionMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .restart local v24    # "result_local":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestPlayLocalContent"

    const-string v6, "play local content with filepath"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    if-eqz p4, :cond_11

    .end local p4    # "title":Ljava/lang/String;
    :goto_7
    move-object/from16 v0, p4

    iput-object v0, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 816
    if-eqz p5, :cond_12

    move-object/from16 v4, p5

    :goto_8
    iput-object v4, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 819
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v7, v8, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I

    move-result v24

    .line 821
    const-string v4, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 815
    .restart local p4    # "title":Ljava/lang/String;
    :cond_11
    const-string p4, "local content"

    goto :goto_7

    .line 816
    .end local p4    # "title":Ljava/lang/String;
    :cond_12
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/utility/HttpMimeUtils;->parseMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 829
    .restart local v15    # "contentUri":Ljava/lang/String;
    .restart local v22    # "requestID":I
    :cond_13
    const/16 v4, 0x2328

    move/from16 v0, v24

    if-ne v0, v4, :cond_14

    .line 830
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_5

    .line 831
    :cond_14
    const/16 v4, 0x2329

    move/from16 v0, v24

    if-ne v0, v4, :cond_15

    .line 832
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_5

    .line 833
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v24

    move/from16 v1, v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v4, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;II)Z

    move-result v4

    if-nez v4, :cond_16

    .line 834
    sget-object v18, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_5

    .line 837
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 838
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    const-wide/32 v4, 0xea60

    invoke-virtual {v13, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v21

    .line 840
    .local v21, "received":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 847
    invoke-virtual {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v17

    .line 848
    .local v17, "ec":I
    if-eqz v21, :cond_18

    if-eqz v17, :cond_18

    .line 849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestPlayLocalContent"

    const-string v6, "try again....."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    new-instance v13, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .end local v13    # "actionMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-direct {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 853
    .restart local v13    # "actionMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    invoke-virtual {v13, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 854
    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 857
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestSyncStop(Ljava/lang/String;)V

    .line 858
    move-object/from16 v0, p5

    iput-object v0, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 860
    if-eqz v9, :cond_19

    .line 861
    iget-object v4, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    if-eqz v4, :cond_17

    .line 862
    iget-object v4, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;->attributes:Ljava/util/ArrayList;

    const-string v5, "upnp:albumart"

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/BundleCreator;->getArtistByName(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->album_art_path:Ljava/lang/String;

    .line 865
    :cond_17
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v6, p3

    invoke-virtual/range {v5 .. v12}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContentEx(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;JLandroid/os/Bundle;)I

    .line 873
    :goto_9
    const-string v4, "BUNDLE_INT_REQUEST_ID"

    invoke-virtual {v12, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 874
    .local v19, "key":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v19

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 875
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    const-wide/32 v4, 0xea60

    invoke-virtual {v13, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 877
    invoke-virtual {v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v17

    .line 878
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 886
    const-string v4, "BUNDLE_STRING_PLAYING_URI"

    invoke-virtual {v12, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 889
    .end local v19    # "key":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v17

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v18

    goto/16 :goto_5

    .line 868
    :cond_19
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v7, v8, v12}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I

    move-result v24

    goto :goto_9

    .line 903
    .end local v17    # "ec":I
    .end local v21    # "received":Z
    :cond_1a
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-wide/from16 v0, p7

    invoke-virtual {v14, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_6
.end method

.method private requestPlayerState(Ljava/lang/String;)V
    .locals 11
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 644
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 645
    .local v4, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 646
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v7, "GetTransportInfo"

    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 648
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v7

    invoke-virtual {v7, p1, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getTrnasportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    .line 649
    .local v6, "result":I
    if-eqz v6, :cond_0

    .line 650
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    const-string v9, "invoke native function failed!"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v8, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 653
    iget v1, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 654
    .local v1, "key":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    const-wide/16 v8, 0x7530

    invoke-virtual {v2, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 658
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 659
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 661
    .local v5, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 662
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v7

    if-nez v7, :cond_1

    .line 663
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 665
    .local v3, "playerState":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    iget v7, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    packed-switch v7, :pswitch_data_0

    .line 683
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 687
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestPlayerState  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    const-string v7, "BUNDLE_STRING_AV_PLAER_STATE"

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    .end local v3    # "playerState":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 698
    return-void

    .line 667
    .restart local v3    # "playerState":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :pswitch_0
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 668
    goto :goto_0

    .line 671
    :pswitch_1
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 672
    goto :goto_0

    .line 675
    :pswitch_2
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 676
    goto :goto_0

    .line 679
    :pswitch_3
    sget-object v3, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 680
    goto :goto_0

    .line 692
    .end local v3    # "playerState":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "monitor error code  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v7, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v8, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 665
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private requestRCSControl(Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 15
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "dmrUDN"    # Ljava/lang/String;
    .param p3, "isMute"    # Z
    .param p4, "volume"    # I

    .prologue
    .line 941
    new-instance v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 942
    .local v8, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 943
    .local v2, "bundle":Landroid/os/Bundle;
    const/4 v10, 0x0

    .line 945
    .local v10, "result":I
    new-instance v6, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 947
    .local v6, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v11, "GetMute"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 949
    const-string v11, "BUNDLE_BOOLEAN_MUTE"

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 951
    const-string v11, "GetMute"

    invoke-virtual {v6, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 952
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getMute(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v10

    .line 976
    :cond_0
    :goto_0
    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "requestRCSControl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "complete..action["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] volume["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] isMute["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " then result["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] requestID["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v12, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v11, v10, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;II)Z

    move-result v11

    if-nez v11, :cond_4

    .line 983
    const-string v11, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v2, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1022
    :goto_1
    return-void

    .line 954
    :cond_1
    const-string v11, "SetMute"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 956
    const-string v11, "BUNDLE_BOOLEAN_MUTE"

    move/from16 v0, p3

    invoke-virtual {v2, v11, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 958
    const-string v11, "SetMute"

    invoke-virtual {v6, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 959
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v11

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v11, v0, v1, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setMute(Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v10

    goto/16 :goto_0

    .line 961
    :cond_2
    const-string v11, "GetVolume"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 963
    const-string v11, "BUNDLE_INT_VOLUME"

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 965
    const-string v11, "GetVolume"

    invoke-virtual {v6, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 966
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getVolume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v10

    goto/16 :goto_0

    .line 968
    :cond_3
    const-string v11, "SetVolume"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 970
    const-string v11, "BUNDLE_INT_VOLUME"

    move/from16 v0, p4

    invoke-virtual {v2, v11, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 972
    const-string v11, "SetVolume"

    invoke-virtual {v6, v11}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 973
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v11

    move-object/from16 v0, p2

    move/from16 v1, p4

    invoke-virtual {v11, v0, v1, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setVolume(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v10

    goto/16 :goto_0

    .line 991
    :cond_4
    iget v5, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 992
    .local v5, "key":I
    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddRCSListener(I)V
    invoke-static {v11, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 993
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v11

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 996
    const-wide/16 v12, 0x7530

    invoke-virtual {v6, v12, v13}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v9

    .line 999
    .local v9, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v11

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1002
    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v4

    .line 1003
    .local v4, "errorCode":I
    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;

    .line 1005
    .local v7, "rcsresponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;
    if-eqz v9, :cond_5

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1006
    :cond_5
    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "requestRCSControl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mRequestID :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " has wrong response"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    iget-object v11, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v11, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v3

    .line 1010
    .local v3, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v11, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v2, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    const-string v11, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    .end local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_2
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1013
    :cond_6
    const-string v11, "GetMute"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1014
    const-string v11, "BUNDLE_BOOLEAN_MUTE"

    iget-boolean v12, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->isMute:Z

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1018
    :cond_7
    :goto_3
    const-string v11, "BUNDLE_STRING_ID"

    iget-object v12, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->dmrUDN:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    const-string v11, "BUNDLE_ENUM_ERROR"

    sget-object v12, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1015
    :cond_8
    const-string v11, "GetVolume"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1016
    const-string v11, "BUNDLE_INT_VOLUME"

    iget v12, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/RCSResponse;->volume:I

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3
.end method

.method private requestSkipDynamicBuffering(Ljava/lang/String;)V
    .locals 9
    .param p1, "dmrUdn"    # Ljava/lang/String;

    .prologue
    .line 1476
    new-instance v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v1}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1477
    .local v1, "id":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1478
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1479
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v5, "X_SkipDynamicBuffer"

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1480
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    invoke-virtual {v5, p1, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->skipDynamicBuffer(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v4

    .line 1481
    .local v4, "ret":I
    if-eqz v4, :cond_0

    .line 1482
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1483
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1508
    :goto_0
    return-void

    .line 1487
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v6, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1488
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$10100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    iget v6, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1491
    const-wide/16 v6, 0x7530

    invoke-virtual {v2, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v3

    .line 1494
    .local v3, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$10200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    iget v6, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1496
    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1497
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$10300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestSkipDynamicBuffering"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "skip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has wrong response"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1502
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$10400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestSkipDynamicBuffering"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "skip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private requestSyncStop(Ljava/lang/String;)V
    .locals 7
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 1323
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1324
    .local v2, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1326
    .local v1, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Stop"

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1327
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v3

    .line 1328
    .local v3, "result_stop":I
    if-eqz v3, :cond_0

    .line 1329
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestSyncStop"

    const-string v6, "Device can`t stop!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v5, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1332
    iget v0, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 1333
    .local v0, "key":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1336
    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 1337
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestSyncStop"

    const-string v6, "Device is stopped!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    return-void
.end method

.method private requestWhaControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "dmrUDN"    # Ljava/lang/String;
    .param p3, "partyID"    # Ljava/lang/String;

    .prologue
    .line 1026
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1027
    .local v4, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1028
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v6, 0x0

    .line 1030
    .local v6, "result":I
    new-instance v3, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1032
    .local v3, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v8, "GetDeviceStatusInfo"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1033
    const-string v8, "GetDeviceStatusInfo"

    invoke-virtual {v3, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1034
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v8

    invoke-virtual {v8, p2, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getDeviceStatusInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    .line 1046
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "requestWhaControl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "complete..action["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] partyID["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " then result["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] requestID["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    iget v9, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->validateReturnValue(II)Z
    invoke-static {v8, v6, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$4100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;II)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1053
    const-string v8, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v8, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v8, "BUNDLE_ENUM_ERROR"

    sget-object v9, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1100
    :goto_1
    return-void

    .line 1035
    :cond_1
    const-string v8, "CreateParty"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1036
    const-string v8, "CreateParty"

    invoke-virtual {v3, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1037
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v8

    invoke-virtual {v8, p2, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->createParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    goto/16 :goto_0

    .line 1038
    :cond_2
    const-string v8, "JoinParty"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1039
    const-string v8, "JoinParty"

    invoke-virtual {v3, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1040
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v8

    invoke-virtual {v8, p2, p3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->joinParty(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    goto/16 :goto_0

    .line 1041
    :cond_3
    const-string v8, "LeaveParty"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1042
    const-string v8, "LeaveParty"

    invoke-virtual {v3, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1043
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v8

    invoke-virtual {v8, p2, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->leaveParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    goto/16 :goto_0

    .line 1061
    :cond_4
    iget v2, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 1062
    .local v2, "key":I
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->requestAddWhaListener(I)V

    .line 1063
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1066
    const-wide/16 v8, 0x7530

    invoke-virtual {v3, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v5

    .line 1069
    .local v5, "responsed":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072
    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v1

    .line 1073
    .local v1, "errorCode":I
    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;

    .line 1075
    .local v7, "whaResponse":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;
    if-eqz v5, :cond_5

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1076
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "requestWhaControl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "FAIL : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", caused by responsed="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " || hasWrongResponse="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    const-string v8, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v8, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const-string v8, "BUNDLE_ENUM_ERROR"

    sget-object v9, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    :goto_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1082
    :cond_6
    const-string v8, "GetDeviceStatusInfo"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1083
    const-string v8, "BUNDLE_STRING_WHA_FRIENDLY_NAME"

    iget-object v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->friendlyName:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    const-string v8, "BUNDLE_STRING_WHA_DEVICE_STATUS"

    iget v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->deviceStatus:I

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1087
    const-string v8, "BUNDLE_STRING_WHA_PARTY_ID"

    iget-object v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyID:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    const-string v8, "BUNDLE_STRING_WHA_PLAY_STATE"

    iget v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyStatus:I

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1089
    const-string v8, "BUNDLE_STRING_WHA_VOLUME"

    iget v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->volume:I

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1095
    :cond_7
    :goto_3
    const-string v8, "BUNDLE_STRING_ID"

    iget-object v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->dmrUDN:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    const-string v8, "BUNDLE_ENUM_ERROR"

    sget-object v9, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1090
    :cond_8
    const-string v8, "CreateParty"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1091
    iget-object v8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$6200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "requestWhaControl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "WHA_PARTY_ID = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v8, "BUNDLE_STRING_WHA_PARTY_ID"

    iget-object v9, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/WHAResponse;->partyID:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 32
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "bundleItem"    # Landroid/os/Bundle;

    .prologue
    .line 1343
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1344
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    sget-object v10, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    invoke-direct/range {v4 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    .line 1345
    const/4 v4, 0x0

    .line 1438
    :goto_0
    return v4

    .line 1348
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, p1

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->isSupportSetNextAVT(Ljava/lang/String;)Z
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1349
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestsetNextTranportUri"

    const-string v9, "Device doesn`t support NextTranport!"

    invoke-static {v4, v5, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 1351
    .local v19, "b":Landroid/os/Bundle;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1352
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1354
    const/4 v4, 0x1

    goto :goto_0

    .line 1356
    .end local v19    # "b":Landroid/os/Bundle;
    :cond_2
    new-instance v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 1357
    .local v10, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v25, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 1358
    .local v25, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "SetNextAVTransportURI"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 1360
    const-string v4, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1361
    .local v6, "filePath":Ljava/lang/String;
    const-string v4, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v31

    check-cast v31, Landroid/net/Uri;

    .line 1362
    .local v31, "uri":Landroid/net/Uri;
    const-string v4, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1364
    .local v26, "objectID":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 1365
    .local v7, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz v31, :cond_7

    .line 1366
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v31

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 1367
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v31

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v7

    .line 1373
    :goto_1
    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "requestsetNextTranportUri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "setNexTransportUri, (filePath!=null) is "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v6, :cond_8

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v9, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    if-eqz v6, :cond_a

    .line 1378
    const-string v4, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1380
    .local v8, "subtitlePath":Ljava/lang/String;
    if-nez v8, :cond_3

    .line 1381
    const-string v8, ""

    .line 1383
    :cond_3
    move-object v11, v6

    .line 1384
    .local v11, "absFilePath":Ljava/lang/String;
    const-string v4, "file://"

    invoke-virtual {v6, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1385
    const/4 v4, 0x7

    invoke-virtual {v6, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 1387
    :cond_4
    iget-object v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    if-nez v4, :cond_5

    .line 1388
    invoke-static {v11}, Lcom/samsung/android/allshare/service/mediashare/utility/HttpMimeUtils;->parseMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 1390
    :cond_5
    iget-object v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    :goto_3
    iput-object v4, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 1392
    iput-object v8, v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->subtitlePath:Ljava/lang/String;

    .line 1393
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    const/4 v9, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v4 .. v10}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setNextLocalTranportUri(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 1411
    .end local v8    # "subtitlePath":Ljava/lang/String;
    .end local v11    # "absFilePath":Ljava/lang/String;
    :goto_4
    iget v0, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v24, v0

    .line 1412
    .local v24, "key":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move/from16 v0, v24

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)V

    .line 1413
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416
    const-wide/16 v4, 0x7530

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v28

    .line 1417
    .local v28, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1418
    new-instance v29, Landroid/os/Bundle;

    invoke-direct/range {v29 .. v29}, Landroid/os/Bundle;-><init>()V

    .line 1420
    .local v29, "retBundle":Landroid/os/Bundle;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 1421
    .local v27, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    if-eqz v28, :cond_6

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1422
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestsetNextTranportUri"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mRequestID :"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v12, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "SetNextAVTransportURI"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " has wrong response"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v5, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v5

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v22

    .line 1427
    .local v22, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v4, "BUNDLE_STRING_ID"

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    const-string v4, "BUNDLE_ENUM_ERROR"

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437
    .end local v22    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 1438
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1369
    .end local v24    # "key":I
    .end local v27    # "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    .end local v28    # "result":Z
    .end local v29    # "retBundle":Landroid/os/Bundle;
    :cond_7
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v6, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ContentsHelper;->queryContentUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    .line 1371
    .local v23, "fileuri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v7

    goto/16 :goto_1

    .line 1375
    .end local v23    # "fileuri":Landroid/net/Uri;
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1390
    .restart local v8    # "subtitlePath":Ljava/lang/String;
    .restart local v11    # "absFilePath":Ljava/lang/String;
    :cond_9
    const-string v4, "local content"

    goto/16 :goto_3

    .line 1396
    .end local v8    # "subtitlePath":Ljava/lang/String;
    .end local v11    # "absFilePath":Ljava/lang/String;
    :cond_a
    const-string v4, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 1397
    .local v30, "serverUdn":Ljava/lang/String;
    new-instance v20, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;-><init>()V

    .line 1398
    .local v20, "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    if-eqz v30, :cond_b

    if-nez v26, :cond_c

    .line 1399
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$8900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestsetNextTranportUri"

    const-string v9, "setNexTransportUri, serverUdn or objectID is null "

    invoke-static {v4, v5, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1403
    :cond_c
    move-object/from16 v0, v20

    move-object/from16 v1, v30

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v21

    .line 1404
    .local v21, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->discard()V

    .line 1405
    if-eqz v21, :cond_d

    .line 1406
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$2900()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1, v10}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setNextTranportUri(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    goto/16 :goto_4

    .line 1408
    :cond_d
    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    sget-object v18, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v12, p0

    move-object/from16 v13, p2

    invoke-direct/range {v12 .. v18}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_4

    .line 1430
    .end local v20    # "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    .end local v21    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v30    # "serverUdn":Ljava/lang/String;
    .restart local v24    # "key":I
    .restart local v27    # "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    .restart local v28    # "result":Z
    .restart local v29    # "retBundle":Landroid/os/Bundle;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$9400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "requestsetNextTranportUri"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "after response,RequestID :"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v12, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ":"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "SetNextAVTransportURI"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " :"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "ErrorCode: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v5, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1434
    const-string v4, "BUNDLE_STRING_ID"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->dmrUDN:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    const-string v4, "BUNDLE_ENUM_ERROR"

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private responsePlayFail(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/ERROR;)V
    .locals 4
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p4, "startingPosition"    # J
    .param p6, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 917
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;->access$5100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "responsePlayFail"

    invoke-virtual {p6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 919
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 922
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 924
    if-eqz p3, :cond_0

    .line 925
    const-string v1, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 930
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;->response(Landroid/os/Bundle;)V

    .line 931
    return-void

    .line 927
    :cond_0
    const-string v1, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 446
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerAsyncActionHandler$AVPlayerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 641
    return-void
.end method
