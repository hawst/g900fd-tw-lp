.class Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;
.super Landroid/content/BroadcastReceiver;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 574
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 576
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 577
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "intent.getAction() == null!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 579
    :cond_1
    const-string v1, "STOP_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 580
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "STOP_SERVICE"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->NONE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1302(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    .line 582
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1400(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/app/NotificationManager;

    move-result-object v1

    if-nez v1, :cond_2

    .line 583
    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v1

    const-string v3, "notification"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;
    invoke-static {v2, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1402(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Landroid/app/NotificationManager;)Landroid/app/NotificationManager;

    .line 585
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mNotiManager:Landroid/app/NotificationManager;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1400(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    .line 586
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 587
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceConn:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$900(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 588
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$102(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    goto :goto_0

    .line 590
    :cond_3
    const-string v1, "CANCEL_DOWNLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 591
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "CANCEL_DOWNLOAD"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->cancelDownload()V

    goto :goto_0

    .line 593
    :cond_4
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1300(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->PROGRESS:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v1, v2, :cond_5

    .line 596
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    goto/16 :goto_0

    .line 597
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mLastNotiType:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$1300(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;->COMPLETE:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$NotiType;

    if-ne v1, v2, :cond_0

    .line 598
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$4;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->completeNotification()V

    goto/16 :goto_0
.end method
