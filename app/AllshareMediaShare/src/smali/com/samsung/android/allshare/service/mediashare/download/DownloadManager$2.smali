.class Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;
.super Ljava/lang/Object;
.source "DownloadManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->addDownloadList(Ljava/lang/String;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

.field final synthetic val$items:Ljava/util/ArrayList;

.field final synthetic val$serverName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->val$items:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->val$serverName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 229
    const-wide/16 v2, 0x0

    .line 230
    .local v2, "addItemsSize":J
    const-wide/16 v12, 0x0

    .line 231
    .local v12, "lfileSize":J
    new-instance v17, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;-><init>()V

    .line 232
    .local v17, "tempList":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    const/4 v5, 0x0

    .line 234
    .local v5, "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    const/4 v6, 0x0

    .line 235
    .local v6, "downloadLocation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v19

    const-string v20, "storage"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/storage/StorageManager;

    .line 237
    .local v14, "mStorageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v14}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v18

    .line 239
    .local v18, "volumePaths":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$300(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "NearbyDownloadTo"

    invoke-static/range {v19 .. v20}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 242
    .local v16, "savePath":Ljava/lang/String;
    if-nez v16, :cond_0

    .line 243
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Nearby"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 245
    :cond_0
    if-eqz v18, :cond_3

    .line 246
    move-object/from16 v4, v18

    .local v4, "arr$":[Ljava/lang/String;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v11, :cond_3

    aget-object v15, v4, v8

    .line 247
    .local v15, "path":Ljava/lang/String;
    if-eqz v15, :cond_2

    const-string v19, "sd"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "Sd"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_2

    :cond_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 248
    const/4 v6, 0x1

    .line 246
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 253
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v11    # "len$":I
    .end local v15    # "path":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->val$items:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v9

    .local v9, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/allshare/Item;>;"
    :cond_4
    :goto_1
    invoke-interface {v9}, Ljava/util/ListIterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 254
    invoke-interface {v9}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/allshare/Item;

    .line 255
    .local v10, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v10, :cond_4

    .line 256
    invoke-virtual {v10}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v12

    .line 257
    const-wide/16 v20, 0x0

    cmp-long v19, v12, v20

    if-gtz v19, :cond_7

    .line 258
    invoke-virtual {v10}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v19

    if-eqz v19, :cond_5

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    invoke-virtual {v10}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->getFileSize(Ljava/lang/String;)J
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$400(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Ljava/lang/String;)J

    move-result-wide v12

    .line 260
    :cond_5
    new-instance v5, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .end local v5    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->val$serverName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v10, v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item;I)V

    .line 262
    .restart local v5    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    const-wide/16 v20, 0x0

    cmp-long v19, v12, v20

    if-gez v19, :cond_6

    .line 263
    const-wide/16 v12, 0x0

    .line 265
    :cond_6
    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getContentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v19

    check-cast v19, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v19

    const-string v20, "BUNDLE_LONG_ITEM_FILE_SIZE"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 272
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->add(Ljava/lang/Object;)Z

    .line 273
    add-long/2addr v2, v12

    goto :goto_1

    .line 268
    :cond_7
    new-instance v5, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .end local v5    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->val$serverName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v10, v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item;I)V

    .restart local v5    # "downloadItem":Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    goto :goto_2

    .line 278
    .end local v10    # "item":Lcom/samsung/android/allshare/Item;
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->isComplete()Z

    move-result v19

    if-eqz v19, :cond_a

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->clear()V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsMemoryFull:Z
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$602(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->addAll(Ljava/util/Collection;)Z

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsUpdate:Z
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$702(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # invokes: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->showNotification()V
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$800(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mIsUpdate:Z
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$702(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Z)Z

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->itemDownload()V

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->acquireLock()V

    .line 304
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->getTotalSize()J

    move-result-wide v20

    add-long v20, v20, v2

    invoke-virtual/range {v19 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->setTotalSize(J)V

    .line 309
    :goto_4
    return-void

    .line 296
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v19

    new-instance v20, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$200(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/Context;

    move-result-object v21

    const-class v22, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    invoke-direct/range {v20 .. v22}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mServiceConn:Landroid/content/ServiceConnection;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$900(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Landroid/content/ServiceConnection;

    move-result-object v21

    const/16 v22, 0x1

    invoke-virtual/range {v19 .. v22}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 306
    :catch_0
    move-exception v7

    .line 307
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_4

    .line 300
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :cond_a
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->addAll(Ljava/util/Collection;)Z

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadItemList:Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$500(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItemList;->updateList()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method
