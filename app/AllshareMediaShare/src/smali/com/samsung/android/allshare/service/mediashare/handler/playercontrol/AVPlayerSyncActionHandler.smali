.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.source "AVPlayerSyncActionHandler.java"


# static fields
.field private static final AVPLAYER_STATE_BUFFERING:I = 0x1

.field private static final AVPLAYER_STATE_PAUSED:I = 0x3

.field private static final AVPLAYER_STATE_PLAYING:I = 0x2

.field private static final AVPLAYER_STATE_STOPPED:I = 0x0

.field private static final AVPLAYER_STATE_UNKNOWN:I = 0x4

.field public static final DYNAMICBUFFER:Ljava/lang/String; = "DynamicBuffer"

.field private static mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 52
    return-void
.end method


# virtual methods
.method protected defineHandler()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_GET_PLAYER_STATE_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 60
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 61
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 62
    const-string v0, "ACTION_AV_PLAYER_IS_SUPPORT_REDIRECT_SYNC"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 63
    const-string v0, "ACTION_AV_PLAYER_IS_SUPPORT_DYNAMIC_BUFFERING"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 66
    const-string v0, "com.sec.android.allshare.action.ACTION_SMSC_iS_SEEKABLE_ON_PAUSE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 67
    const-string v0, "com.sec.android.allshare.action.ACTION_SMSC_IS_WHOLE_HOME_AUDIO"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected finiHandler()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method protected initHandler()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 88
    return-void
.end method

.method protected responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 18
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "responseSync"

    const-string v17, "cvm is null"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v15, 0x0

    .line 245
    :goto_0
    return-object v15

    .line 102
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v12

    .line 103
    .local v12, "reqb":Landroid/os/Bundle;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "actionID":Ljava/lang/String;
    if-eqz v1, :cond_1

    if-nez v12, :cond_2

    .line 105
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "responseSync"

    const-string v17, "bundle/reqb id is null"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v15, 0x0

    goto :goto_0

    .line 108
    :cond_2
    const-string v15, "BUNDLE_STRING_ID"

    invoke-virtual {v12, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 110
    .local v14, "udn":Ljava/lang/String;
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 111
    .local v10, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v15

    const/16 v16, 0x4

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDeviceList(I)Ljava/util/ArrayList;

    move-result-object v9

    .line 113
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    if-nez v9, :cond_3

    .line 114
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto :goto_0

    .line 116
    :cond_3
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v15

    if-ge v5, v15, :cond_4

    .line 117
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 118
    .local v3, "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    iget-object v15, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-interface {v10, v15, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 122
    .end local v3    # "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_4
    const-string v15, "com.sec.android.allshare.action.ACTION_AV_PLAYER_GET_PLAYER_STATE_SYNC"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 123
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 124
    .local v2, "b":Landroid/os/Bundle;
    new-instance v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct {v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 125
    .local v11, "playerState":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    sget-object v15, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    new-instance v16, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v11, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v13

    .line 126
    .local v13, "result":I
    if-eqz v13, :cond_5

    .line 127
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "responseSync"

    const-string v17, "[responseSync] mrcpWrapper.getPlayState failed!"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 131
    :cond_5
    iget v15, v11, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    packed-switch v15, :pswitch_data_0

    .line 152
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 133
    :pswitch_0
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 138
    :pswitch_1
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 143
    :pswitch_2
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 148
    :pswitch_3
    const-string v15, "BUNDLE_STRING_AV_PLAER_STATE"

    sget-object v16, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumToString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 159
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v11    # "playerState":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .end local v13    # "result":I
    :cond_6
    const-string v15, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_SYNC"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 160
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 161
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 162
    .local v4, "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v4, :cond_7

    .line 163
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "responseSync"

    const-string v17, "cannot get device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_VIDEO"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 166
    :cond_7
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_VIDEO"

    iget-boolean v0, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportVideo:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    .line 169
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_8
    const-string v15, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_SYNC"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 170
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 171
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 172
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v4, :cond_9

    .line 173
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v16, "responseSync"

    const-string v17, "cannot get device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_AUDIO"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 178
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 176
    :cond_9
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_AUDIO"

    iget-boolean v0, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSupportAudio:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 179
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_a
    const-string v15, "ACTION_AV_PLAYER_IS_SUPPORT_REDIRECT_SYNC"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 180
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 181
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 183
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v7, 0x0

    .line 185
    .local v7, "isSupport":Z
    if-eqz v4, :cond_b

    .line 186
    iget-boolean v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isWebURLPlayable:Z

    if-eqz v15, :cond_c

    .line 187
    const/4 v7, 0x1

    .line 194
    :cond_b
    :goto_5
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_REDIRECT"

    invoke-virtual {v2, v15, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 195
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 188
    :cond_c
    iget-object v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    if-eqz v15, :cond_b

    iget-object v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->modelName:Ljava/lang/String;

    const-string v16, "Windows Media Player"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 190
    const/4 v7, 0x1

    goto :goto_5

    .line 196
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v7    # "isSupport":Z
    :cond_d
    const-string v15, "ACTION_AV_PLAYER_IS_SUPPORT_DYNAMIC_BUFFERING"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 197
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 198
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 199
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v7, 0x0

    .line 200
    .restart local v7    # "isSupport":Z
    if-eqz v4, :cond_e

    .line 201
    iget-object v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->productCap:Ljava/lang/String;

    if-eqz v15, :cond_e

    iget-object v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->productCap:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    const-string v16, "DynamicBuffer"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 204
    const/4 v7, 0x1

    .line 207
    :cond_e
    const-string v15, "BUNDLE_BOOLEAN_SUPPORT_DYNAMIC_BUFFERING"

    invoke-virtual {v2, v15, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 208
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 211
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v7    # "isSupport":Z
    :cond_f
    const-string v15, "com.sec.android.allshare.action.ACTION_SMSC_iS_SEEKABLE_ON_PAUSE"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 212
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 213
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 215
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v6, 0x0

    .line 217
    .local v6, "isSeekableOnPause":Z
    if-eqz v4, :cond_10

    .line 218
    iget-boolean v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isSeekOnPaused:Z

    if-eqz v15, :cond_11

    .line 219
    const/4 v6, 0x1

    .line 225
    :cond_10
    :goto_6
    const-string v15, "BUNDLE_BOOLEAN_SMSC_iS_SEEKABLE_ON_PAUSE"

    invoke-virtual {v2, v15, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 226
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 221
    :cond_11
    const/4 v6, 0x0

    goto :goto_6

    .line 228
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v6    # "isSeekableOnPause":Z
    :cond_12
    const-string v15, "com.sec.android.allshare.action.ACTION_SMSC_IS_WHOLE_HOME_AUDIO"

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 229
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 230
    .restart local v2    # "b":Landroid/os/Bundle;
    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .line 232
    .restart local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    const/4 v8, 0x0

    .line 234
    .local v8, "isWholeHomeAudio":Z
    if-eqz v4, :cond_13

    .line 235
    iget-boolean v15, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->isWholeHomeAudio:Z

    if-eqz v15, :cond_14

    .line 236
    const/4 v8, 0x1

    .line 241
    :cond_13
    :goto_7
    const-string v15, "BUNDLE_BOOLEAN_SMSC_IS_WHOLE_HOME_AUDIO"

    invoke-virtual {v2, v15, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 242
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 238
    :cond_14
    const/4 v8, 0x0

    goto :goto_7

    .line 245
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v4    # "deviceItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .end local v8    # "isWholeHomeAudio":Z
    :cond_15
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/AVPlayerSyncActionHandler;->response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v15

    goto/16 :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
