.class public Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;
.super Landroid/widget/BaseAdapter;
.source "DownloadListAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;",
            ">;"
        }
    .end annotation
.end field

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadList"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .line 62
    iput-object p4, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mResources:Landroid/content/res/Resources;

    .line 64
    return-void
.end method

.method private getDownloadToStringId(I)I
    .locals 3
    .param p1, "downloadTo"    # I

    .prologue
    .line 96
    packed-switch p1, :pswitch_data_0

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown download loacation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :pswitch_0
    const v0, 0x7f070010

    .line 100
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f07000f

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSizeText(I)Ljava/lang/String;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v2

    .line 68
    .local v2, "totalBytes":J
    const-string v0, ""

    .line 69
    .local v0, "sizeText":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_0
    return-object v0
.end method

.method private getStatusStringId(I)I
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 76
    packed-switch p1, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :pswitch_0
    const v0, 0x7f070006

    .line 90
    :goto_0
    return v0

    .line 81
    :pswitch_1
    const v0, 0x7f070005

    goto :goto_0

    .line 84
    :pswitch_2
    const v0, 0x7f070004

    goto :goto_0

    .line 87
    :pswitch_3
    const v0, 0x7f070008

    goto :goto_0

    .line 90
    :pswitch_4
    const v0, 0x7f070007

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private retrieveAndSetIcon(Landroid/view/View;I)V
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 107
    const v3, 0x7f0a000d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 108
    .local v0, "iconView":Landroid/widget/ImageView;
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 109
    .local v2, "thumbnail":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 110
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 155
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    :cond_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getMediaType()Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "mediaType":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    if-eqz v1, :cond_0

    .line 119
    const-string v3, "audio/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    const v3, 0x7f020001

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 121
    :cond_2
    const-string v3, "video/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 122
    const v3, 0x7f020002

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 124
    :cond_3
    const/high16 v3, 0x7f020000

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 194
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 209
    move-object v5, p2

    check-cast v5, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .line 210
    .local v5, "view":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    if-nez p2, :cond_4

    .line 211
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030001

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5    # "view":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    check-cast v5, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;

    .line 213
    .restart local v5    # "view":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setDownloadListObj(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V

    .line 214
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;-><init>()V

    .line 215
    .local v2, "holder":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;
    const v6, 0x7f0a000c

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 216
    const v6, 0x7f0a000d

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->iconView:Landroid/widget/ImageView;

    .line 217
    const v6, 0x7f0a000e

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->titleView:Landroid/widget/TextView;

    .line 218
    const v6, 0x7f0a000f

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->domainView:Landroid/widget/TextView;

    .line 219
    const v6, 0x7f0a0011

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->sizeView:Landroid/widget/TextView;

    .line 220
    const v6, 0x7f0a0010

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->statusView:Landroid/widget/TextView;

    .line 221
    const v6, 0x7f0a0012

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->pathView:Landroid/widget/TextView;

    .line 223
    invoke-virtual {v5, v2}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setTag(Ljava/lang/Object;)V

    .line 227
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getStatus()I

    move-result v3

    .line 228
    .local v3, "status":I
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDownloadTo()I

    move-result v1

    .line 229
    .local v1, "downloadTo":I
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 231
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 232
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f070003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 234
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getMediaType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, p1, v4, v7}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setData(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getCreatedFileName()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "createFileName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 238
    move-object v0, v4

    .line 240
    :cond_1
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->domainView:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getItem(I)Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->sizeView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getSizeText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->statusView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mResources:Landroid/content/res/Resources;

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getStatusStringId(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->pathView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mResources:Landroid/content/res/Resources;

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getDownloadToStringId(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    invoke-direct {p0, v5, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->retrieveAndSetIcon(Landroid/view/View;I)V

    .line 249
    const/4 v6, 0x2

    if-eq v3, v6, :cond_2

    const/4 v6, 0x3

    if-eq v3, v6, :cond_2

    const/4 v6, 0x4

    if-ne v3, v6, :cond_5

    .line 251
    :cond_2
    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 252
    invoke-virtual {v5, v9}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->setChecked(Z)V

    .line 253
    :cond_3
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 258
    :goto_1
    return-object v5

    .line 225
    .end local v0    # "createFileName":Ljava/lang/String;
    .end local v1    # "downloadTo":I
    .end local v2    # "holder":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;
    .end local v3    # "status":I
    .end local v4    # "title":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadUIItem;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;

    .restart local v2    # "holder":Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;
    goto/16 :goto_0

    .line 255
    .restart local v0    # "createFileName":Ljava/lang/String;
    .restart local v1    # "downloadTo":I
    .restart local v3    # "status":I
    .restart local v4    # "title":Ljava/lang/String;
    :cond_5
    iget-object v6, v2, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method refreshData()V
    .locals 4

    .prologue
    .line 167
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 176
    :goto_0
    return-void

    .line 171
    :cond_0
    const-wide/16 v2, 0xa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDownloadListObj(Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;)V
    .locals 0
    .param p1, "downloadList"    # Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadListAdaptor;->mDownloadList:Lcom/samsung/android/allshare/service/mediashare/download/ui/DownloadActivity;

    .line 160
    return-void
.end method
