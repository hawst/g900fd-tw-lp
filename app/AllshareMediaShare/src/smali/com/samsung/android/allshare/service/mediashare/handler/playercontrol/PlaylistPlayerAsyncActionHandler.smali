.class public Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;
.source "PlaylistPlayerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;
    }
.end annotation


# instance fields
.field private currentIndex:I

.field private currentUDN:Ljava/lang/String;

.field private eventListener:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

.field private isForceStop:Z

.field private uriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 2
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;-><init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V

    .line 97
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z

    .line 103
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;

    .line 130
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->eventListener:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    .line 40
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->playNextItem()V

    return-void
.end method

.method private playNextItem()V
    .locals 10

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->uriList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 113
    .local v9, "uri":Landroid/net/Uri;
    const-string v0, "URI"

    invoke-virtual {v9, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "uriStr":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    invoke-direct {v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;-><init>()V

    .line 117
    .local v7, "albumDetails":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    const-wide/16 v0, 0x5dc

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_1
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 126
    :goto_2
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;

    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    const-wide/16 v4, 0x0

    new-instance v6, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    goto :goto_0

    .line 118
    :catch_0
    move-exception v8

    .line 119
    .local v8, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "playNextItem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    .end local v8    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v8

    .line 124
    .restart local v8    # "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;

    const-string v1, "playNextItem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method protected cancelRequest(J)V
    .locals 0
    .param p1, "req_id"    # J

    .prologue
    .line 95
    return-void
.end method

.method protected createActionWorker()Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$PlaylistPlayerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;)V

    return-object v0
.end method

.method protected defineHandler()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PLAY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 48
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PAUSE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 49
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_RESUME"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 50
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_STOP"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 51
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SEEK"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 52
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_NEXT"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 53
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PERVIOUS"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 54
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 57
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_AUTO_FLIP_MODE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 58
    const-string v0, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_QUICK_NAVIGATE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->setActionID(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method protected finiHandler()V
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->eventListener:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->removeMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z

    .line 77
    return-void
.end method

.method protected initHandler()V
    .locals 2

    .prologue
    .line 67
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->eventListener:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPEventNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;)Z

    .line 68
    return-void
.end method
