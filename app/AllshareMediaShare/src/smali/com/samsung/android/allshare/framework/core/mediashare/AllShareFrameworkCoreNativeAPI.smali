.class public Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;
.super Ljava/lang/Object;
.source "AllShareFrameworkCoreNativeAPI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native browse(Ljava/lang/String;Ljava/lang/String;IIILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native creatObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native createParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native deleteResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native destroyObject(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native exportResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getDefaultUserAgent()Ljava/lang/String;
.end method

.method public static native getDevice(ILjava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)I
.end method

.method public static native getDeviceList(ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getDeviceStatusInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getGenericDeviceDescFieldValue(ILjava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public static native getGenericDeviceStateVariableValue(ILjava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public static native getKiesDeviceDescription(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I
.end method

.method public static native getKiesServiceDescription(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I
.end method

.method public static native getKiesUserAgent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Message;)I
.end method

.method public static native getMediaInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getMute(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getPlayPositionInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getProtocolInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getSearchCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getSortCapabilities(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getSystemUpdateID(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getTransferProgress(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getTransportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getVolume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native getZoomPort(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native importResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native initializeAllShareFrameworkCDCore()I
.end method

.method public static native initializeAllShareFrameworkCore()I
.end method

.method public static native joinParty(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native kiesOptionalCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native leaveParty(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I
.end method

.method public static native mrcpGetStoppedReason(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native mrcpGetUserAgent()Ljava/lang/String;
.end method

.method public static native mrcpSetUserAgent(Ljava/lang/String;)V
.end method

.method public static native mrcpSkipDynamicBuffer(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native mscpGetUserAgent()Ljava/lang/String;
.end method

.method public static native mscpSetUserAgent(Ljava/lang/String;)V
.end method

.method public static native nextPlay(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native notifyNICRemoved(Ljava/lang/String;)I
.end method

.method public static native pause(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I
.end method

.method public static native playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I
.end method

.method public static native playLocalContentEx(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;JLandroid/os/Bundle;)I
.end method

.method public static native playRelayContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLandroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I
.end method

.method public static native playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I
.end method

.method public static native postObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native postObjectStopTransfer(I)I
.end method

.method public static native registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native rescanDevice()I
.end method

.method public static native rescanDeviceByAppID(Ljava/lang/String;)I
.end method

.method public static native rescanDeviceByType(I)I
.end method

.method public static native resume(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native seek(Ljava/lang/String;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native sendGenericDeviceEventNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native sendGenericDeviceResponse(ILjava/lang/StringBuffer;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native setBGMListSlideshow(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;",
            ")I"
        }
    .end annotation
.end method

.method public static native setBGMVolumeSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setDefaultUserAgent(Ljava/lang/String;)V
.end method

.method public static native setGenericDeviceDescFieldValue(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method public static native setKiesAutoSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setKiesSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setKiesUserAgent(Ljava/lang/String;)I
.end method

.method public static native setListSlideshow(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;",
            ")I"
        }
    .end annotation
.end method

.method public static native setMute(Ljava/lang/String;ZLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setNextLocalTranportUri(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setNextTranportUri(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native setScreenStatus(I)I
.end method

.method public static native setVolume(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native startGenericDevice(I)I
.end method

.method public static native startKiesCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
.end method

.method public static native startMSCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
.end method

.method public static native startMrcp()I
.end method

.method public static native startSlideshow(Ljava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native stopGenericDevice(I)I
.end method

.method public static native stopKiesCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
.end method

.method public static native stopMSCP(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/ProfileID;)I
.end method

.method public static native stopMrcp()I
.end method

.method public static native stopSlideshow(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native stopTransResource(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I
.end method

.method public static native subscribeDMR(Ljava/lang/String;)I
.end method

.method public static native subscribeDMS(Ljava/lang/String;)I
.end method

.method public static native subscribeKiesDevice(Ljava/lang/String;)I
.end method

.method public static native terminateAllShareFrameworkCDCore()I
.end method

.method public static native terminateAllShareFrameworkCore()I
.end method

.method public static native unSubscribeDMR(Ljava/lang/String;)I
.end method

.method public static native unSubscribeDMS(Ljava/lang/String;)I
.end method

.method public static native unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native unsubscribeKiesDevice(Ljava/lang/String;)I
.end method
