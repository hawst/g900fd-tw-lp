.class public interface abstract Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;
.super Ljava/lang/Object;
.source "IActionHandler.java"


# static fields
.field public static final ACTION_TYPE_ASYNC:Ljava/lang/String; = "ACTION_TYPE_ASYNC"

.field public static final ACTION_TYPE_SYNC:Ljava/lang/String; = "ACTION_TYPE_SYNC"

.field public static final REQUEST_MESSAGE_DATA:Ljava/lang/String; = "REQUEST_MESSAGE_DATA"


# virtual methods
.method public abstract finiActionHandler()V
.end method

.method public abstract getActionList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getActionType()Ljava/lang/String;
.end method

.method public abstract getHandlerID()Ljava/lang/String;
.end method

.method public abstract initActionHandler()V
.end method
