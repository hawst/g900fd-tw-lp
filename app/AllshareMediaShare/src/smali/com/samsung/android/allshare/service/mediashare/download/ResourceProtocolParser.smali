.class public Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;
.super Ljava/lang/Object;
.source "ResourceProtocolParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$1;,
        Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    }
.end annotation


# static fields
.field public static final AAC_ADTS_320:Ljava/lang/String; = "AAC_ADTS_320"

.field public static final AAC_ISO_320:Ljava/lang/String; = "AAC_ISO_320"

.field public static final AVC_MP4_BL_CIF15_AAC_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_520"

.field public static final BMP:Ljava/lang/String; = "BMP"

.field public static final FILE_EXTENSION_3GP:Ljava/lang/String; = ".3gp"

.field public static final FILE_EXTENSION_3GPP:Ljava/lang/String; = ".3gpp"

.field public static final FILE_EXTENSION_AAC:Ljava/lang/String; = ".aac"

.field public static final FILE_EXTENSION_ADTS:Ljava/lang/String; = ".adts"

.field public static final FILE_EXTENSION_ASF:Ljava/lang/String; = ".asf"

.field public static final FILE_EXTENSION_AVI:Ljava/lang/String; = ".avi"

.field public static final FILE_EXTENSION_BMP:Ljava/lang/String; = ".bmp"

.field public static final FILE_EXTENSION_FLV:Ljava/lang/String; = ".flv"

.field public static final FILE_EXTENSION_JPEG:Ljava/lang/String; = ".jpeg"

.field public static final FILE_EXTENSION_LPCM:Ljava/lang/String; = ".lpcm"

.field public static final FILE_EXTENSION_MKV:Ljava/lang/String; = ".mkv"

.field public static final FILE_EXTENSION_MP3:Ljava/lang/String; = ".mp3"

.field public static final FILE_EXTENSION_MP4:Ljava/lang/String; = ".mp4"

.field public static final FILE_EXTENSION_MPEG:Ljava/lang/String; = ".mpeg"

.field public static final FILE_EXTENSION_PNG:Ljava/lang/String; = ".png"

.field public static final FILE_EXTENSION_RAW:Ljava/lang/String; = ".raw"

.field public static final FILE_EXTENSION_WMA:Ljava/lang/String; = ".wma"

.field public static final FILE_EXTENSION_WMV:Ljava/lang/String; = ".wmv"

.field public static final JPEG_LRG:Ljava/lang/String; = "JPEG_LRG"

.field public static final JPEG_MED:Ljava/lang/String; = "JPEG_MED"

.field public static final JPEG_SM:Ljava/lang/String; = "JPEG_SM"

.field public static final JPEG_TN:Ljava/lang/String; = "JPEG_TN"

.field public static final LPCM:Ljava/lang/String; = "LPCM"

.field public static final MIME_AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final MIME_AUDIO_AAC:Ljava/lang/String; = "audio/aac"

.field public static final MIME_AUDIO_ADTS:Ljava/lang/String; = "audio/vnd.dlna.adts"

.field public static final MIME_AUDIO_L16:Ljava/lang/String; = "audio/L16"

.field public static final MIME_AUDIO_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_AUDIO_MPEG:Ljava/lang/String; = "audio/mpeg"

.field public static final MIME_AUDIO_RAW:Ljava/lang/String; = "audio/vnd.dolby.dd-raw"

.field public static final MIME_AUDIO_SONY_OMA:Ljava/lang/String; = "audio/x-sony-oma"

.field public static final MIME_AUDIO_WMA:Ljava/lang/String; = "audio/x-ms-wma"

.field public static final MIME_IMAGE_BMP:Ljava/lang/String; = "image/bmp"

.field public static final MIME_IMAGE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final MIME_IMAGE_PNG:Ljava/lang/String; = "image/png"

.field public static final MIME_VIDEO_3GPP:Ljava/lang/String; = "video/3gpp"

.field public static final MIME_VIDEO_ASF:Ljava/lang/String; = "video/x-ms-asf"

.field public static final MIME_VIDEO_AVI:Ljava/lang/String; = "video/avi"

.field public static final MIME_VIDEO_DIVX:Ljava/lang/String; = "video/x-divx"

.field public static final MIME_VIDEO_FLV:Ljava/lang/String; = "video/flv"

.field public static final MIME_VIDEO_MATROSKA:Ljava/lang/String; = "video/x-matroska"

.field public static final MIME_VIDEO_MKV:Ljava/lang/String; = "video/mkv"

.field public static final MIME_VIDEO_MP4:Ljava/lang/String; = "video/mp4"

.field public static final MIME_VIDEO_MPEG:Ljava/lang/String; = "video/mpeg"

.field public static final MIME_VIDEO_TTS:Ljava/lang/String; = "video/vnd.dlna.mpeg-tts"

.field public static final MIME_VIDEO_WMV:Ljava/lang/String; = "video/x-ms-wmv"

.field public static final MIME_VIDEO_X_FLV:Ljava/lang/String; = "video/x-flv"

.field public static final MIME_VIDEO_X_MKV:Ljava/lang/String; = "video/x-mkv"

.field public static final MKV:Ljava/lang/String; = "MKV"

.field public static final MP3:Ljava/lang/String; = "MP3"

.field public static final PNG_LRG:Ljava/lang/String; = "PNG_LRG"

.field public static final PNG_TN:Ljava/lang/String; = "PNG_TN"

.field public static final WMABASE:Ljava/lang/String; = "WMABASE"

.field private static parser:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;


# instance fields
.field private mPostfixDictionary:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;

    invoke-direct {v0}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->parser:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    .line 166
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/jpeg"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->JPEG:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/png"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->PNG:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/bmp"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->BMP:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dolby.dd-raw"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->RAW:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dlna.adts"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ADTS:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-asf"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ASF:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/3gpp"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/3gpp"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/L16"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->LPCM:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mp4"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mp4"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mpeg"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP3:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mpeg"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/vnd.dlna.mpeg-tts"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/x-ms-wma"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMA:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-wmv"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/avi"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-divx"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/aac"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AAC:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mkv"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MKV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-mkv"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MKV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-matroska"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MKV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/flv"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->FLV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-flv"

    sget-object v2, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->FLV:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method private getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 259
    const-string v0, ".tmp"

    return-object v0
.end method

.method private getFileExtension(Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .prologue
    .line 193
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$1;->$SwitchMap$com$samsung$android$allshare$service$mediashare$download$ResourceProtocolParser$FILE_EXTENSITON_TYPE:[I

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 231
    const-string v0, ""

    :goto_0
    return-object v0

    .line 195
    :pswitch_0
    const-string v0, ".adts"

    goto :goto_0

    .line 197
    :pswitch_1
    const-string v0, ".asf"

    goto :goto_0

    .line 199
    :pswitch_2
    const-string v0, ".bmp"

    goto :goto_0

    .line 201
    :pswitch_3
    const-string v0, ".3gpp"

    goto :goto_0

    .line 203
    :pswitch_4
    const-string v0, ".3gp"

    goto :goto_0

    .line 205
    :pswitch_5
    const-string v0, ".jpeg"

    goto :goto_0

    .line 207
    :pswitch_6
    const-string v0, ".lpcm"

    goto :goto_0

    .line 209
    :pswitch_7
    const-string v0, ".mp3"

    goto :goto_0

    .line 211
    :pswitch_8
    const-string v0, ".mp4"

    goto :goto_0

    .line 213
    :pswitch_9
    const-string v0, ".mpeg"

    goto :goto_0

    .line 215
    :pswitch_a
    const-string v0, ".png"

    goto :goto_0

    .line 217
    :pswitch_b
    const-string v0, ".raw"

    goto :goto_0

    .line 219
    :pswitch_c
    const-string v0, ".wma"

    goto :goto_0

    .line 221
    :pswitch_d
    const-string v0, ".wmv"

    goto :goto_0

    .line 223
    :pswitch_e
    const-string v0, ".avi"

    goto :goto_0

    .line 225
    :pswitch_f
    const-string v0, ".aac"

    goto :goto_0

    .line 227
    :pswitch_10
    const-string v0, ".mkv"

    goto :goto_0

    .line 229
    :pswitch_11
    const-string v0, ".flv"

    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static declared-synchronized getParser()Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;
    .locals 2

    .prologue
    .line 159
    const-class v0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->parser:Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getFileExtensionByMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 235
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    :goto_0
    return-object v1

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 240
    .local v0, "type":Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    if-nez v0, :cond_2

    .line 241
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 244
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->getFileExtension(Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser$FILE_EXTENSITON_TYPE;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
