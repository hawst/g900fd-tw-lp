.class Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;
.super Ljava/lang/Object;
.source "DeviceStateHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPErrorNotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AVSCallback"
.end annotation


# instance fields
.field public mActionName:Ljava/lang/String;

.field public mErrorCode:I

.field public mIsNotified:Z

.field public mRequestID:I

.field public mResponse:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mRequestID:I

    .line 258
    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mErrorCode:I

    .line 260
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mActionName:Ljava/lang/String;

    .line 262
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mResponse:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 264
    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mIsNotified:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAVTControlResponse(ILjava/lang/String;ILcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "response"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .prologue
    .line 269
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[DeviceStateHandle.AVSCallback][onAVTControlResponse]: "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "actionName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "errorCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mRequestID:I

    .line 274
    iput p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mErrorCode:I

    .line 275
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mActionName:Ljava/lang/String;

    .line 276
    iput-object p4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mResponse:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 278
    monitor-enter p0

    .line 279
    :try_start_0
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[DeviceStateHandle.AVSCallback][onAVTControlResponse]: "

    const-string v2, "notify the waiter."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mIsNotified:Z

    .line 282
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 283
    monitor-exit p0

    .line 284
    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onErrorNotify(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 290
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[DeviceStateHandle.AVSCallback][onErrorNotify]: "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "actionName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "errorCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "description: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iput p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mRequestID:I

    .line 295
    iput p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mErrorCode:I

    .line 296
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mActionName:Ljava/lang/String;

    .line 298
    monitor-enter p0

    .line 299
    :try_start_0
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[DeviceStateHandle.AVSCallback][onErrorNotify]: "

    const-string v2, "notify the waiter."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mIsNotified:Z

    .line 302
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 303
    monitor-exit p0

    .line 304
    return-void

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
