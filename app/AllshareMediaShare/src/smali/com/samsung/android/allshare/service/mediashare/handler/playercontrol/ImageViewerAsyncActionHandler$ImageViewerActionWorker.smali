.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "ImageViewerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageViewerActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$1;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "x3"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p6, "x6"    # J
    .param p8, "x7"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-direct/range {p0 .. p8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->playItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->stopItem(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Landroid/net/Uri;

    .prologue
    .line 173
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "x4"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p5, "x5"    # J

    .prologue
    .line 173
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p7, "x7"    # J

    .prologue
    .line 173
    invoke-direct/range {p0 .. p8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestPlayerState(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p3, "x3"    # J
    .param p5, "x4"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 173
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Landroid/net/Uri;)Z
    .locals 3
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "itemBundle"    # Landroid/os/Bundle;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 869
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 881
    :cond_0
    :goto_0
    return v1

    .line 872
    :cond_1
    if-eqz p2, :cond_0

    .line 873
    const-string v2, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 875
    .local v0, "prepareUri":Landroid/net/Uri;
    invoke-virtual {p3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 876
    const/4 p2, 0x0

    .line 877
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private canRequestNextPlay(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "itemBundle"    # Landroid/os/Bundle;
    .param p3, "itemInfo"    # Ljava/lang/String;
    .param p4, "isLocal"    # Z

    .prologue
    const/4 v1, 0x0

    .line 886
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 902
    :cond_0
    :goto_0
    return v1

    .line 889
    :cond_1
    if-eqz p2, :cond_0

    .line 890
    const/4 v0, 0x0

    .line 891
    .local v0, "prepareInfo":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 892
    const-string v2, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 896
    :goto_1
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 897
    const/4 p2, 0x0

    .line 898
    const/4 v1, 0x1

    goto :goto_0

    .line 894
    :cond_2
    const-string v2, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private isSupportProtocol(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "service"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 857
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 858
    .local v0, "response":Landroid/os/Bundle;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->mrcpGetServiceDescription(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I

    .line 859
    const-string v2, "BUNDLE_STRING_SERVICE_DESCRIPTION"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 862
    .local v1, "serviceDescription":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 863
    const/4 v2, 0x0

    .line 865
    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_0
.end method

.method private playItem(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    .locals 20
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "contentItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .param p3, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p6, "startingPosition"    # J
    .param p8, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v11

    .line 605
    .local v11, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 608
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v2

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 610
    new-instance v17, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 611
    .local v17, "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v19, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 612
    .local v19, "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 613
    move-object/from16 v0, v19

    iget v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    if-eqz v2, :cond_0

    .line 614
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 617
    :cond_0
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 618
    .local v10, "bundle":Landroid/os/Bundle;
    new-instance v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 619
    .local v8, "id":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 621
    new-instance v15, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 622
    .local v15, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v2, "Play"

    invoke-virtual {v15, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 623
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "playItem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v4, p2

    move-object/from16 v9, p8

    invoke-virtual/range {v2 .. v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I

    move-result v18

    .line 626
    .local v18, "retValue":I
    if-eqz v18, :cond_1

    .line 627
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 628
    const-string v2, "BUNDLE_ENUM_ERROR"

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 683
    :goto_0
    return-void

    .line 632
    :cond_1
    :try_start_1
    iget v14, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 633
    .local v14, "key":I
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 634
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    const-wide/32 v2, 0xea60

    invoke-virtual {v15, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v16

    .line 636
    .local v16, "received":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638
    sget-object v13, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 639
    .local v13, "error":Lcom/samsung/android/allshare/ERROR;
    invoke-virtual {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v12

    .line 640
    .local v12, "ec":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "playMediaItem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[response] mRequestID :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    if-eqz v16, :cond_2

    const/16 v2, 0x2c1

    if-ne v12, v2, :cond_2

    .line 643
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "playMediaItem"

    const-string v4, "setAVTUri error 705, try again......"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 649
    new-instance v15, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .end local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-direct {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 650
    .restart local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v2, "Play"

    invoke-virtual {v15, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 653
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 654
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v2

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v4, p2

    move-object/from16 v9, p8

    invoke-virtual/range {v2 .. v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->play(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Ljava/lang/String;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Ljava/lang/String;)I

    .line 655
    iget v2, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 658
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5300()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v2

    iget v3, v8, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    const-wide/32 v2, 0xea60

    invoke-virtual {v15, v2, v3}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 662
    invoke-virtual {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v12

    .line 663
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "playMediaServerItem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[retry response] mRequestID :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v2, v12}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v13

    .line 670
    if-eqz p5, :cond_3

    .line 671
    const-string v2, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, p5

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 676
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move-object/from16 v0, p2

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V
    invoke-static {v2, v10, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)V

    .line 677
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 678
    const-string v2, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v13}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 681
    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 673
    :cond_3
    :try_start_2
    const-string v2, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-wide/from16 v0, p6

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 681
    .end local v8    # "id":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v10    # "bundle":Landroid/os/Bundle;
    .end local v12    # "ec":I
    .end local v13    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v14    # "key":I
    .end local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .end local v16    # "received":Z
    .end local v17    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v18    # "retValue":I
    .end local v19    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    :catchall_0
    move-exception v2

    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
.end method

.method private requestAddAVSListener(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 906
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 907
    return-void
.end method

.method private requestNextPlay(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 10
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "dmrUdn"    # Ljava/lang/String;

    .prologue
    .line 822
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 823
    .local v3, "id":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v4, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 824
    .local v4, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v6, "Next"

    invoke-virtual {v4, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 825
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "requestNextPlay"

    const-string v8, "Next action"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v6

    invoke-virtual {v6, p2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->nextPlay(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 827
    iget v6, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-direct {p0, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 828
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$7000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    const-wide/16 v6, 0x7530

    invoke-virtual {v4, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v5

    .line 834
    .local v5, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$7100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v6

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 836
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v2

    .line 837
    .local v2, "errorCode":I
    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 838
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$7200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "requestNextPlay"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mRequestID :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Next"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has wrong response"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v6, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$7300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v1

    .line 843
    .local v1, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    const-string v6, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    .end local v1    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_0
    const-string v6, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 853
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 854
    return-void

    .line 846
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$7400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "requestNextPlay"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mRequestID :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Next"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ErrorCode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestPlayerState(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 11
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 360
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 361
    .local v4, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 362
    .local v2, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v7, "GetTransportInfo"

    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 364
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v7

    iget-object v8, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v7, v8, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getTrnasportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v6

    .line 365
    .local v6, "result":I
    if-eqz v6, :cond_0

    .line 366
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    const-string v9, "invoke native function failed!"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_0
    iget v7, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 369
    iget v1, v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 370
    .local v1, "key":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    const-wide/16 v8, 0x7530

    invoke-virtual {v2, v8, v9}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 374
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 377
    .local v5, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 378
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v7

    if-nez v7, :cond_1

    .line 379
    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 381
    .local v3, "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    iget v7, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    packed-switch v7, :pswitch_data_0

    .line 395
    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 399
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestPlayerState  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumToString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v7, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    .end local v3    # "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 410
    return-void

    .line 383
    .restart local v3    # "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    :pswitch_0
    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 384
    goto :goto_0

    .line 387
    :pswitch_1
    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 388
    goto :goto_0

    .line 391
    :pswitch_2
    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 392
    goto :goto_0

    .line 404
    .end local v3    # "playerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "requestPlayerState"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "monitor error code  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v7, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    sget-object v8, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string v7, "BUNDLE_ENUM_ERROR"

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 381
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private requestShowLocalContent(Landroid/os/Bundle;Landroid/net/Uri;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    .locals 19
    .param p1, "bundleItem"    # Landroid/os/Bundle;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p4, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p5, "startingPosition"    # J

    .prologue
    .line 415
    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    .line 416
    :cond_0
    if-nez p2, :cond_1

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    .local v8, "error":Ljava/lang/String;
    :goto_0
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    .line 418
    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    .line 419
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "requestShowLocalContent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ERROR : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    .end local v8    # "error":Ljava/lang/String;
    :goto_1
    return-void

    .line 416
    :cond_1
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 423
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "requestShowLocalContent(uri)"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v12

    .line 425
    .local v12, "filePath":Ljava/lang/String;
    if-nez v12, :cond_3

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "requestShowLocalContent(uri)"

    const-string v5, "filePath is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p4

    move-wide/from16 v12, p5

    invoke-direct/range {v9 .. v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    goto :goto_1

    .line 430
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move-object/from16 v0, p2

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getMetadata(Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    invoke-static {v3, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/net/Uri;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v2

    .line 431
    .local v2, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    iget-object v13, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    iget-object v14, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p3

    move-object/from16 v15, p4

    move-wide/from16 v16, p5

    invoke-direct/range {v9 .. v17}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestShowLocalContent(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V

    goto :goto_1
.end method

.method private requestShowLocalContent(Landroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/media/ContentInfo;J)V
    .locals 23
    .param p1, "bundleItem"    # Landroid/os/Bundle;
    .param p2, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "mimetype"    # Ljava/lang/String;
    .param p6, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p7, "startingPosition"    # J

    .prologue
    .line 438
    if-eqz p3, :cond_0

    if-nez p1, :cond_2

    .line 439
    :cond_0
    if-nez p3, :cond_1

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    .local v10, "error":Ljava/lang/String;
    :goto_0
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    .line 441
    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ERROR : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    .end local v10    # "error":Ljava/lang/String;
    .end local p4    # "title":Ljava/lang/String;
    .end local p5    # "mimetype":Ljava/lang/String;
    :goto_1
    return-void

    .line 439
    .restart local p4    # "title":Ljava/lang/String;
    .restart local p5    # "mimetype":Ljava/lang/String;
    :cond_1
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 446
    :cond_2
    const-string v5, "content://"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 448
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "error":Ljava/lang/String;
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    .line 449
    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    .line 450
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ERROR : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". can not find filepath in Media DB - should be updated properly in app-side"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 455
    .end local v10    # "error":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p5

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v12

    .line 459
    .local v12, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 462
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 464
    new-instance v17, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 465
    .local v17, "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v20, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;-><init>()V

    .line 466
    .local v20, "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v5, v6, v0, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getPlayState(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 467
    move-object/from16 v0, v20

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;->state:I

    if-eqz v5, :cond_4

    .line 468
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 470
    :cond_4
    const-string v5, "file://"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 471
    const/4 v5, 0x7

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 474
    :cond_5
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 476
    .local v4, "bundle":Landroid/os/Bundle;
    new-instance v21, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 477
    .local v21, "tempMetadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz p5, :cond_8

    .end local p5    # "mimetype":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p5

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 479
    if-eqz p4, :cond_9

    .end local p4    # "title":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p4

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 480
    const-string v5, ""

    move-object/from16 v0, v21

    iput-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->subtitlePath:Ljava/lang/String;

    .line 482
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    const-string v6, "image/jpg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 483
    const-string v5, "image/jpeg"

    move-object/from16 v0, v21

    iput-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 486
    :cond_6
    if-eqz p3, :cond_7

    .line 487
    const-string v5, "http"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 488
    const-string v5, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 490
    const-string v5, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v6, "WEB_CONTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_7
    :goto_4
    const-string v5, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 509
    if-eqz p6, :cond_c

    .line 510
    const-string v5, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, p6

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 516
    :goto_5
    new-instance v18, Landroid/os/Bundle;

    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    .line 517
    .local v18, "result":Landroid/os/Bundle;
    new-instance v15, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 518
    .local v15, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v5, "Play"

    invoke-virtual {v15, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 520
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I

    move-result v19

    .line 523
    .local v19, "retValue":I
    const-string v5, "BUNDLE_INT_REQUEST_ID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 524
    .local v14, "key":I
    const-string v5, "BUNDLE_STRING_PLAYING_URI"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 526
    .local v11, "contentUri":Ljava/lang/String;
    if-eqz v19, :cond_d

    .line 527
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 529
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v4, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 530
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1

    .line 477
    .end local v11    # "contentUri":Ljava/lang/String;
    .end local v14    # "key":I
    .end local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .end local v18    # "result":Landroid/os/Bundle;
    .end local v19    # "retValue":I
    .restart local p4    # "title":Ljava/lang/String;
    .restart local p5    # "mimetype":Ljava/lang/String;
    :cond_8
    :try_start_1
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/allshare/service/mediashare/utility/HttpMimeUtils;->parseMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    goto/16 :goto_2

    .line 479
    .end local p5    # "mimetype":Ljava/lang/String;
    :cond_9
    const-string p4, "local content"

    goto/16 :goto_3

    .line 492
    .end local p4    # "title":Ljava/lang/String;
    :cond_a
    const-string v5, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 493
    const-string v5, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 495
    const-string v5, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v6, "LOCAL_CONTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 581
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v17    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v20    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .end local v21    # "tempMetadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    :catchall_0
    move-exception v5

    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5

    .line 498
    .restart local v4    # "bundle":Landroid/os/Bundle;
    .restart local v17    # "requestid":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .restart local v20    # "state":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/PlayerState;
    .restart local v21    # "tempMetadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    :cond_b
    :try_start_2
    const-string v5, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const-string v5, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v6, "LOCAL_CONTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 512
    :cond_c
    const-string v5, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-wide/from16 v0, p7

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_5

    .line 533
    .restart local v11    # "contentUri":Ljava/lang/String;
    .restart local v14    # "key":I
    .restart local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    .restart local v18    # "result":Landroid/os/Bundle;
    .restart local v19    # "retValue":I
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 534
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    const-wide/32 v6, 0xea60

    invoke-virtual {v15, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v16

    .line 539
    .local v16, "received":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    sget-object v10, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 541
    .local v10, "error":Lcom/samsung/android/allshare/ERROR;
    invoke-virtual {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v13

    .line 542
    .local v13, "ec":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[response] mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", error code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    if-eqz v16, :cond_e

    const/16 v5, 0x2c1

    if-ne v13, v5, :cond_e

    .line 546
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    const-string v7, "receive 705, try again....."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    new-instance v15, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    .end local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    invoke-direct {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 553
    .restart local v15    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v5, "Play"

    invoke-virtual {v15, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 556
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 557
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playLocalContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Landroid/os/Bundle;)I

    .line 558
    const-string v5, "BUNDLE_INT_REQUEST_ID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 561
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 562
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4200()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    const-wide/32 v6, 0xea60

    invoke-virtual {v15, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 566
    invoke-virtual {v15}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v13

    .line 567
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestShowLocalContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[retry response] mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", error code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v5, "BUNDLE_STRING_PLAYING_URI"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 574
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v5, v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v10

    .line 576
    const-string v5, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 578
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v5, v4, v11}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$3600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 579
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 581
    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1
.end method

.method private requestSyncStop(Ljava/lang/String;)Z
    .locals 6
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 699
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 700
    .local v2, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 702
    .local v1, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Stop"

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 703
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 705
    iget v0, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    .line 706
    .local v0, "key":I
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 707
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5700()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v3

    .line 711
    .local v3, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private requestsetNextTranportUri(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 39
    .param p1, "dmrUdn"    # Ljava/lang/String;
    .param p2, "bundleItem"    # Landroid/os/Bundle;

    .prologue
    .line 717
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 718
    :cond_0
    if-nez p1, :cond_1

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    .line 720
    .local v10, "error":Ljava/lang/String;
    :goto_0
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    .line 721
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$5900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestsetNextTranportUri"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ERROR : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    const/4 v5, 0x0

    .line 818
    .end local v10    # "error":Ljava/lang/String;
    :goto_1
    return v5

    .line 718
    :cond_1
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 725
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->isSupportSetNextAVT(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 726
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestsetNextTranportUri"

    const-string v7, "Device doesn`t support NextTranport!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    new-instance v18, Landroid/os/Bundle;

    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    .line 728
    .local v18, "b":Landroid/os/Bundle;
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 729
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 731
    const/4 v5, 0x1

    goto :goto_1

    .line 733
    .end local v18    # "b":Landroid/os/Bundle;
    :cond_3
    new-instance v17, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 734
    .local v17, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v31, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct/range {v31 .. v31}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 735
    .local v31, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v5, "SetNextAVTransportURI"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 738
    const-string v5, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 739
    .local v13, "filePath":Ljava/lang/String;
    const-string v5, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v38

    check-cast v38, Landroid/net/Uri;

    .line 740
    .local v38, "uri":Landroid/net/Uri;
    const-string v5, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 741
    .local v32, "objectID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v13}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 742
    if-nez v13, :cond_4

    if-eqz v38, :cond_4

    .line 743
    invoke-static/range {v38 .. v38}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v13

    .line 745
    :cond_4
    if-eqz v13, :cond_a

    .line 746
    const/16 v28, 0x1

    .line 750
    .local v28, "isLocal":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestsetNextTranportUri"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setNexTransportUri, isLocal: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    if-eqz v28, :cond_c

    .line 752
    const-string v5, "BUNDLE_STRING_TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 753
    .local v37, "title":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 754
    .local v30, "mimeType":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 756
    .local v15, "subtitlePath":Ljava/lang/String;
    if-nez v15, :cond_5

    .line 757
    const-string v15, ""

    .line 759
    :cond_5
    move-object v4, v13

    .line 760
    .local v4, "absFilePath":Ljava/lang/String;
    const-string v5, "file://"

    invoke-virtual {v13, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 761
    const/4 v5, 0x7

    invoke-virtual {v13, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 763
    :cond_6
    if-nez v30, :cond_7

    .line 764
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/utility/HttpMimeUtils;->parseMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 766
    :cond_7
    new-instance v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v14}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>()V

    .line 767
    .local v14, "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    if-eqz v37, :cond_b

    .end local v37    # "title":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v37

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->title:Ljava/lang/String;

    .line 768
    move-object/from16 v0, v30

    iput-object v0, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 769
    iget-object v5, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    const-string v6, "image/jpg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 770
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestsetNextTranportUri"

    const-string v7, "fix to image/jpeg"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    const-string v5, "image/jpeg"

    iput-object v5, v14, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;->mimeType:Ljava/lang/String;

    .line 774
    :cond_8
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v11

    const/16 v16, 0x0

    move-object/from16 v12, p1

    invoke-virtual/range {v11 .. v17}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setNextLocalTranportUri(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 776
    move-object/from16 v0, v17

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    .line 790
    .end local v4    # "absFilePath":Ljava/lang/String;
    .end local v14    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v15    # "subtitlePath":Ljava/lang/String;
    .end local v30    # "mimeType":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v29, v0

    .line 791
    .local v29, "key":I
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6400()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v5, v6, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    const-wide/16 v6, 0x7530

    move-object/from16 v0, v31

    invoke-virtual {v0, v6, v7}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v34

    .line 795
    .local v34, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v5

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    new-instance v35, Landroid/os/Bundle;

    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    .line 799
    .local v35, "retBundle":Landroid/os/Bundle;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v27

    .line 800
    .local v27, "errorCode":I
    if-eqz v34, :cond_9

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 801
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestAVControl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mRequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v17

    iget v8, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "SetNextAVTransportURI"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has wrong response"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    move/from16 v0, v27

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v5, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v10

    .line 806
    .local v10, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v5, "BUNDLE_STRING_ID"

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const-string v5, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v10}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    .end local v10    # "error":Lcom/samsung/android/allshare/ERROR;
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 818
    const/4 v5, 0x1

    goto/16 :goto_1

    .line 748
    .end local v27    # "errorCode":I
    .end local v28    # "isLocal":Z
    .end local v29    # "key":I
    .end local v34    # "result":Z
    .end local v35    # "retBundle":Landroid/os/Bundle;
    :cond_a
    const/16 v28, 0x0

    .restart local v28    # "isLocal":Z
    goto/16 :goto_2

    .line 767
    .restart local v4    # "absFilePath":Ljava/lang/String;
    .restart local v14    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .restart local v15    # "subtitlePath":Ljava/lang/String;
    .restart local v30    # "mimeType":Ljava/lang/String;
    .restart local v37    # "title":Ljava/lang/String;
    :cond_b
    const-string v37, "local content"

    goto/16 :goto_3

    .line 778
    .end local v4    # "absFilePath":Ljava/lang/String;
    .end local v14    # "metadata":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .end local v15    # "subtitlePath":Ljava/lang/String;
    .end local v30    # "mimeType":Ljava/lang/String;
    .end local v37    # "title":Ljava/lang/String;
    :cond_c
    const-string v5, "BUNDLE_STRING_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 779
    .local v36, "serverUdn":Ljava/lang/String;
    new-instance v25, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;

    invoke-direct/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;-><init>()V

    .line 780
    .local v25, "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    move-object/from16 v0, v25

    move-object/from16 v1, v36

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v26

    .line 781
    .local v26, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->discard()V

    .line 782
    if-eqz v26, :cond_d

    .line 783
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$2300()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move-object/from16 v2, v17

    invoke-virtual {v5, v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->setNextTranportUri(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 784
    move-object/from16 v0, v17

    iget v5, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestAddAVSListener(I)V

    goto/16 :goto_4

    .line 786
    :cond_d
    const/16 v21, 0x0

    const-wide/16 v22, 0x0

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v19, p0

    move-object/from16 v20, p2

    invoke-direct/range {v19 .. v24}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V

    .line 787
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 809
    .end local v25    # "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    .end local v26    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v36    # "serverUdn":Ljava/lang/String;
    .restart local v27    # "errorCode":I
    .restart local v29    # "key":I
    .restart local v34    # "result":Z
    .restart local v35    # "retBundle":Landroid/os/Bundle;
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$6800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestAVControl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "after response,RequestID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v17

    iget v8, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "SetNextAVTransportURI"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ErrorCode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v27

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getResponse()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 813
    .local v33, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mPreparedItemMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$800()Ljava/util/HashMap;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    const-string v5, "BUNDLE_STRING_ID"

    move-object/from16 v0, v33

    iget-object v6, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->dmrUDN:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    const-string v5, "BUNDLE_ENUM_ERROR"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private responseShowFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    .locals 3
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p3, "startingPosition"    # J
    .param p5, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 587
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;->access$4500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "responseShowFail"

    invoke-static {v1, v2, p5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 590
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 591
    if-eqz p2, :cond_0

    .line 592
    const-string v1, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 596
    :goto_0
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 599
    return-void

    .line 594
    :cond_0
    const-string v1, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private stopItem(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V
    .locals 3
    .param p1, "deviceItem"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    .prologue
    .line 686
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 688
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 689
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 690
    const-string v1, "BUNDLE_ENUM_ERROR"

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;->response(Landroid/os/Bundle;)V

    .line 696
    return-void

    .line 692
    :cond_0
    const-string v1, "BUNDLE_ENUM_ERROR"

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 181
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/ImageViewerAsyncActionHandler$ImageViewerActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 357
    return-void
.end method
