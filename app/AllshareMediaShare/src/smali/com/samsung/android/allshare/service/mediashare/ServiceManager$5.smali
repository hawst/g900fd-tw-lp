.class Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;
.super Lcom/sec/android/allshare/iface/ISubscriber$Stub;
.source "ServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 0

    .prologue
    .line 919
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-direct {p0}, Lcom/sec/android/allshare/iface/ISubscriber$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getServiceVersion()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 975
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/ServiceConfig;->getServiceVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public requestCVAsync(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 1
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 930
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->requestCVAsyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z

    move-result v0

    return v0
.end method

.method public requestCVSync(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 1
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 943
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->requestCVSyncToHandler(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v0

    return-object v0
.end method

.method public subscribeEvent(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    .locals 1
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 956
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->requestEventSubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z

    move-result v0

    return v0
.end method

.method public unsubscribeEvent(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 1
    .param p1, "subscriber"    # Ljava/lang/String;
    .param p2, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 969
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/ServiceManager$5;->this$0:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->requestEventUnsubscription(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)V

    .line 971
    return-void
.end method
