.class Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;
.super Ljava/lang/Object;
.source "KiesAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 30

    .prologue
    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v3}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v25

    .line 140
    .local v25, "bundle":Landroid/os/Bundle;
    const-string v3, "BUNDLE_STRING_ID"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 142
    .local v28, "id":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->deviceListManager:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$000()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v3

    const/16 v4, 0x8

    move-object/from16 v0, v28

    invoke-virtual {v3, v4, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v27

    .line 143
    .local v27, "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v27, :cond_1

    .line 144
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    sget-object v4, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;Ljava/lang/String;)V

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 151
    .local v12, "ipAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v3}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_OPTIONAL_COMMAND"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 153
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_COMMANDNAME"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 155
    .local v26, "commandName":Ljava/lang/String;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_ARG1"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 156
    .local v24, "arg1":Ljava/lang/String;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_ARG2"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 158
    .local v8, "arg2":Ljava/lang/String;
    new-instance v29, Landroid/os/Bundle;

    invoke-direct/range {v29 .. v29}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v29, "resBundle":Landroid/os/Bundle;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_COMMANDNAME"

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_ARG1"

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_ARG2"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, v3, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;)Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    move-result-object v5

    const-wide/32 v6, 0x1d4c0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;J)V

    .line 168
    .local v2, "listener":Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;
    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->setResponseBundle(Landroid/os/Bundle;)V

    .line 169
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->addKiesActionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;)V

    .line 171
    new-instance v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 172
    .local v9, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v3

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object v5, v12

    move-object/from16 v6, v26

    move-object/from16 v7, v24

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->optionalCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 175
    iget v3, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->setMonitorRequestID(I)V

    goto/16 :goto_0

    .line 177
    .end local v2    # "listener":Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;
    .end local v8    # "arg2":Ljava/lang/String;
    .end local v9    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v24    # "arg1":Ljava/lang/String;
    .end local v26    # "commandName":Ljava/lang/String;
    .end local v29    # "resBundle":Landroid/os/Bundle;
    :cond_2
    new-instance v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v9}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 179
    .restart local v9    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_PORTNUMBER"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 181
    .local v13, "portNumber":Ljava/lang/String;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_MODEL"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 183
    .local v14, "deviceModel":Ljava/lang/String;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_CONNECTIONTYPE"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 186
    .local v15, "connectionType":Ljava/lang/String;
    new-instance v29, Landroid/os/Bundle;

    invoke-direct/range {v29 .. v29}, Landroid/os/Bundle;-><init>()V

    .line 187
    .restart local v29    # "resBundle":Landroid/os/Bundle;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_PORTNUMBER"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_MODEL"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_CONNECTIONTYPE"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v2, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    iget-object v3, v3, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->worker:Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;
    invoke-static {v5}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;)Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;

    move-result-object v5

    const-wide/32 v6, 0x1d4c0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker;J)V

    .line 198
    .restart local v2    # "listener":Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;
    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->setResponseBundle(Landroid/os/Bundle;)V

    .line 199
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->addKiesActionListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper$KiesActionListener;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v3}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_SYNC_CONNECTION"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 203
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v10

    move-object/from16 v0, v27

    iget-object v11, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v16, v9

    invoke-virtual/range {v10 .. v16}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->setSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 205
    iget v3, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->setMonitorRequestID(I)V

    goto/16 :goto_0

    .line 207
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$ProviderWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v3}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_AUTO_SYNC_CONNECTION"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_UNIQUEID"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 211
    .local v18, "uniqueID":Ljava/lang/String;
    const-string v3, "BUNDLE_STRING_KIES_DEVICE_UNIQUEID"

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->kiesDeviceCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler;->access$200()Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;

    move-result-object v16

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->UDN:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v19, v12

    move-object/from16 v20, v13

    move-object/from16 v21, v14

    move-object/from16 v22, v15

    move-object/from16 v23, v9

    invoke-virtual/range {v16 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/api/kiessharing/KiesDeviceCPWrapper;->setAutoSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 216
    iget v3, v9, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/upnpdevice/KiesAsyncActionHandler$LifeLimitedKiesActionListener;->setMonitorRequestID(I)V

    goto/16 :goto_0
.end method
