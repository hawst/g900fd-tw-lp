.class Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;
.super Ljava/util/TimerTask;
.source "DeviceStateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PollingTask"
.end annotation


# static fields
.field private static final DEFAULT_RETRY_TIMES:I = 0x3


# instance fields
.field private beforeState:I

.field private deviceUdn:Ljava/lang/String;

.field private retryTimesCount:I

.field private stopped:Z

.field private timer:Ljava/util/Timer;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    .line 146
    iput v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->retryTimesCount:I

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->beforeState:I

    .line 148
    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->stopped:Z

    .line 151
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    .line 152
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;

    .line 153
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->start()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->stop()V

    return-void
.end method

.method private start()V
    .locals 6

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1770

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 157
    return-void
.end method

.method private stop()V
    .locals 5

    .prologue
    .line 160
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->stopped:Z

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 165
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->purge()I

    .line 166
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->timer:Ljava/util/Timer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PollingTask"

    const-string v3, "PollingTask is stopped"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PollingTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PollingTask exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 176
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PollingTask is starting, device udn: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    new-instance v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 179
    .local v3, "requestId":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$400()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v6, v7, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getTrnasportInfo(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    move-result v4

    .line 180
    .local v4, "res":I
    const/16 v6, 0x2329

    if-ne v4, v6, :cond_1

    .line 181
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopBufferingStatePolling(Ljava/lang/String;)V

    .line 182
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MRCP stack doesn\'t find this device: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Get transport info, action id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$1;)V

    .line 190
    .local v1, "callback":Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mMRCP:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$400()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v6

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v6, v7, v1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPAVTControlListener(ILcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPAVSControlListener;)V

    .line 194
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    const-wide/16 v6, 0x7530

    :try_start_1
    invoke-virtual {v1, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 196
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    :goto_1
    iget-boolean v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->stopped:Z

    if-eqz v6, :cond_2

    .line 203
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "This task has been canceled, quit polling"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v6
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 197
    :catch_0
    move-exception v2

    .line 198
    .local v2, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Waiting callback reponse exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 208
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-boolean v6, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mIsNotified:Z

    if-eqz v6, :cond_7

    iget v6, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mErrorCode:I

    if-nez v6, :cond_7

    iget-object v6, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mResponse:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    if-eqz v6, :cond_7

    .line 209
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "Got the state of the player."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget v6, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mRequestID:I

    iget v7, v3, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    if-ne v6, v7, :cond_3

    const-string v6, "GetTransportInfo"

    iget-object v7, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mActionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 213
    :cond_3
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "The request id or action name mismatch, retry!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :cond_4
    iget-object v5, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mResponse:Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;

    .line 219
    .local v5, "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    iget v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->beforeState:I

    iget v7, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    if-eq v6, v7, :cond_5

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$600()Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 220
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 221
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "BUNDLE_STRING_CATEGORY"

    const-string v7, "com.sec.android.allshare.event.DEVICE"

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v6, "BUNDLE_STRING_ID"

    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v6, "BUNDLE_ENUM_ERROR"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    iget v7, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->notifyPlayerStatus(Ljava/lang/String;ILandroid/os/Bundle;)V
    invoke-static {v6, v7, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$700(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 226
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "notify changed status of player: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_5
    iget v6, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    iput v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->beforeState:I

    .line 232
    iget v6, v5, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;->state:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_6

    .line 234
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "The player state is not BUFFERRING, stop polling!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopBufferingStatePolling(Ljava/lang/String;)V

    .line 237
    :cond_6
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->retryTimesCount:I

    goto/16 :goto_0

    .line 238
    .end local v5    # "response":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AVSResponse;
    :cond_7
    iget-boolean v6, v1, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$AVSCallback;->mIsNotified:Z

    if-nez v6, :cond_0

    .line 239
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "Get the state of the player time out."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->retryTimesCount:I

    const/4 v7, 0x3

    if-lt v6, v7, :cond_8

    .line 243
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "stop polling!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->deviceUdn:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->stopBufferingStatePolling(Ljava/lang/String;)V

    .line 247
    :cond_8
    iget v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->retryTimesCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler$PollingTask;->retryTimesCount:I

    .line 248
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->mTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "PollingTask"

    const-string v8, "Retry!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
