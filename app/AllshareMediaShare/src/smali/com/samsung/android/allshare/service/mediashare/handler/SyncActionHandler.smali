.class public abstract Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;
.super Ljava/lang/Object;
.source "SyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/service/mediashare/handler/ISyncActionHandler;


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActionType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandlerID:Ljava/lang/String;

.field private mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

.field protected final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)V
    .locals 2
    .param p1, "manager"    # Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mTAG:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mContext:Landroid/content/Context;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionType:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 45
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mContext:Landroid/content/Context;

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private registerHandler()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->registerActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)Z

    .line 176
    :cond_0
    return-void
.end method

.method private unregisterHandler()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;->unregisterActionHandler(Lcom/samsung/android/allshare/service/mediashare/handler/IActionHandler;)V

    .line 188
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract defineHandler()V
.end method

.method public finiActionHandler()V
    .locals 0

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->finiHandler()V

    .line 107
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->unregisterHandler()V

    .line 109
    return-void
.end method

.method protected abstract finiHandler()V
.end method

.method public getActionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getActionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionType:Ljava/lang/String;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getHandlerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mHandlerID:Ljava/lang/String;

    return-object v0
.end method

.method protected getManager()Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    return-object v0
.end method

.method public initActionHandler()V
    .locals 1

    .prologue
    .line 88
    const-string v0, "ACTION_TYPE_SYNC"

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionType:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionList:Ljava/util/ArrayList;

    .line 91
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->defineHandler()V

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->initHandler()V

    .line 93
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->registerHandler()V

    .line 95
    return-void
.end method

.method protected abstract initHandler()V
.end method

.method protected response(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 2
    .param p1, "actionID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 196
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;)V

    .line 198
    .local v0, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez p2, :cond_0

    .line 199
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "bundle":Landroid/os/Bundle;
    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 201
    .restart local p2    # "bundle":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 204
    return-object v0
.end method

.method protected abstract responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
.end method

.method public responseSyncActionRequest(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 1
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->responseSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setActionID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_0
    return-void
.end method

.method protected setHandlerID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/SyncActionHandler;->mHandlerID:Ljava/lang/String;

    .line 63
    return-void
.end method
