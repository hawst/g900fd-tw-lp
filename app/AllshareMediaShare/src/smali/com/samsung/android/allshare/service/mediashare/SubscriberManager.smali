.class public Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;
.super Ljava/lang/Object;
.source "SubscriberManager.java"


# instance fields
.field private mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;>;"
        }
    .end annotation
.end field

.field private mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 54
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->init()V

    .line 56
    return-void
.end method

.method private dumpDeviceEventSubscriptionMap()V
    .locals 9

    .prologue
    .line 308
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 311
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    const-string v7, "==[ Dump Device Event Subscription Map ] ==========================================="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 314
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 315
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 316
    .local v0, "dev_id":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 318
    .local v4, "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 319
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 320
    .local v3, "subscriber":Landroid/os/Messenger;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 323
    .end local v0    # "dev_id":Ljava/lang/String;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    .end local v3    # "subscriber":Landroid/os/Messenger;
    .end local v4    # "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpDeviceEventSubscriptionMap"

    const-string v7, "-----------------------------------------------------------------------------"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method private dumpGlobalEventSubscriptionMap()V
    .locals 9

    .prologue
    .line 332
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 336
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;>;"
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    const-string v7, "==[ Dump Global Event Subscription Map ] ==========================================="

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 340
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 341
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 342
    .local v1, "evt_id":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 343
    .local v4, "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 344
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 345
    .local v3, "subscriber":Landroid/os/Messenger;
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 348
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;>;"
    .end local v1    # "evt_id":Ljava/lang/String;
    .end local v3    # "subscriber":Landroid/os/Messenger;
    .end local v4    # "subscribers":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Messenger;>;"
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v6, "dumpGlobalEventSubscriptionMap"

    const-string v7, "-----------------------------------------------------------------------------"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method private existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 1
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 74
    return-void
.end method

.method private subscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 6
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    const/4 v5, 0x1

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 115
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeDeviceEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " already subscribed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :goto_0
    return v5

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_1

    .line 120
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    :cond_1
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 123
    .local v0, "subscribers":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;"
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private subscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)Z
    .locals 6
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    const/4 v5, 0x1

    .line 159
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 160
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeGlobalEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " already subscribed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :goto_0
    return v5

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_1

    .line 166
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    :cond_1
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 169
    .local v0, "subscribers":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/os/Messenger;>;"
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private unsubscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 4
    .param p1, "device_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->existDeviceEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "unsubscribeDeviceEvent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not subscribed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private unsubscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 4
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->existGlobalEventSubscription(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "existGlobalEventSubscription"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not subscribed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 192
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public fini()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 219
    :cond_0
    return-void
.end method

.method public final getDeviceEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 5
    .param p1, "device_id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 275
    if-nez p1, :cond_0

    .line 282
    :goto_0
    return-object v0

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 278
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "getDeviceEventSubscriber"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no DeviceEventSubscriber for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mDeviceEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    goto :goto_0
.end method

.method public final getGlobalEventSubscriber(Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 4
    .param p1, "evt_id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v1, "getGlobalEventSubscriber"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no GlobalEventSubscriber for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const/4 v0, 0x0

    .line 297
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mGlobalEventSubscriptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    goto :goto_0
.end method

.method public subscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)Z
    .locals 5
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 230
    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 231
    if-nez p3, :cond_0

    .line 232
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "subscribeEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can not find out target device id for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/4 v1, 0x0

    .line 242
    :goto_0
    return v1

    .line 236
    :cond_0
    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "device_id":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->subscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)Z

    move-result v1

    goto :goto_0

    .line 240
    .end local v0    # "device_id":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->subscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)Z

    .line 242
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public unsubscribeEvent(Ljava/lang/String;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "evt_id"    # Ljava/lang/String;
    .param p2, "subscriber"    # Landroid/os/Messenger;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 253
    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    if-nez p3, :cond_0

    .line 255
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->mTag:Ljava/lang/String;

    const-string v2, "unsubscribeEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can not find out target device id for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :goto_0
    return-void

    .line 259
    :cond_0
    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "device_id":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->unsubscribeDeviceEvent(Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0

    .line 265
    .end local v0    # "device_id":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/SubscriberManager;->unsubscribeGlobalEvent(Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0
.end method
