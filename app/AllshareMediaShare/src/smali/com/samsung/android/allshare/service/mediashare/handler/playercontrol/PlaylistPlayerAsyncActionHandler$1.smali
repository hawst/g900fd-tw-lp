.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;
.super Ljava/lang/Object;
.source "PlaylistPlayerAsyncActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPEventNotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMRCPEventNotify(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "deviceUDN"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;>;"
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->currentUDN:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->isForceStop:Z
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "argument":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;

    .line 145
    .local v0, "arg":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    iget-object v3, v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionName:Ljava/lang/String;

    const-string v4, "LastChange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 146
    move-object v1, v0

    .line 152
    .end local v0    # "arg":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;
    :cond_3
    if-eqz v1, :cond_0

    .line 153
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceActionArgument;->actionValue:Ljava/lang/String;

    const-string v4, "STOPPED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 154
    iget-object v3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->playNextItem()V
    invoke-static {v3}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/PlaylistPlayerAsyncActionHandler;)V

    goto :goto_0
.end method
