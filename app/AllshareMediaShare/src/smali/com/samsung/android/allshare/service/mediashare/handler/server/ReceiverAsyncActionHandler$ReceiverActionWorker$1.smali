.class Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;
.super Ljava/lang/Object;
.source "ReceiverAsyncActionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->work(Lcom/sec/android/allshare/iface/CVMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

.field final synthetic val$cvm:Lcom/sec/android/allshare/iface/CVMessage;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 25

    .prologue
    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v5

    .line 112
    .local v5, "bundle":Landroid/os/Bundle;
    const-class v21, Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 115
    const-string v21, "BUNDLE_STRING_ID"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 116
    .local v19, "targetDeviceUDN":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v21

    const/16 v22, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    move-result-object v18

    .line 118
    .local v18, "targetDevice":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    if-nez v18, :cond_1

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "The targetDevice is NULL!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_URI"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_URI"

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->setActionID(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;Ljava/lang/String;)V

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v21

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 133
    sget-object v12, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 134
    .local v12, "result":Lcom/samsung/android/allshare/ERROR;
    const-string v21, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .line 136
    .local v9, "itemBundle":Landroid/os/Bundle;
    if-nez v9, :cond_2

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "The itemBundle is NULL!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    :cond_2
    const-string v21, "BUNDLE_STRING_ID"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 143
    .local v14, "sourceDeviceUDN":Ljava/lang/String;
    if-eqz v14, :cond_4

    .line 145
    const-string v21, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 147
    .local v16, "sourceObjectID":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;

    invoke-direct {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;-><init>()V

    .line 148
    .local v4, "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    move-object/from16 v0, v16

    invoke-virtual {v4, v14, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->browseMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v15

    .line 150
    .local v15, "sourceObject":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;->discard()V

    .line 152
    if-nez v15, :cond_3

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$1000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "The sourceObject is NULL!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v0, v1, v15}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v12

    .line 178
    .end local v4    # "browseHandler":Lcom/samsung/android/allshare/service/mediashare/handler/server/SyncBrowseHandler;
    .end local v15    # "sourceObject":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    .end local v16    # "sourceObjectID":Ljava/lang/String;
    :goto_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 179
    .local v3, "b":Landroid/os/Bundle;
    const-string v21, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v12}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v21, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 162
    .end local v3    # "b":Landroid/os/Bundle;
    :cond_4
    const-string v21, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Landroid/net/Uri;

    .line 164
    .local v17, "srouceContentPath":Landroid/net/Uri;
    const-string v21, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 166
    .local v10, "mimeType":Ljava/lang/String;
    if-eqz v17, :cond_5

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->createItemByFilePath(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    invoke-static {v0, v9}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;

    move-result-object v7

    .line 168
    .local v7, "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move-object/from16 v2, v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->uploadContent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v0, v1, v2, v10, v7}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v12

    .line 170
    goto :goto_1

    .line 171
    .end local v7    # "contentItem":Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AllShareObject;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "The Url is NULL!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    .end local v9    # "itemBundle":Landroid/os/Bundle;
    .end local v10    # "mimeType":Ljava/lang/String;
    .end local v12    # "result":Lcom/samsung/android/allshare/ERROR;
    .end local v14    # "sourceDeviceUDN":Ljava/lang/String;
    .end local v17    # "srouceContentPath":Landroid/net/Uri;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->val$cvm:Lcom/sec/android/allshare/iface/CVMessage;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v21

    const-string v22, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 184
    const/16 v20, 0x0

    .line 186
    .local v20, "transfer_handler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    const-string v21, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .line 188
    .restart local v9    # "itemBundle":Landroid/os/Bundle;
    if-nez v9, :cond_7

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->responseFailMsg(Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Ljava/lang/String;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$1800(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "The itemBundle is NULL!"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    :cond_7
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 195
    .local v6, "bundle1":Landroid/os/Bundle;
    const-string v21, "BUNDLE_ENUM_ERROR"

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v21, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 198
    const-string v21, "BUNDLE_STRING_ID"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 199
    .local v13, "sourceDMSUDN":Ljava/lang/String;
    if-eqz v13, :cond_8

    .line 200
    const-string v21, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 202
    .local v11, "objectID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$1900(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "cancel receive by item,objectID = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "transfer_handler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    check-cast v20, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;

    .line 204
    .restart local v20    # "transfer_handler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    .end local v11    # "objectID":Ljava/lang/String;
    :goto_2
    if-nez v20, :cond_9

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "run"

    const-string v23, "transfer_handler is null.possible case:1.createObject failed. 2.time issue of getting transfer_handler from mUploadThreadList"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v0, v6}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 206
    :cond_8
    const-string v21, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    .line 208
    .local v8, "contentUrl":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v21

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "transfer_handler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    check-cast v20, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;

    .line 209
    .restart local v20    # "transfer_handler":Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->mUploadThreadlList:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler;)Ljava/util/HashMap;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 220
    .end local v8    # "contentUrl":Landroid/net/Uri;
    :cond_9
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/service/mediashare/handler/server/TransferHandler;->stopTransfer()Z

    move-result v12

    .line 221
    .local v12, "result":Z
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 222
    .restart local v3    # "b":Landroid/os/Bundle;
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v12, v0, :cond_a

    .line 223
    const-string v21, "BUNDLE_ENUM_ERROR"

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :goto_3
    const-string v21, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker$1;->this$1:Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->response(Landroid/os/Bundle;)V
    invoke-static {v0, v3}, Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;->access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/server/ReceiverAsyncActionHandler$ReceiverActionWorker;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 225
    :cond_a
    const-string v21, "BUNDLE_ENUM_ERROR"

    sget-object v22, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
