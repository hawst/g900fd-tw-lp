.class Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;
.super Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;
.source "URIPlayerAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayActionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    .line 133
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler$ActionWorker;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;)V

    .line 134
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->getMetadata(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->getAlbumDetails(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    move-result-object v0

    return-object v0
.end method

.method private getAlbumDetails(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;
    .locals 12
    .param p1, "itemBundle"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0x0

    .line 264
    const-string v0, "BUNDLE_STRING_ITEM_ARTIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "artist":Ljava/lang/String;
    const-string v0, "BUNDLE_STRING_ITEM_ALBUM_TITLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 266
    .local v3, "albumTitle":Ljava/lang/String;
    const-string v0, "BUNDLE_STRING_ITEM_GENRE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, "genre":Ljava/lang/String;
    const-string v0, "BUNDLE_DATE_ITEM_DATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 268
    .local v6, "dateTime":J
    const-string v0, "BUNDLE_LONG_ITEM_DURATION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 270
    .local v8, "duration":J
    const-string v5, ""

    .line 271
    .local v5, "durationString":Ljava/lang/String;
    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    .line 272
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 275
    :cond_0
    const-string v4, ""

    .line 276
    .local v4, "dateTimeString":Ljava/lang/String;
    cmp-long v0, v6, v10

    if-lez v0, :cond_1

    .line 277
    const-string v0, "yyyy-MM-dd"

    invoke-static {v0, v6, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v4

    .end local v4    # "dateTimeString":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 280
    .restart local v4    # "dateTimeString":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getMetadata(Landroid/os/Bundle;)Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .locals 6
    .param p1, "itemBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 234
    if-nez p1, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-object v3

    .line 237
    :cond_1
    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 241
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    const-string v5, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->getAbsFilepath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 243
    .local v1, "subtitlePath":Ljava/lang/String;
    const-string v4, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 245
    .local v2, "title":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 246
    :goto_1
    if-nez v2, :cond_2

    .line 247
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 248
    const-string v2, "Audio"

    .line 257
    :cond_2
    :goto_2
    if-eqz v1, :cond_7

    .line 258
    :goto_3
    new-instance v4, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;

    invoke-direct {v4, v2, v0, v1, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    goto :goto_0

    .line 245
    :cond_3
    const-string v0, "*"

    goto :goto_1

    .line 249
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 250
    const-string v2, "Video"

    goto :goto_2

    .line 251
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 252
    const-string v2, "Image"

    goto :goto_2

    .line 254
    :cond_6
    const-string v2, "Unknow"

    goto :goto_2

    .line 257
    :cond_7
    const-string v1, ""

    goto :goto_3
.end method

.method private requestSyncStop(Ljava/lang/String;)Z
    .locals 6
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 216
    new-instance v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 217
    .local v2, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 219
    .local v1, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Stop"

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 220
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->stop(Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;)I

    .line 222
    iget v4, v2, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 223
    .local v0, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)V

    .line 224
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1000()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v3

    .line 228
    .local v3, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1100()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private responsePlayFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    .locals 3
    .param p1, "itemBundle"    # Landroid/os/Bundle;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p3, "startingPosition"    # J
    .param p5, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 286
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 288
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 289
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    if-eqz p2, :cond_0

    .line 292
    const-string v1, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 297
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->response(Landroid/os/Bundle;)V

    .line 298
    return-void

    .line 294
    :cond_0
    const-string v1, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method


# virtual methods
.method public play(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;ZZLcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)V
    .locals 32
    .param p1, "dmrUDN"    # Ljava/lang/String;
    .param p2, "itemBundle"    # Landroid/os/Bundle;
    .param p3, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p4, "startTime"    # J
    .param p6, "metadata"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;
    .param p7, "isImage"    # Z
    .param p8, "isRedirect"    # Z
    .param p9, "albumDetails"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;

    .prologue
    .line 302
    const/4 v14, 0x0

    .line 303
    .local v14, "requestUrl":Ljava/lang/String;
    const/16 v27, 0x0

    .line 305
    .local v27, "playingUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    move-object/from16 v0, p1

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->getDeviceLocker(Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Ljava/lang/String;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v21

    .line 306
    .local v21, "deviceLocker":Ljava/util/concurrent/locks/ReentrantLock;
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 309
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->subscribe(Ljava/lang/String;)V

    .line 311
    if-eqz p2, :cond_0

    .line 312
    const-string v4, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v30

    check-cast v30, Landroid/net/Uri;

    .line 313
    .local v30, "uri":Landroid/net/Uri;
    if-eqz v30, :cond_0

    .line 314
    invoke-virtual/range {v30 .. v30}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    .line 316
    .end local v30    # "uri":Landroid/net/Uri;
    :cond_0
    if-eqz v14, :cond_1

    if-eqz v14, :cond_2

    const-string v4, "content:"

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "file:"

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "/"

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 319
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    const-string v6, "[ASInstantPlayPlayFile] Contents type is wrong!! "

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    sget-object v4, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-wide/from16 v8, p4

    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->responsePlayFail(Landroid/os/Bundle;Lcom/samsung/android/allshare/media/ContentInfo;JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 450
    :goto_0
    return-void

    .line 325
    :cond_2
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 328
    new-instance v26, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct/range {v26 .. v26}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 329
    .local v26, "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 330
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 333
    new-instance v18, Landroid/os/Bundle;

    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    .line 334
    .local v18, "bundle":Landroid/os/Bundle;
    new-instance v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;

    invoke-direct {v10}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;-><init>()V

    .line 336
    .local v10, "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    const/16 v25, 0x0

    .line 338
    .local v25, "local_result":I
    if-eqz p8, :cond_3

    .line 339
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v5, p1

    move-object v6, v14

    move-object/from16 v7, p6

    move-wide/from16 v8, p4

    move-object/from16 v11, p9

    invoke-virtual/range {v4 .. v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    move-result v25

    .line 341
    iget v0, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v24, v0

    .line 344
    .local v24, "key":I
    move-object/from16 v27, v14

    .line 345
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1500(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Play web content in redirect mode, playing uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :goto_1
    if-eqz v25, :cond_4

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Play web content in error, uri = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 348
    .end local v24    # "key":I
    :cond_3
    :try_start_2
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v12

    move-object/from16 v13, p1

    move-object/from16 v15, p6

    move-wide/from16 v16, p4

    move-object/from16 v19, p9

    invoke-virtual/range {v12 .. v19}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playRelayContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLandroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    move-result v25

    .line 350
    const-string v4, "BUNDLE_INT_REQUEST_ID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 353
    .restart local v24    # "key":I
    const-string v4, "BUNDLE_STRING_PLAYING_URI"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 354
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1600(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Play web content in relay mode, playing uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 448
    .end local v10    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .end local v18    # "bundle":Landroid/os/Bundle;
    .end local v24    # "key":I
    .end local v25    # "local_result":I
    .end local v26    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    .line 364
    .restart local v10    # "requestID":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;
    .restart local v18    # "bundle":Landroid/os/Bundle;
    .restart local v24    # "key":I
    .restart local v25    # "local_result":I
    .restart local v26    # "monitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    move/from16 v0, v24

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)V

    .line 365
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1800()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    const-wide/32 v4, 0xea60

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    move-result v28

    .line 367
    .local v28, "result":Z
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$1900()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 373
    sget-object v22, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 374
    .local v22, "error":Lcom/samsung/android/allshare/ERROR;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v23

    .line 375
    .local v23, "errorCode":I
    if-eqz v28, :cond_5

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->hasWrongResponse()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 376
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2100(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mRequestID :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Play"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " has wrong response"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    sget-object v22, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 378
    const/16 v4, 0x2c1

    move/from16 v0, v23

    if-ne v0, v4, :cond_6

    .line 379
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2200(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "got setavt error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " try to stop dmr"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    new-instance v29, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;

    invoke-direct/range {v29 .. v29}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;-><init>()V

    .line 383
    .local v29, "retryMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    const-string v4, "Play"

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setActionName(Ljava/lang/String;)V

    .line 384
    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setDeviceUdn(Ljava/lang/String;)V

    .line 387
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->requestSyncStop(Ljava/lang/String;)Z

    .line 388
    if-eqz p8, :cond_7

    .line 389
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v4

    move-object/from16 v5, p1

    move-object v6, v14

    move-object/from16 v7, p6

    move-wide/from16 v8, p4

    move-object/from16 v11, p9

    invoke-virtual/range {v4 .. v11}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playWebContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    .line 391
    iget v0, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    move/from16 v24, v0

    .line 395
    move-object/from16 v27, v14

    .line 396
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2300(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Play web content in redirect mode, playing uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    move/from16 v0, v24

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->requestAddAVSListener(I)V
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)V

    .line 412
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2500()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    const-wide/32 v4, 0xea60

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->waitResponse(J)Z

    .line 414
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->actionMonitorMap:Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2600()Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;

    move-result-object v4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/service/mediashare/helper/ConcurrentBlockingHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mServiceManager:Lcom/samsung/android/allshare/service/mediashare/ServiceManager;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2700(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Lcom/samsung/android/allshare/service/mediashare/ServiceManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->getInstance(Lcom/samsung/android/allshare/service/mediashare/ServiceManager;)Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/DeviceStateHandler;->startBufferingStatePolling(Ljava/lang/String;)V

    .line 420
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->getErrorCode()I

    move-result v23

    .line 423
    .end local v29    # "retryMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    move/from16 v0, v23

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->convertErrorCode(I)Lcom/samsung/android/allshare/ERROR;
    invoke-static {v4, v0}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2800(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;I)Lcom/samsung/android/allshare/ERROR;

    move-result-object v22

    .line 433
    :goto_3
    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    .line 434
    .local v20, "b":Landroid/os/Bundle;
    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 435
    const-string v4, "BUNDLE_ENUM_ERROR"

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    # invokes: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->putResourceUriListToBundle(Landroid/os/Bundle;Ljava/lang/String;)V
    invoke-static {v4, v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$3000(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 439
    if-eqz p3, :cond_9

    .line 440
    const-string v4, "BUNDLE_PARCELABLE_CONTENT_INFO"

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 445
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->response(Landroid/os/Bundle;)V

    .line 446
    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;->setNotified(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 448
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 399
    .end local v20    # "b":Landroid/os/Bundle;
    .restart local v29    # "retryMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_7
    :try_start_4
    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$800()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v12

    move-object/from16 v13, p1

    move-object/from16 v15, p6

    move-wide/from16 v16, p4

    move-object/from16 v19, p9

    invoke-virtual/range {v12 .. v19}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->playRelayContent(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/Metadata;JLandroid/os/Bundle;Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/AlbumDetails;)I

    .line 401
    const-string v4, "BUNDLE_INT_REQUEST_ID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 405
    const-string v4, "BUNDLE_STRING_PLAYING_URI"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2400(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Play web content in relay mode, playing uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 426
    .end local v29    # "retryMonitor":Lcom/samsung/android/allshare/service/mediashare/helper/ActionResponseMonitor;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;->this$0:Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;->access$2900(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "play"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isdirectly : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p8

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "after response,RequestID :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v10, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/RequestID;->requestID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Play"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ErrorCode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    sget-object v22, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_3

    .line 442
    .restart local v20    # "b":Landroid/os/Bundle;
    :cond_9
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v20

    move-wide/from16 v1, p4

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4
.end method

.method public work(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 2
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 143
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/handler/AsyncActionHandler;->excutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/handler/playercontrol/URIPlayerAsyncActionHandler$PlayActionWorker;Lcom/sec/android/allshare/iface/CVMessage;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 213
    return-void
.end method
