.class public Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;
.super Ljava/lang/Object;
.source "ErrorNotifyResponse.java"


# instance fields
.field private actionName:Ljava/lang/String;

.field private errorCode:I

.field private errorDescription:Ljava/lang/String;

.field private requestID:I


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->requestID:I

    .line 22
    iput-object p2, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->actionName:Ljava/lang/String;

    .line 23
    iput p3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorCode:I

    .line 24
    iput-object p4, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorDescription:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->actionName:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorCode:I

    return v0
.end method

.method public getErrorInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->requestID:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->requestID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " actionName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorDescription: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/mediasharing/ErrorNotifyResponse;->errorDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
