.class Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;
.super Landroid/content/BroadcastReceiver;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 456
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 458
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 459
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive"

    const-string v3, "intent.getAction() == null!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 465
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkWifiState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$200(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 468
    :cond_2
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 469
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkWifiState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$200(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 473
    :cond_3
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 474
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkP2PState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$300(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 478
    :cond_4
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 479
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->getAirplaneModeState(Landroid/content/Context;)I
    invoke-static {v2, p1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$400(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Context;)I

    move-result v2

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkAirplaneModeState(I)V
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$500(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;I)V

    .line 483
    :cond_5
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 485
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkScreenState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$600(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    .line 489
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mFeatureSupportEthernet:Z
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$700(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-static {v1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    move-result v1

    if-ne v1, v4, :cond_8

    .line 491
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkEthernetState"

    const-string v3, "Ethernet conntivity broadcast receiver"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkEthernetState(Z)Z
    invoke-static {v1, v4}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$800(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Z)Z

    .line 496
    :cond_8
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor$2;->this$0:Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;

    # invokes: Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->checkHotspotState(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;->access$900(Lcom/samsung/android/allshare/service/mediashare/monitor/SystemEventMonitor;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
