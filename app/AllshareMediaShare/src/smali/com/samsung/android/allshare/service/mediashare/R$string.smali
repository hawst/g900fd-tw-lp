.class public final Lcom/samsung/android/allshare/service/mediashare/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f070000

.field public static final byteShort:I = 0x7f070023

.field public static final cancel:I = 0x7f07000c

.field public static final cancelled:I = 0x7f070007

.field public static final delete_download:I = 0x7f07000b

.field public static final deselect_all:I = 0x7f070015

.field public static final device:I = 0x7f070010

.field public static final dialog_failed_body:I = 0x7f070020

.field public static final dialog_file_missing_body:I = 0x7f070019

.field public static final dialog_insufficient_space:I = 0x7f07002b

.field public static final dialog_media_not_found:I = 0x7f07001a

.field public static final download_cancelled:I = 0x7f07000a

.field public static final download_err_insufficient_memory:I = 0x7f070016

.field public static final download_error:I = 0x7f070006

.field public static final download_failed:I = 0x7f070009

.field public static final download_files:I = 0x7f07001e

.field public static final download_no_application_title:I = 0x7f07001b

.field public static final download_queued:I = 0x7f070008

.field public static final download_running:I = 0x7f070004

.field public static final download_success:I = 0x7f070005

.field public static final downloaded:I = 0x7f070012

.field public static final downloading_filename:I = 0x7f07000e

.field public static final downloads:I = 0x7f07001d

.field public static final file_downloaded:I = 0x7f070013

.field public static final files:I = 0x7f070022

.field public static final gigabyteShort:I = 0x7f070026

.field public static final item_selected:I = 0x7f070028

.field public static final kilobyteShort:I = 0x7f070024

.field public static final megabyteShort:I = 0x7f070025

.field public static final missing_title:I = 0x7f070003

.field public static final no_downloads:I = 0x7f070017

.field public static final ok:I = 0x7f07000d

.field public static final pd_downloaded:I = 0x7f07001f

.field public static final pd_failed:I = 0x7f070018

.field public static final pd_files_have_been_downloaded:I = 0x7f070021

.field public static final permission_label:I = 0x7f070001

.field public static final permission_label_desc:I = 0x7f070002

.field public static final sd_card:I = 0x7f07000f

.field public static final select_all:I = 0x7f070014

.field public static final selected_count:I = 0x7f07001c

.field public static final stms_appgroup:I = 0x7f07002a

.field public static final stms_version:I = 0x7f070029

.field public static final terabyteShort:I = 0x7f070027

.field public static final total:I = 0x7f070011


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
