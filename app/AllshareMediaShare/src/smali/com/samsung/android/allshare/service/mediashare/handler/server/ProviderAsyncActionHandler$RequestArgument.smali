.class Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;
.super Ljava/lang/Object;
.source "ProviderAsyncActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestArgument"
.end annotation


# instance fields
.field public actionHandle:Ljava/lang/String;

.field public dmsUDN:Ljava/lang/String;

.field public folderItem:Landroid/os/Bundle;

.field public itemTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public providerWorker:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

.field public requestCount:I

.field public searchCriteria:Ljava/lang/String;

.field public startIndex:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;)V
    .locals 0
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "index"    # I
    .param p4, "count"    # I
    .param p5, "search"    # Ljava/lang/String;
    .param p7, "folder"    # Landroid/os/Bundle;
    .param p8, "worker"    # Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    .local p6, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput p3, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->startIndex:I

    .line 142
    iput p4, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->requestCount:I

    .line 143
    iput-object p5, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->searchCriteria:Ljava/lang/String;

    .line 144
    iput-object p8, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->providerWorker:Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$ProviderWorker;

    .line 145
    iput-object p7, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->folderItem:Landroid/os/Bundle;

    .line 146
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->dmsUDN:Ljava/lang/String;

    .line 147
    iput-object p6, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->itemTypeList:Ljava/util/ArrayList;

    .line 148
    iput-object p2, p0, Lcom/samsung/android/allshare/service/mediashare/handler/server/ProviderAsyncActionHandler$RequestArgument;->actionHandle:Ljava/lang/String;

    .line 149
    return-void
.end method
