.class public Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
.super Landroid/app/Service;
.source "DownloadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$DownloadServiceBinder;
    }
.end annotation


# instance fields
.field mAm:Landroid/app/IActivityManager;

.field private final mBinder:Landroid/os/IBinder;

.field final mForegroundToken:Landroid/os/IBinder;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

.field private mTag:Ljava/lang/String;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mbForeground:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    .line 100
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$DownloadServiceBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$DownloadServiceBinder;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mBinder:Landroid/os/IBinder;

    .line 132
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mForegroundToken:Landroid/os/IBinder;

    .line 133
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mAm:Landroid/app/IActivityManager;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;Lcom/samsung/android/allshare/ServiceProvider;)Lcom/samsung/android/allshare/ServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    return-object p1
.end method

.method private acquireWakeLock()V
    .locals 5

    .prologue
    .line 161
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "acquireWakeLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_1
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "acquireWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private acquireWifiLock()V
    .locals 5

    .prologue
    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 189
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "acquireWifiLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "acquireWifiLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 5

    .prologue
    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "releaseWakeLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "releaseWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private releaseWifiLock()V
    .locals 5

    .prologue
    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 200
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "releaseWifiLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "releaseWifiLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public acquireLock()V
    .locals 5

    .prologue
    .line 137
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_0

    .line 138
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mPowerManager:Landroid/os/PowerManager;

    .line 139
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mPowerManager:Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "ASF_DOWNLOAD_WAKE_LOCK"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 142
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->acquireWakeLock()V

    .line 144
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v1, :cond_1

    .line 145
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 146
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    const-string v3, "ASF_DOWNLOAD_WIFI_LOCK"

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->acquireWifiLock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "startInboundForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v1, "onBind"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 42
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onCreate"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 45
    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)V

    const-string v2, "com.samsung.android.allshare.media"

    invoke-static {p0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->acquireLock()V

    .line 62
    iget-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z

    if-eqz v1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startForeground Build.VERSION.SDK_INT = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 70
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z

    .line 71
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "setProcessForeground of JB_MR2 complete "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "setProcessForeground RemoteException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 76
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const v1, 0x1aab4

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->startForeground(ILandroid/app/Notification;)V

    .line 77
    iput-boolean v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z

    .line 78
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "startForeground EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 104
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onDestroy"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onDestroy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopForeground Build.VERSION.SDK_INT = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 110
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    invoke-static {v1}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 121
    invoke-virtual {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->releaseLock()V

    .line 123
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 124
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v2, "onDestroy"

    const-string v3, "RemoteException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 115
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->stopForeground(Z)V

    .line 116
    iput-boolean v5, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mbForeground:Z

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v1, "onStartCommand"

    const-string v2, "null intent"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->mTag:Ljava/lang/String;

    const-string v1, "onStartCommand"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public releaseLock()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->releaseWakeLock()V

    .line 157
    invoke-direct {p0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->releaseWifiLock()V

    .line 158
    return-void
.end method
