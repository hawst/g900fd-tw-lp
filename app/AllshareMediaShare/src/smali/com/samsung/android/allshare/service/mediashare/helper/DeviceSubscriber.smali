.class public Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;
.super Ljava/lang/Object;
.source "DeviceSubscriber.java"

# interfaces
.implements Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;
.implements Lcom/samsung/android/allshare/service/mediashare/monitor/ISystemEventListener;


# static fields
.field private static volatile mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber; = null

.field private static final mTAG:Ljava/lang/String; = "DeviceSubscriber"


# instance fields
.field private mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

.field private mSubscribedDevieSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    .line 18
    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 21
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    .line 22
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    .line 24
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->addMRCPDeviceNotifyListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper$IMRCPDeviceNotifyListener;)Z

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    if-nez v0, :cond_1

    .line 29
    const-class v1, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    monitor-enter v1

    .line 30
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    invoke-direct {v0}, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    .line 33
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mInstance:Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    .line 67
    const-string v0, "DeviceSubscriber"

    const-string v1, "clear"

    const-string v2, "clear the subscribed devices"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 71
    monitor-exit v1

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public eventNotifyReceived(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const-string v0, "DeviceSubscriber"

    const-string v1, "eventNotifyReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive system event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "com.sec.android.allshare.event.EVENT_SCREEN_OFF"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "DeviceSubscriber"

    const-string v1, "eventNotifyReceived"

    const-string v2, "EVENT_SCREEN_OFF, clean all subscribed devices!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 83
    monitor-exit v1

    .line 85
    :cond_0
    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDeviceNotify(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v0, "DeviceSubscriber"

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DeviceNotifyStatus;->DEVICE_REMOVED:I

    if-ne v0, p1, :cond_0

    .line 92
    const-string v0, "DeviceSubscriber"

    const-string v1, "onDeviceNotify"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device is removed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 96
    monitor-exit v1

    .line 98
    :cond_0
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public subscribe(Ljava/lang/String;)V
    .locals 4
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 40
    const-string v0, "DeviceSubscriber"

    const-string v1, "subscribe"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device udn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    if-nez p1, :cond_0

    .line 43
    const-string v0, "DeviceSubscriber"

    const-string v1, "subscribe"

    const-string v2, "device udn is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    if-nez v0, :cond_1

    .line 48
    const-string v0, "DeviceSubscriber"

    const-string v1, "subscribe"

    const-string v2, "the instance of MRCPWrapper is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mSubscribedDevieSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 56
    const-string v0, "DeviceSubscriber"

    const-string v2, "subscribe"

    const-string v3, "send SUBSCRIBE for this device."

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/helper/DeviceSubscriber;->mMrcpWrapper:Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/framework/core/mediashare/api/mediasharing/MRCPWrapper;->subscribeDMR(Ljava/lang/String;)I

    .line 62
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 60
    :cond_2
    :try_start_1
    const-string v0, "DeviceSubscriber"

    const-string v2, "subscribe"

    const-string v3, "this device has been subscribed already!"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
