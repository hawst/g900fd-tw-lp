.class public Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;
.super Ljava/lang/Object;
.source "FileDownloader.java"


# static fields
.field private static final DEFAULT_CHUNK_SIZE:I = 0x80000

.field public static final FILESHARE_HTTP_READ_TIMEOUT:I = 0x61a8

.field public static final fileNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static retryCount:I


# instance fields
.field private mIsTerminated:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    .line 37
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->isDownloadableSize(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .param p2, "x2"    # Z

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->getArbitaryPath(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;
    .param p1, "x1"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->downloadSubtitle(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    return-void
.end method

.method private downloadSubtitle(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 22
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 272
    new-instance v17, Lcom/samsung/android/allshare/extension/SECVideoCaption;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/allshare/extension/SECVideoCaption;-><init>()V

    .line 273
    .local v17, "videoCaption":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getURI()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/allshare/extension/SECVideoCaption;->getSubTitleURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 274
    .local v15, "subTitleURL":Ljava/lang/String;
    if-nez v15, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    const/4 v10, 0x0

    .line 278
    .local v10, "fos":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 279
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    const/4 v12, 0x0

    .line 280
    .local v12, "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 283
    .local v9, "file":Ljava/io/File;
    const/high16 v18, 0x80000

    :try_start_0
    move/from16 v0, v18

    new-array v5, v0, [B

    .line 286
    .local v5, "buffer":[B
    new-instance v16, Ljava/net/URL;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 289
    .local v16, "url":Ljava/net/URL;
    sget-object v13, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .line 290
    .local v13, "proxy":Ljava/net/Proxy;
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljava/net/HttpURLConnection;

    .line 291
    .local v6, "conn":Ljava/net/HttpURLConnection;
    const/16 v18, 0x2710

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 292
    const-string v18, "GET"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 293
    const-string v18, "USER-AGENT"

    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/AllShareFrameworkCoreWrapper;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/16 v18, 0x61a8

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 298
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 300
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->getArbitaryPath(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;

    move-result-object v9

    .line 301
    if-eqz v9, :cond_2

    .line 302
    sget-object v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    :cond_2
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .local v11, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_16
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 308
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .local v4, "bos":Ljava/io/BufferedOutputStream;
    const/4 v14, 0x0

    .line 311
    .local v14, "readCount":I
    :cond_3
    :try_start_2
    invoke-virtual {v12, v5}, Ljava/io/InputStream;->read([B)I

    move-result v14

    .line 312
    if-lez v14, :cond_4

    .line 313
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v5, v0, v14}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 316
    :cond_4
    if-gtz v14, :cond_3

    .line 318
    const/16 v18, 0x0

    sput v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    const-string v20, "retryCount reset to 0"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_17
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_13
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 346
    if-eqz v12, :cond_5

    .line 347
    :try_start_3
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 352
    :cond_5
    :goto_1
    if-eqz v4, :cond_6

    .line 354
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 359
    :cond_6
    :goto_2
    if-eqz v11, :cond_7

    .line 361
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 367
    :cond_7
    :goto_3
    if-eqz v9, :cond_1a

    .line 368
    sget-object v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .line 348
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v7

    .line 349
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 355
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 356
    .local v8, "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 362
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 363
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 320
    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v5    # "buffer":[B
    .end local v6    # "conn":Ljava/net/HttpURLConnection;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "proxy":Ljava/net/Proxy;
    .end local v14    # "readCount":I
    .end local v16    # "url":Ljava/net/URL;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v7

    .line 321
    .local v7, "e":Ljava/net/SocketTimeoutException;
    :goto_4
    if-eqz v9, :cond_8

    .line 322
    :try_start_6
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 323
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 346
    if-eqz v12, :cond_9

    .line 347
    :try_start_7
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 352
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_9
    :goto_5
    if-eqz v3, :cond_a

    .line 354
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 359
    :cond_a
    :goto_6
    if-eqz v10, :cond_b

    .line 361
    :try_start_9
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 367
    :cond_b
    :goto_7
    if-eqz v9, :cond_0

    .line 368
    sget-object v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 348
    .restart local v7    # "e":Ljava/net/SocketTimeoutException;
    :catch_4
    move-exception v7

    .line 349
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 355
    .end local v7    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 356
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 362
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_6
    move-exception v7

    .line 363
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 324
    .end local v7    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 325
    .restart local v7    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    sget v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_10

    .line 328
    sget v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    add-int/lit8 v18, v18, 0x1

    sput v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "retryCount: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    if-eqz v9, :cond_c

    .line 331
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 332
    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->downloadSubtitle(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 346
    :goto_9
    if-eqz v12, :cond_d

    .line 347
    :try_start_b
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 352
    :cond_d
    :goto_a
    if-eqz v3, :cond_e

    .line 354
    :try_start_c
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 359
    :cond_e
    :goto_b
    if-eqz v10, :cond_f

    .line 361
    :try_start_d
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 367
    :cond_f
    :goto_c
    if-eqz v9, :cond_0

    .line 368
    sget-object v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 335
    :cond_10
    const/16 v18, 0x0

    :try_start_e
    sput v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    .line 336
    if-eqz v9, :cond_11

    .line 337
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 338
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "retryCount: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget v21, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->retryCount:I

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_9

    .line 345
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    .line 346
    :goto_d
    if-eqz v12, :cond_12

    .line 347
    :try_start_f
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_f

    .line 352
    :cond_12
    :goto_e
    if-eqz v3, :cond_13

    .line 354
    :try_start_10
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_10

    .line 359
    :cond_13
    :goto_f
    if-eqz v10, :cond_14

    .line 361
    :try_start_11
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_11

    .line 367
    :cond_14
    :goto_10
    if-eqz v9, :cond_15

    .line 368
    sget-object v19, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_15
    throw v18

    .line 348
    .restart local v7    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v7

    .line 349
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 355
    :catch_9
    move-exception v8

    .line 356
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 362
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_a
    move-exception v7

    .line 363
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 340
    .end local v7    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v7

    .line 341
    .local v7, "e":Ljava/lang/Exception;
    :goto_11
    if-eqz v9, :cond_16

    .line 342
    :try_start_12
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 343
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "downloadSubtitle"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 346
    if-eqz v12, :cond_17

    .line 347
    :try_start_13
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    .line 352
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_17
    :goto_12
    if-eqz v3, :cond_18

    .line 354
    :try_start_14
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d

    .line 359
    :cond_18
    :goto_13
    if-eqz v10, :cond_19

    .line 361
    :try_start_15
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e

    .line 367
    :cond_19
    :goto_14
    if-eqz v9, :cond_0

    .line 368
    sget-object v18, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 348
    .restart local v7    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v7

    .line 349
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 355
    .end local v7    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v8

    .line 356
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13

    .line 362
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_e
    move-exception v7

    .line 363
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_14

    .line 348
    .end local v7    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v7

    .line 349
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 355
    .end local v7    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v8

    .line 356
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 362
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_11
    move-exception v7

    .line 363
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_10

    .line 345
    .end local v7    # "e":Ljava/io/IOException;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "buffer":[B
    .restart local v6    # "conn":Ljava/net/HttpURLConnection;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "proxy":Ljava/net/Proxy;
    .restart local v16    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v18

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "readCount":I
    :catchall_2
    move-exception v18

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .line 340
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "readCount":I
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_12
    move-exception v7

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto :goto_11

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "readCount":I
    :catch_13
    move-exception v7

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto :goto_11

    .line 324
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "readCount":I
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_14
    move-exception v7

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "readCount":I
    :catch_15
    move-exception v7

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .line 320
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "readCount":I
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_16
    move-exception v7

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "readCount":I
    :catch_17
    move-exception v7

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :cond_1a
    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_0
.end method

.method private getArbitaryPath(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;Z)Ljava/io/File;
    .locals 10
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;
    .param p2, "isSubtitle"    # Z

    .prologue
    .line 383
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 384
    .local v2, "name":Ljava/lang/String;
    const-string v6, "[\\\\/:*?\"<>|]"

    const-string v7, "_"

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 386
    const/4 v0, 0x0

    .line 388
    .local v0, "ext":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 389
    const-string v0, ".smi"

    .line 398
    :cond_0
    :goto_0
    const/4 v1, 0x1

    .line 399
    .local v1, "index":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 401
    .local v5, "tempName":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSevePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v4, "savePathFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 403
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    .line 407
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSevePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    .local v3, "newFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_3

    sget-object v6, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->fileNameList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 409
    iget-object v6, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    const-string v7, "getArbitaryPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "download : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .end local v3    # "newFile":Ljava/io/File;
    :goto_1
    return-object v3

    .line 391
    .end local v1    # "index":I
    .end local v4    # "savePathFile":Ljava/io/File;
    .end local v5    # "tempName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getContentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v6

    check-cast v6, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v6}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "BUNDLE_STRING_ITEM_EXTENSION"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393
    if-nez v0, :cond_0

    .line 394
    invoke-static {}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getContentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/service/mediashare/download/ResourceProtocolParser;->getFileExtensionByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 412
    .restart local v1    # "index":I
    .restart local v3    # "newFile":Ljava/io/File;
    .restart local v4    # "savePathFile":Ljava/io/File;
    .restart local v5    # "tempName":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 413
    add-int/lit8 v1, v1, 0x1

    .line 414
    const/16 v6, 0x1f4

    if-le v1, v6, :cond_1

    .line 415
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private isDownloadableSize(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)Z
    .locals 14
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 41
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSevePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    .local v5, "savePathFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 43
    invoke-virtual {v5}, Ljava/io/File;->mkdir()Z

    .line 44
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    const-string v10, "isDownloadableSize"

    const-string v11, "savePathFile.exists  == false : make new directory"

    invoke-static {v7, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_0
    new-instance v6, Landroid/os/StatFs;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSevePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 48
    .local v6, "stat":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v2, v7

    .line 49
    .local v2, "blockSize":J
    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v7

    int-to-long v8, v7

    .line 50
    .local v8, "totalBlocks":J
    mul-long v0, v2, v8

    .line 52
    .local v0, "availableMemory":J
    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v10

    cmp-long v7, v0, v10

    if-gez v7, :cond_1

    .line 53
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    const-string v10, "isDownloadableSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "availableMemory :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", ItemSize is : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;->getSize()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    const/4 v7, 0x0

    .line 61
    .end local v0    # "availableMemory":J
    .end local v2    # "blockSize":J
    .end local v5    # "savePathFile":Ljava/io/File;
    .end local v6    # "stat":Landroid/os/StatFs;
    .end local v8    # "totalBlocks":J
    :goto_0
    return v7

    .line 57
    :catch_0
    move-exception v4

    .line 58
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    iget-object v7, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mTag:Ljava/lang/String;

    const-string v10, "isDownloadableSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Check Memory Full: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v10, v11}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const/4 v7, 0x0

    goto :goto_0

    .line 61
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "availableMemory":J
    .restart local v2    # "blockSize":J
    .restart local v5    # "savePathFile":Ljava/io/File;
    .restart local v6    # "stat":Landroid/os/StatFs;
    .restart local v8    # "totalBlocks":J
    :cond_1
    const/4 v7, 0x1

    goto :goto_0
.end method


# virtual methods
.method public doDownload(Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;

    .prologue
    .line 65
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z

    .line 67
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader$1;-><init>(Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;Lcom/samsung/android/allshare/service/mediashare/download/DownloadItem;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 267
    .local v0, "downloadThread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 268
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 269
    return-void
.end method

.method public isTerminate()Z
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z

    return v0
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/FileDownloader;->mIsTerminated:Z

    .line 376
    return-void
.end method
