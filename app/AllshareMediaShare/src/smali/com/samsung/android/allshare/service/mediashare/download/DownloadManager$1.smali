.class Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;
.super Ljava/lang/Object;
.source "DownloadManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    check-cast p2, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$DownloadServiceBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService$DownloadServiceBinder;->getService()Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v1

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$102(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    .line 130
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    const-string v2, "mDownloadService == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$100(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;->acquireLock()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    # getter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$000(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager$1;->this$0:Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->mDownloadService:Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;->access$102(Lcom/samsung/android/allshare/service/mediashare/download/DownloadManager;Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;)Lcom/samsung/android/allshare/service/mediashare/download/DownloadService;

    .line 125
    return-void
.end method
