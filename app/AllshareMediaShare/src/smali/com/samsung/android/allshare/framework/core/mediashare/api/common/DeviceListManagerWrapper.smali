.class public Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
.super Ljava/lang/Object;
.source "DeviceListManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DeviceListManagerWrapper"

.field public static final TYPE_DEVICE_ADD:I = 0x1

.field public static final TYPE_DEVICE_CHANGE:I = 0x3

.field public static final TYPE_DEVICE_INVLID:I = 0x0

.field public static final TYPE_DEVICE_REMOVE:I = 0x2

.field private static instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;


# instance fields
.field private deviceChangeListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/ArrayList;

    .line 46
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    .line 73
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->instance:Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    return-object v0
.end method

.method public static onDeviceChandedEvent(Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;I)V
    .locals 7
    .param p0, "device"    # Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .param p1, "changeType"    # I

    .prologue
    .line 218
    if-nez p0, :cond_1

    .line 232
    :cond_0
    return-void

    .line 222
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->getInstance()Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;

    move-result-object v1

    .line 223
    .local v1, "instance":Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;
    iget-object v3, v1, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .line 224
    .local v2, "listener":Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 225
    const-string v3, "DeviceListManagerWrapper"

    const-string v4, "onDeviceChandedEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "receive TYPE_DEVICE_ADD : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-interface {v2, v3, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;->onDeviceAdded(ILcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0

    .line 228
    :cond_2
    const-string v3, "DeviceListManagerWrapper"

    const-string v4, "onDeviceChandedEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "receive TYPE_DEVICE_REMOVE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget v3, p0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;->type:I

    invoke-interface {v2, v3, p0}, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;->onDeviceRemoved(ILcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)V

    goto :goto_0
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDevice(ILjava/lang/String;)Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    .locals 6
    .param p1, "deviceType"    # I
    .param p2, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 108
    new-instance v0, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;

    invoke-direct {v0}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;-><init>()V

    .line 109
    .local v0, "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    invoke-static {p1, p2, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getDevice(ILjava/lang/String;Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;)I

    move-result v1

    .line 110
    .local v1, "result":I
    if-eqz v1, :cond_0

    .line 111
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get device with type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] & UUID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] failed!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x0

    .line 115
    .end local v0    # "device":Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;
    :cond_0
    return-object v0
.end method

.method public getDeviceList(I)Ljava/util/ArrayList;
    .locals 6
    .param p1, "deviceType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v0, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/framework/core/mediashare/data/common/Device;>;"
    invoke-static {p1, v0}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->getDeviceList(ILjava/util/ArrayList;)I

    move-result v1

    .line 85
    .local v1, "result":I
    if-eqz v1, :cond_0

    .line 86
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDeviceList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get device list with type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 97
    :goto_0
    return-object v0

    .line 93
    :cond_1
    const-string v2, "DeviceListManagerWrapper"

    const-string v3, "getDeviceList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] device list length is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 167
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->registerSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    .line 168
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 169
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "registerSearchTarget"

    const-string v3, "register applicationID with seratch target failed"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    return v0
.end method

.method public removeDeviceChangeListener(Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper$IDeviceChangeListener;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/allshare/framework/core/mediashare/api/common/DeviceListManagerWrapper;->deviceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public rescanDevice()I
    .locals 4

    .prologue
    .line 124
    invoke-static {}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->rescanDevice()I

    move-result v0

    .line 125
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 126
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "rescanDevice"

    const-string v3, "Rescan device failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    return v0
.end method

.method public rescanDeviceByAppID(Ljava/lang/String;)I
    .locals 4
    .param p1, "appID"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->rescanDeviceByAppID(Ljava/lang/String;)I

    move-result v0

    .line 153
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 154
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "appID"

    const-string v3, "Rescan device with appID failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_0
    return v0
.end method

.method public rescanDeviceByType(I)I
    .locals 4
    .param p1, "deviceType"    # I

    .prologue
    .line 138
    invoke-static {p1}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->rescanDeviceByType(I)I

    move-result v0

    .line 139
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 140
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "rescanDeviceByType"

    const-string v3, "Rescan device with type failed!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    return v0
.end method

.method public unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/framework/core/mediashare/AllShareFrameworkCoreNativeAPI;->unregisterSearchTarget(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    .line 184
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 185
    const-string v1, "DeviceListManagerWrapper"

    const-string v2, "unregisterSearchTarget"

    const-string v3, "unregister applicationID with seratch target failed"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/framework/core/mediashare/data/common/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_0
    return v0
.end method
