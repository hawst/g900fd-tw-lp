.class Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;
.super Ljava/lang/Object;
.source "ColorChipSettingPreview.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 190
    const-string v0, "ColorChipSettingPreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mColourAdjustmentSeekBarListener :: onProgressChanged() is called : fromUser "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    if-eqz p3, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    int-to-float v1, p2

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$202(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;F)F

    .line 198
    :cond_0
    const-string v0, "ColorChipSettingPreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mColourAdjustmentSeekBarListener :: onProgressChanged() is called : mUserParameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$400(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$400(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/accessibility/AccessibilityManager;

    .line 204
    .local v6, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$500(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)[I

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 210
    :goto_0
    return-void

    .line 206
    .end local v6    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$700(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Ldmc/cvd/cvdcalculator/CVDCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDseverity:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$600(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F
    invoke-static {v4}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)F

    move-result v4

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v0

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I
    invoke-static {v7, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$502(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;[I)[I

    .line 207
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/accessibility/AccessibilityManager;

    .line 208
    .restart local v6    # "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$500(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)[I

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 214
    const-string v0, "ColorChipSettingPreview"

    const-string v1, "onStartTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 219
    const-string v0, "ColorChipSettingPreview"

    const-string v1, "onStopTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return-void
.end method
