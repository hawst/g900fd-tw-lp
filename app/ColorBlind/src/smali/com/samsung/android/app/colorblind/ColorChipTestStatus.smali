.class public Lcom/samsung/android/app/colorblind/ColorChipTestStatus;
.super Landroid/app/Activity;
.source "ColorChipTestStatus.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mBtnCamera:Landroid/widget/ImageButton;

.field private mBtnImage:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private InitControls()V
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->mBtnCamera:Landroid/widget/ImageButton;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->mBtnCamera:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->mBtnImage:Landroid/widget/ImageButton;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->mBtnImage:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_test"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 133
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 134
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v3, 0x20000000

    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 127
    :goto_0
    :pswitch_0
    return-void

    .line 111
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .local v1, "toPreview":Landroid/content/Intent;
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 118
    .end local v1    # "toPreview":Landroid/content/Intent;
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .local v0, "toImage":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 120
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x7f0a0031
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 54
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->setContentView(I)V

    .line 59
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->InitControls()V

    .line 60
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const/high16 v5, 0x7f090000

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 66
    const v4, 0x7f0a0038

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 67
    .local v0, "cancelItem":Landroid/view/MenuItem;
    const v4, 0x7f0a0039

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 68
    .local v2, "skipItem":Landroid/view/MenuItem;
    const v4, 0x7f0a003a

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 69
    .local v1, "doneItem":Landroid/view/MenuItem;
    const v4, 0x7f0a003b

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 70
    .local v3, "startItem":Landroid/view/MenuItem;
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 71
    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 72
    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 79
    const/4 v4, 0x6

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 80
    const/4 v4, 0x1

    return v4
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 85
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 102
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 87
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->onBackPressed()V

    goto :goto_0

    .line 95
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v0, "toSettingReport":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 85
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0a0039 -> :sswitch_1
    .end sparse-switch
.end method
