.class public Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
.super Landroid/app/Application;
.source "ColorChipSettingValue.java"


# instance fields
.field public final CVD_DEUTERAN:I

.field public final CVD_NORMAL:I

.field public final CVD_PROTAN:I

.field public final CVD_TRITAN:I

.field private mCVDType:I

.field private mCVDseverity:F

.field private mUserParameter:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->CVD_PROTAN:I

    .line 14
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->CVD_DEUTERAN:I

    .line 16
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->CVD_TRITAN:I

    .line 18
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->CVD_NORMAL:I

    return-void
.end method


# virtual methods
.method public SavePrefCVDSettingValue(IFF)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "severity"    # F
    .param p3, "userParam"    # F

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_cvdtype"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_cvdseverity"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_user_parameter"

    invoke-static {v0, v1, p3}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 83
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDType:I

    .line 84
    iput p2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDseverity:F

    .line 85
    iput p3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mUserParameter:F

    .line 86
    return-void
.end method

.method public UpdatePrefCVDSettingValue()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_cvdtype"

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDType:I

    .line 40
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_cvdseverity"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDseverity:F

    .line 41
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_user_parameter"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mUserParameter:F

    .line 42
    return-void
.end method

.method public getCVDType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDType:I

    return v0
.end method

.method public getCVDseverity()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDseverity:F

    return v0
.end method

.method public getUserParameter()F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mUserParameter:F

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 31
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->UpdatePrefCVDSettingValue()V

    .line 32
    return-void
.end method

.method public setCVDType(I)V
    .locals 0
    .param p1, "cvdType"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDType:I

    .line 59
    return-void
.end method

.method public setCVDserverity(F)V
    .locals 0
    .param p1, "cvdSeverity"    # F

    .prologue
    .line 63
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mCVDseverity:F

    .line 64
    return-void
.end method

.method public setPrefUserParameter(F)V
    .locals 2
    .param p1, "userParam"    # F

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind_user_parameter"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 95
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mUserParameter:F

    .line 96
    return-void
.end method

.method public setUserParameter(F)V
    .locals 0
    .param p1, "userParam"    # F

    .prologue
    .line 68
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->mUserParameter:F

    .line 69
    return-void
.end method
