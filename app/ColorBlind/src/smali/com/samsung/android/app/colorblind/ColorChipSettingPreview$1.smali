.class Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;
.super Ljava/lang/Object;
.source "ColorChipSettingPreview.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 145
    const-string v2, "ColorChipSettingPreview"

    const-string v3, "Camera surfaceChanged()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 151
    .local v1, "parameters":Landroid/hardware/Camera$Parameters;
    if-eqz v1, :cond_0

    .line 155
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->setSize(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    invoke-static {v3, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v1    # "parameters":Landroid/hardware/Camera$Parameters;
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$2;-><init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 181
    const-string v2, "ColorChipSettingPreview"

    const-string v3, "Camera surfaceChanged()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 93
    const-string v1, "ColorChipSettingPreview"

    const-string v2, "Camera surfaceCreated()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    monitor-enter v2

    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v3

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1, v3}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera;)Landroid/hardware/Camera;

    .line 99
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 101
    const-string v1, "ColorChipSettingPreview"

    const-string v3, "Camera setPreviewDisplay!!"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    :goto_1
    return-void

    .line 103
    :cond_1
    :try_start_2
    const-string v1, "ColorChipSettingPreview"

    const-string v3, "mCamera is NULL!!"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    :try_start_3
    monitor-exit v2

    goto :goto_1

    .line 115
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 110
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 111
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 112
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1, v3}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 120
    const-string v0, "ColorChipSettingPreview"

    const-string v1, "Camera surfaceDestroyed()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 139
    const-string v0, "ColorChipSettingPreview"

    const-string v1, "Camera released"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-void
.end method
