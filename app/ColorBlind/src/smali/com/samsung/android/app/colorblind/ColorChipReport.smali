.class public Lcom/samsung/android/app/colorblind/ColorChipReport;
.super Landroid/app/Activity;
.source "ColorChipReport.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final MAX_COLOUR_ADJUSTMENT:I

.field private final SETTING_OFF:I

.field private final SETTING_ON:I

.field private TAG:Ljava/lang/String;

.field private cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

.field private isFromSetting:Z

.field private isSwitch:Z

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mCVDType:I

.field private mCVDseverity:F

.field private final mColorBlindObserver:Landroid/database/ContentObserver;

.field private mColourAdjustment:Landroid/widget/TextView;

.field private mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

.field private final mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mNegColorObserver:Landroid/database/ContentObserver;

.field private mNegativeColorDialog:Landroid/app/AlertDialog;

.field private mOptionsMenu:Landroid/view/Menu;

.field private mResultText:Landroid/widget/TextView;

.field private mRetestDialog:Landroid/app/AlertDialog;

.field private mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

.field private mTestCheck:I

.field private mUserParameter:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    const-string v0, "ColorChipReport"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    .line 51
    iput v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->SETTING_ON:I

    .line 65
    iput v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->SETTING_OFF:I

    .line 67
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->MAX_COLOUR_ADJUSTMENT:I

    .line 69
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    .line 71
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    .line 73
    iput v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mTestCheck:I

    .line 75
    iput-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isFromSetting:Z

    .line 77
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    .line 203
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipReport$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColorBlindObserver:Landroid/database/ContentObserver;

    .line 209
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport$2;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegColorObserver:Landroid/database/ContentObserver;

    .line 503
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$8;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method private CheckSwitch()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 586
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "color_blind"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 588
    .local v1, "state":I
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CheckSwitch state : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    if-ne v1, v0, :cond_0

    .line 592
    .local v0, "SwitchState":Z
    :goto_0
    return v0

    .end local v0    # "SwitchState":Z
    :cond_0
    move v0, v2

    .line 590
    goto :goto_0
.end method

.method private CheckTestRecord()V
    .locals 9

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 453
    const/16 v0, 0x9

    new-array v7, v0, [I

    fill-array-data v7, :array_0

    .line 457
    .local v7, "intArr":[I
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mTestCheck:I

    if-ne v0, v3, :cond_3

    .line 458
    iput-boolean v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    .line 459
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 461
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustment:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 462
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setAlpha(F)V

    .line 464
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 466
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 471
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 472
    :cond_0
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/accessibility/AccessibilityManager;

    .line 474
    .local v8, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v8, v0, v7}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 490
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->isOptionsMenuEnable()V

    .line 495
    .end local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    :goto_1
    return-void

    .line 475
    .restart local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v6

    .line 477
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 480
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    float-to-double v2, v2

    iget v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v7

    .line 481
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/accessibility/AccessibilityManager;

    .line 483
    .restart local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v8, v0, v7}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 484
    :catch_1
    move-exception v6

    .line 486
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 493
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->RetestOperate()V

    goto :goto_1

    .line 453
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private CreateNegativeColorDialog()V
    .locals 3

    .prologue
    .line 358
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400
    :goto_0
    return-void

    .line 364
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 365
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 366
    const v1, 0x7f070009

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 367
    const v1, 0x1040013

    new-instance v2, Lcom/samsung/android/app/colorblind/ColorChipReport$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$5;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 382
    const v1, 0x1040009

    new-instance v2, Lcom/samsung/android/app/colorblind/ColorChipReport$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$6;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 390
    new-instance v1, Lcom/samsung/android/app/colorblind/ColorChipReport$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$7;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 399
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method private CreateRetestDialog()V
    .locals 3

    .prologue
    .line 327
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    :goto_0
    return-void

    .line 333
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 334
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070013

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 335
    const v1, 0x7f070012

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 336
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/app/colorblind/ColorChipReport$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$3;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 343
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/app/colorblind/ColorChipReport$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/colorblind/ColorChipReport$4;-><init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 350
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method private FinishAll()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 291
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v0, :cond_1

    .line 293
    :cond_0
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    .line 295
    .local v7, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v8, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    float-to-double v2, v2

    iget v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    .end local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/colorblind/ColorChipStart;->FinishAll()V

    .line 303
    return-void

    .line 296
    .restart local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v6

    .line 298
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private InitControls(Z)V
    .locals 11
    .param p1, "isRotateDevice"    # Z

    .prologue
    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, -0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 86
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "toStartActivity"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "prevActivity":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 88
    const-string v3, "fromSetting"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CheckSwitch()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    .line 90
    iput-boolean v7, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isFromSetting:Z

    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "color_blind_test"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mTestCheck:I

    .line 101
    const v3, 0x7f0a0004

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustment:Landroid/widget/TextView;

    .line 102
    const v3, 0x7f0a0005

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    .line 103
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 106
    if-nez p1, :cond_1

    .line 107
    new-instance v3, Landroid/widget/Switch;

    invoke-direct {v3, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    .line 108
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f060000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v6, v6, v4, v6}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 113
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 114
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v0, v10, v10}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 117
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    const v5, 0x800015

    invoke-direct {v4, v8, v8, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 120
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    iget-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 121
    iget-boolean v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    invoke-direct {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->SetSwitchOnOff(Z)V

    .line 122
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 123
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 124
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 126
    const v3, 0x7f0a0008

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mResultText:Landroid/widget/TextView;

    .line 128
    iget v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mTestCheck:I

    if-nez v3, :cond_3

    .line 129
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mResultText:Landroid/widget/TextView;

    const v4, 0x7f070010

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 141
    :goto_1
    return-void

    .line 93
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_2
    if-nez p1, :cond_0

    .line 94
    iput-boolean v7, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    goto/16 :goto_0

    .line 132
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_3
    iget v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 133
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mResultText:Landroid/widget/TextView;

    const v4, 0x7f070017

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 134
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustment:Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v9}, Landroid/widget/SeekBar;->setVisibility(I)V

    goto :goto_1

    .line 138
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mResultText:Landroid/widget/TextView;

    const v4, 0x7f070016

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method private RetestOperate()V
    .locals 4

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "color_blind_test"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 573
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/colorblind/ColorChipStart;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 574
    .local v0, "resetIntent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 575
    const-string v1, "toStartActivity"

    const-string v2, "fromSwitchOn"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 576
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->startActivity(Landroid/content/Intent;)V

    .line 577
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->finish()V

    .line 578
    return-void
.end method

.method private SetSwitchOnOff(Z)V
    .locals 7
    .param p1, "isOn"    # Z

    .prologue
    const v4, 0x3e99999a    # 0.3f

    const/4 v6, 0x0

    .line 408
    const/16 v3, 0x9

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 412
    .local v1, "intArr":[I
    if-eqz p1, :cond_3

    .line 413
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "high_contrast"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 415
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CreateNegativeColorDialog()V

    .line 416
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CheckTestRecord()V

    .line 443
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v3, :cond_0

    .line 444
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->isOptionsMenuEnable()V

    goto :goto_0

    .line 422
    :cond_3
    iput-boolean v6, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    .line 424
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustment:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 425
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setAlpha(F)V

    .line 427
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 429
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "color_blind"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 431
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 432
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 433
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "high_contrast"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    .line 434
    const-string v3, "accessibility"

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    .line 436
    .local v2, "manager":Landroid/view/accessibility/AccessibilityManager;
    :try_start_0
    invoke-virtual {v2, p1, v1}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 437
    :catch_0
    move-exception v0

    .line 439
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 408
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/colorblind/ColorChipReport;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ldmc/cvd/cvdcalculator/CVDCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/colorblind/ColorChipReport;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->RetestOperate()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/colorblind/ColorChipReport;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CheckTestRecord()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/colorblind/ColorChipReport;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/colorblind/ColorChipReport;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;
    .param p1, "x1"    # F

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/app/colorblind/ColorChipReport;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/colorblind/ColorChipReport;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/colorblind/ColorChipReport;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipReport;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    return-object v0
.end method


# virtual methods
.method public checkColorBlindState()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 185
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CheckSwitch()Z

    move-result v6

    .line 186
    .local v6, "checkSwitch":Z
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-eq v6, v0, :cond_0

    .line 187
    invoke-direct {p0, v6}, Lcom/samsung/android/app/colorblind/ColorChipReport;->SetSwitchOnOff(Z)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 191
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v0, :cond_2

    .line 193
    :cond_1
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/accessibility/AccessibilityManager;

    .line 195
    .local v8, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v9, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    float-to-double v2, v2

    iget v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v0

    invoke-virtual {v8, v9, v0}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_2
    :goto_0
    return-void

    .line 196
    .restart local v8    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v7

    .line 198
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isOptionsMenuEnable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    const v1, 0x7f070011

    invoke-interface {v0, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 257
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 307
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 309
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isFromSetting:Z

    if-nez v0, :cond_0

    .line 310
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->FinishAll()V

    .line 311
    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    const-string v1, "onCheckedChanged() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-direct {p0, p2}, Lcom/samsung/android/app/colorblind/ColorChipReport;->SetSwitchOnOff(Z)V

    .line 501
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 232
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->setContentView(I)V

    .line 235
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->InitControls(Z)V

    .line 236
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->setContentView(I)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 152
    new-instance v0, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->UpdatePrefCVDSettingValue()V

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    .line 156
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDseverity()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getUserParameter()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate : mCVDType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCVDseverity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mUserParameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->InitControls(Z)V

    .line 163
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 240
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 242
    const v0, 0x7f070011

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    .line 243
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;

    .line 244
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->isOptionsMenuEnable()V

    .line 245
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 543
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegColorObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    if-eqz v0, :cond_0

    .line 545
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    if-eqz v0, :cond_1

    .line 549
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 555
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    .line 557
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 558
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 559
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 561
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegativeColorDialog:Landroid/app/AlertDialog;

    .line 563
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 564
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keycode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 316
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-nez v0, :cond_0

    .line 317
    const/4 v0, 0x1

    .line 319
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 264
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 282
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 266
    :sswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z

    if-eqz v0, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->CreateRetestDialog()V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 273
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Click actionbar home icon : mUserParameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->onBackPressed()V

    goto :goto_0

    .line 264
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColorBlindObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 181
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 182
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;

    const-string v1, "onResume() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "color_blind"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mColorBlindObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 172
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "high_contrast"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport;->mNegColorObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 175
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->checkColorBlindState()V

    .line 176
    return-void
.end method
