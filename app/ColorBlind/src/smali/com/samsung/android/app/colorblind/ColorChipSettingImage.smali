.class public Lcom/samsung/android/app/colorblind/ColorChipSettingImage;
.super Landroid/app/Activity;
.source "ColorChipSettingImage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final MAX_COLOUR_ADJUSTMENT:I

.field private final TAG:Ljava/lang/String;

.field private cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

.field private mCVDType:I

.field private mCVDseverity:F

.field private mCancelKey:Landroid/widget/TextView;

.field private mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

.field private final mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mDoneKey:Landroid/widget/TextView;

.field private mIntegerArray:[I

.field private mNeedmDNIeControl:Z

.field private mProcess:I

.field private mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

.field private mTestImage:Landroid/graphics/Bitmap;

.field private mUserParameter:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    const-string v0, "ColorChipSettingImage"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 49
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mNeedmDNIeControl:Z

    .line 55
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->MAX_COLOUR_ADJUSTMENT:I

    .line 80
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void

    .line 49
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private Initcontrols()V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDseverity()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDseverity:F

    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getUserParameter()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F

    .line 68
    const v0, 0x7f0a0002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCancelKey:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCancelKey:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mDoneKey:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mDoneKey:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;
    .param p1, "x1"    # [I

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDseverity:F

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)Ldmc/cvd/cvdcalculator/CVDCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 191
    :goto_0
    return-void

    .line 174
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->onBackPressed()V

    goto :goto_0

    .line 179
    :pswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mNeedmDNIeControl:Z

    .line 180
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->setPrefUserParameter(F)V

    .line 181
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 182
    .local v0, "toReport":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 183
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x7f0a0002
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 131
    new-instance v0, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 132
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->setContentView(I)V

    .line 134
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->Initcontrols()V

    .line 135
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    const-string v0, "ColorChipSettingImage"

    const-string v1, "onDestroy() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    if-eqz v0, :cond_0

    .line 197
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mTestImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mTestImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 202
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mTestImage:Landroid/graphics/Bitmap;

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    if-eqz v0, :cond_2

    .line 206
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 208
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 210
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 139
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 140
    const-string v2, "ColorChipSettingImage"

    const-string v3, "onResume() is called"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iput-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mNeedmDNIeControl:Z

    .line 142
    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v2, :cond_1

    .line 143
    :cond_0
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 145
    .local v1, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v1    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    :goto_0
    return-void

    .line 146
    .restart local v1    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 155
    const-string v2, "ColorChipSettingImage"

    const-string v3, "onStop() is called"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-boolean v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mNeedmDNIeControl:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v2, :cond_1

    .line 158
    :cond_0
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 160
    .local v1, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .end local v1    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 167
    return-void

    .line 161
    .restart local v1    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
