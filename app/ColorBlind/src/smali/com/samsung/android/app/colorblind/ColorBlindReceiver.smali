.class public Lcom/samsung/android/app/colorblind/ColorBlindReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ColorBlindReceiver.java"


# instance fields
.field private final ACTION_COLORBLIND_SWITCH_OFF:Ljava/lang/String;

.field private final ACTION_COLORBLIND_SWITCH_ON:Ljava/lang/String;

.field private final ACTION_SHARING_COMPLETE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    const-string v0, "ColorBlindReceiver"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorBlindReceiver;->TAG:Ljava/lang/String;

    .line 23
    const-string v0, "com.samsung.android.app.shareaccessibilitysettings.SHARING_COMPLETE"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorBlindReceiver;->ACTION_SHARING_COMPLETE:Ljava/lang/String;

    .line 25
    const-string v0, "com.android.settings.ACTION_COLORBLIND_SWITCH_ON"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorBlindReceiver;->ACTION_COLORBLIND_SWITCH_ON:Ljava/lang/String;

    .line 27
    const-string v0, "com.android.settings.ACTION_COLORBLIND_SWITCH_OFF"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorBlindReceiver;->ACTION_COLORBLIND_SWITCH_OFF:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 40
    .local v8, "action":Ljava/lang/String;
    const/16 v2, 0x9

    new-array v9, v2, [I

    fill-array-data v9, :array_0

    .line 44
    .local v9, "integerArray":[I
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 47
    .local v11, "mSettingValue":Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    new-instance v0, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    .line 49
    .local v0, "cvdCalculator":Ldmc/cvd/cvdcalculator/CVDCalculator;
    const-string v2, "com.samsung.android.app.shareaccessibilitysettings.SHARING_COMPLETE"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 50
    const-string v2, "ColorBlindReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ColorBlindReceiver - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind_cvdtype"

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 54
    .local v1, "CVDType":I
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind_cvdseverity"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v6

    .line 56
    .local v6, "CVDSeverity":F
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind_user_parameter"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v7

    .line 59
    .local v7, "UserParameter":F
    const-string v2, "ColorBlindReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReadSharingPref CVDType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ReadSharingPref CVDSeverity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ReadSharingPref UserParameter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-virtual {v11, v1, v6, v7}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->SavePrefCVDSettingValue(IFF)V

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    .line 67
    .local v10, "isSetting":I
    const/4 v2, 0x1

    if-ne v10, v2, :cond_1

    .line 68
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 69
    :cond_0
    float-to-double v2, v6

    float-to-double v4, v7

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v9

    .line 70
    const-string v2, "accessibility"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/accessibility/AccessibilityManager;

    .line 71
    .local v12, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x1

    invoke-virtual {v12, v2, v9}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 91
    .end local v1    # "CVDType":I
    .end local v6    # "CVDSeverity":F
    .end local v7    # "UserParameter":F
    .end local v10    # "isSetting":I
    .end local v12    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    :goto_0
    return-void

    .line 74
    :cond_2
    const-string v2, "com.android.settings.ACTION_COLORBLIND_SWITCH_ON"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 75
    const-string v2, "ColorBlindReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ColorBlindReceiver - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDType()I

    move-result v1

    .line 78
    .restart local v1    # "CVDType":I
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDseverity()F

    move-result v6

    .line 79
    .restart local v6    # "CVDSeverity":F
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getUserParameter()F

    move-result v7

    .line 81
    .restart local v7    # "UserParameter":F
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 82
    :cond_3
    float-to-double v2, v6

    float-to-double v4, v7

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v9

    .line 83
    const-string v2, "accessibility"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/accessibility/AccessibilityManager;

    .line 84
    .restart local v12    # "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x1

    invoke-virtual {v12, v2, v9}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    goto :goto_0

    .line 86
    .end local v1    # "CVDType":I
    .end local v6    # "CVDSeverity":F
    .end local v7    # "UserParameter":F
    .end local v12    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_4
    const-string v2, "com.android.settings.ACTION_COLORBLIND_SWITCH_OFF"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    const-string v2, "ColorBlindReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ColorBlindReceiver - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string v2, "accessibility"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/accessibility/AccessibilityManager;

    .line 89
    .restart local v12    # "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x0

    invoke-virtual {v12, v2, v9}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    goto :goto_0

    .line 40
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method
