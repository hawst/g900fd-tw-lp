.class Lcom/samsung/android/app/colorblind/ColorChipTest$1;
.super Landroid/os/Handler;
.source "ColorChipTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipTest;)V
    .locals 0

    .prologue
    .line 965
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 974
    const-string v9, "ColorChipTest"

    const-string v10, "mHandler"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    .line 1179
    :cond_0
    :goto_0
    return-void

    .line 980
    :pswitch_0
    const-string v9, "ColorChipTest"

    const-string v10, "mHandler :: MSG_LONG_CLICK"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/view/View;

    .line 984
    .local v6, "v":Landroid/view/View;
    const/4 v8, -0x1

    .line 985
    .local v8, "widgetColor":I
    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v7

    .line 987
    .local v7, "viewId":I
    packed-switch v7, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 1003
    :pswitch_2
    const-string v9, "ColorChipTest"

    const-string v10, "mHandler :: MSG_TRADE_CHIP - R.id.color_01~15"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSampleViewIdx(Landroid/view/View;)I
    invoke-static {v9, v6}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$000(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/view/View;)I

    move-result v2

    .line 1007
    .local v2, "idx":I
    const/16 v9, 0x64

    if-ge v2, v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$100(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    const/16 v10, 0x64

    if-ge v9, v10, :cond_0

    .line 1011
    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v10, v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$102(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    .line 1012
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$100(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    const/16 v10, 0xf

    if-ge v9, v10, :cond_0

    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$100(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    if-ltz v9, :cond_0

    .line 1013
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$300(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$100(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1017
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;

    move-result-object v10

    if-eq v9, v10, :cond_1

    .line 1018
    const/4 v9, 0x2

    new-array v3, v9, [I

    .line 1019
    .local v3, "location":[I
    invoke-virtual {v6, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1021
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    const/4 v10, 0x0

    aget v10, v3, v10

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060005

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$502(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1025
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    const/4 v10, 0x1

    aget v10, v3, v10

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060005

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060004

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060006

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$602(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1037
    .end local v3    # "location":[I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I
    invoke-static {v9, v8}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$702(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    .line 1038
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    check-cast v6, Landroid/widget/ImageView;

    .end local v6    # "v":Landroid/view/View;
    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;
    invoke-static {v9, v6}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$802(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1039
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$800(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/ImageView;

    move-result-object v9

    const v10, 0x7f020055

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1042
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->ShowWidget()V
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$900(Lcom/samsung/android/app/colorblind/ColorChipTest;)V

    .line 1043
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->FocusFirstBlankItem()V
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1000(Lcom/samsung/android/app/colorblind/ColorChipTest;)V

    goto/16 :goto_0

    .line 1033
    .restart local v6    # "v":Landroid/view/View;
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v10

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$502(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1034
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getY()F

    move-result v11

    add-float/2addr v10, v11

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$602(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    goto :goto_1

    .line 1062
    .end local v2    # "idx":I
    :pswitch_3
    const-string v9, "ColorChipTest"

    const-string v10, "mHandler :: MSG_TRADE_CHIP - R.id.colorChipimg01~15"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSelectViewIdx(Landroid/view/View;)I
    invoke-static {v9, v6}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1100(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/view/View;)I

    move-result v2

    .line 1066
    .restart local v2    # "idx":I
    const/4 v9, -0x1

    if-eq v2, v9, :cond_2

    .line 1067
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1069
    .local v0, "coloridx":I
    const/4 v9, -0x1

    if-eq v0, v9, :cond_0

    .line 1072
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1302(Lcom/samsung/android/app/colorblind/ColorChipTest;Z)Z

    .line 1073
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$300(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1074
    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I
    invoke-static {v10, v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$102(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    .line 1076
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1077
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1078
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1079
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9, v2}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1502(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    .line 1082
    .end local v0    # "coloridx":I
    :cond_2
    const/4 v9, -0x1

    if-eq v8, v9, :cond_0

    .line 1083
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;

    move-result-object v10

    if-eq v9, v10, :cond_3

    .line 1084
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v10

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$502(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1085
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mlistlayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1600(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getY()F

    move-result v10

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v11

    add-float/2addr v10, v11

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1700(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/LinearLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getY()F

    move-result v11

    add-float/2addr v10, v11

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$602(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1091
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I
    invoke-static {v9, v8}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$702(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    .line 1092
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    check-cast v6, Landroid/widget/ImageView;

    .end local v6    # "v":Landroid/view/View;
    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;
    invoke-static {v9, v6}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$802(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1093
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->ShowWidget()V
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$900(Lcom/samsung/android/app/colorblind/ColorChipTest;)V

    .line 1094
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipTest;->FindNearItem()V
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1800(Lcom/samsung/android/app/colorblind/ColorChipTest;)V

    goto/16 :goto_0

    .line 1087
    .restart local v6    # "v":Landroid/view/View;
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v10

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$502(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    .line 1088
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getY()F

    move-result v11

    add-float/2addr v10, v11

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$602(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F

    goto :goto_2

    .line 1105
    .end local v2    # "idx":I
    .end local v6    # "v":Landroid/view/View;
    .end local v7    # "viewId":I
    .end local v8    # "widgetColor":I
    :pswitch_4
    const-string v9, "ColorChipTest"

    const-string v10, "mHandler :: MSG_TRADE_CHIP"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    if-ltz v9, :cond_0

    .line 1110
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsRunningAnimation:Z
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1900(Lcom/samsung/android/app/colorblind/ColorChipTest;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    const/4 v10, -0x1

    if-le v9, v10, :cond_0

    .line 1111
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060001

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1114
    .local v4, "startX":I
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    if-le v9, v10, :cond_5

    .line 1115
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-virtual {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1117
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v1

    .local v1, "i":I
    :goto_3
    iget v9, p1, Landroid/os/Message;->arg1:I

    if-le v1, v9, :cond_6

    .line 1118
    add-int/lit8 v9, v1, -0x1

    const/4 v10, -0x1

    if-le v9, v10, :cond_4

    .line 1119
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v10

    add-int/lit8 v11, v1, -0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1120
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1121
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$2000(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v11

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1117
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 1128
    .end local v1    # "i":I
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v1

    .restart local v1    # "i":I
    :goto_4
    iget v9, p1, Landroid/os/Message;->arg1:I

    if-ge v1, v9, :cond_6

    .line 1129
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ltz v9, :cond_0

    .line 1132
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v10

    add-int/lit8 v11, v1, 0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1133
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1134
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$2000(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v11

    iget-object v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1128
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1139
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    const/4 v11, -0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1140
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1141
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1143
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    int-to-float v9, v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v5, v9, v10, v11, v12}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1144
    .local v5, "tranTradeAni":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v10, 0xc8

    invoke-virtual {v5, v10, v11}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1145
    new-instance v9, Lcom/samsung/android/app/colorblind/ColorChipTest$1$1;

    invoke-direct {v9, p0}, Lcom/samsung/android/app/colorblind/ColorChipTest$1$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipTest$1;)V

    invoke-virtual {v5, v9}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1160
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsRunningAnimation:Z
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1902(Lcom/samsung/android/app/colorblind/ColorChipTest;Z)Z

    .line 1161
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    if-le v9, v10, :cond_7

    .line 1162
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v1

    :goto_5
    iget v9, p1, Landroid/os/Message;->arg1:I

    if-le v1, v9, :cond_8

    .line 1163
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1162
    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    .line 1166
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I

    move-result v1

    :goto_6
    iget v9, p1, Landroid/os/Message;->arg1:I

    if-ge v1, v9, :cond_8

    .line 1167
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1166
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1170
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipTest;

    iget v10, p1, Landroid/os/Message;->arg1:I

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I
    invoke-static {v9, v10}, Lcom/samsung/android/app/colorblind/ColorChipTest;->access$1502(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I

    goto/16 :goto_0

    .line 978
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 987
    :pswitch_data_1
    .packed-switch 0x7f0a000f
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
