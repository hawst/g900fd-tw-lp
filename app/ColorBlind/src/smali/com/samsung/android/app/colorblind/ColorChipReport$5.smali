.class Lcom/samsung/android/app/colorblind/ColorChipReport$5;
.super Ljava/lang/Object;
.source "ColorChipReport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/colorblind/ColorChipReport;->CreateNegativeColorDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$5;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v4, 0x0

    .line 370
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$5;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-virtual {v2}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "high_contrast"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 372
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$5;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 374
    .local v1, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeNegative(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$5;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # invokes: Lcom/samsung/android/app/colorblind/ColorChipReport;->CheckTestRecord()V
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$300(Lcom/samsung/android/app/colorblind/ColorChipReport;)V

    .line 380
    return-void

    .line 375
    :catch_0
    move-exception v0

    .line 377
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
