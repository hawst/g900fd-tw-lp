.class public Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
.super Landroid/app/Activity;
.source "ColorChipSettingPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final MAX_COLOUR_ADJUSTMENT:I

.field private final TAG:Ljava/lang/String;

.field private cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

.field private integerArray:[I

.field private mCVDType:I

.field private mCVDseverity:F

.field private mCamera:Landroid/hardware/Camera;

.field private mCancelKey:Landroid/widget/TextView;

.field private mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

.field private final mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mDoneKey:Landroid/widget/TextView;

.field private mNeedmDNIeControl:Z

.field private mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

.field private final mSurfaceListener:Landroid/view/SurfaceHolder$Callback;

.field private mUserParameter:F

.field private surface:Landroid/view/SurfaceView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    const-string v0, "ColorChipSettingPreview"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->TAG:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    .line 50
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mNeedmDNIeControl:Z

    .line 56
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->MAX_COLOUR_ADJUSTMENT:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 89
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSurfaceListener:Landroid/view/SurfaceHolder$Callback;

    .line 186
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$2;-><init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void

    .line 50
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private Initcontrols()V
    .locals 3

    .prologue
    .line 69
    const-string v0, "ColorChipSettingPreview"

    const-string v1, "Initcontrols()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDseverity()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDseverity:F

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getUserParameter()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F

    .line 77
    const v0, 0x7f0a0002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCancelKey:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCancelKey:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mDoneKey:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mDoneKey:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mColourAdjustmentSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
    .param p1, "x1"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->setSize(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
    .param p1, "x1"    # F

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;
    .param p1, "x1"    # [I

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDseverity:F

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Ldmc/cvd/cvdcalculator/CVDCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    return-object v0
.end method

.method private setSize(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 20
    .param p1, "parameters"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 335
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 336
    .local v2, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v15

    invoke-interface {v15}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    invoke-virtual {v15, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 337
    iget v8, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 338
    .local v8, "lcdWidth":I
    iget v5, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 339
    .local v5, "lcdHeight":I
    int-to-double v0, v8

    move-wide/from16 v16, v0

    int-to-double v0, v5

    move-wide/from16 v18, v0

    div-double v6, v16, v18

    .line 340
    .local v6, "lcdRatio":D
    const-string v15, "ColorChipSettingPreview"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "setSize() :: LCDSize - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ":"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v11

    .line 344
    .local v11, "previewSizeList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const/4 v10, 0x0

    .line 345
    .local v10, "matchWidth":I
    const/4 v9, 0x0

    .line 346
    .local v9, "matchHeight":I
    if-eqz v11, :cond_2

    .line 347
    const/4 v15, 0x0

    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/hardware/Camera$Size;

    iget v10, v15, Landroid/hardware/Camera$Size;->width:I

    .line 348
    const/4 v15, 0x0

    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/hardware/Camera$Size;

    iget v9, v15, Landroid/hardware/Camera$Size;->height:I

    .line 349
    int-to-double v0, v10

    move-wide/from16 v16, v0

    int-to-double v0, v9

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    sub-double v16, v6, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    .line 351
    .local v12, "ratioGap":D
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_1

    .line 352
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/hardware/Camera$Size;

    iget v14, v15, Landroid/hardware/Camera$Size;->width:I

    .line 353
    .local v14, "width":I
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/hardware/Camera$Size;

    iget v3, v15, Landroid/hardware/Camera$Size;->height:I

    .line 354
    .local v3, "height":I
    const-string v15, "ColorChipSettingPreview"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "setSize() :: getSupportedPreviewSizes() - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ":"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    int-to-double v0, v14

    move-wide/from16 v16, v0

    int-to-double v0, v3

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    sub-double v16, v6, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    cmpl-double v15, v12, v16

    if-lez v15, :cond_0

    .line 356
    move v10, v14

    .line 357
    move v9, v3

    .line 358
    int-to-double v0, v10

    move-wide/from16 v16, v0

    int-to-double v0, v9

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    sub-double v16, v6, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    .line 351
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 362
    .end local v3    # "height":I
    .end local v14    # "width":I
    :cond_1
    const-string v15, "ColorChipSettingPreview"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "setSize() :: find size - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ":"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v4    # "i":I
    .end local v12    # "ratioGap":D
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 366
    return-object p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 282
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 300
    :goto_0
    return-void

    .line 284
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->onBackPressed()V

    goto :goto_0

    .line 289
    :pswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mNeedmDNIeControl:Z

    .line 290
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mUserParameter:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->setPrefUserParameter(F)V

    .line 291
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    .local v0, "toReport":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 293
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch 0x7f0a0002
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 226
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 228
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->setContentView(I)V

    .line 229
    new-instance v1, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v1}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 231
    const v1, 0x7f0a0001

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    .line 232
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 234
    .local v0, "holder":Landroid/view/SurfaceHolder;
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSurfaceListener:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 239
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->Initcontrols()V

    .line 240
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 313
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    if-eqz v0, :cond_0

    .line 314
    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    if-eqz v0, :cond_1

    .line 317
    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 322
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 323
    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    .line 325
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 326
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 270
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 271
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 272
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 275
    .local v0, "holder":Landroid/view/SurfaceHolder;
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSurfaceListener:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 276
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 277
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 278
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 254
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 255
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 256
    .local v0, "holder":Landroid/view/SurfaceHolder;
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSurfaceListener:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 257
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 258
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->surface:Landroid/view/SurfaceView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 259
    iput-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mNeedmDNIeControl:Z

    .line 260
    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v2, :cond_1

    .line 261
    :cond_0
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 262
    .local v1, "manager":Landroid/view/accessibility/AccessibilityManager;
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I

    invoke-virtual {v1, v4, v2}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 264
    .end local v1    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 244
    iget-boolean v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mNeedmDNIeControl:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCVDType:I

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 246
    :cond_0
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 247
    .local v0, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->integerArray:[I

    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 249
    .end local v0    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 250
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 304
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 308
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
