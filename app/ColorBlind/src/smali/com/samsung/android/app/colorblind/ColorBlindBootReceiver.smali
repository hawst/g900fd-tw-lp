.class public Lcom/samsung/android/app/colorblind/ColorBlindBootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ColorBlindBootReceiver.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    const-string v0, "ColorBlindBootReceiver"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorBlindBootReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 33
    .local v8, "action":Ljava/lang/String;
    const-string v2, "ColorBlindBootReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ColorBlindBootReceiver - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind_test"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    .line 38
    .local v13, "testCheck":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_blind"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    .line 41
    .local v10, "isSetting":I
    const/4 v2, 0x1

    if-ne v13, v2, :cond_1

    const/4 v2, 0x1

    if-ne v10, v2, :cond_1

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 45
    .local v11, "mSettingValue":Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDType()I

    move-result v1

    .line 46
    .local v1, "CVDType":I
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getCVDseverity()F

    move-result v6

    .line 47
    .local v6, "CVDSeverity":F
    invoke-virtual {v11}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->getUserParameter()F

    move-result v7

    .line 49
    .local v7, "UserParameter":F
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 50
    :cond_0
    new-instance v0, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    .line 52
    .local v0, "cvdCalculator":Ldmc/cvd/cvdcalculator/CVDCalculator;
    float-to-double v2, v6

    float-to-double v4, v7

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v9

    .line 53
    .local v9, "integerArray":[I
    const-string v2, "accessibility"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/accessibility/AccessibilityManager;

    .line 54
    .local v12, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v2, 0x1

    invoke-virtual {v12, v2, v9}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 57
    .end local v0    # "cvdCalculator":Ldmc/cvd/cvdcalculator/CVDCalculator;
    .end local v1    # "CVDType":I
    .end local v6    # "CVDSeverity":F
    .end local v7    # "UserParameter":F
    .end local v9    # "integerArray":[I
    .end local v11    # "mSettingValue":Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    .end local v12    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_1
    return-void
.end method
