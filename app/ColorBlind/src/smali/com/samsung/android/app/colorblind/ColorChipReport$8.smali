.class Lcom/samsung/android/app/colorblind/ColorChipReport$8;
.super Ljava/lang/Object;
.source "ColorChipReport.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipReport;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v3, 0x1

    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$500(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mClearControlListener :: onProgressChanged() is called start progress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fromUser "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    if-eqz p3, :cond_0

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    int-to-float v1, p2

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$602(Lcom/samsung/android/app/colorblind/ColorChipReport;F)F

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->isSwitch:Z
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$700(Lcom/samsung/android/app/colorblind/ColorChipReport;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$800(Lcom/samsung/android/app/colorblind/ColorChipReport;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$900(Lcom/samsung/android/app/colorblind/ColorChipReport;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$800(Lcom/samsung/android/app/colorblind/ColorChipReport;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$900(Lcom/samsung/android/app/colorblind/ColorChipReport;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v0, :cond_2

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    .line 517
    .local v7, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v8, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$1100(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ldmc/cvd/cvdcalculator/CVDCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDType:I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$800(Lcom/samsung/android/app/colorblind/ColorChipReport;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mCVDseverity:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$1000(Lcom/samsung/android/app/colorblind/ColorChipReport;)F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F
    invoke-static {v4}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$600(Lcom/samsung/android/app/colorblind/ColorChipReport;)F

    move-result v4

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$900(Lcom/samsung/android/app/colorblind/ColorChipReport;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$600(Lcom/samsung/android/app/colorblind/ColorChipReport;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->setPrefUserParameter(F)V

    .line 524
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$500(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChanged() mUserParameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mUserParameter:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$600(Lcom/samsung/android/app/colorblind/ColorChipReport;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    .end local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_2
    return-void

    .line 519
    .restart local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :catch_0
    move-exception v6

    .line 521
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 530
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$500(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStartTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$8;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$500(Lcom/samsung/android/app/colorblind/ColorChipReport;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStopTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    return-void
.end method
