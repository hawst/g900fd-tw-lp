.class public Lcom/samsung/android/app/colorblind/ColorChipStart;
.super Landroid/app/Activity;
.source "ColorChipStart.java"


# static fields
.field private static mThis:Landroid/app/Activity;


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const-string v0, "ColorChipStart"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipStart;->TAG:Ljava/lang/String;

    return-void
.end method

.method private CheckFinishAll()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXIT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->finish()V

    .line 133
    const/4 v0, 0x1

    .line 135
    :cond_0
    return v0
.end method

.method public static FinishAll()V
    .locals 3

    .prologue
    .line 117
    sget-object v1, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 118
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    const-class v2, Lcom/samsung/android/app/colorblind/ColorChipStart;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 120
    const-string v1, "EXIT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 121
    sget-object v1, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 123
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 31
    const-string v6, "ColorChipStart"

    const-string v7, "onCreate()"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    sput-object p0, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    .line 36
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->CheckFinishAll()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 65
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 41
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 42
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070006

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 45
    const/high16 v6, 0x7f030000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/colorblind/ColorChipStart;->setContentView(I)V

    .line 47
    const/high16 v6, 0x7f0a0000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/colorblind/ColorChipStart;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 48
    .local v3, "mainText":Landroid/widget/TextView;
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 49
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 52
    .local v5, "settingValue":Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x3

    invoke-virtual {v5, v6, v9, v9}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->SavePrefCVDSettingValue(IFF)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "color_blind"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 55
    const/16 v6, 0x9

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    .line 58
    .local v2, "integerArray":[I
    const-string v6, "accessibility"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    .line 60
    .local v4, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v4, v6, v2}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 55
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const/high16 v5, 0x7f090000

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 99
    const v4, 0x7f0a0038

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 100
    .local v0, "cancelItem":Landroid/view/MenuItem;
    const v4, 0x7f0a003a

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 101
    .local v1, "doneItem":Landroid/view/MenuItem;
    const v4, 0x7f0a0039

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 102
    .local v2, "skipItem":Landroid/view/MenuItem;
    const v4, 0x7f0a003b

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 104
    .local v3, "startItem":Landroid/view/MenuItem;
    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 105
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 106
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 107
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 109
    return v7
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "ColorChipStart"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 71
    sget-object v0, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/colorblind/ColorChipStart;->mThis:Landroid/app/Activity;

    .line 74
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 78
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 92
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 80
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->finish()V

    goto :goto_0

    .line 83
    :sswitch_1
    const-string v1, "ColorChipStart"

    const-string v2, "onOptionsItemSelected() :: R.id.menu_start"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/colorblind/ColorChipTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipStart;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 78
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0a003b -> :sswitch_1
    .end sparse-switch
.end method
