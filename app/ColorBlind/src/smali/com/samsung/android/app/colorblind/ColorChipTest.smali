.class public Lcom/samsung/android/app/colorblind/ColorChipTest;
.super Landroid/app/Activity;
.source "ColorChipTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static input:[I


# instance fields
.field private final ANIMATION_DELAY_TIMER:I

.field private final ANIMATION_DURATION:I

.field private final BLANK_OFFSET:I

.field private final COLOR_NONE:I

.field private final INVALID_IDX:I

.field private final LONG_CLICK_TIMER:I

.field private final MAX_COLOR_CHIP:I

.field private final MSG_LONG_CLICK:I

.field private final MSG_TRADE_CHIP:I

.field private final TAG:Ljava/lang/String;

.field private cancelItem:Landroid/view/View;

.field private mCVDSeverity:F

.field private mCVDType:I

.field private mColorChipSampleIndex:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mColorChipSampleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mColorChipSelectIndex:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mColorChipSelectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mDoneItemActionBar:Landroid/view/View;

.field private mDragSelectedItemId:I

.field private mFocusedColorIndex:I

.field private final mHandler:Landroid/os/Handler;

.field private mIsRunningAnimation:Z

.field private mIsWidget:Z

.field private mLowTouch:Z

.field private mLowerImageViewArray:Landroid/content/res/TypedArray;

.field private mLowerLayout:Landroid/widget/RelativeLayout;

.field private mPrePositionX:F

.field private mPrePositionY:F

.field private mPressedViewId:I

.field private mPreviousFocusIndex:I

.field private mSampleLongList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSmallList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedColorIndex:I

.field private mUpperImageViewArray:Landroid/content/res/TypedArray;

.field private mUpperLayout:Landroid/widget/LinearLayout;

.field private mUserParameter:F

.field private mWidget:Landroid/widget/ImageView;

.field private mWidgetDragColorId:I

.field private mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mWidgetSampleDragList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetSelectedView:Landroid/widget/ImageView;

.field private mWidgetX:F

.field private mWidgetY:F

.field private mWm:Landroid/view/WindowManager;

.field private mlistlayout:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    const/16 v0, 0xf

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->input:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/16 v1, 0xf

    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const-string v0, "ColorChipTest"

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->TAG:Ljava/lang/String;

    .line 47
    iput v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->MAX_COLOR_CHIP:I

    .line 49
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->COLOR_NONE:I

    .line 51
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->INVALID_IDX:I

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->BLANK_OFFSET:I

    .line 61
    iput-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsRunningAnimation:Z

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    .line 75
    iput-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWm:Landroid/view/WindowManager;

    .line 77
    iput-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 79
    iput-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    .line 81
    iput-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    .line 83
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I

    .line 85
    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    .line 87
    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    .line 89
    iput-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    .line 91
    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionX:F

    .line 93
    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionY:F

    .line 95
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 97
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I

    .line 99
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    .line 101
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPreviousFocusIndex:I

    .line 107
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUserParameter:F

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    .line 115
    iput-boolean v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    .line 117
    iput v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    .line 955
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->MSG_LONG_CLICK:I

    .line 957
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->MSG_TRADE_CHIP:I

    .line 959
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->LONG_CLICK_TIMER:I

    .line 961
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->ANIMATION_DELAY_TIMER:I

    .line 963
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->ANIMATION_DURATION:I

    .line 965
    new-instance v0, Lcom/samsung/android/app/colorblind/ColorChipTest$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/colorblind/ColorChipTest$1;-><init>(Lcom/samsung/android/app/colorblind/ColorChipTest;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private AssignDragItem(Landroid/view/View;)V
    .locals 13
    .param p1, "viewFrom"    # Landroid/view/View;

    .prologue
    const/4 v12, 0x0

    const/16 v11, 0xf

    const/4 v10, -0x1

    .line 516
    const-string v7, "ColorChipTest"

    const-string v8, "AssignDragItem()"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    iget v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    if-eq v7, v10, :cond_5

    .line 520
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 522
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v11, :cond_0

    .line 523
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 524
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    const v8, 0x7f020067

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 526
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 530
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->Drop()V

    .line 548
    .end local v2    # "i":I
    :goto_1
    iget-boolean v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    if-nez v7, :cond_1

    .line 549
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSampleViewIdx(Landroid/view/View;)I

    move-result v6

    .line 550
    .local v6, "vIdx":I
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 552
    .local v5, "preIdx":I
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    add-int/lit8 v8, v5, 0x64

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 575
    .end local v5    # "preIdx":I
    .end local v6    # "vIdx":I
    :cond_1
    :goto_2
    iput v10, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 576
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    if-ge v2, v11, :cond_a

    .line 577
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, v10, :cond_9

    .line 578
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    const v8, 0x7f070001

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 576
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 522
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 533
    .end local v2    # "i":I
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 534
    .local v0, "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->CheckBlankItem()I

    move-result v1

    .line 536
    .local v1, "blankIdx":I
    iget v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    if-le v1, v7, :cond_4

    .line 537
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->RightCopy(Ljava/util/ArrayList;I)V

    .line 538
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->Drop()V

    .line 539
    invoke-direct {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ShiftRight(Ljava/util/ArrayList;)V

    .line 545
    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 541
    :cond_4
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->LeftCopy(Ljava/util/ArrayList;I)V

    .line 542
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->Drop()V

    .line 543
    invoke-direct {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ShiftLeft(Ljava/util/ArrayList;)V

    goto :goto_5

    .line 556
    .end local v0    # "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v1    # "blankIdx":I
    :cond_5
    iget-boolean v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    if-eqz v7, :cond_8

    .line 557
    const-string v7, "ColorChipTest"

    const-string v8, "AssignDragItem() :: mLowTouch is true"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    if-ge v2, v11, :cond_1

    .line 559
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    iget v8, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 561
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_7
    if-ge v3, v11, :cond_7

    .line 562
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v7, v7, -0x64

    if-ne v7, v2, :cond_6

    .line 563
    const-string v7, "ColorChipTest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AssignDragItem() :: j = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", i = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-direct {p0, v7}, Lcom/samsung/android/app/colorblind/ColorChipTest;->SetEnableImageColor(Landroid/view/View;)V

    .line 561
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 558
    .end local v3    # "j":I
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 571
    .end local v2    # "i":I
    :cond_8
    const-string v7, "ColorChipTest"

    const-string v8, "AssignDragItem() :: mLowTouch is false"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    invoke-direct {p0, v7}, Lcom/samsung/android/app/colorblind/ColorChipTest;->SetEnableImageColor(Landroid/view/View;)V

    goto/16 :goto_2

    .line 580
    .restart local v2    # "i":I
    :cond_9
    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    const v8, 0x7f07000f

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 583
    :cond_a
    iput-boolean v12, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    .line 585
    const/4 v4, 0x0

    .line 586
    .local v4, "mAudioManager":Landroid/media/AudioManager;
    const-string v7, "audio"

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "mAudioManager":Landroid/media/AudioManager;
    check-cast v4, Landroid/media/AudioManager;

    .line 587
    .restart local v4    # "mAudioManager":Landroid/media/AudioManager;
    invoke-virtual {v4, v12}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 589
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ToggleDoneKey()V

    .line 590
    return-void
.end method

.method private CheckBlankItem()I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1354
    const-string v1, "ColorChipTest"

    const-string v3, "CheckBlankItem()"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_1

    .line 1356
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1357
    const-string v1, "ColorChipTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CheckBlankItem() :: blank item index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 1355
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1361
    goto :goto_1
.end method

.method private ClearFocusImage()V
    .locals 3

    .prologue
    .line 1369
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    .line 1370
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1372
    :cond_0
    return-void
.end method

.method private Drop()V
    .locals 3

    .prologue
    .line 467
    const-string v0, "ColorChipTest"

    const-string v1, "Drop()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 471
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 474
    :cond_0
    return-void
.end method

.method private FindNearItem()V
    .locals 28

    .prologue
    .line 597
    const-string v24, "ColorChipTest"

    const-string v25, "FindNearItem()"

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 599
    .local v19, "rectWidget":Landroid/graphics/Rect;
    const/4 v11, 0x0

    .line 600
    .local v11, "isIntersects":Z
    const/4 v12, -0x1

    .line 601
    .local v12, "itemIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v9

    .line 603
    .local v9, "gapMin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ImageView;->getWidth()I

    move-result v23

    .line 604
    .local v23, "widgetWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ImageView;->getHeight()I

    move-result v22

    .line 606
    .local v22, "widgetHeight":I
    if-gtz v23, :cond_0

    if-gtz v22, :cond_0

    .line 607
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f060005

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 608
    move/from16 v22, v23

    .line 611
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    add-int v26, v26, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    add-int v27, v27, v22

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 614
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v10, v0, :cond_4

    .line 615
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 617
    .local v14, "rectItem":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 619
    .local v6, "Item":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getX()F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v17, v0

    .line 620
    .local v17, "rectItemX":I
    const-string v24, "com.sec.feature.cocktailbar"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    invoke-virtual {v6}, Landroid/widget/ImageView;->getY()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getY()F

    move-result v25

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f060003

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v18, v0

    .line 624
    .local v18, "rectItemY":I
    :goto_1
    invoke-virtual {v6}, Landroid/widget/ImageView;->getWidth()I

    move-result v24

    add-int v24, v24, v17

    invoke-virtual {v6}, Landroid/widget/ImageView;->getHeight()I

    move-result v25

    add-int v25, v25, v18

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 627
    move-object/from16 v0, v19

    invoke-static {v0, v14}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 629
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v20

    .line 630
    .local v20, "rectWidgetCX":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerY()I

    move-result v21

    .line 632
    .local v21, "rectWidgetCY":I
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v15

    .line 633
    .local v15, "rectItemCX":I
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v16

    .line 635
    .local v16, "rectItemCY":I
    const-string v24, "ColorChipTest"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "FindNearItem - rectWidgetCX :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " rectWidgetCY "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const-string v24, "ColorChipTest"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "FindNearItem - rectItemCX :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " rectItemCY "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    sub-int v24, v20, v15

    sub-int v25, v20, v15

    mul-int v24, v24, v25

    sub-int v25, v21, v16

    sub-int v26, v21, v16

    mul-int v25, v25, v26

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v8, v0

    .line 644
    .local v8, "gap":I
    const-string v24, "ColorChipTest"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "FindNearItem - gapMin :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " gap "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    if-le v9, v8, :cond_1

    .line 647
    move v9, v8

    .line 648
    move v12, v10

    .line 650
    :cond_1
    const/4 v11, 0x1

    .line 614
    .end local v8    # "gap":I
    .end local v15    # "rectItemCX":I
    .end local v16    # "rectItemCY":I
    .end local v20    # "rectWidgetCX":I
    .end local v21    # "rectWidgetCY":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 620
    .end local v18    # "rectItemY":I
    :cond_3
    invoke-virtual {v6}, Landroid/widget/ImageView;->getY()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getY()F

    move-result v25

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v18, v0

    goto/16 :goto_1

    .line 654
    .end local v6    # "Item":Landroid/widget/ImageView;
    .end local v14    # "rectItem":Landroid/graphics/Rect;
    .end local v17    # "rectItemX":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ne v12, v0, :cond_6

    .line 655
    if-eqz v11, :cond_5

    .line 656
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 748
    :cond_5
    :goto_2
    return-void

    .line 660
    :cond_6
    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-eq v12, v0, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    .line 663
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 664
    .local v13, "msg":Landroid/os/Message;
    const/16 v24, 0x2

    move/from16 v0, v24

    iput v0, v13, Landroid/os/Message;->what:I

    .line 665
    iput v12, v13, Landroid/os/Message;->arg1:I

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->removeMessages(I)V

    .line 667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    const-wide/16 v26, 0x64

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 670
    .end local v13    # "msg":Landroid/os/Message;
    :cond_7
    if-eqz v11, :cond_10

    .line 672
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_b

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/widget/ImageView;

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 685
    :cond_8
    :goto_3
    const/16 v24, -0x1

    move/from16 v0, v24

    if-eq v12, v0, :cond_a

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v25

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const v25, 0x7f020069

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 694
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPreviousFocusIndex:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPreviousFocusIndex:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-eq v0, v12, :cond_9

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPreviousFocusIndex:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 697
    :cond_9
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPreviousFocusIndex:I

    .line 733
    :cond_a
    :goto_4
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    goto/16 :goto_2

    .line 679
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const v25, 0x7f020067

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 699
    :cond_c
    const/4 v7, -0x1

    .line 700
    .local v7, "blankIndex":I
    const/4 v10, 0x0

    :goto_5
    const/16 v24, 0xf

    move/from16 v0, v24

    if-ge v10, v0, :cond_d

    .line 702
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v24

    if-eqz v24, :cond_e

    .line 705
    move v7, v10

    .line 725
    :cond_d
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 726
    .restart local v13    # "msg":Landroid/os/Message;
    const/16 v24, 0x2

    move/from16 v0, v24

    iput v0, v13, Landroid/os/Message;->what:I

    .line 727
    add-int/lit8 v24, v7, -0x1

    move/from16 v0, v24

    iput v0, v13, Landroid/os/Message;->arg1:I

    .line 728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->removeMessages(I)V

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 730
    add-int/lit8 v24, v7, -0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    goto/16 :goto_4

    .line 711
    .end local v13    # "msg":Landroid/os/Message;
    :cond_e
    add-int/lit8 v24, v10, 0x1

    const/16 v25, 0xf

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_5

    .line 713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    add-int/lit8 v25, v10, 0x1

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_5

    .line 700
    :cond_f
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_5

    .line 735
    .end local v7    # "blankIndex":I
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ClearFocusImage()V

    .line 736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->removeMessages(I)V

    .line 737
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_11

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 739
    const/16 v24, -0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 741
    :cond_11
    const/4 v10, 0x0

    :goto_6
    const/16 v24, 0xf

    move/from16 v0, v24

    if-ge v10, v0, :cond_5

    .line 742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_12

    .line 743
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    goto/16 :goto_2

    .line 741
    :cond_12
    add-int/lit8 v10, v10, 0x1

    goto :goto_6
.end method

.method private FocusFirstBlankItem()V
    .locals 4

    .prologue
    .line 1336
    const-string v1, "ColorChipTest"

    const-string v2, "FocusFirstBlankItem()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    .line 1338
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 1339
    const-string v1, "ColorChipTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FocusFirstBlankItem() :: first blank item index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020068

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1342
    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .line 1346
    :cond_0
    return-void

    .line 1337
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private GetSampleViewIdx(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 921
    const-string v2, "ColorChipTest"

    const-string v3, "GetSampleViewIdx()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    const/4 v1, -0x1

    .line 925
    .local v1, "idx":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xf

    if-ge v0, v2, :cond_0

    .line 926
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 927
    move v1, v0

    .line 932
    :cond_0
    return v1

    .line 925
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private GetSelectViewIdx(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 942
    const-string v2, "ColorChipTest"

    const-string v3, "GetSelectViewIdx()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    const/4 v1, -0x1

    .line 946
    .local v1, "idx":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xf

    if-ge v0, v2, :cond_0

    .line 947
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 948
    move v1, v0

    .line 952
    :cond_0
    return v1

    .line 946
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private InitActionBar()V
    .locals 3

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 292
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 293
    const v1, 0x7f030006

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 295
    const v1, 0x7f0a0030

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    .line 296
    const v1, 0x7f0a002f

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->cancelItem:Landroid/view/View;

    .line 298
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 299
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 300
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->cancelItem:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    return-void
.end method

.method private InitColorChipAll()V
    .locals 7

    .prologue
    const/4 v1, -0x2

    .line 251
    const-string v0, "ColorChipTest"

    const-string v2, "InitColorChipAll()"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    const/16 v0, 0xf

    if-ge v6, v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 257
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    .line 259
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitColorChipSampleList()V

    .line 260
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitColorChipSelectList()V

    .line 262
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWm:Landroid/view/WindowManager;

    .line 264
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d3

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 270
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 271
    return-void
.end method

.method private InitColorChipSampleList()V
    .locals 4

    .prologue
    .line 216
    const-string v2, "ColorChipTest"

    const-string v3, "InitColorChipSampleList()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    .line 219
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xf

    if-ge v0, v2, :cond_1

    .line 220
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 221
    .local v1, "imageView":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 222
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 224
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_1
    return-void
.end method

.method private InitColorChipSelectList()V
    .locals 4

    .prologue
    .line 234
    const-string v2, "ColorChipTest"

    const-string v3, "InitColorChipSelectList()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f040000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    .line 237
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xf

    if-ge v0, v2, :cond_1

    .line 238
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 239
    .local v1, "imageView":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 240
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 241
    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_1
    return-void
.end method

.method private InitControls()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278
    const-string v1, "ColorChipTest"

    const-string v2, "InitControls()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const v1, 0x7f0a000e

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mlistlayout:Landroid/widget/RelativeLayout;

    .line 280
    const v1, 0x7f0a000c

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    .line 281
    const v1, 0x7f0a001e

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;

    .line 283
    const-string v1, "com.sec.feature.cocktailbar"

    invoke-static {p0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 284
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 285
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 286
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private InitDrawableLists()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02001b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02001f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020023

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020027

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02002b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02002f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020033

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020037

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02003b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02003f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020043

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020047

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02004b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f02004f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    const v1, 0x7f020053

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f02000f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020010

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020011

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020012

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020013

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020014

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020015

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020016

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020017

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    const v1, 0x7f020018

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020058

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020059

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f02005f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020060

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020061

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020062

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020063

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020064

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020065

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    const v1, 0x7f020066

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method

.method private InputSelectFromSample(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v7, 0xf

    const/4 v6, 0x0

    .line 377
    const-string v3, "ColorChipTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InputSelectFromSample() :: viewId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    const/4 v2, 0x0

    .line 380
    .local v2, "sampleidx":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v7, :cond_0

    .line 381
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    invoke-virtual {v4, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 382
    const-string v3, "ColorChipTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InputSelectFromSample() :: setBackgroundResource() blank index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 387
    const v3, 0x7f020055

    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 388
    invoke-virtual {p1, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 389
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    add-int/lit8 v4, v2, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 393
    :cond_0
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    .line 410
    :goto_1
    return-void

    .line 380
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 397
    :cond_2
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_2
    if-ge v1, v7, :cond_3

    .line 398
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 399
    const-string v3, "ColorChipTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InputSelectFromSample() :: setBackgroundResource() select image index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 404
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 405
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v4, 0x7f07000f

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 409
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ToggleDoneKey()V

    goto :goto_1

    .line 397
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private IsValidView(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 1190
    const-string v1, "ColorChipTest"

    const-string v3, "IsValidView()"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    .line 1242
    :goto_0
    return v1

    .line 1209
    :pswitch_1
    const-string v1, "ColorChipTest"

    const-string v3, "IsValidView() :: R.id.color_01~15"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSampleViewIdx(Landroid/view/View;)I

    move-result v0

    .line 1212
    .local v0, "idx":I
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v3, 0x64

    if-lt v1, v3, :cond_0

    move v1, v2

    .line 1213
    goto :goto_0

    .line 1232
    .end local v0    # "idx":I
    :pswitch_2
    const-string v1, "ColorChipTest"

    const-string v3, "IsValidView() :: R.id.colorChipimg01~15"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1234
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSelectViewIdx(Landroid/view/View;)I

    move-result v0

    .line 1235
    .restart local v0    # "idx":I
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    move v1, v2

    .line 1236
    goto :goto_0

    .line 1242
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 1193
    :pswitch_data_0
    .packed-switch 0x7f0a000f
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private LeftCopy(Ljava/util/ArrayList;I)V
    .locals 3
    .param p2, "blankIdx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p1, "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "ColorChipTest"

    const-string v2, "LeftCopy()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .local v0, "i":I
    :goto_0
    if-le v0, p2, :cond_0

    .line 442
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 444
    :cond_0
    return-void
.end method

.method private RemoveWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 361
    const-string v0, "ColorChipTest"

    const-string v1, "RemoveWidget()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    if-eqz v0, :cond_0

    .line 363
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    .line 364
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    .line 365
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    .line 369
    :cond_0
    return-void
.end method

.method private RightCopy(Ljava/util/ArrayList;I)V
    .locals 3
    .param p2, "blankIdx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 454
    .local p1, "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "ColorChipTest"

    const-string v2, "RightCopy()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 456
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 458
    :cond_0
    return-void
.end method

.method private SetEnableImageColor(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 418
    const-string v3, "ColorChipTest"

    const-string v4, "SetEnableImageColor()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, p1

    .line 419
    check-cast v1, Landroid/widget/ImageView;

    .line 421
    .local v1, "iv":Landroid/widget/ImageView;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 422
    .local v2, "viewId":I
    const/4 v0, 0x0

    .line 424
    .local v0, "idx":I
    :goto_0
    const/16 v3, 0xf

    if-ge v0, v3, :cond_0

    .line 425
    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 426
    const-string v3, "ColorChipTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SetEnableImageColor() :: setBackgroundResource() idx = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 431
    :cond_0
    return-void

    .line 424
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private ShiftLeft(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 483
    .local p1, "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "ColorChipTest"

    const-string v2, "ShiftLeft()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 485
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 486
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 488
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 484
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    :cond_0
    return-void
.end method

.method private ShiftRight(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, "backup":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "ColorChipTest"

    const-string v2, "ShiftRight()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 502
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 503
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 505
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDragSelectedItemId:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 501
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 507
    :cond_0
    return-void
.end method

.method private ShowWidget()V
    .locals 3

    .prologue
    .line 324
    const-string v0, "ColorChipTest"

    const-string v1, "ShowWidget()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 327
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 329
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShowWidget : mWidgetLayoutParams.x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShowWidget : mWidgetLayoutParams.y "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShowWidget : mWidgetDragColorId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 334
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    .line 336
    return-void
.end method

.method private ToggleDoneKey()V
    .locals 2

    .prologue
    .line 309
    const-string v0, "ColorChipTest"

    const-string v1, "ToggleDoneKey()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 312
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 315
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mDoneItemActionBar:Landroid/view/View;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private UpdateWidget()V
    .locals 3

    .prologue
    .line 343
    const-string v0, "ColorChipTest"

    const-string v1, "UpdateWidget()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 348
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateWidget : mWidgetLayoutParams.x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateWidget : mWidgetLayoutParams.y "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidget:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 353
    const-string v0, "ColorChipTest"

    const-string v1, "UpdateWidget complete"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSampleViewIdx(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/colorblind/ColorChipTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/colorblind/ColorChipTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->FocusFirstBlankItem()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectedColorIndex:I

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->GetSelectViewIdx(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/app/colorblind/ColorChipTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/colorblind/ColorChipTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mlistlayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/colorblind/ColorChipTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->FindNearItem()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/colorblind/ColorChipTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsRunningAnimation:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/colorblind/ColorChipTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsRunningAnimation:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/colorblind/ColorChipTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    return p1
.end method

.method static synthetic access$602(Lcom/samsung/android/app/colorblind/ColorChipTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    return p1
.end method

.method static synthetic access$702(Lcom/samsung/android/app/colorblind/ColorChipTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetDragColorId:I

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/app/colorblind/ColorChipTest;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/app/colorblind/ColorChipTest;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/app/colorblind/ColorChipTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/colorblind/ColorChipTest;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ShowWidget()V

    return-void
.end method


# virtual methods
.method public Random()V
    .locals 11

    .prologue
    const/16 v10, 0xf

    .line 188
    const-string v5, "ColorChipTest"

    const-string v6, "Random()"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    new-array v1, v10, [I

    fill-array-data v1, :array_0

    .line 193
    .local v1, "ar":[I
    new-instance v4, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x64

    add-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Random;-><init>(J)V

    .line 194
    .local v4, "rnd":Ljava/util/Random;
    array-length v5, v1

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 195
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 197
    .local v3, "index":I
    aget v0, v1, v3

    .line 198
    .local v0, "a":I
    aget v5, v1, v2

    aput v5, v1, v3

    .line 199
    aput v0, v1, v2

    .line 194
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 202
    .end local v0    # "a":I
    .end local v3    # "index":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_1

    .line 203
    iget-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    aget v6, v1, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 202
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 209
    :cond_1
    return-void

    .line 189
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
    .end array-data
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v8, 0x20000000

    const/4 v7, -0x1

    .line 788
    const-string v5, "ColorChipTest"

    const-string v6, "onClick()"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 861
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 805
    :pswitch_1
    const-string v5, "ColorChipTest"

    const-string v6, "onClick() :: R.id.color_01~15"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InputSelectFromSample(Landroid/view/View;)V

    goto :goto_0

    .line 809
    :pswitch_2
    const-string v5, "ColorChipTest"

    const-string v6, "onOptionsItemSelected() :: action_bar_cancel"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    invoke-static {}, Lcom/samsung/android/app/colorblind/ColorChipStart;->FinishAll()V

    goto :goto_0

    .line 813
    :pswitch_3
    const-string v5, "ColorChipTest"

    const-string v6, "onOptionsItemSelected() :: action_bar_done"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    iget-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 816
    new-instance v0, Ldmc/cvd/cvdcalculator/CVDCalculator;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;-><init>()V

    .line 817
    .local v0, "cvdCalculator":Ldmc/cvd/cvdcalculator/CVDCalculator;
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    .line 819
    .local v2, "settingValue":Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_1
    sget-object v5, Lcom/samsung/android/app/colorblind/ColorChipTest;->input:[I

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 820
    iget-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v5, v7, :cond_1

    .line 821
    sget-object v6, Lcom/samsung/android/app/colorblind/ColorChipTest;->input:[I

    iget-object v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    aput v5, v6, v1

    .line 819
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 823
    :cond_1
    sget-object v5, Lcom/samsung/android/app/colorblind/ColorChipTest;->input:[I

    const/4 v6, 0x0

    aput v6, v5, v1

    goto :goto_2

    .line 827
    :cond_2
    sget-object v5, Lcom/samsung/android/app/colorblind/ColorChipTest;->input:[I

    invoke-virtual {v0, v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->setNum([I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 828
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5

    .line 830
    :cond_3
    invoke-virtual {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;->calculate()V

    .line 832
    invoke-virtual {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getCVDType()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDType:I

    .line 833
    invoke-virtual {v0}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getCVDSeverity()D

    move-result-wide v6

    double-to-float v5, v6

    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDSeverity:F

    .line 834
    const/high16 v5, 0x3f000000    # 0.5f

    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUserParameter:F

    .line 836
    iget v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDType:I

    iget v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDSeverity:F

    iget v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUserParameter:F

    invoke-virtual {v2, v5, v6, v7}, Lcom/samsung/android/app/colorblind/ColorChipSettingValue;->SavePrefCVDSettingValue(IFF)V

    .line 839
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "color_blind_test"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 842
    iget v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDType:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x3

    if-eq v5, v6, :cond_4

    iget v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mCVDType:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 844
    :cond_4
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 845
    .local v3, "toReport":Landroid/content/Intent;
    invoke-virtual {v3, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 846
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/colorblind/ColorChipTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 848
    .end local v3    # "toReport":Landroid/content/Intent;
    :cond_5
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/samsung/android/app/colorblind/ColorChipTestStatus;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 849
    .local v4, "toSelection":Landroid/content/Intent;
    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 850
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/colorblind/ColorChipTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 853
    .end local v4    # "toSelection":Landroid/content/Intent;
    :cond_6
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5

    .line 789
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a000f
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 752
    const-string v0, "ColorChipTest"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 755
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->setContentView(I)V

    .line 757
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitActionBar()V

    .line 758
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitControls()V

    .line 759
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitColorChipAll()V

    .line 760
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->InitDrawableLists()V

    .line 761
    invoke-virtual {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->Random()V

    .line 762
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 865
    const-string v0, "ColorChipTest"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 869
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSampleDragList:Ljava/util/ArrayList;

    .line 872
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 873
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 874
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSelectSmallList:Ljava/util/ArrayList;

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 878
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 879
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mSampleLongList:Ljava/util/ArrayList;

    .line 882
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 883
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 884
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleList:Ljava/util/ArrayList;

    .line 887
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 888
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 889
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSampleIndex:Ljava/util/ArrayList;

    .line 892
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 893
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 894
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectList:Ljava/util/ArrayList;

    .line 897
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 898
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 899
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mColorChipSelectIndex:Ljava/util/ArrayList;

    .line 902
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    if-eqz v0, :cond_7

    .line 903
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 904
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mUpperImageViewArray:Landroid/content/res/TypedArray;

    .line 907
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    if-eqz v0, :cond_8

    .line 908
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 909
    iput-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowerImageViewArray:Landroid/content/res/TypedArray;

    .line 911
    :cond_8
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 912
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 772
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause() : mIsWidget "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    iget-boolean v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    if-eqz v0, :cond_0

    .line 774
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->RemoveWidget()V

    .line 776
    const-string v0, "ColorChipTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause() : mIsWidget "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPressedViewId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mWidgetSelectedView.getId() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    .line 780
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetSelectedView:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->AssignDragItem(Landroid/view/View;)V

    .line 783
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 784
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 766
    const-string v0, "ColorChipTest"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 768
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v4, 0x1

    .line 1247
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 1248
    .local v3, "touchViewId":I
    const-string v6, "ColorChipTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onTouch() :: viewID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    iget v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    if-eqz v6, :cond_1

    iget v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    if-eq v3, v6, :cond_1

    .line 1250
    const-string v5, "ColorChipTest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onClick() :: touchViewId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mPressedViewId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    :cond_0
    :goto_0
    return v4

    .line 1255
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_2
    :goto_1
    move v4, v5

    .line 1328
    goto :goto_0

    .line 1258
    :pswitch_0
    const-string v6, "ColorChipTest"

    const-string v7, "onTouch() :: ACTION_DOWN"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1260
    iput v3, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    .line 1262
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->IsValidView(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1266
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1267
    .local v0, "msg":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->what:I

    .line 1268
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1270
    iget-object v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1271
    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1273
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionX:F

    .line 1274
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionY:F

    goto :goto_1

    .line 1279
    .end local v0    # "msg":Landroid/os/Message;
    :pswitch_1
    const-string v6, "ColorChipTest"

    const-string v7, "onTouch() :: ACTION_MOVE"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    const/4 v1, 0x0

    .line 1281
    .local v1, "newX":F
    const/4 v2, 0x0

    .line 1283
    .local v2, "newY":F
    iget-boolean v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    if-eqz v6, :cond_2

    .line 1284
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 1285
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 1287
    iget v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    iget v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionX:F

    sub-float v6, v1, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetX:F

    .line 1288
    iget v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    iget v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionY:F

    sub-float v6, v2, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mWidgetY:F

    .line 1290
    iput v1, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionX:F

    .line 1291
    iput v2, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionY:F

    .line 1293
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->UpdateWidget()V

    .line 1294
    iget-boolean v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    if-eqz v5, :cond_0

    .line 1295
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->FindNearItem()V

    goto :goto_0

    .line 1303
    .end local v1    # "newX":F
    .end local v2    # "newY":F
    :pswitch_2
    const-string v6, "ColorChipTest"

    const-string v7, "onTouch() :: ACTION_UP"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1305
    iget-object v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1306
    iget-object v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 1307
    const/4 v6, -0x1

    iput v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mFocusedColorIndex:I

    .line 1309
    iput v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionX:F

    .line 1310
    iput v9, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPrePositionY:F

    .line 1312
    iput v5, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mPressedViewId:I

    .line 1314
    iget-boolean v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mLowTouch:Z

    if-nez v6, :cond_3

    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->IsValidView(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1318
    :cond_3
    iget-boolean v6, p0, Lcom/samsung/android/app/colorblind/ColorChipTest;->mIsWidget:Z

    if-eqz v6, :cond_2

    .line 1319
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->RemoveWidget()V

    .line 1320
    invoke-direct {p0, p1}, Lcom/samsung/android/app/colorblind/ColorChipTest;->AssignDragItem(Landroid/view/View;)V

    .line 1321
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ClearFocusImage()V

    .line 1322
    invoke-direct {p0}, Lcom/samsung/android/app/colorblind/ColorChipTest;->ToggleDoneKey()V

    goto/16 :goto_0

    .line 1255
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
