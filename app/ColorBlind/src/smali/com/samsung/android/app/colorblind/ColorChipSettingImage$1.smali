.class Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;
.super Ljava/lang/Object;
.source "ColorChipSettingImage.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipSettingImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 83
    const-string v0, "ColorChipSettingImage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mColourAdjustmentSeekBarListener :: onProgressChanged() is called progress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fromUser "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    if-eqz p3, :cond_0

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    int-to-float v1, p2

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;F)F

    .line 91
    :cond_0
    const-string v0, "ColorChipSettingImage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mColourAdjustmentSeekBarListener :: onProgressChanged() is called : mUserParameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mSettingValue:Lcom/samsung/android/app/colorblind/ColorChipSettingValue;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$200(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)Lcom/samsung/android/app/colorblind/ColorChipSettingValue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    .line 98
    .local v7, "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)[I

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v6

    .line 101
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->cvdCalculator:Ldmc/cvd/cvdcalculator/CVDCalculator;
    invoke-static {v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$500(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)Ldmc/cvd/cvdcalculator/CVDCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDType:I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$100(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mCVDseverity:F
    invoke-static {v2}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$400(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mUserParameter:F
    invoke-static {v4}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)F

    move-result v4

    float-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v0

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I
    invoke-static {v8, v0}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$302(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;[I)[I

    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    .line 107
    .restart local v7    # "manager":Landroid/view/accessibility/AccessibilityManager;
    const/4 v0, 0x1

    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingImage$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingImage;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->mIntegerArray:[I
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingImage;->access$300(Lcom/samsung/android/app/colorblind/ColorChipSettingImage;)[I

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/view/accessibility/AccessibilityManager;->setmDNIeColorBlind(Z[I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 108
    :catch_1
    move-exception v6

    .line 110
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 117
    const-string v0, "ColorChipSettingImage"

    const-string v1, "mColourAdjustmentSeekBarListener :: onStartTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 122
    const-string v0, "ColorChipSettingImage"

    const-string v1, "mColourAdjustmentSeekBarListener :: onStopTrackingTouch() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void
.end method
