.class Lcom/samsung/android/app/colorblind/ColorChipReport$2;
.super Landroid/database/ContentObserver;
.source "ColorChipReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/colorblind/ColorChipReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipReport;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v0, 0x0

    .line 215
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    invoke-virtual {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "high_contrast"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 217
    .local v0, "negativeColorEnabled":Z
    :cond_0
    if-eqz v0, :cond_2

    .line 218
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$000(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$000(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mRetestDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$000(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipReport$2;->this$0:Lcom/samsung/android/app/colorblind/ColorChipReport;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipReport;->mOptionsMenu:Landroid/view/Menu;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipReport;->access$100(Lcom/samsung/android/app/colorblind/ColorChipReport;)Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->close()V

    .line 226
    :cond_2
    return-void
.end method
