.class Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;
.super Ljava/lang/Object;
.source "ColorChipSettingPreview.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 124
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v2, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    monitor-enter v2

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v1, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v1, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 128
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v1, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 129
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v1, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    # getter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$000(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 130
    iget-object v1, p0, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1$1;->this$1:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;

    iget-object v1, v1, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview$1;->this$0:Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1, v3}, Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;->access$002(Lcom/samsung/android/app/colorblind/ColorChipSettingPreview;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 136
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 135
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
