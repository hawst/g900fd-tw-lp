.class public Ldmc/cvd/cvdcalculator/CVDCalculator;
.super Ljava/lang/Object;
.source "CVDCalculator.java"


# static fields
.field public static final DEUTAN:I = 0x1

.field public static final NOT_COLORBLIND:I = 0x3

.field public static final PROTAN:I = 0x0

.field private static final RGB_CMY_MAX:I = 0x9

.field public static final TRITAN:I = 0x2


# instance fields
.field private CVDSeverity:D

.field private CVDStrength:D

.field private CVDType:I

.field private SpotsU:[D

.field private SpotsV:[D

.field private c_index:D

.field private mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

.field private mInputNums:[I

.field private majorAngle:D

.field private majorRadius:D

.field private minorRadius:D

.field private s_index:D

.field private tes:D

.field private u:[D

.field private v:[D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-array v0, v1, [D

    fill-array-data v0, :array_0

    iput-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->u:[D

    .line 50
    new-array v0, v1, [D

    fill-array-data v0, :array_1

    iput-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->v:[D

    .line 53
    new-array v0, v1, [D

    iput-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsU:[D

    .line 54
    new-array v0, v1, [D

    iput-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsV:[D

    .line 69
    iput-object v2, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mInputNums:[I

    .line 71
    iput-object v2, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    .line 79
    new-instance v0, Ldmc/cvd/cvdcalculator/ColorTransferTable;

    invoke-direct {v0}, Ldmc/cvd/cvdcalculator/ColorTransferTable;-><init>()V

    iput-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    .line 80
    return-void

    .line 49
    :array_0
    .array-data 8
        -0x3fca75c28f5c28f6L    # -21.54
        -0x3fc8bd70a3d70a3dL    # -23.26
        -0x3fc9970a3d70a3d7L    # -22.41
        -0x3fc8e3d70a3d70a4L    # -23.11
        -0x3fc98ccccccccccdL    # -22.45
        -0x3fca3d70a3d70a3dL    # -21.76
        -0x3fd3d70a3d70a3d7L    # -14.08
        -0x3ffa3d70a3d70a3dL    # -2.72
        0x402dae147ae147aeL    # 14.84
        0x4037deb851eb851fL    # 23.87
        0x403fd1eb851eb852L    # 31.82
        0x403f6b851eb851ecL    # 31.42
        0x403dca3d70a3d70aL    # 29.79
        0x403aa3d70a3d70a4L    # 26.64
        0x4036eb851eb851ecL    # 22.92
        0x4026666666666666L    # 11.2
    .end array-data

    .line 50
    :array_1
    .array-data 8
        -0x3fbcce147ae147aeL    # -38.39
        -0x3fc670a3d70a3d71L    # -25.56
        -0x3fd0f0a3d70a3d71L    # -15.53
        -0x3fe2333333333333L    # -7.45
        0x3ff199999999999aL    # 1.1
        0x401d666666666666L    # 7.35
        0x4032bd70a3d70a3dL    # 18.74
        0x403c2147ae147ae1L    # 28.13
        0x403f2147ae147ae1L    # 31.13
        0x403a59999999999aL    # 26.35
        0x402d851eb851eb85L    # 14.76
        0x401bf5c28f5c28f6L    # 6.99
        0x3fb999999999999aL    # 0.1
        -0x3fdd3d70a3d70a3dL    # -9.38
        -0x3fcd59999999999aL    # -18.65
        -0x3fc763d70a3d70a4L    # -24.61
    .end array-data
.end method

.method private Calc()V
    .locals 32

    .prologue
    .line 177
    const-wide/16 v8, 0x0

    .line 179
    .local v8, "cvdStrength":D
    const/16 v22, 0xf

    move/from16 v0, v22

    new-array v0, v0, [D

    move-object/from16 v20, v0

    .line 180
    .local v20, "temp2u":[D
    const/16 v22, 0xf

    move/from16 v0, v22

    new-array v0, v0, [D

    move-object/from16 v21, v0

    .line 182
    .local v21, "temp2v":[D
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    const/16 v22, 0xf

    move/from16 v0, v22

    if-lt v10, v0, :cond_2

    .line 187
    const-wide/16 v16, 0x0

    .line 188
    .local v16, "sum1":D
    const-wide/16 v18, 0x0

    .line 189
    .local v18, "sum2":D
    const/4 v10, 0x0

    :goto_1
    const/16 v22, 0xf

    move/from16 v0, v22

    if-lt v10, v0, :cond_3

    .line 193
    div-double v22, v16, v18

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->atan(D)D

    move-result-wide v22

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v14, v22, v24

    .line 195
    .local v14, "piAngle":D
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v11, v0, [D

    .line 196
    .local v11, "moms":[D
    const/16 v22, 0x0

    const-wide/16 v24, 0x0

    aput-wide v24, v11, v22

    .line 197
    const/16 v22, 0x1

    const-wide/16 v24, 0x0

    aput-wide v24, v11, v22

    .line 200
    const-wide/16 v22, 0x0

    cmpg-double v22, v14, v22

    if-gez v22, :cond_4

    const-wide v22, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v12, v14, v22

    .line 203
    .local v12, "newAngle":D
    :goto_2
    const/4 v10, 0x0

    :goto_3
    const/16 v22, 0xf

    move/from16 v0, v22

    if-lt v10, v0, :cond_5

    .line 208
    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-wide v24, v11, v23

    const-wide/high16 v26, 0x402e000000000000L    # 15.0

    div-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    aput-wide v24, v11, v22

    .line 209
    const/16 v22, 0x1

    const/16 v23, 0x1

    aget-wide v24, v11, v23

    const-wide/high16 v26, 0x402e000000000000L    # 15.0

    div-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    aput-wide v24, v11, v22

    .line 210
    const/16 v22, 0x0

    aget-wide v22, v11, v22

    const/16 v24, 0x1

    aget-wide v24, v11, v24

    cmpl-double v22, v22, v24

    if-lez v22, :cond_6

    .line 211
    const/16 v22, 0x0

    aget-wide v22, v11, v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    .line 212
    const/16 v22, 0x1

    aget-wide v22, v11, v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->minorRadius:D

    .line 213
    const-wide v22, 0x4066800000000000L    # 180.0

    mul-double v22, v22, v12

    const-wide v24, 0x400921fb54442d18L    # Math.PI

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorAngle:D

    .line 220
    :goto_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    move-wide/from16 v22, v0

    const-wide v24, 0x4022795e9e1b089aL    # 9.23705

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->c_index:D

    .line 221
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->minorRadius:D

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->s_index:D

    .line 222
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->minorRadius:D

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->tes:D

    .line 224
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->c_index:D

    move-wide/from16 v22, v0

    const-wide v24, 0x3ff999999999999aL    # 1.6

    cmpg-double v22, v22, v24

    if-gtz v22, :cond_7

    .line 225
    const/16 v22, 0x3

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    .line 234
    :goto_5
    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->c_index:D

    .line 235
    .local v6, "adjusted_c":D
    const-wide v22, 0x3ff999999999999aL    # 1.6

    cmpg-double v22, v6, v22

    if-gez v22, :cond_0

    const-wide v6, 0x3ff999999999999aL    # 1.6

    .line 236
    :cond_0
    const-wide v22, 0x4010cccccccccccdL    # 4.2

    cmpl-double v22, v6, v22

    if-lez v22, :cond_1

    const-wide v6, 0x4010cccccccccccdL    # 4.2

    .line 237
    :cond_1
    const-wide v22, 0x3ff999999999999aL    # 1.6

    sub-double v22, v6, v22

    const-wide v24, 0x4004cccccccccccdL    # 2.6

    div-double v8, v22, v24

    .line 238
    move-object/from16 v0, p0

    iput-wide v8, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDStrength:D

    .line 242
    const-wide v22, 0x3fb999999999999aL    # 0.1

    cmpg-double v22, v8, v22

    if-gez v22, :cond_a

    .line 243
    const-wide/high16 v22, 0x4014000000000000L    # 5.0

    mul-double v4, v22, v8

    .line 248
    .local v4, "SimSeverity":D
    :goto_6
    move-object/from16 v0, p0

    iput-wide v4, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDSeverity:D

    .line 249
    return-void

    .line 183
    .end local v4    # "SimSeverity":D
    .end local v6    # "adjusted_c":D
    .end local v11    # "moms":[D
    .end local v12    # "newAngle":D
    .end local v14    # "piAngle":D
    .end local v16    # "sum1":D
    .end local v18    # "sum2":D
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsU:[D

    move-object/from16 v22, v0

    add-int/lit8 v23, v10, 0x1

    aget-wide v22, v22, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsU:[D

    move-object/from16 v24, v0

    aget-wide v24, v24, v10

    sub-double v22, v22, v24

    aput-wide v22, v20, v10

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsV:[D

    move-object/from16 v22, v0

    add-int/lit8 v23, v10, 0x1

    aget-wide v22, v22, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsV:[D

    move-object/from16 v24, v0

    aget-wide v24, v24, v10

    sub-double v22, v22, v24

    aput-wide v22, v21, v10

    .line 182
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 190
    .restart local v16    # "sum1":D
    .restart local v18    # "sum2":D
    :cond_3
    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    aget-wide v24, v20, v10

    mul-double v22, v22, v24

    aget-wide v24, v21, v10

    mul-double v22, v22, v24

    add-double v16, v16, v22

    .line 191
    aget-wide v22, v20, v10

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v22

    aget-wide v24, v21, v10

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    sub-double v22, v22, v24

    add-double v18, v18, v22

    .line 189
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 201
    .restart local v11    # "moms":[D
    .restart local v14    # "piAngle":D
    :cond_4
    const-wide v22, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v12, v14, v22

    .restart local v12    # "newAngle":D
    goto/16 :goto_2

    .line 204
    :cond_5
    const/16 v22, 0x0

    aget-wide v24, v11, v22

    aget-wide v26, v21, v10

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v28

    mul-double v26, v26, v28

    aget-wide v28, v20, v10

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    sub-double v26, v26, v28

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    aput-wide v24, v11, v22

    .line 205
    const/16 v22, 0x1

    aget-wide v24, v11, v22

    aget-wide v26, v21, v10

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v28

    mul-double v26, v26, v28

    aget-wide v28, v20, v10

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    sub-double v26, v26, v28

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v26

    add-double v24, v24, v26

    aput-wide v24, v11, v22

    .line 203
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 215
    :cond_6
    const/16 v22, 0x1

    aget-wide v22, v11, v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    .line 216
    const/16 v22, 0x0

    aget-wide v22, v11, v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->minorRadius:D

    .line 217
    const-wide v22, 0x4066800000000000L    # 180.0

    mul-double v22, v22, v14

    const-wide v24, 0x400921fb54442d18L    # Math.PI

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorAngle:D

    goto/16 :goto_4

    .line 226
    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorAngle:D

    move-wide/from16 v22, v0

    const-wide v24, 0x3fe6666666666666L    # 0.7

    cmpl-double v22, v22, v24

    if-ltz v22, :cond_8

    .line 227
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    goto/16 :goto_5

    .line 228
    :cond_8
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorAngle:D

    move-wide/from16 v22, v0

    const-wide v24, -0x3fafc00000000000L    # -65.0

    cmpg-double v22, v22, v24

    if-gez v22, :cond_9

    .line 229
    const/16 v22, 0x2

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    goto/16 :goto_5

    .line 231
    :cond_9
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    goto/16 :goto_5

    .line 245
    .restart local v6    # "adjusted_c":D
    :cond_a
    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    const-wide v24, 0x3fb999999999999aL    # 0.1

    sub-double v24, v8, v24

    const-wide/high16 v26, 0x4014000000000000L    # 5.0

    mul-double v24, v24, v26

    const-wide/high16 v26, 0x4022000000000000L    # 9.0

    div-double v24, v24, v26

    add-double v4, v22, v24

    .restart local v4    # "SimSeverity":D
    goto/16 :goto_6
.end method

.method private InitMakeUV()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 146
    const/4 v3, 0x0

    .line 148
    .local v3, "result":Z
    const/4 v2, 0x0

    .line 150
    .local v2, "num":I
    const/4 v3, 0x1

    .line 153
    iget-object v4, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsU:[D

    iget-object v5, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->u:[D

    aget-wide v6, v5, v8

    aput-wide v6, v4, v8

    .line 154
    iget-object v4, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsV:[D

    iget-object v5, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->v:[D

    aget-wide v6, v5, v8

    aput-wide v6, v4, v8

    .line 157
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0xf

    if-lt v1, v4, :cond_0

    .line 167
    :goto_1
    return v3

    .line 158
    :cond_0
    :try_start_0
    iget-object v4, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mInputNums:[I

    aget v2, v4, v1

    .line 159
    iget-object v4, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsU:[D

    add-int/lit8 v5, v1, 0x1

    iget-object v6, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->u:[D

    aget-wide v6, v6, v2

    aput-wide v6, v4, v5

    .line 160
    iget-object v4, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->SpotsV:[D

    add-int/lit8 v5, v1, 0x1

    iget-object v6, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->v:[D

    aget-wide v6, v6, v2

    aput-wide v6, v4, v5
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private roundHalfUp(D)D
    .locals 7
    .param p1, "value"    # D

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 428
    const-wide/16 v0, 0x0

    .line 430
    .local v0, "roundedValue":D
    mul-double/2addr p1, v4

    .line 431
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-double v0, v2

    .line 432
    div-double/2addr v0, v4

    .line 434
    return-wide v0
.end method


# virtual methods
.method public calculate()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ldmc/cvd/cvdcalculator/CVDCalculator;->Calc()V

    .line 108
    return-void
.end method

.method public getCVDSeverity()D
    .locals 4

    .prologue
    .line 131
    const-wide/16 v0, 0x0

    .line 134
    .local v0, "roundedCVDSeverity":D
    iget-wide v2, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDSeverity:D

    invoke-direct {p0, v2, v3}, Ldmc/cvd/cvdcalculator/CVDCalculator;->roundHalfUp(D)D

    move-result-wide v0

    .line 136
    return-wide v0
.end method

.method public getCVDStrength()D
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDStrength:D

    return-wide v0
.end method

.method public getCVDType()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    return v0
.end method

.method public getC_index()D
    .locals 2

    .prologue
    .line 474
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->c_index:D

    return-wide v0
.end method

.method public getMajorAngle()D
    .locals 2

    .prologue
    .line 464
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorAngle:D

    return-wide v0
.end method

.method public getMajorRadius()D
    .locals 2

    .prologue
    .line 444
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->majorRadius:D

    return-wide v0
.end method

.method public getMinorRadius()D
    .locals 2

    .prologue
    .line 454
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->minorRadius:D

    return-wide v0
.end method

.method public getRGBCMY(D)[I
    .locals 7
    .param p1, "userParameter"    # D

    .prologue
    .line 259
    const/4 v6, 0x0

    check-cast v6, [I

    .line 262
    .local v6, "rgbCmy":[I
    iget v1, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDType:I

    iget-wide v2, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->CVDSeverity:D

    move-object v0, p0

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Ldmc/cvd/cvdcalculator/CVDCalculator;->getRGBCMY(IDD)[I

    move-result-object v6

    .line 264
    return-object v6
.end method

.method public getRGBCMY(IDD)[I
    .locals 12
    .param p1, "cvdType"    # I
    .param p2, "severity"    # D
    .param p4, "userParameter"    # D

    .prologue
    .line 279
    const/4 v8, 0x0

    .line 280
    .local v8, "firstInteger":I
    const/4 v10, 0x0

    .line 283
    .local v10, "secondInteger":I
    const/4 v9, 0x0

    check-cast v9, [I

    .line 284
    .local v9, "rgbCmy":[I
    const/16 v0, 0x9

    new-array v9, v0, [I

    .line 287
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 290
    :cond_0
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x1

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 291
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x4

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 293
    const/4 v0, 0x0

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 295
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x1

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 296
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x4

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 298
    const/4 v0, 0x1

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 300
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x1

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 301
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x4

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 303
    const/4 v0, 0x2

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 305
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x3

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 306
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x6

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 308
    const/4 v0, 0x3

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 310
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x3

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 311
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x6

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 313
    const/4 v0, 0x4

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 315
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x3

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 316
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x6

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 318
    const/4 v0, 0x5

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 320
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x5

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 321
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x2

    const/4 v2, 0x1

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 323
    const/4 v0, 0x6

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 325
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x5

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 326
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x2

    const/4 v2, 0x3

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 328
    const/4 v0, 0x7

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 330
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x5

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v8

    .line 331
    iget-object v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mColorTransferTable:Ldmc/cvd/cvdcalculator/ColorTransferTable;

    const/4 v1, 0x2

    const/4 v2, 0x5

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v0 .. v7}, Ldmc/cvd/cvdcalculator/ColorTransferTable;->getColorTransferValue(IIIDD)I

    move-result v10

    .line 333
    const/16 v0, 0x8

    mul-int/lit16 v1, v8, 0x100

    add-int/2addr v1, v10

    aput v1, v9, v0

    .line 350
    :goto_0
    return-object v9

    .line 338
    :cond_1
    const/4 v0, 0x0

    const v1, 0xff00

    aput v1, v9, v0

    .line 339
    const/4 v0, 0x1

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 340
    const/4 v0, 0x2

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 341
    const/4 v0, 0x3

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 342
    const/4 v0, 0x4

    const v1, 0xff00

    aput v1, v9, v0

    .line 343
    const/4 v0, 0x5

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 344
    const/4 v0, 0x6

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 345
    const/4 v0, 0x7

    const/16 v1, 0xff

    aput v1, v9, v0

    .line 346
    const/16 v0, 0x8

    const v1, 0xff00

    aput v1, v9, v0

    goto :goto_0
.end method

.method public getS_index()D
    .locals 2

    .prologue
    .line 484
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->s_index:D

    return-wide v0
.end method

.method public getTes()D
    .locals 2

    .prologue
    .line 494
    iget-wide v0, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->tes:D

    return-wide v0
.end method

.method public setNum([I)Z
    .locals 1
    .param p1, "nums"    # [I

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 92
    .local v0, "result":Z
    iput-object p1, p0, Ldmc/cvd/cvdcalculator/CVDCalculator;->mInputNums:[I

    .line 95
    invoke-direct {p0}, Ldmc/cvd/cvdcalculator/CVDCalculator;->InitMakeUV()Z

    move-result v0

    .line 98
    return v0
.end method
