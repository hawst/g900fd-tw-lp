.class public Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;
.super Lcom/google/android/common/LoggingThreadedSyncAdapter;
.source "ContactsSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$ContactsGDataFeedFetcher;,
        Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    }
.end annotation


# static fields
.field private static final PROJECTION_GROUPS:[Ljava/lang/String;

.field private static final PROJECTION_RAW_CONTACTS_ID:[Ljava/lang/String;

.field static final sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

.field static final sEntityEndMarker:Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

.field static final sEntryEndMarker:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            ">;"
        }
    .end annotation
.end field

.field static final sGroupHandler:Lcom/google/android/syncadapters/contacts/GroupHandler;


# instance fields
.field private mAccountManager:Landroid/accounts/AccountManager;

.field mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

.field private mPhotoDownloads:I

.field private mPhotoUploads:I

.field private final mSpecialGroupsLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntryEndMarker:Landroid/util/Pair;

    .line 78
    new-instance v0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

    invoke-direct {v0, v1, v2, v1}, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;-><init>(Lcom/google/wireless/gdata2/data/Entry;ILandroid/content/Entity;)V

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntityEndMarker:Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

    .line 90
    new-instance v0, Lcom/google/android/syncadapters/contacts/GroupHandler;

    invoke-direct {v0}, Lcom/google/android/syncadapters/contacts/GroupHandler;-><init>()V

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sGroupHandler:Lcom/google/android/syncadapters/contacts/GroupHandler;

    .line 91
    new-instance v0, Lcom/google/android/syncadapters/contacts/ContactHandler;

    invoke-direct {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;-><init>()V

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    .line 112
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->PROJECTION_RAW_CONTACTS_ID:[Ljava/lang/String;

    .line 115
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "system_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "auto_add"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "favorites"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "group_is_read_only"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->PROJECTION_GROUPS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/common/LoggingThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 146
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mSpecialGroupsLock:Ljava/lang/Object;

    .line 150
    new-instance v0, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    new-instance v1, Lcom/google/android/syncadapters/HttpsOnlyAndroidGDataClient;

    const-string v2, "Android-GData-Contacts/1.3"

    const-string v3, "5.0"

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/syncadapters/HttpsOnlyAndroidGDataClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlContactsGDataParserFactory;

    new-instance v3, Lcom/google/android/common/gdata2/AndroidXmlParserFactory;

    invoke-direct {v3}, Lcom/google/android/common/gdata2/AndroidXmlParserFactory;-><init>()V

    invoke-direct {v2, v3}, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlContactsGDataParserFactory;-><init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;)V

    invoke-direct {v0, v1, v2}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;-><init>(Lcom/google/wireless/gdata2/client/GDataClient;Lcom/google/wireless/gdata2/client/GDataParserFactory;)V

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    .line 153
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;

    .line 155
    return-void
.end method

.method static addDeleteOperation(Ljava/util/ArrayList;Landroid/net/Uri;JZ)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "JZ)V"
        }
    .end annotation

    .prologue
    .line 1345
    .local p0, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    return-void
.end method

.method static addInsertOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "contactId"    # Ljava/lang/Long;
    .param p4, "contactBackRef"    # Ljava/lang/Integer;
    .param p5, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1315
    .local p0, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1317
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    if-eqz p3, :cond_0

    .line 1318
    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1320
    :cond_0
    if-eqz p4, :cond_1

    .line 1321
    const-string v1, "raw_contact_id"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1324
    :cond_1
    invoke-virtual {v0, p5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    .line 1325
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1326
    return-void
.end method

.method static addUpdateOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "rowId"    # Ljava/lang/Long;
    .param p4, "rowIdBackRef"    # Ljava/lang/Integer;
    .param p5, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p0, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1330
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1332
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    if-eqz p3, :cond_0

    .line 1333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 1335
    :cond_0
    if-eqz p4, :cond_1

    .line 1336
    const-string v1, "_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 1337
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    .line 1339
    :cond_1
    invoke-virtual {v0, p5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    .line 1340
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1341
    return-void
.end method

.method private applyOperations(Ljava/util/ArrayList;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 5
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/content/ContentProviderClient;",
            "Landroid/accounts/Account;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 1290
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1291
    invoke-virtual {p2, p1}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    .line 1292
    .local v1, "result":[Landroid/content/ContentProviderResult;
    const-string v2, "ContactsSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1293
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "batch applied successfully, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " results"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1307
    .end local v1    # "result":[Landroid/content/ContentProviderResult;
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1309
    return-void

    .line 1296
    :catch_0
    move-exception v0

    .line 1297
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "ContactsSyncAdapter"

    const-string v3, "error applying batch, an unknown number of yield points succeeded"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1298
    new-instance v2, Ljava/io/IOException;

    const-string v3, "error while applying batch"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1307
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    throw v2

    .line 1299
    :catch_1
    move-exception v0

    .line 1300
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    const-string v2, "ContactsSyncAdapter"

    const-string v3, "error applying batch, an unknown number of yield points succeeded"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1301
    new-instance v2, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v3, "error while applying batch"

    invoke-direct {v2, v3}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1302
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1303
    .local v0, "e":Landroid/content/OperationApplicationException;
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error applying batch, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->getNumSuccessfulYieldPoints()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " yield points succeeded"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1305
    new-instance v2, Ljava/io/IOException;

    const-string v3, "error while applying batch"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private doServerOperation(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)V
    .locals 9
    .param p1, "operation"    # Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    .param p2, "entryClass"    # Ljava/lang/Class;
    .param p3, "client"    # Lcom/google/wireless/gdata2/client/GDataServiceClient;
    .param p4, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/client/AuthenticationException;,
            Lcom/google/wireless/gdata2/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/ConflictDetectedException;,
            Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 844
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v0

    .line 845
    .local v0, "activeTag":I
    const/4 v2, 0x0

    .line 847
    .local v2, "tag":I
    :try_start_0
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 878
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bad operation type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/google/wireless/gdata2/client/PreconditionFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/wireless/gdata2/client/BadRequestException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 880
    :catch_0
    move-exception v1

    .line 881
    .local v1, "e":Lcom/google/wireless/gdata2/client/PreconditionFailedException;
    :try_start_1
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-eq v3, v7, :cond_0

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v8, :cond_b

    .line 887
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->fetchEntry(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;

    .line 893
    const/4 v3, 0x2

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$102(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 931
    if-eqz v2, :cond_1

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_1
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 936
    .end local v1    # "e":Lcom/google/wireless/gdata2/client/PreconditionFailedException;
    :goto_0
    return-void

    .line 849
    :pswitch_0
    :try_start_2
    const-string v3, "ContactsSyncAdapter"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 850
    const-string v3, "ContactsSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inserting with entry =====\n "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_2
    or-int/lit8 v2, v0, 0x1

    .line 853
    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 854
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->url:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$200(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v4

    invoke-virtual {p3, v3, p4, v4}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->createEntry(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;
    :try_end_2
    .catch Lcom/google/wireless/gdata2/client/PreconditionFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/wireless/gdata2/client/BadRequestException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 931
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_4
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto :goto_0

    .line 858
    :pswitch_1
    :try_start_3
    const-string v3, "ContactsSyncAdapter"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 859
    const-string v3, "ContactsSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updating with entry =====\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    :cond_5
    or-int/lit8 v2, v0, 0x2

    .line 862
    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 863
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    invoke-virtual {p3, v3, p4}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->updateEntry(Lcom/google/wireless/gdata2/data/Entry;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;
    :try_end_3
    .catch Lcom/google/wireless/gdata2/client/PreconditionFailedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/wireless/gdata2/client/BadRequestException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 897
    :catch_1
    move-exception v1

    .line 898
    .local v1, "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :try_start_4
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v7, :cond_d

    .line 900
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->fetchEntry(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 931
    :cond_6
    :goto_2
    if-eqz v2, :cond_7

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_7
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 867
    .end local v1    # "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :pswitch_2
    or-int/lit8 v2, v0, 0x3

    .line 868
    :try_start_5
    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 869
    const-string v3, "ContactsSyncAdapter"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 870
    const-string v3, "ContactsSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->url:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$200(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    :cond_8
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->url:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$200(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 873
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->url:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$200(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->etag:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$300(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v3, p4, v4}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->deleteEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/google/wireless/gdata2/client/PreconditionFailedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/wireless/gdata2/client/BadRequestException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 907
    :catch_2
    move-exception v1

    .line 908
    .local v1, "e":Lcom/google/wireless/gdata2/client/BadRequestException;
    :try_start_6
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v7, :cond_f

    .line 910
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->fetchEntry(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 931
    :cond_9
    :goto_3
    if-eqz v2, :cond_a

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_a
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 895
    .local v1, "e":Lcom/google/wireless/gdata2/client/PreconditionFailedException;
    :cond_b
    :try_start_7
    new-instance v3, Lcom/google/wireless/gdata2/ConflictDetectedException;

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/wireless/gdata2/ConflictDetectedException;-><init>(Lcom/google/wireless/gdata2/data/Entry;)V

    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 931
    .end local v1    # "e":Lcom/google/wireless/gdata2/client/PreconditionFailedException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_c

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_c
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v3

    .line 901
    .local v1, "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :cond_d
    :try_start_8
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v6, :cond_e

    .line 903
    const/4 v3, 0x3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$102(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;I)I

    goto :goto_2

    .line 904
    :cond_e
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v8, :cond_6

    goto/16 :goto_2

    .line 911
    .local v1, "e":Lcom/google/wireless/gdata2/client/BadRequestException;
    :cond_f
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v6, :cond_10

    .line 913
    const/4 v3, 0x3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$102(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;I)I

    goto :goto_3

    .line 914
    :cond_10
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v8, :cond_9

    goto :goto_3

    .line 917
    .end local v1    # "e":Lcom/google/wireless/gdata2/client/BadRequestException;
    :catch_3
    move-exception v1

    .line 918
    .local v1, "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v7, :cond_13

    .line 920
    const/4 v3, 0x3

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$102(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;I)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 931
    :cond_11
    :goto_4
    if-eqz v2, :cond_12

    .line 932
    invoke-static {v2, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 934
    :cond_12
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 921
    :cond_13
    :try_start_9
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->type:I
    invoke-static {p1}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$100(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)I

    move-result v3

    if-ne v3, v8, :cond_11

    goto :goto_4

    .line 924
    .end local v1    # "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    :catch_4
    move-exception v1

    .line 925
    .local v1, "e":Lcom/google/wireless/gdata2/client/HttpException;
    invoke-virtual {v1}, Lcom/google/wireless/gdata2/client/HttpException;->getStatusCode()I

    move-result v3

    const/16 v4, 0x1f7

    if-ne v3, v4, :cond_14

    .line 926
    new-instance v3, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/client/HttpException;->getRetryAfter()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;-><init>(J)V

    throw v3

    .line 928
    :cond_14
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received unhandled http error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/client/HttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 847
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static fetchEntry(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;
    .locals 4
    .param p0, "operation"    # Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    .param p1, "entryClass"    # Ljava/lang/Class;
    .param p2, "client"    # Lcom/google/wireless/gdata2/client/GDataServiceClient;
    .param p3, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/parser/ParseException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
        }
    .end annotation

    .prologue
    .line 946
    :try_start_0
    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->url:Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$200(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p2, p1, v1, p3, v2}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->getEntry(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;
    :try_end_0
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 947
    :catch_0
    move-exception v0

    .line 948
    .local v0, "e":Lcom/google/wireless/gdata2/client/HttpException;
    invoke-virtual {v0}, Lcom/google/wireless/gdata2/client/HttpException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x1f7

    if-ne v1, v2, :cond_0

    .line 949
    new-instance v1, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/client/HttpException;->getRetryAfter()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;-><init>(J)V

    throw v1

    .line 951
    :cond_0
    new-instance v1, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error when redownloading the entry due to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in response to an operation of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 954
    .end local v0    # "e":Lcom/google/wireless/gdata2/client/HttpException;
    :catch_1
    move-exception v0

    .line 955
    .local v0, "e":Lcom/google/wireless/gdata2/GDataException;
    new-instance v1, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error when redownloading the entry due to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in response to an operation of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getCount(Landroid/content/ContentProviderClient;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "client"    # Landroid/content/ContentProviderClient;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 659
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_count"

    aput-object v0, v2, v1

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 663
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 664
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 668
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    .line 669
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 671
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getOrCreateGDataSyncState(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .locals 14
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "groupFeed"    # Ljava/lang/String;
    .param p3, "contactFeed"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 542
    const/4 v10, 0x0

    .line 543
    .local v10, "syncStateChanged":Z
    invoke-static {p1, p0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->getOrCreate(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/contacts/GDataSyncState;

    move-result-object v9

    .line 545
    .local v9, "syncState":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v3

    .line 548
    .local v3, "data":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUri()Ljava/lang/String;

    move-result-object v4

    .line 551
    .local v4, "existingContactsFeedUri":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 552
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->clearContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 553
    const/4 v10, 0x1

    .line 561
    :cond_0
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasHiresPhotoUploadNeeded()Z

    move-result v11

    if-nez v11, :cond_1

    .line 562
    if-eqz v4, :cond_7

    const-string v11, "gprofiles_highresphotos"

    invoke-virtual {v4, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    const/4 v8, 0x1

    .line 564
    .local v8, "stateIsPreHires":Z
    :goto_1
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11, v8}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setHiresPhotoUploadNeeded(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 565
    const/4 v10, 0x1

    .line 570
    .end local v8    # "stateIsPreHires":Z
    :cond_1
    const/4 v2, 0x0

    .line 571
    .local v2, "createNewContactFeedSyncState":Z
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState()Z

    move-result v11

    if-nez v11, :cond_8

    .line 572
    const-string v11, "ContactsSyncAdapter"

    const-string v12, "No contacts feed data; creating new feedSyncState..."

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    const/4 v2, 0x1

    .line 588
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    .line 589
    new-instance v1, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-direct {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;-><init>()V

    .line 590
    .local v1, "contactSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedUri(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 591
    const-string v11, "5.0"

    invoke-virtual {v1, v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedVersion(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 592
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setContactFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 593
    const/4 v10, 0x1

    .line 597
    .end local v1    # "contactSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    :cond_3
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasGroupFeedState()Z

    move-result v11

    if-nez v11, :cond_4

    .line 598
    new-instance v5, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-direct {v5}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;-><init>()V

    .line 599
    .local v5, "groupFeedState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedUri(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 600
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11, v5}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setGroupFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 601
    const/4 v10, 0x1

    .line 604
    .end local v5    # "groupFeedState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    :cond_4
    if-eqz v10, :cond_5

    .line 605
    invoke-virtual {v9, p1}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 607
    :cond_5
    return-object v9

    .line 548
    .end local v2    # "createNewContactFeedSyncState":Z
    .end local v4    # "existingContactsFeedUri":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x0

    goto :goto_0

    .line 562
    .restart local v4    # "existingContactsFeedUri":Ljava/lang/String;
    :cond_7
    const/4 v8, 0x0

    goto :goto_1

    .line 576
    .restart local v2    # "createNewContactFeedSyncState":Z
    :cond_8
    iget-object v11, v9, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v7

    .line 577
    .local v7, "lastSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    const/4 v6, 0x0

    .line 578
    .local v6, "lastFeedVersion":Ljava/lang/String;
    if-eqz v7, :cond_9

    .line 579
    invoke-virtual {v7}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedVersion()Ljava/lang/String;

    move-result-object v6

    .line 581
    :cond_9
    const-string v11, "5.0"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 582
    const-string v11, "ContactsSyncAdapter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Feed version mismatch: lastFeedVersion = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", current version = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "5.0"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    const-string v11, "ContactsSyncAdapter"

    const-string v12, "Creating new feedSyncState..."

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    const/4 v2, 0x1

    goto :goto_2
.end method

.method private getServerDiffs(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/util/Set;I)V
    .locals 58
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "authToken"    # Ljava/lang/String;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .param p6, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .param p8, "trafficTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentProviderClient;",
            "Ljava/lang/String;",
            "Landroid/content/SyncResult;",
            "Lcom/google/android/syncadapters/contacts/GDataSyncState;",
            "Lcom/google/android/syncadapters/EntryAndEntityHandler;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/client/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 1063
    .local p7, "contactsSyncSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1273
    :cond_0
    :goto_0
    return-void

    .line 1066
    :cond_1
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1067
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "starting getServerDiffs for account "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    :cond_2
    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v5, 0x32

    invoke-direct {v7, v5}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 1072
    .local v7, "entries":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Landroid/util/Pair<Ljava/lang/Integer;Lcom/google/wireless/gdata2/data/Entry;>;>;"
    new-instance v19, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v5, 0x32

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 1075
    .local v19, "entities":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;>;"
    const/16 v48, 0x0

    .line 1076
    .local v48, "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    const/16 v44, 0x0

    .line 1078
    .local v44, "entityReader":Lcom/google/android/syncadapters/EntityReader;
    const/16 v51, 0x0

    .line 1079
    .local v51, "numRecords":I
    const/16 v52, 0x0

    .line 1080
    .local v52, "numRecordsPerBatch":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v56

    .line 1081
    .local v56, "startTimeMs":J
    const/16 v49, 0x0

    .line 1082
    .local v49, "fetchSuccessful":Z
    invoke-static/range {p5 .. p6}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->getFeedState(Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v11

    .line 1083
    .local v11, "feedSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUri()Ljava/lang/String;

    move-result-object v9

    .line 1084
    .local v9, "feedUrl":Ljava/lang/String;
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v39

    .line 1085
    .local v39, "activeTag":I
    or-int v5, v39, p8

    invoke-static {v5}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1087
    :try_start_0
    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1089
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1090
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no last updated time present for feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", preparing for full sync"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->updateProviderForInitialSync(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/EntryAndEntityHandler;)V

    .line 1096
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "google_contacts_sync_num_events_per_batch"

    const/16 v8, 0x1f4

    invoke-static {v5, v6, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    .line 1100
    .local v12, "maxResults":I
    new-instance v4, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$ContactsGDataFeedFetcher;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEntryClass()Ljava/lang/Class;

    move-result-object v6

    sget-object v8, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntryEndMarker:Landroid/util/Pair;

    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v13

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v13}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$ContactsGDataFeedFetcher;-><init>(Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Landroid/util/Pair;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1103
    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .local v4, "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :try_start_1
    new-instance v50, Ljava/lang/Thread;

    const-string v5, "GDataFeedFetcher"

    move-object/from16 v0, v50

    invoke-direct {v0, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1104
    .local v50, "fetcherThread":Ljava/lang/Thread;
    invoke-virtual/range {v50 .. v50}, Ljava/lang/Thread;->start()V

    .line 1106
    new-instance v13, Lcom/google/android/syncadapters/EntityReader;

    const-string v14, "ContactsSyncAdapter"

    sget-object v18, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntryEndMarker:Landroid/util/Pair;

    sget-object v20, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntityEndMarker:Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

    const-string v22, "sourceid"

    move-object/from16 v15, p2

    move-object/from16 v16, p1

    move-object/from16 v17, v7

    move-object/from16 v21, p6

    invoke-direct/range {v13 .. v22}, Lcom/google/android/syncadapters/EntityReader;-><init>(Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/util/concurrent/BlockingQueue;Landroid/util/Pair;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1109
    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .local v13, "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :try_start_2
    new-instance v45, Ljava/lang/Thread;

    const-string v5, "EntityReader"

    move-object/from16 v0, v45

    invoke-direct {v0, v13, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1110
    .local v45, "entityReaderThread":Ljava/lang/Thread;
    invoke-virtual/range {v45 .. v45}, Ljava/lang/Thread;->start()V

    .line 1112
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1113
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "starting processing of fetched entries"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    :cond_5
    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v29

    .line 1118
    .local v29, "rawContactsUri":Landroid/net/Uri;
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v30

    .line 1119
    .local v30, "dataUri":Landroid/net/Uri;
    sget-object v5, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v31

    .line 1121
    .local v31, "groupsUri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v21

    .line 1126
    .local v21, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v32

    .line 1128
    .local v32, "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_6
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1129
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1130
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getServerDiffs: noticed a cancel during feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", bailing out"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1247
    :cond_7
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_8

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_8

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1251
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .local v42, "endTimeMs":J
    if-eqz v49, :cond_d

    const-string v40, "SUCCESS"

    .line 1253
    .local v40, "disposition":Ljava/lang/String;
    :goto_2
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_9
    if-eqz v4, :cond_c

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_a
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_b
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_c
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1252
    .restart local v42    # "endTimeMs":J
    :cond_d
    const-string v40, "FAILURE"

    goto/16 :goto_2

    .line 1136
    .end local v42    # "endTimeMs":J
    :cond_e
    :try_start_3
    invoke-interface/range {v19 .. v19}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

    .line 1137
    .local v46, "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    sget-object v5, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sEntityEndMarker:Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;

    move-object/from16 v0, v46

    if-ne v0, v5, :cond_14

    .line 1139
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->applyOperations(Ljava/util/ArrayList;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 1204
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isPartialSyncUnavailable()Z

    move-result v5

    if-eqz v5, :cond_22

    .line 1205
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)V

    .line 1206
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "partial sync unavailable for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1229
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    :catch_0
    move-exception v41

    .line 1230
    .end local v12    # "maxResults":I
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed due to an IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_f

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_f

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1251
    :cond_f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_43

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_4
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_10
    if-eqz v4, :cond_13

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_11
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_12
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_13
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1142
    .end local v41    # "e":Ljava/io/IOException;
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    :cond_14
    :try_start_5
    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entry:Lcom/google/wireless/gdata2/data/Entry;

    move-object/from16 v25, v0

    .line 1143
    .local v25, "entry":Lcom/google/wireless/gdata2/data/Entry;
    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entity:Landroid/content/Entity;

    move-object/from16 v26, v0

    .line 1144
    .local v26, "entity":Landroid/content/Entity;
    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entryIndex:I

    move/from16 v47, v0

    .line 1145
    .local v47, "entryIndex":I
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_15

    const-string v5, "ContactsSyncAdapterFine"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1146
    :cond_15
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "calling applyEntryToEntity for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    :cond_16
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v53

    .line 1152
    .local v53, "previousSize":I
    invoke-static/range {v32 .. v32}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v54

    .line 1163
    .local v54, "savedMatchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v26, :cond_17

    :try_start_6
    invoke-virtual/range {v25 .. v25}, Lcom/google/wireless/gdata2/data/Entry;->getETag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v6

    invoke-interface/range {p6 .. p6}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEtagColumnName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 1166
    :cond_17
    const/16 v27, 0x0

    move-object/from16 v20, p6

    move-object/from16 v22, p1

    move-object/from16 v23, p2

    move-object/from16 v24, p7

    move-object/from16 v28, p4

    invoke-interface/range {v20 .. v32}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->applyEntryToEntity(Ljava/util/ArrayList;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/Set;Lcom/google/wireless/gdata2/data/Entry;Landroid/content/Entity;ZLandroid/content/SyncResult;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1181
    :cond_18
    :try_start_7
    invoke-virtual/range {v25 .. v25}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setIdOfLastFetched(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1182
    move/from16 v0, v47

    invoke-virtual {v11, v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setIndexOfLastFetched(I)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1183
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_19

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1184
    :cond_19
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "index "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " of "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getFeed()Lcom/google/wireless/gdata2/data/Feed;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/data/Feed;->getTotalResults()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1187
    :cond_1a
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1188
    add-int/lit8 v51, v51, 0x1

    .line 1189
    add-int/lit8 v52, v52, 0x1

    .line 1190
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numEntries:J

    .line 1191
    const/16 v5, 0x14

    move/from16 v0, v52

    if-ge v0, v5, :cond_1b

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0x1f4

    if-lt v5, v6, :cond_6

    .line 1193
    :cond_1b
    const/16 v52, 0x0

    .line 1194
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->applyOperations(Ljava/util/ArrayList;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 1195
    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_1c

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1197
    :cond_1c
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "applied "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    sub-long v14, v14, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 1232
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v25    # "entry":Lcom/google/wireless/gdata2/data/Entry;
    .end local v26    # "entity":Landroid/content/Entity;
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .end local v47    # "entryIndex":I
    .end local v53    # "previousSize":I
    .end local v54    # "savedMatchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v41

    .line 1233
    .end local v12    # "maxResults":I
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Landroid/os/RemoteException;
    :goto_5
    :try_start_8
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed"

    move-object/from16 v0, v41

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1234
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_1d

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_1d

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1251
    :cond_1d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_44

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_6
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_1e
    if-eqz v4, :cond_21

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_1f
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_20
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_21
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1170
    .end local v41    # "e":Landroid/os/RemoteException;
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v25    # "entry":Lcom/google/wireless/gdata2/data/Entry;
    .restart local v26    # "entity":Landroid/content/Entity;
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v47    # "entryIndex":I
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    .restart local v53    # "previousSize":I
    .restart local v54    # "savedMatchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_2
    move-exception v55

    .line 1173
    .local v55, "t":Ljava/lang/NullPointerException;
    :try_start_9
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v53

    move-object/from16 v3, v55

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->skipEntry(Ljava/util/ArrayList;ILjava/lang/Throwable;)V

    .line 1174
    move-object/from16 v32, v54

    .line 1175
    goto/16 :goto_1

    .line 1176
    .end local v55    # "t":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v55

    .line 1177
    .local v55, "t":Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v53

    move-object/from16 v3, v55

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->skipEntry(Ljava/util/ArrayList;ILjava/lang/Throwable;)V

    .line 1178
    move-object/from16 v32, v54

    .line 1179
    goto/16 :goto_1

    .line 1208
    .end local v25    # "entry":Lcom/google/wireless/gdata2/data/Entry;
    .end local v26    # "entity":Landroid/content/Entity;
    .end local v47    # "entryIndex":I
    .end local v53    # "previousSize":I
    .end local v54    # "savedMatchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v55    # "t":Ljava/lang/IllegalArgumentException;
    :cond_22
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_28

    .line 1209
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connection failed during feed read of "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1235
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    :catch_4
    move-exception v41

    .line 1236
    .end local v12    # "maxResults":I
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :goto_7
    :try_start_a
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed"

    move-object/from16 v0, v41

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1237
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_23

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_23

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1251
    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_45

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_8
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_24
    if-eqz v4, :cond_27

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_25

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_25
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_26
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_27
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1211
    .end local v41    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    :cond_28
    :try_start_b
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->resumptionFailed()Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 1212
    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIdOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1213
    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIndexOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1214
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 1215
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resumption failed during feed read of "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1238
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    :catch_5
    move-exception v41

    .line 1239
    .end local v12    # "maxResults":I
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Ljava/lang/InterruptedException;
    :goto_9
    :try_start_c
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs interrupted, canceling the sync"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_29

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_29

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 1251
    :cond_29
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_46

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_a
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_2a
    if-eqz v4, :cond_2d

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_2b

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_2b
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_2c
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_2d
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1217
    .end local v41    # "e":Ljava/lang/InterruptedException;
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    :cond_2e
    :try_start_d
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isUnparsableFeed()Z

    move-result v5

    if-eqz v5, :cond_34

    .line 1218
    new-instance v5, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unparsable feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1241
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    :catch_6
    move-exception v41

    .line 1242
    .end local v12    # "maxResults":I
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    :goto_b
    :try_start_e
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getServerDiffs failed due to a ResourceUnavailableException, retryAfter is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v41 .. v41}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->getRetryAfter()J

    move-result-wide v14

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1245
    move-object/from16 v0, p4

    iget-wide v14, v0, Landroid/content/SyncResult;->delayUntil:J

    invoke-virtual/range {v41 .. v41}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->getRetryAfter()J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v14

    move-object/from16 v0, p4

    iput-wide v14, v0, Landroid/content/SyncResult;->delayUntil:J
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_2f

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_2f

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 1251
    :cond_2f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_47

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_c
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_30
    if-eqz v4, :cond_33

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_31

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_31
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_32
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_33
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1220
    .end local v41    # "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    :cond_34
    :try_start_f
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isResourceUnavailable()Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 1221
    new-instance v5, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getRetryAfter()J

    move-result-wide v14

    invoke-direct {v5, v14, v15}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;-><init>(J)V

    throw v5
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 1247
    .end local v12    # "maxResults":I
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    :catchall_0
    move-exception v5

    :goto_d
    or-int v6, v39, p8

    const/4 v8, 0x1

    invoke-static {v6, v8}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_35

    const-string v6, "ContactsSyncAdapter"

    const/4 v8, 0x2

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_35

    const-string v6, "ContactsSyncAdapterP"

    const/4 v8, 0x2

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_36

    .line 1251
    :cond_35
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_48

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_e
    const-string v6, "ContactsSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ": processed "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v51

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " records in "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v14, v42, v56

    invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " ms from feed "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ", updated time is "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_36
    if-eqz v4, :cond_39

    .line 1259
    move-object/from16 v0, p4

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v6, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v8

    int-to-long v0, v8

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v6, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v6

    if-eqz v6, :cond_37

    .line 1261
    move-object/from16 v0, p4

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_37
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v6

    if-eqz v6, :cond_38

    .line 1264
    const-string v6, "ContactsSyncAdapter"

    const-string v8, "getServerDiffs failed - no connection"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v6, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v6, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_38
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_39
    if-eqz v13, :cond_3a

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    :cond_3a
    throw v5

    .line 1223
    .restart local v12    # "maxResults":I
    .restart local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "rawContactsUri":Landroid/net/Uri;
    .restart local v30    # "dataUri":Landroid/net/Uri;
    .restart local v31    # "groupsUri":Landroid/net/Uri;
    .restart local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v45    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .restart local v50    # "fetcherThread":Ljava/lang/Thread;
    :cond_3b
    :try_start_10
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 1224
    new-instance v5, Lcom/google/wireless/gdata2/client/AuthenticationException;

    invoke-direct {v5}, Lcom/google/wireless/gdata2/client/AuthenticationException;-><init>()V

    throw v5

    .line 1226
    :cond_3c
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getFeed()Lcom/google/wireless/gdata2/data/Feed;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/gdata2/data/Feed;->getLastUpdated()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v33, p0

    move-object/from16 v34, p1

    move-object/from16 v35, p2

    move-object/from16 v36, p5

    move-object/from16 v37, p6

    invoke-direct/range {v33 .. v38}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->updateSyncStateAfterFeedRead(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 1228
    const/16 v49, 0x1

    .line 1247
    or-int v5, v39, p8

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1248
    invoke-static/range {v39 .. v39}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1249
    if-eqz v49, :cond_3d

    const-string v5, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_3d

    const-string v5, "ContactsSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3e

    .line 1251
    :cond_3d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v42

    .line 1252
    .restart local v42    # "endTimeMs":J
    if-eqz v49, :cond_42

    const-string v40, "SUCCESS"

    .line 1253
    .restart local v40    # "disposition":Ljava/lang/String;
    :goto_f
    const-string v5, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v14, v42, v56

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v40    # "disposition":Ljava/lang/String;
    .end local v42    # "endTimeMs":J
    :cond_3e
    if-eqz v4, :cond_41

    .line 1259
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getNumUnparsableEntries()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1260
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_3f

    .line 1261
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 1263
    :cond_3f
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->isConnectionFailed()Z

    move-result v5

    if-eqz v5, :cond_40

    .line 1264
    const-string v5, "ContactsSyncAdapter"

    const-string v6, "getServerDiffs failed - no connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1267
    :cond_40
    invoke-virtual {v4}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->close()V

    .line 1269
    :cond_41
    if-eqz v13, :cond_0

    .line 1270
    invoke-virtual {v13}, Lcom/google/android/syncadapters/EntityReader;->close()V

    goto/16 :goto_0

    .line 1252
    .restart local v42    # "endTimeMs":J
    :cond_42
    const-string v40, "FAILURE"

    goto/16 :goto_f

    .end local v12    # "maxResults":I
    .end local v21    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "rawContactsUri":Landroid/net/Uri;
    .end local v30    # "dataUri":Landroid/net/Uri;
    .end local v31    # "groupsUri":Landroid/net/Uri;
    .end local v32    # "matchedEntities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v45    # "entityReaderThread":Ljava/lang/Thread;
    .end local v46    # "entryAndEntity":Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
    .end local v50    # "fetcherThread":Ljava/lang/Thread;
    .local v41, "e":Ljava/io/IOException;
    :cond_43
    const-string v40, "FAILURE"

    goto/16 :goto_4

    .local v41, "e":Landroid/os/RemoteException;
    :cond_44
    const-string v40, "FAILURE"

    goto/16 :goto_6

    .local v41, "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :cond_45
    const-string v40, "FAILURE"

    goto/16 :goto_8

    .local v41, "e":Ljava/lang/InterruptedException;
    :cond_46
    const-string v40, "FAILURE"

    goto/16 :goto_a

    .local v41, "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    :cond_47
    const-string v40, "FAILURE"

    goto/16 :goto_c

    .end local v41    # "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    :cond_48
    const-string v40, "FAILURE"

    goto/16 :goto_e

    .line 1247
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .end local v42    # "endTimeMs":J
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catchall_1
    move-exception v5

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_d

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catchall_2
    move-exception v5

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_d

    .line 1241
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v12    # "maxResults":I
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catch_7
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_b

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catch_8
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_b

    .line 1238
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v12    # "maxResults":I
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catch_9
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_9

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catch_a
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_9

    .line 1235
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v12    # "maxResults":I
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catch_b
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_7

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catch_c
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_7

    .line 1232
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v12    # "maxResults":I
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catch_d
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_5

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catch_e
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_5

    .line 1229
    .end local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .end local v12    # "maxResults":I
    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    :catch_f
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    move-object/from16 v4, v48

    .end local v48    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    .restart local v4    # "feedFetcher":Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
    goto/16 :goto_3

    .end local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v12    # "maxResults":I
    .restart local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    :catch_10
    move-exception v41

    move-object/from16 v13, v44

    .end local v44    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    .restart local v13    # "entityReader":Lcom/google/android/syncadapters/EntityReader;
    goto/16 :goto_3
.end method

.method private innerPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Lcom/google/android/syncadapters/contacts/AuthInfo;)V
    .locals 32
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .param p6, "authInfo"    # Lcom/google/android/syncadapters/contacts/AuthInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/client/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 197
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 198
    invoke-virtual/range {p2 .. p2}, Landroid/os/Bundle;->isEmpty()Z

    .line 199
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "performSync: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    const-string v4, "initialize"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 203
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_1

    .line 208
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@youtube.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 354
    :cond_1
    :goto_1
    return-void

    .line 208
    :cond_2
    const/4 v4, 0x1

    goto :goto_0

    .line 215
    :cond_3
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sGroupHandler:Lcom/google/android/syncadapters/contacts/GroupHandler;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/syncadapters/contacts/GroupHandler;->getFeedForAccount(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v23

    .line 216
    .local v23, "groupFeed":Ljava/lang/String;
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getFeedForAccount(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v21

    .line 217
    .local v21, "contactsFeed":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContactsSyncSet(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Ljava/util/Set;

    move-result-object v11

    .line 220
    .local v11, "contactsSyncSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v4, "?"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 221
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 225
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sz="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getPhotoDownloadSize()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 227
    if-eqz v11, :cond_4

    .line 228
    const-string v4, "?"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 229
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 233
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "group="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-static {v5, v11}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 236
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.google"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->ensureSpecialGroupsAreCreated([Landroid/accounts/Account;)V

    .line 240
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.android.contacts"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    invoke-virtual {v6}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->getServiceName()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v21, v8, v10

    const/4 v10, 0x1

    aput-object v23, v8, v10

    move-object/from16 v0, p1

    invoke-static {v4, v0, v5, v6, v8}, Lcom/google/android/gsf/SubscribedFeeds;->manageSubscriptions(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    .line 245
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v23

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getOrCreateGDataSyncState(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/GDataSyncState;

    move-result-object v9

    .line 248
    .local v9, "syncState":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->markPhotosForUploadIfNeeded(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;)V

    .line 250
    const-string v4, "upload"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v30

    .line 251
    .local v30, "uploadOnly":Z
    if-eqz v30, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->stillDoingInitialSync(Lcom/google/android/syncadapters/contacts/GDataSyncState;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 252
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 253
    const-string v4, "ContactsSyncAdapter"

    const-string v5, "still doing the initial sync so forcing this uploadOnly sync to be a two-way sync"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_5
    const/16 v30, 0x0

    .line 259
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 263
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v7

    .line 264
    .local v7, "authToken":Ljava/lang/String;
    const-string v4, "deletions_override"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    .line 266
    .local v18, "overrideTooManyDeletions":Z
    const-string v4, "discard_deletions"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v22

    .line 268
    .local v22, "discardLocalDeletions":Z
    if-eqz v22, :cond_7

    .line 270
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    .line 271
    .local v31, "values":Landroid/content/ContentValues;
    const-string v4, "deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 272
    sget-object v4, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "deleted=1"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v31

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 275
    invoke-virtual/range {v31 .. v31}, Landroid/content/ContentValues;->clear()V

    .line 276
    const-string v4, "deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 277
    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "deleted=1"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v31

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 282
    .end local v31    # "values":Landroid/content/ContentValues;
    :cond_7
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v20

    .line 283
    .local v20, "activeTag":I
    if-nez v30, :cond_d

    .line 284
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 285
    const-string v4, "ContactsSyncAdapter"

    const-string v5, "fetching changes from server"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_8
    const-string v4, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 289
    .local v25, "specificFeedToFetch":Ljava/lang/String;
    if-eqz v25, :cond_9

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 290
    :cond_9
    sget-object v10, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sGroupHandler:Lcom/google/android/syncadapters/contacts/GroupHandler;

    const/high16 v12, 0x2000000

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v4 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getServerDiffs(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/util/Set;I)V

    .line 292
    invoke-virtual/range {p5 .. p5}, Landroid/content/SyncResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_1

    .line 297
    :cond_a
    if-eqz v25, :cond_b

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 298
    :cond_b
    sget-object v10, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    const/high16 v12, 0x1000000

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v4 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getServerDiffs(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/util/Set;I)V

    .line 300
    invoke-virtual/range {p5 .. p5}, Landroid/content/SyncResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_1

    .line 305
    :cond_c
    const/high16 v4, 0x1000000

    or-int v4, v4, v20

    or-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 307
    :try_start_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoDownloads:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-static {v0, v1, v7, v2, v5}, Lcom/google/android/syncadapters/contacts/ContactHandler;->downloadPhotos(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/wireless/gdata2/contacts/client/ContactsClient;)I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoDownloads:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    const/high16 v4, 0x1000000

    or-int v4, v4, v20

    or-int/lit8 v4, v4, 0x4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 311
    invoke-static/range {v20 .. v20}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 313
    invoke-virtual/range {p5 .. p5}, Landroid/content/SyncResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_1

    .line 319
    .end local v25    # "specificFeedToFetch":Ljava/lang/String;
    :cond_d
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 320
    const-string v4, "ContactsSyncAdapter"

    const-string v5, "scanning for local changes to send to server"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_e
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_4
    const/4 v4, 0x6

    move/from16 v0, v24

    if-ge v0, v4, :cond_f

    .line 323
    move-object/from16 v0, p5

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numInserts:J

    move-object/from16 v0, p5

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v6, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v4, v12

    move-object/from16 v0, p5

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v6, Landroid/content/SyncStats;->numDeletes:J

    add-long v28, v4, v12

    .line 326
    .local v28, "numChangesBefore":J
    sget-object v17, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    const/high16 v19, 0x1000000

    move-object/from16 v12, p0

    move-object/from16 v13, p6

    move-object/from16 v14, p4

    move-object v15, v11

    move-object/from16 v16, p5

    invoke-virtual/range {v12 .. v19}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->processLocalChanges(Lcom/google/android/syncadapters/contacts/AuthInfo;Landroid/content/ContentProviderClient;Ljava/util/Set;Landroid/content/SyncResult;Lcom/google/android/syncadapters/EntryAndEntityHandler;ZI)V

    .line 329
    sget-object v17, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sGroupHandler:Lcom/google/android/syncadapters/contacts/GroupHandler;

    const/high16 v19, 0x2000000

    move-object/from16 v12, p0

    move-object/from16 v13, p6

    move-object/from16 v14, p4

    move-object v15, v11

    move-object/from16 v16, p5

    invoke-virtual/range {v12 .. v19}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->processLocalChanges(Lcom/google/android/syncadapters/contacts/AuthInfo;Landroid/content/ContentProviderClient;Ljava/util/Set;Landroid/content/SyncResult;Lcom/google/android/syncadapters/EntryAndEntityHandler;ZI)V

    .line 332
    const/high16 v4, 0x1000000

    or-int v4, v4, v20

    or-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 334
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    move-object/from16 v12, p1

    move-object/from16 v13, p4

    move-object v14, v7

    move-object/from16 v15, p5

    invoke-static/range {v12 .. v17}, Lcom/google/android/syncadapters/contacts/ContactHandler;->uploadPhotos(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/wireless/gdata2/contacts/client/ContactsClient;Landroid/content/ContentResolver;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 338
    const/high16 v4, 0x1000000

    or-int v4, v4, v20

    or-int/lit8 v4, v4, 0x4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 339
    invoke-static/range {v20 .. v20}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 342
    move-object/from16 v0, p5

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numInserts:J

    move-object/from16 v0, p5

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v6, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v4, v12

    move-object/from16 v0, p5

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v6, Landroid/content/SyncStats;->numDeletes:J

    add-long v26, v4, v12

    .line 344
    .local v26, "numChangesAfter":J
    cmp-long v4, v26, v28

    if-nez v4, :cond_12

    .line 349
    .end local v26    # "numChangesAfter":J
    .end local v28    # "numChangesBefore":J
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 351
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 352
    const-string v4, "ContactsSyncAdapter"

    const-string v5, "performSync: sync is complete"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 223
    .end local v7    # "authToken":Ljava/lang/String;
    .end local v9    # "syncState":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .end local v18    # "overrideTooManyDeletions":Z
    .end local v20    # "activeTag":I
    .end local v22    # "discardLocalDeletions":Z
    .end local v24    # "i":I
    .end local v30    # "uploadOnly":Z
    :cond_10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_2

    .line 231
    :cond_11
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_3

    .line 310
    .restart local v7    # "authToken":Ljava/lang/String;
    .restart local v9    # "syncState":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .restart local v18    # "overrideTooManyDeletions":Z
    .restart local v20    # "activeTag":I
    .restart local v22    # "discardLocalDeletions":Z
    .restart local v25    # "specificFeedToFetch":Ljava/lang/String;
    .restart local v30    # "uploadOnly":Z
    :catchall_0
    move-exception v4

    const/high16 v5, 0x1000000

    or-int v5, v5, v20

    or-int/lit8 v5, v5, 0x4

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 311
    invoke-static/range {v20 .. v20}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v4

    .line 338
    .end local v25    # "specificFeedToFetch":Ljava/lang/String;
    .restart local v24    # "i":I
    .restart local v28    # "numChangesBefore":J
    :catchall_1
    move-exception v4

    const/high16 v5, 0x1000000

    or-int v5, v5, v20

    or-int/lit8 v5, v5, 0x4

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 339
    invoke-static/range {v20 .. v20}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v4

    .line 322
    .restart local v26    # "numChangesAfter":J
    :cond_12
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_4
.end method

.method private isCanceled()Z
    .locals 1

    .prologue
    .line 190
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method private markPhotosForUploadIfNeeded(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 359
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "google_contacts_sync_dont_mark_photos_for_upload_upon_upgrade"

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 362
    .local v0, "dontForceUpload":Z
    iget-object v3, p3, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getHiresPhotoUploadNeeded()Z

    move-result v2

    .line 364
    .local v2, "syncStateIndicatesNeeded":Z
    if-eqz v2, :cond_1

    .line 365
    if-nez v0, :cond_0

    .line 367
    :try_start_0
    invoke-static {p1, p2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->markLocalHiresPhotosAsDirty(Landroid/accounts/Account;Landroid/content/ContentProviderClient;)V
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :cond_0
    iget-object v3, p3, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v3, v5}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setHiresPhotoUploadNeeded(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 374
    invoke-virtual {p3, p2}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 376
    :cond_1
    return-void

    .line 368
    :catch_0
    move-exception v1

    .line 370
    .local v1, "e":Landroid/content/OperationApplicationException;
    new-instance v3, Ljava/io/IOException;

    const-string v4, "error while updating photo rows"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)V
    .locals 1
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .param p3, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1001
    invoke-static {p2, p3}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->getFeedState(Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v0

    .line 1003
    .local v0, "feedSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearFeedUpdatedTime()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1004
    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearDoIncrementalSync()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1005
    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIdOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1006
    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIndexOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1007
    invoke-virtual {p2, p1}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 1008
    return-void
.end method

.method private sendEntityToServer(Ljava/util/Set;Landroid/content/Entity;Lcom/google/android/syncadapters/contacts/AuthInfo;Lcom/google/wireless/gdata2/client/GDataServiceClient;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/EntryAndEntityHandler;Landroid/content/SyncResult;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 22
    .param p2, "entity"    # Landroid/content/Entity;
    .param p3, "authInfo"    # Lcom/google/android/syncadapters/contacts/AuthInfo;
    .param p4, "gDataClient"    # Lcom/google/wireless/gdata2/client/GDataServiceClient;
    .param p5, "provider"    # Landroid/content/ContentProviderClient;
    .param p6, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .param p7, "syncResult"    # Landroid/content/SyncResult;
    .param p8, "rawContactsUri"    # Landroid/net/Uri;
    .param p9, "dataUri"    # Landroid/net/Uri;
    .param p10, "groupsUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Entity;",
            "Lcom/google/android/syncadapters/contacts/AuthInfo;",
            "Lcom/google/wireless/gdata2/client/GDataServiceClient;",
            "Landroid/content/ContentProviderClient;",
            "Lcom/google/android/syncadapters/EntryAndEntityHandler;",
            "Landroid/content/SyncResult;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/parser/ParseException;,
            Lcom/google/wireless/gdata2/client/AuthenticationException;,
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/ConflictDetectedException;,
            Lcom/google/wireless/gdata2/client/ResourceUnavailableException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 788
    .local p1, "contactsSyncSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x1

    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-interface {v0, v1, v4, v2, v6}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->convertEntityToEntry(Landroid/content/Entity;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Z)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v18

    .line 791
    .local v18, "entry":Lcom/google/wireless/gdata2/data/Entry;
    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/data/Entry;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 792
    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/data/Entry;->getEditUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/data/Entry;->getETag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->newDelete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;

    move-result-object v20

    .line 800
    .local v20, "operation":Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    :goto_0
    invoke-interface/range {p6 .. p6}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEntryClass()Ljava/lang/Class;

    move-result-object v19

    .line 802
    .local v19, "entryClass":Ljava/lang/Class;
    :try_start_0
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 803
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "sending operation to server"

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->doServerOperation(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 813
    :goto_1
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 814
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "applying resulting entry to entity"

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 839
    :cond_2
    :goto_2
    return-void

    .line 793
    .end local v19    # "entryClass":Ljava/lang/Class;
    .end local v20    # "operation":Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 794
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getFeedForAccount(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->newInsert(Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;

    move-result-object v20

    .restart local v20    # "operation":Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    goto :goto_0

    .line 796
    .end local v20    # "operation":Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    :cond_4
    invoke-static/range {v18 .. v18}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->newUpdate(Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;

    move-result-object v20

    .restart local v20    # "operation":Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;
    goto :goto_0

    .line 806
    .restart local v19    # "entryClass":Ljava/lang/Class;
    :catch_0
    move-exception v17

    .line 808
    .local v17, "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/syncadapters/contacts/AuthInfo;->invalidateAuthToken()V

    .line 809
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->doServerOperation(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Ljava/lang/Class;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/String;)V

    goto :goto_1

    .line 818
    .end local v17    # "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :pswitch_0
    const/4 v4, 0x0

    move-object/from16 v0, v20

    # setter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static {v0, v4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$002(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;

    .line 822
    :pswitch_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 824
    .local v5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_1
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    # getter for: Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;
    invoke-static/range {v20 .. v20}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;->access$000(Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v9

    const/4 v11, 0x1

    const/16 v16, 0x0

    move-object/from16 v4, p6

    move-object/from16 v7, p5

    move-object/from16 v8, p1

    move-object/from16 v10, p2

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    invoke-interface/range {v4 .. v16}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->applyEntryToEntity(Ljava/util/ArrayList;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/Set;Lcom/google/wireless/gdata2/data/Entry;Landroid/content/Entity;ZLandroid/content/SyncResult;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/Object;)V

    .line 828
    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v21

    .line 829
    .local v21, "result":[Landroid/content/ContentProviderResult;
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 830
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "results are: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    move-object/from16 v0, v21

    invoke-static {v7, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_2

    .line 832
    .end local v21    # "result":[Landroid/content/ContentProviderResult;
    :catch_1
    move-exception v17

    .line 833
    .local v17, "e":Landroid/content/OperationApplicationException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error applying batch"

    move-object/from16 v0, v17

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 834
    .end local v17    # "e":Landroid/content/OperationApplicationException;
    :catch_2
    move-exception v17

    .line 835
    .local v17, "e":Landroid/os/RemoteException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error applying batch"

    move-object/from16 v0, v17

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 816
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private skipEntry(Ljava/util/ArrayList;ILjava/lang/Throwable;)V
    .locals 2
    .param p2, "previousSize"    # I
    .param p3, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v0, "ContactsSyncAdapter"

    const-string v1, "Entry failed, skipping "

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1281
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 1283
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 1285
    :cond_0
    return-void
.end method

.method private stillDoingInitialSync(Lcom/google/android/syncadapters/contacts/GDataSyncState;)Z
    .locals 2
    .param p1, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;

    .prologue
    const/4 v0, 0x1

    .line 379
    iget-object v1, p1, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v1

    if-nez v1, :cond_1

    .line 385
    :cond_0
    :goto_0
    return v0

    .line 382
    :cond_1
    iget-object v1, p1, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateProviderForInitialSync(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/EntryAndEntityHandler;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1016
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1017
    .local v0, "values":Landroid/content/ContentValues;
    invoke-interface {p3}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEtagColumnName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "local android etag magic value"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    invoke-interface {p3, p1}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEntityUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getSourceIdColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IS NOT NULL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1020
    return-void
.end method

.method private updateSyncStateAfterFeedRead(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;Ljava/lang/String;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .param p4, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .param p5, "updatedMin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 1025
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1027
    .local v3, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p3, p4}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->getFeedState(Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v1

    .line 1028
    .local v1, "feedSyncState":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    invoke-virtual {v1, p5}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedUpdatedTime(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1029
    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIdOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1030
    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->clearIndexOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1032
    invoke-virtual {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1033
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1034
    const-string v4, "ContactsSyncAdapter"

    const-string v5, "switching from full to incremental"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    :cond_0
    invoke-interface {p4, p1}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEntityUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEtagColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "local android etag magic value"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1045
    invoke-virtual {v1, v9}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setDoIncrementalSync(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 1048
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1051
    :try_start_0
    invoke-virtual {p2, v3}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1057
    return-void

    .line 1052
    :catch_0
    move-exception v0

    .line 1053
    .local v0, "e":Landroid/content/OperationApplicationException;
    new-instance v4, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v5, "unable to update sync state after successful feed read"

    invoke-direct {v4, v5, v0}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1054
    .end local v0    # "e":Landroid/content/OperationApplicationException;
    :catch_1
    move-exception v2

    .line 1055
    .local v2, "ise":Ljava/lang/IllegalStateException;
    new-instance v4, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v5, "unable to update sync state after successful feed read"

    invoke-direct {v4, v5, v2}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method public ensureSpecialGroupsAreCreated([Landroid/accounts/Account;)V
    .locals 32
    .param p1, "accounts"    # [Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mSpecialGroupsLock:Ljava/lang/Object;

    move-object/from16 v30, v0

    monitor-enter v30

    .line 391
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v17

    .line 392
    .local v17, "hasMyContactsGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v16

    .line 393
    .local v16, "hasFavoritesGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 394
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->PROJECTION_GROUPS:[Ljava/lang/String;

    const-string v5, "account_type=? AND data_set IS NULL"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v31, "com.google"

    aput-object v31, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 398
    .local v12, "c":Landroid/database/Cursor;
    if-nez v12, :cond_0

    .line 399
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3

    .line 536
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v12    # "c":Landroid/database/Cursor;
    .end local v16    # "hasFavoritesGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v17    # "hasMyContactsGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit v30
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 402
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v12    # "c":Landroid/database/Cursor;
    .restart local v16    # "hasFavoritesGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v17    # "hasMyContactsGroup":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 403
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 404
    .local v26, "systemId":Ljava/lang/String;
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 405
    .local v9, "accountName":Ljava/lang/String;
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    const/4 v11, 0x1

    .line 406
    .local v11, "autoAddBit":Z
    :goto_1
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    const/4 v13, 0x1

    .line 407
    .local v13, "favoritesBit":Z
    :goto_2
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 408
    .local v22, "rowId":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 409
    .local v27, "title":Ljava/lang/String;
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 410
    .local v25, "sourceId":Ljava/lang/String;
    const/4 v3, 0x7

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    const/4 v14, 0x1

    .line 414
    .local v14, "groupIsReadOnlyBit":Z
    :goto_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 419
    const-string v3, "Starred in Android"

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 421
    .local v19, "isFavoritesGroup":Z
    const-string v3, "Contacts"

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "System Group: My Contacts"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v26, :cond_6

    if-nez v25, :cond_6

    :cond_1
    const/16 v20, 0x1

    .line 434
    .local v20, "isMyContactsGroup":Z
    :goto_4
    if-eqz v20, :cond_7

    .line 435
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 436
    const-string v3, "My Contacts"

    move-object/from16 v0, v27

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Contacts"

    move-object/from16 v0, v26

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v11, :cond_2

    if-nez v14, :cond_0

    .line 441
    :cond_2
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 442
    .local v29, "values":Landroid/content/ContentValues;
    const-string v3, "auto_add"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 443
    const-string v3, "system_id"

    const-string v4, "Contacts"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 445
    const-string v3, "title"

    const-string v4, "My Contacts"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v22

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    .line 485
    .end local v9    # "accountName":Ljava/lang/String;
    .end local v11    # "autoAddBit":Z
    .end local v13    # "favoritesBit":Z
    .end local v14    # "groupIsReadOnlyBit":Z
    .end local v19    # "isFavoritesGroup":Z
    .end local v20    # "isMyContactsGroup":Z
    .end local v22    # "rowId":J
    .end local v25    # "sourceId":Ljava/lang/String;
    .end local v26    # "systemId":Ljava/lang/String;
    .end local v27    # "title":Ljava/lang/String;
    .end local v29    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v3

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405
    .restart local v9    # "accountName":Ljava/lang/String;
    .restart local v26    # "systemId":Ljava/lang/String;
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 406
    .restart local v11    # "autoAddBit":Z
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_2

    .line 410
    .restart local v13    # "favoritesBit":Z
    .restart local v22    # "rowId":J
    .restart local v25    # "sourceId":Ljava/lang/String;
    .restart local v27    # "title":Ljava/lang/String;
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 421
    .restart local v14    # "groupIsReadOnlyBit":Z
    .restart local v19    # "isFavoritesGroup":Z
    :cond_6
    const/16 v20, 0x0

    goto :goto_4

    .line 451
    .restart local v20    # "isMyContactsGroup":Z
    :cond_7
    if-eqz v26, :cond_a

    .line 452
    if-eqz v27, :cond_c

    :try_start_3
    const-string v3, "System Group: "

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v28, 0x1

    .line 454
    .local v28, "titleStillHasSystemGroup":Z
    :goto_5
    if-nez v28, :cond_8

    if-nez v11, :cond_8

    if-nez v14, :cond_a

    .line 457
    :cond_8
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 458
    .restart local v29    # "values":Landroid/content/ContentValues;
    const-string v3, "auto_add"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 459
    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 460
    if-eqz v28, :cond_9

    .line 461
    const-string v3, "System Group: "

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    .line 463
    .local v24, "shortTitle":Ljava/lang/String;
    const-string v3, "title"

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    .end local v24    # "shortTitle":Ljava/lang/String;
    :cond_9
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v22

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 471
    .end local v28    # "titleStillHasSystemGroup":Z
    .end local v29    # "values":Landroid/content/ContentValues;
    :cond_a
    if-eqz v19, :cond_0

    .line 472
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 473
    if-eqz v13, :cond_b

    if-nez v14, :cond_0

    .line 475
    :cond_b
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 476
    .restart local v29    # "values":Landroid/content/ContentValues;
    const-string v3, "favorites"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 477
    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v22

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    .line 452
    .end local v29    # "values":Landroid/content/ContentValues;
    :cond_c
    const/16 v28, 0x0

    goto/16 :goto_5

    .line 485
    .end local v9    # "accountName":Ljava/lang/String;
    .end local v11    # "autoAddBit":Z
    .end local v13    # "favoritesBit":Z
    .end local v14    # "groupIsReadOnlyBit":Z
    .end local v19    # "isFavoritesGroup":Z
    .end local v20    # "isMyContactsGroup":Z
    .end local v22    # "rowId":J
    .end local v25    # "sourceId":Ljava/lang/String;
    .end local v26    # "systemId":Ljava/lang/String;
    .end local v27    # "title":Ljava/lang/String;
    :cond_d
    :try_start_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 489
    move-object/from16 v10, p1

    .local v10, "arr$":[Landroid/accounts/Account;
    array-length v0, v10

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_6
    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_10

    aget-object v8, v10, v18

    .line 490
    .local v8, "account":Landroid/accounts/Account;
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v15

    .line 491
    .local v15, "groupsUri":Landroid/net/Uri;
    iget-object v3, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 492
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 493
    .restart local v29    # "values":Landroid/content/ContentValues;
    const-string v3, "title"

    const-string v4, "My Contacts"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string v3, "system_id"

    const-string v4, "Contacts"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v3, "group_visible"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 496
    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 497
    const-string v3, "auto_add"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 498
    move-object/from16 v0, v29

    invoke-virtual {v2, v15, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 500
    .end local v29    # "values":Landroid/content/ContentValues;
    :cond_e
    iget-object v3, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 501
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 502
    .restart local v29    # "values":Landroid/content/ContentValues;
    const-string v3, "title"

    const-string v4, "Starred in Android"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v3, "group_visible"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 504
    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 505
    const-string v3, "favorites"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 506
    move-object/from16 v0, v29

    invoke-virtual {v2, v15, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 489
    .end local v29    # "values":Landroid/content/ContentValues;
    :cond_f
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_6

    .line 513
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v15    # "groupsUri":Landroid/net/Uri;
    :cond_10
    move-object/from16 v0, p1

    array-length v3, v0

    if-lez v3, :cond_13

    .line 514
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->PROJECTION_RAW_CONTACTS_ID:[Ljava/lang/String;

    const-string v5, "account_name is null AND account_type is null"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 518
    if-nez v12, :cond_11

    .line 519
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 523
    :cond_11
    const/4 v3, 0x0

    :try_start_5
    aget-object v8, p1, v3

    .line 524
    .restart local v8    # "account":Landroid/accounts/Account;
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 525
    .restart local v29    # "values":Landroid/content/ContentValues;
    const-string v3, "account_name"

    iget-object v4, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v3, "account_type"

    iget-object v4, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :goto_7
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 528
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "account_name is null AND account_type is null"

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_7

    .line 533
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v29    # "values":Landroid/content/ContentValues;
    :catchall_2
    move-exception v3

    :try_start_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3

    .restart local v8    # "account":Landroid/accounts/Account;
    .restart local v29    # "values":Landroid/content/ContentValues;
    :cond_12
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 536
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v29    # "values":Landroid/content/ContentValues;
    :cond_13
    monitor-exit v30
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 537
    return-void
.end method

.method getContactsSyncSet(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Ljava/util/Set;
    .locals 11
    .param p1, "contactsProvider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 612
    sget-object v0, Landroid/provider/ContactsContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "should_sync"

    aput-object v0, v2, v10

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 617
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 618
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 622
    :cond_0
    const/4 v8, 0x1

    .line 623
    .local v8, "syncEverything":Z
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 624
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    move v8, v9

    :goto_1
    goto :goto_0

    :cond_1
    move v8, v10

    goto :goto_1

    .line 627
    :cond_2
    if-eqz v8, :cond_3

    .line 643
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_2
    return-object v3

    .line 631
    :cond_3
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 632
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "sourceid"

    aput-object v3, v2, v0

    const-string v3, "data_set is null AND should_sync!=0 AND sourceid is not null"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 637
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    .line 638
    .local v7, "groupsToSync":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 639
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/syncadapters/contacts/GroupHandler;->getCanonicalGroupSourceId(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 643
    .end local v7    # "groupsToSync":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .restart local v7    # "groupsToSync":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v3, v7

    goto :goto_2
.end method

.method public getPhotoDownloadSize()I
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 676
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "display_max_dim"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 680
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 681
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 683
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method protected getStatsString(Ljava/lang/StringBuffer;Landroid/content/SyncResult;)V
    .locals 6
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .param p2, "result"    # Landroid/content/SyncResult;

    .prologue
    const-wide/16 v4, 0x0

    .line 1370
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numUpdates:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 1371
    const-string v0, "u"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numUpdates:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1373
    :cond_0
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numInserts:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 1374
    const-string v0, "i"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numInserts:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1376
    :cond_1
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numDeletes:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 1377
    const-string v0, "d"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numDeletes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1379
    :cond_2
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numEntries:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 1380
    const-string v0, "n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numEntries:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1382
    :cond_3
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoUploads:I

    if-lez v0, :cond_4

    .line 1383
    const-string v0, "p"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoUploads:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1385
    :cond_4
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoDownloads:I

    if-lez v0, :cond_5

    .line 1386
    const-string v0, "P"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoDownloads:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1388
    :cond_5
    invoke-virtual {p2}, Landroid/content/SyncResult;->toDebugString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1389
    return-void
.end method

.method protected hasTooManyChanges(JJ)Z
    .locals 7
    .param p1, "numEntries"    # J
    .param p3, "numChanges"    # J

    .prologue
    const-wide/16 v0, 0x0

    .line 648
    cmp-long v3, p1, v0

    if-nez v3, :cond_0

    .line 651
    .local v0, "percentChanged":J
    :goto_0
    const-wide/16 v4, 0x5

    cmp-long v3, p3, v4

    if-lez v3, :cond_1

    const-wide/16 v4, 0x14

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    const/4 v2, 0x1

    .line 654
    .local v2, "tooMany":Z
    :goto_1
    return v2

    .line 648
    .end local v0    # "percentChanged":J
    .end local v2    # "tooMany":Z
    :cond_0
    const-wide/16 v4, 0x64

    mul-long/2addr v4, p3

    div-long v0, v4, p1

    goto :goto_0

    .line 651
    .restart local v0    # "percentChanged":J
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public markPhotoForHighResSync(Landroid/net/Uri;Landroid/content/ContentProviderClient;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;

    .prologue
    .line 939
    sget-object v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->markPhotoForHighResSync(Landroid/net/Uri;Landroid/content/ContentProviderClient;)V

    .line 940
    return-void
.end method

.method protected onLogSyncDetails(JJLandroid/content/SyncResult;)V
    .locals 5
    .param p1, "bytesSent"    # J
    .param p3, "bytesReceived"    # J
    .param p5, "result"    # Landroid/content/SyncResult;

    .prologue
    .line 1360
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1361
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0, v0, p5}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getStatsString(Ljava/lang/StringBuffer;Landroid/content/SyncResult;)V

    .line 1362
    const v1, 0x318f9

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "com.android.contacts"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 1364
    return-void
.end method

.method public onPerformLoggedSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoUploads:I

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mPhotoDownloads:I

    .line 162
    new-instance v6, Lcom/google/android/syncadapters/contacts/AuthInfo;

    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "cp"

    invoke-direct {v6, v0, p1, v1}, Lcom/google/android/syncadapters/contacts/AuthInfo;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 163
    .local v6, "authInfo":Lcom/google/android/syncadapters/contacts/AuthInfo;
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v0

    const/high16 v1, 0x400000

    or-int/2addr v0, v1

    const/high16 v1, 0x40000

    or-int v8, v0, v1

    .line 166
    .local v8, "tag":I
    invoke-static {v8}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 168
    :try_start_0
    invoke-direct/range {v0 .. v6}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->innerPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Lcom/google/android/syncadapters/contacts/AuthInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    const/4 v0, 0x1

    invoke-static {v8, v0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 187
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v7

    .line 170
    .local v7, "e":Landroid/os/RemoteException;
    :try_start_1
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    const/4 v0, 0x1

    invoke-static {v8, v0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 171
    .end local v7    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v7

    .line 172
    .local v7, "e":Ljava/io/IOException;
    :try_start_2
    const-string v0, "ContactsSyncAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "ContactsSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IOException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    const/4 v0, 0x1

    invoke-static {v8, v0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 176
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 177
    .local v7, "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :try_start_3
    invoke-virtual {v6}, Lcom/google/android/syncadapters/contacts/AuthInfo;->invalidateAuthToken()V

    .line 178
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 179
    const-string v0, "ContactsSyncAdapter"

    const-string v1, "innerSync failed"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 184
    const/4 v0, 0x1

    invoke-static {v8, v0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 180
    .end local v7    # "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :catch_3
    move-exception v0

    .line 184
    const/4 v0, 0x1

    invoke-static {v8, v0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 184
    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    invoke-static {v8, v1}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 185
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0
.end method

.method processLocalChanges(Lcom/google/android/syncadapters/contacts/AuthInfo;Landroid/content/ContentProviderClient;Ljava/util/Set;Landroid/content/SyncResult;Lcom/google/android/syncadapters/EntryAndEntityHandler;ZI)V
    .locals 24
    .param p1, "authInfo"    # Lcom/google/android/syncadapters/contacts/AuthInfo;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;
    .param p6, "overrideTooManyDeletions"    # Z
    .param p7, "trafficTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/syncadapters/contacts/AuthInfo;",
            "Landroid/content/ContentProviderClient;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SyncResult;",
            "Lcom/google/android/syncadapters/EntryAndEntityHandler;",
            "ZI)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Lcom/google/wireless/gdata2/client/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 691
    .local p3, "contactsSyncSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 780
    :goto_0
    return-void

    .line 698
    :cond_0
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v18

    .line 699
    .local v18, "activeTag":I
    or-int v2, v18, p7

    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 705
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data_set IS NULL AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p5 .. p5}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getSourceIdColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IS NULL OR ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p5 .. p5}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEditUriColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IS NOT NULL AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p5 .. p5}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != 0 OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p5 .. p5}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != 0)))"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 710
    .local v6, "selection":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p5

    move-object/from16 v3, p2

    invoke-interface/range {v2 .. v7}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->newEntityIterator(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/lang/Long;Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v22

    .line 713
    .local v22, "iterator":Landroid/content/EntityIterator;
    if-nez p6, :cond_6

    .line 714
    :try_start_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getEntityUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "data_set IS NULL"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->getCount(Landroid/content/ContentProviderClient;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v23

    .line 716
    .local v23, "total":I
    const/16 v19, 0x0

    .line 717
    .local v19, "deletes":I
    :cond_1
    :goto_1
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 718
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 771
    :try_start_2
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->close()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 777
    or-int v2, v18, p7

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 721
    :cond_2
    :try_start_3
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Entity;

    .line 722
    .local v9, "entity":Landroid/content/Entity;
    invoke-virtual {v9}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    invoke-interface/range {p5 .. p5}, Lcom/google/android/syncadapters/EntryAndEntityHandler;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    const/16 v21, 0x1

    .line 724
    .local v21, "isDeleted":Z
    :goto_2
    if-eqz v21, :cond_1

    .line 725
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 722
    .end local v21    # "isDeleted":Z
    :cond_3
    const/16 v21, 0x0

    goto :goto_2

    .line 728
    .end local v9    # "entity":Landroid/content/Entity;
    :cond_4
    move/from16 v0, v23

    int-to-long v2, v0

    move/from16 v0, v19

    int-to-long v4, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->hasTooManyChanges(JJ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 729
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runSyncLoop: Too many deletions were found in provider "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", not doing any more updates"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-virtual {v2}, Landroid/content/SyncStats;->clear()V

    .line 732
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move/from16 v0, v23

    int-to-long v4, v0

    iput-wide v4, v2, Landroid/content/SyncStats;->numEntries:J

    .line 733
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move/from16 v0, v19

    int-to-long v4, v0

    iput-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    .line 734
    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->tooManyDeletions:Z

    .line 735
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 771
    :try_start_4
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->close()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 777
    or-int v2, v18, p7

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 738
    :cond_5
    :try_start_5
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->reset()V

    .line 741
    .end local v19    # "deletes":I
    .end local v23    # "total":I
    :cond_6
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v15

    .line 743
    .local v15, "rawContactsUri":Landroid/net/Uri;
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v16

    .line 745
    .local v16, "dataUri":Landroid/net/Uri;
    sget-object v2, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/syncadapters/contacts/AuthInfo;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v17

    .line 748
    .local v17, "groupsUri":Landroid/net/Uri;
    :goto_3
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 749
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->isCanceled()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    .line 771
    :try_start_6
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->close()V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 777
    or-int v2, v18, p7

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 752
    :cond_7
    :try_start_7
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Entity;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 754
    .restart local v9    # "entity":Landroid/content/Entity;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    move-object/from16 v7, p0

    move-object/from16 v8, p3

    move-object/from16 v10, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p5

    move-object/from16 v14, p4

    invoke-direct/range {v7 .. v17}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->sendEntityToServer(Ljava/util/Set;Landroid/content/Entity;Lcom/google/android/syncadapters/contacts/AuthInfo;Lcom/google/wireless/gdata2/client/GDataServiceClient;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/EntryAndEntityHandler;Landroid/content/SyncResult;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    :try_end_8
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/wireless/gdata2/ConflictDetectedException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/google/wireless/gdata2/client/ResourceUnavailableException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 756
    :catch_0
    move-exception v20

    .line 757
    .local v20, "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :try_start_9
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error with entity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 758
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 771
    .end local v9    # "entity":Landroid/content/Entity;
    .end local v15    # "rawContactsUri":Landroid/net/Uri;
    .end local v16    # "dataUri":Landroid/net/Uri;
    .end local v17    # "groupsUri":Landroid/net/Uri;
    .end local v20    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :catchall_0
    move-exception v2

    :try_start_a
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->close()V

    throw v2
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 773
    .end local v6    # "selection":Ljava/lang/String;
    .end local v22    # "iterator":Landroid/content/EntityIterator;
    :catch_1
    move-exception v20

    .line 775
    .local v20, "e":Landroid/os/RemoteException;
    :try_start_b
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 777
    or-int v2, v18, p7

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 759
    .end local v20    # "e":Landroid/os/RemoteException;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local v9    # "entity":Landroid/content/Entity;
    .restart local v15    # "rawContactsUri":Landroid/net/Uri;
    .restart local v16    # "dataUri":Landroid/net/Uri;
    .restart local v17    # "groupsUri":Landroid/net/Uri;
    .restart local v22    # "iterator":Landroid/content/EntityIterator;
    :catch_2
    move-exception v20

    .line 760
    .local v20, "e":Ljava/io/IOException;
    :try_start_c
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_3

    .line 762
    .end local v20    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v20

    .line 763
    .local v20, "e":Lcom/google/wireless/gdata2/ConflictDetectedException;
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    goto/16 :goto_3

    .line 764
    .end local v20    # "e":Lcom/google/wireless/gdata2/ConflictDetectedException;
    :catch_4
    move-exception v20

    .line 765
    .local v20, "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    move-object/from16 v0, p4

    iget-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->getRetryAfter()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-object/from16 v0, p4

    iput-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    .line 766
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ResourceUnavailableException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_3

    .line 771
    .end local v9    # "entity":Landroid/content/Entity;
    .end local v20    # "e":Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
    :cond_8
    :try_start_d
    invoke-interface/range {v22 .. v22}, Landroid/content/EntityIterator;->close()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 777
    or-int v2, v18, p7

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 777
    .end local v6    # "selection":Ljava/lang/String;
    .end local v15    # "rawContactsUri":Landroid/net/Uri;
    .end local v16    # "dataUri":Landroid/net/Uri;
    .end local v17    # "groupsUri":Landroid/net/Uri;
    .end local v22    # "iterator":Landroid/content/EntityIterator;
    :catchall_1
    move-exception v2

    or-int v3, v18, p7

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 778
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v2
.end method
