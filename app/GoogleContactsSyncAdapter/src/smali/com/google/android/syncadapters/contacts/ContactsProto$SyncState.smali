.class public final Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ContactsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/contacts/ContactsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SyncState"
.end annotation


# instance fields
.field private cachedSize:I

.field private contactFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

.field private groupFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

.field private hasContactFeedState:Z

.field private hasGroupFeedState:Z

.field private hasHiresPhotoUploadNeeded:Z

.field private hasVersion:Z

.field private hiresPhotoUploadNeeded_:Z

.field private version_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 252
    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    .line 257
    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->version_:I

    .line 274
    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hiresPhotoUploadNeeded_:Z

    .line 291
    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->contactFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 311
    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->groupFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 359
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->cachedSize:I

    .line 252
    return-void
.end method


# virtual methods
.method public clearContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState:Z

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->contactFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 305
    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    .prologue
    .line 362
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->cachedSize:I

    if-gez v0, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getSerializedSize()I

    .line 366
    :cond_0
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->cachedSize:I

    return v0
.end method

.method public getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->contactFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    return-object v0
.end method

.method public getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->groupFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    return-object v0
.end method

.method public getHiresPhotoUploadNeeded()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hiresPhotoUploadNeeded_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "size":I
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasVersion()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasHiresPhotoUploadNeeded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 377
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getHiresPhotoUploadNeeded()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 381
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    .line 384
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasGroupFeedState()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 385
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    .line 388
    :cond_3
    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->cachedSize:I

    .line 389
    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->version_:I

    return v0
.end method

.method public hasContactFeedState()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState:Z

    return v0
.end method

.method public hasGroupFeedState()Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasGroupFeedState:Z

    return v0
.end method

.method public hasHiresPhotoUploadNeeded()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasHiresPhotoUploadNeeded:Z

    return v0
.end method

.method public hasVersion()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    .line 398
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 402
    invoke-virtual {p0, p1, v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 403
    :sswitch_0
    return-object p0

    .line 408
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setVersion(I)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    goto :goto_0

    .line 412
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setHiresPhotoUploadNeeded(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    goto :goto_0

    .line 416
    :sswitch_3
    new-instance v1, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-direct {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;-><init>()V

    .line 417
    .local v1, "value":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    .line 418
    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setContactFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    goto :goto_0

    .line 422
    .end local v1    # "value":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    :sswitch_4
    new-instance v1, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-direct {v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;-><init>()V

    .line 423
    .restart local v1    # "value":Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    .line 424
    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setGroupFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    goto :goto_0

    .line 398
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    move-result-object v0

    return-object v0
.end method

.method public setContactFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 1
    .param p1, "value"    # Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 298
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState:Z

    .line 299
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->contactFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 300
    return-object p0
.end method

.method public setGroupFeedState(Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 1
    .param p1, "value"    # Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .prologue
    .line 315
    if-nez p1, :cond_0

    .line 316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 318
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasGroupFeedState:Z

    .line 319
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->groupFeedState_:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 320
    return-object p0
.end method

.method public setHiresPhotoUploadNeeded(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasHiresPhotoUploadNeeded:Z

    .line 279
    iput-boolean p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hiresPhotoUploadNeeded_:Z

    .line 280
    return-object p0
.end method

.method public setVersion(I)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasVersion:Z

    .line 262
    iput p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->version_:I

    .line 263
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasVersion()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 348
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasHiresPhotoUploadNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getHiresPhotoUploadNeeded()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    .line 351
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasContactFeedState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    .line 354
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->hasGroupFeedState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    .line 357
    :cond_3
    return-void
.end method
