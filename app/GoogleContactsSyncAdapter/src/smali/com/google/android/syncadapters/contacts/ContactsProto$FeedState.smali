.class public final Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ContactsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/contacts/ContactsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeedState"
.end annotation


# instance fields
.field private cachedSize:I

.field private doIncrementalSync_:Z

.field private feedUpdatedTime_:Ljava/lang/String;

.field private feedUri_:Ljava/lang/String;

.field private feedVersion_:Ljava/lang/String;

.field private hasDoIncrementalSync:Z

.field private hasFeedUpdatedTime:Z

.field private hasFeedUri:Z

.field private hasFeedVersion:Z

.field private hasIdOfLastFetched:Z

.field private hasIndexOfLastFetched:Z

.field private idOfLastFetched_:Ljava/lang/String;

.field private indexOfLastFetched_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    .line 15
    iput-boolean v1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->doIncrementalSync_:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUpdatedTime_:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->indexOfLastFetched_:I

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->idOfLastFetched_:Ljava/lang/String;

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUri_:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedVersion_:Ljava/lang/String;

    .line 152
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->cachedSize:I

    .line 10
    return-void
.end method


# virtual methods
.method public clearDoIncrementalSync()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasDoIncrementalSync:Z

    .line 25
    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->doIncrementalSync_:Z

    .line 26
    return-object p0
.end method

.method public clearFeedUpdatedTime()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime:Z

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUpdatedTime_:Ljava/lang/String;

    .line 43
    return-object p0
.end method

.method public clearIdOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIdOfLastFetched:Z

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->idOfLastFetched_:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public clearIndexOfLastFetched()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched:Z

    .line 59
    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->indexOfLastFetched_:I

    .line 60
    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->cachedSize:I

    if-gez v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getSerializedSize()I

    .line 159
    :cond_0
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->cachedSize:I

    return v0
.end method

.method public getDoIncrementalSync()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->doIncrementalSync_:Z

    return v0
.end method

.method public getFeedUpdatedTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUpdatedTime_:Ljava/lang/String;

    return-object v0
.end method

.method public getFeedUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getFeedVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getIdOfLastFetched()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->idOfLastFetched_:Ljava/lang/String;

    return-object v0
.end method

.method public getIndexOfLastFetched()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->indexOfLastFetched_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "size":I
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasDoIncrementalSync()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIndexOfLastFetched()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIdOfLastFetched()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIdOfLastFetched()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUri()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 182
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedVersion()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 186
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_5
    iput v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->cachedSize:I

    .line 190
    return v0
.end method

.method public hasDoIncrementalSync()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasDoIncrementalSync:Z

    return v0
.end method

.method public hasFeedUpdatedTime()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime:Z

    return v0
.end method

.method public hasFeedUri()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUri:Z

    return v0
.end method

.method public hasFeedVersion()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedVersion:Z

    return v0
.end method

.method public hasIdOfLastFetched()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIdOfLastFetched:Z

    return v0
.end method

.method public hasIndexOfLastFetched()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    .line 199
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 203
    invoke-virtual {p0, p1, v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    :sswitch_0
    return-object p0

    .line 209
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setDoIncrementalSync(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 213
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedUpdatedTime(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 217
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setIndexOfLastFetched(I)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 221
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setIdOfLastFetched(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 225
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedUri(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 229
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->setFeedVersion(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    goto :goto_0

    .line 199
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v0

    return-object v0
.end method

.method public setDoIncrementalSync(Z)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasDoIncrementalSync:Z

    .line 20
    iput-boolean p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->doIncrementalSync_:Z

    .line 21
    return-object p0
.end method

.method public setFeedUpdatedTime(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime:Z

    .line 37
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUpdatedTime_:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public setFeedUri(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUri:Z

    .line 88
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedUri_:Ljava/lang/String;

    .line 89
    return-object p0
.end method

.method public setFeedVersion(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedVersion:Z

    .line 105
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->feedVersion_:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public setIdOfLastFetched(Ljava/lang/String;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIdOfLastFetched:Z

    .line 71
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->idOfLastFetched_:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public setIndexOfLastFetched(I)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched:Z

    .line 54
    iput p1, p0, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->indexOfLastFetched_:I

    .line 55
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasDoIncrementalSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUpdatedTime()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 138
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIndexOfLastFetched()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 141
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIdOfLastFetched()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIdOfLastFetched()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 144
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedUri()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 145
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 147
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasFeedVersion()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 148
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 150
    :cond_5
    return-void
.end method
