.class Lcom/google/android/syncadapters/contacts/GalProvider$1;
.super Ljava/lang/Object;
.source "GalProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/syncadapters/contacts/GalProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/syncadapters/contacts/GalProvider;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$filter:Ljava/lang/String;

.field final synthetic val$finalNumResults:I

.field final synthetic val$match:I

.field final synthetic val$projection:[Ljava/lang/String;

.field final synthetic val$removeDuplicates:Z

.field final synthetic val$sortOrder:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/syncadapters/contacts/GalProvider;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->this$0:Lcom/google/android/syncadapters/contacts/GalProvider;

    iput p2, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$match:I

    iput-object p3, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$projection:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$accountName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$filter:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$sortOrder:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$finalNumResults:I

    iput-boolean p8, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$removeDuplicates:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 226
    iget v2, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$match:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    move v6, v0

    .line 227
    .local v6, "isEmail":Z
    :goto_0
    iget v2, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$match:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    move v7, v0

    .line 228
    .local v7, "isPhone":Z
    :goto_1
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->this$0:Lcom/google/android/syncadapters/contacts/GalProvider;

    iget-object v1, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$projection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$accountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$filter:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$sortOrder:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$finalNumResults:I

    iget-boolean v8, p0, Lcom/google/android/syncadapters/contacts/GalProvider$1;->val$removeDuplicates:Z

    # invokes: Lcom/google/android/syncadapters/contacts/GalProvider;->handleFilter([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;
    invoke-static/range {v0 .. v8}, Lcom/google/android/syncadapters/contacts/GalProvider;->access$000(Lcom/google/android/syncadapters/contacts/GalProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .end local v6    # "isEmail":Z
    .end local v7    # "isPhone":Z
    :cond_0
    move v6, v1

    .line 226
    goto :goto_0

    .restart local v6    # "isEmail":Z
    :cond_1
    move v7, v1

    .line 227
    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/GalProvider$1;->call()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
