.class Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;
.super Ljava/lang/Object;
.source "GalProvider.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/contacts/GalProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NameComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final collator:Ljava/text/Collator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;->collator:Ljava/text/Collator;

    .line 919
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;->collator:Ljava/text/Collator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 920
    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;)I
    .locals 6
    .param p1, "lhs"    # Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;
    .param p2, "rhs"    # Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 924
    iget-object v3, p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 925
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;->collator:Ljava/text/Collator;

    iget-object v4, p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    iget-object v5, p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 926
    .local v0, "res":I
    if-eqz v0, :cond_2

    move v1, v0

    .line 937
    .end local v0    # "res":I
    :cond_0
    :goto_0
    return v1

    .line 929
    :cond_1
    iget-object v3, p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 931
    iget-object v3, p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->sortName:Ljava/lang/String;

    if-eqz v3, :cond_2

    move v1, v2

    .line 932
    goto :goto_0

    .line 934
    :cond_2
    iget v3, p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->id:I

    iget v4, p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->id:I

    if-eq v3, v4, :cond_3

    .line 935
    iget v3, p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->id:I

    iget v4, p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;->id:I

    if-gt v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 937
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 913
    check-cast p1, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;->compare(Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;)I

    move-result v0

    return v0
.end method
