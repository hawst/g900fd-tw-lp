.class public Lcom/google/android/syncadapters/contacts/GDataSyncState;
.super Ljava/lang/Object;
.source "GDataSyncState.java"


# instance fields
.field public final feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

.field public final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    .line 48
    new-instance v0, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-direct {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "feedData"    # Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    .line 53
    iput-object p2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 54
    return-void
.end method

.method public static create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .locals 5
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 134
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v2, "values":Landroid/content/ContentValues;
    new-instance v0, Lcom/google/android/syncadapters/contacts/GDataSyncState;

    invoke-direct {v0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;-><init>()V

    .line 136
    .local v0, "syncState":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    const-string v3, "data"

    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->toBytes(Lcom/google/android/syncadapters/contacts/GDataSyncState;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 137
    const-string v3, "account_name"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, "account_type"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget-object v3, Landroid/provider/ContactsContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v3, v2}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 140
    .local v1, "uri":Landroid/net/Uri;
    new-instance v3, Lcom/google/android/syncadapters/contacts/GDataSyncState;

    iget-object v4, v0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-direct {v3, v1, v4}, Lcom/google/android/syncadapters/contacts/GDataSyncState;-><init>(Landroid/net/Uri;Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;)V

    return-object v3
.end method

.method private static feedStateToString(Ljava/lang/String;Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "feedState"    # Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .param p2, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v2, 0x20

    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIndexOfLastFetched()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIdOfLastFetched()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :cond_0
    return-void
.end method

.method static fromBytes(Landroid/net/Uri;[B)Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .locals 7
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "syncStateData"    # [B

    .prologue
    const/4 v4, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 86
    if-eqz p1, :cond_1

    array-length v3, p1

    if-le v3, v4, :cond_1

    const/4 v3, 0x0

    aget-byte v3, p1, v3

    const/4 v4, -0x2

    if-ne v3, v4, :cond_1

    aget-byte v3, p1, v5

    const/16 v4, -0x13

    if-ne v3, v4, :cond_1

    const/4 v3, 0x2

    aget-byte v3, p1, v3

    const/16 v4, -0x3f

    if-ne v3, v4, :cond_1

    aget-byte v3, p1, v6

    const/16 v4, 0x12

    if-ne v3, v4, :cond_1

    .line 92
    new-instance v2, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-direct {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;-><init>()V

    .line 94
    .local v2, "syncState":Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    const/4 v3, 0x4

    :try_start_0
    array-length v4, p1

    add-int/lit8 v4, v4, -0x4

    invoke-virtual {v2, p1, v3, v4}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->mergeFrom([BII)Lcom/google/protobuf/micro/MessageMicro;

    .line 96
    invoke-virtual {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getVersion()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 97
    new-instance v1, Lcom/google/android/syncadapters/contacts/GDataSyncState;

    invoke-direct {v1, p0, v2}, Lcom/google/android/syncadapters/contacts/GDataSyncState;-><init>(Landroid/net/Uri;Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;)V

    .line 98
    .local v1, "ret":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    const-string v3, "GDataSyncState"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    const-string v3, "GDataSyncState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    .end local v1    # "ret":Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .end local v2    # "syncState":Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    :cond_0
    :goto_0
    return-object v1

    .line 103
    .restart local v2    # "syncState":Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    const-string v3, "GDataSyncState"

    const-string v4, "Failed to parse sync state."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    .end local v0    # "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    .end local v2    # "syncState":Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;
    :cond_1
    new-instance v1, Lcom/google/android/syncadapters/contacts/GDataSyncState;

    new-instance v3, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-direct {v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;-><init>()V

    invoke-direct {v1, p0, v3}, Lcom/google/android/syncadapters/contacts/GDataSyncState;-><init>(Landroid/net/Uri;Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;)V

    goto :goto_0
.end method

.method public static getFeedState(Lcom/google/android/syncadapters/contacts/GDataSyncState;Lcom/google/android/syncadapters/EntryAndEntityHandler;)Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .locals 4
    .param p0, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .param p1, "handler"    # Lcom/google/android/syncadapters/EntryAndEntityHandler;

    .prologue
    const/4 v0, 0x0

    .line 164
    if-nez p1, :cond_0

    .line 165
    const-string v1, "GDataSyncState"

    const-string v2, "Null handler"

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    :goto_0
    return-object v0

    .line 168
    :cond_0
    instance-of v1, p1, Lcom/google/android/syncadapters/contacts/ContactHandler;

    if-eqz v1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_1
    instance-of v1, p1, Lcom/google/android/syncadapters/contacts/GroupHandler;

    if-eqz v1, :cond_2

    .line 172
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v0}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v0

    goto :goto_0

    .line 174
    :cond_2
    const-string v1, "GDataSyncState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown handler type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getOrCreate(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/contacts/GDataSyncState;
    .locals 3
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-static {p0, p1}, Landroid/provider/ContactsContract$SyncState;->getWithUri(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Landroid/util/Pair;

    move-result-object v0

    .line 147
    .local v0, "syncStateUriAndData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/net/Uri;[B>;"
    if-nez v0, :cond_0

    .line 148
    invoke-static {p0, p1}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/contacts/GDataSyncState;

    move-result-object v1

    .line 150
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v2, v1}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->fromBytes(Landroid/net/Uri;[B)Lcom/google/android/syncadapters/contacts/GDataSyncState;

    move-result-object v1

    goto :goto_0
.end method

.method static toBytes(Lcom/google/android/syncadapters/contacts/GDataSyncState;)[B
    .locals 5
    .param p0, "syncState"    # Lcom/google/android/syncadapters/contacts/GDataSyncState;

    .prologue
    .line 114
    const-string v2, "GDataSyncState"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const-string v2, "GDataSyncState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Writing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->setVersion(I)Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    .line 119
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 120
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 121
    const/16 v2, -0x13

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 122
    const/16 v2, -0x3f

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 123
    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 124
    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 125
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 128
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    return-object v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "GDataSyncState"

    const-string v3, "Failed to encode."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 128
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public newUpdateOperation()Landroid/content/ContentProviderOperation;
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->toBytes(Lcom/google/android/syncadapters/contacts/GDataSyncState;)[B

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/SyncStateContract$Helpers;->newUpdateOperation(Landroid/net/Uri;[B)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "GDataSyncData:{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    iget-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 63
    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getHiresPhotoUploadNeeded()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 64
    const-string v1, "c"

    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getContactFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedStateToString(Ljava/lang/String;Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;Ljava/lang/StringBuilder;)V

    .line 65
    const-string v1, "g"

    iget-object v2, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedData:Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;

    invoke-virtual {v2}, Lcom/google/android/syncadapters/contacts/ContactsProto$SyncState;->getGroupFeedState()Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->feedStateToString(Ljava/lang/String;Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;Ljava/lang/StringBuilder;)V

    .line 67
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateInProvider(Landroid/content/ContentProviderClient;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/contacts/GDataSyncState;->toBytes(Lcom/google/android/syncadapters/contacts/GDataSyncState;)[B

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/provider/SyncStateContract$Helpers;->update(Landroid/content/ContentProviderClient;Landroid/net/Uri;[B)V

    .line 157
    return-void
.end method
