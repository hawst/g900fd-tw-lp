.class public final Lcom/google/android/syncadapters/contacts/ContactHandler;
.super Ljava/lang/Object;
.source "ContactHandler.java"

# interfaces
.implements Lcom/google/android/syncadapters/EntryAndEntityHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;
    }
.end annotation


# static fields
.field private static final DATA_VALUES_EXACT_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

.field private static final DATA_VALUES_KEY_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

.field static final ENTRY_IM_PROTOCOL_TO_PROVIDER_PROTOCOL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TO_PROVIDER_PRIORITY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TO_PROVIDER_SENSITIVITY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_CALENDAR_LINK:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_EMAIL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_EVENT:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_EXTERNAL_ID:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_IM:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_JOT:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_ORGANIZATION:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_PHONE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_POSTAL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_RELATION:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_SIP_ADDRESS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ENTRY_TYPE_TO_PROVIDER_WEBSITE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final PROJECTION_PHOTO_COLUMNS:[Ljava/lang/String;

.field static final PROJECTION_PHOTO_FULLDATA:[Ljava/lang/String;

.field static final PROJECTION_PHOTO_METADATA:[Ljava/lang/String;

.field static final PROVIDER_IM_PROTOCOL_TO_ENTRY_PROTOCOL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TO_ENTRY_PRIORITY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TO_ENTRY_SENSITIVITY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_CALENDAR_LINK:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_EMAIL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_EVENT:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_EXTERNAL_ID:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_IM:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_JOT:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_ORGANIZATION:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_PHONE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_POSTAL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_RELATION:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_SIP_ADDRESS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final PROVIDER_TYPE_TO_ENTRY_WEBSITE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

.field static final SERVER_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SYNC_HIGH_RES:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 139
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    .line 175
    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "vnd.android.cursor.item/photo"

    aput-object v2, v1, v8

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

    .line 177
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v8

    const-string v2, "raw_contact_id"

    aput-object v2, v1, v4

    const-string v2, "data_sync2"

    aput-object v2, v1, v5

    const-string v2, "data_sync1"

    aput-object v2, v1, v6

    const-string v2, "data_version"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "data14"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "starred"

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_METADATA:[Ljava/lang/String;

    .line 186
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v8

    const-string v2, "raw_contact_id"

    aput-object v2, v1, v4

    const-string v2, "data_sync2"

    aput-object v2, v1, v5

    const-string v2, "data_sync1"

    aput-object v2, v1, v6

    const-string v2, "data_version"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "data15"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "data_sync4"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "data_sync3"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "photo_uri"

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_FULLDATA:[Ljava/lang/String;

    .line 197
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v8

    const-string v2, "raw_contact_id"

    aput-object v2, v1, v4

    const-string v2, "data_sync1"

    aput-object v2, v1, v5

    const-string v2, "data_version"

    aput-object v2, v1, v6

    const-string v2, "data14"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "data_sync4"

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_COLUMNS:[Ljava/lang/String;

    .line 211
    const-string v1, "Sync_High_Res"

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SYNC_HIGH_RES:Ljava/lang/String;

    .line 2145
    new-instance v1, Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    invoke-direct {v1, v4}, Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;-><init>(Z)V

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_EXACT_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    .line 2147
    new-instance v1, Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    invoke-direct {v1, v8}, Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;-><init>(Z)V

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_KEY_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    .line 2160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2161
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2162
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2163
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2169
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2170
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2171
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_IM_PROTOCOL_TO_PROVIDER_PROTOCOL:Ljava/util/HashMap;

    .line 2172
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_IM_PROTOCOL_TO_ENTRY_PROTOCOL:Ljava/util/HashMap;

    .line 2174
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2175
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2176
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2177
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2178
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EMAIL:Ljava/util/HashMap;

    .line 2180
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EMAIL:Ljava/util/HashMap;

    .line 2182
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2183
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2187
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2188
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2189
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2190
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2191
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2192
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2193
    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2194
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2195
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2196
    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2197
    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2199
    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200
    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2201
    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2202
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2203
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_PHONE:Ljava/util/HashMap;

    .line 2204
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_PHONE:Ljava/util/HashMap;

    .line 2206
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2207
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2209
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2210
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2211
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_POSTAL:Ljava/util/HashMap;

    .line 2212
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_POSTAL:Ljava/util/HashMap;

    .line 2214
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2215
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2216
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2217
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_IM:Ljava/util/HashMap;

    .line 2220
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_IM:Ljava/util/HashMap;

    .line 2222
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2223
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2225
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2226
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_ORGANIZATION:Ljava/util/HashMap;

    .line 2227
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_ORGANIZATION:Ljava/util/HashMap;

    .line 2229
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2230
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2231
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2233
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2235
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_WEBSITE:Ljava/util/HashMap;

    .line 2239
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_WEBSITE:Ljava/util/HashMap;

    .line 2241
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2242
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2243
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2244
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2245
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2247
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2251
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2252
    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2253
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2254
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255
    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2257
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_RELATION:Ljava/util/HashMap;

    .line 2258
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_RELATION:Ljava/util/HashMap;

    .line 2260
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2261
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2262
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2263
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2265
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2266
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_JOT:Ljava/util/HashMap;

    .line 2267
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_JOT:Ljava/util/HashMap;

    .line 2269
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2270
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2271
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2272
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EXTERNAL_ID:Ljava/util/HashMap;

    .line 2276
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EXTERNAL_ID:Ljava/util/HashMap;

    .line 2278
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2279
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2280
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2281
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2282
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2283
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TO_PROVIDER_PRIORITY:Ljava/util/HashMap;

    .line 2284
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TO_ENTRY_PRIORITY:Ljava/util/HashMap;

    .line 2286
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2287
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2289
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2293
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TO_PROVIDER_SENSITIVITY:Ljava/util/HashMap;

    .line 2297
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TO_ENTRY_SENSITIVITY:Ljava/util/HashMap;

    .line 2299
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2300
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2301
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2302
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2303
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2304
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_CALENDAR_LINK:Ljava/util/HashMap;

    .line 2305
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_CALENDAR_LINK:Ljava/util/HashMap;

    .line 2307
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2308
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2309
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2310
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2311
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EVENT:Ljava/util/HashMap;

    .line 2312
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EVENT:Ljava/util/HashMap;

    .line 2314
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2315
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2316
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2317
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2318
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319
    sput-object v0, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_SIP_ADDRESS:Ljava/util/HashMap;

    .line 2320
    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_SIP_ADDRESS:Ljava/util/HashMap;

    .line 2323
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/name"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2324
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/photo"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2325
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/note"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2326
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/sip_address"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2327
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2328
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_misc"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2329
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2330
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/website"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2331
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_hobby"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2332
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_user_defined_field"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2333
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_language"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2334
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_calendar_link"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2335
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_external_id"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2336
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_jot"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2337
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/relation"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2338
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/contact_event"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2339
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2340
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2341
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2342
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/organization"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2343
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/im"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2344
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.com.google.cursor.item/contact_extended_property"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2345
    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    const-string v2, "vnd.android.cursor.item/identity"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2346
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Landroid/content/ContentValues;
    .param p2, "x2"    # Landroid/content/ContentValues;

    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->compareStringColumn(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Landroid/content/ContentValues;
    .param p2, "x2"    # Landroid/content/ContentValues;

    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->compareBooleanColumn(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method private static compareBooleanColumn(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 8
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "o1"    # Landroid/content/ContentValues;
    .param p2, "o2"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2115
    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    move v0, v2

    .line 2116
    .local v0, "b1":Z
    :goto_0
    invoke-virtual {p2, p0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p2, p0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    move v1, v2

    .line 2117
    .local v1, "b2":Z
    :goto_1
    if-eq v0, v1, :cond_3

    .line 2118
    if-eqz v0, :cond_0

    const/4 v2, -0x1

    .line 2120
    :cond_0
    :goto_2
    return v2

    .end local v0    # "b1":Z
    .end local v1    # "b2":Z
    :cond_1
    move v0, v3

    .line 2115
    goto :goto_0

    .restart local v0    # "b1":Z
    :cond_2
    move v1, v3

    .line 2116
    goto :goto_1

    .restart local v1    # "b2":Z
    :cond_3
    move v2, v3

    .line 2120
    goto :goto_2
.end method

.method private static compareStringColumn(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 3
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "o1"    # Landroid/content/ContentValues;
    .param p2, "o2"    # Landroid/content/ContentValues;

    .prologue
    .line 2125
    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2126
    .local v0, "s1":Ljava/lang/String;
    invoke-virtual {p2, p0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2127
    .local v1, "s2":Ljava/lang/String;
    const-string v2, "null"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2128
    const/4 v0, 0x0

    .line 2130
    :cond_0
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2131
    const/4 v1, 0x0

    .line 2133
    :cond_1
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 2134
    const/4 v2, 0x0

    .line 2142
    :goto_0
    return v2

    .line 2136
    :cond_2
    if-nez v0, :cond_3

    .line 2137
    const/4 v2, -0x1

    goto :goto_0

    .line 2139
    :cond_3
    if-nez v1, :cond_4

    .line 2140
    const/4 v2, 0x1

    goto :goto_0

    .line 2142
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method static downloadPhotos(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/wireless/gdata2/contacts/client/ContactsClient;)I
    .locals 34
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "authToken"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;
    .param p4, "client"    # Lcom/google/wireless/gdata2/contacts/client/ContactsClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/client/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 1731
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    .line 1732
    .local v5, "uriToRead":Landroid/net/Uri;
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v30

    .line 1733
    .local v30, "uriToWrite":Landroid/net/Uri;
    const/4 v11, 0x0

    .line 1735
    .local v11, "count":I
    :try_start_0
    sget-object v6, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_METADATA:[Ljava/lang/String;

    const-string v7, "mimetype=? AND data_sync2 IS NOT NULL AND (data_sync3 IS NULL   OR data_sync3!=data_sync2) AND data_sync1 IS NOT NULL"

    sget-object v8, Lcom/google/android/syncadapters/contacts/ContactHandler;->SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    check-cast v10, Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1739
    .local v10, "c":Landroid/database/AbstractWindowedCursor;
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1740
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v24

    .line 1741
    .local v24, "photoDataRowId":J
    const/4 v4, 0x1

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v12

    .line 1742
    .local v12, "contactId":J
    const/4 v4, 0x2

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1743
    .local v18, "etag":Ljava/lang/String;
    const/4 v4, 0x3

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1744
    .local v17, "editUriField":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v32

    .line 1745
    .local v32, "version":J
    const-wide/16 v6, 0x1

    add-long v20, v32, v6

    .line 1746
    .local v20, "newVersion":J
    const/4 v4, 0x5

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v26

    .line 1747
    .local v26, "photoFileId":J
    const/4 v4, 0x6

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v28

    .line 1749
    .local v28, "starred":J
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1750
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EDIT_URI"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " STARRED "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v28

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1754
    :cond_0
    invoke-static/range {v17 .. v17}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 1756
    .local v16, "editUriArray":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v4, v16, v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1757
    const-wide/16 v6, 0x1

    cmp-long v4, v28, v6

    if-eqz v4, :cond_1

    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->SYNC_HIGH_RES:Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v6, v16, v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1759
    :cond_1
    const/4 v4, 0x1

    aget-object v15, v16, v4

    .line 1767
    .local v15, "editUri":Ljava/lang/String;
    :goto_1
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1768
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "need to download photo row id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for contact row id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1772
    :cond_2
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777
    .local v22, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_2
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V
    :try_end_2
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1780
    .local v31, "values":Landroid/content/ContentValues;
    const/4 v4, 0x0

    :try_start_3
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {v0, v15, v1, v4}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->getMediaEntryAsStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v19

    .line 1782
    .local v19, "inputStream":Ljava/io/InputStream;
    invoke-static/range {v19 .. v19}, Lcom/google/android/syncadapters/contacts/ContactsUtils;->inputStreamToByteArray(Ljava/io/InputStream;)[B
    :try_end_3
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v23

    .line 1788
    .end local v19    # "inputStream":Ljava/io/InputStream;
    .local v23, "photoData":[B
    :goto_2
    :try_start_4
    const-string v4, "data15"

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1789
    const-string v4, "data_sync3"

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1790
    const-string v4, "data_version"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1791
    const-string v4, "data_sync4"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1793
    move-object/from16 v0, v30

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data_version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1798
    add-int/lit8 v11, v11, 0x1

    .line 1814
    .end local v23    # "photoData":[B
    .end local v31    # "values":Landroid/content/ContentValues;
    :goto_3
    :try_start_5
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1817
    .end local v12    # "contactId":J
    .end local v15    # "editUri":Ljava/lang/String;
    .end local v16    # "editUriArray":[Ljava/lang/String;
    .end local v17    # "editUriField":Ljava/lang/String;
    .end local v18    # "etag":Ljava/lang/String;
    .end local v20    # "newVersion":J
    .end local v22    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v24    # "photoDataRowId":J
    .end local v26    # "photoFileId":J
    .end local v28    # "starred":J
    .end local v32    # "version":J
    :catchall_0
    move-exception v4

    :try_start_6
    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v4
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1819
    .end local v10    # "c":Landroid/database/AbstractWindowedCursor;
    :catch_0
    move-exception v14

    .line 1820
    .local v14, "e":Landroid/os/RemoteException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error writing photo data into provider"

    invoke-static {v4, v6, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1821
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v4, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v4, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1829
    .end local v14    # "e":Landroid/os/RemoteException;
    :goto_4
    return v11

    .line 1761
    .restart local v10    # "c":Landroid/database/AbstractWindowedCursor;
    .restart local v12    # "contactId":J
    .restart local v16    # "editUriArray":[Ljava/lang/String;
    .restart local v17    # "editUriField":Ljava/lang/String;
    .restart local v18    # "etag":Ljava/lang/String;
    .restart local v20    # "newVersion":J
    .restart local v24    # "photoDataRowId":J
    .restart local v26    # "photoFileId":J
    .restart local v28    # "starred":J
    .restart local v32    # "version":J
    :cond_3
    const/4 v4, 0x0

    :try_start_7
    aget-object v15, v16, v4

    .restart local v15    # "editUri":Ljava/lang/String;
    goto/16 :goto_1

    .line 1764
    .end local v15    # "editUri":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    aget-object v15, v16, v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .restart local v15    # "editUri":Ljava/lang/String;
    goto/16 :goto_1

    .line 1783
    .restart local v22    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v31    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v14

    .line 1784
    .local v14, "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    :try_start_8
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ResourceNotFoundException while downloading photo "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", treating it as an empty photo"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1786
    const/16 v23, 0x0

    .restart local v23    # "photoData":[B
    goto/16 :goto_2

    .line 1799
    .end local v14    # "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    .end local v23    # "photoData":[B
    .end local v31    # "values":Landroid/content/ContentValues;
    :catch_2
    move-exception v14

    .line 1800
    .local v14, "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :try_start_9
    throw v14

    .line 1801
    .end local v14    # "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :catch_3
    move-exception v14

    .line 1804
    .local v14, "e":Lcom/google/wireless/gdata2/GDataException;
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "error downloading photo "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1805
    move-object/from16 v0, v30

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync3"

    move-object/from16 v0, v18

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_version"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync4"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data_version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_3

    .line 1817
    .end local v12    # "contactId":J
    .end local v14    # "e":Lcom/google/wireless/gdata2/GDataException;
    .end local v15    # "editUri":Ljava/lang/String;
    .end local v16    # "editUriArray":[Ljava/lang/String;
    .end local v17    # "editUriField":Ljava/lang/String;
    .end local v18    # "etag":Ljava/lang/String;
    .end local v20    # "newVersion":J
    .end local v22    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v24    # "photoDataRowId":J
    .end local v26    # "photoFileId":J
    .end local v28    # "starred":J
    .end local v32    # "version":J
    :cond_5
    :try_start_a
    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->close()V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_4

    .line 1822
    .end local v10    # "c":Landroid/database/AbstractWindowedCursor;
    :catch_4
    move-exception v14

    .line 1823
    .local v14, "e":Landroid/content/OperationApplicationException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error writing photo data into provider"

    invoke-static {v4, v6, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1824
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v4, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v4, Landroid/content/SyncStats;->numParseExceptions:J

    goto/16 :goto_4

    .line 1825
    .end local v14    # "e":Landroid/content/OperationApplicationException;
    :catch_5
    move-exception v14

    .line 1826
    .local v14, "e":Ljava/io/IOException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "IOException downloading photo"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1827
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v4, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_4
.end method

.method private static dumpDataValues(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 5
    .param p0, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2151
    .local p1, "valuesCollection":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/content/ContentValues;>;"
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dataValues for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2152
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 2153
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2155
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method public static editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "editUriArray"    # [Ljava/lang/String;

    .prologue
    .line 2357
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2359
    .local v0, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2360
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2361
    const/4 v1, 0x1

    aget-object v1, p0, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2362
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2363
    const/4 v1, 0x2

    aget-object v1, p0, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2365
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static editUriToArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2372
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v5

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    .line 2373
    .local v1, "editUriArray":[Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2390
    :cond_0
    :goto_0
    return-object v1

    .line 2377
    :cond_1
    const-string v2, " "

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2379
    .local v0, "editUri":[Ljava/lang/String;
    array-length v2, v0

    if-lez v2, :cond_2

    .line 2380
    aget-object v2, v0, v5

    aput-object v2, v1, v5

    .line 2383
    :cond_2
    array-length v2, v0

    if-le v2, v3, :cond_3

    .line 2384
    aget-object v2, v0, v3

    aput-object v2, v1, v3

    .line 2387
    :cond_3
    array-length v2, v0

    if-le v2, v4, :cond_0

    .line 2388
    aget-object v2, v0, v4

    aput-object v2, v1, v4

    goto :goto_0
.end method

.method private static formatPhoneNumber(Ljava/lang/String;Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    .locals 4
    .param p0, "phoneNumber"    # Ljava/lang/String;
    .param p1, "newFormat"    # Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    .prologue
    .line 1265
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v2

    .line 1267
    .local v2, "phoneUtil":Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    :try_start_0
    const-string v3, "ZZ"

    invoke-virtual {v2, p0, v3}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 1268
    .local v1, "number":Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v2, v1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->isValidNumber(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v1, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->format(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v3

    .line 1270
    .end local v1    # "number":Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :goto_0
    return-object v3

    .line 1268
    .restart local v1    # "number":Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :cond_0
    const-string v3, ""
    :try_end_0
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1269
    .end local v1    # "number":Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :catch_0
    move-exception v0

    .line 1270
    .local v0, "e":Lcom/android/i18n/phonenumbers/NumberParseException;
    const-string v3, ""

    goto :goto_0
.end method

.method private getNewDataVersion(Landroid/content/ContentValues;)J
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 721
    const-string v2, "data_version"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_0

    const-wide/16 v0, 0x0

    .line 723
    .local v0, "newDataVersion":J
    :goto_0
    return-wide v0

    .line 721
    .end local v0    # "newDataVersion":J
    :cond_0
    const-string v2, "data_version"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    goto :goto_0
.end method

.method static markLocalHiresPhotosAsDirty(Landroid/accounts/Account;Landroid/content/ContentProviderClient;)V
    .locals 22
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 1679
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    .line 1680
    .local v5, "uriToRead":Landroid/net/Uri;
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v20

    .line 1681
    .local v20, "uriToWrite":Landroid/net/Uri;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1682
    .local v15, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v6, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_METADATA:[Ljava/lang/String;

    const-string v7, "mimetype=? AND data14 IS NOT NULL"

    sget-object v8, Lcom/google/android/syncadapters/contacts/ContactHandler;->SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    check-cast v10, Landroid/database/AbstractWindowedCursor;

    .line 1687
    .local v10, "c":Landroid/database/AbstractWindowedCursor;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1688
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v18

    .line 1689
    .local v18, "photoDataRowId":J
    const/4 v4, 0x3

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1690
    .local v14, "editUriField":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-virtual {v10, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v12

    .line 1691
    .local v12, "dataVersion":J
    const-wide/16 v6, 0x1

    add-long v16, v12, v6

    .line 1693
    .local v16, "newDataVersion":J
    invoke-static {v14}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1694
    .local v11, "editUriArray":[Ljava/lang/String;
    const/4 v4, 0x2

    sget-object v6, Lcom/google/android/syncadapters/contacts/ContactHandler;->SYNC_HIGH_RES:Ljava/lang/String;

    aput-object v6, v11, v4

    .line 1696
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1697
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "marking photo row id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v18

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " dirty so it will be re-uploaded"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    :cond_1
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 1702
    .local v21, "values":Landroid/content/ContentValues;
    const-string v4, "data_version"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1703
    const-string v4, "data_sync1"

    invoke-static {v11}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data_version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1709
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v6, 0x14

    if-le v4, v6, :cond_0

    .line 1710
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 1711
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1715
    .end local v11    # "editUriArray":[Ljava/lang/String;
    .end local v12    # "dataVersion":J
    .end local v14    # "editUriField":Ljava/lang/String;
    .end local v16    # "newDataVersion":J
    .end local v18    # "photoDataRowId":J
    .end local v21    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v4

    :cond_2
    invoke-virtual {v10}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 1717
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1718
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 1719
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 1721
    :cond_3
    return-void
.end method

.method static newBirthdayDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1601
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getBirthday()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1602
    const/4 v0, 0x0

    .line 1608
    :goto_0
    return-object v0

    .line 1604
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1605
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1606
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getBirthday()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    const-string v1, "data2"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private static newContactValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1668
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1669
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "sourceid"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/syncadapters/contacts/ContactsUtils;->lastItemFromUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    const-string v1, "sync1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEditUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    const-string v1, "sync2"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getETag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    const-string v1, "sync3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getUpdateDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1673
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/CalendarLink;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/CalendarLink;

    .prologue
    .line 1360
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1361
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_calendar_link"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_CALENDAR_LINK:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1364
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->getHRef()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1366
    const-string v2, "data4"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->isPrimary()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1367
    return-object v0

    .line 1366
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .prologue
    .line 1471
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1472
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EMAIL:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1475
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    const-string v1, "data4"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    const-string v2, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->isPrimary()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1479
    return-object v0

    .line 1478
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Event;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/Event;

    .prologue
    .line 1494
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1495
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EVENT:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Event;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1498
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Event;->getStartDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Event;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ExternalId;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/ExternalId;

    .prologue
    .line 1381
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1382
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_external_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_EXTERNAL_ID:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ExternalId;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1385
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ExternalId;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ExternalId;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;

    .prologue
    .line 1463
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1464
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1465
    const-string v1, "group_sourceid"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->getGroup()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/syncadapters/contacts/ContactsUtils;->lastItemFromUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ImAddress;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/ImAddress;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1433
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1434
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "mimetype"

    const-string v6, "vnd.android.cursor.item/im"

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getProtocolPredefined()B

    move-result v0

    .line 1437
    .local v0, "protocolType":B
    const/16 v3, 0xb

    if-ne v0, v3, :cond_0

    .line 1439
    const-string v6, "data5"

    move-object v3, v4

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    const-string v3, "data6"

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    :goto_0
    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/im"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454
    const-string v4, "data2"

    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_IM:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getType()B

    move-result v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1456
    const-string v3, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    const-string v3, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    const-string v4, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->isPrimary()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v5

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1459
    return-object v2

    .line 1441
    :cond_0
    if-ne v0, v5, :cond_1

    .line 1443
    const-string v3, "data5"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1444
    const-string v3, "data6"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getProtocolCustom()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1447
    :cond_1
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_IM_PROTOCOL_TO_PROVIDER_PROTOCOL:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1449
    .local v1, "providerProtocolType":Ljava/lang/Integer;
    const-string v3, "data5"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1450
    const-string v3, "data6"

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1458
    .end local v1    # "providerProtocolType":Ljava/lang/Integer;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Jot;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/Jot;

    .prologue
    .line 1391
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1392
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_jot"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_JOT:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Jot;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1394
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Jot;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Language;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/Language;

    .prologue
    .line 1334
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1335
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_language"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    const-string v1, "data2"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Language;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Language;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Organization;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/Organization;

    .prologue
    .line 1399
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1400
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/organization"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_ORGANIZATION:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1403
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    const-string v1, "data4"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    const-string v1, "data5"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getOrgDepartment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    const-string v1, "data6"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getOrgJobDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1408
    const-string v1, "data7"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getOrgSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    const-string v1, "data8"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->getNameYomi()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    const-string v2, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Organization;->isPrimary()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1411
    return-object v0

    .line 1410
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;)Landroid/content/ContentValues;
    .locals 6
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .prologue
    .line 1342
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1343
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    const-string v4, "data2"

    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_PHONE:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getType()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1346
    const-string v3, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getLinksTo()Ljava/lang/String;

    move-result-object v1

    .line 1348
    .local v1, "uri":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1349
    sget-object v3, Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->E164:Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-static {v1, v3}, Lcom/google/android/syncadapters/contacts/ContactHandler;->formatPhoneNumber(Ljava/lang/String;Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 1350
    .local v0, "normalizedNumber":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1351
    const-string v3, "data4"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    .end local v0    # "normalizedNumber":Ljava/lang/String;
    :cond_0
    const-string v3, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    const-string v4, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->isPrimary()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1356
    return-object v2

    .line 1355
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Relation;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/Relation;

    .prologue
    .line 1504
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1505
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/relation"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_RELATION:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Relation;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1508
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Relation;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1509
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/Relation;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1510
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/SipAddress;)Landroid/content/ContentValues;
    .locals 5
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/SipAddress;

    .prologue
    .line 1514
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1515
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    const-string v3, "data2"

    sget-object v2, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_SIP_ADDRESS:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->getType()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1527
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 1528
    .local v0, "address":Ljava/lang/String;
    const-string v2, "sip:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1529
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1532
    :cond_0
    const-string v2, "data1"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const-string v2, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    const-string v3, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->isPrimary()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1535
    return-object v1

    .line 1534
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;

    .prologue
    .line 1415
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1416
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_POSTAL:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1418
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    const-string v2, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->isPrimary()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1420
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getFormattedAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    const-string v1, "data4"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getStreet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    const-string v1, "data5"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getPobox()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    const-string v1, "data6"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getNeighborhood()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    const-string v1, "data7"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    const-string v1, "data9"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getPostcode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v1, "data8"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getRegion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    const-string v1, "data10"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    return-object v0

    .line 1419
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;

    .prologue
    .line 1326
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1327
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_user_defined_field"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const-string v1, "data2"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330
    return-object v0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/WebSite;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/contacts/data/WebSite;

    .prologue
    .line 1483
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1484
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/website"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    const-string v2, "data2"

    sget-object v1, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TYPE_TO_PROVIDER_WEBSITE:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getType()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1487
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getHRef()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    const-string v1, "data3"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    const-string v2, "is_primary"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->isPrimary()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1490
    return-object v0

    .line 1489
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/data/ExtendedProperty;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "serverItem"    # Lcom/google/wireless/gdata2/data/ExtendedProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/gdata2/data/ExtendedProperty;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1540
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1541
    .local v6, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/data/ExtendedProperty;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1542
    .local v1, "jsonData":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 1543
    const-string v7, "ContactsSyncAdapter"

    const-string v8, "dropping unparsable extended property: missing value"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1566
    :cond_0
    :goto_0
    return-object v6

    .line 1548
    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1553
    .local v3, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 1554
    .local v2, "jsonIterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1555
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1557
    .local v4, "key":Ljava/lang/String;
    :try_start_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1558
    .local v5, "values":Landroid/content/ContentValues;
    const-string v7, "mimetype"

    const-string v8, "vnd.com.google.cursor.item/contact_extended_property"

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    const-string v7, "data1"

    invoke-virtual {v5, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    const-string v7, "data2"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1562
    .end local v5    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v7

    goto :goto_1

    .line 1549
    .end local v2    # "jsonIterator":Ljava/util/Iterator;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "key":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1550
    .local v0, "e":Lorg/json/JSONException;
    const-string v7, "ContactsSyncAdapter"

    const-string v8, "dropping unparsable extended property: parse error"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static newDataValuesFromHobbyEntryElement(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "serverItem"    # Ljava/lang/String;

    .prologue
    .line 1319
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1320
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.com.google.cursor.item/contact_hobby"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    const-string v1, "data1"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    return-object v0
.end method

.method static newFocusMiscDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 5
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1612
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1613
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "mimetype"

    const-string v3, "vnd.com.google.cursor.item/contact_misc"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    const-string v2, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getBillingInformation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    const-string v2, "data2"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getDirectoryServer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getGender()Ljava/lang/String;

    move-result-object v0

    .line 1617
    .local v0, "gender":Ljava/lang/String;
    const-string v2, "female"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1618
    const-string v2, "data3"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1622
    :cond_0
    :goto_0
    const-string v2, "data4"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getInitials()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1623
    const-string v2, "data5"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getMaidenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    const-string v2, "data6"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getMileage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    const-string v2, "data7"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getOccupation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    const-string v2, "data10"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    const-string v2, "data11"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    const-string v3, "data8"

    sget-object v2, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TO_PROVIDER_PRIORITY:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPriority()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1630
    const-string v3, "data9"

    sget-object v2, Lcom/google/android/syncadapters/contacts/ContactHandler;->ENTRY_TO_PROVIDER_SENSITIVITY:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getSensitivity()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1633
    return-object v1

    .line 1619
    :cond_1
    const-string v2, "male"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1620
    const-string v2, "data3"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method static newIdentityDataValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "linksto"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1371
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1372
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/identity"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    const-string v1, "data1"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1374
    const-string v1, "data2"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1375
    const-string v1, "data_sync3"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    return-object v0
.end method

.method static newNameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1570
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1571
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "mimetype"

    const-string v6, "vnd.android.cursor.item/name"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getName()Lcom/google/wireless/gdata2/contacts/data/Name;

    move-result-object v0

    .line 1573
    .local v0, "name":Lcom/google/wireless/gdata2/contacts/data/Name;
    if-eqz v0, :cond_0

    .line 1574
    const-string v5, "data2"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getGivenName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    const-string v5, "data3"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFamilyName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1576
    const-string v5, "data4"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getNamePrefix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1577
    const-string v5, "data5"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getAdditionalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    const-string v5, "data6"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getNameSuffix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getGivenNameYomi()Ljava/lang/String;

    move-result-object v2

    .line 1580
    .local v2, "phoneticGivenName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getAdditionalNameYomi()Ljava/lang/String;

    move-result-object v3

    .line 1581
    .local v3, "phoneticMiddleName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFamilyNameYomi()Ljava/lang/String;

    move-result-object v1

    .line 1582
    .local v1, "phoneticFamilyName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1587
    const-string v5, "data7"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFullNameYomi()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    const-string v5, "data8"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1589
    const-string v5, "data9"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1595
    :goto_0
    const-string v5, "data1"

    invoke-virtual {v0}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFullName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    .end local v1    # "phoneticFamilyName":Ljava/lang/String;
    .end local v2    # "phoneticGivenName":Ljava/lang/String;
    .end local v3    # "phoneticMiddleName":Ljava/lang/String;
    :cond_0
    return-object v4

    .line 1591
    .restart local v1    # "phoneticFamilyName":Ljava/lang/String;
    .restart local v2    # "phoneticGivenName":Ljava/lang/String;
    .restart local v3    # "phoneticMiddleName":Ljava/lang/String;
    :cond_1
    const-string v5, "data7"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1592
    const-string v5, "data8"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1593
    const-string v5, "data9"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static newNicknameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1637
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1638
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    const-string v1, "data1"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getNickname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1640
    return-object v0
.end method

.method static newNoteDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1644
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1645
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/note"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getContent()Ljava/lang/String;

    move-result-object v0

    .line 1647
    .local v0, "note":Ljava/lang/String;
    const-string v2, "data1"

    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "note":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    return-object v1
.end method

.method private static newPhotoDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .prologue
    .line 1652
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1653
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/photo"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    const-string v2, "data_sync2"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLinkPhotoETag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1657
    .local v0, "editUriArray":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLinkPhotoHref()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1659
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLinkPhotoHrefHighRes()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1660
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLinkPhotoHrefHighRes()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1663
    :cond_0
    const-string v2, "data_sync1"

    invoke-static {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1664
    return-object v1
.end method

.method private static providerTypeToEntryType(Ljava/lang/Integer;Ljava/util/HashMap;I)B
    .locals 2
    .param p0, "typeInProvider"    # Ljava/lang/Integer;
    .param p2, "defaultProviderType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;I)B"
        }
    .end annotation

    .prologue
    .line 843
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Byte;>;"
    const/4 v0, 0x0

    .line 844
    .local v0, "typeInEntry":Ljava/lang/Byte;
    if-eqz p0, :cond_0

    .line 845
    invoke-virtual {p1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "typeInEntry":Ljava/lang/Byte;
    check-cast v0, Ljava/lang/Byte;

    .line 847
    .restart local v0    # "typeInEntry":Ljava/lang/Byte;
    :cond_0
    if-nez v0, :cond_1

    .line 848
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "typeInEntry":Ljava/lang/Byte;
    check-cast v0, Ljava/lang/Byte;

    .line 850
    .restart local v0    # "typeInEntry":Ljava/lang/Byte;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    return v1
.end method

.method private static setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 5
    .param p0, "item"    # Lcom/google/wireless/gdata2/contacts/data/TypedElement;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "typeColumn"    # Ljava/lang/String;
    .param p4, "labelColumn"    # Ljava/lang/String;
    .param p5, "defaultType"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/gdata2/contacts/data/TypedElement;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "B)V"
        }
    .end annotation

    .prologue
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Byte;>;"
    const/4 v4, -0x1

    .line 1276
    invoke-virtual {p2, p4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1277
    .local v0, "label":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1278
    invoke-virtual {p0, v0}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;->setLabel(Ljava/lang/String;)V

    .line 1279
    invoke-virtual {p0, v4}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;->setType(B)V

    .line 1292
    :goto_0
    return-void

    .line 1282
    :cond_0
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 1283
    .local v2, "typeInProvider":Ljava/lang/Integer;
    const/4 v1, 0x0

    .line 1284
    .local v1, "typeInEntry":Ljava/lang/Byte;
    if-eqz v2, :cond_1

    .line 1285
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "typeInEntry":Ljava/lang/Byte;
    check-cast v1, Ljava/lang/Byte;

    .line 1287
    .restart local v1    # "typeInEntry":Ljava/lang/Byte;
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-ne v3, v4, :cond_3

    .line 1288
    :cond_2
    invoke-static {p5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    .line 1290
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;->setType(B)V

    .line 1291
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/wireless/gdata2/contacts/data/TypedElement;->setLabel(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static swapMap(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/HashMap",
            "<TA;TB;>;)",
            "Ljava/util/HashMap",
            "<TB;TA;>;"
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "originalMap":Ljava/util/HashMap;, "Ljava/util/HashMap<TA;TB;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 219
    .local v2, "newMap":Ljava/util/HashMap;, "Ljava/util/HashMap<TB;TA;>;"
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 220
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TA;TB;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 221
    .local v3, "originalValue":Ljava/lang/Object;, "TB;"
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 222
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was already encountered"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 225
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TA;TB;>;"
    .end local v3    # "originalValue":Ljava/lang/Object;, "TB;"
    :cond_1
    return-object v2
.end method

.method private updateDataVersions(Landroid/content/ContentValues;J)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "newDataVersion"    # J

    .prologue
    .line 727
    const-string v0, "data_sync4"

    const-wide/16 v2, 0xa

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 728
    const-string v0, "data_version"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 729
    return-void
.end method

.method static uploadPhotos(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/content/SyncResult;Lcom/google/wireless/gdata2/contacts/client/ContactsClient;Landroid/content/ContentResolver;)I
    .locals 32
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "authToken"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;
    .param p4, "client"    # Lcom/google/wireless/gdata2/contacts/client/ContactsClient;
    .param p5, "contentResolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/client/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 1840
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    .line 1841
    .local v5, "uriToRead":Landroid/net/Uri;
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v25

    .line 1842
    .local v25, "uriToWrite":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 1844
    .local v13, "count":I
    :try_start_0
    sget-object v6, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_FULLDATA:[Ljava/lang/String;

    const-string v7, "mimetype=? AND (data_sync4 IS NULL OR data_sync4!=data_version) AND (data15 IS NOT NULL OR data_sync2 IS NOT NULL) AND data_sync1 IS NOT NULL"

    sget-object v8, Lcom/google/android/syncadapters/contacts/ContactHandler;->SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    check-cast v12, Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1848
    .local v12, "c":Landroid/database/AbstractWindowedCursor;
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v12}, Landroid/database/AbstractWindowedCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1849
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v22

    .line 1850
    .local v22, "photoDataRowId":J
    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v14

    .line 1851
    .local v14, "contactId":J
    const/4 v4, 0x2

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1852
    .local v11, "etag":Ljava/lang/String;
    const/4 v4, 0x3

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1853
    .local v17, "editUriField":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v26

    .line 1854
    .local v26, "version":J
    const-wide/16 v28, 0x1

    add-long v20, v26, v28

    .line 1855
    .local v20, "newVersion":J
    const/16 v4, 0x8

    invoke-virtual {v12, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 1856
    .local v24, "photoUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1858
    .local v8, "photoStream":Ljava/io/FileInputStream;
    invoke-static/range {v17 .. v17}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    aget-object v7, v4, v6

    .line 1861
    .local v7, "uploadUri":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 1865
    if-eqz v24, :cond_1

    .line 1867
    :try_start_2
    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "r"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v18

    .line 1869
    .local v18, "fd":Landroid/content/res/AssetFileDescriptor;
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    .line 1875
    .end local v18    # "fd":Landroid/content/res/AssetFileDescriptor;
    :cond_1
    :goto_1
    :try_start_3
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1876
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Photo URI "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1879
    :cond_2
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1883
    .local v19, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v8, :cond_4

    .line 1884
    :try_start_4
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1885
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uploading photo row id "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " for contact row id "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " to "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1888
    :cond_3
    const-string v9, "image/*"

    move-object/from16 v6, p4

    move-object/from16 v10, p2

    invoke-virtual/range {v6 .. v11}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->updateMediaEntry(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/MediaEntry;

    .line 1900
    :goto_2
    move-object/from16 v0, v25

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync3"

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync2"

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_version"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync4"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "data_version="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1908
    add-int/lit8 v13, v13, 0x1

    .line 1926
    :goto_3
    :try_start_5
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1929
    .end local v7    # "uploadUri":Ljava/lang/String;
    .end local v8    # "photoStream":Ljava/io/FileInputStream;
    .end local v11    # "etag":Ljava/lang/String;
    .end local v14    # "contactId":J
    .end local v17    # "editUriField":Ljava/lang/String;
    .end local v19    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v20    # "newVersion":J
    .end local v22    # "photoDataRowId":J
    .end local v24    # "photoUri":Ljava/lang/String;
    .end local v26    # "version":J
    :catchall_0
    move-exception v4

    :try_start_6
    invoke-virtual {v12}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v4
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1931
    .end local v12    # "c":Landroid/database/AbstractWindowedCursor;
    :catch_0
    move-exception v16

    .line 1932
    .local v16, "e":Landroid/os/RemoteException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error writing photo metadata into provider"

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1933
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v4, Landroid/content/SyncStats;->numParseExceptions:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x1

    add-long v28, v28, v30

    move-wide/from16 v0, v28

    iput-wide v0, v4, Landroid/content/SyncStats;->numParseExceptions:J

    .line 1941
    .end local v16    # "e":Landroid/os/RemoteException;
    :goto_4
    return v13

    .line 1891
    .restart local v7    # "uploadUri":Ljava/lang/String;
    .restart local v8    # "photoStream":Ljava/io/FileInputStream;
    .restart local v11    # "etag":Ljava/lang/String;
    .restart local v12    # "c":Landroid/database/AbstractWindowedCursor;
    .restart local v14    # "contactId":J
    .restart local v17    # "editUriField":Ljava/lang/String;
    .restart local v19    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v20    # "newVersion":J
    .restart local v22    # "photoDataRowId":J
    .restart local v24    # "photoUri":Ljava/lang/String;
    .restart local v26    # "version":J
    :cond_4
    :try_start_7
    const-string v4, "ContactsSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1892
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "deleting photo row id "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " for contact row id "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " to "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1895
    :cond_5
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {v0, v7, v1, v11}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->deleteEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 1909
    :catch_1
    move-exception v16

    .line 1910
    .local v16, "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :try_start_8
    throw v16

    .line 1911
    .end local v16    # "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :catch_2
    move-exception v16

    .line 1915
    .local v16, "e":Lcom/google/wireless/gdata2/GDataException;
    const-string v4, "ContactsSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "error uploading photo "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", disabling uploads "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "until it changes again"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1918
    move-object/from16 v0, v25

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_version"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data_sync4"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "data_version="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 1929
    .end local v7    # "uploadUri":Ljava/lang/String;
    .end local v8    # "photoStream":Ljava/io/FileInputStream;
    .end local v11    # "etag":Ljava/lang/String;
    .end local v14    # "contactId":J
    .end local v16    # "e":Lcom/google/wireless/gdata2/GDataException;
    .end local v17    # "editUriField":Ljava/lang/String;
    .end local v19    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v20    # "newVersion":J
    .end local v22    # "photoDataRowId":J
    .end local v24    # "photoUri":Ljava/lang/String;
    .end local v26    # "version":J
    :cond_6
    :try_start_9
    invoke-virtual {v12}, Landroid/database/AbstractWindowedCursor;->close()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_4

    .line 1934
    .end local v12    # "c":Landroid/database/AbstractWindowedCursor;
    :catch_3
    move-exception v16

    .line 1935
    .local v16, "e":Landroid/content/OperationApplicationException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "error writing photo metadata into provider"

    move-object/from16 v0, v16

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1936
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v4, Landroid/content/SyncStats;->numParseExceptions:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x1

    add-long v28, v28, v30

    move-wide/from16 v0, v28

    iput-wide v0, v4, Landroid/content/SyncStats;->numParseExceptions:J

    goto/16 :goto_4

    .line 1937
    .end local v16    # "e":Landroid/content/OperationApplicationException;
    :catch_4
    move-exception v16

    .line 1938
    .local v16, "e":Ljava/io/IOException;
    const-string v4, "ContactsSyncAdapter"

    const-string v6, "IOException while uploading photo"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v4, Landroid/content/SyncStats;->numIoExceptions:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x1

    add-long v28, v28, v30

    move-wide/from16 v0, v28

    iput-wide v0, v4, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_4

    .line 1870
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v7    # "uploadUri":Ljava/lang/String;
    .restart local v8    # "photoStream":Ljava/io/FileInputStream;
    .restart local v11    # "etag":Ljava/lang/String;
    .restart local v12    # "c":Landroid/database/AbstractWindowedCursor;
    .restart local v14    # "contactId":J
    .restart local v17    # "editUriField":Ljava/lang/String;
    .restart local v20    # "newVersion":J
    .restart local v22    # "photoDataRowId":J
    .restart local v24    # "photoUri":Ljava/lang/String;
    .restart local v26    # "version":J
    :catch_5
    move-exception v4

    goto/16 :goto_1
.end method

.method public static validateDate(Landroid/text/format/Time;Ljava/lang/String;Z)Z
    .locals 5
    .param p0, "validator"    # Landroid/text/format/Time;
    .param p1, "date"    # Ljava/lang/String;
    .param p2, "isBirthday"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 855
    if-nez p1, :cond_1

    .line 864
    :cond_0
    :goto_0
    return v1

    .line 858
    :cond_1
    if-eqz p2, :cond_2

    :try_start_0
    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 859
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2000"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 861
    :cond_2
    invoke-virtual {p0, p1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 862
    if-eqz p2, :cond_3

    iget-boolean v3, p0, Landroid/text/format/Time;->allDay:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 863
    :catch_0
    move-exception v0

    .line 864
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method


# virtual methods
.method public applyEntryToEntity(Ljava/util/ArrayList;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/Set;Lcom/google/wireless/gdata2/data/Entry;Landroid/content/Entity;ZLandroid/content/SyncResult;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/Object;)V
    .locals 68
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "client"    # Landroid/content/ContentProviderClient;
    .param p5, "entry"    # Lcom/google/wireless/gdata2/data/Entry;
    .param p6, "entity"    # Landroid/content/Entity;
    .param p7, "clearDirtyFlag"    # Z
    .param p8, "syncResult"    # Landroid/content/SyncResult;
    .param p9, "contactUriWithAccount"    # Landroid/net/Uri;
    .param p10, "dataUriWithAccount"    # Landroid/net/Uri;
    .param p11, "groupUri"    # Landroid/net/Uri;
    .param p12, "extra"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentProviderClient;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            "Landroid/content/Entity;",
            "Z",
            "Landroid/content/SyncResult;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, "contactsSyncSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v4, "ContactsSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v64

    .line 270
    .local v64, "verboseLogging":Z
    if-nez v64, :cond_0

    const-string v4, "ContactsSyncAdapterFine"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_0
    const/16 v40, 0x1

    .line 272
    .local v40, "fineOrVerboseLogging":Z
    :goto_0
    if-eqz v64, :cond_1

    .line 273
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "entry is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "entity is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_1
    const/16 v28, 0x0

    .line 278
    .local v28, "contactEntry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    if-eqz p5, :cond_2

    move-object/from16 v28, p5

    .line 279
    check-cast v28, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .line 283
    :cond_2
    if-nez p4, :cond_8

    const/16 v45, 0x1

    .line 284
    .local v45, "isInSyncSet":Z
    :goto_1
    if-nez v45, :cond_4

    .line 285
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getGroups()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    .local v43, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .local v56, "object":Ljava/lang/Object;
    move-object/from16 v41, v56

    .line 286
    check-cast v41, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;

    .line 287
    .local v41, "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    invoke-virtual/range {v41 .. v41}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->isDeleted()Z

    move-result v4

    if-nez v4, :cond_3

    .line 290
    :try_start_0
    invoke-virtual/range {v41 .. v41}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->getGroup()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v42

    .line 296
    .local v42, "groupSourceId":Ljava/lang/String;
    move-object/from16 v0, p4

    move-object/from16 v1, v42

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 297
    const/16 v45, 0x1

    .line 303
    .end local v41    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .end local v42    # "groupSourceId":Ljava/lang/String;
    .end local v43    # "i$":Ljava/util/Iterator;
    .end local v56    # "object":Ljava/lang/Object;
    :cond_4
    if-eqz v28, :cond_5

    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->isDeleted()Z

    move-result v4

    if-nez v4, :cond_5

    if-nez v45, :cond_9

    :cond_5
    const/16 v44, 0x1

    .line 304
    .local v44, "isDelete":Z
    :goto_3
    if-nez v44, :cond_a

    if-nez p6, :cond_a

    const/16 v46, 0x1

    .line 305
    .local v46, "isInsert":Z
    :goto_4
    if-nez v44, :cond_b

    if-eqz p6, :cond_b

    const/16 v47, 0x1

    .line 307
    .local v47, "isUpdate":Z
    :goto_5
    if-eqz v44, :cond_c

    .line 308
    if-eqz p6, :cond_6

    .line 309
    invoke-virtual/range {p6 .. p6}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v7, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p9

    invoke-static {v0, v1, v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addDeleteOperation(Ljava/util/ArrayList;Landroid/net/Uri;JZ)V

    .line 312
    move-object/from16 v0, p8

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v4, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v4, Landroid/content/SyncStats;->numDeletes:J

    .line 698
    :cond_6
    :goto_6
    return-void

    .line 270
    .end local v28    # "contactEntry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .end local v40    # "fineOrVerboseLogging":Z
    .end local v44    # "isDelete":Z
    .end local v45    # "isInSyncSet":Z
    .end local v46    # "isInsert":Z
    .end local v47    # "isUpdate":Z
    :cond_7
    const/16 v40, 0x0

    goto/16 :goto_0

    .line 283
    .restart local v28    # "contactEntry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .restart local v40    # "fineOrVerboseLogging":Z
    :cond_8
    const/16 v45, 0x0

    goto :goto_1

    .line 291
    .restart local v41    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .restart local v43    # "i$":Ljava/util/Iterator;
    .restart local v45    # "isInSyncSet":Z
    .restart local v56    # "object":Ljava/lang/Object;
    :catch_0
    move-exception v31

    .line 294
    .local v31, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_2

    .line 303
    .end local v31    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v41    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .end local v43    # "i$":Ljava/util/Iterator;
    .end local v56    # "object":Ljava/lang/Object;
    :cond_9
    const/16 v44, 0x0

    goto :goto_3

    .line 304
    .restart local v44    # "isDelete":Z
    :cond_a
    const/16 v46, 0x0

    goto :goto_4

    .line 305
    .restart local v46    # "isInsert":Z
    :cond_b
    const/16 v47, 0x0

    goto :goto_5

    .line 316
    .restart local v47    # "isUpdate":Z
    :cond_c
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newContactValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v6

    .line 317
    .local v6, "contactsValuesServer":Landroid/content/ContentValues;
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 319
    .local v30, "dataValuesListServer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNoteDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNicknameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newBirthdayDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v27

    .line 323
    .local v27, "birthdayDataValues":Landroid/content/ContentValues;
    if-eqz v27, :cond_d

    .line 324
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    :cond_d
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newFocusMiscDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEmailAddresses()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    .restart local v43    # "i$":Ljava/util/Iterator;
    :cond_e
    :goto_7
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .restart local v56    # "object":Ljava/lang/Object;
    move-object/from16 v35, v56

    .line 329
    check-cast v35, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .line 330
    .local v35, "emailAddress":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    invoke-static/range {v35 .. v35}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-virtual/range {v35 .. v35}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 332
    invoke-virtual/range {v35 .. v35}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual/range {v35 .. v35}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newIdentityDataValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 337
    .end local v35    # "emailAddress":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    .end local v56    # "object":Ljava/lang/Object;
    :cond_f
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPostalAddresses()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_8
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 338
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 342
    :cond_10
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getImAddresses()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_9
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 343
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/ImAddress;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ImAddress;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 346
    :cond_11
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getOrganizations()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_a
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 347
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/Organization;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Organization;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 350
    :cond_12
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getGroups()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_13
    :goto_b
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .restart local v56    # "object":Ljava/lang/Object;
    move-object/from16 v41, v56

    .line 351
    check-cast v41, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;

    .line 352
    .restart local v41    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    invoke-virtual/range {v41 .. v41}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->isDeleted()Z

    move-result v4

    if-nez v4, :cond_13

    .line 354
    invoke-static/range {v41 .. v41}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;)Landroid/content/ContentValues;

    move-result-object v63

    .line 355
    .local v63, "values":Landroid/content/ContentValues;
    move-object/from16 v0, v30

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 358
    .end local v41    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .end local v56    # "object":Ljava/lang/Object;
    .end local v63    # "values":Landroid/content/ContentValues;
    :cond_14
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getHobbies()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_c
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 359
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Ljava/lang/String;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromHobbyEntryElement(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 362
    :cond_15
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getUserDefinedFields()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_d
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 363
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 366
    :cond_16
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLanguages()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_e
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 367
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/Language;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Language;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 370
    :cond_17
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getExternalIds()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_f
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 371
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/ExternalId;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ExternalId;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 374
    :cond_18
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getCalendarLinks()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_10
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_19

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 375
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/CalendarLink;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 378
    :cond_19
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getJots()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_11
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1a

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 379
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/Jot;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Jot;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 382
    :cond_1a
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPhoneNumbers()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_1b
    :goto_12
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .restart local v56    # "object":Ljava/lang/Object;
    move-object/from16 v55, v56

    .line 383
    check-cast v55, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .line 384
    .local v55, "number":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    invoke-static/range {v55 .. v55}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    invoke-virtual/range {v55 .. v55}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1b

    .line 386
    invoke-virtual/range {v55 .. v55}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual/range {v55 .. v55}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newIdentityDataValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 391
    .end local v55    # "number":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    .end local v56    # "object":Ljava/lang/Object;
    :cond_1c
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getWebSites()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_1d
    :goto_13
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1e

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .restart local v56    # "object":Ljava/lang/Object;
    move-object/from16 v59, v56

    .line 392
    check-cast v59, Lcom/google/wireless/gdata2/contacts/data/WebSite;

    .line 393
    .local v59, "site":Lcom/google/wireless/gdata2/contacts/data/WebSite;
    invoke-static/range {v59 .. v59}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/WebSite;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    invoke-virtual/range {v59 .. v59}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1d

    .line 395
    invoke-virtual/range {v59 .. v59}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getLinksTo()Ljava/lang/String;

    move-result-object v4

    const-string v5, "vnd.android.cursor.item/website"

    invoke-virtual/range {v59 .. v59}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getHRef()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newIdentityDataValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 400
    .end local v56    # "object":Ljava/lang/Object;
    .end local v59    # "site":Lcom/google/wireless/gdata2/contacts/data/WebSite;
    :cond_1e
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getRelations()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_14
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1f

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 401
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/Relation;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Relation;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 404
    :cond_1f
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEvents()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_15
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_20

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 405
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/Event;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Event;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 408
    :cond_20
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getSipAddresses()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_16
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .line 409
    .restart local v56    # "object":Ljava/lang/Object;
    check-cast v56, Lcom/google/wireless/gdata2/contacts/data/SipAddress;

    .end local v56    # "object":Ljava/lang/Object;
    invoke-static/range {v56 .. v56}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/SipAddress;)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_16

    .line 412
    :cond_21
    invoke-virtual/range {v28 .. v28}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getExtendedProperties()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_22
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v56

    .restart local v56    # "object":Ljava/lang/Object;
    move-object/from16 v39, v56

    .line 413
    check-cast v39, Lcom/google/wireless/gdata2/data/ExtendedProperty;

    .line 414
    .local v39, "extendedProperty":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    const-string v4, "android"

    invoke-virtual/range {v39 .. v39}, Lcom/google/wireless/gdata2/data/ExtendedProperty;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 415
    invoke-static/range {v39 .. v39}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/data/ExtendedProperty;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_17
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v62

    check-cast v62, Landroid/content/ContentValues;

    .line 416
    .local v62, "v":Landroid/content/ContentValues;
    move-object/from16 v0, v30

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_17

    .line 422
    .end local v39    # "extendedProperty":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    .end local v56    # "object":Ljava/lang/Object;
    .end local v62    # "v":Landroid/content/ContentValues;
    :cond_23
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newPhotoDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v20

    .line 425
    .local v20, "serverPhotoValues":Landroid/content/ContentValues;
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 429
    .local v29, "dataValuesListExisting":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    if-eqz v46, :cond_28

    .line 430
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getSourceIdColumnName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "sourceid"

    invoke-virtual {v6, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-static {v0, v4, v5}, Lcom/google/android/syncadapters/contacts/ContactsUtils;->newEntityDoesNotExistAssert(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 434
    .local v11, "contactBackRefIfInsert":Ljava/lang/Integer;
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v4, p1

    move-object/from16 v5, p9

    invoke-static/range {v4 .. v9}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addInsertOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 436
    const/4 v10, 0x0

    .line 439
    .local v10, "contactIdIfUpdate":Ljava/lang/Long;
    const-string v4, "data_version"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    const-string v4, "data_sync4"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 441
    const/4 v12, 0x0

    move-object/from16 v7, p1

    move-object/from16 v8, p10

    move-object/from16 v9, v20

    invoke-static/range {v7 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addInsertOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 444
    move-object/from16 v0, p8

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v4, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v4, Landroid/content/SyncStats;->numInserts:J

    .line 577
    :cond_24
    :goto_18
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_25

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_25

    .line 578
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_EXACT_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v30

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 579
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_EXACT_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v29

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 582
    :cond_25
    if-eqz v64, :cond_26

    .line 583
    const-string v4, "existing (pre cleanup)"

    move-object/from16 v0, v29

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->dumpDataValues(Ljava/lang/String;Ljava/util/Collection;)V

    .line 584
    const-string v4, "server (pre cleanup)"

    move-object/from16 v0, v30

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->dumpDataValues(Ljava/lang/String;Ljava/util/Collection;)V

    .line 587
    :cond_26
    new-instance v53, Ljava/util/ArrayList;

    invoke-direct/range {v53 .. v53}, Ljava/util/ArrayList;-><init>()V

    .line 588
    .local v53, "newExisting":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    new-instance v54, Ljava/util/ArrayList;

    invoke-direct/range {v54 .. v54}, Ljava/util/ArrayList;-><init>()V

    .line 590
    .local v54, "newServer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    new-instance v48, Lcom/google/android/syncadapters/contacts/IteratorJoiner;

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    sget-object v7, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_EXACT_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v48

    invoke-direct {v0, v4, v5, v7}, Lcom/google/android/syncadapters/contacts/IteratorJoiner;-><init>(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Comparator;)V

    .line 593
    .local v48, "joiner":Lcom/google/android/syncadapters/contacts/IteratorJoiner;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>;"
    invoke-virtual/range {v48 .. v48}, Lcom/google/android/syncadapters/contacts/IteratorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :goto_19
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3a

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v58

    check-cast v58, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;

    .line 594
    .local v58, "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    invoke-virtual/range {v58 .. v58}, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;->getLeft()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Landroid/content/ContentValues;

    .line 595
    .local v38, "existingValues":Landroid/content/ContentValues;
    invoke-virtual/range {v58 .. v58}, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;->getRight()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/ContentValues;

    .line 596
    .local v23, "serverValues":Landroid/content/ContentValues;
    if-eqz v38, :cond_36

    if-eqz v23, :cond_36

    .line 598
    if-eqz v64, :cond_27

    .line 599
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dropping identical data row: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_27
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p10

    move-object/from16 v3, v38

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setExistsOnServerFlag(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto :goto_19

    .line 446
    .end local v10    # "contactIdIfUpdate":Ljava/lang/Long;
    .end local v11    # "contactBackRefIfInsert":Ljava/lang/Integer;
    .end local v23    # "serverValues":Landroid/content/ContentValues;
    .end local v38    # "existingValues":Landroid/content/ContentValues;
    .end local v48    # "joiner":Lcom/google/android/syncadapters/contacts/IteratorJoiner;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>;"
    .end local v53    # "newExisting":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v54    # "newServer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v58    # "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    :cond_28
    const/4 v11, 0x0

    .line 447
    .restart local v11    # "contactBackRefIfInsert":Ljava/lang/Integer;
    invoke-virtual/range {p6 .. p6}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    .line 448
    .restart local v10    # "contactIdIfUpdate":Ljava/lang/Long;
    invoke-virtual/range {p6 .. p6}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "version"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 449
    .local v16, "contactVersion":J
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getVersionColumnName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v12, p9

    invoke-static/range {v12 .. v17}, Lcom/google/android/syncadapters/contacts/ContactsUtils;->newEntityVersionMatchesAssert(Landroid/net/Uri;JLjava/lang/String;J)Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    const/16 v37, 0x0

    .line 452
    .local v37, "existingPhotoValues":Landroid/content/ContentValues;
    invoke-virtual/range {p6 .. p6}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_29
    :goto_1a
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2c

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Landroid/content/Entity$NamedContentValues;

    .line 453
    .local v52, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v52

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v61, v0

    .line 454
    .local v61, "uri":Landroid/net/Uri;
    move-object/from16 v0, v52

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v63, v0

    .line 455
    .restart local v63    # "values":Landroid/content/ContentValues;
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v61

    invoke-virtual {v4, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 456
    const-string v4, "vnd.android.cursor.item/photo"

    const-string v5, "mimetype"

    move-object/from16 v0, v63

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 457
    if-nez v37, :cond_2a

    .line 458
    move-object/from16 v37, v63

    goto :goto_1a

    .line 461
    :cond_2a
    const-string v4, "_id"

    move-object/from16 v0, v63

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p10

    invoke-static {v0, v1, v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addDeleteOperation(Ljava/util/ArrayList;Landroid/net/Uri;JZ)V

    goto :goto_1a

    .line 467
    :cond_2b
    const-string v4, "mimetype"

    move-object/from16 v0, v63

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 468
    .local v51, "mimetype":Ljava/lang/String;
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->SERVER_MIME_TYPES:Ljava/util/Set;

    move-object/from16 v0, v51

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 469
    move-object/from16 v0, v29

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 475
    .end local v51    # "mimetype":Ljava/lang/String;
    .end local v52    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v61    # "uri":Landroid/net/Uri;
    .end local v63    # "values":Landroid/content/ContentValues;
    :cond_2c
    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v4, p1

    move-object/from16 v5, p9

    move-object v7, v10

    invoke-static/range {v4 .. v9}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addUpdateOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 477
    move-object/from16 v0, p8

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v4, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v4, Landroid/content/SyncStats;->numUpdates:J

    .line 479
    if-nez v37, :cond_2d

    .line 481
    const-string v4, "data_version"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 482
    const-string v4, "data_sync4"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 483
    const/4 v12, 0x0

    move-object/from16 v7, p1

    move-object/from16 v8, p10

    move-object/from16 v9, v20

    invoke-static/range {v7 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addInsertOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_18

    .line 496
    :cond_2d
    const-string v4, "data_sync1"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 497
    .local v32, "editUri":Ljava/lang/String;
    const-string v4, "data_sync2"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 498
    .local v36, "etag":Ljava/lang/String;
    const-string v4, "data_sync1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    .line 500
    .local v49, "localEditUri":Ljava/lang/String;
    const-string v4, "data_sync2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 502
    .local v50, "localEtag":Ljava/lang/String;
    move-object/from16 v0, v32

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2e

    move-object/from16 v0, v36

    move-object/from16 v1, v50

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_24

    .line 504
    :cond_2e
    const-string v4, "data_version"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v66

    .line 507
    .local v66, "version":J
    const-string v4, "data_sync4"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v60

    .line 515
    .local v60, "syncMetaVersion":Ljava/lang/Long;
    if-nez v50, :cond_31

    const-string v4, "data15"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_31

    if-eqz v60, :cond_31

    .line 518
    const-string v4, "data_version"

    const-wide/16 v12, 0x1

    add-long v12, v12, v66

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 520
    const-string v4, "data_sync4"

    const-wide/16 v12, 0x1

    add-long v12, v12, v66

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 521
    const-string v4, "data_sync3"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string v4, "data_sync2"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    .end local v66    # "version":J
    :cond_2f
    :goto_1b
    invoke-static/range {v49 .. v49}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v33

    .line 554
    .local v33, "editUriLocalArray":[Ljava/lang/String;
    invoke-static/range {v32 .. v32}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    .line 555
    .local v34, "editUriServerArray":[Ljava/lang/String;
    const/4 v4, 0x2

    const/4 v5, 0x2

    aget-object v5, v33, v5

    aput-object v5, v34, v4

    .line 561
    if-eqz p7, :cond_30

    .line 562
    const/4 v4, 0x1

    aget-object v4, v34, v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 563
    const/4 v4, 0x1

    const/4 v5, 0x1

    aget-object v5, v33, v5

    aput-object v5, v34, v4

    .line 567
    :cond_30
    const-string v4, "data_sync1"

    invoke-static/range {v34 .. v34}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v4, "_id"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v18, p1

    move-object/from16 v19, p10

    invoke-static/range {v18 .. v23}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addUpdateOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_18

    .line 528
    .end local v33    # "editUriLocalArray":[Ljava/lang/String;
    .end local v34    # "editUriServerArray":[Ljava/lang/String;
    .restart local v66    # "version":J
    :cond_31
    if-nez v60, :cond_34

    .line 529
    const-string v4, "data_version"

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_32

    const/16 v57, 0x1

    .line 531
    .local v57, "photoExistsLocally":Z
    :goto_1c
    const-string v4, "data_version"

    const-wide/16 v12, 0x1

    add-long v12, v12, v66

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 533
    const-string v4, "data_sync4"

    if-eqz v57, :cond_33

    .end local v66    # "version":J
    :goto_1d
    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1b

    .line 529
    .end local v57    # "photoExistsLocally":Z
    .restart local v66    # "version":J
    :cond_32
    const/16 v57, 0x0

    goto :goto_1c

    .line 533
    .restart local v57    # "photoExistsLocally":Z
    :cond_33
    const-wide/16 v12, 0x1

    add-long v66, v66, v12

    goto :goto_1d

    .line 541
    .end local v57    # "photoExistsLocally":Z
    :cond_34
    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 542
    if-nez v36, :cond_35

    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 543
    const-string v4, "data15"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 545
    :cond_35
    const-string v4, "data_version"

    const-wide/16 v12, 0x1

    add-long v12, v12, v66

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 547
    const-string v4, "data_sync4"

    const-wide/16 v12, 0x1

    add-long v12, v12, v66

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1b

    .line 605
    .end local v16    # "contactVersion":J
    .end local v32    # "editUri":Ljava/lang/String;
    .end local v36    # "etag":Ljava/lang/String;
    .end local v37    # "existingPhotoValues":Landroid/content/ContentValues;
    .end local v49    # "localEditUri":Ljava/lang/String;
    .end local v50    # "localEtag":Ljava/lang/String;
    .end local v60    # "syncMetaVersion":Ljava/lang/Long;
    .end local v66    # "version":J
    .restart local v23    # "serverValues":Landroid/content/ContentValues;
    .restart local v38    # "existingValues":Landroid/content/ContentValues;
    .restart local v48    # "joiner":Lcom/google/android/syncadapters/contacts/IteratorJoiner;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>;"
    .restart local v53    # "newExisting":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .restart local v54    # "newServer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .restart local v58    # "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    :cond_36
    if-eqz v38, :cond_38

    .line 606
    if-eqz v64, :cond_37

    .line 607
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "add existing values: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    :cond_37
    move-object/from16 v0, v53

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_19

    .line 611
    :cond_38
    if-eqz v64, :cond_39

    .line 612
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "add server values: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :cond_39
    move-object/from16 v0, v54

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_19

    .line 618
    .end local v23    # "serverValues":Landroid/content/ContentValues;
    .end local v38    # "existingValues":Landroid/content/ContentValues;
    .end local v58    # "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    :cond_3a
    move-object/from16 v30, v54

    .line 619
    move-object/from16 v29, v53

    .line 621
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3b

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3b

    .line 622
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_KEY_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v30

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 623
    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_KEY_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v29

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 626
    :cond_3b
    if-eqz v40, :cond_3c

    .line 627
    const-string v4, "existing (post cleanup)"

    move-object/from16 v0, v29

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->dumpDataValues(Ljava/lang/String;Ljava/util/Collection;)V

    .line 628
    const-string v4, "server (post cleanup)"

    move-object/from16 v0, v30

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;->dumpDataValues(Ljava/lang/String;Ljava/util/Collection;)V

    .line 632
    :cond_3c
    new-instance v48, Lcom/google/android/syncadapters/contacts/IteratorJoiner;

    .end local v48    # "joiner":Lcom/google/android/syncadapters/contacts/IteratorJoiner;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>;"
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    sget-object v7, Lcom/google/android/syncadapters/contacts/ContactHandler;->DATA_VALUES_KEY_COMPARATOR:Lcom/google/android/syncadapters/contacts/ContactHandler$DataValuesComparator;

    move-object/from16 v0, v48

    invoke-direct {v0, v4, v5, v7}, Lcom/google/android/syncadapters/contacts/IteratorJoiner;-><init>(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Comparator;)V

    .line 635
    .restart local v48    # "joiner":Lcom/google/android/syncadapters/contacts/IteratorJoiner;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>;"
    invoke-virtual/range {v48 .. v48}, Lcom/google/android/syncadapters/contacts/IteratorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v43

    :cond_3d
    :goto_1e
    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_45

    invoke-interface/range {v43 .. v43}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v58

    check-cast v58, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;

    .line 636
    .restart local v58    # "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    invoke-virtual/range {v58 .. v58}, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;->getLeft()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Landroid/content/ContentValues;

    .line 637
    .restart local v38    # "existingValues":Landroid/content/ContentValues;
    invoke-virtual/range {v58 .. v58}, Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;->getRight()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/ContentValues;

    .line 648
    .restart local v23    # "serverValues":Landroid/content/ContentValues;
    if-eqz v38, :cond_40

    if-eqz v23, :cond_40

    .line 649
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->isDataNotDirty(Landroid/content/ContentValues;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 651
    if-eqz v40, :cond_3e

    .line 652
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updating data row: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    :cond_3e
    const-string v4, "mimetype"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 656
    .restart local v51    # "mimetype":Ljava/lang/String;
    const-string v4, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3f

    .line 657
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getNewDataVersion(Landroid/content/ContentValues;)J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/syncadapters/contacts/ContactHandler;->updateDataVersions(Landroid/content/ContentValues;J)V

    .line 659
    :cond_3f
    const-string v4, "_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v21, p1

    move-object/from16 v22, p10

    invoke-static/range {v21 .. v26}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addUpdateOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_1e

    .line 663
    .end local v51    # "mimetype":Ljava/lang/String;
    :cond_40
    if-eqz v38, :cond_43

    .line 666
    const-string v4, "mimetype"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 667
    .restart local v51    # "mimetype":Ljava/lang/String;
    const-string v4, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_41

    .line 668
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "this should never happen"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 670
    :cond_41
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->hasBeenOnServer(Landroid/content/ContentValues;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 671
    if-eqz v40, :cond_42

    .line 672
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleting data row "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    :cond_42
    const-string v4, "_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p10

    invoke-static {v0, v1, v4, v5, v7}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addDeleteOperation(Ljava/util/ArrayList;Landroid/net/Uri;JZ)V

    goto/16 :goto_1e

    .line 679
    .end local v51    # "mimetype":Ljava/lang/String;
    :cond_43
    if-eqz v40, :cond_44

    .line 680
    const-string v4, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inserting data row "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    :cond_44
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->clearDataDirty(Landroid/content/ContentValues;)V

    .line 684
    const/4 v12, 0x0

    move-object/from16 v7, p1

    move-object/from16 v8, p10

    move-object/from16 v9, v23

    invoke-static/range {v7 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addInsertOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_1e

    .line 690
    .end local v23    # "serverValues":Landroid/content/ContentValues;
    .end local v38    # "existingValues":Landroid/content/ContentValues;
    .end local v58    # "result":Lcom/google/android/syncadapters/contacts/IteratorJoiner$Result;, "Lcom/google/android/syncadapters/contacts/IteratorJoiner<Landroid/content/ContentValues;>.Result;"
    :cond_45
    if-eqz p7, :cond_6

    .line 691
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 692
    .local v9, "syncDirtyValues":Landroid/content/ContentValues;
    const-string v4, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v9, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    const/4 v12, 0x0

    move-object/from16 v7, p1

    move-object/from16 v8, p9

    invoke-static/range {v7 .. v12}, Lcom/google/android/syncadapters/contacts/ContactsSyncAdapter;->addUpdateOperation(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_6
.end method

.method clearDataDirty(Landroid/content/ContentValues;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 713
    const-string v2, "vnd.android.cursor.item/photo"

    const-string v3, "mimetype"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 718
    :goto_0
    return-void

    .line 716
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getNewDataVersion(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 717
    .local v0, "newDataVersion":J
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->updateDataVersions(Landroid/content/ContentValues;J)V

    goto :goto_0
.end method

.method public convertEntityToEntry(Landroid/content/Entity;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Z)Lcom/google/wireless/gdata2/data/Entry;
    .locals 50
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "client"    # Landroid/content/ContentProviderClient;
    .param p4, "validate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/parser/ParseException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 870
    new-instance v15, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    invoke-direct {v15}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;-><init>()V

    .line 871
    .local v15, "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v14

    .line 873
    .local v14, "entityValues":Landroid/content/ContentValues;
    const-string v3, "_id"

    invoke-virtual {v14, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    .line 875
    .local v38, "rawContactRowId":J
    const-string v3, "sourceid"

    invoke-virtual {v14, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setId(Ljava/lang/String;)V

    .line 877
    const-string v3, "sync1"

    invoke-virtual {v14, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v13, v3, v5

    .line 880
    .local v13, "editUri":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v13, 0x0

    .end local v13    # "editUri":Ljava/lang/String;
    :cond_0
    invoke-virtual {v15, v13}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setEditUri(Ljava/lang/String;)V

    .line 881
    const-string v3, "sync2"

    invoke-virtual {v14, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setETag(Ljava/lang/String;)V

    .line 882
    const-string v3, "deleted"

    invoke-virtual {v14, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setDeleted(Z)V

    .line 884
    const/16 v16, 0x0

    .line 886
    .local v16, "extendedPropertiesJson":Lorg/json/JSONObject;
    const/16 v18, 0x0

    .line 889
    .local v18, "hasSomeData":Z
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 891
    .local v9, "dateValidator":Landroid/text/format/Time;
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 892
    .local v26, "linksToValues":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    .line 895
    .local v25, "linksToPossibleTargets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/content/Entity$NamedContentValues;

    .line 896
    .local v30, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v45, v0

    .line 897
    .local v45, "uri":Landroid/net/Uri;
    move-object/from16 v0, v30

    iget-object v4, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    .line 898
    .local v4, "values":Landroid/content/ContentValues;
    const-string v3, "_id"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 899
    .local v10, "dataRowId":J
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v45

    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 900
    const-string v3, "mimetype"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 901
    .local v27, "mimetype":Ljava/lang/String;
    const-string v3, "vnd.android.cursor.item/name"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 902
    new-instance v29, Lcom/google/wireless/gdata2/contacts/data/Name;

    invoke-direct/range {v29 .. v29}, Lcom/google/wireless/gdata2/contacts/data/Name;-><init>()V

    .line 903
    .local v29, "name":Lcom/google/wireless/gdata2/contacts/data/Name;
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setGivenName(Ljava/lang/String;)V

    .line 904
    const-string v3, "data3"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setFamilyName(Ljava/lang/String;)V

    .line 905
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setNamePrefix(Ljava/lang/String;)V

    .line 906
    const-string v3, "data5"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setAdditionalName(Ljava/lang/String;)V

    .line 907
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setNameSuffix(Ljava/lang/String;)V

    .line 908
    const-string v3, "data7"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setGivenNameYomi(Ljava/lang/String;)V

    .line 909
    const-string v3, "data8"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setAdditionalNameYomi(Ljava/lang/String;)V

    .line 911
    const-string v3, "data9"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setFamilyNameYomi(Ljava/lang/String;)V

    .line 912
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/Name;->setFullName(Ljava/lang/String;)V

    .line 919
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setName(Lcom/google/wireless/gdata2/contacts/data/Name;)V

    .line 920
    const/16 v18, 0x1

    .line 921
    goto/16 :goto_1

    .line 882
    .end local v4    # "values":Landroid/content/ContentValues;
    .end local v9    # "dateValidator":Landroid/text/format/Time;
    .end local v10    # "dataRowId":J
    .end local v16    # "extendedPropertiesJson":Lorg/json/JSONObject;
    .end local v18    # "hasSomeData":Z
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v25    # "linksToPossibleTargets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v26    # "linksToValues":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v27    # "mimetype":Ljava/lang/String;
    .end local v29    # "name":Lcom/google/wireless/gdata2/contacts/data/Name;
    .end local v30    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v45    # "uri":Landroid/net/Uri;
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 921
    .restart local v4    # "values":Landroid/content/ContentValues;
    .restart local v9    # "dateValidator":Landroid/text/format/Time;
    .restart local v10    # "dataRowId":J
    .restart local v16    # "extendedPropertiesJson":Lorg/json/JSONObject;
    .restart local v18    # "hasSomeData":Z
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v25    # "linksToPossibleTargets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v26    # "linksToValues":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v27    # "mimetype":Ljava/lang/String;
    .restart local v30    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .restart local v45    # "uri":Landroid/net/Uri;
    :cond_3
    const-string v3, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 923
    const-string v3, "vnd.android.cursor.item/identity"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 924
    const-string v3, "com.google"

    const-string v5, "data2"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 926
    const-string v3, "data_sync3"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 928
    .local v28, "mimetypeAndValue":Ljava/lang/String;
    if-eqz v28, :cond_1

    .line 929
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 933
    .end local v28    # "mimetypeAndValue":Ljava/lang/String;
    :cond_4
    const-string v3, "vnd.android.cursor.item/note"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 934
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setContent(Ljava/lang/String;)V

    .line 935
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 936
    :cond_5
    const-string v3, "vnd.android.cursor.item/sip_address"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 937
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/SipAddress;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;-><init>()V

    .line 938
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/SipAddress;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_SIP_ADDRESS:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x3

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 941
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->setIsPrimary(Z)V

    .line 942
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 947
    .local v8, "address":Ljava/lang/String;
    const-string v3, ":"

    invoke-virtual {v8, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 948
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sip:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 950
    :cond_6
    invoke-virtual {v2, v8}, Lcom/google/wireless/gdata2/contacts/data/SipAddress;->setAddress(Ljava/lang/String;)V

    .line 951
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addSipAddress(Lcom/google/wireless/gdata2/contacts/data/SipAddress;)V

    .line 952
    const/16 v18, 0x1

    .line 953
    goto/16 :goto_1

    .line 941
    .end local v8    # "address":Ljava/lang/String;
    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    .line 953
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/SipAddress;
    :cond_8
    const-string v3, "vnd.android.cursor.item/nickname"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 954
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setNickname(Ljava/lang/String;)V

    .line 955
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 956
    :cond_9
    const-string v3, "vnd.com.google.cursor.item/contact_misc"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 957
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setBillingInformation(Ljava/lang/String;)V

    .line 959
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setDirectoryServer(Ljava/lang/String;)V

    .line 961
    const-string v3, "data3"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    .line 962
    .local v17, "gender":Ljava/lang/Integer;
    if-eqz v17, :cond_a

    .line 963
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 964
    const-string v3, "male"

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setGender(Ljava/lang/String;)V

    .line 973
    :cond_a
    :goto_3
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setInitials(Ljava/lang/String;)V

    .line 974
    const-string v3, "data5"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setMaidenName(Ljava/lang/String;)V

    .line 975
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setMileage(Ljava/lang/String;)V

    .line 976
    const-string v3, "data7"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setOccupation(Ljava/lang/String;)V

    .line 977
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TO_ENTRY_PRIORITY:Ljava/util/HashMap;

    const-string v5, "data8"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/Byte;

    .line 979
    .local v34, "priorityByte":Ljava/lang/Byte;
    if-nez v34, :cond_b

    .line 980
    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v34

    .line 982
    :cond_b
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setPriority(B)V

    .line 983
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TO_ENTRY_SENSITIVITY:Ljava/util/HashMap;

    const-string v5, "data9"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/Byte;

    .line 985
    .local v40, "sensitivityByte":Ljava/lang/Byte;
    if-nez v40, :cond_c

    .line 986
    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v40

    .line 988
    :cond_c
    invoke-virtual/range {v40 .. v40}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setSensitivity(B)V

    .line 989
    const-string v3, "data10"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setShortName(Ljava/lang/String;)V

    .line 990
    const-string v3, "data11"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setSubject(Ljava/lang/String;)V

    .line 991
    const/16 v18, 0x1

    .line 992
    goto/16 :goto_1

    .line 965
    .end local v34    # "priorityByte":Ljava/lang/Byte;
    .end local v40    # "sensitivityByte":Ljava/lang/Byte;
    :cond_d
    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 966
    const-string v3, "female"

    invoke-virtual {v15, v3}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setGender(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 968
    :cond_e
    const-string v3, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertEntityToEntry: dropping invalid gender "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for raw contact "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v38

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " data row "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 992
    .end local v17    # "gender":Ljava/lang/Integer;
    :cond_f
    const-string v3, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 993
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;-><init>()V

    .line 994
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EMAIL:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x3

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 997
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_10

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->setIsPrimary(Z)V

    .line 998
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->setAddress(Ljava/lang/String;)V

    .line 999
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->setDisplayName(Ljava/lang/String;)V

    .line 1001
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addEmailAddress(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)V

    .line 1002
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1004
    const/16 v18, 0x1

    .line 1005
    goto/16 :goto_1

    .line 997
    :cond_10
    const/4 v3, 0x0

    goto :goto_4

    .line 1005
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    :cond_11
    const-string v3, "vnd.android.cursor.item/website"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1006
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1007
    .local v20, "hrefValue":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1008
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/WebSite;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/WebSite;-><init>()V

    .line 1009
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/WebSite;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_WEBSITE:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x6

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1012
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_12

    const/4 v3, 0x1

    :goto_5
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->setIsPrimary(Z)V

    .line 1013
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->setHRef(Ljava/lang/String;)V

    .line 1014
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addWebSite(Lcom/google/wireless/gdata2/contacts/data/WebSite;)V

    .line 1015
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->getHRef()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1017
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 1012
    :cond_12
    const/4 v3, 0x0

    goto :goto_5

    .line 1019
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/WebSite;
    .end local v20    # "hrefValue":Ljava/lang/String;
    :cond_13
    const-string v3, "vnd.com.google.cursor.item/contact_hobby"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 1020
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1021
    .local v19, "hobby":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1022
    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addHobby(Ljava/lang/String;)V

    .line 1023
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 1025
    .end local v19    # "hobby":Ljava/lang/String;
    :cond_14
    const-string v3, "vnd.com.google.cursor.item/contact_user_defined_field"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1026
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;-><init>()V

    .line 1027
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1028
    .local v24, "key":Ljava/lang/String;
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 1032
    .local v46, "value":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-static/range {v46 .. v46}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1033
    :cond_15
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;->setKey(Ljava/lang/String;)V

    .line 1034
    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;->setValue(Ljava/lang/String;)V

    .line 1035
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addUserDefinedField(Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;)V

    .line 1036
    const/16 v18, 0x1

    goto/16 :goto_1

    .line 1038
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;
    .end local v24    # "key":Ljava/lang/String;
    .end local v46    # "value":Ljava/lang/String;
    :cond_16
    const-string v3, "vnd.com.google.cursor.item/contact_language"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 1039
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/Language;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/Language;-><init>()V

    .line 1040
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/Language;
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Language;->setCode(Ljava/lang/String;)V

    .line 1041
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/Language;->getCode()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_17

    .line 1042
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Language;->setLabel(Ljava/lang/String;)V

    .line 1044
    :cond_17
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addLanguage(Lcom/google/wireless/gdata2/contacts/data/Language;)V

    .line 1045
    const/16 v18, 0x1

    .line 1046
    goto/16 :goto_1

    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/Language;
    :cond_18
    const-string v3, "vnd.com.google.cursor.item/contact_calendar_link"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 1047
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;-><init>()V

    .line 1048
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/CalendarLink;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_CALENDAR_LINK:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1051
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->setHRef(Ljava/lang/String;)V

    .line 1052
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_19

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;->setIsPrimary(Z)V

    .line 1054
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addCalendarLink(Lcom/google/wireless/gdata2/contacts/data/CalendarLink;)V

    .line 1055
    const/16 v18, 0x1

    .line 1056
    goto/16 :goto_1

    .line 1052
    :cond_19
    const/4 v3, 0x0

    goto :goto_6

    .line 1056
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/CalendarLink;
    :cond_1a
    const-string v3, "vnd.com.google.cursor.item/contact_external_id"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1057
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/ExternalId;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/ExternalId;-><init>()V

    .line 1058
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/ExternalId;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EXTERNAL_ID:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x4

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1061
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ExternalId;->setValue(Ljava/lang/String;)V

    .line 1062
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addExternalId(Lcom/google/wireless/gdata2/contacts/data/ExternalId;)V

    .line 1063
    const/16 v18, 0x1

    .line 1064
    goto/16 :goto_1

    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/ExternalId;
    :cond_1b
    const-string v3, "vnd.com.google.cursor.item/contact_jot"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 1065
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/Jot;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/Jot;-><init>()V

    .line 1066
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/Jot;
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    sget-object v5, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_JOT:Ljava/util/HashMap;

    const/4 v6, 0x5

    invoke-static {v3, v5, v6}, Lcom/google/android/syncadapters/contacts/ContactHandler;->providerTypeToEntryType(Ljava/lang/Integer;Ljava/util/HashMap;I)B

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Jot;->setType(B)V

    .line 1070
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Jot;->setValue(Ljava/lang/String;)V

    .line 1071
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addJot(Lcom/google/wireless/gdata2/contacts/data/Jot;)V

    .line 1072
    const/16 v18, 0x1

    .line 1073
    goto/16 :goto_1

    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/Jot;
    :cond_1c
    const-string v3, "vnd.android.cursor.item/relation"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1074
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/Relation;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/Relation;-><init>()V

    .line 1075
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/Relation;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_RELATION:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/16 v7, 0xb

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1078
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 1079
    .local v37, "relationName":Ljava/lang/String;
    if-eqz v37, :cond_1

    invoke-static/range {v37 .. v37}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1082
    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/Relation;->setText(Ljava/lang/String;)V

    .line 1083
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addRelation(Lcom/google/wireless/gdata2/contacts/data/Relation;)V

    .line 1084
    const/16 v18, 0x1

    .line 1085
    goto/16 :goto_1

    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/Relation;
    .end local v37    # "relationName":Ljava/lang/String;
    :cond_1d
    const-string v3, "vnd.android.cursor.item/contact_event"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1086
    const-string v3, "data2"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v44

    .line 1087
    .local v44, "type":Ljava/lang/Integer;
    if-eqz v44, :cond_1e

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    const/16 v22, 0x1

    .line 1089
    .local v22, "isBirthday":Z
    :goto_7
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 1090
    .local v41, "startDate":Ljava/lang/String;
    move-object/from16 v0, v41

    move/from16 v1, v22

    invoke-static {v9, v0, v1}, Lcom/google/android/syncadapters/contacts/ContactHandler;->validateDate(Landroid/text/format/Time;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1f

    .line 1091
    const-string v3, "ContactsSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertEntityToEntry: dropping invalid startDate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for raw contact "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v38

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " data row "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1087
    .end local v22    # "isBirthday":Z
    .end local v41    # "startDate":Ljava/lang/String;
    :cond_1e
    const/16 v22, 0x0

    goto :goto_7

    .line 1096
    .restart local v22    # "isBirthday":Z
    .restart local v41    # "startDate":Ljava/lang/String;
    :cond_1f
    if-eqz v22, :cond_20

    .line 1097
    move-object/from16 v0, v41

    invoke-virtual {v15, v0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->setBirthday(Ljava/lang/String;)V

    .line 1106
    :goto_8
    const/16 v18, 0x1

    .line 1107
    goto/16 :goto_1

    .line 1099
    :cond_20
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/Event;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/Event;-><init>()V

    .line 1100
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/Event;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_EVENT:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x2

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1103
    move-object/from16 v0, v41

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/Event;->setStartDate(Ljava/lang/String;)V

    .line 1104
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addEvent(Lcom/google/wireless/gdata2/contacts/data/Event;)V

    goto :goto_8

    .line 1107
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/Event;
    .end local v22    # "isBirthday":Z
    .end local v41    # "startDate":Ljava/lang/String;
    .end local v44    # "type":Ljava/lang/Integer;
    :cond_21
    const-string v3, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 1108
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;-><init>()V

    .line 1109
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_POSTAL:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x3

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1112
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_22

    const/4 v3, 0x1

    :goto_9
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setIsPrimary(Z)V

    .line 1113
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setFormattedAddress(Ljava/lang/String;)V

    .line 1115
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setStreet(Ljava/lang/String;)V

    .line 1116
    const-string v3, "data5"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setPobox(Ljava/lang/String;)V

    .line 1117
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setNeighborhood(Ljava/lang/String;)V

    .line 1118
    const-string v3, "data7"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setCity(Ljava/lang/String;)V

    .line 1119
    const-string v3, "data9"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setPostcode(Ljava/lang/String;)V

    .line 1120
    const-string v3, "data8"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setRegion(Ljava/lang/String;)V

    .line 1121
    const-string v3, "data10"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;->setCountry(Ljava/lang/String;)V

    .line 1122
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addPostalAddress(Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;)V

    .line 1123
    const/16 v18, 0x1

    .line 1124
    goto/16 :goto_1

    .line 1112
    :cond_22
    const/4 v3, 0x0

    goto :goto_9

    .line 1124
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;
    :cond_23
    const-string v3, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 1125
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;-><init>()V

    .line 1126
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_PHONE:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/16 v7, 0x13

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1129
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_25

    const/4 v3, 0x1

    :goto_a
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->setIsPrimary(Z)V

    .line 1130
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->setPhoneNumber(Ljava/lang/String;)V

    .line 1131
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1133
    .local v31, "normalizedNumber":Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_24

    .line 1134
    sget-object v3, Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->RFC3966:Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    move-object/from16 v0, v31

    invoke-static {v0, v3}, Lcom/google/android/syncadapters/contacts/ContactHandler;->formatPhoneNumber(Ljava/lang/String;Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v33

    .line 1136
    .local v33, "phoneUri":Ljava/lang/String;
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_24

    .line 1137
    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->setLinksTo(Ljava/lang/String;)V

    .line 1140
    .end local v33    # "phoneUri":Ljava/lang/String;
    :cond_24
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addPhoneNumber(Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;)V

    .line 1141
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143
    const/16 v18, 0x1

    .line 1144
    goto/16 :goto_1

    .line 1129
    .end local v31    # "normalizedNumber":Ljava/lang/String;
    :cond_25
    const/4 v3, 0x0

    goto :goto_a

    .line 1144
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    :cond_26
    const-string v3, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 1145
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;-><init>()V

    .line 1146
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    const-string v3, "group_sourceid"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 1148
    .local v43, "truncatedGroupId":Ljava/lang/String;
    if-nez v43, :cond_27

    .line 1149
    new-instance v3, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v5, "the group doesn\'t yet have a source id"

    invoke-direct {v3, v5}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1151
    :cond_27
    move-object/from16 v0, p2

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/contacts/GroupHandler;->getCanonicalGroupSourceId(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->setGroup(Ljava/lang/String;)V

    .line 1153
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addGroup(Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;)V

    .line 1154
    const/16 v18, 0x1

    .line 1155
    goto/16 :goto_1

    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .end local v43    # "truncatedGroupId":Ljava/lang/String;
    :cond_28
    const-string v3, "vnd.android.cursor.item/organization"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 1156
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/Organization;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/Organization;-><init>()V

    .line 1157
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/Organization;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_ORGANIZATION:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x2

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1160
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_29

    const/4 v3, 0x1

    :goto_b
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setIsPrimary(Z)V

    .line 1161
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setName(Ljava/lang/String;)V

    .line 1163
    const-string v3, "data4"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setTitle(Ljava/lang/String;)V

    .line 1165
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addOrganization(Lcom/google/wireless/gdata2/contacts/data/Organization;)V

    .line 1166
    const-string v3, "data5"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setOrgDepartment(Ljava/lang/String;)V

    .line 1168
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setOrgJobDescription(Ljava/lang/String;)V

    .line 1170
    const-string v3, "data7"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setOrgSymbol(Ljava/lang/String;)V

    .line 1172
    const-string v3, "data8"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/Organization;->setNameYomi(Ljava/lang/String;)V

    .line 1174
    const/16 v18, 0x1

    .line 1175
    goto/16 :goto_1

    .line 1160
    :cond_29
    const/4 v3, 0x0

    goto :goto_b

    .line 1175
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/Organization;
    :cond_2a
    const-string v3, "vnd.android.cursor.item/im"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 1176
    new-instance v2, Lcom/google/wireless/gdata2/contacts/data/ImAddress;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;-><init>()V

    .line 1177
    .local v2, "serverItem":Lcom/google/wireless/gdata2/contacts/data/ImAddress;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_TYPE_TO_ENTRY_IM:Ljava/util/HashMap;

    const-string v5, "data2"

    const-string v6, "data3"

    const/4 v7, 0x3

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/contacts/ContactHandler;->setTypeAndLabel(Lcom/google/wireless/gdata2/contacts/data/TypedElement;Ljava/util/HashMap;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;B)V

    .line 1180
    const-string v3, "is_primary"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v48, 0x0

    cmp-long v3, v6, v48

    if-eqz v3, :cond_2c

    const/4 v3, 0x1

    :goto_c
    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setIsPrimary(Z)V

    .line 1181
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setAddress(Ljava/lang/String;)V

    .line 1182
    const-string v3, "data5"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v36

    .line 1183
    .local v36, "protocolType":Ljava/lang/Integer;
    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROVIDER_IM_PROTOCOL_TO_ENTRY_PROTOCOL:Ljava/util/HashMap;

    move-object/from16 v0, v36

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/Byte;

    .line 1185
    .local v35, "protocolEntry":Ljava/lang/Byte;
    if-nez v36, :cond_2d

    .line 1186
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolPredefined(B)V

    .line 1187
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolCustom(Ljava/lang/String;)V

    .line 1199
    :goto_d
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->getProtocolPredefined()B

    move-result v3

    const/16 v5, 0xa

    if-ne v3, v5, :cond_2b

    .line 1200
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolPredefined(B)V

    .line 1201
    const-string v3, "netmeeting"

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolCustom(Ljava/lang/String;)V

    .line 1203
    :cond_2b
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addImAddress(Lcom/google/wireless/gdata2/contacts/data/ImAddress;)V

    .line 1204
    const/16 v18, 0x1

    .line 1205
    goto/16 :goto_1

    .line 1180
    .end local v35    # "protocolEntry":Ljava/lang/Byte;
    .end local v36    # "protocolType":Ljava/lang/Integer;
    :cond_2c
    const/4 v3, 0x0

    goto :goto_c

    .line 1188
    .restart local v35    # "protocolEntry":Ljava/lang/Byte;
    .restart local v36    # "protocolType":Ljava/lang/Integer;
    :cond_2d
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2e

    if-nez v35, :cond_2f

    .line 1191
    :cond_2e
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolPredefined(B)V

    .line 1192
    const-string v3, "data6"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolCustom(Ljava/lang/String;)V

    goto :goto_d

    .line 1196
    :cond_2f
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/contacts/data/ImAddress;->setProtocolPredefined(B)V

    goto :goto_d

    .line 1205
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/contacts/data/ImAddress;
    .end local v35    # "protocolEntry":Ljava/lang/Byte;
    .end local v36    # "protocolType":Ljava/lang/Integer;
    :cond_30
    const-string v3, "vnd.com.google.cursor.item/contact_extended_property"

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1206
    if-nez v16, :cond_31

    .line 1207
    new-instance v16, Lorg/json/JSONObject;

    .end local v16    # "extendedPropertiesJson":Lorg/json/JSONObject;
    invoke-direct/range {v16 .. v16}, Lorg/json/JSONObject;-><init>()V

    .line 1210
    .restart local v16    # "extendedPropertiesJson":Lorg/json/JSONObject;
    :cond_31
    :try_start_0
    const-string v3, "data1"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "data2"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1213
    :catch_0
    move-exception v12

    .line 1214
    .local v12, "e":Lorg/json/JSONException;
    new-instance v3, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v5, "bad key or value"

    invoke-direct {v3, v5, v12}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1220
    .end local v4    # "values":Landroid/content/ContentValues;
    .end local v10    # "dataRowId":J
    .end local v12    # "e":Lorg/json/JSONException;
    .end local v27    # "mimetype":Ljava/lang/String;
    .end local v30    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v45    # "uri":Landroid/net/Uri;
    :cond_32
    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_33
    :goto_e
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_36

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/util/Map$Entry;

    .line 1221
    .local v32, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v32 .. v32}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    .line 1222
    .local v42, "target":Ljava/lang/Object;
    if-eqz v42, :cond_33

    .line 1223
    move-object/from16 v0, v42

    instance-of v3, v0, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    if-eqz v3, :cond_34

    .line 1224
    check-cast v42, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .end local v42    # "target":Ljava/lang/Object;
    invoke-interface/range {v32 .. v32}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->setLinksTo(Ljava/lang/String;)V

    goto :goto_e

    .line 1225
    .restart local v42    # "target":Ljava/lang/Object;
    :cond_34
    move-object/from16 v0, v42

    instance-of v3, v0, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    if-eqz v3, :cond_35

    .line 1226
    check-cast v42, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .end local v42    # "target":Ljava/lang/Object;
    invoke-interface/range {v32 .. v32}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->setLinksTo(Ljava/lang/String;)V

    goto :goto_e

    .line 1227
    .restart local v42    # "target":Ljava/lang/Object;
    :cond_35
    move-object/from16 v0, v42

    instance-of v3, v0, Lcom/google/wireless/gdata2/contacts/data/WebSite;

    if-eqz v3, :cond_33

    .line 1228
    check-cast v42, Lcom/google/wireless/gdata2/contacts/data/WebSite;

    .end local v42    # "target":Ljava/lang/Object;
    invoke-interface/range {v32 .. v32}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-virtual {v0, v3}, Lcom/google/wireless/gdata2/contacts/data/WebSite;->setLinksTo(Ljava/lang/String;)V

    goto :goto_e

    .line 1233
    .end local v32    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_36
    if-eqz v16, :cond_38

    .line 1234
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1235
    .local v23, "jsonString":Ljava/lang/String;
    if-nez v23, :cond_37

    .line 1236
    new-instance v3, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unable to convert JSON object into a JSON string, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1239
    :cond_37
    new-instance v2, Lcom/google/wireless/gdata2/data/ExtendedProperty;

    invoke-direct {v2}, Lcom/google/wireless/gdata2/data/ExtendedProperty;-><init>()V

    .line 1240
    .local v2, "serverItem":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    const-string v3, "android"

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/data/ExtendedProperty;->setName(Ljava/lang/String;)V

    .line 1241
    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/google/wireless/gdata2/data/ExtendedProperty;->setValue(Ljava/lang/String;)V

    .line 1242
    invoke-virtual {v15, v2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->addExtendedProperty(Lcom/google/wireless/gdata2/data/ExtendedProperty;)V

    .line 1243
    const/16 v18, 0x1

    .line 1246
    .end local v2    # "serverItem":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    .end local v23    # "jsonString":Ljava/lang/String;
    :cond_38
    if-eqz p4, :cond_39

    .line 1247
    invoke-virtual {v15}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->isDeleted()Z

    move-result v3

    if-nez v3, :cond_39

    if-nez v18, :cond_39

    .line 1248
    new-instance v3, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v5, "empty entry"

    invoke-direct {v3, v5}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1252
    :cond_39
    return-object v15
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1303
    const-string v0, "deleted"

    return-object v0
.end method

.method public getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1299
    const-string v0, "dirty"

    return-object v0
.end method

.method public getEditUriColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1307
    const-string v0, "sync1"

    return-object v0
.end method

.method public getEntityUri(Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 253
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getEntryClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 239
    const-class v0, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    return-object v0
.end method

.method public getEtagColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1311
    const-string v0, "sync2"

    return-object v0
.end method

.method public getFeedForAccount(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://www.google.com/m8/feeds/contacts/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/base2_property-android_linksto-gprofiles_highresphotos"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "url":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->rewriteUrlForAccount(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getSourceIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1295
    const-string v0, "sourceid"

    return-object v0
.end method

.method public getVersionColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1315
    const-string v0, "version"

    return-object v0
.end method

.method hasBeenOnServer(Landroid/content/ContentValues;)Z
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 833
    const-string v1, "vnd.android.cursor.item/photo"

    const-string v2, "mimetype"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 834
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 836
    :cond_0
    const-string v1, "data_sync4"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 838
    .local v0, "sync4":Ljava/lang/Long;
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public idToSourceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 235
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isDataNotDirty(Landroid/content/ContentValues;)Z
    .locals 10
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v8, 0xa

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 811
    const-string v5, "vnd.android.cursor.item/photo"

    const-string v6, "mimetype"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 812
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    throw v3

    .line 814
    :cond_0
    const-string v5, "data_sync4"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 817
    .local v2, "sync4":Ljava/lang/Long;
    if-nez v2, :cond_2

    .line 829
    :cond_1
    :goto_0
    return v3

    .line 822
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    .line 823
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    move v3, v4

    goto :goto_0

    .line 828
    :cond_3
    const-string v5, "data_version"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 829
    .local v0, "dataVersion":J
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, v8

    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    move v3, v4

    goto :goto_0
.end method

.method public markPhotoForHighResSync(Landroid/net/Uri;Landroid/content/ContentProviderClient;)V
    .locals 22
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;

    .prologue
    .line 737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mimetype=? AND raw_contact_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 741
    .local v5, "selectionPhoto":Ljava/lang/String;
    const-string v2, "ContactsSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 742
    const-string v2, "ContactsSyncAdapter"

    const-string v3, "IN MARK PHOTOS FOR SYNC METHOD"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    :cond_0
    :try_start_0
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/syncadapters/contacts/ContactHandler;->PROJECTION_PHOTO_COLUMNS:[Ljava/lang/String;

    sget-object v6, Lcom/google/android/syncadapters/contacts/ContactHandler;->SELECTION_ARGS_PHOTO_METADATA_MIMETYPE:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 751
    .local v8, "c":Landroid/database/Cursor;
    :cond_1
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 752
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 753
    .local v14, "photoDataRowId":J
    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 754
    .local v10, "editUriArray":[Ljava/lang/String;
    const/4 v2, 0x3

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 755
    .local v20, "version":J
    const/4 v2, 0x4

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 756
    .local v13, "photoFileId":Ljava/lang/String;
    const/4 v2, 0x5

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 758
    .local v18, "syncVersion":J
    const-string v2, "ContactsSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 759
    const-string v2, "ContactsSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " EDIT_URI = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v10}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    :cond_2
    const/4 v2, 0x1

    aget-object v2, v10, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/syncadapters/contacts/ContactHandler;->SYNC_HIGH_RES:Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v10, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v13, :cond_1

    .line 771
    const/4 v2, 0x2

    sget-object v3, Lcom/google/android/syncadapters/contacts/ContactHandler;->SYNC_HIGH_RES:Ljava/lang/String;

    aput-object v3, v10, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 774
    :try_start_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 776
    .local v12, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v16

    .line 778
    .local v16, "photoRowToUpdate":Landroid/net/Uri;
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data_sync1"

    invoke-static {v10}, Lcom/google/android/syncadapters/contacts/ContactHandler;->editUriArrayToField([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data_sync3"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data_sync4"

    const-wide/16 v6, 0x1

    add-long v6, v6, v18

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "data_version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 794
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 795
    .local v11, "extras":Landroid/os/Bundle;
    const/4 v2, 0x0

    const-string v3, "com.android.contacts"

    invoke-static {v2, v3, v11}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 797
    .end local v11    # "extras":Landroid/os/Bundle;
    .end local v12    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v16    # "photoRowToUpdate":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 798
    .local v9, "e":Landroid/content/OperationApplicationException;
    :try_start_3
    const-string v2, "ContactsSyncAdapter"

    const-string v3, "error writing photo data into provider"

    invoke-static {v2, v3, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 803
    .end local v9    # "e":Landroid/content/OperationApplicationException;
    .end local v10    # "editUriArray":[Ljava/lang/String;
    .end local v13    # "photoFileId":Ljava/lang/String;
    .end local v14    # "photoDataRowId":J
    .end local v18    # "syncVersion":J
    .end local v20    # "version":J
    :catchall_0
    move-exception v2

    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    .line 805
    .end local v8    # "c":Landroid/database/Cursor;
    :catch_1
    move-exception v9

    .line 806
    .local v9, "e":Landroid/os/RemoteException;
    const-string v2, "ContactsSyncAdapter"

    const-string v3, "error writing photo data into provider"

    invoke-static {v2, v3, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 808
    .end local v9    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void

    .line 803
    .restart local v8    # "c":Landroid/database/Cursor;
    :cond_3
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1
.end method

.method public newEntityIterator(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/lang/Long;Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;
    .locals 6
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "id"    # Ljava/lang/Long;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 258
    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v0, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, v2

    .line 259
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/ContactsContract$RawContacts;->newEntityIterator(Landroid/database/Cursor;)Landroid/content/EntityIterator;

    move-result-object v0

    return-object v0
.end method

.method setExistsOnServerFlag(Ljava/util/ArrayList;Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 8
    .param p2, "dataUri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            ")V"
        }
    .end annotation

    .prologue
    .line 702
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v3, "vnd.android.cursor.item/photo"

    const-string v4, "mimetype"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 710
    :goto_0
    return-void

    .line 705
    :cond_0
    const-string v3, "_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 706
    .local v2, "uri":Landroid/net/Uri;
    invoke-direct {p0, p3}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getNewDataVersion(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 707
    .local v0, "newDataVersion":J
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data_sync4"

    const-wide/16 v6, 0xa

    add-long/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
