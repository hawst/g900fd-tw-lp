.class public Lcom/google/android/syncadapters/contacts/GalProvider;
.super Landroid/content/ContentProvider;
.source "GalProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable;,
        Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;,
        Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;,
        Lcom/google/android/syncadapters/contacts/GalProvider$PhotoDataWriter;
    }
.end annotation


# static fields
.field private static final sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mAccountManager:Landroid/accounts/AccountManager;

.field private final mActiveTasks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/util/concurrent/FutureTask;",
            ">;"
        }
    .end annotation
.end field

.field private mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

.field private mThumbnailSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 127
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    .line 128
    new-instance v0, Lcom/google/android/syncadapters/contacts/ContactHandler;

    invoke-direct {v0}, Lcom/google/android/syncadapters/contacts/ContactHandler;-><init>()V

    sput-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    .line 167
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "directories"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 168
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "contacts/filter/*"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 169
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "contacts/lookup/*/entities"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 170
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "contacts/lookup/*/#/entities"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 171
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "data/emails/filter/*"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 172
    sget-object v0, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.contacts.gal.provider"

    const-string v2, "data/phones/filter/*"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 173
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 175
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    .line 1349
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/syncadapters/contacts/GalProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/syncadapters/contacts/GalProvider;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z
    .param p7, "x7"    # Z
    .param p8, "x8"    # Z

    .prologue
    .line 105
    invoke-direct/range {p0 .. p8}, Lcom/google/android/syncadapters/contacts/GalProvider;->handleFilter([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/syncadapters/contacts/GalProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 2
    .param p0, "x0"    # Lcom/google/android/syncadapters/contacts/GalProvider;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J

    .prologue
    .line 105
    invoke-direct/range {p0 .. p5}, Lcom/google/android/syncadapters/contacts/GalProvider;->handleContact([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/syncadapters/contacts/GalProvider;Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/syncadapters/contacts/GalProvider;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/contacts/GalProvider;->openFile(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/syncadapters/contacts/GalProvider;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/syncadapters/contacts/GalProvider;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    return-object v0
.end method

.method private addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V
    .locals 0
    .param p2, "row"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 724
    .local p1, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/Object;>;"
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    return-void
.end method

.method private buildGalResultCursor([Ljava/lang/String;Lcom/google/wireless/gdata2/parser/GDataParser;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;
    .locals 55
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "parser"    # Lcom/google/wireless/gdata2/parser/GDataParser;
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "numResults"    # I
    .param p6, "emailFilter"    # Z
    .param p7, "phoneFilter"    # Z
    .param p8, "removeDuplicates"    # Z

    .prologue
    .line 960
    const/4 v15, -0x1

    .line 961
    .local v15, "displayNameIndex":I
    const/16 v16, -0x1

    .line 962
    .local v16, "displayNameSourceIndex":I
    const/4 v9, -0x1

    .line 963
    .local v9, "alternateDisplayNameIndex":I
    const/16 v19, -0x1

    .line 964
    .local v19, "emailIndex":I
    const/16 v21, -0x1

    .line 965
    .local v21, "emailTypeIndex":I
    const/16 v20, -0x1

    .line 966
    .local v20, "emailLabelIndex":I
    const/16 v38, -0x1

    .line 967
    .local v38, "phoneNumberIndex":I
    const/16 v37, -0x1

    .line 968
    .local v37, "phoneNormalizedNumberIndex":I
    const/16 v40, -0x1

    .line 969
    .local v40, "phoneTypeIndex":I
    const/16 v36, -0x1

    .line 970
    .local v36, "phoneLabelIndex":I
    const/16 v23, -0x1

    .line 971
    .local v23, "hasPhoneNumberIndex":I
    const/16 v41, -0x1

    .line 972
    .local v41, "photoIndex":I
    const/16 v42, -0x1

    .line 973
    .local v42, "photoThumbnailIndex":I
    const/16 v27, -0x1

    .line 974
    .local v27, "idIndex":I
    const/4 v12, -0x1

    .line 975
    .local v12, "contactIdIndex":I
    const/16 v28, -0x1

    .line 977
    .local v28, "lookupIndex":I
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v52, v0

    move/from16 v0, v24

    move/from16 v1, v52

    if-ge v0, v1, :cond_11

    .line 978
    aget-object v10, p1, v24

    .line 979
    .local v10, "column":Ljava/lang/String;
    const-string v52, "display_name"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-nez v52, :cond_0

    const-string v52, "display_name"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_2

    .line 981
    :cond_0
    move/from16 v15, v24

    .line 977
    :cond_1
    :goto_1
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    .line 982
    :cond_2
    const-string v52, "display_name_alt"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_3

    .line 983
    move/from16 v9, v24

    goto :goto_1

    .line 984
    :cond_3
    const-string v52, "display_name_source"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_4

    .line 985
    move/from16 v16, v24

    goto :goto_1

    .line 986
    :cond_4
    const-string v52, "has_phone_number"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_5

    .line 987
    move/from16 v23, v24

    goto :goto_1

    .line 988
    :cond_5
    const-string v52, "photo_uri"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_6

    .line 989
    move/from16 v41, v24

    goto :goto_1

    .line 990
    :cond_6
    const-string v52, "photo_thumb_uri"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_7

    .line 991
    move/from16 v42, v24

    goto :goto_1

    .line 992
    :cond_7
    const-string v52, "_id"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_8

    .line 993
    move/from16 v27, v24

    goto :goto_1

    .line 994
    :cond_8
    const-string v52, "contact_id"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_9

    .line 995
    move/from16 v12, v24

    goto :goto_1

    .line 996
    :cond_9
    const-string v52, "lookup"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_a

    .line 997
    move/from16 v28, v24

    goto :goto_1

    .line 998
    :cond_a
    if-eqz p7, :cond_e

    .line 999
    const-string v52, "data1"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_b

    .line 1000
    move/from16 v38, v24

    goto :goto_1

    .line 1001
    :cond_b
    const-string v52, "data2"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_c

    .line 1002
    move/from16 v40, v24

    goto/16 :goto_1

    .line 1003
    :cond_c
    const-string v52, "data3"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_d

    .line 1004
    move/from16 v36, v24

    goto/16 :goto_1

    .line 1005
    :cond_d
    const-string v52, "data4"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1

    .line 1006
    move/from16 v37, v24

    goto/16 :goto_1

    .line 1011
    :cond_e
    const-string v52, "data1"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_f

    .line 1012
    move/from16 v19, v24

    goto/16 :goto_1

    .line 1013
    :cond_f
    const-string v52, "data2"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_10

    .line 1014
    move/from16 v21, v24

    goto/16 :goto_1

    .line 1015
    :cond_10
    const-string v52, "data3"

    move-object/from16 v0, v52

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1

    .line 1016
    move/from16 v20, v24

    goto/16 :goto_1

    .line 1021
    .end local v10    # "column":Ljava/lang/String;
    :cond_11
    const/16 v50, 0x0

    .line 1022
    .local v50, "usePrimarySortKey":Z
    const/16 v49, 0x0

    .line 1023
    .local v49, "useAlternateSortKey":Z
    const-string v52, "sort_key"

    move-object/from16 v0, v52

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_22

    .line 1024
    const/16 v50, 0x1

    .line 1031
    :cond_12
    :goto_2
    new-instance v45, Ljava/util/TreeMap;

    new-instance v52, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;

    invoke-direct/range {v52 .. v52}, Lcom/google/android/syncadapters/contacts/GalProvider$NameComparator;-><init>()V

    move-object/from16 v0, v45

    move-object/from16 v1, v52

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 1036
    .local v45, "sortedResultsMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;[Ljava/lang/Object;>;"
    const/16 v26, 0x1

    .line 1041
    .local v26, "id":I
    const/4 v11, 0x1

    .line 1043
    .local v11, "contactId":I
    :try_start_0
    invoke-interface/range {p2 .. p2}, Lcom/google/wireless/gdata2/parser/GDataParser;->parseFeedEnvelope()Lcom/google/wireless/gdata2/data/Feed;

    .line 1044
    :cond_13
    invoke-interface/range {p2 .. p2}, Lcom/google/wireless/gdata2/parser/GDataParser;->hasMoreData()Z

    move-result v52

    if-eqz v52, :cond_36

    .line 1045
    const/16 v52, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v52

    invoke-interface {v0, v1}, Lcom/google/wireless/gdata2/parser/GDataParser;->readNextEntry(Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v22

    check-cast v22, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    .line 1048
    .local v22, "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    const/4 v4, 0x0

    .line 1050
    .local v4, "addedContact":Z
    invoke-virtual/range {v22 .. v22}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getId()Ljava/lang/String;

    move-result-object v29

    .line 1051
    .local v29, "lookupKey":Ljava/lang/String;
    const-string v52, "/"

    move-object/from16 v0, v29

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v35

    .line 1052
    .local v35, "p":I
    if-ltz v35, :cond_13

    .line 1056
    add-int/lit8 v52, v35, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v29

    .line 1058
    invoke-static/range {v22 .. v22}, Lcom/google/android/syncadapters/contacts/GalProvider;->getDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/util/Pair;

    move-result-object v14

    .line 1059
    .local v14, "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v52, v0

    check-cast v52, Ljava/lang/CharSequence;

    invoke-static/range {v52 .. v52}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v52

    if-nez v52, :cond_13

    .line 1064
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v52, v0

    check-cast v52, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v52

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/contacts/GalProvider;->getAlternateDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1066
    .local v8, "alternateDisplayName":Ljava/lang/String;
    if-eqz v50, :cond_24

    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v52, v0

    check-cast v52, Ljava/lang/String;

    move-object/from16 v44, v52

    .line 1068
    .local v44, "sortName":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v52, v0

    move/from16 v0, v52

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v43, v0

    .line 1069
    .local v43, "row":[Ljava/lang/Object;
    const/16 v52, -0x1

    move/from16 v0, v52

    if-eq v15, v0, :cond_14

    .line 1070
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v52, v0

    aput-object v52, v43, v15

    .line 1072
    :cond_14
    const/16 v52, -0x1

    move/from16 v0, v16

    move/from16 v1, v52

    if-eq v0, v1, :cond_15

    .line 1073
    iget-object v0, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v52, v0

    aput-object v52, v43, v16

    .line 1076
    :cond_15
    const/16 v52, -0x1

    move/from16 v0, v52

    if-eq v9, v0, :cond_16

    .line 1077
    aput-object v8, v43, v9

    .line 1080
    :cond_16
    const/16 v52, -0x1

    move/from16 v0, v23

    move/from16 v1, v52

    if-eq v0, v1, :cond_17

    .line 1081
    invoke-virtual/range {v22 .. v22}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPhoneNumbers()Ljava/util/Vector;

    move-result-object v39

    .line 1082
    .local v39, "phoneNumbers":Ljava/util/Vector;
    if-eqz v39, :cond_17

    invoke-virtual/range {v39 .. v39}, Ljava/util/Vector;->size()I

    move-result v52

    if-lez v52, :cond_17

    .line 1083
    const/16 v52, 0x1

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    aput-object v52, v43, v23

    .line 1087
    .end local v39    # "phoneNumbers":Ljava/util/Vector;
    :cond_17
    const/16 v52, -0x1

    move/from16 v0, v41

    move/from16 v1, v52

    if-eq v0, v1, :cond_18

    .line 1088
    const/16 v52, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v22

    move/from16 v3, v52

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->getPhotoUri(Ljava/lang/String;Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Z)Landroid/net/Uri;

    move-result-object v52

    aput-object v52, v43, v41

    .line 1091
    :cond_18
    const/16 v52, -0x1

    move/from16 v0, v42

    move/from16 v1, v52

    if-eq v0, v1, :cond_19

    .line 1092
    const/16 v52, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v22

    move/from16 v3, v52

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->getPhotoUri(Ljava/lang/String;Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Z)Landroid/net/Uri;

    move-result-object v52

    aput-object v52, v43, v42

    .line 1095
    :cond_19
    const/16 v52, -0x1

    move/from16 v0, v52

    if-eq v12, v0, :cond_1a

    .line 1096
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    aput-object v52, v43, v12

    .line 1099
    :cond_1a
    const/16 v52, -0x1

    move/from16 v0, v28

    move/from16 v1, v52

    if-eq v0, v1, :cond_1b

    .line 1100
    aput-object v29, v43, v28

    .line 1106
    :cond_1b
    if-eqz p6, :cond_27

    .line 1108
    invoke-virtual/range {v22 .. v22}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEmailAddresses()Ljava/util/Vector;

    move-result-object v7

    .line 1109
    .local v7, "addresses":Ljava/util/Vector;
    if-eqz v7, :cond_35

    .line 1110
    new-instance v47, Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v52

    move-object/from16 v0, v47

    move/from16 v1, v52

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 1112
    .local v47, "uniqueAddresses":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .local v25, "i$":Ljava/util/Iterator;
    :cond_1c
    :goto_4
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v52

    if-eqz v52, :cond_35

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    .line 1113
    .local v34, "o":Ljava/lang/Object;
    move-object/from16 v0, v34

    check-cast v0, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 1114
    .local v5, "address":Ljava/lang/String;
    if-eqz v5, :cond_26

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1115
    .local v6, "addressStr":Ljava/lang/String;
    :goto_5
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v52

    if-nez v52, :cond_1c

    .line 1118
    if-eqz p8, :cond_1d

    .line 1119
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v47

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1c

    .line 1123
    :cond_1d
    const/16 v52, -0x1

    move/from16 v0, v19

    move/from16 v1, v52

    if-eq v0, v1, :cond_1e

    .line 1124
    aput-object v5, v43, v19

    .line 1127
    :cond_1e
    check-cast v34, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .end local v34    # "o":Ljava/lang/Object;
    invoke-static/range {v34 .. v34}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)Landroid/content/ContentValues;

    move-result-object v51

    .line 1129
    .local v51, "values":Landroid/content/ContentValues;
    const/16 v52, -0x1

    move/from16 v0, v21

    move/from16 v1, v52

    if-eq v0, v1, :cond_1f

    .line 1130
    const-string v52, "data2"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v21

    .line 1132
    :cond_1f
    const/16 v52, -0x1

    move/from16 v0, v20

    move/from16 v1, v52

    if-eq v0, v1, :cond_20

    .line 1133
    const-string v52, "data3"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v20

    .line 1135
    :cond_20
    const/16 v52, -0x1

    move/from16 v0, v27

    move/from16 v1, v52

    if-eq v0, v1, :cond_21

    .line 1136
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    aput-object v52, v43, v27

    .line 1138
    :cond_21
    new-instance v52, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    move-object/from16 v0, v52

    move-object/from16 v1, v44

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;-><init>(Ljava/lang/String;I)V

    invoke-virtual/range {v43 .. v43}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v53

    move-object/from16 v0, v45

    move-object/from16 v1, v52

    move-object/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1140
    const/4 v4, 0x1

    .line 1141
    add-int/lit8 v26, v26, 0x1

    .line 1142
    goto/16 :goto_4

    .line 1025
    .end local v4    # "addedContact":Z
    .end local v5    # "address":Ljava/lang/String;
    .end local v6    # "addressStr":Ljava/lang/String;
    .end local v7    # "addresses":Ljava/util/Vector;
    .end local v8    # "alternateDisplayName":Ljava/lang/String;
    .end local v11    # "contactId":I
    .end local v14    # "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v22    # "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v26    # "id":I
    .end local v29    # "lookupKey":Ljava/lang/String;
    .end local v35    # "p":I
    .end local v43    # "row":[Ljava/lang/Object;
    .end local v44    # "sortName":Ljava/lang/String;
    .end local v45    # "sortedResultsMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;[Ljava/lang/Object;>;"
    .end local v47    # "uniqueAddresses":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v51    # "values":Landroid/content/ContentValues;
    :cond_22
    const-string v52, "sort_key_alt"

    move-object/from16 v0, v52

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_23

    .line 1026
    const/16 v49, 0x1

    goto/16 :goto_2

    .line 1027
    :cond_23
    if-eqz p4, :cond_12

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v52

    if-lez v52, :cond_12

    .line 1028
    const-string v52, "GalProvider"

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    const-string v54, "Ignoring unsupported sort order: "

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    move-object/from16 v0, v53

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v52 .. v53}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1066
    .restart local v4    # "addedContact":Z
    .restart local v8    # "alternateDisplayName":Ljava/lang/String;
    .restart local v11    # "contactId":I
    .restart local v14    # "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v22    # "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .restart local v26    # "id":I
    .restart local v29    # "lookupKey":Ljava/lang/String;
    .restart local v35    # "p":I
    .restart local v45    # "sortedResultsMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;[Ljava/lang/Object;>;"
    :cond_24
    if-eqz v49, :cond_25

    move-object/from16 v44, v8

    goto/16 :goto_3

    :cond_25
    :try_start_1
    const-string v44, ""

    goto/16 :goto_3

    .line 1114
    .restart local v5    # "address":Ljava/lang/String;
    .restart local v7    # "addresses":Ljava/util/Vector;
    .restart local v25    # "i$":Ljava/util/Iterator;
    .restart local v34    # "o":Ljava/lang/Object;
    .restart local v43    # "row":[Ljava/lang/Object;
    .restart local v44    # "sortName":Ljava/lang/String;
    .restart local v47    # "uniqueAddresses":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_26
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 1144
    .end local v5    # "address":Ljava/lang/String;
    .end local v7    # "addresses":Ljava/util/Vector;
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v34    # "o":Ljava/lang/Object;
    .end local v47    # "uniqueAddresses":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_27
    if-eqz p7, :cond_31

    .line 1146
    invoke-virtual/range {v22 .. v22}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPhoneNumbers()Ljava/util/Vector;

    move-result-object v39

    .line 1147
    .restart local v39    # "phoneNumbers":Ljava/util/Vector;
    if-eqz v39, :cond_35

    .line 1148
    new-instance v48, Ljava/util/HashSet;

    invoke-virtual/range {v39 .. v39}, Ljava/util/Vector;->size()I

    move-result v52

    move-object/from16 v0, v48

    move/from16 v1, v52

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 1149
    .local v48, "uniqueNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {v39 .. v39}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .restart local v25    # "i$":Ljava/util/Iterator;
    :cond_28
    :goto_6
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v52

    if-eqz v52, :cond_35

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    .line 1151
    .restart local v34    # "o":Ljava/lang/Object;
    check-cast v34, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .end local v34    # "o":Ljava/lang/Object;
    invoke-static/range {v34 .. v34}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;)Landroid/content/ContentValues;

    move-result-object v51

    .line 1153
    .restart local v51    # "values":Landroid/content/ContentValues;
    const-string v52, "data4"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v31

    .line 1154
    .local v31, "normalizedNumber":Ljava/lang/Object;
    const-string v52, "data1"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v32

    .line 1155
    .local v32, "number":Ljava/lang/Object;
    if-eqz v32, :cond_2f

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v33

    .line 1156
    .local v33, "numberStr":Ljava/lang/String;
    :goto_7
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v52

    if-nez v52, :cond_28

    .line 1159
    if-eqz p8, :cond_29

    .line 1160
    if-eqz v31, :cond_30

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v30

    .line 1164
    .local v30, "normNumberStr":Ljava/lang/String;
    :goto_8
    invoke-static/range {v30 .. v30}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 1166
    .local v46, "strippedNumber":Ljava/lang/String;
    move-object/from16 v0, v48

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_28

    .line 1170
    .end local v30    # "normNumberStr":Ljava/lang/String;
    .end local v46    # "strippedNumber":Ljava/lang/String;
    :cond_29
    const/16 v52, -0x1

    move/from16 v0, v38

    move/from16 v1, v52

    if-eq v0, v1, :cond_2a

    .line 1171
    aput-object v32, v43, v38

    .line 1173
    :cond_2a
    const/16 v52, -0x1

    move/from16 v0, v37

    move/from16 v1, v52

    if-eq v0, v1, :cond_2b

    .line 1174
    aput-object v31, v43, v37

    .line 1176
    :cond_2b
    const/16 v52, -0x1

    move/from16 v0, v40

    move/from16 v1, v52

    if-eq v0, v1, :cond_2c

    .line 1177
    const-string v52, "data2"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v40

    .line 1179
    :cond_2c
    const/16 v52, -0x1

    move/from16 v0, v36

    move/from16 v1, v52

    if-eq v0, v1, :cond_2d

    .line 1180
    const-string v52, "data3"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v36

    .line 1182
    :cond_2d
    const/16 v52, -0x1

    move/from16 v0, v27

    move/from16 v1, v52

    if-eq v0, v1, :cond_2e

    .line 1183
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    aput-object v52, v43, v27

    .line 1185
    :cond_2e
    new-instance v52, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    move-object/from16 v0, v52

    move-object/from16 v1, v44

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;-><init>(Ljava/lang/String;I)V

    invoke-virtual/range {v43 .. v43}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v53

    move-object/from16 v0, v45

    move-object/from16 v1, v52

    move-object/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1187
    const/4 v4, 0x1

    .line 1188
    add-int/lit8 v26, v26, 0x1

    .line 1189
    goto/16 :goto_6

    .line 1155
    .end local v33    # "numberStr":Ljava/lang/String;
    :cond_2f
    const/16 v33, 0x0

    goto :goto_7

    .restart local v33    # "numberStr":Ljava/lang/String;
    :cond_30
    move-object/from16 v30, v33

    .line 1160
    goto :goto_8

    .line 1193
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v31    # "normalizedNumber":Ljava/lang/Object;
    .end local v32    # "number":Ljava/lang/Object;
    .end local v33    # "numberStr":Ljava/lang/String;
    .end local v39    # "phoneNumbers":Ljava/util/Vector;
    .end local v48    # "uniqueNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v51    # "values":Landroid/content/ContentValues;
    :cond_31
    const/16 v52, -0x1

    move/from16 v0, v27

    move/from16 v1, v52

    if-eq v0, v1, :cond_32

    .line 1194
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    aput-object v52, v43, v27

    .line 1201
    :cond_32
    const/16 v52, -0x1

    move/from16 v0, v19

    move/from16 v1, v52

    if-eq v0, v1, :cond_34

    .line 1202
    invoke-virtual/range {v22 .. v22}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEmailAddresses()Ljava/util/Vector;

    move-result-object v7

    .line 1203
    .restart local v7    # "addresses":Ljava/util/Vector;
    if-eqz v7, :cond_34

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v52

    if-lez v52, :cond_34

    .line 1204
    const/16 v52, 0x0

    move/from16 v0, v52

    invoke-virtual {v7, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .line 1205
    .local v18, "email":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v52

    aput-object v52, v43, v19

    .line 1207
    invoke-static/range {v18 .. v18}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)Landroid/content/ContentValues;

    move-result-object v51

    .line 1209
    .restart local v51    # "values":Landroid/content/ContentValues;
    const/16 v52, -0x1

    move/from16 v0, v21

    move/from16 v1, v52

    if-eq v0, v1, :cond_33

    .line 1210
    const-string v52, "data2"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v21

    .line 1212
    :cond_33
    const/16 v52, -0x1

    move/from16 v0, v20

    move/from16 v1, v52

    if-eq v0, v1, :cond_34

    .line 1213
    const-string v52, "data3"

    invoke-virtual/range {v51 .. v52}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v52

    aput-object v52, v43, v20

    .line 1217
    .end local v7    # "addresses":Ljava/util/Vector;
    .end local v18    # "email":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    .end local v51    # "values":Landroid/content/ContentValues;
    :cond_34
    new-instance v52, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;

    move-object/from16 v0, v52

    move-object/from16 v1, v44

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider$GalSortKey;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, v45

    move-object/from16 v1, v52

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1218
    const/4 v4, 0x1

    .line 1219
    add-int/lit8 v26, v26, 0x1

    .line 1221
    :cond_35
    if-eqz v4, :cond_13

    .line 1222
    add-int/lit8 v11, v11, 0x1

    .line 1223
    move/from16 v0, p5

    if-le v11, v0, :cond_13

    .line 1237
    .end local v4    # "addedContact":Z
    .end local v8    # "alternateDisplayName":Ljava/lang/String;
    .end local v14    # "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v22    # "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .end local v29    # "lookupKey":Ljava/lang/String;
    .end local v35    # "p":I
    .end local v43    # "row":[Ljava/lang/Object;
    .end local v44    # "sortName":Ljava/lang/String;
    :cond_36
    :goto_9
    new-instance v13, Landroid/database/MatrixCursor;

    invoke-virtual/range {v45 .. v45}, Ljava/util/TreeMap;->size()I

    move-result v52

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-direct {v13, v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 1239
    .local v13, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual/range {v45 .. v45}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v52

    invoke-interface/range {v52 .. v52}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .restart local v25    # "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v52

    if-eqz v52, :cond_37

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, [Ljava/lang/Object;

    .line 1240
    .restart local v43    # "row":[Ljava/lang/Object;
    move-object/from16 v0, v43

    invoke-virtual {v13, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_a

    .line 1228
    .end local v13    # "cursor":Landroid/database/MatrixCursor;
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v43    # "row":[Ljava/lang/Object;
    :catch_0
    move-exception v17

    .line 1229
    .local v17, "e":Ljava/io/IOException;
    const-string v52, "GalProvider"

    const-string v53, "Exception when parsing feed"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 1230
    .end local v17    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v17

    .line 1231
    .local v17, "e":Lcom/google/wireless/gdata2/parser/ParseException;
    const-string v52, "GalProvider"

    const-string v53, "Exception when parsing feed"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 1242
    .end local v17    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    .restart local v13    # "cursor":Landroid/database/MatrixCursor;
    .restart local v25    # "i$":Ljava/util/Iterator;
    :cond_37
    return-object v13
.end method

.method private createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;
    .locals 7
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "contactId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/ContentValues;",
            "J)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 732
    .local p1, "columnMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-nez p2, :cond_1

    .line 733
    const/4 v3, 0x0

    .line 743
    :cond_0
    return-object v3

    .line 735
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v5

    new-array v3, v5, [Ljava/lang/Object;

    .line 736
    .local v3, "row":[Ljava/lang/Object;
    const-string v5, "contact_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {p0, v3, p1, v5, v6}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 737
    const-string v5, "raw_contact_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {p0, v3, p1, v5, v6}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 738
    invoke-virtual {p2}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 739
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 740
    .local v0, "columnName":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 741
    .local v4, "value":Ljava/lang/Object;
    invoke-direct {p0, v3, p1, v0, v4}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private execute(Ljava/util/concurrent/Callable;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 9
    .param p2, "threadName"    # Ljava/lang/String;
    .param p3, "timeout"    # J
    .param p5, "timeUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    new-instance v2, Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable;

    invoke-direct {v2, p0, p1}, Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable;-><init>(Lcom/google/android/syncadapters/contacts/GalProvider;Ljava/util/concurrent/Callable;)V

    .line 271
    .local v2, "futureCallable":Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable;, "Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable<TT;>;"
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 272
    .local v1, "future":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    invoke-virtual {v2, v1}, Lcom/google/android/syncadapters/contacts/GalProvider$FutureCallable;->setFuture(Ljava/util/concurrent/FutureTask;)V

    .line 274
    iget-object v6, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    monitor-enter v6

    .line 275
    :try_start_0
    iget-object v5, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 277
    const-string v5, "GalProvider"

    const/4 v7, 0x2

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 278
    const-string v5, "GalProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Currently running tasks: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    const/16 v7, 0x8

    if-le v5, v7, :cond_1

    .line 282
    const-string v5, "GalProvider"

    const-string v7, "Too many tasks, canceling one"

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v5, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mActiveTasks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/FutureTask;

    .line 284
    .local v3, "task":Ljava/util/concurrent/FutureTask;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    goto :goto_0

    .line 286
    .end local v3    # "task":Ljava/util/concurrent/FutureTask;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    const-string v5, "GalProvider"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 288
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Starting task "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_2
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 291
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 293
    :try_start_2
    const-string v5, "GalProvider"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 294
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Getting future "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_3
    invoke-virtual {v1, p3, p4, p5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v5

    .line 310
    :goto_1
    return-object v5

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Gal task was interrupted: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 300
    const/4 v5, 0x0

    goto :goto_1

    .line 301
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 302
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Gal task threw an exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 303
    const/4 v5, 0x0

    goto :goto_1

    .line 304
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v0

    .line 305
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Gal task timed out: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 307
    const/4 v5, 0x0

    goto :goto_1

    .line 308
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_3
    move-exception v0

    .line 309
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    const-string v5, "GalProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Gal task was cancelled: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 7
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 883
    iget-object v5, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v6, "com.google"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 885
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 886
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 890
    .end local v0    # "account":Landroid/accounts/Account;
    :goto_1
    return-object v0

    .line 885
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 890
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static getAlternateDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1309
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getName()Lcom/google/wireless/gdata2/contacts/data/Name;

    move-result-object v2

    .line 1310
    .local v2, "name":Lcom/google/wireless/gdata2/contacts/data/Name;
    if-eqz v2, :cond_2

    .line 1311
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/Name;->getGivenName()Ljava/lang/String;

    move-result-object v1

    .line 1312
    .local v1, "givenName":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFamilyName()Ljava/lang/String;

    move-result-object v0

    .line 1314
    .local v0, "familyName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1315
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1321
    .end local v0    # "familyName":Ljava/lang/String;
    .end local v1    # "givenName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 1317
    .restart local v0    # "familyName":Ljava/lang/String;
    .restart local v1    # "givenName":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .end local v0    # "familyName":Ljava/lang/String;
    .end local v1    # "givenName":Ljava/lang/String;
    :cond_2
    move-object v0, p1

    .line 1321
    goto :goto_0
.end method

.method private static getDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/util/Pair;
    .locals 12
    .param p0, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/gdata2/contacts/data/ContactEntry;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1255
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getName()Lcom/google/wireless/gdata2/contacts/data/Name;

    move-result-object v6

    .line 1256
    .local v6, "name":Lcom/google/wireless/gdata2/contacts/data/Name;
    if-eqz v6, :cond_4

    .line 1257
    invoke-virtual {v6}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFullName()Ljava/lang/String;

    move-result-object v4

    .line 1258
    .local v4, "fullName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/google/wireless/gdata2/contacts/data/Name;->getGivenName()Ljava/lang/String;

    move-result-object v5

    .line 1259
    .local v5, "givenName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/google/wireless/gdata2/contacts/data/Name;->getFamilyName()Ljava/lang/String;

    move-result-object v3

    .line 1260
    .local v3, "familyName":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1262
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1263
    move-object v1, v4

    .line 1271
    .local v1, "displayName":Ljava/lang/String;
    :goto_0
    const/16 v9, 0x28

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    .line 1297
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v3    # "familyName":Ljava/lang/String;
    .end local v4    # "fullName":Ljava/lang/String;
    .end local v5    # "givenName":Ljava/lang/String;
    :goto_1
    return-object v9

    .line 1264
    .restart local v3    # "familyName":Ljava/lang/String;
    .restart local v4    # "fullName":Ljava/lang/String;
    .restart local v5    # "givenName":Ljava/lang/String;
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 1265
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "displayName":Ljava/lang/String;
    goto :goto_0

    .line 1266
    .end local v1    # "displayName":Ljava/lang/String;
    :cond_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1267
    move-object v1, v5

    .restart local v1    # "displayName":Ljava/lang/String;
    goto :goto_0

    .line 1269
    .end local v1    # "displayName":Ljava/lang/String;
    :cond_3
    move-object v1, v3

    .restart local v1    # "displayName":Ljava/lang/String;
    goto :goto_0

    .line 1276
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v3    # "familyName":Ljava/lang/String;
    .end local v4    # "fullName":Ljava/lang/String;
    .end local v5    # "givenName":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEmailAddresses()Ljava/util/Vector;

    move-result-object v0

    .line 1277
    .local v0, "addresses":Ljava/util/Vector;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v9

    if-lez v9, :cond_6

    .line 1278
    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .line 1279
    .local v2, "email":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1280
    .restart local v1    # "displayName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1281
    invoke-virtual {v2}, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 1283
    :cond_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 1284
    const/16 v9, 0xa

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    goto :goto_1

    .line 1289
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v2    # "email":Lcom/google/wireless/gdata2/contacts/data/EmailAddress;
    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPhoneNumbers()Ljava/util/Vector;

    move-result-object v8

    .line 1290
    .local v8, "phoneNumbers":Ljava/util/Vector;
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v9

    if-lez v9, :cond_7

    .line 1291
    invoke-virtual {v8, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .line 1292
    .local v7, "phoneNumber":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    invoke-virtual {v7}, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .line 1293
    .restart local v1    # "displayName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1294
    const/16 v9, 0x14

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    goto/16 :goto_1

    .line 1297
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneNumber":Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;
    :cond_7
    invoke-static {v11, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    goto/16 :goto_1
.end method

.method private getEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .locals 17
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "lookup"    # Ljava/lang/String;

    .prologue
    .line 827
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/GalProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "connectivity"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    .line 829
    .local v6, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v9

    .line 830
    .local v9, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v13

    if-nez v13, :cond_1

    .line 831
    :cond_0
    const-string v13, "GalProvider"

    const-string v14, "getEntry(): Not connected"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    const/4 v13, 0x0

    .line 877
    :goto_0
    return-object v13

    .line 835
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/syncadapters/contacts/GalProvider;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 836
    .local v2, "account":Landroid/accounts/Account;
    if-nez v2, :cond_2

    .line 837
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Account not found: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    const/4 v13, 0x0

    goto :goto_0

    .line 841
    :cond_2
    new-instance v3, Lcom/google/android/syncadapters/contacts/AuthInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v14, "cp"

    invoke-direct {v3, v13, v2, v14}, Lcom/google/android/syncadapters/contacts/AuthInfo;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 843
    .local v3, "authInfo":Lcom/google/android/syncadapters/contacts/AuthInfo;
    const/16 v13, 0x40

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 844
    .local v10, "p":I
    if-gez v10, :cond_3

    .line 845
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid account name: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    const/4 v13, 0x0

    goto :goto_0

    .line 849
    :cond_3
    add-int/lit8 v13, v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 850
    .local v7, "domain":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "https://www.google.com/m8/feeds/gal/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/full/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 851
    .local v12, "url":Ljava/lang/String;
    const-string v13, "GalProvider"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 852
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getEntry("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "): GData URL: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    :cond_4
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 855
    .local v4, "callingId":J
    iget-object v13, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v13

    const/high16 v14, 0x400000

    or-int/2addr v13, v14

    const/high16 v14, 0x80000

    or-int/2addr v13, v14

    or-int/lit8 v11, v13, 0x2

    .line 860
    .local v11, "tag":I
    :try_start_0
    invoke-static {v11}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 861
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/syncadapters/contacts/GalProvider;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    sget-object v14, Lcom/google/android/syncadapters/contacts/GalProvider;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    invoke-virtual {v14}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getEntryClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v12, v15, v0}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->getEntry(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v13

    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 873
    const/4 v14, 0x1

    invoke-static {v11, v14}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 874
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 875
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 866
    :catch_0
    move-exception v8

    .line 867
    .local v8, "e":Ljava/io/IOException;
    :try_start_1
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception when getting feed "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 873
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 874
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 875
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 877
    .end local v8    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 868
    :catch_1
    move-exception v8

    .line 869
    .local v8, "e":Lcom/google/wireless/gdata2/GDataException;
    :try_start_2
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception when getting feed "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 873
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 874
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 875
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 870
    .end local v8    # "e":Lcom/google/wireless/gdata2/GDataException;
    :catch_2
    move-exception v13

    .line 873
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 874
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 875
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 873
    :catchall_0
    move-exception v13

    const/4 v14, 0x1

    invoke-static {v11, v14}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 874
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 875
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v13
.end method

.method private getFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/parser/GDataParser;
    .locals 17
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "projection"    # Ljava/lang/String;

    .prologue
    .line 766
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/GalProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "connectivity"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    .line 768
    .local v6, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v9

    .line 769
    .local v9, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v13

    if-nez v13, :cond_1

    .line 770
    :cond_0
    const-string v13, "GalProvider"

    const-string v14, "getFeed(): Not connected"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    const/4 v13, 0x0

    .line 823
    :goto_0
    return-object v13

    .line 774
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/syncadapters/contacts/GalProvider;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 775
    .local v2, "account":Landroid/accounts/Account;
    if-nez v2, :cond_2

    .line 776
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Account not found: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    const/4 v13, 0x0

    goto :goto_0

    .line 780
    :cond_2
    new-instance v3, Lcom/google/android/syncadapters/contacts/AuthInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v14, "cp"

    invoke-direct {v3, v13, v2, v14}, Lcom/google/android/syncadapters/contacts/AuthInfo;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 782
    .local v3, "authInfo":Lcom/google/android/syncadapters/contacts/AuthInfo;
    const/16 v13, 0x40

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 783
    .local v10, "p":I
    if-gez v10, :cond_3

    .line 784
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid account name: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    const/4 v13, 0x0

    goto :goto_0

    .line 788
    :cond_3
    add-int/lit8 v13, v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 789
    .local v7, "domain":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "https://www.google.com/m8/feeds/gal/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 790
    .local v12, "url":Ljava/lang/String;
    const-string v13, "GalProvider"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 791
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getFeed("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "): GData URL: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    :cond_4
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 794
    .local v4, "callingId":J
    iget-object v13, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v13

    const/high16 v14, 0x400000

    or-int/2addr v13, v14

    const/high16 v14, 0x80000

    or-int/2addr v13, v14

    or-int/lit8 v11, v13, 0x1

    .line 799
    .local v11, "tag":I
    :try_start_0
    invoke-static {v11}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 800
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/syncadapters/contacts/GalProvider;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    sget-object v14, Lcom/google/android/syncadapters/contacts/GalProvider;->sContactHandler:Lcom/google/android/syncadapters/contacts/ContactHandler;

    invoke-virtual {v14}, Lcom/google/android/syncadapters/contacts/ContactHandler;->getEntryClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v12, v15, v0}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->getParserForFeed(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/parser/GDataParser;
    :try_end_0
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 819
    const/4 v14, 0x1

    invoke-static {v11, v14}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 805
    :catch_0
    move-exception v8

    .line 806
    .local v8, "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :try_start_1
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "ForbiddenException when getting feed. Probably a non Gmail consumer account."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/client/ForbiddenException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 809
    const/4 v13, 0x0

    .line 819
    const/4 v14, 0x1

    invoke-static {v11, v14}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 810
    .end local v8    # "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :catch_1
    move-exception v8

    .line 811
    .local v8, "e":Ljava/io/IOException;
    :try_start_2
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception when getting feed "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 819
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 823
    .end local v8    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 812
    :catch_2
    move-exception v8

    .line 813
    .local v8, "e":Lcom/google/wireless/gdata2/client/ResourceGoneException;
    :try_start_3
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception when getting feed "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 819
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 814
    .end local v8    # "e":Lcom/google/wireless/gdata2/client/ResourceGoneException;
    :catch_3
    move-exception v8

    .line 815
    .local v8, "e":Lcom/google/wireless/gdata2/GDataException;
    :try_start_4
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception when getting feed "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 819
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 816
    .end local v8    # "e":Lcom/google/wireless/gdata2/GDataException;
    :catch_4
    move-exception v13

    .line 819
    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 819
    :catchall_0
    move-exception v13

    const/4 v14, 0x1

    invoke-static {v11, v14}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 820
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 821
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v13
.end method

.method private getPhotoUri(Ljava/lang/String;Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Z)Landroid/net/Uri;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "entry"    # Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    .param p3, "isThumbnail"    # Z

    .prologue
    .line 1325
    invoke-virtual {p2}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLinkPhotoHref()Ljava/lang/String;

    move-result-object v0

    .line 1326
    .local v0, "photoUrl":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1327
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "com.google.contacts.gal.provider"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "photo"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1335
    .local v1, "uri":Landroid/net/Uri;
    if-eqz p3, :cond_0

    .line 1336
    invoke-direct {p0, v1}, Lcom/google/android/syncadapters/contacts/GalProvider;->getThumbnailUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 1341
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getThumbnailUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1, "photoUrl"    # Landroid/net/Uri;

    .prologue
    .line 1345
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sz"

    iget v2, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mThumbnailSize:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private handleContact([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 24
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "lookupKey"    # Ljava/lang/String;
    .param p4, "contactId"    # J

    .prologue
    .line 541
    const-string v21, "GalProvider"

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 542
    const-string v21, "GalProvider"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "handleContact("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_0
    if-nez p2, :cond_2

    .line 546
    const-string v21, "GalProvider"

    const-string v22, "Account name cannot be null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    new-instance v6, Landroid/database/MatrixCursor;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 720
    :cond_1
    :goto_0
    return-object v6

    .line 549
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->getEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/contacts/data/ContactEntry;

    move-result-object v8

    .line 550
    .local v8, "entry":Lcom/google/wireless/gdata2/contacts/data/ContactEntry;
    if-nez v8, :cond_3

    .line 551
    const-string v21, "GalProvider"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Can\'t find entry: accountName: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " lookupKey: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    new-instance v6, Landroid/database/MatrixCursor;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_0

    .line 556
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    .line 558
    .local v5, "columnMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v11, v0, :cond_4

    .line 559
    aget-object v21, p1, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 562
    :cond_4
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v18

    .line 563
    .local v18, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/Object;>;"
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getName()Lcom/google/wireless/gdata2/contacts/data/Name;

    move-result-object v21

    if-eqz v21, :cond_5

    .line 564
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    .line 568
    :cond_5
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getContent()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_6

    .line 569
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNoteDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    .line 573
    :cond_6
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newNicknameDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    .line 576
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newBirthdayDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    .line 579
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newFocusMiscDataValues(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    .line 582
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEmailAddresses()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    .line 583
    .local v14, "object":Ljava/lang/Object;
    check-cast v14, Lcom/google/wireless/gdata2/contacts/data/EmailAddress;

    .end local v14    # "object":Ljava/lang/Object;
    invoke-static {v14}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/EmailAddress;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_2

    .line 588
    :cond_7
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPostalAddresses()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 589
    .local v13, "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/StructuredPostalAddress;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_3

    .line 594
    :cond_8
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getImAddresses()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 595
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/ImAddress;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ImAddress;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_4

    .line 600
    :cond_9
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getOrganizations()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_a

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 601
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/Organization;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Organization;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_5

    .line 605
    :cond_a
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getGroups()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_b
    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .restart local v13    # "o":Ljava/lang/Object;
    move-object v10, v13

    .line 606
    check-cast v10, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;

    .line 607
    .local v10, "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    invoke-virtual {v10}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->isDeleted()Z

    move-result v21

    if-nez v21, :cond_b

    .line 611
    invoke-static {v10}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_6

    .line 615
    .end local v10    # "group":Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
    .end local v13    # "o":Ljava/lang/Object;
    :cond_c
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getHobbies()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 616
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Ljava/lang/String;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromHobbyEntryElement(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_7

    .line 620
    :cond_d
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getUserDefinedFields()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_e

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 621
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/UserDefinedField;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_8

    .line 625
    :cond_e
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getLanguages()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_f

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 626
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/Language;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Language;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_9

    .line 631
    :cond_f
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getExternalIds()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_a
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_10

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 632
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/ExternalId;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/ExternalId;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_a

    .line 637
    :cond_10
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getCalendarLinks()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_11

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 638
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/CalendarLink;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/CalendarLink;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_b

    .line 642
    :cond_11
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getJots()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_c
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_12

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 643
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/Jot;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Jot;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_c

    .line 647
    :cond_12
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getPhoneNumbers()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_d
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 648
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/PhoneNumber;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_d

    .line 653
    :cond_13
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getWebSites()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_e
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_14

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 654
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/WebSite;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/WebSite;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_e

    .line 659
    :cond_14
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getRelations()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_15

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 660
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/Relation;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Relation;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_f

    .line 665
    :cond_15
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getEvents()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_10
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_16

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 666
    .restart local v13    # "o":Ljava/lang/Object;
    check-cast v13, Lcom/google/wireless/gdata2/contacts/data/Event;

    .end local v13    # "o":Ljava/lang/Object;
    invoke-static {v13}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/contacts/data/Event;)Landroid/content/ContentValues;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_10

    .line 670
    :cond_16
    invoke-virtual {v8}, Lcom/google/wireless/gdata2/contacts/data/ContactEntry;->getExtendedProperties()Ljava/util/Vector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_17
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_18

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .restart local v13    # "o":Ljava/lang/Object;
    move-object/from16 v16, v13

    .line 671
    check-cast v16, Lcom/google/wireless/gdata2/data/ExtendedProperty;

    .line 672
    .local v16, "property":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    const-string v21, "android"

    invoke-virtual/range {v16 .. v16}, Lcom/google/wireless/gdata2/data/ExtendedProperty;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17

    .line 673
    invoke-static/range {v16 .. v16}, Lcom/google/android/syncadapters/contacts/ContactHandler;->newDataValuesFromEntryElement(Lcom/google/wireless/gdata2/data/ExtendedProperty;)Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_11
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_18

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/content/ContentValues;

    .line 674
    .local v20, "v":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-wide/from16 v2, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->createContactRow(Ljava/util/Map;Landroid/content/ContentValues;J)[Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->addRow(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    goto :goto_11

    .line 680
    .end local v13    # "o":Ljava/lang/Object;
    .end local v16    # "property":Lcom/google/wireless/gdata2/data/ExtendedProperty;
    .end local v20    # "v":Landroid/content/ContentValues;
    :cond_18
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_1c

    .line 681
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    .line 682
    .local v9, "first":[Ljava/lang/Object;
    const-string v21, "contact_id"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 684
    invoke-static {v8}, Lcom/google/android/syncadapters/contacts/GalProvider;->getDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;)Landroid/util/Pair;

    move-result-object v7

    .line 685
    .local v7, "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v21, v0

    check-cast v21, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-static {v8, v0}, Lcom/google/android/syncadapters/contacts/GalProvider;->getAlternateDisplayName(Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 688
    .local v4, "alternateDisplayName":Ljava/lang/String;
    const-string v21, "raw_contact_id"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 689
    const-string v21, "display_name"

    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 690
    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v21, v0

    if-eqz v21, :cond_19

    .line 691
    const-string v21, "display_name_source"

    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 694
    :cond_19
    const-string v21, "display_name_alt"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v9, v5, v1, v4}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 696
    const-string v21, "account_type"

    const-string v22, "com.google"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 698
    const-string v21, "account_name"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 699
    const-string v21, "raw_contact_is_read_only"

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 700
    const-string v21, "is_read_only"

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 701
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v21

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->getPhotoUri(Ljava/lang/String;Lcom/google/wireless/gdata2/contacts/data/ContactEntry;Z)Landroid/net/Uri;

    move-result-object v15

    .line 702
    .local v15, "photoUrl":Landroid/net/Uri;
    if-eqz v15, :cond_1a

    .line 703
    const-string v21, "photo_uri"

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 704
    const-string v21, "photo_thumb_uri"

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/syncadapters/contacts/GalProvider;->getThumbnailUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v9, v5, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 708
    :cond_1a
    new-instance v6, Landroid/database/MatrixCursor;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 710
    .local v6, "cursor":Landroid/database/MatrixCursor;
    const/4 v11, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v19

    .local v19, "rowsSize":I
    :goto_12
    move/from16 v0, v19

    if-ge v11, v0, :cond_1

    .line 711
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [Ljava/lang/Object;

    .line 712
    .local v17, "row":[Ljava/lang/Object;
    if-nez v17, :cond_1b

    .line 710
    :goto_13
    add-int/lit8 v11, v11, 0x1

    goto :goto_12

    .line 715
    :cond_1b
    const-string v21, "data_id"

    add-int/lit8 v22, v11, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/syncadapters/contacts/GalProvider;->putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 716
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_13

    .line 720
    .end local v4    # "alternateDisplayName":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/MatrixCursor;
    .end local v7    # "displayName":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v9    # "first":[Ljava/lang/Object;
    .end local v15    # "photoUrl":Landroid/net/Uri;
    .end local v17    # "row":[Ljava/lang/Object;
    .end local v19    # "rowsSize":I
    :cond_1c
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private handleDirectories([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 431
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v14, "com.google"

    invoke-virtual {v13, v14}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 432
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v6, Landroid/database/MatrixCursor;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 433
    .local v6, "cursor":Landroid/database/MatrixCursor;
    if-eqz v2, :cond_b

    .line 434
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v10, v3

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_b

    aget-object v1, v3, v9

    .line 435
    .local v1, "account":Landroid/accounts/Account;
    const-string v13, "GalProvider"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 436
    const-string v13, "GalProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleDirectories: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    iget-object v11, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 440
    .local v11, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/syncadapters/contacts/GalProvider;->mayHaveGalSupport(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 434
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 443
    :cond_1
    move-object/from16 v0, p1

    array-length v13, v0

    new-array v12, v13, [Ljava/lang/Object;

    .line 445
    .local v12, "row":[Ljava/lang/Object;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v8, v13, :cond_a

    .line 446
    aget-object v5, p1, v8

    .line 447
    .local v5, "column":Ljava/lang/String;
    const-string v13, "accountName"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 448
    aput-object v11, v12, v8

    .line 445
    :cond_2
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 449
    :cond_3
    const-string v13, "accountType"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 450
    iget-object v13, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v13, v12, v8

    goto :goto_3

    .line 451
    :cond_4
    const-string v13, "typeResourceId"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 452
    const v13, 0x7f050001

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v8

    goto :goto_3

    .line 453
    :cond_5
    const-string v13, "displayName"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 456
    const/16 v13, 0x40

    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 457
    .local v4, "atIndex":I
    const/4 v13, -0x1

    if-eq v4, v13, :cond_6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x2

    if-ge v4, v13, :cond_6

    .line 458
    add-int/lit8 v13, v4, 0x1

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v13}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v7

    .line 460
    .local v7, "firstLetter":C
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v4, 0x2

    invoke-virtual {v11, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v8

    goto :goto_3

    .line 462
    .end local v7    # "firstLetter":C
    :cond_6
    aput-object v11, v12, v8

    goto :goto_3

    .line 464
    .end local v4    # "atIndex":I
    :cond_7
    const-string v13, "exportSupport"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 465
    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v8

    goto :goto_3

    .line 466
    :cond_8
    const-string v13, "shortcutSupport"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 467
    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v8

    goto/16 :goto_3

    .line 468
    :cond_9
    const-string v13, "photoSupport"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 469
    const/4 v13, 0x3

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v8

    goto/16 :goto_3

    .line 472
    .end local v5    # "column":Ljava/lang/String;
    :cond_a
    invoke-virtual {v6, v12}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 475
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v8    # "i":I
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "row":[Ljava/lang/Object;
    :cond_b
    return-object v6
.end method

.method private handleFilter([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;
    .locals 14
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "numResults"    # I
    .param p6, "emailFilter"    # Z
    .param p7, "phoneFilter"    # Z
    .param p8, "removeDuplicates"    # Z

    .prologue
    .line 487
    const-string v1, "GalProvider"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    const-string v1, "GalProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleFilter("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 492
    :cond_1
    const/4 v10, 0x0

    .line 535
    :goto_0
    return-object v10

    .line 495
    :cond_2
    if-nez p2, :cond_3

    .line 496
    const-string v1, "GalProvider"

    const-string v2, "Account name cannot be null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    const/4 v10, 0x0

    goto :goto_0

    .line 506
    :cond_3
    move/from16 v13, p5

    .line 507
    .local v13, "queryNumResults":I
    if-eqz p7, :cond_4

    .line 508
    mul-int/lit8 v13, v13, 0x3

    .line 510
    :cond_4
    const/16 v1, 0x64

    if-le v13, v1, :cond_5

    .line 511
    const/16 v13, 0x64

    .line 513
    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 514
    .local v12, "query":Ljava/lang/StringBuilder;
    const-string v1, "?q="

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string v1, "&max-results="

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 516
    const-string v11, "/autocompletepic/"

    .line 517
    .local v11, "projectionPath":Ljava/lang/String;
    if-nez p6, :cond_6

    .line 518
    const-string v1, "&group=false"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :cond_6
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/autocompletepic/"

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/syncadapters/contacts/GalProvider;->getFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/parser/GDataParser;

    move-result-object v3

    .line 521
    .local v3, "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    if-nez v3, :cond_7

    .line 522
    const/4 v10, 0x0

    goto :goto_0

    :cond_7
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    .line 526
    :try_start_0
    invoke-direct/range {v1 .. v9}, Lcom/google/android/syncadapters/contacts/GalProvider;->buildGalResultCursor([Ljava/lang/String;Lcom/google/wireless/gdata2/parser/GDataParser;Ljava/lang/String;Ljava/lang/String;IZZZ)Landroid/database/Cursor;

    move-result-object v10

    .line 529
    .local v10, "cursor":Landroid/database/Cursor;
    const-string v1, "GalProvider"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 530
    const-string v1, "GalProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleFilter("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "): "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " matches"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    :cond_8
    invoke-interface {v3}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V

    goto/16 :goto_0

    .end local v10    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    invoke-interface {v3}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V

    throw v1
.end method

.method private mayHaveGalSupport(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 761
    const-string v0, "@gmail.com"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "@googlemail.com"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openFile(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 365
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 366
    .local v7, "accountName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 368
    .local v10, "photoUrl":Ljava/lang/String;
    const-string v0, "GalProvider"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "GalProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    invoke-direct {p0, v7}, Lcom/google/android/syncadapters/contacts/GalProvider;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v6

    .line 372
    .local v6, "account":Landroid/accounts/Account;
    if-nez v6, :cond_1

    .line 373
    const-string v0, "GalProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Account not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v12

    .line 400
    :goto_0
    return-object v0

    .line 377
    :cond_1
    new-instance v8, Lcom/google/android/syncadapters/contacts/AuthInfo;

    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "cp"

    invoke-direct {v8, v0, v6, v1}, Lcom/google/android/syncadapters/contacts/AuthInfo;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 378
    .local v8, "authInfo":Lcom/google/android/syncadapters/contacts/AuthInfo;
    iget-object v0, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v0

    const/high16 v1, 0x400000

    or-int/2addr v0, v1

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    or-int/lit8 v11, v0, 0x3

    .line 383
    .local v11, "tag":I
    :try_start_0
    invoke-static {v11}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    invoke-virtual {v8}, Lcom/google/android/syncadapters/contacts/AuthInfo;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;->getMediaEntryAsStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 386
    .local v4, "inputStream":Ljava/io/InputStream;
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v5, Lcom/google/android/syncadapters/contacts/GalProvider$PhotoDataWriter;

    invoke-direct {v5, v7, v10}, Lcom/google/android/syncadapters/contacts/GalProvider$PhotoDataWriter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/syncadapters/contacts/GalProvider;->openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Lcom/google/wireless/gdata2/client/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/wireless/gdata2/GDataException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 397
    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 388
    .end local v4    # "inputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v9

    .line 389
    .local v9, "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    :try_start_1
    const-string v0, "GalProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): Not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .end local v9    # "e":Lcom/google/wireless/gdata2/client/ResourceNotFoundException;
    :goto_1
    move-object v0, v12

    .line 400
    goto :goto_0

    .line 390
    :catch_1
    move-exception v9

    .line 391
    .local v9, "e":Ljava/io/IOException;
    :try_start_2
    const-string v0, "GalProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 397
    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_1

    .line 392
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 393
    .local v9, "e":Lcom/google/wireless/gdata2/GDataException;
    :try_start_3
    const-string v0, "GalProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 397
    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_1

    .line 394
    .end local v9    # "e":Lcom/google/wireless/gdata2/GDataException;
    :catch_3
    move-exception v0

    .line 397
    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_1

    .line 397
    :catchall_0
    move-exception v0

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 398
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0
.end method

.method private putContactRowValue([Ljava/lang/Object;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "row"    # [Ljava/lang/Object;
    .param p3, "columnName"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 751
    .local p2, "columnMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-nez p4, :cond_1

    .line 758
    :cond_0
    :goto_0
    return-void

    .line 754
    :cond_1
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 755
    .local v0, "integer":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 756
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput-object p4, p1, v1

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 415
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 405
    sget-object v1, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 406
    .local v0, "match":I
    packed-switch v0, :pswitch_data_0

    .line 410
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 408
    :pswitch_0
    const-string v1, "vnd.android.cursor.item/contact"

    goto :goto_0

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 420
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/syncadapters/contacts/GalProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 180
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mAccountManager:Landroid/accounts/AccountManager;

    .line 181
    new-instance v1, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    new-instance v2, Lcom/google/android/common/gdata2/AndroidGDataClient;

    const-string v3, "Android-GData-Contacts/1.3"

    const-string v4, ""

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/common/gdata2/AndroidGDataClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlContactsGDataParserFactory;

    new-instance v4, Lcom/google/android/common/gdata2/AndroidXmlParserFactory;

    invoke-direct {v4}, Lcom/google/android/common/gdata2/AndroidXmlParserFactory;-><init>()V

    invoke-direct {v3, v4}, Lcom/google/wireless/gdata2/contacts/parser/xml/XmlContactsGDataParserFactory;-><init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;)V

    invoke-direct {v1, v2, v3}, Lcom/google/wireless/gdata2/contacts/client/ContactsClient;-><init>(Lcom/google/wireless/gdata2/client/GDataClient;Lcom/google/wireless/gdata2/client/GDataParserFactory;)V

    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mContactsClient:Lcom/google/wireless/gdata2/contacts/client/ContactsClient;

    .line 184
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/syncadapters/contacts/GalProvider;->mThumbnailSize:I

    .line 187
    const/4 v1, 0x1

    return v1
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 353
    const-string v0, "r"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mode must be \"r\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_0
    new-instance v2, Lcom/google/android/syncadapters/contacts/GalProvider$4;

    invoke-direct {v2, p0, p1}, Lcom/google/android/syncadapters/contacts/GalProvider$4;-><init>(Lcom/google/android/syncadapters/contacts/GalProvider;Landroid/net/Uri;)V

    const-string v3, "GalOpenFileThread"

    const-wide/16 v4, 0x4e20

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/syncadapters/contacts/GalProvider;->execute(Ljava/util/concurrent/Callable;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 27
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 193
    const-string v2, "GalProvider"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    const-string v2, "GalProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "query: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_0
    sget-object v2, Lcom/google/android/syncadapters/contacts/GalProvider;->sURIMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 198
    .local v4, "match":I
    packed-switch v4, :pswitch_data_0

    .line 264
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 200
    :pswitch_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/syncadapters/contacts/GalProvider;->handleDirectories([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 206
    :pswitch_1
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 207
    .local v6, "accountName":Ljava/lang/String;
    const-string v2, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 210
    .local v25, "limit":Ljava/lang/String;
    if-eqz v25, :cond_1

    :try_start_0
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v26

    .line 218
    .local v26, "numResults":I
    :goto_1
    const/4 v2, 0x5

    if-ne v4, v2, :cond_2

    const/4 v10, 0x1

    .line 219
    .local v10, "removeDuplicates":Z
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 221
    .local v7, "filter":Ljava/lang/String;
    move/from16 v9, v26

    .line 223
    .local v9, "finalNumResults":I
    new-instance v2, Lcom/google/android/syncadapters/contacts/GalProvider$1;

    move-object/from16 v3, p0

    move-object/from16 v5, p2

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v10}, Lcom/google/android/syncadapters/contacts/GalProvider$1;-><init>(Lcom/google/android/syncadapters/contacts/GalProvider;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    const-string v13, "GalFilterThread"

    const-wide/16 v14, 0x1388

    sget-object v16, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v11, p0

    move-object v12, v2

    invoke-direct/range {v11 .. v16}, Lcom/google/android/syncadapters/contacts/GalProvider;->execute(Ljava/util/concurrent/Callable;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    goto :goto_0

    .line 210
    .end local v7    # "filter":Ljava/lang/String;
    .end local v9    # "finalNumResults":I
    .end local v10    # "removeDuplicates":Z
    .end local v26    # "numResults":I
    :cond_1
    const/16 v26, 0x14

    goto :goto_1

    .line 211
    :catch_0
    move-exception v18

    .line 212
    .local v18, "e":Ljava/lang/NumberFormatException;
    const-string v2, "GalProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "query: invalid limit parameter: \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/16 v26, 0x14

    .restart local v26    # "numResults":I
    goto :goto_1

    .line 218
    .end local v18    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    .line 238
    .end local v6    # "accountName":Ljava/lang/String;
    .end local v25    # "limit":Ljava/lang/String;
    .end local v26    # "numResults":I
    :pswitch_2
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 239
    .restart local v6    # "accountName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 241
    .local v15, "lookupKey":Ljava/lang/String;
    new-instance v12, Lcom/google/android/syncadapters/contacts/GalProvider$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v12, v0, v1, v6, v15}, Lcom/google/android/syncadapters/contacts/GalProvider$2;-><init>(Lcom/google/android/syncadapters/contacts/GalProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "GalContactThread"

    const-wide/16 v14, 0x1388

    sget-object v16, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v16}, Lcom/google/android/syncadapters/contacts/GalProvider;->execute(Ljava/util/concurrent/Callable;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    goto/16 :goto_0

    .line 251
    .end local v6    # "accountName":Ljava/lang/String;
    .end local v15    # "lookupKey":Ljava/lang/String;
    :pswitch_3
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 252
    .restart local v6    # "accountName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 253
    .restart local v15    # "lookupKey":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 255
    .local v16, "contactId":J
    new-instance v11, Lcom/google/android/syncadapters/contacts/GalProvider$3;

    move-object/from16 v12, p0

    move-object/from16 v13, p2

    move-object v14, v6

    invoke-direct/range {v11 .. v17}, Lcom/google/android/syncadapters/contacts/GalProvider$3;-><init>(Lcom/google/android/syncadapters/contacts/GalProvider;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    const-string v21, "GalContactWithIdThread"

    const-wide/16 v22, 0x1388

    sget-object v24, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v19, p0

    move-object/from16 v20, v11

    invoke-direct/range {v19 .. v24}, Lcom/google/android/syncadapters/contacts/GalProvider;->execute(Ljava/util/concurrent/Callable;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    goto/16 :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 425
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
