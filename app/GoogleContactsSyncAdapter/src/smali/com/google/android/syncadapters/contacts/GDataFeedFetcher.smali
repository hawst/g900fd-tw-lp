.class public Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;
.super Ljava/lang/Object;
.source "GDataFeedFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mAuthToken:Ljava/lang/String;

.field private volatile mAuthenticationFailed:Z

.field protected final mClient:Lcom/google/wireless/gdata2/client/GDataServiceClient;

.field private volatile mConnectionFailed:Z

.field private final mEntryClass:Ljava/lang/Class;

.field private final mEntryEndMarker:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mFeed:Lcom/google/wireless/gdata2/data/Feed;

.field protected final mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

.field private final mFeedUrl:Ljava/lang/String;

.field private volatile mForcedClosed:Z

.field private final mLogTag:Ljava/lang/String;

.field private mMaxResults:I

.field private volatile mNumUnparsableEntries:I

.field private volatile mPartialSyncUnavailable:Z

.field private volatile mResourceUnavailable:Z

.field private volatile mResumptionFailed:Z

.field private volatile mRetryAfter:J

.field private volatile mThread:Ljava/lang/Thread;

.field private volatile mUnparsableFeed:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/wireless/gdata2/client/GDataServiceClient;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Landroid/util/Pair;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;I)V
    .locals 3
    .param p1, "logTag"    # Ljava/lang/String;
    .param p2, "client"    # Lcom/google/wireless/gdata2/client/GDataServiceClient;
    .param p3, "entryClass"    # Ljava/lang/Class;
    .param p6, "url"    # Ljava/lang/String;
    .param p7, "authToken"    # Ljava/lang/String;
    .param p8, "feedSyncState"    # Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;
    .param p9, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/wireless/gdata2/client/GDataServiceClient;",
            "Ljava/lang/Class;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            ">;>;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/gdata2/data/Entry;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p4, "entryQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Landroid/util/Pair<Ljava/lang/Integer;Lcom/google/wireless/gdata2/data/Entry;>;>;"
    .local p5, "entryEndMarker":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Lcom/google/wireless/gdata2/data/Entry;>;"
    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 70
    iput-object p1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    .line 71
    iput-object p4, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    .line 72
    iput-object p6, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedUrl:Ljava/lang/String;

    .line 73
    iput-object p7, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthToken:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mClient:Lcom/google/wireless/gdata2/client/GDataServiceClient;

    .line 75
    iput-object p5, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    .line 76
    iput-object p3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryClass:Ljava/lang/Class;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    .line 79
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mConnectionFailed:Z

    .line 80
    iput v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mNumUnparsableEntries:I

    .line 81
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mUnparsableFeed:Z

    .line 82
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthenticationFailed:Z

    .line 83
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResumptionFailed:Z

    .line 84
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mPartialSyncUnavailable:Z

    .line 85
    iput-object p8, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    .line 86
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mRetryAfter:J

    .line 88
    iput-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResourceUnavailable:Z

    .line 90
    if-gtz p9, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxResults cannot be zero or negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    iput p9, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mMaxResults:I

    .line 94
    return-void
.end method

.method private fetchFeed()V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getQueryParams()Lcom/google/wireless/gdata2/client/QueryParams;

    move-result-object v11

    .line 156
    .local v11, "params":Lcom/google/wireless/gdata2/client/QueryParams;
    const/4 v6, 0x0

    .line 157
    .local v6, "expectedFirstId":Ljava/lang/String;
    const/4 v14, 0x1

    .line 158
    .local v14, "startIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->hasIndexOfLastFetched()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIdOfLastFetched()Ljava/lang/String;

    move-result-object v6

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getIndexOfLastFetched()I

    move-result v14

    .line 170
    invoke-virtual {v11, v14}, Lcom/google/wireless/gdata2/client/QueryParams;->setStartIndex(I)V

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "resuming download from index "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", id "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :cond_0
    add-int/lit8 v16, v14, -0x1

    .line 177
    .local v16, "totalResultsFetched":I
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/google/wireless/gdata2/client/QueryParams;->generateQueryUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, "feedUrlToQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Starting to make a new fetch request for feed url: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " with MaxResults: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mMaxResults:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_1
    const/4 v9, 0x0

    .line 185
    .local v9, "lastEntryId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mClient:Lcom/google/wireless/gdata2/client/GDataServiceClient;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryClass:Ljava/lang/Class;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthToken:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v7, v2, v3}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->getParserForFeed(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/gdata2/parser/GDataParser;
    :try_end_0
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v12

    .line 188
    .local v12, "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    :try_start_1
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->parseFeedEnvelope()Lcom/google/wireless/gdata2/data/Feed;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getTotalResults()I

    move-result v15

    .line 191
    .local v15, "totalResults":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v8

    .line 192
    .local v8, "itemsPerPage":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Got totalResults = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Got itemsPerPage = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_2
    const/4 v13, 0x0

    .line 197
    .local v13, "resultsFetched":I
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_10

    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->hasMoreData()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v17

    if-eqz v17, :cond_10

    .line 199
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0
    :try_end_2
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v17, :cond_6

    .line 240
    :try_start_3
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V
    :try_end_3
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .local v10, "numExpectedEntries":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 306
    .end local v7    # "feedUrlToQuery":Ljava/lang/String;
    .end local v8    # "itemsPerPage":I
    .end local v9    # "lastEntryId":Ljava/lang/String;
    .end local v12    # "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    .end local v13    # "resultsFetched":I
    .end local v15    # "totalResults":I
    :cond_4
    :goto_3
    return-void

    .line 297
    .restart local v7    # "feedUrlToQuery":Ljava/lang/String;
    .restart local v8    # "itemsPerPage":I
    .restart local v9    # "lastEntryId":Ljava/lang/String;
    .restart local v12    # "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    .restart local v13    # "resultsFetched":I
    .restart local v15    # "totalResults":I
    :cond_5
    const/4 v10, 0x0

    goto :goto_2

    .line 202
    :cond_6
    add-int/lit8 v16, v16, 0x1

    .line 203
    add-int/lit8 v13, v13, 0x1

    .line 204
    const/16 v17, 0x0

    :try_start_4
    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Lcom/google/wireless/gdata2/parser/GDataParser;->readNextEntry(Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;

    move-result-object v5

    .line 205
    .local v5, "entry":Lcom/google/wireless/gdata2/data/Entry;
    if-eqz v6, :cond_b

    .line 206
    invoke-virtual {v5}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Expected the first id to be "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " but instead read "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", abandoning resumption of feed "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResumptionFailed:Z
    :try_end_4
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 240
    :try_start_5
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V
    :try_end_5
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_8
    const/4 v10, 0x0

    goto :goto_4

    .line 213
    :cond_9
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "ExpectedFirstId is correct"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_a
    const/4 v6, 0x0

    .line 221
    :cond_b
    if-lt v13, v8, :cond_c

    move/from16 v0, v16

    if-ne v0, v15, :cond_e

    .line 223
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "enqueing entry "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v5}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 229
    :cond_e
    invoke-virtual {v5}, Lcom/google/wireless/gdata2/data/Entry;->getId()Ljava/lang/String;
    :try_end_6
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v9

    goto/16 :goto_1

    .line 230
    .end local v5    # "entry":Lcom/google/wireless/gdata2/data/Entry;
    :catch_0
    move-exception v4

    .line 232
    .local v4, "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "parse error, ignoring entry"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 233
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mNumUnparsableEntries:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mNumUnparsableEntries:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 240
    .end local v4    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    .end local v8    # "itemsPerPage":I
    .end local v13    # "resultsFetched":I
    .end local v15    # "totalResults":I
    :catchall_0
    move-exception v17

    :try_start_8
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V

    throw v17
    :try_end_8
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 258
    .end local v7    # "feedUrlToQuery":Ljava/lang/String;
    .end local v9    # "lastEntryId":Ljava/lang/String;
    .end local v12    # "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    :catch_1
    move-exception v4

    .line 259
    .restart local v4    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "parse error, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 260
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mUnparsableFeed:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 236
    .end local v4    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    .restart local v7    # "feedUrlToQuery":Ljava/lang/String;
    .restart local v8    # "itemsPerPage":I
    .restart local v9    # "lastEntryId":Ljava/lang/String;
    .restart local v12    # "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    .restart local v13    # "resultsFetched":I
    .restart local v15    # "totalResults":I
    :cond_10
    move/from16 v0, v16

    if-lt v0, v15, :cond_13

    .line 240
    :try_start_a
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V
    :try_end_a
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_a .. :try_end_a} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_a .. :try_end_a} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_a .. :try_end_a} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_12
    const/4 v10, 0x0

    goto :goto_6

    .line 240
    :cond_13
    :try_start_b
    invoke-interface {v12}, Lcom/google/wireless/gdata2/parser/GDataParser;->close()V

    .line 244
    move-object v6, v9

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_14

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Updated expectedFirstId to: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_14
    move/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/google/wireless/gdata2/client/QueryParams;->setStartIndex(I)V
    :try_end_b
    .catch Lcom/google/wireless/gdata2/parser/ParseException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/google/wireless/gdata2/client/HttpException; {:try_start_b .. :try_end_b} :catch_3
    .catch Lcom/google/wireless/gdata2/client/AuthenticationException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lcom/google/wireless/gdata2/client/ResourceNotModifiedException; {:try_start_b .. :try_end_b} :catch_5
    .catch Lcom/google/wireless/gdata2/client/ForbiddenException; {:try_start_b .. :try_end_b} :catch_6
    .catch Lcom/google/wireless/gdata2/client/ResourceGoneException; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 256
    add-int/lit8 v16, v16, -0x1

    .line 257
    goto/16 :goto_0

    .line 297
    .end local v7    # "feedUrlToQuery":Ljava/lang/String;
    .end local v8    # "itemsPerPage":I
    .end local v9    # "lastEntryId":Ljava/lang/String;
    .end local v12    # "parser":Lcom/google/wireless/gdata2/parser/GDataParser;
    .end local v13    # "resultsFetched":I
    .end local v15    # "totalResults":I
    .restart local v4    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :cond_15
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 261
    .end local v4    # "e":Lcom/google/wireless/gdata2/parser/ParseException;
    :catch_2
    move-exception v4

    .line 262
    .local v4, "e":Ljava/io/IOException;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IO error, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 263
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mConnectionFailed:Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_16

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_17
    const/4 v10, 0x0

    goto :goto_7

    .line 264
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 265
    .local v4, "e":Lcom/google/wireless/gdata2/client/HttpException;
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Http error, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    invoke-virtual {v4}, Lcom/google/wireless/gdata2/client/HttpException;->getStatusCode()I

    move-result v17

    sparse-switch v17, :sswitch_data_0

    .line 278
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mUnparsableFeed:Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 296
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_18

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 268
    :sswitch_0
    const/16 v17, 0x1

    :try_start_e
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthenticationFailed:Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_8

    .line 296
    .end local v4    # "e":Lcom/google/wireless/gdata2/client/HttpException;
    :catchall_1
    move-exception v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v18, v0

    if-eqz v18, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "finished fetching "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " out of "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " entries, adding the end marker"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v18, v0

    if-nez v18, :cond_1a

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :cond_1a
    throw v17

    .line 271
    .restart local v4    # "e":Lcom/google/wireless/gdata2/client/HttpException;
    :sswitch_1
    const/16 v17, 0x1

    :try_start_f
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mPartialSyncUnavailable:Z

    goto/16 :goto_8

    .line 274
    :sswitch_2
    invoke-virtual {v4}, Lcom/google/wireless/gdata2/client/HttpException;->getRetryAfter()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mRetryAfter:J

    .line 275
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResourceUnavailable:Z

    goto/16 :goto_8

    .line 297
    :cond_1b
    const/4 v10, 0x0

    goto/16 :goto_9

    .line 281
    .end local v4    # "e":Lcom/google/wireless/gdata2/client/HttpException;
    :catch_4
    move-exception v4

    .line 282
    .local v4, "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Authentication error, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthenticationFailed:Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1c

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_1d
    const/4 v10, 0x0

    goto :goto_b

    .line 284
    .end local v4    # "e":Lcom/google/wireless/gdata2/client/AuthenticationException;
    :catch_5
    move-exception v4

    .line 286
    .local v4, "e":Lcom/google/wireless/gdata2/client/ResourceNotModifiedException;
    :try_start_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "ResourceNotModifiedException, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1e

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_1f
    const/4 v10, 0x0

    goto :goto_c

    .line 287
    .end local v4    # "e":Lcom/google/wireless/gdata2/client/ResourceNotModifiedException;
    :catch_6
    move-exception v4

    .line 289
    .local v4, "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    const/16 v17, 0x1

    :try_start_11
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mUnparsableFeed:Z

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "ForbiddenException, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_20

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_21
    const/4 v10, 0x0

    goto :goto_d

    .line 291
    .end local v4    # "e":Lcom/google/wireless/gdata2/client/ForbiddenException;
    :catch_7
    move-exception v4

    .line 292
    .local v4, "e":Lcom/google/wireless/gdata2/client/ResourceGoneException;
    :try_start_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "ResourceGoneException, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 293
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mPartialSyncUnavailable:Z
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_22

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    if-eqz v17, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/gdata2/data/Feed;->getItemsPerPage()I

    move-result v10

    .line 298
    .restart local v10    # "numExpectedEntries":I
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "finished fetching "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " out of "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entries, adding the end marker"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v10    # "numExpectedEntries":I
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 302
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mEntryEndMarker:Landroid/util/Pair;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 297
    :cond_23
    const/4 v10, 0x0

    goto :goto_e

    .end local v4    # "e":Lcom/google/wireless/gdata2/client/ResourceGoneException;
    :cond_24
    const/4 v10, 0x0

    goto/16 :goto_a

    .line 266
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x19a -> :sswitch_1
        0x1f7 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mThread:Ljava/lang/Thread;

    .line 113
    .local v0, "thread":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 114
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    .line 115
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 117
    :cond_0
    return-void
.end method

.method public getFeed()Lcom/google/wireless/gdata2/data/Feed;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeed:Lcom/google/wireless/gdata2/data/Feed;

    return-object v0
.end method

.method public getNumUnparsableEntries()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mNumUnparsableEntries:I

    return v0
.end method

.method protected getQueryParams()Lcom/google/wireless/gdata2/client/QueryParams;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->getQueryParams(Z)Lcom/google/wireless/gdata2/client/QueryParams;

    move-result-object v0

    return-object v0
.end method

.method protected getQueryParams(Z)Lcom/google/wireless/gdata2/client/QueryParams;
    .locals 6
    .param p1, "noIncremental"    # Z

    .prologue
    .line 130
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mClient:Lcom/google/wireless/gdata2/client/GDataServiceClient;

    invoke-virtual {v3}, Lcom/google/wireless/gdata2/client/GDataServiceClient;->createQueryParams()Lcom/google/wireless/gdata2/client/QueryParams;

    move-result-object v2

    .line 131
    .local v2, "params":Lcom/google/wireless/gdata2/client/QueryParams;
    iget v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mMaxResults:I

    invoke-virtual {v2, v3}, Lcom/google/wireless/gdata2/client/QueryParams;->setMaxResults(I)V

    .line 134
    const-string v3, "orderby"

    const-string v4, "lastmodified"

    invoke-virtual {v2, v3, v4}, Lcom/google/wireless/gdata2/client/QueryParams;->setParamValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v3, "sortorder"

    const-string v4, "ascending"

    invoke-virtual {v2, v3, v4}, Lcom/google/wireless/gdata2/client/QueryParams;->setParamValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getDoIncrementalSync()Z

    move-result v0

    .line 138
    .local v0, "doIncrementalSync":Z
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;

    invoke-virtual {v3}, Lcom/google/android/syncadapters/contacts/ContactsProto$FeedState;->getFeedUpdatedTime()Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "feedUpdatedTime":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 142
    iget-object v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "feedFetch, incremental "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", updatedMin "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",  feed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mFeedUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 147
    invoke-virtual {v2, v1}, Lcom/google/wireless/gdata2/client/QueryParams;->setUpdatedMin(Ljava/lang/String;)V

    .line 148
    const-string v3, "requirealldeleted"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Lcom/google/wireless/gdata2/client/QueryParams;->setParamValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v3, "showdeleted"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Lcom/google/wireless/gdata2/client/QueryParams;->setParamValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_1
    return-object v2
.end method

.method public getRetryAfter()J
    .locals 2

    .prologue
    .line 363
    iget-wide v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mRetryAfter:J

    return-wide v0
.end method

.method public isAuthenticationFailed()Z
    .locals 1

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mAuthenticationFailed:Z

    return v0
.end method

.method public isConnectionFailed()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mConnectionFailed:Z

    return v0
.end method

.method public isPartialSyncUnavailable()Z
    .locals 1

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mPartialSyncUnavailable:Z

    return v0
.end method

.method public isResourceUnavailable()Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResourceUnavailable:Z

    return v0
.end method

.method public isUnparsableFeed()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mUnparsableFeed:Z

    return v0
.end method

.method public resumptionFailed()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mResumptionFailed:Z

    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 97
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mThread:Ljava/lang/Thread;

    .line 98
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 100
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->fetchFeed()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mThread:Ljava/lang/Thread;

    .line 105
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GDataFeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_1
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 104
    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mThread:Ljava/lang/Thread;

    .line 105
    iget-boolean v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GDataFeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mThread:Ljava/lang/Thread;

    .line 105
    iget-boolean v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 106
    :cond_3
    iget-object v1, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GDataFeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/syncadapters/contacts/GDataFeedFetcher;->mForcedClosed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v0
.end method
