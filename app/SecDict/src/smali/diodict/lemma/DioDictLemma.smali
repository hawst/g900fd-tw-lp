.class public Ldiodict/lemma/DioDictLemma;
.super Ljava/lang/Object;
.source "DioDictLemma.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldiodict/lemma/DioDictLemma$LemmaToken;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$diodict$lemma$LemmaLanguage:[I


# instance fields
.field private result:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$diodict$lemma$LemmaLanguage()[I
    .locals 3

    .prologue
    .line 10
    sget-object v0, Ldiodict/lemma/DioDictLemma;->$SWITCH_TABLE$diodict$lemma$LemmaLanguage:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ldiodict/lemma/LemmaLanguage;->values()[Ldiodict/lemma/LemmaLanguage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->CHINESE:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_13

    :goto_1
    :try_start_1
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->DANISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_12

    :goto_2
    :try_start_2
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->DUTCH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_11

    :goto_3
    :try_start_3
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->ENGLISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_10

    :goto_4
    :try_start_4
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->FINNISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_f

    :goto_5
    :try_start_5
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->FRENCH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_e

    :goto_6
    :try_start_6
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->GERMAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_d

    :goto_7
    :try_start_7
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->GREEK:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_c

    :goto_8
    :try_start_8
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->INDONESIAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_b

    :goto_9
    :try_start_9
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->ITALIAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_a

    :goto_a
    :try_start_a
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->JAPANESE:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_9

    :goto_b
    :try_start_b
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->KOREAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_8

    :goto_c
    :try_start_c
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_7

    :goto_d
    :try_start_d
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->POLISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_6

    :goto_e
    :try_start_e
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_5

    :goto_f
    :try_start_f
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->RUSSIAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_4

    :goto_10
    :try_start_10
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->SPANISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_3

    :goto_11
    :try_start_11
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->SWEDISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_2

    :goto_12
    :try_start_12
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->TURKISH:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_1

    :goto_13
    :try_start_13
    sget-object v1, Ldiodict/lemma/LemmaLanguage;->UKRAINIAN:Ldiodict/lemma/LemmaLanguage;

    invoke-virtual {v1}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_0

    :goto_14
    sput-object v0, Ldiodict/lemma/DioDictLemma;->$SWITCH_TABLE$diodict$lemma$LemmaLanguage:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_14

    :catch_1
    move-exception v1

    goto :goto_13

    :catch_2
    move-exception v1

    goto :goto_12

    :catch_3
    move-exception v1

    goto :goto_11

    :catch_4
    move-exception v1

    goto :goto_10

    :catch_5
    move-exception v1

    goto :goto_f

    :catch_6
    move-exception v1

    goto :goto_e

    :catch_7
    move-exception v1

    goto :goto_d

    :catch_8
    move-exception v1

    goto :goto_c

    :catch_9
    move-exception v1

    goto :goto_b

    :catch_a
    move-exception v1

    goto :goto_a

    :catch_b
    move-exception v1

    goto/16 :goto_9

    :catch_c
    move-exception v1

    goto/16 :goto_8

    :catch_d
    move-exception v1

    goto/16 :goto_7

    :catch_e
    move-exception v1

    goto/16 :goto_6

    :catch_f
    move-exception v1

    goto/16 :goto_5

    :catch_10
    move-exception v1

    goto/16 :goto_4

    :catch_11
    move-exception v1

    goto/16 :goto_3

    :catch_12
    move-exception v1

    goto/16 :goto_2

    :catch_13
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "stlport_shared"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    const-string v0, "nltk"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 15
    const-string v0, "jma"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 16
    const-string v0, "diolemma"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native getLemma([B)[B
.end method

.method private native lemmaInit(Ljava/lang/String;Ljava/lang/String;I)Z
.end method

.method private native lemmaRelease()V
.end method

.method private parse(Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "result"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldiodict/lemma/DioDictLemma$LemmaToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v9, "listLemTok":Ljava/util/List;, "Ljava/util/List<Ldiodict/lemma/DioDictLemma$LemmaToken;>;"
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 58
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 59
    .local v10, "tokens":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v10

    if-lt v8, v0, :cond_0

    .line 76
    return-object v9

    .line 60
    :cond_0
    aget-object v0, v10, v8

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 61
    .local v6, "attrs":[Ljava/lang/String;
    array-length v0, v6

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 63
    :try_start_0
    new-instance v0, Ldiodict/lemma/DioDictLemma$LemmaToken;

    const/4 v1, 0x0

    aget-object v1, v6, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v1, 0x1

    aget-object v3, v6, v1

    const/4 v1, 0x2

    aget-object v4, v6, v1

    const/4 v1, 0x3

    aget-object v5, v6, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldiodict/lemma/DioDictLemma$LemmaToken;-><init>(Ldiodict/lemma/DioDictLemma;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 65
    :catch_0
    move-exception v7

    .line 66
    .local v7, "e":Ljava/lang/NumberFormatException;
    const-string v0, "DioDictLemma"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lemma index is not NumberFormatted. :: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, v10, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v0, "DioDictLemma"

    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 72
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    const-string v0, "DioDictLemma"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lack of token entries. :: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, v10, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ldiodict/lemma/DioDictLemma;->lemmaRelease()V

    .line 151
    return-void
.end method

.method public lemmatize(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "sent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldiodict/lemma/DioDictLemma$LemmaToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 155
    .local v0, "bytes":[B
    invoke-direct {p0, v0}, Ldiodict/lemma/DioDictLemma;->getLemma([B)[B

    move-result-object v1

    .line 156
    .local v1, "stem":[B
    if-nez v1, :cond_0

    .line 157
    new-instance v2, Ljava/lang/String;

    const-string v3, "No result"

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Ldiodict/lemma/DioDictLemma;->result:Ljava/lang/String;

    .line 160
    :goto_0
    iget-object v2, p0, Ldiodict/lemma/DioDictLemma;->result:Ljava/lang/String;

    invoke-direct {p0, v2}, Ldiodict/lemma/DioDictLemma;->parse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    return-object v2

    .line 159
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    iput-object v2, p0, Ldiodict/lemma/DioDictLemma;->result:Ljava/lang/String;

    goto :goto_0
.end method

.method public open(Ljava/lang/String;Ljava/lang/String;Ldiodict/lemma/LemmaLanguage;)Z
    .locals 3
    .param p1, "inifile"    # Ljava/lang/String;
    .param p2, "dbconv"    # Ljava/lang/String;
    .param p3, "eLangCode"    # Ldiodict/lemma/LemmaLanguage;

    .prologue
    .line 81
    invoke-static {}, Ldiodict/lemma/DioDictLemma;->$SWITCH_TABLE$diodict$lemma$LemmaLanguage()[I

    move-result-object v1

    invoke-virtual {p3}, Ldiodict/lemma/LemmaLanguage;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 143
    const/4 v0, 0x5

    .line 146
    .local v0, "langcode":I
    :goto_0
    invoke-direct {p0, p1, p2, v0}, Ldiodict/lemma/DioDictLemma;->lemmaInit(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    return v1

    .line 83
    .end local v0    # "langcode":I
    :pswitch_0
    const/4 v0, 0x0

    .line 84
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 86
    .end local v0    # "langcode":I
    :pswitch_1
    const/4 v0, 0x1

    .line 87
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 89
    .end local v0    # "langcode":I
    :pswitch_2
    const/4 v0, 0x2

    .line 90
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 92
    .end local v0    # "langcode":I
    :pswitch_3
    const/4 v0, 0x3

    .line 93
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 95
    .end local v0    # "langcode":I
    :pswitch_4
    const/4 v0, 0x4

    .line 96
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 98
    .end local v0    # "langcode":I
    :pswitch_5
    const/4 v0, 0x5

    .line 99
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 101
    .end local v0    # "langcode":I
    :pswitch_6
    const/4 v0, 0x6

    .line 102
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 104
    .end local v0    # "langcode":I
    :pswitch_7
    const/4 v0, 0x7

    .line 105
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 107
    .end local v0    # "langcode":I
    :pswitch_8
    const/16 v0, 0x8

    .line 108
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 110
    .end local v0    # "langcode":I
    :pswitch_9
    const/16 v0, 0x9

    .line 111
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 113
    .end local v0    # "langcode":I
    :pswitch_a
    const/16 v0, 0xa

    .line 114
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 116
    .end local v0    # "langcode":I
    :pswitch_b
    const/16 v0, 0xb

    .line 117
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 119
    .end local v0    # "langcode":I
    :pswitch_c
    const/16 v0, 0xc

    .line 120
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 122
    .end local v0    # "langcode":I
    :pswitch_d
    const/16 v0, 0xd

    .line 123
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 125
    .end local v0    # "langcode":I
    :pswitch_e
    const/16 v0, 0xe

    .line 126
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 128
    .end local v0    # "langcode":I
    :pswitch_f
    const/16 v0, 0xf

    .line 129
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 131
    .end local v0    # "langcode":I
    :pswitch_10
    const/16 v0, 0x10

    .line 132
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 134
    .end local v0    # "langcode":I
    :pswitch_11
    const/16 v0, 0x11

    .line 135
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 137
    .end local v0    # "langcode":I
    :pswitch_12
    const/16 v0, 0x12

    .line 138
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 140
    .end local v0    # "langcode":I
    :pswitch_13
    const/16 v0, 0x13

    .line 141
    .restart local v0    # "langcode":I
    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
