.class public Ldiodict/lemma/DioDictLemma$LemmaToken;
.super Ljava/lang/Object;
.source "DioDictLemma.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ldiodict/lemma/DioDictLemma;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LemmaToken"
.end annotation


# instance fields
.field private index:I

.field private lemma:Ljava/lang/String;

.field private pos:Ljava/lang/String;

.field private surface:Ljava/lang/String;

.field final synthetic this$0:Ldiodict/lemma/DioDictLemma;


# direct methods
.method public constructor <init>(Ldiodict/lemma/DioDictLemma;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "index"    # Ljava/lang/Integer;
    .param p3, "surface"    # Ljava/lang/String;
    .param p4, "lemma"    # Ljava/lang/String;
    .param p5, "pos"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->this$0:Ldiodict/lemma/DioDictLemma;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->index:I

    .line 26
    iput-object p3, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->surface:Ljava/lang/String;

    .line 27
    const-string v0, "^.*[\u00b9\u00b2\u00b3\u2074\u2075\u2076\u2077\u2078]$"

    invoke-virtual {p4, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    .line 30
    :cond_0
    iput-object p4, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->lemma:Ljava/lang/String;

    .line 31
    iput-object p5, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->pos:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->index:I

    return v0
.end method

.method public getLemma()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->lemma:Ljava/lang/String;

    return-object v0
.end method

.method public getPos()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->pos:Ljava/lang/String;

    return-object v0
.end method

.method public getSurface()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldiodict/lemma/DioDictLemma$LemmaToken;->surface:Ljava/lang/String;

    return-object v0
.end method
