.class public final enum Ldiodict/lemma/LemmaLanguage;
.super Ljava/lang/Enum;
.source "LemmaLanguage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldiodict/lemma/LemmaLanguage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHINESE:Ldiodict/lemma/LemmaLanguage;

.field public static final enum DANISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum DUTCH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum ENGLISH:Ldiodict/lemma/LemmaLanguage;

.field private static final synthetic ENUM$VALUES:[Ldiodict/lemma/LemmaLanguage;

.field public static final enum FINNISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum FRENCH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum GERMAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum GREEK:Ldiodict/lemma/LemmaLanguage;

.field public static final enum INDONESIAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum ITALIAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum JAPANESE:Ldiodict/lemma/LemmaLanguage;

.field public static final enum KOREAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum POLISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

.field public static final enum RUSSIAN:Ldiodict/lemma/LemmaLanguage;

.field public static final enum SPANISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum SWEDISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum TURKISH:Ldiodict/lemma/LemmaLanguage;

.field public static final enum UKRAINIAN:Ldiodict/lemma/LemmaLanguage;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "KOREAN"

    invoke-direct {v0, v1, v4, v4}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->KOREAN:Ldiodict/lemma/LemmaLanguage;

    .line 5
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "JAPANESE"

    invoke-direct {v0, v1, v5, v5}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->JAPANESE:Ldiodict/lemma/LemmaLanguage;

    .line 6
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "CHINESE"

    invoke-direct {v0, v1, v6, v6}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->CHINESE:Ldiodict/lemma/LemmaLanguage;

    .line 7
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "DANISH"

    invoke-direct {v0, v1, v7, v7}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->DANISH:Ldiodict/lemma/LemmaLanguage;

    .line 8
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "DUTCH"

    invoke-direct {v0, v1, v8, v8}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->DUTCH:Ldiodict/lemma/LemmaLanguage;

    .line 9
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "ENGLISH"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->ENGLISH:Ldiodict/lemma/LemmaLanguage;

    .line 10
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "FINNISH"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->FINNISH:Ldiodict/lemma/LemmaLanguage;

    .line 11
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "FRENCH"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->FRENCH:Ldiodict/lemma/LemmaLanguage;

    .line 12
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "GERMAN"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->GERMAN:Ldiodict/lemma/LemmaLanguage;

    .line 13
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "GREEK"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->GREEK:Ldiodict/lemma/LemmaLanguage;

    .line 14
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "INDONESIAN"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->INDONESIAN:Ldiodict/lemma/LemmaLanguage;

    .line 15
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "ITALIAN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->ITALIAN:Ldiodict/lemma/LemmaLanguage;

    .line 16
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "NORWEGIAN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

    .line 17
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "POLISH"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->POLISH:Ldiodict/lemma/LemmaLanguage;

    .line 18
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "PORTUGUESE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

    .line 19
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "RUSSIAN"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->RUSSIAN:Ldiodict/lemma/LemmaLanguage;

    .line 20
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "SPANISH"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->SPANISH:Ldiodict/lemma/LemmaLanguage;

    .line 21
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "SWEDISH"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->SWEDISH:Ldiodict/lemma/LemmaLanguage;

    .line 22
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "TURKISH"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->TURKISH:Ldiodict/lemma/LemmaLanguage;

    .line 23
    new-instance v0, Ldiodict/lemma/LemmaLanguage;

    const-string v1, "UKRAINIAN"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Ldiodict/lemma/LemmaLanguage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->UKRAINIAN:Ldiodict/lemma/LemmaLanguage;

    .line 3
    const/16 v0, 0x14

    new-array v0, v0, [Ldiodict/lemma/LemmaLanguage;

    sget-object v1, Ldiodict/lemma/LemmaLanguage;->KOREAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v1, v0, v4

    sget-object v1, Ldiodict/lemma/LemmaLanguage;->JAPANESE:Ldiodict/lemma/LemmaLanguage;

    aput-object v1, v0, v5

    sget-object v1, Ldiodict/lemma/LemmaLanguage;->CHINESE:Ldiodict/lemma/LemmaLanguage;

    aput-object v1, v0, v6

    sget-object v1, Ldiodict/lemma/LemmaLanguage;->DANISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v1, v0, v7

    sget-object v1, Ldiodict/lemma/LemmaLanguage;->DUTCH:Ldiodict/lemma/LemmaLanguage;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->ENGLISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->FINNISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->FRENCH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->GERMAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->GREEK:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->INDONESIAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->ITALIAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->POLISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->RUSSIAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->SPANISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->SWEDISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->TURKISH:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ldiodict/lemma/LemmaLanguage;->UKRAINIAN:Ldiodict/lemma/LemmaLanguage;

    aput-object v2, v0, v1

    sput-object v0, Ldiodict/lemma/LemmaLanguage;->ENUM$VALUES:[Ldiodict/lemma/LemmaLanguage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Ldiodict/lemma/LemmaLanguage;->value:I

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldiodict/lemma/LemmaLanguage;
    .locals 1

    .prologue
    .line 1
    const-class v0, Ldiodict/lemma/LemmaLanguage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldiodict/lemma/LemmaLanguage;

    return-object v0
.end method

.method public static values()[Ldiodict/lemma/LemmaLanguage;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->ENUM$VALUES:[Ldiodict/lemma/LemmaLanguage;

    array-length v1, v0

    new-array v2, v1, [Ldiodict/lemma/LemmaLanguage;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
