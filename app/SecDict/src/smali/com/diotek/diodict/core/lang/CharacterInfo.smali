.class public Lcom/diotek/diodict/core/lang/CharacterInfo;
.super Ljava/lang/Object;
.source "CharacterInfo.java"


# static fields
.field public static final CP_SPECIAL:I = 0xff

.field private static final VOWEL:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "A"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "E"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "i"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "I"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "O"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "U"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "@"

    aput-object v2, v0, v1

    sput-object v0, Lcom/diotek/diodict/core/lang/CharacterInfo;->VOWEL:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static checkSpaceInFlashcardName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x20

    .line 153
    const-string v1, ""

    .line 154
    .local v1, "convertName":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 155
    const/4 v2, 0x0

    .line 164
    :goto_0
    return-object v2

    .line 159
    :cond_0
    const/16 v2, 0xa0

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 160
    const/16 v0, 0x2002

    .local v0, "c":C
    :goto_1
    const/16 v2, 0x200b

    if-gt v0, v2, :cond_1

    .line 161
    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 160
    add-int/lit8 v2, v0, 0x1

    int-to-char v0, v2

    goto :goto_1

    .line 163
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 164
    goto :goto_0
.end method

.method private static convertCodeBlockToLaguageInfo(I)I
    .locals 1
    .param p0, "codeBlock"    # I

    .prologue
    .line 110
    packed-switch p0, :pswitch_data_0

    .line 122
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 114
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 118
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 120
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getLanguage(Ljava/lang/String;)I
    .locals 6
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 74
    const/16 v1, 0xff

    .line 76
    .local v1, "codeblock":I
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "bufword":Ljava/lang/String;
    const-string v4, "[?+]"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    const-string v4, "[*+]"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 82
    .local v3, "len":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 83
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/diotek/diodict/core/lang/CodeBlock;->getCodeBlock(C)I

    move-result v1

    .line 106
    :cond_0
    invoke-static {v1}, Lcom/diotek/diodict/core/lang/CharacterInfo;->convertCodeBlockToLaguageInfo(I)I

    move-result v4

    return v4

    .line 86
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 87
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/diotek/diodict/core/lang/CodeBlock;->isLatin(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 89
    const/4 v1, 0x6

    .line 94
    :cond_2
    const/4 v4, 0x6

    if-eq v1, v4, :cond_0

    .line 95
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    .line 96
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/diotek/diodict/core/lang/CodeBlock;->getCodeBlock(C)I

    move-result v1

    .line 97
    const/4 v4, 0x5

    if-ne v1, v4, :cond_0

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 86
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static hasLatinCharacter(Ljava/lang/String;)Z
    .locals 3
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 55
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 60
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/diotek/diodict/core/lang/CodeBlock;->isLatin(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    const/4 v1, 0x1

    goto :goto_0

    .line 59
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static hasWildcardCharacter(Ljava/lang/String;)Z
    .locals 3
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 43
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 48
    .local v0, "charaters":[C
    const/16 v2, 0x2a

    invoke-static {v0, v2}, Lcom/google/common/primitives/Chars;->contains([CC)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x3f

    invoke-static {v0, v2}, Lcom/google/common/primitives/Chars;->contains([CC)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isConsistOfConsonant(Ljava/lang/String;)Z
    .locals 4
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 132
    sget-object v3, Lcom/diotek/diodict/core/lang/CharacterInfo;->VOWEL:[Ljava/lang/String;

    array-length v1, v3

    .line 133
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 134
    sget-object v3, Lcom/diotek/diodict/core/lang/CharacterInfo;->VOWEL:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    :cond_0
    :goto_1
    return v2

    .line 133
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 139
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_3

    .line 140
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 144
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static isUnifiedCodeSetString(Ljava/lang/String;)Z
    .locals 10
    .param p0, "searchWord"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 175
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 215
    :cond_0
    :goto_0
    return v6

    .line 179
    :cond_1
    const-string v0, ""

    .line 181
    .local v0, "bufword":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 182
    const-string v8, "[?+]"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    const-string v8, "[*+]"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    .line 189
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-ne v8, v7, :cond_2

    move v6, v7

    .line 190
    goto :goto_0

    .line 193
    :cond_2
    const/4 v2, 0x0

    .line 194
    .local v2, "i":I
    const/4 v3, -0x1

    .line 195
    .local v3, "langFirst":I
    const/4 v4, -0x1

    .line 196
    .local v4, "langNext":I
    move-object v5, v0

    .line 199
    .local v5, "word":Ljava/lang/String;
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_3

    const/4 v8, -0x1

    if-ne v3, v8, :cond_3

    .line 200
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/diotek/diodict/core/lang/CharacterInfo;->getLanguage(Ljava/lang/String;)I

    move-result v3

    .line 199
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 203
    :cond_3
    const/4 v1, 0x0

    .line 204
    .local v1, "ch":C
    :goto_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_6

    .line 205
    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 206
    const/16 v8, 0x20

    if-ne v1, v8, :cond_5

    .line 204
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 209
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/diotek/diodict/core/lang/CharacterInfo;->getLanguage(Ljava/lang/String;)I

    move-result v4

    .line 210
    if-eq v4, v3, :cond_4

    goto :goto_0

    :cond_6
    move v6, v7

    .line 215
    goto :goto_0
.end method

.method public static isWildcardCharacter(C)Z
    .locals 2
    .param p0, "c"    # C

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 31
    .local v0, "result":Z
    const/16 v1, 0x2a

    if-eq p0, v1, :cond_0

    const/16 v1, 0x3f

    if-ne p0, v1, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 34
    :cond_1
    return v0
.end method
