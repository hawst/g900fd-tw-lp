.class public Lcom/diotek/diodict/core/engine/CoordList;
.super Ljava/lang/Object;
.source "CoordList.java"


# instance fields
.field private mCoordList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([I)V
    .locals 6
    .param p1, "coords"    # [I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lcom/diotek/diodict/core/engine/CoordList;->mCoordList:Ljava/util/Vector;

    .line 24
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/CoordList;->mCoordList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 26
    if-eqz p1, :cond_1

    .line 27
    const/4 v3, 0x0

    .local v3, "start":I
    const/4 v1, 0x0

    .line 28
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_1

    .line 29
    rem-int/lit8 v4, v2, 0x2

    if-nez v4, :cond_0

    .line 30
    aget v3, p1, v2

    .line 28
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 32
    :cond_0
    aget v1, p1, v2

    .line 33
    new-instance v0, Landroid/util/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    .local v0, "coord":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/CoordList;->mCoordList:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 38
    .end local v0    # "coord":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v1    # "end":I
    .end local v2    # "i":I
    .end local v3    # "start":I
    :cond_1
    return-void
.end method


# virtual methods
.method public getCoordList()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/CoordList;->mCoordList:Ljava/util/Vector;

    return-object v0
.end method
