.class public Lcom/diotek/diodict/core/lang/CodeBlock;
.super Ljava/lang/Object;
.source "CodeBlock.java"


# static fields
.field public static final CB_ALPHABET:I = 0x2

.field public static final CB_ASCII_SYMBOL:I = 0x5

.field public static final CB_CHINESE:I = 0x1

.field public static final CB_HANGUL:I = 0x3

.field public static final CB_JAPANESE:I = 0x4

.field public static final CB_LATIN:I = 0x6

.field public static final CB_NONE:I = 0xff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCodeBlock(C)I
    .locals 2
    .param p0, "c"    # C

    .prologue
    const/4 v0, 0x2

    .line 51
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isAlpabetCodeBlock(C)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isLatin(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isSpecialCodeBlock(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 56
    const/4 v0, 0x5

    goto :goto_0

    .line 59
    :cond_2
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHangulCodeBlock(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 60
    const/4 v0, 0x3

    goto :goto_0

    .line 63
    :cond_3
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isChineseCodeBlock(C)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 64
    const/4 v0, 0x1

    goto :goto_0

    .line 67
    :cond_4
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isJapan(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static isAlpabetCodeBlock(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 92
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_2

    .line 93
    :cond_1
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isArabic(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 314
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 315
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    :cond_0
    const/4 v1, 0x1

    .line 320
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isChinese(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 135
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 136
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->KANGXI_RADICALS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    const/4 v1, 0x1

    .line 159
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isChineseCodeBlock(C)Z
    .locals 1
    .param p0, "a"    # C

    .prologue
    .line 80
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isChinese(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCyrillic(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 274
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 275
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CYRILLIC:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CYRILLIC_SUPPLEMENTARY:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 277
    :cond_0
    const/4 v1, 0x1

    .line 279
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isGreek(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 287
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 288
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->GREEK:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->GREEK_EXTENDED:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 290
    :cond_0
    const/4 v1, 0x1

    .line 292
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isHangulCodeBlock(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 116
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHangulSyllable(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHangulCompJamo(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHangulJamo(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHangulCompJamo(C)Z
    .locals 1
    .param p0, "a"    # C

    .prologue
    .line 170
    const/16 v0, 0x3130

    if-lt p0, v0, :cond_0

    const/16 v0, 0x318f

    if-gt p0, v0, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 173
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHangulJamo(C)Z
    .locals 1
    .param p0, "a"    # C

    .prologue
    .line 163
    const/16 v0, 0x1100

    if-lt p0, v0, :cond_0

    const/16 v0, 0x11ff

    if-ge p0, v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHangulSyllable(C)Z
    .locals 1
    .param p0, "a"    # C

    .prologue
    .line 177
    const v0, 0xac00

    if-lt p0, v0, :cond_0

    const v0, 0xd7af

    if-gt p0, v0, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 180
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHindi(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 301
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 302
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    const/4 v1, 0x1

    .line 305
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isHiragana(C)Z
    .locals 2
    .param p0, "c"    # C

    .prologue
    .line 218
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 219
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->HIRAGANA:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const/4 v1, 0x1

    .line 223
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isJapan(C)Z
    .locals 1
    .param p0, "a"    # C

    .prologue
    .line 205
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHiragana(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isKatakana(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    :cond_0
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKatakana(C)Z
    .locals 2
    .param p0, "c"    # C

    .prologue
    .line 232
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 233
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->KATAKANA:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    :cond_0
    const/4 v1, 0x1

    .line 238
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLatin(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 189
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 190
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0xc0

    if-gt v1, p0, :cond_0

    const/16 v1, 0xd6

    if-le p0, v1, :cond_3

    :cond_0
    const/16 v1, 0xd8

    if-gt v1, p0, :cond_1

    const/16 v1, 0xf6

    if-le p0, v1, :cond_3

    :cond_1
    const/16 v1, 0xf8

    if-gt v1, p0, :cond_2

    const/16 v1, 0xff

    if-le p0, v1, :cond_3

    :cond_2
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 194
    :cond_3
    const/4 v1, 0x1

    .line 196
    :goto_0
    return v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isNumber(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 248
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 251
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSpecialCodeBlock(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 104
    const/16 v0, 0x21

    if-lt p0, v0, :cond_0

    const/16 v0, 0x40

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x5b

    if-lt p0, v0, :cond_1

    const/16 v0, 0x60

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x7b

    if-lt p0, v0, :cond_2

    const/16 v0, 0x7e

    if-le p0, v0, :cond_3

    :cond_2
    const/16 v0, 0x20

    if-ne p0, v0, :cond_4

    .line 105
    :cond_3
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isThai(C)Z
    .locals 2
    .param p0, "a"    # C

    .prologue
    .line 260
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 261
    .local v0, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->THAI:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 262
    const/4 v1, 0x1

    .line 265
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
