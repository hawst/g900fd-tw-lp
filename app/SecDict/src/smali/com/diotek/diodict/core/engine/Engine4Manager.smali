.class public Lcom/diotek/diodict/core/engine/Engine4Manager;
.super Ljava/lang/Object;
.source "Engine4Manager.java"

# interfaces
.implements Lcom/diotek/diodict/core/engine/EngineManagerInterface;


# instance fields
.field private mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    return-void
.end method


# virtual methods
.method public closeDatabase()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 205
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EngineManager;->closeDatabase()V

    goto :goto_0
.end method

.method public closeSpellcheck()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 210
    const/16 v0, -0x3e8

    .line 213
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EngineManager;->closeSpellcheck()I

    move-result v0

    goto :goto_0
.end method

.method public extendSearchWord(ILjava/lang/String;IZ)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 228
    if-eqz p2, :cond_0

    if-gez p3, :cond_1

    .line 229
    :cond_0
    const/4 v0, -0x1

    .line 238
    :goto_0
    return v0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_2

    .line 233
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    const/16 v0, -0x3e8

    goto :goto_0

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->extendSearchWord(ILjava/lang/String;IZ)I

    move-result v0

    goto :goto_0
.end method

.method public getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "posp"    # I
    .param p3, "infl"    # Ljava/lang/String;
    .param p4, "ending"    # Ljava/lang/String;
    .param p5, "stem"    # Ljava/lang/String;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 305
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const-string v0, ""

    .line 309
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManager;->getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getContentsType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 338
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    const/16 v0, -0x3e8

    .line 343
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getContentsType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getContentsVendor(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 132
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/16 v0, -0x64

    .line 136
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getContentsVendor(I)I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentDbType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const/16 v0, -0x3e8

    .line 70
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getCurrentDbType()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentEngineVersion(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/Engine4Manager;->getVersion()I

    move-result v0

    return v0
.end method

.method public getDataBaseType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 348
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 349
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    const/16 v0, -0x3e8

    .line 354
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getDataBaseType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getDatabaseVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 359
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 360
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, ""

    .line 365
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getDatabaseVersion(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEngineVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 324
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 325
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    const-string v0, ""

    .line 330
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getEngineVersion(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEngineVersionByNumeric(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 370
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 371
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/16 v0, -0x3e8

    .line 376
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getEngineVersionByNumeric(I)I

    move-result v0

    goto :goto_0
.end method

.method public getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 282
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 287
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v0

    goto :goto_0
.end method

.method public getEntryLanguage(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 160
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const/16 v0, -0x64

    .line 164
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getEntryLanguage(I)I

    move-result v0

    goto :goto_0
.end method

.method public getError(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 381
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 382
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    const/16 v0, -0x3e8

    .line 387
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getError(I)I

    move-result v0

    goto :goto_0
.end method

.method public getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 392
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 393
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x0

    .line 398
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchword"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 403
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 404
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x0

    .line 409
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const-string v0, ""

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHangulroError(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 414
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 415
    const/16 v0, -0x3e8

    .line 418
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getHangulroError(I)I

    move-result v0

    goto :goto_0
.end method

.method public getHangulroList(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 423
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 424
    const/16 v0, -0x3e8

    .line 427
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getHangulroList(I)I

    move-result v0

    goto :goto_0
.end method

.method public getHeadType(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 260
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x0

    .line 265
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getHeadType(II)I

    move-result v0

    goto :goto_0
.end method

.method public getHeadwordId(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 270
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 271
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    const/16 v0, -0x3e8

    .line 276
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getHeadwordId(II)I

    move-result v0

    goto :goto_0
.end method

.method public getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 433
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    const-string v0, ""

    .line 438
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 22
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const-string v0, ""

    .line 28
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p3}, Lcom/diotek/diodict/core/engine/EngineManager;->getMeaning(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNormalizedText(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 443
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 444
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    const-string v0, ""

    .line 449
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getNormalizedText(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreview(ILjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 150
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string v0, ""

    .line 154
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p3}, Lcom/diotek/diodict/core/engine/EngineManager;->getPreview(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    const-string v0, ""

    .line 42
    :goto_0
    return-object v0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_1

    .line 38
    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    const-string v0, ""

    goto :goto_0

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getDbType()I

    move-result v1

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getEntryId()Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/diotek/diodict/core/engine/EngineManager;->getPreview(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPronounce(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "uniqueid"    # I
    .param p3, "dbType"    # I

    .prologue
    .line 476
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 477
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    const-string v0, ""

    .line 482
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;
    .locals 1
    .param p1, "searchlistType"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v0

    goto :goto_0
.end method

.method public getSpellcheckResultSize()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 186
    const/16 v0, -0x3e8

    .line 188
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getSpellcheckResultSize()I

    move-result v0

    goto :goto_0
.end method

.method public getSpellcheckWord(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 194
    const-string v0, ""

    .line 196
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getSpellcheckWord(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTranslationLanguages(I)[I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 454
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 455
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x0

    .line 460
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getTranslationLanguages(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 244
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    const/16 v0, -0x3e8

    .line 254
    :goto_0
    return v0

    .line 249
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v0

    if-gez v0, :cond_2

    .line 251
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 254
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I

    move-result v0

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 142
    const/16 v0, -0x3e8

    .line 144
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getVersion()I

    move-result v0

    goto :goto_0
.end method

.method public getWordmap(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "suid"    # I

    .prologue
    .line 314
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 315
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    const/16 v0, -0x3e8

    .line 319
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getWordmap(II)I

    move-result v0

    goto :goto_0
.end method

.method public initEngine(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 12
    const/4 v0, 0x0

    .line 14
    .local v0, "err":I
    invoke-static {}, Lcom/diotek/diodict/core/engine/EngineManager;->getInstance()Lcom/diotek/diodict/core/engine/EngineManager;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    .line 15
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    .line 17
    return v0
.end method

.method public initSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 170
    const/16 v0, -0x3e8

    .line 172
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->initSpellcheck(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public isValidInstance(I)Z
    .locals 2
    .param p1, "lastDbType"    # I

    .prologue
    .line 103
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 110
    :goto_0
    return v0

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->isValidHandler(I)Z

    move-result v0

    .line 110
    .local v0, "result":Z
    goto :goto_0
.end method

.method public searchHangulro(ILjava/lang/String;)Z
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 466
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    const/4 v0, 0x0

    .line 471
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->searchHangulro(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public searchSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 178
    const/16 v0, -0x3e8

    .line 180
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->searchSpellcheck(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public searchWord(ILjava/lang/String;IIZ)I
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    .line 49
    if-eqz p2, :cond_0

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    const/4 v0, 0x2

    if-le p4, v0, :cond_1

    .line 51
    :cond_0
    const/4 v0, -0x1

    .line 59
    :goto_0
    return v0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_2

    .line 55
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManager;->searchWord(ILjava/lang/String;IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public setCurrentDbType(I)I
    .locals 3
    .param p1, "dbType"    # I

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 77
    .local v0, "err":I
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v2, :cond_0

    .line 78
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    .line 79
    if-eqz v0, :cond_0

    move v1, v0

    .line 87
    .end local v0    # "err":I
    .local v1, "err":I
    :goto_0
    return v1

    .line 84
    .end local v1    # "err":I
    .restart local v0    # "err":I
    :cond_0
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v2}, Lcom/diotek/diodict/core/engine/EngineManager;->getCurrentDbType()I

    move-result v2

    if-eq p1, v2, :cond_1

    .line 85
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v2, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    :cond_1
    move v1, v0

    .line 87
    .end local v0    # "err":I
    .restart local v1    # "err":I
    goto :goto_0
.end method

.method public setCurrentDbType(ILjava/lang/String;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 93
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/Engine4Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/16 v0, -0x3e8

    .line 98
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Engine4Manager;->mEngine4:Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-virtual {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(ILjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public totalSearchWord(ILjava/lang/String;IIZ)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    .line 294
    if-eqz p2, :cond_0

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    const/4 v0, 0x2

    if-le p4, v0, :cond_1

    .line 296
    :cond_0
    const/4 v0, -0x1

    .line 299
    :goto_0
    return v0

    :cond_1
    invoke-virtual/range {p0 .. p5}, Lcom/diotek/diodict/core/engine/Engine4Manager;->searchWord(ILjava/lang/String;IIZ)I

    move-result v0

    goto :goto_0
.end method
