.class public Lcom/diotek/diodict/core/lang/LanguageInfo;
.super Ljava/lang/Object;
.source "LanguageInfo.java"


# static fields
.field public static final CHINESE:I = 0x3

.field public static final ENGLISH:I = 0x0

.field public static final ENGLISH_UK:I = 0x5

.field public static final JAPANESE:I = 0x2

.field public static final KOREAN:I = 0x1

.field public static final MAX:I = 0x4

.field public static final NONE:I = -0x1

.field public static final VOICE_MODE_CHN:Ljava/lang/String;

.field public static final VOICE_MODE_ENG:Ljava/lang/String;

.field public static final VOICE_MODE_FRA:Ljava/lang/String;

.field public static final VOICE_MODE_GER:Ljava/lang/String;

.field public static final VOICE_MODE_JPN:Ljava/lang/String;

.field public static final VOICE_MODE_KOR:Ljava/lang/String;

.field public static final VOICE_MODE_NONE:Ljava/lang/String; = ""

.field public static final VOICE_MODE_SPN:Ljava/lang/String; = "es"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x5f

    const/16 v1, 0x2d

    .line 39
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_ENG:Ljava/lang/String;

    .line 41
    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_KOR:Ljava/lang/String;

    .line 43
    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_JPN:Ljava/lang/String;

    .line 45
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_CHN:Ljava/lang/String;

    .line 51
    sget-object v0, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_FRA:Ljava/lang/String;

    .line 53
    sget-object v0, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/core/lang/LanguageInfo;->VOICE_MODE_GER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocaleByLanguageId(I)Ljava/util/Locale;
    .locals 1
    .param p0, "lang"    # I

    .prologue
    .line 61
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 63
    .local v0, "ret":Ljava/util/Locale;
    packed-switch p0, :pswitch_data_0

    .line 83
    :goto_0
    :pswitch_0
    return-object v0

    .line 65
    :pswitch_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 66
    goto :goto_0

    .line 68
    :pswitch_2
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    .line 69
    goto :goto_0

    .line 71
    :pswitch_3
    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    .line 72
    goto :goto_0

    .line 74
    :pswitch_4
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    .line 75
    goto :goto_0

    .line 77
    :pswitch_5
    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    .line 78
    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getLocaleStringByLanguageId(I)Ljava/lang/String;
    .locals 1
    .param p0, "lang"    # I

    .prologue
    .line 92
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/LanguageInfo;->getLocaleByLanguageId(I)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
