.class public Lcom/diotek/diodict/core/engine/ExamList;
.super Ljava/lang/Object;
.source "ExamList.java"


# instance fields
.field private mExamList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/diotek/diodict/core/engine/ExamListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    .line 19
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    .line 20
    return-void
.end method

.method private addExamtem(Lcom/diotek/diodict/core/engine/ExamListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/diotek/diodict/core/engine/ExamListItem;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method


# virtual methods
.method public addExamItem(ILjava/lang/String;)V
    .locals 1
    .param p1, "examNum"    # I
    .param p2, "exam"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v0, Lcom/diotek/diodict/core/engine/ExamListItem;

    invoke-direct {v0, p1, p2}, Lcom/diotek/diodict/core/engine/ExamListItem;-><init>(ILjava/lang/String;)V

    .line 53
    .local v0, "item":Lcom/diotek/diodict/core/engine/ExamListItem;
    invoke-direct {p0, v0}, Lcom/diotek/diodict/core/engine/ExamList;->addExamtem(Lcom/diotek/diodict/core/engine/ExamListItem;)V

    .line 54
    return-void
.end method

.method public clearList()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 73
    :cond_0
    return-void
.end method

.method public getExamItem(I)Lcom/diotek/diodict/core/engine/ExamListItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/ExamListItem;

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 29
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamList;->mExamList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0
.end method
