.class public interface abstract Lcom/diotek/diodict/core/engine/EngineManagerInterface;
.super Ljava/lang/Object;
.source "EngineManagerInterface.java"


# static fields
.field public static final DEDT_TOTAL_SEARCH:I = 0xfff0

.field public static final ENGINE_MALFUNCTION_NULL:I = -0x3e8

.field public static final ENGINE_MALFUNCTION_STR_NULL:Ljava/lang/String; = ""

.field public static final ENGINE_NONE_ACTION_CODE:I = -0x1

.field public static final ENGINE_NONE_ACTION_TEXT:Ljava/lang/String; = ""

.field public static final ENGINE_SWITCH_FAIL:I = -0x64

.field public static final ENGINE_SWITCH_FAIL_STR:Ljava/lang/String; = ""

.field public static final INVALID_PARAM:Ljava/lang/String; = ""

.field public static final INVALID_PARAM_NUM:I = -0x1

.field public static final SEARCH_LIST_EMPTY:I


# virtual methods
.method public abstract closeDatabase()V
.end method

.method public abstract closeSpellcheck()I
.end method

.method public abstract extendSearchWord(ILjava/lang/String;IZ)I
.end method

.method public abstract getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getContentsType(I)I
.end method

.method public abstract getContentsVendor(I)I
.end method

.method public abstract getCurrentDbType(I)I
.end method

.method public abstract getCurrentEngineVersion(I)I
.end method

.method public abstract getDataBaseType(I)I
.end method

.method public abstract getDatabaseVersion(I)Ljava/lang/String;
.end method

.method public abstract getEngineVersion(I)Ljava/lang/String;
.end method

.method public abstract getEngineVersionByNumeric(I)I
.end method

.method public abstract getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;
.end method

.method public abstract getEntryLanguage(I)I
.end method

.method public abstract getError(I)I
.end method

.method public abstract getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
.end method

.method public abstract getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
.end method

.method public abstract getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
.end method

.method public abstract getHangulroError(I)I
.end method

.method public abstract getHangulroList(I)I
.end method

.method public abstract getHeadType(II)I
.end method

.method public abstract getHeadwordId(II)I
.end method

.method public abstract getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
.end method

.method public abstract getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;
.end method

.method public abstract getNormalizedText(ILjava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getPreview(ILjava/lang/String;I)Ljava/lang/String;
.end method

.method public abstract getPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;
.end method

.method public abstract getPronounce(Ljava/lang/String;II)Ljava/lang/String;
.end method

.method public abstract getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;
.end method

.method public abstract getSpellcheckResultSize()I
.end method

.method public abstract getSpellcheckWord(I)Ljava/lang/String;
.end method

.method public abstract getTranslationLanguages(I)[I
.end method

.method public abstract getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I
.end method

.method public abstract getVersion()I
.end method

.method public abstract getWordmap(II)I
.end method

.method public abstract initEngine(I)I
.end method

.method public abstract initSpellcheck(Ljava/lang/String;I)I
.end method

.method public abstract isValidInstance(I)Z
.end method

.method public abstract searchHangulro(ILjava/lang/String;)Z
.end method

.method public abstract searchSpellcheck(Ljava/lang/String;I)I
.end method

.method public abstract searchWord(ILjava/lang/String;IIZ)I
.end method

.method public abstract setCurrentDbType(I)I
.end method

.method public abstract setCurrentDbType(ILjava/lang/String;)I
.end method

.method public abstract totalSearchWord(ILjava/lang/String;IIZ)I
.end method
