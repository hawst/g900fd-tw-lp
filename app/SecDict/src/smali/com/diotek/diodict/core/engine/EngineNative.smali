.class public Lcom/diotek/diodict/core/engine/EngineNative;
.super Ljava/lang/Object;
.source "EngineNative.java"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    :try_start_0
    const-string v1, "JNI"

    const-string v2, "Trying to load stlport_shared"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 15
    const-string v1, "stlport_shared"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 16
    const-string v1, "JNI"

    const-string v2, "Trying to load jma"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    const-string v1, "jma"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 18
    const-string v1, "JNI"

    const-string v2, "Trying to load nltk"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    const-string v1, "nltk"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 20
    const-string v1, "JNI"

    const-string v2, "Trying to load diolemma"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const-string v1, "diolemma"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 22
    const-string v1, "JNI"

    const-string v2, "Trying to load DioDict4EngineNativeFrame"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    const-string v1, "DioDict4EngineNativeFrame"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 24
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 25
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "JNI"

    const-string v2, "Warning: Could not load DioDict4EngineNativeFrame"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native closeDatabase(Ljava/lang/String;)Z
.end method

.method public static native compare(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native createPowerSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native getConjugation(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
.end method

.method public static native getContentsType(Ljava/lang/String;)I
.end method

.method public static native getContentsVendor(Ljava/lang/String;)I
.end method

.method public static native getCoordList(II)[I
.end method

.method public static native getDatabaseType(Ljava/lang/String;)I
.end method

.method public static native getDatabaseVersion(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native getEngineVersion(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native getEngineVersionByNumeric(Ljava/lang/String;)I
.end method

.method public static native getEntry(II)[I
.end method

.method public static native getEntryId(Ljava/lang/String;I)[B
.end method

.method public static native getEntryLanguage(Ljava/lang/String;)I
.end method

.method public static native getError(Ljava/lang/String;)I
.end method

.method public static native getExamList(Ljava/lang/String;IIIIZ)I
.end method

.method public static native getExamListbyKeyword(Ljava/lang/String;Ljava/lang/String;IZ)I
.end method

.method public static native getExamNum(I)I
.end method

.method public static native getExample(I)[B
.end method

.method public static native getExtendList(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native getGuideWord(Ljava/lang/String;IIII)[B
.end method

.method public static native getHangulroError(I)I
.end method

.method public static native getHangulroList(I)I
.end method

.method public static native getHeadNo(II)I
.end method

.method public static native getHeadwordId(Ljava/lang/String;I)I
.end method

.method public static native getMeaning(Ljava/lang/String;I)[B
.end method

.method public static native getMeaningbyEntryId(Ljava/lang/String;III)[B
.end method

.method public static native getPreview(II)[B
.end method

.method public static native getPreview(Ljava/lang/String;I)[B
.end method

.method public static native getPreviewbyEntryId(Ljava/lang/String;III)[B
.end method

.method public static native getSearchList(Ljava/lang/String;Ljava/lang/String;IIZ)I
.end method

.method public static native getSpellCheckResultNum()I
.end method

.method public static native getSpellCheckWord(I)[B
.end method

.method public static native getTranslationLanguages(Ljava/lang/String;)[I
.end method

.method public static native getType(Ljava/lang/String;I)I
.end method

.method public static native getUniqueId(Ljava/lang/String;III)I
.end method

.method public static native getWordmap(Ljava/lang/String;I)I
.end method

.method public static native normalize(Ljava/lang/String;Ljava/lang/String;)[B
.end method

.method public static native openDatabase(Ljava/lang/String;)Z
.end method

.method public static native searchHangulro(Ljava/lang/String;Ljava/lang/String;ZI)Z
.end method

.method public static native spellCheckInit(Ljava/lang/String;I)I
.end method

.method public static native spellCheckSearch(Ljava/lang/String;I)I
.end method

.method public static native spellCheckTerm()I
.end method
