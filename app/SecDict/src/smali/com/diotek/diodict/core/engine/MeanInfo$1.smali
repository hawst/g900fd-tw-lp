.class final Lcom/diotek/diodict/core/engine/MeanInfo$1;
.super Ljava/lang/Object;
.source "MeanInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/core/engine/MeanInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/diotek/diodict/core/engine/MeanInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/diotek/diodict/core/engine/MeanInfo;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 58
    new-instance v0, Lcom/diotek/diodict/core/engine/MeanInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const-class v1, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/diotek/diodict/core/engine/MeanInfo;-><init>(ILcom/diotek/diodict/core/engine/EntryId;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .local v0, "meanInfo":Lcom/diotek/diodict/core/engine/MeanInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/MeanInfo;->access$002(Lcom/diotek/diodict/core/engine/MeanInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/MeanInfo;->access$102(Lcom/diotek/diodict/core/engine/MeanInfo;I)I

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/MeanInfo;->access$202(Lcom/diotek/diodict/core/engine/MeanInfo;I)I

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/MeanInfo;->access$302(Lcom/diotek/diodict/core/engine/MeanInfo;I)I

    .line 64
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/MeanInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/diotek/diodict/core/engine/MeanInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/diotek/diodict/core/engine/MeanInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 69
    new-array v0, p1, [Lcom/diotek/diodict/core/engine/MeanInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/MeanInfo$1;->newArray(I)[Lcom/diotek/diodict/core/engine/MeanInfo;

    move-result-object v0

    return-object v0
.end method
