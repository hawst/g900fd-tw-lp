.class public Lcom/diotek/diodict/core/engine/ResultWordListItem;
.super Ljava/lang/Object;
.source "ResultWordListItem.java"


# instance fields
.field private mCoordList:Lcom/diotek/diodict/core/engine/CoordList;

.field private mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

.field private mHeadNo:I

.field private mPreview:Lcom/diotek/diodict/core/engine/Preview;


# direct methods
.method public constructor <init>(I[I[ILjava/lang/String;)V
    .locals 1
    .param p1, "headNo"    # I
    .param p2, "entryId"    # [I
    .param p3, "coordList"    # [I
    .param p4, "preview"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mHeadNo:I

    .line 39
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v0, p2}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 40
    new-instance v0, Lcom/diotek/diodict/core/engine/CoordList;

    invoke-direct {v0, p3}, Lcom/diotek/diodict/core/engine/CoordList;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mCoordList:Lcom/diotek/diodict/core/engine/CoordList;

    .line 41
    new-instance v0, Lcom/diotek/diodict/core/engine/Preview;

    invoke-direct {v0, p4}, Lcom/diotek/diodict/core/engine/Preview;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mPreview:Lcom/diotek/diodict/core/engine/Preview;

    .line 42
    return-void
.end method


# virtual methods
.method public getCoordList()Lcom/diotek/diodict/core/engine/CoordList;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mCoordList:Lcom/diotek/diodict/core/engine/CoordList;

    return-object v0
.end method

.method public getEntryId()Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public getPreview()Lcom/diotek/diodict/core/engine/Preview;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mPreview:Lcom/diotek/diodict/core/engine/Preview;

    return-object v0
.end method

.method public getUniqueId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/diotek/diodict/core/engine/ResultWordListItem;->mHeadNo:I

    return v0
.end method
