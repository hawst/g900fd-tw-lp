.class public Lcom/diotek/diodict/core/engine/DictDefines;
.super Ljava/lang/Object;
.source "DictDefines.java"


# static fields
.field public static final DBEXTENSION:Ljava/lang/String; = ".ps2"

.field public static final DBEXTENSION_OLD:Ljava/lang/String; = ".dat"

.field public static final ENCODE_UTF_8:Ljava/lang/String; = "utf-8"

.field public static final ENGINE3_TYPE:I = 0x3

.field public static final ENGINE4_TYPE:I = 0x4

.field public static final HEAD_TYPE_COMPLEX:I = 0x4

.field public static final HEAD_TYPE_DERIVIATIVES:I = 0x2

.field public static final HEAD_TYPE_EXAMPLE:I = 0x5

.field public static final HEAD_TYPE_IDIOM:I = 0x3

.field public static final HEAD_TYPE_KEYWORD:I = 0x0

.field public static final HEAD_TYPE_NOTE:I = 0xa

.field public static final HEAD_TYPE_PART:I = 0x6

.field public static final HEAD_TYPE_PROVERB:I = 0x8

.field public static final HEAD_TYPE_PV_G:I = 0x7

.field public static final HEAD_TYPE_RELW:I = 0x9

.field public static final HEAD_TYPE_SENSECAT:I = 0xb

.field public static final HEAD_TYPE_SUB_ENTRY:I = 0x1

.field public static final INDEPENDENCEDB_FALSE:I = 0x3

.field public static final INDEPENDENCEDB_MAIN_COUPLE:I = 0x1

.field public static final INDEPENDENCEDB_MAIN_ONLY:I = 0x0

.field public static final INDEPENDENCEDB_SUB:I = 0x2

.field public static final INIT_ENGINE_ERROR_DBMANAGER:I = 0x1

.field public static final INIT_ENGINE_ERROR_INVALID_PARAM:I = 0x3

.field public static final INIT_ENGINE_ERROR_NATIVE:I = 0x2

.field public static final INIT_ENGINE_ERROR_NONE:I = 0x0

.field public static final SEARCHLIST_TYPE_HANGULRO:I = 0x2

.field public static final SEARCHLIST_TYPE_HYPER:I = 0x1

.field public static final SEARCHLIST_TYPE_NORMAL:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
