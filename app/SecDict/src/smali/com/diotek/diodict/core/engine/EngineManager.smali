.class public Lcom/diotek/diodict/core/engine/EngineManager;
.super Ljava/lang/Object;
.source "EngineManager.java"


# static fields
.field private static final TOTAL_SEARCH_DBTYPE:I = 0xfff0

.field private static volatile mInstance:Lcom/diotek/diodict/core/engine/EngineManager;


# instance fields
.field private mDBPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDbType:I

.field private mEnginEntries:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mEngineEntry:Ljava/lang/String;

.field private mExamList:Lcom/diotek/diodict/core/engine/ExamList;

.field private mHangulroWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

.field private mHyperSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

.field private mLastDBPath:Ljava/lang/String;

.field private mSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/diotek/diodict/core/engine/EngineManager;->mInstance:Lcom/diotek/diodict/core/engine/EngineManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    .line 38
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    .line 43
    iput-object v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mLastDBPath:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDBPathList:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 58
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mHyperSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 63
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mHangulroWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 68
    new-instance v0, Lcom/diotek/diodict/core/engine/ExamList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ExamList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    .line 73
    return-void
.end method

.method private clearSearchlList(I)V
    .locals 1
    .param p1, "searchlistType"    # I

    .prologue
    .line 857
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;->clearWordList()V

    .line 858
    return-void
.end method

.method private closeDatabase(Ljava/lang/String;)Z
    .locals 3
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 508
    const/4 v1, 0x0

    .line 509
    .local v1, "result":Z
    invoke-direct {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getEngineEntryPair(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 510
    .local v0, "item":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 511
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->closeDatabase(Ljava/lang/String;)Z

    move-result v1

    .line 512
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 515
    :cond_0
    return v1
.end method

.method private createSearchEngine(ILjava/lang/String;)Z
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDictSqlFileName(I)Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "engineEntry":Ljava/lang/String;
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDictFilename(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, "pwDBName":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 229
    :cond_0
    const/4 v2, 0x0

    .line 232
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->createSearchEngine(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method

.method private createSearchEngine(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;
    .param p3, "pwDBName"    # Ljava/lang/String;
    .param p4, "sqliteDBName"    # Ljava/lang/String;

    .prologue
    .line 248
    :try_start_0
    new-instance v6, Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const-string v10, "utf-8"

    invoke-direct {v6, v9, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    .local v6, "pwName":Ljava/lang/String;
    :try_start_1
    new-instance v8, Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const-string v10, "utf-8"

    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 260
    .local v8, "sqlName":Ljava/lang/String;
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getLemmaPath(I)Ljava/lang/String;

    move-result-object v5

    .line 261
    .local v5, "lemmaPath":Ljava/lang/String;
    const-string v4, ""

    .line 266
    .local v4, "lemmaFullPath":Ljava/lang/String;
    invoke-static {p2, v6, v8, v4}, Lcom/diotek/diodict/core/engine/EngineNative;->createPowerSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 268
    .local v3, "error":I
    if-eqz v3, :cond_0

    .line 269
    const/4 v9, 0x2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createPowerSearch error : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/diotek/diodict/core/util/MSG;->l(ILjava/lang/String;)V

    .line 270
    const/4 v9, 0x0

    .line 289
    .end local v3    # "error":I
    .end local v4    # "lemmaFullPath":Ljava/lang/String;
    .end local v5    # "lemmaPath":Ljava/lang/String;
    .end local v6    # "pwName":Ljava/lang/String;
    .end local v8    # "sqlName":Ljava/lang/String;
    :goto_0
    return v9

    .line 249
    :catch_0
    move-exception v2

    .line 250
    .local v2, "e1":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    goto :goto_0

    .line 256
    .end local v2    # "e1":Ljava/io/UnsupportedEncodingException;
    .restart local v6    # "pwName":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 257
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    goto :goto_0

    .line 273
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v3    # "error":I
    .restart local v4    # "lemmaFullPath":Ljava/lang/String;
    .restart local v5    # "lemmaPath":Ljava/lang/String;
    .restart local v8    # "sqlName":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    .line 275
    if-nez v3, :cond_2

    iget-object v9, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    new-instance v10, Landroid/util/Pair;

    iget-object v11, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v9, v10}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 279
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->openDatabase(Ljava/lang/String;)Z

    move-result v7

    .line 280
    .local v7, "result":Z
    invoke-static/range {p4 .. p4}, Lcom/diotek/diodict/core/engine/EngineNative;->getError(Ljava/lang/String;)I

    move-result v3

    .line 282
    if-nez v7, :cond_1

    if-eqz v3, :cond_1

    .line 283
    const/4 v9, 0x2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "openDatabase error : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/diotek/diodict/core/util/MSG;->l(ILjava/lang/String;)V

    .line 284
    const/4 v9, 0x0

    goto :goto_0

    .line 286
    :cond_1
    iget-object v9, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    new-instance v10, Landroid/util/Pair;

    iget-object v11, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v9, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 289
    .end local v7    # "result":Z
    :cond_2
    if-nez v3, :cond_3

    const/4 v9, 0x1

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    goto :goto_0
.end method

.method private extendSearchWord(Ljava/lang/String;Ljava/lang/String;IZ)I
    .locals 4
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    const/4 v3, 0x1

    .line 806
    invoke-virtual {p0, v3}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v1

    iget v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    invoke-virtual {v1, v2}, Lcom/diotek/diodict/core/engine/ResultWordList;->setDbType(I)V

    .line 807
    invoke-static {p1, p2, p3}, Lcom/diotek/diodict/core/engine/EngineNative;->getExtendList(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 809
    .local v0, "resultSize":I
    if-lez v0, :cond_0

    .line 810
    const/4 v1, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/diotek/diodict/core/engine/EngineManager;->updateList(IIZ)V

    .line 813
    :cond_0
    return v0
.end method

.method private getDBPathList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDBPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getDatabaseType(Ljava/lang/String;)I
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 376
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getDatabaseType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private getEngineEntryPair(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .param p1, "engineEntry"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 519
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v1, v3

    .line 529
    :goto_0
    return-object v1

    .line 523
    :cond_1
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 524
    .local v1, "item":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .end local v1    # "item":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    move-object v1, v3

    .line 529
    goto :goto_0
.end method

.method public static getInstance()Lcom/diotek/diodict/core/engine/EngineManager;
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/diotek/diodict/core/engine/EngineManager;->mInstance:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v0, :cond_0

    .line 82
    const-class v1, Lcom/diotek/diodict/core/engine/EngineManager;

    monitor-enter v1

    .line 83
    :try_start_0
    new-instance v0, Lcom/diotek/diodict/core/engine/EngineManager;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/EngineManager;-><init>()V

    sput-object v0, Lcom/diotek/diodict/core/engine/EngineManager;->mInstance:Lcom/diotek/diodict/core/engine/EngineManager;

    .line 91
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    sget-object v0, Lcom/diotek/diodict/core/engine/EngineManager;->mInstance:Lcom/diotek/diodict/core/engine/EngineManager;

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 7
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 586
    if-nez p2, :cond_1

    .line 587
    const-string v2, ""

    .line 604
    :cond_0
    :goto_0
    return-object v2

    .line 590
    :cond_1
    const/4 v0, 0x0

    .line 592
    .local v0, "array":[B
    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v4

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v5

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v6

    invoke-static {p1, v4, v5, v6}, Lcom/diotek/diodict/core/engine/EngineNative;->getMeaningbyEntryId(Ljava/lang/String;III)[B

    move-result-object v0

    .line 595
    const-string v2, ""

    .line 596
    .local v2, "meaning":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 598
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v4, "utf-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "meaning":Ljava/lang/String;
    .local v3, "meaning":Ljava/lang/String;
    move-object v2, v3

    .line 601
    .end local v3    # "meaning":Ljava/lang/String;
    .restart local v2    # "meaning":Ljava/lang/String;
    goto :goto_0

    .line 599
    :catch_0
    move-exception v1

    .line 600
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPreview(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 6
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 692
    if-nez p2, :cond_0

    .line 693
    const-string v1, ""

    .line 704
    :goto_0
    return-object v1

    .line 696
    :cond_0
    const-string v1, ""

    .line 698
    .local v1, "preview":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v3

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v4

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v5

    invoke-static {p1, v3, v4, v5}, Lcom/diotek/diodict/core/engine/EngineNative;->getPreviewbyEntryId(Ljava/lang/String;III)[B

    move-result-object v3

    const-string v4, "utf-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "preview":Ljava/lang/String;
    .local v2, "preview":Ljava/lang/String;
    move-object v1, v2

    .line 702
    .end local v2    # "preview":Ljava/lang/String;
    .restart local v1    # "preview":Ljava/lang/String;
    goto :goto_0

    .line 700
    :catch_0
    move-exception v0

    .line 701
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private openDatabase(Ljava/lang/String;)Z
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 497
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->openDatabase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private searchWord(Ljava/lang/String;Ljava/lang/String;IIZ)I
    .locals 8
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    const/4 v3, 0x0

    .line 761
    invoke-virtual {p0, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v4

    iget v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    invoke-virtual {v4, v5}, Lcom/diotek/diodict/core/engine/ResultWordList;->setDbType(I)V

    .line 763
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    new-instance v5, Landroid/util/Pair;

    iget-object v6, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 765
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/diotek/diodict/core/engine/EngineManager;->openDatabase(Ljava/lang/String;)Z

    move-result v1

    .line 766
    .local v1, "result":Z
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v4}, Lcom/diotek/diodict/core/engine/EngineNative;->getError(Ljava/lang/String;)I

    move-result v0

    .line 768
    .local v0, "error":I
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 769
    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "openDatabase error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/diotek/diodict/core/util/MSG;->l(ILjava/lang/String;)V

    move v2, v3

    .line 781
    .end local v0    # "error":I
    .end local v1    # "result":Z
    :cond_0
    :goto_0
    return v2

    .line 772
    .restart local v0    # "error":I
    .restart local v1    # "result":Z
    :cond_1
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    new-instance v5, Landroid/util/Pair;

    iget-object v6, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 775
    .end local v0    # "error":I
    .end local v1    # "result":Z
    :cond_2
    invoke-static {p1, p2, p3, p4, p5}, Lcom/diotek/diodict/core/engine/EngineNative;->getSearchList(Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v2

    .line 777
    .local v2, "resultSize":I
    if-lez v2, :cond_0

    .line 778
    invoke-direct {p0, v2, p4, v3}, Lcom/diotek/diodict/core/engine/EngineManager;->updateList(IIZ)V

    goto :goto_0
.end method

.method private setDBPathList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1297
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDBPathList:Ljava/util/ArrayList;

    .line 1298
    return-void
.end method

.method private updateList(IIZ)V
    .locals 8
    .param p1, "listSize"    # I
    .param p2, "searchlistType"    # I
    .param p3, "isAppend"    # Z

    .prologue
    .line 817
    if-nez p3, :cond_0

    .line 818
    invoke-direct {p0, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->clearSearchlList(I)V

    .line 820
    :cond_0
    if-lez p1, :cond_1

    .line 821
    const-string v2, ""

    .line 822
    .local v2, "preview":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    move-object v3, v2

    .end local v2    # "preview":Ljava/lang/String;
    .local v3, "preview":Ljava/lang/String;
    :goto_0
    if-ge v1, p1, :cond_1

    .line 824
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-static {p2, v1}, Lcom/diotek/diodict/core/engine/EngineNative;->getPreview(II)[B

    move-result-object v4

    const-string v5, "utf-8"

    invoke-direct {v2, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    .end local v3    # "preview":Ljava/lang/String;
    .restart local v2    # "preview":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v4

    invoke-static {p2, v1}, Lcom/diotek/diodict/core/engine/EngineNative;->getHeadNo(II)I

    move-result v5

    invoke-static {p2, v1}, Lcom/diotek/diodict/core/engine/EngineNative;->getEntry(II)[I

    move-result-object v6

    invoke-static {p2, v1}, Lcom/diotek/diodict/core/engine/EngineNative;->getCoordList(II)[I

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7, v2}, Lcom/diotek/diodict/core/engine/ResultWordList;->addWordItem(I[I[ILjava/lang/String;)V

    .line 822
    add-int/lit8 v1, v1, 0x1

    move-object v3, v2

    .end local v2    # "preview":Ljava/lang/String;
    .restart local v3    # "preview":Ljava/lang/String;
    goto :goto_0

    .line 825
    :catch_0
    move-exception v0

    .line 826
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v2, v3

    .end local v3    # "preview":Ljava/lang/String;
    .restart local v2    # "preview":Ljava/lang/String;
    goto :goto_1

    .line 832
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "i":I
    .end local v2    # "preview":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public closeDatabase()V
    .locals 3

    .prologue
    .line 536
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    .line 538
    .local v0, "entriesSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 539
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/diotek/diodict/core/engine/EngineNative;->closeDatabase(Ljava/lang/String;)Z

    .line 538
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 542
    :cond_0
    iget-object v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEnginEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 543
    return-void
.end method

.method public closeSpellcheck()I
    .locals 1

    .prologue
    .line 1199
    invoke-static {}, Lcom/diotek/diodict/core/engine/EngineNative;->spellCheckTerm()I

    move-result v0

    return v0
.end method

.method public extendSearchWord(ILjava/lang/String;IZ)I
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    const/4 v0, 0x0

    .line 793
    if-gez p1, :cond_1

    .line 801
    :cond_0
    :goto_0
    return v0

    .line 796
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 797
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 801
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->extendSearchWord(Ljava/lang/String;Ljava/lang/String;IZ)I

    move-result v0

    goto :goto_0
.end method

.method public getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I
    .param p2, "posp"    # I
    .param p3, "infl"    # Ljava/lang/String;
    .param p4, "ending"    # Ljava/lang/String;
    .param p5, "stem"    # Ljava/lang/String;

    .prologue
    .line 638
    if-gez p1, :cond_0

    .line 639
    const-string v0, ""

    .line 654
    :goto_0
    return-object v0

    .line 642
    :cond_0
    iget v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v3, p1, :cond_1

    .line 643
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v3

    if-eqz v3, :cond_1

    .line 644
    const-string v0, ""

    goto :goto_0

    .line 648
    :cond_1
    const-string v0, ""

    .line 650
    .local v0, "conjugation":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v3, p2, p3, p4, p5}, Lcom/diotek/diodict/core/engine/EngineNative;->getConjugation(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    const-string v4, "utf-8"

    invoke-direct {v1, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "conjugation":Ljava/lang/String;
    .local v1, "conjugation":Ljava/lang/String;
    move-object v0, v1

    .line 653
    .end local v1    # "conjugation":Ljava/lang/String;
    .restart local v0    # "conjugation":Ljava/lang/String;
    goto :goto_0

    .line 651
    :catch_0
    move-exception v2

    .line 652
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getContentsType(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 423
    if-gez p1, :cond_1

    .line 431
    :cond_0
    :goto_0
    return v0

    .line 426
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 427
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 431
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getContentsType(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getContentsType(Ljava/lang/String;)I
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 413
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getContentsType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getContentsVendor(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 395
    if-gez p1, :cond_1

    .line 403
    :cond_0
    :goto_0
    return v0

    .line 398
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 399
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 403
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getContentsVendor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getContentsVendor(Ljava/lang/String;)I
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 385
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getContentsVendor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCurrentDbType()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    return v0
.end method

.method public getDataBaseType(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 359
    if-gez p1, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 363
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getDatabaseType(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getDatabaseVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 332
    if-gez p1, :cond_0

    .line 333
    const-string v0, ""

    .line 340
    :goto_0
    return-object v0

    .line 335
    :cond_0
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_1

    .line 336
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    const-string v0, ""

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getDatabaseVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDatabaseVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getDatabaseVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEngineVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 298
    if-gez p1, :cond_0

    .line 299
    const-string v0, ""

    .line 306
    :goto_0
    return-object v0

    .line 301
    :cond_0
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_1

    .line 302
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, ""

    goto :goto_0

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0}, Lcom/diotek/diodict/core/engine/EngineNative;->getEngineVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEngineVersionByNumeric(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 315
    if-gez p1, :cond_1

    .line 323
    :cond_0
    :goto_0
    return v0

    .line 318
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 319
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0}, Lcom/diotek/diodict/core/engine/EngineNative;->getEngineVersionByNumeric(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 10
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x0

    .line 1064
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move-object v2, v6

    .line 1091
    :goto_0
    return-object v2

    .line 1069
    :cond_1
    iget v7, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v7, p1, :cond_2

    .line 1070
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v7

    if-eqz v7, :cond_2

    move-object v2, v6

    .line 1071
    goto :goto_0

    .line 1075
    :cond_2
    const-string v4, ""

    .line 1077
    .local v4, "strEntryId":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/lang/String;

    iget-object v7, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v7, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getEntryId(Ljava/lang/String;I)[B

    move-result-object v7

    const-string v8, "utf-8"

    invoke-direct {v5, v7, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1079
    .end local v4    # "strEntryId":Ljava/lang/String;
    .local v5, "strEntryId":Ljava/lang/String;
    :try_start_1
    const-string v7, "\\|"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1080
    .local v0, "arrayEntryId":[Ljava/lang/String;
    if-eqz v0, :cond_3

    array-length v7, v0

    if-ne v7, v9, :cond_3

    .line 1081
    const/4 v7, 0x3

    new-array v3, v7, [I

    .line 1082
    .local v3, "entryids":[I
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v3, v7

    .line 1083
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v3, v7

    .line 1084
    const/4 v7, 0x2

    const/4 v8, 0x2

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v3, v7

    .line 1085
    new-instance v2, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v2, v3}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1086
    .local v2, "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    goto :goto_0

    .end local v2    # "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    .end local v3    # "entryids":[I
    :cond_3
    move-object v4, v5

    .end local v0    # "arrayEntryId":[Ljava/lang/String;
    .end local v5    # "strEntryId":Ljava/lang/String;
    .restart local v4    # "strEntryId":Ljava/lang/String;
    :goto_1
    move-object v2, v6

    .line 1091
    goto :goto_0

    .line 1088
    :catch_0
    move-exception v1

    .line 1089
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    :goto_2
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 1088
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v4    # "strEntryId":Ljava/lang/String;
    .restart local v5    # "strEntryId":Ljava/lang/String;
    :catch_1
    move-exception v1

    move-object v4, v5

    .end local v5    # "strEntryId":Ljava/lang/String;
    .restart local v4    # "strEntryId":Ljava/lang/String;
    goto :goto_2
.end method

.method public getEntryLanguage(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 440
    if-gez p1, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v0

    .line 444
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 445
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 449
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getEntryLanguage(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getEntryLanguage(Ljava/lang/String;)I
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 459
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getEntryLanguage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getError(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, -0x1

    .line 1163
    if-gez p1, :cond_1

    .line 1172
    :cond_0
    :goto_0
    return v0

    .line 1167
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 1168
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1172
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0}, Lcom/diotek/diodict/core/engine/EngineNative;->getError(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 870
    if-eqz p2, :cond_0

    if-gez p1, :cond_1

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 872
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    .line 882
    :goto_0
    return-object v0

    .line 875
    :cond_1
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_2

    .line 876
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 877
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 878
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    goto :goto_0

    .line 882
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->getExamList(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getExamList(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 11
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 918
    const-class v1, Lcom/diotek/diodict/core/engine/EngineManager;

    monitor-enter v1

    .line 919
    :try_start_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    if-nez v0, :cond_0

    .line 920
    new-instance v0, Lcom/diotek/diodict/core/engine/ExamList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ExamList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    .line 922
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 924
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 926
    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v1

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v2

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v3

    move-object v0, p1

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineNative;->getExamList(Ljava/lang/String;IIIIZ)I

    move-result v10

    .line 927
    .local v10, "resultSize":I
    if-lez v10, :cond_1

    .line 928
    const-string v7, ""

    .line 929
    .local v7, "example":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "i":I
    move-object v8, v7

    .end local v7    # "example":Ljava/lang/String;
    .local v8, "example":Ljava/lang/String;
    :goto_0
    if-ge v9, v10, :cond_1

    .line 931
    :try_start_1
    new-instance v7, Ljava/lang/String;

    invoke-static {v9}, Lcom/diotek/diodict/core/engine/EngineNative;->getExample(I)[B

    move-result-object v0

    const-string v1, "utf-8"

    invoke-direct {v7, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .line 936
    .end local v8    # "example":Ljava/lang/String;
    .restart local v7    # "example":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-static {v9}, Lcom/diotek/diodict/core/engine/EngineNative;->getExamNum(I)I

    move-result v1

    invoke-virtual {v0, v1, v7}, Lcom/diotek/diodict/core/engine/ExamList;->addExamItem(ILjava/lang/String;)V

    .line 929
    add-int/lit8 v9, v9, 0x1

    move-object v8, v7

    .end local v7    # "example":Ljava/lang/String;
    .restart local v8    # "example":Ljava/lang/String;
    goto :goto_0

    .line 922
    .end local v8    # "example":Ljava/lang/String;
    .end local v9    # "i":I
    .end local v10    # "resultSize":I
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 932
    .restart local v8    # "example":Ljava/lang/String;
    .restart local v9    # "i":I
    .restart local v10    # "resultSize":I
    :catch_0
    move-exception v6

    .line 933
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v7, v8

    .end local v8    # "example":Ljava/lang/String;
    .restart local v7    # "example":Ljava/lang/String;
    goto :goto_1

    .line 939
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v7    # "example":Ljava/lang/String;
    .end local v9    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    return-object v0
.end method

.method public getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchword"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 894
    if-eqz p2, :cond_0

    if-gez p1, :cond_1

    .line 895
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 896
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    .line 906
    :goto_0
    return-object v0

    .line 899
    :cond_1
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_2

    .line 900
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 901
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 902
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    goto :goto_0

    .line 906
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManager;->getExamListbyKeyword(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getExamListbyKeyword(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 7
    .param p1, "engineEntry"    # Ljava/lang/String;
    .param p2, "searchword"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 951
    const-class v6, Lcom/diotek/diodict/core/engine/EngineManager;

    monitor-enter v6

    .line 952
    :try_start_0
    iget-object v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    if-nez v5, :cond_0

    .line 953
    new-instance v5, Lcom/diotek/diodict/core/engine/ExamList;

    invoke-direct {v5}, Lcom/diotek/diodict/core/engine/ExamList;-><init>()V

    iput-object v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    .line 955
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 957
    iget-object v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-virtual {v5}, Lcom/diotek/diodict/core/engine/ExamList;->clearList()V

    .line 959
    invoke-static {p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineNative;->getExamListbyKeyword(Ljava/lang/String;Ljava/lang/String;IZ)I

    move-result v4

    .line 960
    .local v4, "resultSize":I
    if-lez v4, :cond_1

    .line 961
    const-string v1, ""

    .line 962
    .local v1, "example":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    move-object v2, v1

    .end local v1    # "example":Ljava/lang/String;
    .local v2, "example":Ljava/lang/String;
    :goto_0
    if-ge v3, v4, :cond_1

    .line 964
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-static {v3}, Lcom/diotek/diodict/core/engine/EngineNative;->getExample(I)[B

    move-result-object v5

    const-string v6, "utf-8"

    invoke-direct {v1, v5, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .line 969
    .end local v2    # "example":Ljava/lang/String;
    .restart local v1    # "example":Ljava/lang/String;
    :goto_1
    iget-object v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    invoke-static {v3}, Lcom/diotek/diodict/core/engine/EngineNative;->getExamNum(I)I

    move-result v6

    invoke-virtual {v5, v6, v1}, Lcom/diotek/diodict/core/engine/ExamList;->addExamItem(ILjava/lang/String;)V

    .line 962
    add-int/lit8 v3, v3, 0x1

    move-object v2, v1

    .end local v1    # "example":Ljava/lang/String;
    .restart local v2    # "example":Ljava/lang/String;
    goto :goto_0

    .line 955
    .end local v2    # "example":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "resultSize":I
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 965
    .restart local v2    # "example":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "resultSize":I
    :catch_0
    move-exception v0

    .line 966
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v1, v2

    .end local v2    # "example":Ljava/lang/String;
    .restart local v1    # "example":Ljava/lang/String;
    goto :goto_1

    .line 972
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "example":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mExamList:Lcom/diotek/diodict/core/engine/ExamList;

    return-object v5
.end method

.method public getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 9
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 1101
    if-ltz p1, :cond_0

    if-nez p2, :cond_2

    .line 1102
    :cond_0
    const-string v2, ""

    .line 1122
    :cond_1
    :goto_0
    return-object v2

    .line 1106
    :cond_2
    iget v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v4, p1, :cond_3

    .line 1107
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 1108
    const-string v2, ""

    goto :goto_0

    .line 1112
    :cond_3
    const/4 v0, 0x0

    .line 1113
    .local v0, "array":[B
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v5

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v6

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v4, v5, v6, v7, v8}, Lcom/diotek/diodict/core/engine/EngineNative;->getGuideWord(Ljava/lang/String;IIII)[B

    move-result-object v0

    .line 1114
    const-string v2, ""

    .line 1115
    .local v2, "result":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1117
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v4, "utf-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "result":Ljava/lang/String;
    .local v3, "result":Ljava/lang/String;
    move-object v2, v3

    .line 1120
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    goto :goto_0

    .line 1118
    :catch_0
    move-exception v1

    .line 1119
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getHangulroError(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 1289
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getHangulroError(I)I

    move-result v0

    return v0
.end method

.method public getHangulroList(I)I
    .locals 4
    .param p1, "langType"    # I

    .prologue
    const/4 v3, 0x2

    .line 1273
    invoke-virtual {p0, v3}, Lcom/diotek/diodict/core/engine/EngineManager;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v1

    iget v2, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    invoke-virtual {v1, v2}, Lcom/diotek/diodict/core/engine/ResultWordList;->setDbType(I)V

    .line 1274
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getHangulroList(I)I

    move-result v0

    .line 1276
    .local v0, "listSize":I
    if-lez v0, :cond_0

    .line 1277
    const/4 v1, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/diotek/diodict/core/engine/EngineManager;->updateList(IIZ)V

    .line 1280
    :cond_0
    return v0
.end method

.method public getHeadType(II)I
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    const/4 v0, -0x1

    .line 1001
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 1012
    :cond_0
    :goto_0
    return v0

    .line 1006
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 1007
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1012
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getType(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public getHeadwordId(II)I
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    const/4 v0, -0x1

    .line 1022
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 1033
    :cond_0
    :goto_0
    return v0

    .line 1027
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 1028
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1033
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getHeadwordId(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public getMeaning(II)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 552
    if-ltz p1, :cond_0

    if-gez p2, :cond_2

    .line 553
    :cond_0
    const-string v2, ""

    .line 575
    :cond_1
    :goto_0
    return-object v2

    .line 556
    :cond_2
    iget v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v4, p1, :cond_3

    .line 557
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 558
    const-string v2, ""

    goto :goto_0

    .line 562
    :cond_3
    const/4 v0, 0x0

    .line 564
    .local v0, "array":[B
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v4, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getMeaning(Ljava/lang/String;I)[B

    move-result-object v0

    .line 566
    const-string v2, ""

    .line 567
    .local v2, "meaning":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 569
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v4, "utf-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "meaning":Ljava/lang/String;
    .local v3, "meaning":Ljava/lang/String;
    move-object v2, v3

    .line 572
    .end local v3    # "meaning":Ljava/lang/String;
    .restart local v2    # "meaning":Ljava/lang/String;
    goto :goto_0

    .line 570
    :catch_0
    move-exception v1

    .line 571
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 615
    if-eqz p2, :cond_0

    if-gez p1, :cond_1

    .line 616
    :cond_0
    const-string v0, ""

    .line 625
    :goto_0
    return-object v0

    .line 619
    :cond_1
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_2

    .line 620
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 621
    const-string v0, ""

    goto :goto_0

    .line 625
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNormalizedText(ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 1132
    if-ltz p1, :cond_0

    if-nez p2, :cond_2

    .line 1133
    :cond_0
    const-string v2, ""

    .line 1154
    :cond_1
    :goto_0
    return-object v2

    .line 1136
    :cond_2
    iget v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v4, p1, :cond_3

    .line 1137
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 1138
    const-string v2, ""

    goto :goto_0

    .line 1142
    :cond_3
    const/4 v0, 0x0

    .line 1144
    .local v0, "array":[B
    iget-object v4, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v4, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->normalize(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1146
    const-string v2, ""

    .line 1147
    .local v2, "result":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1149
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v4, "utf-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "result":Ljava/lang/String;
    .local v3, "result":Ljava/lang/String;
    move-object v2, v3

    .line 1152
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    goto :goto_0

    .line 1150
    :catch_0
    move-exception v1

    .line 1151
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPreview(II)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 664
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 665
    :cond_0
    const-string v1, ""

    .line 681
    :goto_0
    return-object v1

    .line 669
    :cond_1
    iget v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v3, p1, :cond_2

    .line 670
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 671
    const-string v1, ""

    goto :goto_0

    .line 675
    :cond_2
    const-string v1, ""

    .line 677
    .local v1, "preview":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v3, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getPreview(Ljava/lang/String;I)[B

    move-result-object v3

    const-string v4, "utf-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "preview":Ljava/lang/String;
    .local v2, "preview":Ljava/lang/String;
    move-object v1, v2

    .line 680
    .end local v2    # "preview":Ljava/lang/String;
    .restart local v1    # "preview":Ljava/lang/String;
    goto :goto_0

    .line 678
    :catch_0
    move-exception v0

    .line 679
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPreview(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 714
    if-nez p2, :cond_0

    .line 715
    const-string v0, ""

    .line 725
    :goto_0
    return-object v0

    .line 719
    :cond_0
    iget v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v0, p1, :cond_1

    .line 720
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    const-string v0, ""

    goto :goto_0

    .line 725
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->getPreview(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;
    .locals 1
    .param p1, "searchlistType"    # I

    .prologue
    .line 841
    packed-switch p1, :pswitch_data_0

    .line 847
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    :goto_0
    return-object v0

    .line 843
    :pswitch_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mHyperSearchWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    goto :goto_0

    .line 845
    :pswitch_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mHangulroWordList:Lcom/diotek/diodict/core/engine/ResultWordList;

    goto :goto_0

    .line 841
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSpellcheckResultSize()I
    .locals 1

    .prologue
    .line 1221
    invoke-static {}, Lcom/diotek/diodict/core/engine/EngineNative;->getSpellCheckResultNum()I

    move-result v0

    return v0
.end method

.method public getSpellcheckWord(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1230
    if-gez p1, :cond_1

    .line 1231
    const-string v0, ""

    .line 1240
    :cond_0
    :goto_0
    return-object v0

    .line 1234
    :cond_1
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getSpellCheckWord(I)[B

    move-result-object v1

    .line 1235
    .local v1, "word":[B
    const-string v0, ""

    .line 1237
    .local v0, "result":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1238
    new-instance v0, Ljava/lang/String;

    .end local v0    # "result":Ljava/lang/String;
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0
.end method

.method public getTranslationLanguages(I)[I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    const/4 v0, 0x0

    .line 468
    if-gez p1, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-object v0

    .line 472
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 473
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 477
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/core/engine/EngineManager;->getTranslationLanguages(Ljava/lang/String;)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getTranslationLanguages(Ljava/lang/String;)[I
    .locals 1
    .param p1, "engineEntry"    # Ljava/lang/String;

    .prologue
    .line 487
    invoke-static {p1}, Lcom/diotek/diodict/core/engine/EngineNative;->getTranslationLanguages(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I
    .locals 4
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    const/4 v0, -0x1

    .line 1043
    if-ltz p1, :cond_0

    if-nez p2, :cond_1

    .line 1054
    :cond_0
    :goto_0
    return v0

    .line 1048
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 1049
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v1

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v2

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/diotek/diodict/core/engine/EngineNative;->getUniqueId(Ljava/lang/String;III)I

    move-result v0

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x4

    return v0
.end method

.method public getWordmap(II)I
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "suid"    # I

    .prologue
    const/4 v0, -0x1

    .line 982
    if-gez p1, :cond_1

    .line 991
    :cond_0
    :goto_0
    return v0

    .line 986
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 987
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 991
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->getWordmap(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public initSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 1187
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-gez p2, :cond_1

    .line 1188
    :cond_0
    const/4 v0, 0x3

    .line 1191
    :goto_0
    return v0

    :cond_1
    invoke-static {p1, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->spellCheckInit(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public isValidHandler(I)Z
    .locals 4
    .param p1, "lastDbType"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 102
    sget-object v3, Lcom/diotek/diodict/core/engine/EngineManager;->mInstance:Lcom/diotek/diodict/core/engine/EngineManager;

    if-nez v3, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    iget v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-ltz v3, :cond_0

    .line 110
    iget-object v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    iget-object v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-lez v3, :cond_0

    .line 116
    const v3, 0xfff0

    if-ne p1, v3, :cond_2

    move v1, v2

    .line 117
    goto :goto_0

    .line 120
    :cond_2
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v0

    .line 121
    .local v0, "result":I
    if-nez v0, :cond_0

    move v1, v2

    .line 122
    goto :goto_0
.end method

.method public searchHangulro(ILjava/lang/String;)Z
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1255
    if-gez p1, :cond_1

    .line 1263
    :cond_0
    :goto_0
    return v0

    .line 1258
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 1259
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1263
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->getEntryLanguage(I)I

    move-result v2

    invoke-static {v0, p2, v1, v2}, Lcom/diotek/diodict/core/engine/EngineNative;->searchHangulro(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v0

    goto :goto_0
.end method

.method public searchSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 1209
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-gez p2, :cond_1

    .line 1210
    :cond_0
    const/4 v0, 0x3

    .line 1213
    :goto_0
    return v0

    :cond_1
    invoke-static {p1, p2}, Lcom/diotek/diodict/core/engine/EngineNative;->spellCheckSearch(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public searchWord(ILjava/lang/String;IIZ)I
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    const/4 v0, 0x0

    .line 738
    if-gez p1, :cond_1

    .line 746
    :cond_0
    :goto_0
    return v0

    .line 741
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    if-eq v1, p1, :cond_2

    .line 742
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 746
    :cond_2
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mEngineEntry:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManager;->searchWord(Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public setCurrentDict(I)I
    .locals 8
    .param p1, "dbType"    # I

    .prologue
    const/4 v5, 0x3

    .line 152
    const/4 v4, 0x0

    .line 154
    .local v4, "result":I
    const-string v0, ""

    .line 159
    .local v0, "dbPath":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDBPath(I)Ljava/lang/String;

    move-result-object v2

    .line 160
    .local v2, "dbTypePath":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 187
    .end local v2    # "dbTypePath":Ljava/lang/String;
    :goto_0
    return v5

    .line 163
    .restart local v2    # "dbTypePath":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const-string v7, "utf-8"

    invoke-direct {v1, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    .end local v0    # "dbPath":Ljava/lang/String;
    .local v1, "dbPath":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, p1, v1}, Lcom/diotek/diodict/core/engine/EngineManager;->setCurrentDict(ILjava/lang/String;)I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    move-object v0, v1

    .end local v1    # "dbPath":Ljava/lang/String;
    .restart local v0    # "dbPath":Ljava/lang/String;
    move v5, v4

    .line 187
    goto :goto_0

    .line 165
    .end local v2    # "dbTypePath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 166
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    goto :goto_0

    .line 165
    .end local v0    # "dbPath":Ljava/lang/String;
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v1    # "dbPath":Ljava/lang/String;
    .restart local v2    # "dbTypePath":Ljava/lang/String;
    :catch_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "dbPath":Ljava/lang/String;
    .restart local v0    # "dbPath":Ljava/lang/String;
    goto :goto_1
.end method

.method public setCurrentDict(ILjava/lang/String;)I
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 197
    const/4 v1, 0x0

    .line 198
    .local v1, "result":I
    invoke-direct {p0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManager;->createSearchEngine(ILjava/lang/String;)Z

    move-result v0

    .line 200
    .local v0, "engineResult":Z
    if-eqz v0, :cond_0

    .line 201
    iput p1, p0, Lcom/diotek/diodict/core/engine/EngineManager;->mDbType:I

    .line 215
    :goto_0
    return v1

    .line 213
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method
