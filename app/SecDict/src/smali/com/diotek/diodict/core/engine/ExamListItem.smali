.class public Lcom/diotek/diodict/core/engine/ExamListItem;
.super Ljava/lang/Object;
.source "ExamListItem.java"


# instance fields
.field private mExamNum:I

.field private mExample:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "examNum"    # I
    .param p2, "exam"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExamNum:I

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExample:Ljava/lang/String;

    .line 20
    iput p1, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExamNum:I

    .line 21
    iput-object p2, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExample:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getExamNum()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExamNum:I

    return v0
.end method

.method public getExample()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ExamListItem;->mExample:Ljava/lang/String;

    return-object v0
.end method
