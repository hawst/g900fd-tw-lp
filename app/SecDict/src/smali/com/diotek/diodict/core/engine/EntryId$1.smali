.class final Lcom/diotek/diodict/core/engine/EntryId$1;
.super Ljava/lang/Object;
.source "EntryId.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/core/engine/EntryId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/diotek/diodict/core/engine/EntryId;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/EntryId;-><init>()V

    .line 62
    .local v0, "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;->access$002(Lcom/diotek/diodict/core/engine/EntryId;I)I

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;->access$102(Lcom/diotek/diodict/core/engine/EntryId;I)I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I
    invoke-static {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;->access$202(Lcom/diotek/diodict/core/engine/EntryId;I)I

    .line 65
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EntryId$1;->createFromParcel(Landroid/os/Parcel;)Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 70
    new-array v0, p1, [Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/core/engine/EntryId$1;->newArray(I)[Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v0

    return-object v0
.end method
