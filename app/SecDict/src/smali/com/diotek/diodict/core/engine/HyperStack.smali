.class public Lcom/diotek/diodict/core/engine/HyperStack;
.super Ljava/lang/Object;
.source "HyperStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    }
.end annotation


# static fields
.field public static final ERR_FULL:I = 0x2

.field public static final ERR_INVALID_PARAM:I = 0x1

.field public static final ERR_NONE:I = 0x0

.field public static final MAX:I = 0xb


# instance fields
.field private mHyperStackArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    .line 36
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    .line 39
    :cond_0
    return-void
.end method


# virtual methods
.method public addHyperStack(ILjava/lang/String;Lcom/diotek/diodict/core/engine/MeanInfo;)I
    .locals 2
    .param p1, "index"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 63
    :cond_0
    const/4 v0, 0x1

    .line 70
    :goto_0
    return v0

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    invoke-virtual {p3}, Lcom/diotek/diodict/core/engine/MeanInfo;->getEntryId()Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 67
    const/4 v0, 0x2

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    new-instance v1, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    invoke-direct {v1, p1, p2, p3}, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;-><init>(ILjava/lang/String;Lcom/diotek/diodict/core/engine/MeanInfo;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public availableAdd()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 80
    :cond_0
    return-void
.end method

.method public getCountHyperStack()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 194
    const/4 v0, -0x1

    .line 196
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFirst()Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    goto :goto_0
.end method

.method public getLast()Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 147
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    goto :goto_0
.end method

.method public getPrevInfoOfLast()Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmptyOrSingle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    goto :goto_0
.end method

.method public isDouble()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 116
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 124
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public isEmptyOrSingle()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 99
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 107
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeExtendedMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/MeanInfo;)Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    .locals 1
    .param p1, "index"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 207
    new-instance v0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;-><init>(ILjava/lang/String;Lcom/diotek/diodict/core/engine/MeanInfo;)V

    return-object v0
.end method

.method public removeAndGetLast()Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->removeLast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->getLast()Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeLast()Z
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/diotek/diodict/core/engine/HyperStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack;->mHyperStackArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 172
    const/4 v0, 0x1

    goto :goto_0
.end method
