.class public Lcom/diotek/diodict/core/engine/ResultWordList;
.super Ljava/lang/Object;
.source "ResultWordList.java"


# instance fields
.field private mDbType:I

.field private mResultWordList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/diotek/diodict/core/engine/ResultWordListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mDbType:I

    .line 19
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    .line 20
    return-void
.end method

.method private addWordItem(Lcom/diotek/diodict/core/engine/ResultWordListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/diotek/diodict/core/engine/ResultWordListItem;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method


# virtual methods
.method public addWordItem(I[I[ILjava/lang/String;)V
    .locals 1
    .param p1, "headNo"    # I
    .param p2, "entryId"    # [I
    .param p3, "coordList"    # [I
    .param p4, "preview"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordListItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/ResultWordListItem;-><init>(I[I[ILjava/lang/String;)V

    .line 71
    .local v0, "item":Lcom/diotek/diodict/core/engine/ResultWordListItem;
    invoke-direct {p0, v0}, Lcom/diotek/diodict/core/engine/ResultWordList;->addWordItem(Lcom/diotek/diodict/core/engine/ResultWordListItem;)V

    .line 72
    return-void
.end method

.method public clearWordList()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 91
    :cond_0
    return-void
.end method

.method public getDbType()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mDbType:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 29
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getWordItem(I)Lcom/diotek/diodict/core/engine/ResultWordListItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mResultWordList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/ResultWordListItem;

    goto :goto_0
.end method

.method public setDbType(I)V
    .locals 0
    .param p1, "dbType"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/diotek/diodict/core/engine/ResultWordList;->mDbType:I

    .line 51
    return-void
.end method
