.class public Lcom/diotek/diodict/core/engine/DBInfo;
.super Ljava/lang/Object;
.source "DBInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEDT_MAX:I = 0xff02

.field public static final DEL_ENGLISH:I = 0x2

.field public static final DEL_KOREAN:I = 0x0

.field public static final INDEX_ID_ABBREVIATION:I = 0x12

.field public static final INDEX_ID_COMPANYLOGORES:I = 0x4

.field public static final INDEX_ID_CONTENTSVENDOR:I = 0x9

.field public static final INDEX_ID_DBTYPE:I = 0x0

.field public static final INDEX_ID_DHWR_ALLOW:I = 0x14

.field public static final INDEX_ID_DICTNAME:I = 0x1

.field public static final INDEX_ID_ENCODING:I = 0x8

.field public static final INDEX_ID_ENGINEVER:I = 0x15

.field public static final INDEX_ID_FONT_ARRAY:I = 0xe

.field public static final INDEX_ID_HANGULRO_DBNAME:I = 0x10

.field public static final INDEX_ID_ICON:I = 0x2

.field public static final INDEX_ID_LEMMA_DBARRAY:I = 0x11

.field public static final INDEX_ID_LISTICON:I = 0x3

.field public static final INDEX_ID_PAIRDB:I = 0xa

.field public static final INDEX_ID_PARENTDB:I = 0xb

.field public static final INDEX_ID_POWERSEARCH_DBNAME:I = 0x6

.field public static final INDEX_ID_PRODUCTSTRINGRES:I = 0x5

.field public static final INDEX_ID_SOURCELANGUAGE:I = 0xc

.field public static final INDEX_ID_SPELLCHECK_DBNAME:I = 0xf

.field public static final INDEX_ID_SQL_DBNAME:I = 0x7

.field public static final INDEX_ID_TARGETLANGUAGE:I = 0xd

.field public static final INDEX_ID_TTS_ALLOW:I = 0x13

.field private static final serialVersionUID:J = 0x7fe72177d54065d7L


# instance fields
.field private mAbbreviationFileName:Ljava/lang/String;

.field private mCompanyLogoResId:I

.field private mContentsVendor:I

.field private mDbType:I

.field private mEncode:I

.field private mEngineVersion:I

.field private mFontArrayResId:[Ljava/lang/CharSequence;

.field private mHangulroFileName:Ljava/lang/String;

.field private mIconResId:I

.field private mLemmaDbArrayResId:[Ljava/lang/CharSequence;

.field private mLemmaPath:Ljava/lang/String;

.field private mListIconResId:I

.field private mNameResId:I

.field private mPairDBType:I

.field private mParentDBType:I

.field private mProductStringResId:I

.field private mSQLFileName:Ljava/lang/String;

.field private mSearchFileName:Ljava/lang/String;

.field private mSourceLanguage:I

.field private mSpellcheckFileName:Ljava/lang/String;

.field private mTargetLanguage:I


# direct methods
.method public constructor <init>(IIIIIILjava/lang/String;Ljava/lang/String;IIIIII[Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;I)V
    .locals 2
    .param p1, "dbType"    # I
    .param p2, "dicname"    # I
    .param p3, "nIconId"    # I
    .param p4, "nListIconId"    # I
    .param p5, "nCompanyLogoResId"    # I
    .param p6, "nProductStringResId"    # I
    .param p7, "pwDbName"    # Ljava/lang/String;
    .param p8, "sqlDbName"    # Ljava/lang/String;
    .param p9, "encoding"    # I
    .param p10, "vendor"    # I
    .param p11, "pairDB"    # I
    .param p12, "parentDB"    # I
    .param p13, "sourceLang"    # I
    .param p14, "targetLang"    # I
    .param p15, "fontArrayResId"    # [Ljava/lang/CharSequence;
    .param p16, "spellcheckDbName"    # Ljava/lang/String;
    .param p17, "hangulroDbName"    # Ljava/lang/String;
    .param p18, "lemmaDbArrayResId"    # [Ljava/lang/CharSequence;
    .param p19, "abbreviationFileName"    # Ljava/lang/String;
    .param p20, "engineVersion"    # I

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p1, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mDbType:I

    .line 130
    iput p11, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mPairDBType:I

    .line 131
    iput p12, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mParentDBType:I

    .line 132
    iput-object p7, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSearchFileName:Ljava/lang/String;

    .line 133
    iput-object p8, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSQLFileName:Ljava/lang/String;

    .line 134
    iput p9, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mEncode:I

    .line 135
    iput p10, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mContentsVendor:I

    .line 136
    iput p2, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mNameResId:I

    .line 137
    iput p3, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mIconResId:I

    .line 138
    iput p4, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mListIconResId:I

    .line 139
    iput p5, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mCompanyLogoResId:I

    .line 140
    iput p6, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mProductStringResId:I

    .line 141
    iput p13, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSourceLanguage:I

    .line 142
    move/from16 v0, p14

    iput v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mTargetLanguage:I

    .line 143
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mFontArrayResId:[Ljava/lang/CharSequence;

    .line 144
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSpellcheckFileName:Ljava/lang/String;

    .line 145
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mHangulroFileName:Ljava/lang/String;

    .line 146
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mLemmaDbArrayResId:[Ljava/lang/CharSequence;

    .line 147
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mLemmaDbArrayResId:[Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/diotek/diodict/core/engine/DBInfo;->parseLemmaPath([Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mLemmaPath:Ljava/lang/String;

    .line 148
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mAbbreviationFileName:Ljava/lang/String;

    .line 149
    move/from16 v0, p20

    iput v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mEngineVersion:I

    .line 150
    return-void
.end method

.method private parseLemmaPath([Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 7
    .param p1, "lemmaArray"    # [Ljava/lang/CharSequence;

    .prologue
    .line 153
    if-eqz p1, :cond_0

    array-length v5, p1

    if-nez v5, :cond_1

    .line 154
    :cond_0
    const-string v5, ""

    .line 161
    :goto_0
    return-object v5

    .line 157
    :cond_1
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/CharSequence;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 158
    .local v1, "filePath":Ljava/lang/CharSequence;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 159
    .local v4, "strPath":Ljava/lang/String;
    const/4 v5, 0x0

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 161
    .end local v1    # "filePath":Ljava/lang/CharSequence;
    .end local v4    # "strPath":Ljava/lang/String;
    :cond_2
    const-string v5, ""

    goto :goto_0
.end method


# virtual methods
.method public getAbbreviationFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mAbbreviationFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanyLogoResId()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mCompanyLogoResId:I

    return v0
.end method

.method public getContentsVendor()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mContentsVendor:I

    return v0
.end method

.method public getDBType()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mDbType:I

    return v0
.end method

.method public getDicNameResId()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mNameResId:I

    return v0
.end method

.method public getEncode()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mEncode:I

    return v0
.end method

.method public getEngineVersion()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mEngineVersion:I

    return v0
.end method

.method public getFontArrayResId()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mFontArrayResId:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getHangulroFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mHangulroFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getIconResID()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mIconResId:I

    return v0
.end method

.method public getLemmaDbArrayResId()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mLemmaDbArrayResId:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getLemmaPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mLemmaPath:Ljava/lang/String;

    return-object v0
.end method

.method public getListIconResID()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mListIconResId:I

    return v0
.end method

.method public getPairDBType()I
    .locals 2

    .prologue
    .line 241
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mPairDBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_0

    .line 242
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mDbType:I

    .line 244
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mPairDBType:I

    goto :goto_0
.end method

.method public getParentDBType()I
    .locals 2

    .prologue
    .line 276
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mParentDBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_0

    .line 277
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mDbType:I

    .line 279
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mParentDBType:I

    goto :goto_0
.end method

.method public getProductStringResId()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mProductStringResId:I

    return v0
.end method

.method public getSQLFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSQLFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSearchFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceLanguage()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSourceLanguage:I

    return v0
.end method

.method public getSpellcheckFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mSpellcheckFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetLanguage()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mTargetLanguage:I

    return v0
.end method

.method public isIndepencenceMainOnly()Z
    .locals 2

    .prologue
    .line 344
    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mDbType:I

    iget v1, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mPairDBType:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/diotek/diodict/core/engine/DBInfo;->mPairDBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
