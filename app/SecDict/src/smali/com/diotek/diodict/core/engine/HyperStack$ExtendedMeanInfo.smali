.class public Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;
.super Ljava/lang/Object;
.source "HyperStack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/core/engine/HyperStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtendedMeanInfo"
.end annotation


# instance fields
.field private mKeyword:Ljava/lang/String;

.field private mListIndex:I

.field private mMeanInfo:Lcom/diotek/diodict/core/engine/MeanInfo;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/diotek/diodict/core/engine/MeanInfo;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    const/4 v1, 0x0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const/4 v0, 0x0

    iput v0, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mListIndex:I

    .line 221
    iput-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mKeyword:Ljava/lang/String;

    .line 223
    iput-object v1, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mMeanInfo:Lcom/diotek/diodict/core/engine/MeanInfo;

    .line 233
    iput p1, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mListIndex:I

    .line 234
    iput-object p2, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mKeyword:Ljava/lang/String;

    .line 235
    iput-object p3, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mMeanInfo:Lcom/diotek/diodict/core/engine/MeanInfo;

    .line 236
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mListIndex:I

    return v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getMeanInfo()Lcom/diotek/diodict/core/engine/MeanInfo;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/HyperStack$ExtendedMeanInfo;->mMeanInfo:Lcom/diotek/diodict/core/engine/MeanInfo;

    return-object v0
.end method
