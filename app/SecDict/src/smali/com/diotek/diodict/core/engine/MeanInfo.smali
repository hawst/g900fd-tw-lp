.class public Lcom/diotek/diodict/core/engine/MeanInfo;
.super Ljava/lang/Object;
.source "MeanInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict/core/engine/MeanInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContentsVendor:I

.field private mDbType:I

.field private mEngineVer:I

.field private mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

.field private mKeyword:Ljava/lang/String;

.field private mMeanHtml:Ljava/lang/String;

.field private mUniqueId:I

.field private mXrid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/diotek/diodict/core/engine/MeanInfo$1;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/MeanInfo$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict/core/engine/MeanInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/diotek/diodict/core/engine/EntryId;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uniqueId"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "keyword"    # Ljava/lang/String;
    .param p4, "mean"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mUniqueId:I

    .line 31
    iput v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    .line 33
    iput v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I

    .line 35
    iput v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I

    .line 45
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mUniqueId:I

    .line 46
    iput-object p2, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 47
    iput-object p3, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static synthetic access$002(Lcom/diotek/diodict/core/engine/MeanInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/MeanInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/diotek/diodict/core/engine/MeanInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/MeanInfo;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    return p1
.end method

.method static synthetic access$202(Lcom/diotek/diodict/core/engine/MeanInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/MeanInfo;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I

    return p1
.end method

.method static synthetic access$302(Lcom/diotek/diodict/core/engine/MeanInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/MeanInfo;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I

    return p1
.end method


# virtual methods
.method public compareMainInfo(Lcom/diotek/diodict/core/engine/MeanInfo;)Z
    .locals 2
    .param p1, "targetMeanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 240
    if-eqz p1, :cond_4

    .line 241
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mUniqueId:I

    iget v1, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getEntryId()Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v0

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getEntryId()Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v0

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getEntryId()Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    iget v1, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    iget-object v1, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    iget-object v1, p1, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 249
    :cond_3
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public getClone()Lcom/diotek/diodict/core/engine/MeanInfo;
    .locals 2

    .prologue
    .line 261
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/core/engine/MeanInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-object v1

    .line 262
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 265
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getContentsVendor()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I

    return v0
.end method

.method public getDbType()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    return v0
.end method

.method public getEngineVer()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I

    return v0
.end method

.method public getEntryId()Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getMeanHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueId()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mUniqueId:I

    return v0
.end method

.method public getXrid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    return-object v0
.end method

.method public hasMeanHtml()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 132
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 136
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasXrid()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 106
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isKeywordType()Z
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 229
    :cond_0
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentsVendor(I)V
    .locals 0
    .param p1, "vendor"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I

    .line 180
    return-void
.end method

.method public setDbType(I)V
    .locals 0
    .param p1, "dbType"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    .line 163
    return-void
.end method

.method public setEngineVer(I)V
    .locals 0
    .param p1, "engineVer"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I

    .line 171
    return-void
.end method

.method public setMeanHtml(Ljava/lang/String;)V
    .locals 0
    .param p1, "mean"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setXrid(Ljava/lang/String;)V
    .locals 0
    .param p1, "xrid"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 213
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mUniqueId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 214
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 215
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mKeyword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mMeanHtml:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mXrid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mDbType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 219
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mEngineVer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget v0, p0, Lcom/diotek/diodict/core/engine/MeanInfo;->mContentsVendor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    return-void
.end method
