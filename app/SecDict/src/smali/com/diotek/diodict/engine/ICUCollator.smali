.class public Lcom/diotek/diodict/engine/ICUCollator;
.super Ljava/lang/Object;
.source "ICUCollator.java"


# static fields
.field private static collatorInstance:Lcom/diotek/diodict/engine/ICUCollator;

.field private static mRuledCollator:Ljava/text/RuleBasedCollator;


# instance fields
.field private mOpenedICURuleFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    sput-object v0, Lcom/diotek/diodict/engine/ICUCollator;->collatorInstance:Lcom/diotek/diodict/engine/ICUCollator;

    .line 21
    sput-object v0, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/diotek/diodict/engine/ICUCollator;->mOpenedICURuleFile:Ljava/lang/String;

    return-void
.end method

.method public static getInstance()Lcom/diotek/diodict/engine/ICUCollator;
    .locals 2

    .prologue
    .line 30
    const-class v1, Lcom/diotek/diodict/engine/ICUCollator;

    monitor-enter v1

    .line 31
    :try_start_0
    sget-object v0, Lcom/diotek/diodict/engine/ICUCollator;->collatorInstance:Lcom/diotek/diodict/engine/ICUCollator;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/diotek/diodict/engine/ICUCollator;

    invoke-direct {v0}, Lcom/diotek/diodict/engine/ICUCollator;-><init>()V

    sput-object v0, Lcom/diotek/diodict/engine/ICUCollator;->collatorInstance:Lcom/diotek/diodict/engine/ICUCollator;

    .line 35
    :cond_0
    sget-object v0, Lcom/diotek/diodict/engine/ICUCollator;->collatorInstance:Lcom/diotek/diodict/engine/ICUCollator;

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isUsableICU(I)Z
    .locals 4
    .param p0, "dbType"    # I

    .prologue
    .line 118
    :try_start_0
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getIcuDBFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v2, "table":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    const/4 v3, 0x1

    .line 126
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "table":Ljava/io/File;
    :goto_0
    return v3

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private readRule(I)Ljava/lang/String;
    .locals 9
    .param p1, "dbType"    # I

    .prologue
    .line 80
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .local v4, "rules":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 83
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p1}, Lcom/diotek/diodict/engine/ICUCollator;->isUsableICU(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 84
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getIcuDBFileName(I)Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v8, "UTF-16"

    invoke-static {v8}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 90
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .line 91
    .local v5, "temp":Ljava/lang/String;
    if-nez v5, :cond_2

    move-object v0, v1

    .line 101
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .end local v3    # "path":Ljava/lang/String;
    .end local v5    # "temp":Ljava/lang/String;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 103
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 108
    :cond_1
    :goto_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 94
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v3    # "path":Ljava/lang/String;
    .restart local v5    # "temp":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 98
    .end local v5    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 99
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .end local v3    # "path":Ljava/lang/String;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 104
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 105
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 98
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    goto :goto_3
.end method


# virtual methods
.method public compareTo(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "a"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;

    .prologue
    .line 68
    sget-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    if-nez v2, :cond_0

    .line 69
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 75
    :goto_0
    return v2

    .line 72
    :cond_0
    new-instance v0, Lcom/diotek/diodict/engine/StringKey;

    sget-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    invoke-virtual {v2, p1}, Ljava/text/RuleBasedCollator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/diotek/diodict/engine/StringKey;-><init>(Ljava/lang/String;Ljava/text/CollationKey;)V

    .line 73
    .local v0, "keyA":Lcom/diotek/diodict/engine/StringKey;
    new-instance v1, Lcom/diotek/diodict/engine/StringKey;

    sget-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    invoke-virtual {v2, p2}, Ljava/text/RuleBasedCollator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lcom/diotek/diodict/engine/StringKey;-><init>(Ljava/lang/String;Ljava/text/CollationKey;)V

    .line 75
    .local v1, "keyB":Lcom/diotek/diodict/engine/StringKey;
    invoke-virtual {v0, v1}, Lcom/diotek/diodict/engine/StringKey;->compareTo(Lcom/diotek/diodict/engine/StringKey;)I

    move-result v2

    goto :goto_0
.end method

.method public initialize(I)V
    .locals 4
    .param p1, "dbType"    # I

    .prologue
    .line 44
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getIcuDBFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "fileName":Ljava/lang/String;
    iget-object v2, p0, Lcom/diotek/diodict/engine/ICUCollator;->mOpenedICURuleFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 59
    :goto_0
    return-void

    .line 49
    :cond_0
    sget-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    if-eqz v2, :cond_1

    .line 50
    const/4 v2, 0x0

    sput-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    .line 54
    :cond_1
    :try_start_0
    new-instance v2, Ljava/text/RuleBasedCollator;

    invoke-direct {p0, p1}, Lcom/diotek/diodict/engine/ICUCollator;->readRule(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/text/RuleBasedCollator;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/diotek/diodict/engine/ICUCollator;->mRuledCollator:Ljava/text/RuleBasedCollator;

    .line 55
    iput-object v1, p0, Lcom/diotek/diodict/engine/ICUCollator;->mOpenedICURuleFile:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
