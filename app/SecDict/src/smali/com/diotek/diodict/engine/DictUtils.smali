.class public Lcom/diotek/diodict/engine/DictUtils;
.super Ljava/lang/Object;
.source "DictUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ICU_CompareString([B[B)I
    .locals 7
    .param p0, "a"    # [B
    .param p1, "b"    # [B

    .prologue
    const/4 v6, 0x0

    .line 441
    const-string v2, "UTF-16LE"

    .line 442
    .local v2, "encoding":Ljava/lang/String;
    array-length v5, p0

    new-array v0, v5, [B

    .line 443
    .local v0, "arr1":[B
    array-length v5, p1

    new-array v1, v5, [B

    .line 445
    .local v1, "arr2":[B
    array-length v5, p0

    invoke-static {p0, v6, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 446
    array-length v5, p1

    invoke-static {p1, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 448
    const-string v5, "UTF-16LE"

    invoke-static {v0, v5, v6}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "strA":Ljava/lang/String;
    const-string v5, "UTF-16LE"

    invoke-static {v1, v5, v6}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 451
    .local v4, "strB":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 452
    const-string v3, ""

    .line 454
    :cond_0
    if-nez v4, :cond_1

    .line 455
    const-string v4, ""

    .line 457
    :cond_1
    invoke-static {}, Lcom/diotek/diodict/engine/ICUCollator;->getInstance()Lcom/diotek/diodict/engine/ICUCollator;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Lcom/diotek/diodict/engine/ICUCollator;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    return v5
.end method

.method public static getCodePage(C)I
    .locals 6
    .param p0, "ch"    # C

    .prologue
    const/16 v5, 0x30

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/16 v4, 0xc0

    const/16 v1, 0xff

    .line 188
    const/16 v3, 0x5b

    if-eq p0, v3, :cond_0

    const/16 v3, 0x28

    if-eq p0, v3, :cond_0

    const/16 v3, 0xad

    if-eq p0, v3, :cond_0

    const/16 v3, 0xaf

    if-eq p0, v3, :cond_0

    const/16 v3, 0xb7

    if-eq p0, v3, :cond_0

    const/16 v3, 0xa8

    if-eq p0, v3, :cond_0

    const/16 v3, 0xb7

    if-eq p0, v3, :cond_0

    const/16 v3, 0xb0

    if-eq p0, v3, :cond_0

    const/16 v3, 0xb4

    if-eq p0, v3, :cond_0

    const/16 v3, 0xb8

    if-ne p0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 308
    :cond_1
    :goto_0
    return v0

    .line 196
    :cond_2
    const/16 v3, 0x41

    if-lt p0, v3, :cond_3

    const/16 v3, 0x5a

    if-le p0, v3, :cond_1

    :cond_3
    const/16 v3, 0x61

    if-lt p0, v3, :cond_4

    const/16 v3, 0x7a

    if-le p0, v3, :cond_1

    :cond_4
    if-lt p0, v4, :cond_5

    const/16 v3, 0xf6

    if-le p0, v3, :cond_1

    :cond_5
    const/16 v3, 0xf8

    if-lt p0, v3, :cond_6

    if-le p0, v1, :cond_1

    :cond_6
    if-lt p0, v5, :cond_7

    const/16 v3, 0x39

    if-le p0, v3, :cond_1

    :cond_7
    const v3, 0xa312

    if-eq p0, v3, :cond_1

    if-lt p0, v4, :cond_8

    if-le p0, v1, :cond_1

    :cond_8
    const/16 v3, 0x152

    if-lt p0, v3, :cond_9

    const/16 v3, 0x153

    if-le p0, v3, :cond_1

    :cond_9
    const/16 v3, 0x17d

    if-lt p0, v3, :cond_a

    const/16 v3, 0x17e

    if-le p0, v3, :cond_1

    :cond_a
    const/16 v3, 0x178

    if-eq p0, v3, :cond_1

    const/16 v3, 0x192

    if-eq p0, v3, :cond_1

    const/16 v3, 0x2c6

    if-eq p0, v3, :cond_1

    const/16 v3, 0x2dc

    if-eq p0, v3, :cond_1

    const/16 v3, 0x25b

    if-eq p0, v3, :cond_1

    const/16 v3, 0x1e00

    if-lt p0, v3, :cond_b

    const/16 v3, 0x1eff

    if-le p0, v3, :cond_1

    :cond_b
    const/16 v3, 0x212b

    if-eq p0, v3, :cond_1

    const/16 v3, 0x101

    if-eq p0, v3, :cond_1

    const/16 v3, 0x12b

    if-eq p0, v3, :cond_1

    const/16 v3, 0x152

    if-eq p0, v3, :cond_1

    const/16 v3, 0x153

    if-eq p0, v3, :cond_1

    const/16 v3, 0x16b

    if-eq p0, v3, :cond_1

    const/16 v3, 0x160

    if-lt p0, v3, :cond_c

    const/16 v3, 0x161

    if-le p0, v3, :cond_1

    :cond_c
    const/16 v3, 0x2013

    if-lt p0, v3, :cond_d

    const/16 v3, 0x2014

    if-le p0, v3, :cond_1

    :cond_d
    const/16 v3, 0x2018

    if-lt p0, v3, :cond_e

    const/16 v3, 0x201a

    if-le p0, v3, :cond_1

    :cond_e
    const/16 v3, 0x201c

    if-lt p0, v3, :cond_f

    const/16 v3, 0x201e

    if-le p0, v3, :cond_1

    :cond_f
    const/16 v3, 0x2020

    if-lt p0, v3, :cond_10

    const/16 v3, 0x2022

    if-le p0, v3, :cond_1

    :cond_10
    const/16 v3, 0x2026

    if-eq p0, v3, :cond_1

    const/16 v3, 0x2030

    if-eq p0, v3, :cond_1

    const/16 v3, 0x2039

    if-lt p0, v3, :cond_11

    const/16 v3, 0x203a

    if-le p0, v3, :cond_1

    :cond_11
    const/16 v3, 0x20ac

    if-eq p0, v3, :cond_1

    const/16 v3, 0x2122

    if-eq p0, v3, :cond_1

    const/16 v3, 0x100

    if-lt p0, v3, :cond_12

    const/16 v3, 0x241

    if-le p0, v3, :cond_1

    .line 235
    :cond_12
    const/16 v3, 0x1100

    if-lt p0, v3, :cond_13

    const/16 v3, 0x11ff

    if-le p0, v3, :cond_15

    :cond_13
    const/16 v3, 0x3130

    if-lt p0, v3, :cond_14

    const/16 v3, 0x318f

    if-le p0, v3, :cond_15

    :cond_14
    const v3, 0xac00

    if-lt p0, v3, :cond_16

    const v3, 0xd7af

    if-gt p0, v3, :cond_16

    .line 239
    :cond_15
    const/16 v0, 0x3b5

    goto/16 :goto_0

    .line 242
    :cond_16
    const/16 v3, 0x2e80

    if-lt p0, v3, :cond_17

    const/16 v3, 0x2eff

    if-le p0, v3, :cond_24

    :cond_17
    const/16 v3, 0x3100

    if-lt p0, v3, :cond_18

    const/16 v3, 0x312f

    if-le p0, v3, :cond_24

    :cond_18
    const/16 v3, 0x31a0

    if-lt p0, v3, :cond_19

    const/16 v3, 0x31bf

    if-le p0, v3, :cond_24

    :cond_19
    const/16 v3, 0x3400

    if-lt p0, v3, :cond_1a

    const/16 v3, 0x4dbf

    if-le p0, v3, :cond_24

    :cond_1a
    const/16 v3, 0x4e00

    if-lt p0, v3, :cond_1b

    const v3, 0x9faf

    if-le p0, v3, :cond_24

    :cond_1b
    const v3, 0xf900

    if-lt p0, v3, :cond_1c

    const v3, 0xfa32

    if-le p0, v3, :cond_24

    :cond_1c
    const v3, 0xfa84

    if-eq p0, v3, :cond_24

    const v3, 0xfa85

    if-eq p0, v3, :cond_24

    const v3, 0xfa93

    if-eq p0, v3, :cond_24

    const v3, 0xfa94

    if-eq p0, v3, :cond_24

    const v3, 0xfa96

    if-eq p0, v3, :cond_24

    const v3, 0xfab1

    if-lt p0, v3, :cond_1d

    const v3, 0xfaff

    if-le p0, v3, :cond_24

    :cond_1d
    const v3, 0xf897

    if-lt p0, v3, :cond_1e

    const v3, 0xf8c6

    if-le p0, v3, :cond_24

    :cond_1e
    const v3, 0xf8c8

    if-lt p0, v3, :cond_1f

    const v3, 0xf8ed

    if-le p0, v3, :cond_24

    :cond_1f
    const v3, 0xf8f0

    if-lt p0, v3, :cond_20

    const v3, 0xf8f1

    if-le p0, v3, :cond_24

    :cond_20
    const v3, 0xf8f4

    if-lt p0, v3, :cond_21

    const v3, 0xf8ff

    if-le p0, v3, :cond_24

    :cond_21
    const/16 v3, 0x1800

    if-lt p0, v3, :cond_22

    const/16 v3, 0x1c56

    if-le p0, v3, :cond_24

    :cond_22
    const v3, 0xe0d2

    if-lt p0, v3, :cond_23

    const v3, 0xe0d3

    if-le p0, v3, :cond_24

    :cond_23
    const v3, 0xf831

    if-eq p0, v3, :cond_24

    const/16 v3, 0x261e

    if-ne p0, v3, :cond_25

    .line 275
    :cond_24
    const/16 v0, 0x3a8

    goto/16 :goto_0

    .line 277
    :cond_25
    const/16 v3, 0x3040

    if-lt p0, v3, :cond_26

    const/16 v3, 0x309f

    if-le p0, v3, :cond_27

    :cond_26
    const/16 v3, 0x30a0

    if-lt p0, v3, :cond_28

    const/16 v3, 0x30ff

    if-gt p0, v3, :cond_28

    .line 279
    :cond_27
    const/16 v0, 0x3a4

    goto/16 :goto_0

    .line 281
    :cond_28
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isCyrillic(C)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 282
    const/16 v0, 0x556a

    goto/16 :goto_0

    .line 284
    :cond_29
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isGreek(C)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 285
    const/16 v0, 0x4e5

    goto/16 :goto_0

    .line 287
    :cond_2a
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isThai(C)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 288
    const/16 v0, 0x36a

    goto/16 :goto_0

    .line 290
    :cond_2b
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isArabic(C)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 291
    const/16 v0, 0x4e8

    goto/16 :goto_0

    .line 293
    :cond_2c
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHindi(C)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 294
    const/16 v0, 0x533

    goto/16 :goto_0

    .line 296
    :cond_2d
    const/16 v3, 0x41

    if-lt p0, v3, :cond_2e

    const/16 v3, 0x5a

    if-le p0, v3, :cond_1

    :cond_2e
    const/16 v3, 0x61

    if-lt p0, v3, :cond_2f

    const/16 v3, 0x7a

    if-le p0, v3, :cond_1

    :cond_2f
    if-lt p0, v4, :cond_30

    const/16 v3, 0xf6

    if-le p0, v3, :cond_1

    :cond_30
    const/16 v3, 0xf8

    if-lt p0, v3, :cond_31

    if-le p0, v1, :cond_1

    :cond_31
    if-lt p0, v5, :cond_32

    const/16 v3, 0x39

    if-le p0, v3, :cond_1

    :cond_32
    const v3, 0xa312

    if-eq p0, v3, :cond_1

    .line 304
    const/16 v0, 0x21

    if-lt p0, v0, :cond_33

    const/16 v0, 0x7e

    if-gt p0, v0, :cond_33

    move v0, v1

    .line 305
    goto/16 :goto_0

    :cond_33
    move v0, v2

    .line 308
    goto/16 :goto_0
.end method

.method public static getCodePage(Ljava/lang/String;)I
    .locals 10
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x3a8

    const/16 v9, 0xff

    const/16 v8, 0xc0

    const/16 v7, 0xb7

    .line 19
    const/4 v1, -0x1

    .line 20
    .local v1, "codepage":I
    const/4 v0, 0x0

    .line 21
    .local v0, "bSpecialChar":Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 22
    .local v3, "len":I
    const/4 v2, 0x0

    .line 24
    .local v2, "i":I
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->isZhuyinCharacter(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 157
    :goto_0
    return v4

    .line 28
    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 32
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5b

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x28

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xad

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xaf

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v5, v7, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xa8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v5, v7, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xb0

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xb4

    if-eq v5, v6, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xb8

    if-ne v5, v6, :cond_7

    .line 70
    :cond_1
    :goto_2
    invoke-static {v1}, Lcom/diotek/diodict/engine/DictUtils;->isLatinCP(I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 71
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_5

    .line 72
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1100

    if-lt v5, v6, :cond_2

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x11ff

    if-le v5, v6, :cond_4

    :cond_2
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3130

    if-lt v5, v6, :cond_3

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x318f

    if-le v5, v6, :cond_4

    :cond_3
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xac00

    if-lt v5, v6, :cond_14

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xd7af

    if-gt v5, v6, :cond_14

    .line 76
    :cond_4
    const/16 v1, 0x3b5

    .line 148
    :cond_5
    :goto_4
    if-eqz v1, :cond_6

    if-eq v1, v4, :cond_6

    const/16 v4, 0x3a4

    if-eq v1, v4, :cond_6

    const/16 v4, 0x3b5

    if-eq v1, v4, :cond_6

    invoke-static {v1}, Lcom/diotek/diodict/engine/DictUtils;->isLatinCP(I)Z

    move-result v4

    if-nez v4, :cond_6

    const/16 v4, 0x556a

    if-ne v1, v4, :cond_33

    :cond_6
    move v4, v1

    .line 151
    goto/16 :goto_0

    .line 40
    :cond_7
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-lt v5, v8, :cond_8

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-le v5, v9, :cond_12

    :cond_8
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x100

    if-lt v5, v6, :cond_9

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2a7

    if-le v5, v6, :cond_12

    :cond_9
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1e00

    if-lt v5, v6, :cond_a

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1eff

    if-le v5, v6, :cond_12

    :cond_a
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x212b

    if-eq v5, v6, :cond_12

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1c5f

    if-lt v5, v6, :cond_b

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1c68

    if-le v5, v6, :cond_12

    :cond_b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x160

    if-lt v5, v6, :cond_c

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x161

    if-le v5, v6, :cond_12

    :cond_c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2013

    if-lt v5, v6, :cond_d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2014

    if-le v5, v6, :cond_12

    :cond_d
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2018

    if-lt v5, v6, :cond_e

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x201a

    if-le v5, v6, :cond_12

    :cond_e
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x201c

    if-lt v5, v6, :cond_f

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x201e

    if-le v5, v6, :cond_12

    :cond_f
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2020

    if-lt v5, v6, :cond_10

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2022

    if-le v5, v6, :cond_12

    :cond_10
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2026

    if-eq v5, v6, :cond_12

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2030

    if-eq v5, v6, :cond_12

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2039

    if-lt v5, v6, :cond_11

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x203a

    if-le v5, v6, :cond_12

    :cond_11
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x20ac

    if-eq v5, v6, :cond_12

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2122

    if-ne v5, v6, :cond_13

    .line 65
    :cond_12
    const/16 v1, 0x4e4

    .line 66
    goto/16 :goto_2

    .line 28
    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 78
    :cond_14
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2e80

    if-lt v5, v6, :cond_15

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2eff

    if-le v5, v6, :cond_22

    :cond_15
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3100

    if-lt v5, v6, :cond_16

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x312f

    if-le v5, v6, :cond_22

    :cond_16
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x31a0

    if-lt v5, v6, :cond_17

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x31bf

    if-le v5, v6, :cond_22

    :cond_17
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3400

    if-lt v5, v6, :cond_18

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x4dbf

    if-le v5, v6, :cond_22

    :cond_18
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x4e00

    if-lt v5, v6, :cond_19

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0x9faf

    if-le v5, v6, :cond_22

    :cond_19
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf900

    if-lt v5, v6, :cond_1a

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa32

    if-le v5, v6, :cond_22

    :cond_1a
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa84

    if-eq v5, v6, :cond_22

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa85

    if-eq v5, v6, :cond_22

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa93

    if-eq v5, v6, :cond_22

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa94

    if-eq v5, v6, :cond_22

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfa96

    if-eq v5, v6, :cond_22

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfab1

    if-lt v5, v6, :cond_1b

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xfaff

    if-le v5, v6, :cond_22

    :cond_1b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf897

    if-lt v5, v6, :cond_1c

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8c6

    if-le v5, v6, :cond_22

    :cond_1c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8c8

    if-lt v5, v6, :cond_1d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8ed

    if-le v5, v6, :cond_22

    :cond_1d
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8f0

    if-lt v5, v6, :cond_1e

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8f1

    if-le v5, v6, :cond_22

    :cond_1e
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8f4

    if-lt v5, v6, :cond_1f

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf8ff

    if-le v5, v6, :cond_22

    :cond_1f
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1800

    if-lt v5, v6, :cond_20

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x1c56

    if-le v5, v6, :cond_22

    :cond_20
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xe0d2

    if-lt v5, v6, :cond_21

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xe0d3

    if-le v5, v6, :cond_22

    :cond_21
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const v6, 0xf831

    if-ne v5, v6, :cond_23

    .line 111
    :cond_22
    const/16 v1, 0x3a8

    .line 112
    goto/16 :goto_4

    .line 113
    :cond_23
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3040

    if-lt v5, v6, :cond_24

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x309f

    if-le v5, v6, :cond_25

    :cond_24
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x30a0

    if-lt v5, v6, :cond_26

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x30ff

    if-gt v5, v6, :cond_26

    .line 115
    :cond_25
    const/16 v1, 0x3a4

    .line 116
    goto/16 :goto_4

    .line 117
    :cond_26
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x400

    if-lt v5, v6, :cond_27

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x4ff

    if-gt v5, v6, :cond_27

    .line 118
    const/16 v1, 0x556a

    .line 119
    goto/16 :goto_4

    .line 120
    :cond_27
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/diotek/diodict/core/lang/CodeBlock;->isGreek(C)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 121
    const/16 v1, 0x4e5

    .line 122
    goto/16 :goto_4

    .line 123
    :cond_28
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/diotek/diodict/core/lang/CodeBlock;->isThai(C)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 124
    const/16 v1, 0x36a

    .line 125
    goto/16 :goto_4

    .line 126
    :cond_29
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/diotek/diodict/core/lang/CodeBlock;->isArabic(C)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 127
    const/16 v1, 0x4e8

    .line 128
    goto/16 :goto_4

    .line 129
    :cond_2a
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/diotek/diodict/core/lang/CodeBlock;->isHindi(C)Z

    move-result v5

    if-eqz v5, :cond_2b

    .line 130
    const/16 v1, 0x533

    .line 131
    goto/16 :goto_4

    .line 133
    :cond_2b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x41

    if-lt v5, v6, :cond_2c

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5a

    if-le v5, v6, :cond_30

    :cond_2c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x61

    if-lt v5, v6, :cond_2d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x7a

    if-le v5, v6, :cond_30

    :cond_2d
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-lt v5, v8, :cond_2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xf6

    if-le v5, v6, :cond_30

    :cond_2e
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xf8

    if-lt v5, v6, :cond_2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-le v5, v9, :cond_30

    :cond_2f
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/diotek/diodict/engine/DictUtils;->isDioSymbolAlphabet(C)Z

    move-result v5

    if-eqz v5, :cond_31

    .line 139
    :cond_30
    const/4 v1, 0x0

    .line 140
    goto/16 :goto_4

    .line 141
    :cond_31
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x21

    if-lt v5, v6, :cond_32

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x7e

    if-gt v5, v6, :cond_32

    .line 142
    const/4 v0, 0x1

    .line 71
    :cond_32
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 154
    :cond_33
    if-eqz v0, :cond_34

    .line 155
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 157
    :cond_34
    const/4 v4, -0x1

    goto/16 :goto_0
.end method

.method private static isDioSymbolAlphabet(C)Z
    .locals 3
    .param p0, "charAt"    # C

    .prologue
    .line 339
    const/16 v2, 0x1a

    new-array v1, v2, [C

    fill-array-data v1, :array_0

    .line 343
    .local v1, "symbolCode":[C
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 344
    aget-char v2, v1, v0

    if-ne p0, v2, :cond_0

    .line 345
    const/4 v2, 0x1

    .line 348
    :goto_1
    return v2

    .line 343
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 348
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 339
    :array_0
    .array-data 2
        0x1d00s
        0x299s
        0x1d04s
        0x1d05s
        0x1d07s
        -0x1f42s
        0x262s
        0x29cs
        0x26as
        0x1d0as
        0x1d0bs
        0x29fs
        0x1d0ds
        0x274s
        0x1d0fs
        0x1d18s
        -0x1f41s
        0x280s
        -0x1f40s
        0x1d1bs
        0x1d1cs
        0x1d20s
        0x1d21s
        -0x1f3fs
        0x28fs
        0x1d22s
    .end array-data
.end method

.method public static isLatin1CP(I)Z
    .locals 2
    .param p0, "codepage"    # I

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 332
    .local v0, "ret":Z
    const/16 v1, 0x4e4

    if-eq p0, v1, :cond_0

    const/16 v1, 0x4e2

    if-ne p0, v1, :cond_1

    .line 333
    :cond_0
    const/4 v0, 0x1

    .line 335
    :cond_1
    return v0
.end method

.method public static isLatinCP(I)Z
    .locals 2
    .param p0, "codepage"    # I

    .prologue
    .line 317
    const/4 v0, 0x0

    .line 318
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->isLatin1CP(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x4e9

    if-eq p0, v1, :cond_0

    const/16 v1, 0x4e6

    if-ne p0, v1, :cond_1

    .line 320
    :cond_0
    const/4 v0, 0x1

    .line 322
    :cond_1
    return v0
.end method

.method public static isValidWildCondition(Ljava/lang/String;)Z
    .locals 9
    .param p0, "searchWord"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x3f

    const/16 v7, 0x2a

    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v4, 0x0

    .line 396
    if-nez p0, :cond_1

    .line 423
    :cond_0
    :goto_0
    return v4

    .line 400
    :cond_1
    const/4 v1, -0x1

    .line 401
    .local v1, "nPositiveKeyPos":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 403
    .local v2, "size":I
    if-eqz v2, :cond_0

    .line 407
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_6

    .line 408
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v5, v7, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_5

    .line 409
    :cond_2
    if-gt v1, v6, :cond_0

    .line 412
    move v1, v0

    .line 414
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 415
    if-ne v0, v2, :cond_4

    move v4, v3

    .line 416
    goto :goto_0

    .line 419
    :cond_4
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v5, v7, :cond_3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v5, v8, :cond_3

    .line 407
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 423
    :cond_6
    if-le v1, v6, :cond_7

    :goto_2
    move v4, v3

    goto :goto_0

    :cond_7
    move v3, v4

    goto :goto_2
.end method

.method public static isWildcardCharacter(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 381
    const/16 v0, 0x2a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWildcardSearch(Ljava/lang/String;)Z
    .locals 6
    .param p0, "searchWord"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 357
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 359
    .local v1, "size":I
    if-nez v1, :cond_1

    .line 372
    :cond_0
    :goto_0
    return v2

    .line 363
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 364
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2a

    if-eq v4, v5, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x3f

    if-ne v4, v5, :cond_3

    :cond_2
    move v2, v3

    .line 365
    goto :goto_0

    .line 367
    :cond_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const v5, 0xff1f

    if-ne v4, v5, :cond_4

    move v2, v3

    .line 368
    goto :goto_0

    .line 363
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static isZhuyinCharacter(Ljava/lang/String;)Z
    .locals 5
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 165
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v2, v4, :cond_2

    :cond_0
    move v2, v3

    .line 176
    :cond_1
    :goto_0
    return v2

    .line 169
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 170
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 171
    .local v0, "c":C
    const/16 v4, 0x3100

    if-gt v4, v0, :cond_3

    const/16 v4, 0x3130

    if-ge v4, v0, :cond_1

    :cond_3
    const/16 v4, 0x318f

    if-gt v4, v0, :cond_4

    const/16 v4, 0x312f

    if-ge v4, v0, :cond_1

    .line 169
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "c":C
    :cond_5
    move v2, v3

    .line 176
    goto :goto_0
.end method

.method public static logMessage(Ljava/lang/String;)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 431
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NativeLog:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/diotek/diodict/core/util/MSG;->l(ILjava/lang/String;)V

    .line 432
    return-void
.end method
