.class public Lcom/diotek/diodict/engine/EngineManager3rd;
.super Ljava/lang/Object;
.source "EngineManager3rd.java"


# static fields
.field public static final ERROR_NOT_FOUND_SO:I = 0x1

.field public static final ERROR_NOT_FOUND_STARTDICT:I = 0x3

.field public static final ERROR_NO_EXIST_DB_FILE:I = 0x2

.field public static final ERROR_SEARCH_FAIL:I = 0x4

.field public static final NO_ERROR:I

.field private static volatile engineInstance:Lcom/diotek/diodict/engine/EngineManager3rd;


# instance fields
.field private mCurDicType:I

.field private mEngineNoErr:Z

.field private mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

.field private mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/diotek/diodict/engine/EngineManager3rd;->engineInstance:Lcom/diotek/diodict/engine/EngineManager3rd;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mEngineNoErr:Z

    .line 19
    iput-object v1, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 20
    iput-object v1, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mCurDicType:I

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mEngineNoErr:Z

    .line 37
    return-void
.end method

.method private static convertToEngineSearchCharSet(Ljava/lang/String;I)[B
    .locals 1
    .param p0, "word"    # Ljava/lang/String;
    .param p1, "nDicType"    # I

    .prologue
    .line 434
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/diotek/diodict/engine/StringConvert;->makeSearchWord(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/diotek/diodict/engine/EngineManager3rd;
    .locals 2

    .prologue
    .line 44
    const-class v1, Lcom/diotek/diodict/engine/EngineManager3rd;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lcom/diotek/diodict/engine/EngineManager3rd;->engineInstance:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-direct {v0}, Lcom/diotek/diodict/engine/EngineManager3rd;-><init>()V

    sput-object v0, Lcom/diotek/diodict/engine/EngineManager3rd;->engineInstance:Lcom/diotek/diodict/engine/EngineManager3rd;

    .line 48
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    sget-object v0, Lcom/diotek/diodict/engine/EngineManager3rd;->engineInstance:Lcom/diotek/diodict/engine/EngineManager3rd;

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static openDB(I)Z
    .locals 6
    .param p0, "nDicType"    # I

    .prologue
    const/4 v3, 0x0

    .line 347
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDictFilename(I)Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "fileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDBPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "dbFileName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v3

    .line 354
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4, v3}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibOpenDB([BZ)I

    move-result v1

    .line 356
    .local v1, "err":I
    if-nez v1, :cond_0

    .line 357
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private searchTotalWrapper([BI)I
    .locals 4
    .param p1, "wordbyte"    # [B
    .param p2, "dicType"    # I

    .prologue
    .line 542
    const/4 v1, 0x0

    .line 543
    .local v1, "searchednum":I
    const/4 v0, 0x4

    .line 544
    .local v0, "err":I
    const/16 v2, 0x32

    const/4 v3, 0x0

    invoke-static {p1, p2, v2, v3}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibUnifiySearch([BIII)I

    move-result v1

    .line 546
    if-lez v1, :cond_0

    .line 547
    const/4 v0, 0x0

    .line 549
    :cond_0
    return v0
.end method


# virtual methods
.method public changeDictionary(ILjava/lang/String;I)Z
    .locals 2
    .param p1, "nDicType"    # I
    .param p2, "word"    # Ljava/lang/String;
    .param p3, "suid"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 110
    if-ne p1, v1, :cond_0

    .line 119
    :goto_0
    return v0

    .line 113
    :cond_0
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    .line 115
    if-ne p3, v1, :cond_1

    .line 116
    invoke-virtual {p0, p2, v0, v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->searchWord(Ljava/lang/String;IZ)Z

    move-result v0

    goto :goto_0

    .line 119
    :cond_1
    invoke-virtual {p0, p2, p3, v0, v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->searchWordByWL(Ljava/lang/String;IIZ)Z

    move-result v0

    goto :goto_0
.end method

.method public clearResultList(I)V
    .locals 1
    .param p1, "nResultListType"    # I

    .prologue
    const/4 v0, 0x0

    .line 272
    packed-switch p1, :pswitch_data_0

    .line 277
    iput-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 280
    :goto_0
    return-void

    .line 274
    :pswitch_0
    iput-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public createResultList()Lcom/diotek/diodict/engine/ResultWordList3rd;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263
    new-instance v0, Lcom/diotek/diodict/engine/ResultWordList3rd;

    invoke-direct {v0, v1, v1, v1}, Lcom/diotek/diodict/engine/ResultWordList3rd;-><init>(IIZ)V

    .line 264
    .local v0, "resultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    return-object v0
.end method

.method public getCurDict()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mCurDicType:I

    return v0
.end method

.method public getEngineFirstWord()Ljava/lang/String;
    .locals 4

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "firstStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v2

    invoke-static {v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetFirstWordList(I)[B

    move-result-object v1

    .line 389
    .local v1, "firstWordArray":[B
    if-nez v1, :cond_0

    .line 390
    const/4 v2, 0x0

    .line 395
    :goto_0
    return-object v2

    .line 392
    :cond_0
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 395
    goto :goto_0
.end method

.method public getEngineLastWord()Ljava/lang/String;
    .locals 4

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "lastStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v2

    invoke-static {v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetLastWordList(I)[B

    move-result-object v1

    .line 372
    .local v1, "lastWordArray":[B
    if-nez v1, :cond_0

    .line 373
    const/4 v2, 0x0

    .line 378
    :goto_0
    return-object v2

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 378
    goto :goto_0
.end method

.method public getEngineNoError()Z
    .locals 1

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mEngineNoErr:Z

    return v0
.end method

.method public getHyperTextExactMatch(I)Z
    .locals 2
    .param p1, "nResultListType"    # I

    .prologue
    .line 502
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v1

    .line 503
    .local v1, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v1, :cond_0

    .line 504
    const/4 v0, 0x0

    .line 508
    :goto_0
    return v0

    .line 507
    :cond_0
    invoke-virtual {v1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->isBExactmatch()Z

    move-result v0

    .line 508
    .local v0, "bExactmatch":Z
    goto :goto_0
.end method

.method public getMeaning(Ljava/lang/String;IIIIZZI)Ljava/lang/String;
    .locals 8
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I
    .param p4, "enDispMode"    # I
    .param p5, "bufsize"    # I
    .param p6, "isConvertSym"    # Z
    .param p7, "isKeepTag"    # Z
    .param p8, "nMode"    # I

    .prologue
    .line 335
    invoke-static {p1, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->convertToEngineSearchCharSet(Ljava/lang/String;I)[B

    move-result-object v2

    .local v2, "searchwordbyte":[B
    move v0, p3

    move v1, p4

    move v3, p2

    move v4, p5

    move v5, p6

    move v6, p7

    .line 336
    invoke-static/range {v0 .. v6}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetMeaning(II[BIIZZ)[B

    move-result-object v7

    .line 338
    .local v7, "array":[B
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v7, v0, v1}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMeaningData(Ljava/lang/String;IIIZ)Ljava/lang/String;
    .locals 4
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I
    .param p4, "bufsize"    # I
    .param p5, "isKeyword"    # Z

    .prologue
    .line 313
    invoke-static {p1, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->convertToEngineSearchCharSet(Ljava/lang/String;I)[B

    move-result-object v1

    .line 314
    .local v1, "searchwordbyte":[B
    invoke-static {p3, v1, p2, p4, p5}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetMeaningData(I[BIIZ)[B

    move-result-object v0

    .line 316
    .local v0, "array":[B
    const/4 v3, 0x0

    invoke-static {v0, p3, v3}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v2

    .line 318
    .local v2, "str":Ljava/lang/String;
    return-object v2
.end method

.method public getMeaningOutByHTML(II[BII)[B
    .locals 1
    .param p1, "dictype"    # I
    .param p2, "enDispMode"    # I
    .param p3, "word"    # [B
    .param p4, "suid"    # I
    .param p5, "bufsize"    # I

    .prologue
    .line 599
    invoke-static {p1, p2, p3, p4, p5}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetMeaningOutByHTMLWithToken(II[BII)[B

    move-result-object v0

    return-object v0
.end method

.method public getResultDicTypeByPos(II)I
    .locals 2
    .param p1, "nPos"    # I
    .param p2, "nResultListType"    # I

    .prologue
    .line 518
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v1

    .line 519
    .local v1, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v1, :cond_0

    .line 520
    const/4 v0, -0x1

    .line 524
    :goto_0
    return v0

    .line 523
    :cond_0
    invoke-virtual {v1, p1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getDicTypeByPos(I)I

    move-result v0

    .line 524
    .local v0, "nDicType":I
    goto :goto_0
.end method

.method public getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;
    .locals 1
    .param p1, "nResultListType"    # I

    .prologue
    .line 288
    packed-switch p1, :pswitch_data_0

    .line 295
    iget-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->createResultList()Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    :goto_0
    return-object v0

    .line 290
    :pswitch_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    if-nez v0, :cond_1

    .line 291
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->createResultList()Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getResultListCount(I)I
    .locals 2
    .param p1, "nResultListType"    # I

    .prologue
    .line 533
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    .line 534
    .local v0, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v0, :cond_0

    .line 535
    const/4 v1, 0x0

    .line 538
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v1

    goto :goto_0
.end method

.method public getResultListDictByPos(II)I
    .locals 2
    .param p1, "nPos"    # I
    .param p2, "nResultListType"    # I

    .prologue
    .line 474
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    .line 475
    .local v0, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v0, :cond_0

    .line 476
    const/4 v1, -0x1

    .line 479
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getDicTypeByPos(I)I

    move-result v1

    goto :goto_0
.end method

.method public getResultListKeywordByPos(II)Ljava/lang/String;
    .locals 2
    .param p1, "nPos"    # I
    .param p2, "nResultListType"    # I

    .prologue
    .line 459
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    .line 460
    .local v0, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v0, :cond_0

    .line 461
    const/4 v1, 0x0

    .line 464
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getKeywordByPos(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getResultListKeywordPos(I)I
    .locals 2
    .param p1, "nResultListType"    # I

    .prologue
    .line 488
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    .line 489
    .local v0, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v0, :cond_0

    .line 490
    const/4 v1, 0x0

    .line 493
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getKeywordPos()I

    move-result v1

    goto :goto_0
.end method

.method public getResultListSUIDByPos(II)I
    .locals 2
    .param p1, "nPos"    # I
    .param p2, "nResultListType"    # I

    .prologue
    .line 444
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v0

    .line 445
    .local v0, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-nez v0, :cond_0

    .line 446
    const/4 v1, 0x0

    .line 449
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSUIDByPos(I)I

    move-result v1

    goto :goto_0
.end method

.method public getSourceLanguage(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 558
    const/4 v0, -0x1

    .line 560
    .local v0, "sourceLanguage":I
    invoke-static {p1}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetSourceLanguage(I)I

    move-result v0

    .line 562
    return v0
.end method

.method public getTargetLanguage(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 571
    const/4 v0, -0x1

    .line 573
    .local v0, "targetLanguage":I
    invoke-static {p1}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetTargetLanguage(I)I

    move-result v0

    .line 575
    return v0
.end method

.method public initDBManager()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public initNativeEngine()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->initNativeEngineWithResult()I

    move-result v1

    if-nez v1, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 75
    :cond_0
    iput-boolean v0, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mEngineNoErr:Z

    goto :goto_0
.end method

.method public initNativeEngineWithResult()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 84
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->getLastErr()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 85
    iput-boolean v4, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mEngineNoErr:Z

    .line 90
    :goto_0
    return v1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 90
    .local v0, "dbUHCPFullPath":[B
    invoke-static {v0, v4, v1, v4}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibInit([BZZZ)I

    move-result v1

    goto :goto_0
.end method

.method public searchWord(Ljava/lang/String;IZ)Z
    .locals 7
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "nResultListType"    # I
    .param p3, "bWildcard"    # Z

    .prologue
    const/4 v4, 0x0

    .line 131
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v1

    .line 132
    .local v1, "nTmpDicType":I
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 133
    .local v2, "word":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/diotek/diodict/engine/EngineManager3rd;->convertToEngineSearchCharSet(Ljava/lang/String;I)[B

    move-result-object v3

    .line 134
    .local v3, "wordbyte":[B
    const/4 v0, -0x1

    .line 136
    .local v0, "err":I
    if-eqz p3, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v5

    const v6, 0xfff0

    if-ne v5, v6, :cond_0

    .line 138
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->clearResultList(I)V

    .line 156
    :goto_0
    return v4

    .line 143
    :cond_0
    invoke-virtual {p0, v1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    .line 145
    if-eqz p3, :cond_1

    .line 146
    invoke-static {v3}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibWildCardSearch([B)I

    move-result v0

    .line 151
    :goto_1
    if-eqz v0, :cond_2

    .line 152
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->clearResultList(I)V

    goto :goto_0

    .line 148
    :cond_1
    invoke-static {v3, v4}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibSearchWord([BZ)I

    move-result v0

    goto :goto_1

    .line 156
    :cond_2
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->setResultList(I)Z

    move-result v4

    goto :goto_0
.end method

.method public searchWordByWL(Ljava/lang/String;IIZ)Z
    .locals 5
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "nResultListType"    # I
    .param p4, "bWildcard"    # Z

    .prologue
    const/4 v2, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v3

    invoke-static {p1, v3}, Lcom/diotek/diodict/engine/EngineManager3rd;->convertToEngineSearchCharSet(Ljava/lang/String;I)[B

    move-result-object v1

    .line 169
    .local v1, "wordbyte":[B
    const/4 v0, -0x1

    .line 170
    .local v0, "err":I
    if-eqz p4, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v3

    const v4, 0xfff0

    if-ne v3, v4, :cond_0

    .line 172
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->clearResultList(I)V

    .line 187
    :goto_0
    return v2

    .line 176
    :cond_0
    if-eqz p4, :cond_1

    .line 177
    invoke-static {v1, p2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibWildCardSearchByWL([BI)I

    move-result v0

    .line 182
    :goto_1
    if-eqz v0, :cond_2

    .line 183
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->clearResultList(I)V

    goto :goto_0

    .line 179
    :cond_1
    invoke-static {v1, p2, v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibSearchByWL([BIZ)I

    move-result v0

    goto :goto_1

    .line 187
    :cond_2
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->setResultList(I)Z

    move-result v2

    goto :goto_0
.end method

.method public setCurDict(I)V
    .locals 1
    .param p1, "nDicType"    # I

    .prologue
    .line 411
    iput p1, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mCurDicType:I

    .line 413
    const v0, 0xfff0

    if-eq p1, v0, :cond_0

    .line 414
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setNativeDicType(I)V

    .line 416
    :cond_0
    return-void
.end method

.method public setNativeDicType(I)V
    .locals 0
    .param p1, "nDicType"    # I

    .prologue
    .line 98
    invoke-static {p1}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibSetDicType(I)V

    .line 99
    invoke-static {p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->openDB(I)Z

    .line 100
    return-void
.end method

.method public setResultList(I)Z
    .locals 9
    .param p1, "nResultListType"    # I

    .prologue
    const v8, 0xfff0

    const/4 v5, 0x0

    .line 215
    const/4 v3, 0x0

    .line 216
    .local v3, "nTotalSearchDictType":[I
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v1

    .line 217
    .local v1, "nCurDictType":I
    move v2, v1

    .line 218
    .local v2, "nItemDictType":I
    packed-switch p1, :pswitch_data_0

    .line 226
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetResult()Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v6

    iput-object v6, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    .line 229
    :goto_0
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v4

    .line 230
    .local v4, "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    invoke-virtual {v4}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v6

    if-gtz v6, :cond_1

    .line 255
    :cond_0
    :goto_1
    return v5

    .line 220
    .end local v4    # "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    :pswitch_0
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetResult()Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v6

    iput-object v6, p0, Lcom/diotek/diodict/engine/EngineManager3rd;->mHyperResultList:Lcom/diotek/diodict/engine/ResultWordList3rd;

    goto :goto_0

    .line 234
    .restart local v4    # "tmpResultList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    :cond_1
    if-ne v1, v8, :cond_2

    .line 235
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibIntegrationDBArray()[I

    move-result-object v3

    .line 236
    if-eqz v3, :cond_0

    .line 241
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-virtual {v4}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v6

    if-ge v0, v6, :cond_4

    .line 242
    if-ne v1, v8, :cond_3

    .line 243
    if-eqz v3, :cond_0

    .line 246
    aget v2, v3, v0

    .line 249
    :cond_3
    invoke-static {v0, v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetWordList(II)[B

    move-result-object v6

    invoke-static {v0}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetWordSuid(I)I

    move-result v7

    invoke-virtual {v4, v6, v7, v0, v2}, Lcom/diotek/diodict/engine/ResultWordList3rd;->setAstWordList([BIII)V

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 255
    :cond_4
    const/4 v5, 0x1

    goto :goto_1

    .line 218
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public terminateEngine()V
    .locals 2

    .prologue
    .line 582
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->getLastErr()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 583
    const/4 v0, 0x2

    const-string v1, "not found so file"

    invoke-static {v0, v1}, Lcom/diotek/diodict/core/util/MSG;->l(ILjava/lang/String;)V

    .line 587
    :goto_0
    return-void

    .line 586
    :cond_0
    invoke-static {}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibTerminate()I

    goto :goto_0
.end method

.method public totalSearch(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "nResultListType"    # I

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v1

    .line 198
    .local v1, "nTmpDicType":I
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "word":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/diotek/diodict/engine/EngineManager3rd;->convertToEngineSearchCharSet(Ljava/lang/String;I)[B

    move-result-object v3

    .line 200
    .local v3, "wordbyte":[B
    invoke-direct {p0, v3, v1}, Lcom/diotek/diodict/engine/EngineManager3rd;->searchTotalWrapper([BI)I

    move-result v0

    .line 202
    .local v0, "err":I
    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->clearResultList(I)V

    .line 204
    const/4 v4, 0x0

    .line 206
    :goto_0
    return v4

    :cond_0
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/engine/EngineManager3rd;->setResultList(I)Z

    move-result v4

    goto :goto_0
.end method
