.class public Lcom/diotek/diodict/engine/ResultWordList3rd;
.super Ljava/lang/Object;
.source "ResultWordList3rd.java"


# instance fields
.field private mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

.field private mExactmatch:Z

.field private mKeywordPos:I

.field private mSize:I


# direct methods
.method public constructor <init>(IIZ)V
    .locals 1
    .param p1, "vSize"    # I
    .param p2, "vPos"    # I
    .param p3, "exactmatch"    # Z

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mSize:I

    .line 11
    iput v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mKeywordPos:I

    .line 12
    iput-boolean v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mExactmatch:Z

    .line 21
    iput p1, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mSize:I

    .line 22
    iput p2, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mKeywordPos:I

    .line 23
    iput-boolean p3, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mExactmatch:Z

    .line 24
    iget v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mSize:I

    new-array v0, v0, [Lcom/diotek/diodict/engine/WordList3rd;

    iput-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    .line 25
    return-void
.end method


# virtual methods
.method public getAstWordList()[Lcom/diotek/diodict/engine/WordList3rd;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    return-object v0
.end method

.method public getDicTypeByPos(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 78
    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/diotek/diodict/engine/WordList3rd;->getDicType()I

    move-result v0

    goto :goto_0
.end method

.method public getKeywordByPos(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x0

    .line 56
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/diotek/diodict/engine/WordList3rd;->getKeyword()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getKeywordPos()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mKeywordPos:I

    return v0
.end method

.method public getSUIDByPos(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 66
    :cond_0
    const/4 v0, 0x0

    .line 68
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/diotek/diodict/engine/WordList3rd;->getSUID()I

    move-result v0

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mSize:I

    return v0
.end method

.method public getWordList(I)Lcom/diotek/diodict/engine/WordList3rd;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public isBExactmatch()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mExactmatch:Z

    return v0
.end method

.method public setAstWordList([BIII)V
    .locals 2
    .param p1, "vwordList"    # [B
    .param p2, "suid"    # I
    .param p3, "index"    # I
    .param p4, "nDicType"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mAstWordList:[Lcom/diotek/diodict/engine/WordList3rd;

    new-instance v1, Lcom/diotek/diodict/engine/WordList3rd;

    invoke-direct {v1, p1, p2, p4}, Lcom/diotek/diodict/engine/WordList3rd;-><init>([BII)V

    aput-object v1, v0, p3

    .line 116
    return-void
.end method

.method public setBExtractmatch(Z)V
    .locals 0
    .param p1, "extractmatch"    # Z

    .prologue
    .line 139
    iput-boolean p1, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mExactmatch:Z

    .line 140
    return-void
.end method

.method public setNSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mSize:I

    .line 124
    return-void
.end method

.method public setNkeywordPos(I)V
    .locals 0
    .param p1, "nkeywordPos"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/diotek/diodict/engine/ResultWordList3rd;->mKeywordPos:I

    .line 132
    return-void
.end method
