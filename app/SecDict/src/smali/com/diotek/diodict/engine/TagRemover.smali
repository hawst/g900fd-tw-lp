.class public Lcom/diotek/diodict/engine/TagRemover;
.super Ljava/lang/Object;
.source "TagRemover.java"


# static fields
.field private static final UNICODE_END_TAG:Ljava/lang/String; = "?"

.field private static final UNICODE_START_TAG:Ljava/lang/String; = "%U"

.field private static final UNICODE_SYMBOL_TABLE:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/16 v0, 0x392

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/diotek/diodict/engine/TagRemover;->UNICODE_SYMBOL_TABLE:[C

    return-void

    :array_0
    .array-data 2
        0xc1s
        0xc0s
        -0x1ffes
        -0x1ffds
        -0x1ffcs
        -0x1ffbs
        0xc5s
        0xc6s
        0x28cs
        -0x1ff9s
        -0x1ff8s
        0x101s
        0xe1s
        0xe0s
        0xe2s
        0x103s
        0xe3s
        0xe4s
        -0x1ff2s
        -0x1ff1s
        -0x1ff0s
        -0x1fefs
        -0x1fees
        -0x1feds
        -0x1fecs
        0xe6s
        0x1fds
        -0x1fe9s
        0x259s
        -0x1fe7s
        -0x1fe6s
        -0x1fe5s
        -0x1fe4s
        -0x1fe3s
        -0x1fe2s
        -0x1fe1s
        0x106s
        0x10cs
        0x107s
        -0x1fe0s
        -0x1fdfs
        -0x1fdes
        0xe7s
        -0x1fdcs
        0xc9s
        0xc8s
        -0x1fd9s
        -0x1fd8s
        -0x1fd7s
        0x113s
        0xe9s
        0xe8s
        0xeas
        0x115s
        0xebs
        -0x1fd2s
        0x25bs
        -0x1fd0s
        -0x1fcfs
        -0x1fces
        0x283s
        -0x1fccs
        -0x1fcbs
        -0x1fcas
        -0x1fc9s
        0x124s
        -0x1fc8s
        -0x1fc7s
        0x272s
        -0x1fc5s
        0xcds
        0xccs
        -0x1fc2s
        -0x1fc1s
        -0x1fc0s
        0x12bs
        0xeds
        0xecs
        0xees
        0x1d0s
        0xefs
        -0x1fbas
        -0x1fb9s
        -0x1fb8s
        0x139s
        -0x1fb7s
        -0x1fb6s
        -0x1fb5s
        0x144s
        0xf1s
        -0x1fb3s
        0xd3s
        0xd2s
        -0x1fb0s
        -0x1fafs
        -0x1faes
        -0x1fads
        0x14ds
        0xf3s
        0xf2s
        0xf4s
        0x1d2s
        -0x1fa9s
        0xf6s
        -0x1fa7s
        0xf8s
        -0x1fa5s
        0x153s
        -0x1fa3s
        -0x1fa2s
        -0x1fa1s
        0x254s
        -0x1f9fs
        -0x1f9es
        -0x1f9ds
        -0x1f9cs
        0xf0s
        0x3b8s
        0x14bs
        0x154s
        0x159s
        -0x1f98s
        0x15as
        0x161s
        0xdas
        0xd9s
        -0x1f95s
        0xdcs
        -0x1f93s
        0x16bs
        0xfas
        0xf9s
        0xfbs
        0x1d4s
        0xfcs
        0x1d8s
        0x1dcs
        0x1das
        -0x1f8ds
        -0x1f8cs
        0x28as
        0x1e82s
        0x1e83s
        -0x1f8as
        0xdds
        0x1ef2s
        0xfds
        0x1ef3s
        0x179s
        0x292s
        -0x1f86s
        0x3b1s
        0x3b2s
        0x3b3s
        0x3b8s
        0x3bcs
        0x3c0s
        0x3a3s
        0x3bbs
        0x3c3s
        0x3c8s
        0x2070s
        0xb9s
        0xb2s
        0xb3s
        0x2074s
        0x2075s
        0x2076s
        0x2077s
        0x2078s
        0x2079s
        -0x1f59s
        -0x1f58s
        -0x1f57s
        -0x1f56s
        -0x1f55s
        -0x1f75s
        -0x1f74s
        -0x1f73s
        -0x1f72s
        -0x1f71s
        -0x1f70s
        -0x1f6fs
        -0x1f6es
        -0x1f6ds
        -0x1f6cs
        -0x1f6bs
        -0x1f6as
        -0x1f69s
        -0x1f68s
        -0x1f67s
        -0x1f66s
        -0x1f65s
        -0x1f64s
        -0x1f63s
        -0x1f62s
        -0x1f61s
        -0x1f60s
        -0x1f5fs
        -0x1f5es
        -0x1f5ds
        -0x1f5cs
        -0x1f5bs
        -0x1f5as
        -0x1f51s
        -0x1f54s
        -0x1f53s
        -0x1f52s
        0x2d0s
        -0x1f7fs
        -0x1f7es
        -0x1f7ds
        -0x1f7cs
        -0x1f7bs
        -0x1f7as
        -0x1f79s
        -0x1f78s
        -0x1f77s
        -0x1f76s
        0x2020s
        0x5es
        0x2c7s
        0x2d8s
        0x2das
        0x2dcs
        0x2cas
        0x2cfs
        0x32cs
        0xa7s
        0xb7s
        0x2026s
        0x33ds
        0x2228s
        0x2014s
        0x203bs
        0x3010s
        0x3011s
        0x25cbs
        0x25a0s
        0x252s
        0x25cs
        0x2b3s
        0x2b0s
        0x1d4as
        -0x1f4cs
        -0x1f4bs
        0x1e0ds
        0x1e25s
        0x1e63s
        0x1e6ds
        0x1e93s
        0x10ds
        0x142s
        0x16fs
        0x2082s
        0xe5s
        0x2080s
        0x2081s
        0x2083s
        0x2084s
        0x2085s
        0x2086s
        0x2087s
        0x2088s
        0x2089s
        0x148s
        0x15fs
        0x397s
        0x2013s
        0x207bs
        0x2207s
        0x25abs
        0x2610s
        0x2666s
        0x266es
        0x266fs
        0x221as
        0x2714s
        0x2717s
        0x301as
        0x301bs
        -0x1f4as
        -0x1f49s
        -0x1f48s
        -0x5faes
        -0x5f7cs
        -0x7bcs
        -0x1f46s
        -0x1f45s
        -0x1f44s
        -0x1f43s
        -0x1f3as
        0x1d00s
        0x299s
        0x1d04s
        0x1d05s
        0x1d07s
        -0x1f42s
        0x262s
        0x29cs
        0x26as
        0x1d0as
        0x1d0bs
        0x29fs
        0x1d0ds
        0x274s
        0x1d0fs
        0x1d18s
        -0x1f41s
        0x280s
        -0x1f40s
        0x1d1bs
        0x1d1cs
        0x1d20s
        0x1d21s
        -0x1f3fs
        0x28fs
        0x1d22s
        -0x1f3es
        -0x1f3ds
        -0x1f3cs
        0x2021s
        0x2248s
        0x24c7s
        0x2726s
        0x207fs
        0x2c8s
        0x2ccs
        0x28es
        0x2a4s
        0x2a7s
        -0x1f3bs
        0x20a9s
        0x28as
        0x21ds
        0x2260s
        0x2116s
        0x2727s
        0x25b6s
        0x25b7s
        0x2bcs
        -0x1fces
        0x1ebds
        0x129s
        0x169s
        0x2dcs
        -0x1fa1s
        -0x1f39s
        -0x1f38s
        -0x1f37s
        -0xfe0s
        -0xfdfs
        -0xfdes
        -0xfdds
        -0xfdcs
        -0xfdbs
        -0xfdas
        -0xfd9s
        -0xfd8s
        -0xfd7s
        -0xfd6s
        -0xfd5s
        -0xfd4s
        -0xfd3s
        -0xfd1s
        0x215cs
        -0x1f36s
        -0x1f35s
        0x20acs
        0xbds
        0x14cs
        0x207as
        0x2153s
        0x24d0s
        0x24d1s
        0x24d2s
        0x2ecas
        0x3b74s
        0x3e74s
        0x415fs
        0x44dds
        0x45e9s
        0x4d18s
        0x4d19s
        0x4d88s
        0x100s
        -0x1f34s
        -0x1f33s
        0x27a1s
        -0x1f30s
        0x25c8s
        0x2605s
        0x2663s
        0x2660s
        0x2665s
        0x266ds
        0x25c7s
        0x261es
        0x394s
        0x393s
        0x39bs
        0x3a9s
        0x3a6s
        0x3a0s
        0x3a8s
        0x2032s
        0x2033s
        0x201cs
        0x201ds
        0x2018s
        0x2019s
        0x3c7s
        0x3b7s
        0x3b9s
        0x3c9s
        0x3c6s
        0x3c1s
        0x3c4s
        0x3bes
        0x3b6s
        0x2192s
        0x2194s
        0x251s
        0x221bs
        0x2122s
        0x2261s
        0x398s
        0x39es
        0x3a5s
        0x3bds
        0x215ds
        0x215es
        -0x1f32s
        -0x1f31s
        -0x200s
        -0x1ffs
        -0x1fes
        -0x1fds
        -0x1fcs
        -0x1fbs
        -0x1fas
        -0x1f9s
        0x3003s
        0x3b4s
        0x3bas
        -0x578s
        0x105s
        0x109s
        0x117s
        0x119s
        0x11bs
        0x11ds
        0x125s
        0x135s
        0x151s
        0x15bs
        0x15ds
        0x15es
        0x160s
        0x16as
        0x173s
        0x17as
        0x17cs
        0x17ds
        0x17es
        0x1b1s
        0x1e7s
        0x21bs
        0x227s
        0x229s
        0x395s
        0x3a1s
        0x3bfs
        0x4abs
        0x4f1s
        0x11e2s
        0x1e24s
        0x1e2bs
        0x1e37s
        0x1e43s
        0x1e45s
        0x1e47s
        0x1e49s
        0x1e5as
        0x1e5bs
        0x1e60s
        0x1e62s
        0x1e6cs
        0x1e92s
        0x1e94s
        0x1ea1s
        0x1fd1s
        0x1fd9s
        0x2022s
        0x207ds
        0x207es
        0x208bs
        0x2090s
        0x2093s
        0x216as
        0x216bs
        0x21c4s
        0x2214s
        0x2202s
        0x220bs
        0x2262s
        0x2264s
        0x2265s
        0x2267s
        0x24dds
        0x258ds
        0x2627s
        0x2630s
        0x2631s
        0x2632s
        0x2633s
        0x2634s
        0x2635s
        0x2636s
        0x2637s
        0x2640s
        0x2667s
        0x321cs
        0x3388s
        0x3389s
        0x338es
        0x3390s
        0x3391s
        0x3392s
        0x3393s
        0x3398s
        0x339as
        0x339fs
        0x33a3s
        0x33a8s
        0x33a9s
        0x33b2s
        0x33b3s
        0x33b7s
        0x33b8s
        0x33bds
        0x33bes
        0x33bfs
        0x33c4s
        0x33c5s
        0x33c8s
        0x33cas
        0x33cfs
        0x33d6s
        -0x5fa3s
        -0x1e59s
        -0x1d7ds
        -0x1d7bs
        -0x1d78s
        -0x1d67s
        -0x1c76s
        -0x1c58s
        -0x1b94s
        -0x1b90s
        -0x1a9as
        -0x19b6s
        -0x19b1s
        -0x19ads
        -0x17a9s
        -0x175fs
        -0x1759s
        -0x1757s
        -0x1756s
        -0x174es
        -0x1683s
        -0x1675s
        -0x166fs
        -0x1641s
        -0x158fs
        -0x156fs
        -0x1519s
        -0x12f4s
        -0x1271s
        -0x1178s
        -0x1174s
        -0x116bs
        -0xeacs
        -0xe04s
        -0xe00s
        -0xcbfs
        -0xcacs
        -0xbb8s
        -0xac9s
        -0xac6s
        -0xac4s
        -0xab0s
        -0x819s
        -0x7b7s
        -0x7b6s
        -0x7b4s
        -0x286s
        -0x252s
        -0x251s
        -0x227s
        -0x226s
        -0x225s
        -0x224s
        -0x222s
        -0x221s
        -0x220s
        -0x21fs
        -0x21es
        -0x21ds
        -0x21cs
        -0x21bs
        -0x21as
        -0x219s
        -0x218s
        -0x217s
        -0x216s
        -0x215s
        -0x214s
        -0x213s
        -0x212s
        -0x211s
        -0x210s
        -0x20fs
        -0x20es
        -0x20ds
        -0x20cs
        -0x20bs
        -0x20as
        -0x209s
        -0x208s
        -0x207s
        -0x206s
        -0x205s
        -0x204s
        -0x203s
        -0x202s
        -0x201s
        -0x1cas
        -0x5fs
        -0x5es
        -0x5ds
        -0x5cs
        -0x5bs
        -0x5as
        -0x59s
        -0x58s
        -0x57s
        -0x56s
        -0x55s
        -0x54s
        -0x53s
        -0x52s
        -0x51s
        -0x50s
        -0x4fs
        -0x4es
        -0x4ds
        -0x4cs
        -0x4bs
        -0x4as
        -0x49s
        -0x48s
        -0x47s
        -0x46s
        -0x45s
        -0x44s
        -0x43s
        -0x42s
        -0x41s
        -0x40s
        -0x3fs
        -0x3es
        -0x3ds
        -0x3cs
        0x12ds
        0x143s
        0x155s
        0x16ds
        0x1b7s
        0x1dds
        0x1f4s
        0x25as
        0x261s
        0x278s
        0x311s
        0x3ads
        0x3d2s
        0x3dcs
        0x1e30s
        0x1e3es
        0x1e43s
        0x1e54s
        0x1e55s
        0x1e82s
        0x1ef3s
        0x1f72s
        0x201bs
        0x2042s
        0x2135s
        0x2322s
        0x2323s
        0x2628s
        0x262es
        0x2636s
        0x266es
        0x2720s
        0x2721s
        -0x5faes
        -0x5f7cs
        -0x7b7s
        -0x4ffs
        -0x29fs
        -0x29es
        -0x29ds
        -0x298s
        -0x297s
        -0x296s
        -0x295s
        -0x294s
        -0x293s
        -0x292s
        -0x291s
        -0x290s
        -0x28fs
        -0x190s
        -0x18cs
        -0x18bs
        -0x18as
        -0x189s
        -0x188s
        -0x187s
        -0x186s
        -0x185s
        -0x184s
        -0x183s
        -0x182s
        -0x181s
        -0x180s
        -0x17fs
        -0x17es
        -0x17ds
        -0x17cs
        -0x17bs
        -0x17as
        -0x179s
        -0x178s
        -0x177s
        -0x176s
        -0x175s
        -0x173s
        -0x172s
        -0x171s
        -0x170s
        -0x16fs
        -0x16es
        -0x16ds
        -0x16cs
        -0x16bs
        -0x16as
        -0x169s
        -0x168s
        -0x167s
        -0x166s
        -0x165s
        -0x164s
        -0x163s
        -0x158s
        -0x157s
        -0x156s
        -0x155s
        -0x154s
        -0x153s
        -0x152s
        -0x10as
        -0x107s
        -0x106s
        -0x105s
        -0x104s
        -0x103s
        -0x101s
        -0x1f2s
        -0x1f1s
        -0x1f0s
        -0x1efs
        -0x1ees
        -0x1eds
        -0x1ecs
        -0x1ebs
        -0x1eas
        -0x1e9s
        -0x1e8s
        -0x1e7s
        -0x1e6s
        -0x1e5s
        0x272s
        0x303s
        0x30bs
        0x30cs
        0x30ds
        0x30es
        0x30fs
        0x310s
        0x312s
        0x313s
        0x314s
        0x315s
        0x316s
        0x317s
        0x318s
        0x327s
        0x337s
        0x338s
        0x203fs
        0x20a4s
        0x2191s
        0x163s
        -0x56as
        -0x569s
        -0x568s
        -0x567s
        -0x566s
        -0x565s
        -0x564s
        -0x563s
        -0x562s
        -0x561s
        0x275s
        0x2154s
        -0x1e4s
        -0x7ds
        -0x7bs
        -0x7as
        -0x79s
        -0x78s
        -0x77s
        -0x76s
        -0x75s
        -0x74s
        -0x73s
        -0x72s
        -0x71s
        -0x70s
        -0x6es
        -0x6ds
        -0x6cs
        -0x6bs
        0x18fs
        0x190s
        0x191s
        0x1f8s
        0x250s
        0x2b9s
        0x2bas
        0x308s
        0x30as
        0x341s
        0x3f0s
        0x475s
        0x5c3s
        0x1fb6s
        0x20a6s
        0x2502s
        0x2503s
        0x27f5s
        0x27f6s
        0x2919s
        0x291as
        0x46a1s
        -0x61s
        -0x165ds
        -0xb8bs
        0x1c7as
        0x1c7bs
        0x1cb7s
        -0x1f17s
        -0x1f16s
        -0x1f13s
        -0x1f12s
        -0x1f0fs
        -0x1f0es
        -0x1f0ds
        -0x1f0cs
        -0x1f0bs
        -0x1f0as
        0x1e05s
        0x1e41s
        0x1e61s
        0x840s
        0x841s
        0x842s
        0x843s
        0x844s
        0x845s
        0x846s
        0x847s
        0x848s
        0x849s
        0x84as
        0x84bs
        0x84cs
        0x84ds
        0x84es
        -0x2671s
        -0x2670s
        -0x266fs
        -0x266es
        -0x266ds
        -0x266cs
        -0x266bs
        -0x266as
        -0x2669s
        -0x2668s
        -0x2667s
        -0x2666s
        -0x2665s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private removeAllTags(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "meanData"    # Ljava/lang/String;

    .prologue
    .line 144
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    .end local p1    # "meanData":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "meanData":Ljava/lang/String;
    :cond_1
    const-string v0, "%L0[46].*?%l0[46]|%[Ll]0\\d|%\\w\\d{0,1}"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private replaceUnicodeInSymbolTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "meanData"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    .line 114
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 135
    .end local p1    # "meanData":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 118
    .restart local p1    # "meanData":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 119
    .local v0, "bufString":Ljava/lang/StringBuffer;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-le v5, v6, :cond_2

    .line 120
    const-string v5, "%U"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 121
    .local v2, "startIdx":I
    const-string v5, "?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 122
    .local v1, "endIdx":I
    if-ltz v2, :cond_2

    if-ge v1, v6, :cond_3

    .line 135
    .end local v1    # "endIdx":I
    .end local v2    # "startIdx":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 125
    .restart local v1    # "endIdx":I
    .restart local v2    # "startIdx":I
    :cond_3
    const-string v4, ""

    .line 126
    .local v4, "symbolValue":Ljava/lang/String;
    add-int/lit8 v5, v2, 0x2

    if-ge v5, v1, :cond_4

    .line 127
    add-int/lit8 v5, v2, 0x2

    invoke-virtual {v0, v5, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 128
    .local v3, "symbolCodeIndex":I
    if-ltz v3, :cond_4

    sget-object v5, Lcom/diotek/diodict/engine/TagRemover;->UNICODE_SYMBOL_TABLE:[C

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 129
    sget-object v5, Lcom/diotek/diodict/engine/TagRemover;->UNICODE_SYMBOL_TABLE:[C

    aget-char v5, v5, v3

    invoke-static {v5}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    .line 132
    .end local v3    # "symbolCodeIndex":I
    :cond_4
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0, v2, v5, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method


# virtual methods
.method public convertTags(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "meanData"    # Ljava/lang/String;

    .prologue
    .line 156
    move-object v0, p1

    .line 158
    .local v0, "buffer":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/diotek/diodict/engine/TagRemover;->replaceUnicodeInSymbolTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-direct {p0, v0}, Lcom/diotek/diodict/engine/TagRemover;->removeAllTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    return-object v0
.end method
