.class Lcom/diotek/diodict/engine/StringKey;
.super Ljava/lang/Object;
.source "ICUCollator.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/diotek/diodict/engine/StringKey;",
        ">;"
    }
.end annotation


# instance fields
.field private mKey:Ljava/text/CollationKey;

.field private mString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/text/CollationKey;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/text/CollationKey;

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p1, p0, Lcom/diotek/diodict/engine/StringKey;->mString:Ljava/lang/String;

    .line 148
    iput-object p2, p0, Lcom/diotek/diodict/engine/StringKey;->mKey:Ljava/text/CollationKey;

    .line 149
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/diotek/diodict/engine/StringKey;)I
    .locals 2
    .param p1, "obj"    # Lcom/diotek/diodict/engine/StringKey;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/diotek/diodict/engine/StringKey;->mKey:Ljava/text/CollationKey;

    iget-object v1, p1, Lcom/diotek/diodict/engine/StringKey;->mKey:Ljava/text/CollationKey;

    invoke-virtual {v0, v1}, Ljava/text/CollationKey;->compareTo(Ljava/text/CollationKey;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 136
    check-cast p1, Lcom/diotek/diodict/engine/StringKey;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/StringKey;->compareTo(Lcom/diotek/diodict/engine/StringKey;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 163
    instance-of v1, p1, Lcom/diotek/diodict/engine/StringKey;

    if-eqz v1, :cond_0

    .line 164
    check-cast p1, Lcom/diotek/diodict/engine/StringKey;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/StringKey;->compareTo(Lcom/diotek/diodict/engine/StringKey;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 166
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/diotek/diodict/engine/StringKey;->mString:Ljava/lang/String;

    return-object v0
.end method
