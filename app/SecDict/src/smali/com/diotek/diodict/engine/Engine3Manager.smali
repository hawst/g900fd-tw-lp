.class public Lcom/diotek/diodict/engine/Engine3Manager;
.super Ljava/lang/Object;
.source "Engine3Manager.java"

# interfaces
.implements Lcom/diotek/diodict/core/engine/EngineManagerInterface;


# static fields
.field private static final DIODICT3_MEANSIZE:I = 0x1a000

.field private static final DIODICT3_PREVIEW_STR_LENGTH:I = 0x1f4

.field private static final DSP_ALL_MASK:I = 0x7f

.field private static final DSP_KEYFIELD_ALL_MASK:I = 0x70

.field private static final DSP_MEANFIELD_ALL_MASK:I = 0xf

.field private static final NORMALIZING_DASH_SYMBOLS:[Ljava/lang/String;

.field private static final NORMALIZING_EXCEPT_SYMBOLS:[Ljava/lang/String;

.field private static final NORMALIZING_LEFT_BRACKET_SYMBOLS:[Ljava/lang/String;

.field public static final PREVIEW_KEY_KEYWORD:Ljava/lang/String; = "keyword"

.field public static final PREVIEW_KEY_PREVIEW:Ljava/lang/String; = "preview"

.field public static final PREVIEW_KEY_SUB:Ljava/lang/String; = "sub"

.field public static final PREVIEW_KEY_SUB_EQUIV:Ljava/lang/String; = "equiv"


# instance fields
.field private mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

.field private mHyperResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

.field private mLemma:Lcom/diotek/diodict4/adapter/LemmaManager;

.field private mResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

.field private mSymbolManager:Lcom/diotek/diodict/engine/TagRemover;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "("

    aput-object v1, v0, v2

    const-string v1, "["

    aput-object v1, v0, v3

    const-string v1, "\t"

    aput-object v1, v0, v4

    sput-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_LEFT_BRACKET_SYMBOLS:[Ljava/lang/String;

    .line 61
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "\uff0d"

    aput-object v1, v0, v2

    const-string v1, "-"

    aput-object v1, v0, v3

    const-string v1, "\u2500"

    aput-object v1, v0, v4

    const-string v1, "\u2011"

    aput-object v1, v0, v5

    sput-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_DASH_SYMBOLS:[Ljava/lang/String;

    .line 62
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\u02d0"

    aput-object v1, v0, v2

    const-string v1, "\u00b9"

    aput-object v1, v0, v3

    const-string v1, "\u2071"

    aput-object v1, v0, v4

    const-string v1, "\u2474"

    aput-object v1, v0, v5

    const-string v1, "\u2475"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "\u2476"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\u00b9"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\u00b2"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\u00b3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\u2070"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\u2071"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\u2072"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\u2073"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\u2074"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\u2075"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "\u2076"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\u2077"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "\u2078"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\u2079"

    aput-object v2, v0, v1

    sput-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_EXCEPT_SYMBOLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    .line 29
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 30
    new-instance v0, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mHyperResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 56
    new-instance v0, Lcom/diotek/diodict4/adapter/LemmaManager;

    invoke-direct {v0}, Lcom/diotek/diodict4/adapter/LemmaManager;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mLemma:Lcom/diotek/diodict4/adapter/LemmaManager;

    .line 58
    new-instance v0, Lcom/diotek/diodict/engine/TagRemover;

    invoke-direct {v0}, Lcom/diotek/diodict/engine/TagRemover;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mSymbolManager:Lcom/diotek/diodict/engine/TagRemover;

    return-void
.end method

.method private make3rdPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;
    .locals 2
    .param p1, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 197
    if-nez p1, :cond_0

    .line 198
    const-string v0, ""

    .line 201
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getKeyword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getMeanHtml()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/diotek/diodict/engine/Engine3Manager;->make3rdPreview(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private make3rdPreview(Ljava/lang/String;II)Ljava/lang/String;
    .locals 18
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "uniqueid"    # I
    .param p3, "dbtype"    # I

    .prologue
    .line 243
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 244
    .local v12, "mPreview":Lorg/json/JSONObject;
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 246
    .local v13, "mSubPreview":Lorg/json/JSONObject;
    new-instance v17, Lorg/json/JSONArray;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONArray;-><init>()V

    .line 247
    .local v17, "subListArray":Lorg/json/JSONArray;
    new-instance v16, Lorg/json/JSONObject;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONObject;-><init>()V

    .line 249
    .local v16, "subList":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v2, :cond_0

    .line 250
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    const-string v2, ""

    .line 278
    :goto_0
    return-object v2

    .line 255
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    const/4 v6, 0x0

    const v7, 0x1a000

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-virtual/range {v2 .. v10}, Lcom/diotek/diodict/engine/EngineManager3rd;->getMeaning(Ljava/lang/String;IIIIZZI)Ljava/lang/String;

    move-result-object v14

    .line 256
    .local v14, "meanData":Ljava/lang/String;
    if-eqz v14, :cond_3

    .line 258
    const-string v2, "%M"

    invoke-virtual {v14, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v15

    .line 259
    .local v15, "meanPos":I
    if-ltz v15, :cond_1

    .line 260
    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 263
    :cond_1
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1f4

    if-le v2, v3, :cond_2

    .line 264
    const/4 v2, 0x0

    const/16 v3, 0x1f4

    invoke-virtual {v14, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 266
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/diotek/diodict/engine/Engine3Manager;->mSymbolManager:Lcom/diotek/diodict/engine/TagRemover;

    invoke-virtual {v2, v14}, Lcom/diotek/diodict/engine/TagRemover;->convertTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 269
    .end local v15    # "meanPos":I
    :cond_3
    :try_start_0
    const-string v2, "equiv"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 270
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 271
    const-string v2, "sub"

    move-object/from16 v0, v17

    invoke-virtual {v13, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 272
    const-string v2, "keyword"

    move-object/from16 v0, p1

    invoke-virtual {v13, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 273
    const-string v2, "preview"

    invoke-virtual {v12, v2, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_1
    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 274
    :catch_0
    move-exception v11

    .line 275
    .local v11, "e":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private make3rdPreview(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "meanHtml"    # Ljava/lang/String;

    .prologue
    .line 205
    if-nez p1, :cond_0

    .line 206
    const-string v8, ""

    .line 239
    :goto_0
    return-object v8

    .line 209
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 210
    .local v1, "mPreview":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 212
    .local v2, "mSubPreview":Lorg/json/JSONObject;
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 213
    .local v7, "subListArray":Lorg/json/JSONArray;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 219
    .local v6, "subList":Lorg/json/JSONObject;
    const-string v3, ""

    .line 220
    .local v3, "meanData":Ljava/lang/String;
    move-object v5, p2

    .line 221
    .local v5, "sBuffer":Ljava/lang/String;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 222
    const-string v8, "<div class=\'meanfield\' id=\'meanfield\'>"

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 223
    .local v4, "nPos":I
    if-lez v4, :cond_1

    .line 224
    invoke-virtual {v5, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 226
    :cond_1
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 230
    .end local v4    # "nPos":I
    :cond_2
    :try_start_0
    const-string v8, "equiv"

    invoke-virtual {v6, v8, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 232
    const-string v8, "sub"

    invoke-virtual {v2, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 233
    const-string v8, "keyword"

    invoke-virtual {v2, v8, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 234
    const-string v8, "preview"

    invoke-virtual {v1, v8, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private searchWord(Ljava/lang/String;IIZ)I
    .locals 7
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "num"    # I
    .param p3, "searchlistType"    # I
    .param p4, "isNext"    # Z

    .prologue
    const/4 v5, 0x0

    .line 512
    invoke-static {p1}, Lcom/diotek/diodict/engine/DictUtils;->isWildcardSearch(Ljava/lang/String;)Z

    move-result v0

    .line 513
    .local v0, "bWildcard":Z
    if-eqz p4, :cond_3

    .line 514
    iget-object v6, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v6, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v3

    .line 515
    .local v3, "wordList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v6

    if-nez v6, :cond_1

    .line 532
    .end local v3    # "wordList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    :cond_0
    :goto_0
    return v5

    .line 518
    .restart local v3    # "wordList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    :cond_1
    invoke-virtual {v3}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getWordList(I)Lcom/diotek/diodict/engine/WordList3rd;

    move-result-object v4

    .line 519
    .local v4, "wordListItem":Lcom/diotek/diodict/engine/WordList3rd;
    if-eqz v4, :cond_0

    .line 522
    invoke-virtual {v4}, Lcom/diotek/diodict/engine/WordList3rd;->getKeyword()Ljava/lang/String;

    move-result-object v2

    .line 523
    .local v2, "keyword":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/diotek/diodict/engine/WordList3rd;->getSUID()I

    move-result v1

    .line 524
    .local v1, "entryId":I
    if-eqz v2, :cond_2

    iget-object v5, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v5, v2, v1, p3, v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->searchWordByWL(Ljava/lang/String;IIZ)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 525
    invoke-virtual {p0, p3, p4}, Lcom/diotek/diodict/engine/Engine3Manager;->setSearchList(IZ)V

    .line 532
    .end local v1    # "entryId":I
    .end local v2    # "keyword":Ljava/lang/String;
    .end local v3    # "wordList":Lcom/diotek/diodict/engine/ResultWordList3rd;
    .end local v4    # "wordListItem":Lcom/diotek/diodict/engine/WordList3rd;
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v5, p3}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v5

    invoke-virtual {v5}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v5

    goto :goto_0

    .line 528
    :cond_3
    iget-object v5, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v5, p1, p3, v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->searchWord(Ljava/lang/String;IZ)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 529
    invoke-virtual {p0, p3, p4}, Lcom/diotek/diodict/engine/Engine3Manager;->setSearchList(IZ)V

    goto :goto_1
.end method


# virtual methods
.method public closeDatabase()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->terminateEngine()V

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    .line 358
    :cond_0
    return-void
.end method

.method public closeSpellcheck()I
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    return v0
.end method

.method public extendSearchWord(ILjava/lang/String;IZ)I
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 425
    if-eqz p2, :cond_0

    if-ltz p3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 426
    :cond_0
    const/4 v0, -0x1

    .line 435
    :goto_0
    return v0

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_2

    .line 430
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    const/16 v0, -0x64

    goto :goto_0

    .line 435
    :cond_2
    const/4 v4, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict/engine/Engine3Manager;->searchWord(ILjava/lang/String;IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "posp"    # I
    .param p3, "infl"    # Ljava/lang/String;
    .param p4, "ending"    # Ljava/lang/String;
    .param p5, "stem"    # Ljava/lang/String;

    .prologue
    .line 538
    const-string v0, ""

    return-object v0
.end method

.method public getContentsType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 559
    const/4 v0, -0x1

    return v0
.end method

.method public getContentsVendor(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 293
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDBInfo(I)Lcom/diotek/diodict/core/engine/DBInfo;

    move-result-object v0

    .line 295
    .local v0, "dbInfo":Lcom/diotek/diodict/core/engine/DBInfo;
    if-nez v0, :cond_0

    .line 296
    const/4 v1, -0x1

    .line 299
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/DBInfo;->getContentsVendor()I

    move-result v1

    goto :goto_0
.end method

.method public getCurrentDbType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_0

    .line 148
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    const/16 v0, -0x3e8

    .line 153
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v0}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentEngineVersion(I)I
    .locals 1
    .param p1, "dictype"    # I

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/diotek/diodict/engine/Engine3Manager;->getVersion()I

    move-result v0

    return v0
.end method

.method public getDataBaseType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 564
    const/4 v0, -0x1

    return v0
.end method

.method public getDatabaseVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 569
    const-string v0, ""

    return-object v0
.end method

.method public getEngineVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 550
    const-string v0, "3"

    .line 552
    .local v0, "engineVersion":Ljava/lang/String;
    return-object v0
.end method

.method public getEngineVersionByNumeric(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 574
    const/4 v0, -0x1

    return v0
.end method

.method public getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    const/4 v2, 0x0

    .line 471
    if-gez p2, :cond_0

    .line 472
    const/4 v1, 0x0

    .line 478
    :goto_0
    return-object v1

    .line 475
    :cond_0
    const/4 v1, 0x3

    new-array v0, v1, [I

    aput p2, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 478
    .local v0, "entryNum":[I
    new-instance v1, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v1, v0}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    goto :goto_0
.end method

.method public getEntryLanguage(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 318
    const/4 v0, -0x1

    return v0
.end method

.method public getError(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 579
    const/4 v0, -0x1

    return v0
.end method

.method public getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 584
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchword"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 288
    const-string v0, ""

    return-object v0
.end method

.method public getHangulroError(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 594
    const/4 v0, -0x1

    return v0
.end method

.method public getHangulroList(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 599
    const/4 v0, -0x1

    return v0
.end method

.method public getHeadType(II)I
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    const/4 v2, 0x0

    .line 450
    if-gez p2, :cond_0

    .line 451
    const/4 v1, -0x1

    .line 458
    :goto_0
    return v1

    .line 454
    :cond_0
    const/4 v1, 0x3

    new-array v0, v1, [I

    aput p2, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 458
    .local v0, "entryNum":[I
    new-instance v1, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v1, v0}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    invoke-virtual {v1}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v1

    goto :goto_0
.end method

.method public getHeadwordId(II)I
    .locals 0
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 463
    if-gez p2, :cond_0

    .line 464
    const/4 p2, -0x1

    .line 466
    .end local p2    # "uniqueId":I
    :cond_0
    return p2
.end method

.method public getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 604
    const-string v0, ""

    return-object v0
.end method

.method public getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 89
    if-eqz p2, :cond_0

    if-gez p3, :cond_1

    .line 90
    :cond_0
    const-string v8, ""

    .line 108
    :goto_0
    return-object v8

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_2

    .line 94
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    const-string v8, ""

    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    .line 100
    const/4 v7, 0x0

    .line 102
    .local v7, "htmlMeaning":[B
    :try_start_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    const/16 v2, 0x7f

    const-string v1, "UTF-16LE"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const v5, 0x1a000

    move v1, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict/engine/EngineManager3rd;->getMeaningOutByHTML(II[BII)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 107
    :goto_1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v7, v0, v1}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v8

    .line 108
    .local v8, "meaning":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v8    # "meaning":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 105
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public getNormalizedText(ILjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "dbType"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 609
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 610
    :cond_0
    const-string v1, ""

    .line 637
    :cond_1
    :goto_0
    return-object v1

    .line 612
    :cond_2
    move-object v1, p2

    .line 614
    .local v1, "buf":Ljava/lang/String;
    sget-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_LEFT_BRACKET_SYMBOLS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v4, v0, v3

    .line 615
    .local v4, "leftBracketCode":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 616
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 614
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 620
    .end local v4    # "leftBracketCode":Ljava/lang/String;
    :cond_4
    sget-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_EXCEPT_SYMBOLS:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_6

    aget-object v2, v0, v3

    .line 621
    .local v2, "exceptCode":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 622
    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 620
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 626
    .end local v2    # "exceptCode":Ljava/lang/String;
    :cond_6
    sget-object v0, Lcom/diotek/diodict/engine/Engine3Manager;->NORMALIZING_DASH_SYMBOLS:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_9

    aget-object v2, v0, v3

    .line 627
    .restart local v2    # "exceptCode":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_7

    invoke-virtual {v1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 628
    const/4 v6, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 630
    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 631
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 626
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 634
    .end local v2    # "exceptCode":Ljava/lang/String;
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 635
    const-string v1, ""

    goto/16 :goto_0
.end method

.method public getPreview(ILjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 309
    if-eqz p2, :cond_0

    if-gez p3, :cond_1

    .line 310
    :cond_0
    const-string v0, ""

    .line 313
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p2, p3, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->make3rdPreview(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 114
    const-string v0, ""

    .line 124
    :goto_0
    return-object v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_1

    .line 118
    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    const-string v0, ""

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict/engine/Engine3Manager;->setCurrentDbType(I)I

    .line 124
    invoke-direct {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->make3rdPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPronounce(Ljava/lang/String;II)Ljava/lang/String;
    .locals 7
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "uniqueid"    # I
    .param p3, "dbType"    # I

    .prologue
    .line 652
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_1

    .line 653
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 654
    const-string v6, ""

    .line 666
    :cond_0
    :goto_0
    return-object v6

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    const/16 v4, 0xc8

    const/4 v5, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict/engine/EngineManager3rd;->getMeaningData(Ljava/lang/String;IIIZ)Ljava/lang/String;

    move-result-object v6

    .line 659
    .local v6, "meanData":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 660
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mSymbolManager:Lcom/diotek/diodict/engine/TagRemover;

    invoke-virtual {v0, v6}, Lcom/diotek/diodict/engine/TagRemover;->convertTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 661
    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    const-string v6, ""

    goto :goto_0
.end method

.method public getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;
    .locals 1
    .param p1, "searchlistType"    # I

    .prologue
    .line 412
    packed-switch p1, :pswitch_data_0

    .line 416
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    :goto_0
    return-object v0

    .line 414
    :pswitch_0
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mHyperResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getSpellcheckResultSize()I
    .locals 1

    .prologue
    .line 343
    const/4 v0, -0x1

    return v0
.end method

.method public getSpellcheckWord(I)Ljava/lang/String;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 348
    iget-object v1, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v0

    .line 349
    .local v0, "dbType":I
    invoke-static {p1, v0}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetWordList(II)[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getTranslationLanguages(I)[I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 642
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 440
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadSn()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getHeadType()I

    move-result v0

    if-gez v0, :cond_1

    .line 442
    :cond_0
    const/4 v0, -0x1

    .line 445
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x3

    return v0
.end method

.method public getWordmap(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "suid"    # I

    .prologue
    .line 544
    const/4 v0, -0x1

    return v0
.end method

.method public initEngine(I)I
    .locals 4
    .param p1, "dbType"    # I

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "err":I
    invoke-static {}, Lcom/diotek/diodict/engine/EngineManager3rd;->getInstance()Lcom/diotek/diodict/engine/EngineManager3rd;

    move-result-object v3

    iput-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    .line 77
    iget-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v3}, Lcom/diotek/diodict/engine/EngineManager3rd;->initNativeEngine()Z

    move-result v2

    .line 79
    .local v2, "isInitEngine":Z
    if-nez v2, :cond_0

    .line 80
    const/4 v0, 0x2

    move v1, v0

    .line 84
    .end local v0    # "err":I
    .local v1, "err":I
    :goto_0
    return v1

    .line 83
    .end local v1    # "err":I
    .restart local v0    # "err":I
    :cond_0
    iget-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v3, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    move v1, v0

    .line 84
    .end local v0    # "err":I
    .restart local v1    # "err":I
    goto :goto_0
.end method

.method public initSpellcheck(Ljava/lang/String;I)I
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 323
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v2}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v0

    .line 324
    .local v0, "curDicType":I
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v0}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibSpellCheckInit([BI)I

    move-result v1

    .line 325
    .local v1, "err":I
    return v1
.end method

.method public isValidInstance(I)Z
    .locals 3
    .param p1, "lastDbType"    # I

    .prologue
    const/4 v1, 0x0

    .line 183
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v2, :cond_1

    .line 184
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 189
    :cond_1
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->setCurrentDbType(I)I

    move-result v0

    .line 190
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 191
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public searchHangulro(ILjava/lang/String;)Z
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    .line 647
    const/4 v0, 0x0

    return v0
.end method

.method public searchSpellcheck(Ljava/lang/String;I)I
    .locals 4
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 330
    const/4 v2, 0x0

    .line 331
    .local v2, "tmpbyte":[B
    const/4 v1, -0x1

    .line 333
    .local v1, "err":I
    :try_start_0
    const-string v3, "ISO-8859-1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 334
    invoke-static {v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibSpellCheckSearch([B)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 338
    :goto_0
    return v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public searchWord(ILjava/lang/String;IIZ)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    .line 131
    if-eqz p2, :cond_0

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    const/4 v0, 0x2

    if-le p4, v0, :cond_1

    .line 133
    :cond_0
    const/4 v0, -0x1

    .line 142
    :goto_0
    return v0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v0, :cond_2

    .line 137
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    const/16 v0, -0x64

    goto :goto_0

    .line 142
    :cond_2
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/diotek/diodict/engine/Engine3Manager;->searchWord(Ljava/lang/String;IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public setCurrentDbType(I)I
    .locals 4
    .param p1, "dbType"    # I

    .prologue
    .line 158
    const/4 v1, 0x0

    .line 160
    .local v1, "err":I
    iget-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v3, :cond_0

    .line 161
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v1

    .line 162
    if-eqz v1, :cond_0

    move v2, v1

    .line 173
    .end local v1    # "err":I
    .local v2, "err":I
    :goto_0
    return v2

    .line 167
    .end local v2    # "err":I
    .restart local v1    # "err":I
    :cond_0
    iget-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v3}, Lcom/diotek/diodict/engine/EngineManager3rd;->getCurDict()I

    move-result v0

    .line 169
    .local v0, "curDicType":I
    if-eq v0, p1, :cond_1

    .line 170
    iget-object v3, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v3, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    :cond_1
    move v2, v1

    .line 173
    .end local v1    # "err":I
    .restart local v2    # "err":I
    goto :goto_0
.end method

.method public setCurrentDbType(ILjava/lang/String;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 178
    const/4 v0, -0x1

    return v0
.end method

.method public setSearchList(IZ)V
    .locals 13
    .param p1, "searchlistType"    # I
    .param p2, "isNext"    # Z

    .prologue
    const/4 v11, 0x0

    .line 372
    iget-object v10, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v10, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v8

    .line 373
    .local v8, "resultList3rd":Lcom/diotek/diodict/engine/ResultWordList3rd;
    new-instance v7, Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-direct {v7}, Lcom/diotek/diodict/core/engine/ResultWordList;-><init>()V

    .line 374
    .local v7, "result":Lcom/diotek/diodict/core/engine/ResultWordList;
    const/4 v1, -0x1

    .line 376
    .local v1, "dicType":I
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v10

    if-lez v10, :cond_0

    .line 377
    invoke-virtual {v8, v11}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getDicTypeByPos(I)I

    move-result v1

    .line 380
    :cond_0
    invoke-virtual {v8}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getKeywordPos()I

    move-result v12

    if-eqz p2, :cond_2

    const/4 v10, 0x1

    :goto_0
    add-int v5, v12, v10

    .line 382
    .local v5, "keywordPosition":I
    move v3, v5

    .local v3, "idx":I
    :goto_1
    invoke-virtual {v8}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v10

    if-ge v3, v10, :cond_3

    .line 383
    invoke-virtual {v8, v3}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getWordList(I)Lcom/diotek/diodict/engine/WordList3rd;

    move-result-object v9

    .line 384
    .local v9, "wordInfo":Lcom/diotek/diodict/engine/WordList3rd;
    if-eqz v9, :cond_1

    .line 385
    const/4 v10, 0x3

    new-array v2, v10, [I

    fill-array-data v2, :array_0

    .line 388
    .local v2, "entryId":[I
    new-array v0, v11, [I

    .line 389
    .local v0, "coordList":[I
    invoke-virtual {v9}, Lcom/diotek/diodict/engine/WordList3rd;->getSUID()I

    move-result v10

    aput v10, v2, v11

    .line 391
    invoke-virtual {v9}, Lcom/diotek/diodict/engine/WordList3rd;->getKeyword()Ljava/lang/String;

    move-result-object v4

    .line 392
    .local v4, "keyword":Ljava/lang/String;
    aget v10, v2, v11

    invoke-direct {p0, v4, v10, v1}, Lcom/diotek/diodict/engine/Engine3Manager;->make3rdPreview(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    .line 393
    .local v6, "previewString":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/diotek/diodict/engine/WordList3rd;->getSUID()I

    move-result v10

    invoke-virtual {v7, v10, v2, v0, v6}, Lcom/diotek/diodict/core/engine/ResultWordList;->addWordItem(I[I[ILjava/lang/String;)V

    .line 382
    .end local v0    # "coordList":[I
    .end local v2    # "entryId":[I
    .end local v4    # "keyword":Ljava/lang/String;
    .end local v6    # "previewString":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v3    # "idx":I
    .end local v5    # "keywordPosition":I
    .end local v9    # "wordInfo":Lcom/diotek/diodict/engine/WordList3rd;
    :cond_2
    move v10, v11

    .line 380
    goto :goto_0

    .line 396
    .restart local v3    # "idx":I
    .restart local v5    # "keywordPosition":I
    :cond_3
    invoke-virtual {v7, v1}, Lcom/diotek/diodict/core/engine/ResultWordList;->setDbType(I)V

    .line 398
    packed-switch p1, :pswitch_data_0

    .line 404
    iget-object v10, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-virtual {v10}, Lcom/diotek/diodict/core/engine/ResultWordList;->clearWordList()V

    .line 405
    iput-object v7, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    .line 408
    :goto_2
    return-void

    .line 400
    :pswitch_0
    iget-object v10, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mHyperResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    invoke-virtual {v10}, Lcom/diotek/diodict/core/engine/ResultWordList;->clearWordList()V

    .line 401
    iput-object v7, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mHyperResultList:Lcom/diotek/diodict/core/engine/ResultWordList;

    goto :goto_2

    .line 385
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 398
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public totalSearchWord(ILjava/lang/String;IIZ)I
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    const/4 v1, 0x0

    .line 485
    if-eqz p2, :cond_0

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    const/4 v2, 0x2

    if-le p4, v2, :cond_2

    .line 487
    :cond_0
    const/4 v1, -0x1

    .line 508
    :cond_1
    :goto_0
    return v1

    .line 490
    :cond_2
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    if-nez v2, :cond_3

    .line 491
    invoke-virtual {p0, p1}, Lcom/diotek/diodict/engine/Engine3Manager;->initEngine(I)I

    move-result v2

    if-nez v2, :cond_1

    .line 496
    :cond_3
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v2, p1}, Lcom/diotek/diodict/engine/EngineManager3rd;->setCurDict(I)V

    .line 497
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v2, p2, p4}, Lcom/diotek/diodict/engine/EngineManager3rd;->totalSearch(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 500
    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mLemma:Lcom/diotek/diodict4/adapter/LemmaManager;

    invoke-virtual {v2, p1, p2}, Lcom/diotek/diodict4/adapter/LemmaManager;->getOriginWord(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "originWord":Ljava/lang/String;
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v2, v0, p4}, Lcom/diotek/diodict/engine/EngineManager3rd;->totalSearch(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 507
    .end local v0    # "originWord":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, p4, p5}, Lcom/diotek/diodict/engine/Engine3Manager;->setSearchList(IZ)V

    .line 508
    iget-object v1, p0, Lcom/diotek/diodict/engine/Engine3Manager;->mEngine3:Lcom/diotek/diodict/engine/EngineManager3rd;

    invoke-virtual {v1, p4}, Lcom/diotek/diodict/engine/EngineManager3rd;->getResultList(I)Lcom/diotek/diodict/engine/ResultWordList3rd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/diotek/diodict/engine/ResultWordList3rd;->getSize()I

    move-result v1

    goto :goto_0
.end method
