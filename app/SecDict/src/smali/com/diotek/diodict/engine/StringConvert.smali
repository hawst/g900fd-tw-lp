.class public Lcom/diotek/diodict/engine/StringConvert;
.super Ljava/lang/Object;
.source "StringConvert.java"


# static fields
.field public static final CHARACTERSET_KSC5601:Ljava/lang/String; = "KSC5601"

.field public static final CHARACTERSET_UTF16LE:Ljava/lang/String; = "UTF-16LE"

.field public static final CHARACTERSET_UTF8:Ljava/lang/String; = "UTF-8"

.field public static final ENCODE_BIG5:I = 0x6

.field public static final ENCODE_CYRILLIC:I = 0x3

.field public static final ENCODE_GB2312:I = 0x5

.field public static final ENCODE_KSC5601:I = 0x4

.field public static final ENCODE_LATIN_I:I = 0x2

.field public static final ENCODE_SHIFT_JIS:I = 0x7

.field public static final ENCODE_UCS_2LE:I = 0x0

.field public static final ENCODE_UTF8:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertByteToString([BIZ)Ljava/lang/String;
    .locals 2
    .param p0, "wordbyte"    # [B
    .param p1, "encode"    # I
    .param p2, "convertSymbol"    # Z

    .prologue
    .line 43
    if-nez p0, :cond_0

    .line 44
    const/4 v1, 0x0

    .line 66
    :goto_0
    return-object v1

    .line 46
    :cond_0
    const/4 v1, 0x0

    .line 47
    .local v1, "convered":Ljava/lang/String;
    const/4 v0, 0x0

    .line 49
    .local v0, "charset":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 60
    :pswitch_0
    const-string v0, "UTF-16LE"

    .line 64
    :goto_1
    invoke-static {p0, v0, p2}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 66
    goto :goto_0

    .line 51
    :pswitch_1
    const-string v0, "UTF-8"

    .line 52
    goto :goto_1

    .line 54
    :pswitch_2
    const-string v0, "UTF-16LE"

    .line 55
    goto :goto_1

    .line 57
    :pswitch_3
    const-string v0, "KSC5601"

    .line 58
    goto :goto_1

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static convertByteToString([BLjava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p0, "wordbyte"    # [B
    .param p1, "charset"    # Ljava/lang/String;
    .param p2, "convertSymbol"    # Z

    .prologue
    .line 77
    if-nez p0, :cond_1

    .line 78
    const/4 v0, 0x0

    .line 102
    :cond_0
    :goto_0
    return-object v0

    .line 80
    :cond_1
    const/4 v0, 0x0

    .line 82
    .local v0, "convered":Ljava/lang/String;
    const-string v5, "UTF-16LE"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 84
    :try_start_0
    invoke-static {p0}, Lcom/diotek/diodict/engine/StringConvert;->getWordByteLength([B)I

    move-result v4

    .line 85
    .local v4, "size":I
    invoke-static {v4, p0}, Lcom/diotek/diodict/engine/StringConvert;->getRealWordByte(I[B)[B

    move-result-object v3

    .line 86
    .local v3, "realByte":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "convered":Ljava/lang/String;
    .local v1, "convered":Ljava/lang/String;
    move-object v0, v1

    .line 99
    .end local v1    # "convered":Ljava/lang/String;
    .end local v3    # "realByte":[B
    .end local v4    # "size":I
    .restart local v0    # "convered":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz p2, :cond_0

    .line 100
    invoke-static {v0}, Lcom/diotek/diodict/engine/StringConvert;->getSuffixString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v2

    .line 89
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 91
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_3
    const-string v5, "KSC5601"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 93
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v0    # "convered":Ljava/lang/String;
    .restart local v1    # "convered":Ljava/lang/String;
    move-object v0, v1

    .line 96
    .end local v1    # "convered":Ljava/lang/String;
    .restart local v0    # "convered":Ljava/lang/String;
    goto :goto_1

    .line 94
    :catch_1
    move-exception v2

    .line 95
    .restart local v2    # "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method private static getRealWordByte(I[B)[B
    .locals 3
    .param p0, "size"    # I
    .param p1, "wordbyte"    # [B

    .prologue
    .line 107
    new-array v0, p0, [B

    .line 109
    .local v0, "convered":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p0, :cond_0

    .line 110
    array-length v2, p1

    if-le v2, v1, :cond_0

    .line 111
    aget-byte v2, p1, v1

    aput-byte v2, v0, v1

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_0
    return-object v0
.end method

.method private static getSuffixString(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 137
    const/4 v7, 0x0

    .line 138
    .local v7, "suffixword":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-object v9

    .line 141
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 142
    .local v5, "size":I
    const/4 v3, 0x0

    .line 144
    .local v3, "isFound":Z
    const-string v10, "%U"

    invoke-virtual {p0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 145
    const/4 v10, 0x4

    if-lt v5, v10, :cond_0

    .line 149
    add-int/lit8 v9, v5, -0x3

    new-array v8, v9, [C

    .line 151
    .local v8, "wordcharbuf":[C
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v2, v5, :cond_5

    .line 152
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x25

    if-ne v9, v10, :cond_3

    add-int/lit8 v9, v2, 0x1

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x55

    if-ne v9, v10, :cond_3

    .line 153
    const/4 v3, 0x1

    .line 154
    const-string v9, "%U"

    invoke-virtual {p0, v9, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    add-int/lit8 v6, v9, 0x2

    .line 155
    .local v6, "start":I
    const-string v9, "?"

    invoke-virtual {p0, v9, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 156
    .local v1, "end":I
    invoke-static {p0, v6, v1}, Lcom/diotek/diodict/engine/StringConvert;->getSymboleCode(Ljava/lang/String;II)S

    move-result v0

    .line 157
    .local v0, "code":S
    int-to-char v9, v0

    aput-char v9, v8, v4

    .line 158
    add-int/lit8 v4, v4, 0x1

    .line 159
    sub-int v9, v1, v6

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v2, v9

    .line 151
    .end local v0    # "code":S
    .end local v1    # "end":I
    .end local v6    # "start":I
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 161
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3f

    if-eq v9, v10, :cond_2

    .line 164
    :cond_4
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    aput-char v9, v8, v4

    .line 165
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 168
    :cond_5
    new-instance v7, Ljava/lang/String;

    .end local v7    # "suffixword":Ljava/lang/String;
    add-int/lit8 v9, v5, -0x3

    invoke-direct {v7, v8, v11, v9}, Ljava/lang/String;-><init>([CII)V

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v8    # "wordcharbuf":[C
    .restart local v7    # "suffixword":Ljava/lang/String;
    :goto_3
    move-object v9, v7

    .line 174
    goto :goto_0

    .line 170
    :cond_6
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_3
.end method

.method private static getSymboleCode(Ljava/lang/String;II)S
    .locals 4
    .param p0, "word"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 177
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "symbol":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-short v2, v3

    .line 180
    .local v2, "temp":S
    invoke-static {v2}, Lcom/diotek/diodict/engine/EngineNative3rd;->LibGetUnicodeFromDioSymbol(I)I

    move-result v3

    int-to-short v0, v3

    .line 181
    .local v0, "code":S
    return v0
.end method

.method private static getWordByteLength([B)I
    .locals 4
    .param p0, "wordbyte"    # [B

    .prologue
    .line 120
    array-length v2, p0

    .line 121
    .local v2, "total":I
    const/4 v1, 0x0

    .line 122
    .local v1, "size":I
    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 124
    aget-byte v3, p0, v0

    if-nez v3, :cond_1

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p0, v3

    if-nez v3, :cond_1

    .line 133
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 127
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x2

    .line 123
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 131
    .end local v0    # "i":I
    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public static makeSearchWord(Ljava/lang/String;I)[B
    .locals 3
    .param p0, "word"    # Ljava/lang/String;
    .param p1, "encode"    # I

    .prologue
    .line 191
    if-nez p0, :cond_0

    .line 192
    const/4 v0, 0x0

    .line 213
    :goto_0
    return-object v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    .line 195
    .local v0, "converted":[B
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 198
    :sswitch_0
    :try_start_0
    const-string v2, "UTF-16LE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :sswitch_1
    :try_start_1
    const-string v2, "KSC5601"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 206
    :catch_1
    move-exception v1

    .line 207
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 195
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
    .end sparse-switch
.end method
