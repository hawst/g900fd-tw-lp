.class public Lcom/diotek/diodict/engine/EngineNative3rd;
.super Ljava/lang/Object;
.source "EngineNative3rd.java"


# static fields
.field public static final ERR_NONE:I = 0x0

.field public static final ERR_NOT_FOUND_SO:I = -0x1

.field private static mLastErr:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 18
    sput v1, Lcom/diotek/diodict/engine/EngineNative3rd;->mLastErr:I

    .line 22
    :try_start_0
    const-string v1, "JNI"

    const-string v2, "Trying to load DioDict3EngineNativeFrame"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    const-string v1, "DioDict3EngineNativeFrame"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 25
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/diotek/diodict/engine/EngineNative3rd;->setLastErr(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 26
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 27
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "JNI"

    const-string v2, "Warning: Could not load DioDict3EngineNativeFrame"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    const/4 v1, -0x1

    invoke-static {v1}, Lcom/diotek/diodict/engine/EngineNative3rd;->setLastErr(I)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native LibChangeBuf(Z)V
.end method

.method public static native LibCompareSUID(III)Z
.end method

.method public static native LibCompareWord(I[BI[BI)Z
.end method

.method public static native LibDBMove([B[B)I
.end method

.method public static native LibDraCallBackTest()I
.end method

.method public static native LibDrawMeanView(ZZ)V
.end method

.method public static native LibGetCurDB()I
.end method

.method public static native LibGetCurMeanSuid()I
.end method

.method public static native LibGetDBVersion(I)[B
.end method

.method public static native LibGetFirstWordList(I)[B
.end method

.method public static native LibGetGurMeanWord()[B
.end method

.method public static native LibGetImportantMeaning(II[BIIZZ)[B
.end method

.method public static native LibGetLastWordList(I)[B
.end method

.method public static native LibGetMeaning(II[BIIZZ)[B
.end method

.method public static native LibGetMeaningData(I[BIIZ)[B
.end method

.method public static native LibGetMeaningFromOCR([BIZ)[B
.end method

.method public static native LibGetMeaningOutByHTML(II[BII)[B
.end method

.method public static native LibGetMeaningOutByHTMLWithToken(II[BII)[B
.end method

.method public static native LibGetMeaningStr([BII)[B
.end method

.method public static native LibGetModelHashKey()[B
.end method

.method public static native LibGetPTNativePronouce(II)I
.end method

.method public static native LibGetResult()Lcom/diotek/diodict/engine/ResultWordList3rd;
.end method

.method public static native LibGetResultExactMatch()Z
.end method

.method public static native LibGetResultKeyPos()I
.end method

.method public static native LibGetResultSize()I
.end method

.method public static native LibGetSourceLanguage(I)I
.end method

.method public static native LibGetSpellCheckLangType(I)I
.end method

.method public static native LibGetSpellCheckResultNum()I
.end method

.method public static native LibGetSpellCheckWordByIndex(I)[B
.end method

.method public static native LibGetTalkerNotation(II)[B
.end method

.method public static native LibGetTalkerSUID(II)J
.end method

.method public static native LibGetTalkerSentence(II)[B
.end method

.method public static native LibGetTargetLanguage(I)I
.end method

.method public static native LibGetUnicodeFromDioSymbol(I)I
.end method

.method public static native LibGetWordList(II)[B
.end method

.method public static native LibGetWordSuid(I)I
.end method

.method public static native LibHangulroSearch([IZI)I
.end method

.method public static native LibIdiomExampleSearch([B)I
.end method

.method public static native LibIdiomExampleSearchByWL([BI)I
.end method

.method public static native LibInit([BZZZ)I
.end method

.method public static native LibInitialSearch([B)I
.end method

.method public static native LibInitialSearchByWL([BI)I
.end method

.method public static native LibIntegrationDBArray()[I
.end method

.method public static native LibIsValidDBHandle()Z
.end method

.method public static native LibOpenDB([BZ)I
.end method

.method public static native LibPageGetCurLine()I
.end method

.method public static native LibPageGetKeyFieldLine()I
.end method

.method public static native LibPageGetLinePerPage()I
.end method

.method public static native LibPageGetMeanFieldLine(Z)I
.end method

.method public static native LibPageGetSelectedLine()J
.end method

.method public static native LibPageGetSelectedText(Z)[B
.end method

.method public static native LibPageGetTotalLine(Z)I
.end method

.method public static native LibPageHasSelectedText()Z
.end method

.method public static native LibPageHyperAt(II)Z
.end method

.method public static native LibPageHyperDown()Z
.end method

.method public static native LibPageHyperLeft()Z
.end method

.method public static native LibPageHyperRight()Z
.end method

.method public static native LibPageHyperUp()Z
.end method

.method public static native LibPageIsEnabledHyper()Z
.end method

.method public static native LibPageScroll(I)Z
.end method

.method public static native LibPageSetDisplayMode(I)V
.end method

.method public static native LibPageSetDisplayModeByCode(I)V
.end method

.method public static native LibPageSetEnableHyper(ZZ)V
.end method

.method public static native LibPageSetHyperType(ZZ)V
.end method

.method public static native LibPageTouchDown(II)Z
.end method

.method public static native LibPageTouchMove(II)V
.end method

.method public static native LibPageTouchUp(II)Z
.end method

.method public static native LibReSearchMean()I
.end method

.method public static native LibReleaseCBInterface()I
.end method

.method public static native LibSearchByWL([BIZ)I
.end method

.method public static native LibSearchMean([BI)I
.end method

.method public static native LibSearchMeanByPos(I)I
.end method

.method public static native LibSearchMeanBySUID(I[BI)I
.end method

.method public static native LibSearchWord([BZ)I
.end method

.method public static native LibSetDicType(I)V
.end method

.method public static native LibSetHighlightPenColor([I)V
.end method

.method public static native LibSetLineGap(I)V
.end method

.method public static native LibSetMargin(IIII)V
.end method

.method public static native LibSetMyDic([BI)V
.end method

.method public static native LibSetResultSize(I)V
.end method

.method public static native LibSetScreen(IIIIZ)V
.end method

.method public static native LibSetSepGap(I)V
.end method

.method public static native LibSetTalkerCurLang(I)V
.end method

.method public static native LibSetTalkerTransLang(I)V
.end method

.method public static native LibSetTheme([I[I)V
.end method

.method public static native LibSpellCheckInit([BI)I
.end method

.method public static native LibSpellCheckSearch([B)I
.end method

.method public static native LibSpellCheckTerm()I
.end method

.method public static native LibTalkerInit([B)I
.end method

.method public static native LibTalkerSearchResult(II)I
.end method

.method public static native LibTalkerSetDBPath([B[B[B[B[B[B[B)V
.end method

.method public static native LibTalkerTerm()I
.end method

.method public static native LibTerminate()I
.end method

.method public static native LibUnifiySearch([BIII)I
.end method

.method public static native LibWildCardSearch([B)I
.end method

.method public static native LibWildCardSearchByWL([BI)I
.end method

.method public static native LibisUnicodeDB(I)Z
.end method

.method public static getLastErr()I
    .locals 1

    .prologue
    .line 182
    sget v0, Lcom/diotek/diodict/engine/EngineNative3rd;->mLastErr:I

    return v0
.end method

.method public static setLastErr(I)V
    .locals 0
    .param p0, "m_lastErr"    # I

    .prologue
    .line 179
    sput p0, Lcom/diotek/diodict/engine/EngineNative3rd;->mLastErr:I

    .line 180
    return-void
.end method
