.class public Lcom/diotek/diodict/engine/DBType$enlanguage;
.super Ljava/lang/Object;
.source "DBType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/engine/DBType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "enlanguage"
.end annotation


# static fields
.field public static final DEL_ARABIC:I = 0x19

.field public static final DEL_BENGALI:I = 0x20

.field public static final DEL_CHINESE:I = 0x4

.field public static final DEL_CHINESE_PINYIN:I = 0x8

.field public static final DEL_CHINESE_PINYIN_INITIAL:I = 0x9

.field public static final DEL_CHINESE_SIMP:I = 0x5

.field public static final DEL_CHINESE_SIMPTRAD:I = 0x7

.field public static final DEL_CHINESE_TRAD:I = 0x6

.field public static final DEL_CHINESE_ZHUYIN:I = 0x2c

.field public static final DEL_DANISH:I = 0x14

.field public static final DEL_DUTCH:I = 0x15

.field public static final DEL_EASTERN:I = 0x28

.field public static final DEL_ENGCHNJPNKOR:I = 0x2a

.field public static final DEL_ENGLISH:I = 0x2

.field public static final DEL_ENGLISH_UK:I = 0x3

.field public static final DEL_ESPANOL:I = 0xe

.field public static final DEL_FINNISH:I = 0x18

.field public static final DEL_FRENCH:I = 0xc

.field public static final DEL_GAEILGE:I = 0x26

.field public static final DEL_GERMAN:I = 0xd

.field public static final DEL_GREEK:I = 0x24

.field public static final DEL_HANJA:I = 0x2d

.field public static final DEL_HINDI:I = 0x1f

.field public static final DEL_INDONESIAN:I = 0x1a

.field public static final DEL_ITALIAN:I = 0x10

.field public static final DEL_JAPANESE:I = 0xa

.field public static final DEL_JAPANESE_KANJI:I = 0xb

.field public static final DEL_KOREAN:I = 0x0

.field public static final DEL_KOREAN_OLD:I = 0x1

.field public static final DEL_MALAYSIAN:I = 0x1e

.field public static final DEL_MAX:I = 0x2e

.field public static final DEL_NORWEGIAN:I = 0x16

.field public static final DEL_PERSIAN:I = 0x22

.field public static final DEL_POLISH:I = 0x25

.field public static final DEL_PORTUGUESE:I = 0xf

.field public static final DEL_RUSSIAN:I = 0x12

.field public static final DEL_SWEDISH:I = 0x17

.field public static final DEL_TAGALOG:I = 0x23

.field public static final DEL_THAI:I = 0x1c

.field public static final DEL_THAI_PHONETIC:I = 0x1d

.field public static final DEL_TURKISH:I = 0x11

.field public static final DEL_UKRAINIAN:I = 0x13

.field public static final DEL_URDU:I = 0x21

.field public static final DEL_URDU_ARABIC:I = 0x2b

.field public static final DEL_VIETNAMESE:I = 0x1b

.field public static final DEL_WESTERN:I = 0x27

.field public static final DEL_WORLD:I = 0x29


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/engine/DBType;


# direct methods
.method public constructor <init>(Lcom/diotek/diodict/engine/DBType;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/diotek/diodict/engine/DBType$enlanguage;->this$0:Lcom/diotek/diodict/engine/DBType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
