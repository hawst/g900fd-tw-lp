.class public Lcom/diotek/diodict/engine/WordList3rd;
.super Ljava/lang/Object;
.source "WordList3rd.java"


# instance fields
.field private mDCKeyword:Ljava/lang/String;

.field private mDCUID:I

.field private mDicType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dcUID"    # I
    .param p3, "nDicType"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCKeyword:Ljava/lang/String;

    .line 21
    iput p3, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDicType:I

    .line 22
    iput p2, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCUID:I

    .line 23
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "keyword"    # [B
    .param p2, "dcUID"    # I
    .param p3, "nDicType"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    invoke-static {p1, p3, v0}, Lcom/diotek/diodict/engine/StringConvert;->convertByteToString([BIZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCKeyword:Ljava/lang/String;

    .line 33
    iput p2, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCUID:I

    .line 34
    iput p3, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDicType:I

    .line 35
    return-void
.end method


# virtual methods
.method public getDicType()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDicType:I

    return v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSUID()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/diotek/diodict/engine/WordList3rd;->mDCUID:I

    return v0
.end method
