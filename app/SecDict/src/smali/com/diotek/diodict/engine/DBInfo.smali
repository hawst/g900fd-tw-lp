.class public Lcom/diotek/diodict/engine/DBInfo;
.super Ljava/lang/Object;
.source "DBInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEDT_MAX:I = 0xff02

.field public static final DEL_ENGLISH:I = 0x2

.field public static final DEL_KOREAN:I = 0x0

.field private static final serialVersionUID:J = 0x7fe72177d54065d7L


# instance fields
.field private mCompanyLogoResId:I

.field private mContentsVendor:I

.field private mDbType:I

.field private mEncode:I

.field private mEngineVersion:I

.field private mExtra1DBType:I

.field private mExtra2DBType:I

.field private mFileName:Ljava/lang/String;

.field private mFontArrayResId:[Ljava/lang/CharSequence;

.field private mIconResId:I

.field private mListIconResId:I

.field private mNameResId:I

.field private mNotifyFilename:Ljava/lang/String;

.field private mPairDBType:I

.field private mParentDBType:I

.field private mProductLogoResId:I

.field private mProductStringResId:I

.field private mSearchMethod:I

.field private mSourceLanguage:I

.field private mTargetLanguage:I


# direct methods
.method constructor <init>(IIIIIIIILjava/lang/String;IIIIIIII[Ljava/lang/CharSequence;Ljava/lang/String;I)V
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "dicname"    # I
    .param p3, "nIconId"    # I
    .param p4, "nListIconId"    # I
    .param p5, "nCompanyLogoResId"    # I
    .param p6, "nProductLogoResId"    # I
    .param p7, "nProductStringResId"    # I
    .param p8, "searchmethod"    # I
    .param p9, "filename"    # Ljava/lang/String;
    .param p10, "encoding"    # I
    .param p11, "nContentsVendor"    # I
    .param p12, "pairDB"    # I
    .param p13, "parentDB"    # I
    .param p14, "extra1DB"    # I
    .param p15, "extra2DB"    # I
    .param p16, "sourceLang"    # I
    .param p17, "targetLang"    # I
    .param p18, "fontArrayResId"    # [Ljava/lang/CharSequence;
    .param p19, "notifyName"    # Ljava/lang/String;
    .param p20, "engineVersion"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    .line 48
    iput p2, p0, Lcom/diotek/diodict/engine/DBInfo;->mNameResId:I

    .line 49
    iput p3, p0, Lcom/diotek/diodict/engine/DBInfo;->mIconResId:I

    .line 50
    iput p4, p0, Lcom/diotek/diodict/engine/DBInfo;->mListIconResId:I

    .line 51
    iput p5, p0, Lcom/diotek/diodict/engine/DBInfo;->mCompanyLogoResId:I

    .line 52
    iput p6, p0, Lcom/diotek/diodict/engine/DBInfo;->mProductLogoResId:I

    .line 53
    iput p7, p0, Lcom/diotek/diodict/engine/DBInfo;->mProductStringResId:I

    .line 54
    iput p8, p0, Lcom/diotek/diodict/engine/DBInfo;->mSearchMethod:I

    .line 55
    iput-object p9, p0, Lcom/diotek/diodict/engine/DBInfo;->mFileName:Ljava/lang/String;

    .line 56
    iput p10, p0, Lcom/diotek/diodict/engine/DBInfo;->mEncode:I

    .line 57
    iput p11, p0, Lcom/diotek/diodict/engine/DBInfo;->mContentsVendor:I

    .line 58
    iput p12, p0, Lcom/diotek/diodict/engine/DBInfo;->mPairDBType:I

    .line 59
    iput p13, p0, Lcom/diotek/diodict/engine/DBInfo;->mParentDBType:I

    .line 60
    iput p14, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra1DBType:I

    .line 61
    move/from16 v0, p15

    iput v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra2DBType:I

    .line 62
    move/from16 v0, p16

    iput v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mSourceLanguage:I

    .line 63
    move/from16 v0, p17

    iput v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mTargetLanguage:I

    .line 64
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mFontArrayResId:[Ljava/lang/CharSequence;

    .line 65
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mNotifyFilename:Ljava/lang/String;

    .line 66
    move/from16 v0, p20

    iput v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mEngineVersion:I

    .line 67
    return-void
.end method


# virtual methods
.method public getCompanyLogoResId()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mCompanyLogoResId:I

    return v0
.end method

.method public getContentsVendor()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mContentsVendor:I

    return v0
.end method

.method public getDBType()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    return v0
.end method

.method public getDicNameResId()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mNameResId:I

    return v0
.end method

.method public getEncode()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mEncode:I

    return v0
.end method

.method public getEngineVersion()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mEngineVersion:I

    return v0
.end method

.method public getExtra1DBType()I
    .locals 2

    .prologue
    .line 182
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra1DBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_0

    .line 183
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    .line 186
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra1DBType:I

    goto :goto_0
.end method

.method public getExtra2DBType()I
    .locals 2

    .prologue
    .line 218
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra2DBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_0

    .line 219
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    .line 221
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra2DBType:I

    goto :goto_0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontArrayResId()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mFontArrayResId:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getIconResID()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mIconResId:I

    return v0
.end method

.method public getListIconResID()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mListIconResId:I

    return v0
.end method

.method public getNotifyFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mNotifyFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getPairDBType()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mPairDBType:I

    return v0
.end method

.method public getParentDBType()I
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mParentDBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_0

    .line 171
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    .line 174
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mParentDBType:I

    goto :goto_0
.end method

.method public getProductLogoResId()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mProductLogoResId:I

    return v0
.end method

.method public getProductStringResId()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mProductStringResId:I

    return v0
.end method

.method public getSearchMethod()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mSearchMethod:I

    return v0
.end method

.method public getSourceLanguage()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mSourceLanguage:I

    return v0
.end method

.method public getTargetLanguage()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mTargetLanguage:I

    return v0
.end method

.method public isExtra1DBType()Z
    .locals 2

    .prologue
    .line 194
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra1DBType:I

    iget v1, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    if-ne v0, v1, :cond_0

    .line 195
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExtra2DBType()Z
    .locals 2

    .prologue
    .line 206
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra2DBType:I

    iget v1, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    if-ne v0, v1, :cond_0

    .line 207
    const/4 v0, 0x1

    .line 210
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIndepencenceMainOnly()Z
    .locals 2

    .prologue
    .line 262
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    iget v1, p0, Lcom/diotek/diodict/engine/DBInfo;->mPairDBType:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mPairDBType:I

    const v1, 0xff02

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotIndepencence()Z
    .locals 2

    .prologue
    .line 270
    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    iget v1, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra1DBType:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/diotek/diodict/engine/DBInfo;->mDbType:I

    iget v1, p0, Lcom/diotek/diodict/engine/DBInfo;->mExtra2DBType:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
