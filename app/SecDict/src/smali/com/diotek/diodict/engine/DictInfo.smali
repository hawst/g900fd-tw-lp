.class public Lcom/diotek/diodict/engine/DictInfo;
.super Ljava/lang/Object;
.source "DictInfo.java"


# static fields
.field public static final CP_1250:I = 0x4e2

.field public static final CP_ARABIC:I = 0x4e8

.field public static final CP_BAL:I = 0x4e9

.field public static final CP_CHN:I = 0x3a8

.field public static final CP_CRL:I = 0x556a

.field public static final CP_ENG:I = 0x0

.field public static final CP_ERR:I = -0x1

.field public static final CP_GREEK:I = 0x4e5

.field public static final CP_HIN:I = 0x533

.field public static final CP_JPN:I = 0x3a4

.field public static final CP_KOR:I = 0x3b5

.field public static final CP_LT1:I = 0x4e4

.field public static final CP_SPECIAL:I = 0xff

.field public static final CP_THAI:I = 0x36a

.field public static final CP_TUR:I = 0x4e6

.field public static final CP_TWN:I = 0x3b6

.field public static final ERR_FINISH:I = 0x3

.field public static final ERR_NONE:I = 0x0

.field public static final ERR_NOT_EXIST:I = 0x1

.field public static final ERR_NOT_SUPPORT_DICT:I = 0x2

.field public static final ERR_NOWLOADING:I = 0x4

.field public static final RESULTLIST_TYPE_HYPER:I = 0x1

.field public static final RESULTLIST_TYPE_NORMAL:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
