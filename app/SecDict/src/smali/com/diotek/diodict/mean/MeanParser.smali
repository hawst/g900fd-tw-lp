.class public abstract Lcom/diotek/diodict/mean/MeanParser;
.super Ljava/lang/Object;
.source "MeanParser.java"


# static fields
.field protected static final ADD_BUTTON_TAG:Ljava/lang/String; = "###ADD_BUTTON###"

.field protected static final CSS_FONTSIZE_TAG:Ljava/lang/String; = "(body \\{ font-size: ###FONT_SIZE###pt; \\})"

.field protected static final CSS_TAG:Ljava/lang/String; = "###LAYOUT###"

.field protected static final ENCODE_UNICODE:Ljava/lang/String; = "UTF-16"

.field protected static final ENCODE_UTF8:Ljava/lang/String; = "utf-8"

.field protected static final EXAMPLE_BULLET_TAG:Ljava/lang/String; = "\ufffd"

.field protected static final EXAMPLE_BULLET_TAG_FORMAT:Ljava/lang/String;

.field protected static final FONT_SIZE_TAG:Ljava/lang/String; = "style=\"font-size:#####FONTSIZE#####px\""

.field protected static final JAVASCRIPT_TAG:Ljava/lang/String; = "###JAVASCRIPT_CODE###"

.field protected static final TTS_TAG:Ljava/lang/String; = "###TTS###"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    const-string v0, "%c"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/16 v3, 0x25aa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/diotek/diodict/mean/MeanParser;->EXAMPLE_BULLET_TAG_FORMAT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract parseMeaning(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
.end method
