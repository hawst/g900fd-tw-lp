.class public Lcom/diotek/diodict/mean/MeanParser3DB;
.super Lcom/diotek/diodict/mean/MeanParser;
.source "MeanParser3DB.java"


# static fields
.field private static final CSS_TAG_FONTFACE:Ljava/lang/String; = "@font-face { font-family: \'DioDictFnt3\'; src: url(\'file://%s\'); font-weight: normal; font-style: normal;}"

.field private static final FONTFACE_FILENAME:Ljava/lang/String; = "DioDictFnt3.ttf"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/diotek/diodict/mean/MeanParser;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFontFaceCSSFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "fontPath"    # Ljava/lang/String;

    .prologue
    .line 62
    const-string v0, "@font-face { font-family: \'DioDictFnt3\'; src: url(\'file://%s\'); font-weight: normal; font-style: normal;}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "DioDictFnt3.ttf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parseMeaning(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "fontPath"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 33
    if-nez p2, :cond_0

    .line 34
    const/4 v6, 0x0

    .line 58
    :goto_0
    return-object v6

    .line 36
    :cond_0
    const-string v7, "<body>"

    invoke-virtual {p2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v5, v7, 0x6

    .line 37
    .local v5, "start":I
    const-string v7, "</body>"

    invoke-virtual {p2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 38
    .local v3, "end":I
    invoke-virtual {p2, v10, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 39
    .local v4, "head":Ljava/lang/String;
    invoke-virtual {p2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "bottom":Ljava/lang/String;
    const-string v2, ""

    .line 43
    .local v2, "css":Ljava/lang/String;
    if-eqz p3, :cond_1

    .line 44
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/mean/MeanParser3DB;->getFontFaceCSSFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 46
    :cond_1
    const-string v7, "(body \\{ font-size: ###FONT_SIZE###pt; \\})"

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 47
    const-string v7, "###LAYOUT###"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 48
    const-string v7, "###JAVASCRIPT_CODE###"

    const-string v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 49
    const-string v7, "UTF-16"

    const-string v8, "utf-8"

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 51
    const-string v7, "###ADD_BUTTON###"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    const-string v7, "###TTS###"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    const-string v7, "\ufffd"

    sget-object v8, Lcom/diotek/diodict/mean/MeanParser3DB;->EXAMPLE_BULLET_TAG_FORMAT:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    const/4 v7, 0x3

    new-array v6, v7, [Ljava/lang/String;

    aput-object v4, v6, v10

    const/4 v7, 0x1

    aput-object v0, v6, v7

    const/4 v7, 0x2

    aput-object v1, v6, v7

    .line 58
    .local v6, "strings":[Ljava/lang/String;
    goto :goto_0
.end method
