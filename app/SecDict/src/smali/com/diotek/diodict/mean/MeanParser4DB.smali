.class public Lcom/diotek/diodict/mean/MeanParser4DB;
.super Lcom/diotek/diodict/mean/MeanParser;
.source "MeanParser4DB.java"


# static fields
.field private static final CSS_FILENAME:Ljava/lang/String; = "diodict4_newace.css"

.field private static final FONTFACE_CSS_TAG_FORMAT:Ljava/lang/String; = "@font-face { font-family: \'newace_diotek\'; src: url(\'file://%s\'); }"

.field private static final FONTFACE_FILENAME:Ljava/lang/String; = "newace_diotek_regular.ttf"

.field private static final HTML_FILEPATH:Ljava/lang/String; = "diodict4_meaning.html"

.field private static final HTML_TAG:Ljava/lang/String; = "#####CONTENTS#####"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/diotek/diodict/mean/MeanParser;-><init>()V

    return-void
.end method

.method private assetFileToStringBuilder(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "builder"    # Ljava/lang/StringBuilder;
    .param p3, "assetsFilename"    # Ljava/lang/String;
    .param p4, "encode"    # Ljava/lang/String;

    .prologue
    .line 80
    const/4 v2, 0x0

    .line 82
    .local v2, "input":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 83
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 85
    .local v0, "buffer":[B
    :goto_0
    const/4 v4, -0x1

    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 86
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0, p4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 87
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    .end local v0    # "buffer":[B
    .end local v3    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    if-eqz v2, :cond_0

    .line 95
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 99
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    return-void

    .line 94
    .restart local v0    # "buffer":[B
    :cond_1
    if-eqz v2, :cond_0

    .line 95
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 96
    :catch_1
    move-exception v4

    goto :goto_1

    .line 93
    .end local v0    # "buffer":[B
    :catchall_0
    move-exception v4

    .line 94
    if-eqz v2, :cond_2

    .line 95
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 97
    :cond_2
    :goto_2
    throw v4

    .line 96
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    goto :goto_2
.end method

.method private replaceTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "findTag"    # Ljava/lang/String;
    .param p2, "replaceString"    # Ljava/lang/String;
    .param p3, "destBuf"    # Ljava/lang/StringBuilder;

    .prologue
    .line 102
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 107
    .local v0, "startIndex":I
    if-lez v0, :cond_0

    .line 108
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p3, v0, v1, p2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method getCSSFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "diodict4_newace.css"

    return-object v0
.end method

.method getFontFaceCSSFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "fontPath"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "@font-face { font-family: \'newace_diotek\'; src: url(\'file://%s\'); }"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "newace_diotek_regular.ttf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getFullMeaning(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .local v0, "bufString":Ljava/lang/StringBuilder;
    const-string v1, "diodict4_meaning.html"

    const-string v2, "utf-8"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/diotek/diodict/mean/MeanParser4DB;->assetFileToStringBuilder(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v1, "#####CONTENTS#####"

    invoke-direct {p0, v1, p2, v0}, Lcom/diotek/diodict/mean/MeanParser4DB;->replaceTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public parseMeaning(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "fontPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 27
    if-nez p2, :cond_0

    move-object v2, v3

    .line 48
    :goto_0
    return-object v2

    .line 30
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/diotek/diodict/mean/MeanParser4DB;->getFullMeaning(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "bufString":Ljava/lang/String;
    invoke-virtual {p0, p3}, Lcom/diotek/diodict/mean/MeanParser4DB;->getFontFaceCSSFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "css":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 33
    const-string v4, "(body \\{ font-size: ###FONT_SIZE###pt; \\})"

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    :cond_1
    const-string v4, "###LAYOUT###"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    const-string v4, "\ufffd"

    sget-object v5, Lcom/diotek/diodict/mean/MeanParser4DB;->EXAMPLE_BULLET_TAG_FORMAT:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v4, "style=\"font-size:#####FONTSIZE#####px\""

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    const-string v4, "###JAVASCRIPT_CODE###"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    const-string v4, "###ADD_BUTTON###"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    const-string v4, "###TTS###"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    const-string v4, "UTF-16"

    const-string v5, "utf-8"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const/4 v4, 0x3

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v4, 0x1

    aput-object v0, v2, v4

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 48
    .local v2, "strings":[Ljava/lang/String;
    goto :goto_0
.end method
