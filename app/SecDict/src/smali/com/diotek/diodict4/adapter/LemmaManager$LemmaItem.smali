.class Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;
.super Ljava/lang/Object;
.source "LemmaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict4/adapter/LemmaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LemmaItem"
.end annotation


# instance fields
.field private mKeyword:Ljava/lang/String;

.field private mResultList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/diotek/diodict4/adapter/LemmaManager;


# direct methods
.method constructor <init>(Lcom/diotek/diodict4/adapter/LemmaManager;)V
    .locals 1

    .prologue
    .line 260
    iput-object p1, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->this$0:Lcom/diotek/diodict4/adapter/LemmaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mKeyword:Ljava/lang/String;

    .line 262
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getResult(I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "srcEnLang"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 278
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public setResultList(Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "srcEnLang"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 270
    :cond_1
    iput-object p1, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mKeyword:Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->mResultList:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    return-void
.end method
