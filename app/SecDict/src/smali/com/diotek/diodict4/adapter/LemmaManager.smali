.class public Lcom/diotek/diodict4/adapter/LemmaManager;
.super Ljava/lang/Object;
.source "LemmaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;
    }
.end annotation


# static fields
.field private static final LEMMA_DBPATH:Ljava/lang/String; = "/lemmaDB/"

.field private static final LEMMA_DBPATH_CHINESE:Ljava/lang/String; = "chinese/"

.field private static final LEMMA_DBPATH_DANISH:Ljava/lang/String; = "danish/"

.field private static final LEMMA_DBPATH_DUTCH:Ljava/lang/String; = "dutch/"

.field private static final LEMMA_DBPATH_ENGLISH:Ljava/lang/String; = "english/"

.field private static final LEMMA_DBPATH_FRENCH:Ljava/lang/String; = "french/"

.field private static final LEMMA_DBPATH_GERMAN:Ljava/lang/String; = "german/"

.field private static final LEMMA_DBPATH_GREEK:Ljava/lang/String; = "greek/"

.field private static final LEMMA_DBPATH_INDONESIAN:Ljava/lang/String; = "indonesian/"

.field private static final LEMMA_DBPATH_ITALIAN:Ljava/lang/String; = "italian/"

.field private static final LEMMA_DBPATH_JAPANESE:Ljava/lang/String; = "japanese/"

.field private static final LEMMA_DBPATH_KOREAN:Ljava/lang/String; = "korean/"

.field private static final LEMMA_DBPATH_NORWEGIAN:Ljava/lang/String; = "norwegian/"

.field private static final LEMMA_DBPATH_POLISH:Ljava/lang/String; = "polish/"

.field private static final LEMMA_DBPATH_PORTUGUESE:Ljava/lang/String; = "portuguese/"

.field private static final LEMMA_DBPATH_RUSSIAN:Ljava/lang/String; = "russian/"

.field private static final LEMMA_DBPATH_SPANISH:Ljava/lang/String; = "spanish/"

.field private static final LEMMA_DBPATH_SWEDISH:Ljava/lang/String; = "swedish/"

.field private static final LEMMA_DBPATH_TURKISH:Ljava/lang/String; = "turkish/"

.field private static final LEMMA_DBPATH_UKRAINIAN:Ljava/lang/String; = "ukrainian/"

.field private static final LEMMA_ICU_DATA_FILE:Ljava/lang/String; = "icudt52l.dat"

.field private static final LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;


# instance fields
.field private mLemma:Ldiodict/lemma/DioDictLemma;

.field private mLemmaItem:Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/diotek/diodict4/adapter/LemmaManager;->LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemma:Ldiodict/lemma/DioDictLemma;

    .line 46
    new-instance v0, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;

    invoke-direct {v0, p0}, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;-><init>(Lcom/diotek/diodict4/adapter/LemmaManager;)V

    iput-object v0, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemmaItem:Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;

    .line 260
    return-void
.end method

.method private getLanguageCode(I)Ldiodict/lemma/LemmaLanguage;
    .locals 1
    .param p1, "enlang"    # I

    .prologue
    .line 112
    packed-switch p1, :pswitch_data_0

    .line 203
    :pswitch_0
    sget-object v0, Lcom/diotek/diodict4/adapter/LemmaManager;->LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;

    .line 206
    .local v0, "langCode":Ldiodict/lemma/LemmaLanguage;
    :goto_0
    return-object v0

    .line 115
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_1
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->KOREAN:Ldiodict/lemma/LemmaLanguage;

    .line 116
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 119
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_2
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->ENGLISH:Ldiodict/lemma/LemmaLanguage;

    .line 120
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 127
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_3
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->CHINESE:Ldiodict/lemma/LemmaLanguage;

    .line 128
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 131
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_4
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->JAPANESE:Ldiodict/lemma/LemmaLanguage;

    .line 132
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 134
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_5
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->FRENCH:Ldiodict/lemma/LemmaLanguage;

    .line 135
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 137
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_6
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->GERMAN:Ldiodict/lemma/LemmaLanguage;

    .line 138
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 140
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_7
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->SPANISH:Ldiodict/lemma/LemmaLanguage;

    .line 141
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 143
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_8
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

    .line 144
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 146
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_9
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->ITALIAN:Ldiodict/lemma/LemmaLanguage;

    .line 147
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 149
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_a
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->TURKISH:Ldiodict/lemma/LemmaLanguage;

    .line 150
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 152
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_b
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->RUSSIAN:Ldiodict/lemma/LemmaLanguage;

    .line 153
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 155
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_c
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->UKRAINIAN:Ldiodict/lemma/LemmaLanguage;

    .line 156
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 158
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_d
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->DANISH:Ldiodict/lemma/LemmaLanguage;

    .line 159
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 161
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_e
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->DUTCH:Ldiodict/lemma/LemmaLanguage;

    .line 162
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 164
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_f
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

    .line 165
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 167
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_10
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->SWEDISH:Ldiodict/lemma/LemmaLanguage;

    .line 168
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 171
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_11
    sget-object v0, Lcom/diotek/diodict4/adapter/LemmaManager;->LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;

    .line 172
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 174
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_12
    sget-object v0, Lcom/diotek/diodict4/adapter/LemmaManager;->LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;

    .line 175
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 177
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_13
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->INDONESIAN:Ldiodict/lemma/LemmaLanguage;

    .line 178
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 180
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_14
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->GREEK:Ldiodict/lemma/LemmaLanguage;

    .line 181
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 183
    .end local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    :pswitch_15
    sget-object v0, Ldiodict/lemma/LemmaLanguage;->POLISH:Ldiodict/lemma/LemmaLanguage;

    .line 184
    .restart local v0    # "langCode":Ldiodict/lemma/LemmaLanguage;
    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method private getLemmaPath(Ldiodict/lemma/LemmaLanguage;)Ljava/lang/String;
    .locals 4
    .param p1, "langCode"    # Ldiodict/lemma/LemmaLanguage;

    .prologue
    .line 209
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/lemmaDB/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "dbpath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 211
    .local v1, "inifilepath":Ljava/lang/String;
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->KOREAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_1

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "korean/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 252
    :cond_0
    :goto_0
    return-object v1

    .line 213
    :cond_1
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->JAPANESE:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_2

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "japanese/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 215
    :cond_2
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->CHINESE:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_3

    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "chinese/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 217
    :cond_3
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->DANISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_4

    .line 218
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "danish/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 219
    :cond_4
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->DUTCH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_5

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dutch/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 221
    :cond_5
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->ENGLISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_6

    .line 222
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "english/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 225
    :cond_6
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->FRENCH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_7

    .line 226
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "french/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 227
    :cond_7
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->GERMAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_8

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "german/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 229
    :cond_8
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->GREEK:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_9

    .line 230
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "greek/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 231
    :cond_9
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->INDONESIAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_a

    .line 232
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "indonesian/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 233
    :cond_a
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->ITALIAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_b

    .line 234
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "italian/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 235
    :cond_b
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->NORWEGIAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_c

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "norwegian/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 237
    :cond_c
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->POLISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_d

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "polish/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 239
    :cond_d
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->PORTUGUESE:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_e

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "portuguese/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 241
    :cond_e
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->RUSSIAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_f

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "russian/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 243
    :cond_f
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->SPANISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_10

    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "spanish/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 245
    :cond_10
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->SWEDISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_11

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "swedish/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 247
    :cond_11
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->UKRAINIAN:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_12

    .line 248
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ukrainian/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 249
    :cond_12
    sget-object v2, Ldiodict/lemma/LemmaLanguage;->TURKISH:Ldiodict/lemma/LemmaLanguage;

    if-ne p1, v2, :cond_0

    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "turkish/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method


# virtual methods
.method public getLemma(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 8
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "srcEnLang"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v6, Ldiodict/lemma/DioDictLemma;

    invoke-direct {v6}, Ldiodict/lemma/DioDictLemma;-><init>()V

    iput-object v6, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemma:Ldiodict/lemma/DioDictLemma;

    .line 69
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/diotek/diodict4/adapter/LemmaManager;->getLanguageCode(I)Ldiodict/lemma/LemmaLanguage;

    move-result-object v2

    .line 71
    .local v2, "langCode":Ldiodict/lemma/LemmaLanguage;
    sget-object v6, Lcom/diotek/diodict4/adapter/LemmaManager;->LEMMA_MAX:Ldiodict/lemma/LemmaLanguage;

    if-ne v2, v6, :cond_0

    .line 91
    :goto_0
    return-object v4

    .line 74
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/lemmaDB/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "icudt52l.dat"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "icuFilePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemma:Ldiodict/lemma/DioDictLemma;

    invoke-direct {p0, v2}, Lcom/diotek/diodict4/adapter/LemmaManager;->getLemmaPath(Ldiodict/lemma/LemmaLanguage;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1, v2}, Ldiodict/lemma/DioDictLemma;->open(Ljava/lang/String;Ljava/lang/String;Ldiodict/lemma/LemmaLanguage;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 76
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 78
    iget-object v6, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemma:Ldiodict/lemma/DioDictLemma;

    invoke-virtual {v6, p1}, Ldiodict/lemma/DioDictLemma;->lemmatize(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 79
    .local v5, "tokens":Ljava/util/List;, "Ljava/util/List<Ldiodict/lemma/DioDictLemma$LemmaToken;>;"
    const/4 v3, -0x1

    .line 80
    .local v3, "oldindex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_2

    .line 81
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldiodict/lemma/DioDictLemma$LemmaToken;

    invoke-virtual {v6}, Ldiodict/lemma/DioDictLemma$LemmaToken;->getIndex()I

    move-result v6

    if-eq v3, v6, :cond_1

    .line 82
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldiodict/lemma/DioDictLemma$LemmaToken;

    invoke-virtual {v6}, Ldiodict/lemma/DioDictLemma$LemmaToken;->getIndex()I

    move-result v3

    .line 83
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldiodict/lemma/DioDictLemma$LemmaToken;

    invoke-virtual {v6}, Ldiodict/lemma/DioDictLemma$LemmaToken;->getLemma()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    :cond_1
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldiodict/lemma/DioDictLemma$LemmaToken;

    invoke-virtual {v6}, Ldiodict/lemma/DioDictLemma$LemmaToken;->getLemma()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 89
    .end local v0    # "i":I
    .end local v3    # "oldindex":I
    .end local v5    # "tokens":Ljava/util/List;, "Ljava/util/List<Ldiodict/lemma/DioDictLemma$LemmaToken;>;"
    :cond_2
    iget-object v6, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemma:Ldiodict/lemma/DioDictLemma;

    invoke-virtual {v6}, Ldiodict/lemma/DioDictLemma;->close()V

    .line 90
    iget-object v6, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemmaItem:Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;

    invoke-virtual {v6, p1, p2, v4}, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->setResultList(Ljava/lang/String;ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public getOriginWord(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getDBInfo(I)Lcom/diotek/diodict/core/engine/DBInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/diotek/diodict/core/engine/DBInfo;->getSourceLanguage()I

    move-result v1

    .line 51
    .local v1, "srcEnLang":I
    invoke-virtual {p0, p2, v1}, Lcom/diotek/diodict4/adapter/LemmaManager;->getSearcedLemmaResult(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 52
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {p0, p2, v1}, Lcom/diotek/diodict4/adapter/LemmaManager;->getLemma(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 55
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    .end local p2    # "searchWord":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "searchWord":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object p2, v2

    goto :goto_0
.end method

.method public getSearcedLemmaResult(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "srcEnLang"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemmaItem:Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;

    invoke-virtual {v1}, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->getKeyword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/diotek/diodict4/adapter/LemmaManager;->mLemmaItem:Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;

    invoke-virtual {v1, p2}, Lcom/diotek/diodict4/adapter/LemmaManager$LemmaItem;->getResult(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 104
    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    return-object v0
.end method
