.class public Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
.super Ljava/lang/Object;
.source "DictDBManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SupportDBInfo"
.end annotation


# instance fields
.field private mDBType:I

.field private mDicFileName:Ljava/lang/String;

.field private mDictName:Ljava/lang/String;

.field private mEngineType:I

.field private mSQLDBFileName:Ljava/lang/String;

.field private mSourceLanguage:I

.field private mSpellCheckDBFileName:Ljava/lang/String;

.field private mTargetLanguage:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "pwDbName"    # Ljava/lang/String;
    .param p3, "sqlDbName"    # Ljava/lang/String;
    .param p4, "dicname"    # Ljava/lang/String;
    .param p5, "sourceLang"    # I
    .param p6, "targetLang"    # I

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput p1, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDBType:I

    .line 119
    iput-object p2, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDicFileName:Ljava/lang/String;

    .line 120
    iput-object p3, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    .line 121
    iput-object p4, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDictName:Ljava/lang/String;

    .line 122
    iput p5, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSourceLanguage:I

    .line 123
    iput p6, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mTargetLanguage:I

    .line 124
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const/4 v0, 0x4

    iput v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    .line 129
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSpellCheckDBFileName:Ljava/lang/String;

    .line 130
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "pwDbName"    # Ljava/lang/String;
    .param p3, "sqlDbName"    # Ljava/lang/String;
    .param p4, "dicname"    # Ljava/lang/String;
    .param p5, "sourceLang"    # I
    .param p6, "targetLang"    # I
    .param p7, "spellDbName"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput p1, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDBType:I

    .line 134
    iput-object p2, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDicFileName:Ljava/lang/String;

    .line 135
    iput-object p3, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    .line 136
    iput-object p4, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDictName:Ljava/lang/String;

    .line 137
    iput p5, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSourceLanguage:I

    .line 138
    iput p6, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mTargetLanguage:I

    .line 139
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x4

    iput v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    .line 144
    :goto_0
    iput-object p7, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSpellCheckDBFileName:Ljava/lang/String;

    .line 145
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    goto :goto_0
.end method


# virtual methods
.method public getDBInfo()Lcom/diotek/diodict/core/engine/DBInfo;
    .locals 22

    .prologue
    .line 148
    new-instance v1, Lcom/diotek/diodict/core/engine/DBInfo;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDBType:I

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDicFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const v12, 0xff02

    const v13, 0xff02

    move-object/from16 v0, p0

    iget v14, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSourceLanguage:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mTargetLanguage:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    move/from16 v21, v0

    invoke-direct/range {v1 .. v21}, Lcom/diotek/diodict/core/engine/DBInfo;-><init>(IIIIIILjava/lang/String;Ljava/lang/String;IIIIII[Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;I)V

    return-object v1
.end method

.method public getDBType()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDBType:I

    return v0
.end method

.method public getDictFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDicFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getDictName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mDictName:Ljava/lang/String;

    return-object v0
.end method

.method public getEngineVersion()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mEngineType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSQLDBFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSQLDBFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceLang()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSourceLanguage:I

    return v0
.end method

.method public getSpellCheckDBFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mSpellCheckDBFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetLand()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->mTargetLanguage:I

    return v0
.end method
