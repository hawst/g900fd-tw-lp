.class public Lcom/diotek/diodict4/core/adapter/LanguageChecker;
.super Ljava/lang/Object;
.source "LanguageChecker.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isArabicWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x4e8

    if-ne v0, v1, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isChineseWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CharacterInfo;->getLanguage(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 137
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isEnglishWord(Ljava/lang/String;)Z
    .locals 1
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isGreekWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x4e5

    if-ne v0, v1, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHindiWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x533

    if-ne v0, v1, :cond_0

    .line 163
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isJapaneseWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CharacterInfo;->getLanguage(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 142
    const/4 v0, 0x1

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKoreanWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 127
    invoke-static {p0}, Lcom/diotek/diodict/core/lang/CharacterInfo;->getLanguage(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 130
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportLangType(Ljava/lang/String;I)Z
    .locals 3
    .param p0, "word"    # Ljava/lang/String;
    .param p1, "sourceLang"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 22
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isEnglishWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 23
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 116
    :cond_0
    :goto_0
    :pswitch_1
    :sswitch_0
    return v0

    .line 49
    :cond_1
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isKoreanWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 50
    packed-switch p1, :pswitch_data_1

    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_2
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isChineseWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_3
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isJapaneseWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 73
    packed-switch p1, :pswitch_data_2

    move v0, v1

    .line 77
    goto :goto_0

    .line 79
    :cond_4
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isArabicWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 80
    packed-switch p1, :pswitch_data_3

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isGreekWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 87
    packed-switch p1, :pswitch_data_4

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_6
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isHindiWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 94
    packed-switch p1, :pswitch_data_5

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_7
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isThaiWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 101
    packed-switch p1, :pswitch_data_6

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_8
    invoke-static {p0}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isTurkishWord(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    packed-switch p1, :pswitch_data_7

    move v0, v1

    .line 113
    goto :goto_0

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 50
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 58
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xb -> :sswitch_0
        0x2c -> :sswitch_0
        0x2d -> :sswitch_0
    .end sparse-switch

    .line 73
    :pswitch_data_2
    .packed-switch 0xa
        :pswitch_1
    .end packed-switch

    .line 80
    :pswitch_data_3
    .packed-switch 0x19
        :pswitch_1
    .end packed-switch

    .line 87
    :pswitch_data_4
    .packed-switch 0x24
        :pswitch_1
    .end packed-switch

    .line 94
    :pswitch_data_5
    .packed-switch 0x1f
        :pswitch_1
    .end packed-switch

    .line 101
    :pswitch_data_6
    .packed-switch 0x1c
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 109
    :pswitch_data_7
    .packed-switch 0x11
        :pswitch_1
    .end packed-switch
.end method

.method private static isThaiWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x36a

    if-ne v0, v1, :cond_0

    .line 170
    const/4 v0, 0x1

    .line 172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isTurkishWord(Ljava/lang/String;)Z
    .locals 2
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-static {p0}, Lcom/diotek/diodict/engine/DictUtils;->getCodePage(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x4e6

    if-ne v0, v1, :cond_0

    .line 177
    const/4 v0, 0x1

    .line 179
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
