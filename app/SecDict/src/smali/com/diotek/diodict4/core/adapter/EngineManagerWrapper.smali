.class public Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;
.super Ljava/lang/Object;
.source "EngineManagerWrapper.java"

# interfaces
.implements Lcom/diotek/diodict/core/engine/EngineManagerInterface;


# static fields
.field private static volatile mInstance:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;


# instance fields
.field private mCurrentDbType:I

.field private mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

.field private mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

.field private mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentDbType:I

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    .line 38
    new-instance v0, Lcom/diotek/diodict/engine/Engine3Manager;

    invoke-direct {v0}, Lcom/diotek/diodict/engine/Engine3Manager;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    .line 39
    new-instance v0, Lcom/diotek/diodict/core/engine/Engine4Manager;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/Engine4Manager;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    return-void
.end method

.method public static getInstance()Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    if-nez v0, :cond_0

    .line 30
    const-class v1, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    monitor-enter v1

    .line 31
    :try_start_0
    new-instance v0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-direct {v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;-><init>()V

    sput-object v0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    .line 32
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_0
    sget-object v0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private isNotOpenedDB(I)Z
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public closeDatabase()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->closeDatabase()V

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->closeDatabase()V

    .line 316
    :cond_1
    return-void
.end method

.method public closeSpellcheck()I
    .locals 2

    .prologue
    .line 320
    const/4 v0, 0x0

    .line 322
    .local v0, "err":I
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->closeSpellcheck()I

    move-result v0

    .line 325
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    if-eqz v1, :cond_1

    .line 326
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->closeSpellcheck()I

    move-result v0

    .line 329
    :cond_1
    return v0
.end method

.method public extendSearchWord(ILjava/lang/String;IZ)I
    .locals 3
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    const/4 v1, 0x0

    .line 345
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    :goto_0
    return v1

    .line 348
    :cond_0
    iget-object v2, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->extendSearchWord(ILjava/lang/String;IZ)I

    move-result v0

    .line 349
    .local v0, "listSize":I
    if-lez v0, :cond_1

    .end local v0    # "listSize":I
    :goto_1
    move v1, v0

    goto :goto_0

    .restart local v0    # "listSize":I
    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "posp"    # I
    .param p3, "infl"    # Ljava/lang/String;
    .param p4, "ending"    # Ljava/lang/String;
    .param p5, "stem"    # Ljava/lang/String;

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    const/4 v0, 0x0

    .line 403
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getConjugation(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getContentsType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 418
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    const/16 v0, -0x64

    .line 422
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getContentsType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getContentsVendor(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 239
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    const/16 v0, -0x64

    .line 243
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getContentsVendor(I)I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentDbType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const/16 v0, -0x64

    .line 126
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getCurrentDbType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentEngineVersion(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 221
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    const/16 v0, -0x64

    .line 225
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getCurrentEngineVersion(I)I

    move-result v0

    goto :goto_0
.end method

.method protected getCurrentManager(I)Lcom/diotek/diodict/core/engine/EngineManagerInterface;
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 64
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getEngineType(I)I

    move-result v0

    .line 66
    .local v0, "engineType":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine3Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    .line 72
    :goto_0
    return-object v1

    .line 68
    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mEngine4Manager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    goto :goto_0

    .line 72
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDataBaseType(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 427
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 428
    const/16 v0, -0x64

    .line 431
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getDataBaseType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getDatabaseVersion(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 436
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    const-string v0, ""

    .line 440
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getDatabaseVersion(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEngineVersion(I)Ljava/lang/String;
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 190
    const-string v0, ""

    .line 192
    .local v0, "engineVersion":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    const-string v1, ""

    .line 198
    :goto_0
    return-object v1

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getEngineVersion(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 198
    goto :goto_0
.end method

.method public getEngineVersionByNumeric(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 445
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 446
    const/16 v0, -0x64

    .line 449
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getEngineVersionByNumeric(I)I

    move-result v0

    goto :goto_0
.end method

.method public getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 378
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    const/4 v0, 0x0

    .line 381
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getEntryId(II)Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v0

    goto :goto_0
.end method

.method public getEntryLanguage(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 265
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const/16 v0, -0x64

    .line 269
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getEntryLanguage(I)I

    move-result v0

    goto :goto_0
.end method

.method public getError(I)I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    const/16 v0, -0x64

    .line 458
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getError(I)I

    move-result v0

    goto :goto_0
.end method

.method public getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 463
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    const/4 v0, 0x0

    .line 467
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getExamList(ILcom/diotek/diodict/core/engine/EntryId;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchword"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "isNext"    # Z

    .prologue
    .line 472
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    const/4 v0, 0x0

    .line 476
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getExamListbyKeyword(ILjava/lang/String;IZ)Lcom/diotek/diodict/core/engine/ExamList;

    move-result-object v0

    goto :goto_0
.end method

.method public getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const-string v0, ""

    .line 234
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getGuideWord(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHangulroError(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    const/16 v0, -0x64

    .line 485
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getHangulroError(I)I

    move-result v0

    goto :goto_0
.end method

.method public getHangulroList(I)I
    .locals 1
    .param p1, "langType"    # I

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    const/16 v0, -0x64

    .line 494
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getHangulroList(I)I

    move-result v0

    goto :goto_0
.end method

.method public getHeadType(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 362
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    const/16 v0, -0x64

    .line 365
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getHeadType(II)I

    move-result v0

    goto :goto_0
.end method

.method public getHeadwordId(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "uniqueId"    # I

    .prologue
    .line 370
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    const/16 v0, -0x64

    .line 373
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getHeadwordId(II)I

    move-result v0

    goto :goto_0
.end method

.method public getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    const-string v0, ""

    .line 503
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getMeaning(ILcom/diotek/diodict/core/engine/EntryId;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->setCurrentDbType(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, ""

    .line 97
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNormalizedText(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 508
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    const-string v0, ""

    .line 512
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getNormalizedText(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreview(ILjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueId"    # I

    .prologue
    .line 256
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    const-string v0, ""

    .line 260
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getPreview(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "meanInfo"    # Lcom/diotek/diodict/core/engine/MeanInfo;

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/diotek/diodict/core/engine/MeanInfo;->getDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->setCurrentDbType(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, ""

    .line 106
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getPreview(Lcom/diotek/diodict/core/engine/MeanInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPronounce(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "uniqueid"    # I
    .param p3, "dbType"    # I

    .prologue
    .line 535
    invoke-virtual {p0, p3}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 536
    const-string v0, ""

    .line 539
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getPronounce(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;
    .locals 1
    .param p1, "searchlistType"    # I

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 338
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v0

    goto :goto_0
.end method

.method public getSpellcheckResultSize()I
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    const/16 v0, -0x64

    .line 296
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getSpellcheckResultSize()I

    move-result v0

    goto :goto_0
.end method

.method public getSpellcheckWord(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    const-string v0, ""

    .line 305
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getSpellcheckWord(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTranslationLanguages(I)[I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 518
    const/4 v0, 0x0

    .line 521
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getTranslationLanguages(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    const/16 v0, -0x64

    .line 357
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getUniqueId(ILcom/diotek/diodict/core/engine/EntryId;)I

    move-result v0

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    const/16 v0, -0x64

    .line 251
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getVersion()I

    move-result v0

    goto :goto_0
.end method

.method public getWordmap(II)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "suid"    # I

    .prologue
    .line 408
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    const/4 v0, -0x1

    .line 411
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->getWordmap(II)I

    move-result v0

    goto :goto_0
.end method

.method public getWrapperDbType()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentDbType:I

    return v0
.end method

.method public initEngine(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 79
    .local v0, "err":I
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    const/16 v1, -0x64

    .line 88
    :goto_0
    return v1

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->initEngine(I)I

    move-result v0

    .line 84
    if-nez v0, :cond_1

    .line 85
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->setWrapperDbType(I)V

    :cond_1
    move v1, v0

    .line 88
    goto :goto_0
.end method

.method public initSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    const/16 v0, -0x64

    .line 278
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->initSpellcheck(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public isOldEngineVersion(I)Z
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "currentEngineVer":I
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getEngineType(I)I

    move-result v0

    .line 180
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValidInstance(I)Z
    .locals 1
    .param p1, "lastDbType"    # I

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->isValidInstance(I)Z

    move-result v0

    goto :goto_0
.end method

.method public searchHangulro(ILjava/lang/String;)Z
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;

    .prologue
    .line 526
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    const/4 v0, 0x0

    .line 530
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->searchHangulro(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public searchSpellcheck(Ljava/lang/String;I)I
    .locals 1
    .param p1, "searchWord"    # Ljava/lang/String;
    .param p2, "langType"    # I

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getWrapperDbType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    const/16 v0, -0x64

    .line 287
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->searchSpellcheck(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public searchWord(ILjava/lang/String;IIZ)I
    .locals 8
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    const/4 v7, 0x0

    .line 112
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return v7

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->searchWord(ILjava/lang/String;IIZ)I

    move-result v6

    .line 116
    .local v6, "listSize":I
    if-lez v6, :cond_1

    .end local v6    # "listSize":I
    :goto_1
    move v7, v6

    goto :goto_0

    .restart local v6    # "listSize":I
    :cond_1
    move v6, v7

    goto :goto_1
.end method

.method public setCurrentDbType(I)I
    .locals 2
    .param p1, "dbType"    # I

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 133
    .local v0, "err":I
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    const/16 v1, -0x64

    .line 147
    :goto_0
    return v1

    .line 137
    :cond_0
    invoke-direct {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->isNotOpenedDB(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->isValidInstance(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 139
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->initEngine(I)I

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v1, p1}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->setCurrentDbType(I)I

    move-result v0

    .line 143
    if-nez v0, :cond_2

    .line 144
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->setWrapperDbType(I)V

    :cond_2
    move v1, v0

    .line 147
    goto :goto_0
.end method

.method public setCurrentDbType(ILjava/lang/String;)I
    .locals 1
    .param p1, "dbType"    # I
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    const/16 v0, -0x64

    .line 156
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    invoke-interface {v0, p1, p2}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->setCurrentDbType(ILjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method protected setWrapperDbType(I)V
    .locals 0
    .param p1, "dbType"    # I

    .prologue
    .line 207
    iput p1, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentDbType:I

    .line 208
    return-void
.end method

.method protected switchManager(I)Z
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getCurrentManager(I)Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    .line 50
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public totalSearchWord(ILjava/lang/String;IIZ)I
    .locals 8
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "num"    # I
    .param p4, "searchlistType"    # I
    .param p5, "isNext"    # Z

    .prologue
    const/4 v7, 0x0

    .line 387
    invoke-virtual {p0, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->switchManager(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    :goto_0
    return v7

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->mCurrentManager:Lcom/diotek/diodict/core/engine/EngineManagerInterface;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/diotek/diodict/core/engine/EngineManagerInterface;->totalSearchWord(ILjava/lang/String;IIZ)I

    move-result v6

    .line 391
    .local v6, "listSize":I
    if-lez v6, :cond_1

    .end local v6    # "listSize":I
    :goto_1
    move v7, v6

    goto :goto_0

    .restart local v6    # "listSize":I
    :cond_1
    move v6, v7

    goto :goto_1
.end method
