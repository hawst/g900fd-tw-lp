.class public Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;
.super Ljava/lang/Object;
.source "DictDBManagerWrapper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    }
.end annotation


# static fields
.field private static volatile mInstance:Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    return-void
.end method

.method public static getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    if-nez v0, :cond_0

    .line 26
    const-class v1, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    monitor-enter v1

    .line 27
    :try_start_0
    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    invoke-direct {v0}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;-><init>()V

    sput-object v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    .line 28
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :cond_0
    sget-object v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->mInstance:Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    return-object v0

    .line 28
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getDBInfo(I)Lcom/diotek/diodict/core/engine/DBInfo;
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 43
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 44
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 45
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBInfo()Lcom/diotek/diodict/core/engine/DBInfo;

    move-result-object v4

    .line 48
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return-object v4

    .line 43
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getDBPath(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 188
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDBPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 196
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDictFilename(I)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 56
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 57
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 58
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDictFilename()Ljava/lang/String;

    move-result-object v4

    .line 61
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return-object v4

    .line 56
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getDictName(I)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 74
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 75
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 76
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDictName()Ljava/lang/String;

    move-result-object v4

    .line 79
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return-object v4

    .line 74
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getDictSqlFileName(I)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 65
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 66
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 67
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getSQLDBFileName()Ljava/lang/String;

    move-result-object v4

    .line 70
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return-object v4

    .line 65
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getEngineType(I)I
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 34
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 35
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 36
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getEngineVersion()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 39
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return v4

    .line 34
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public getIcuDBFileName(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 87
    const-string v0, ""

    return-object v0
.end method

.method public getLanguageWeight(I)[I
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLemmaPath(I)Ljava/lang/String;
    .locals 1
    .param p1, "dbType"    # I

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMainDBPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpellCheckDBFileName(I)Ljava/lang/String;
    .locals 5
    .param p1, "dbType"    # I

    .prologue
    .line 91
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v0

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 92
    .local v2, "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 93
    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getSpellCheckDBFileName()Ljava/lang/String;

    move-result-object v4

    .line 96
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :goto_1
    return-object v4

    .line 91
    .restart local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v2    # "item":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public setSupportDBRes([Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;)V
    .locals 0
    .param p1, "supportDBinfo"    # [Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .prologue
    .line 99
    invoke-static {p1}, Lcom/diotek/diodict4/main/DioDictSettings;->setSupportDBinfo([Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;)V

    .line 100
    return-void
.end method
