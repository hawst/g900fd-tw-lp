.class public final Lcom/diotek/diodict4/main/DioDictSettings;
.super Ljava/lang/Object;
.source "DioDictSettings.java"


# static fields
.field private static final ASSET_PATH:Ljava/lang/String; = "/android_asset/"

.field public static final DIODICT3_PREVIEW_STR_LENGTH:I = 0x1f4

.field public static final LEMMA_JMA:Ljava/lang/String; = "jma"

.field public static final LEMMA_LIB_NAME:Ljava/lang/String; = "diolemma"

.field public static final LEMMA_NLTK:Ljava/lang/String; = "nltk"

.field public static final LIB_3RD_NAME:Ljava/lang/String; = "DioDict3EngineNativeFrame"

.field public static final LIB_NAME:Ljava/lang/String; = "DioDict4EngineNativeFrame"

.field public static final STLPORT_SHARED:Ljava/lang/String; = "stlport_shared"

.field public static final USE_DARK_THEME:Z

.field public static final USE_LEMMA:Z = false

.field public static final USE_MULTI_DBPATH:Z = false

.field public static final USE_SDIC_LEMMA:Z = true

.field private static mSupportDBinfo:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 17
    const/16 v0, 0x40

    new-array v8, v0, [Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/4 v9, 0x0

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x100

    const-string v2, "CollinsEngToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Cobuild Advanced Dictionary of English"

    const/4 v5, 0x2

    const/4 v6, 0x2

    const-string v7, "CollinsEngSpellCheckDB.bin"

    invoke-direct/range {v0 .. v7}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    aput-object v0, v8, v9

    const/4 v7, 0x1

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x10c

    const-string v2, "CollinsEngToFraDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins English French Electronic Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0xc

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x2

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x10d

    const-string v2, "CollinsFraToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins English French Electronic Dictionary"

    const/16 v5, 0xc

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x3

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x10e

    const-string v2, "CollinsEngToItaDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Italian Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x10

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x4

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x10f

    const-string v2, "CollinsItaToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Italian Dictionary"

    const/16 v5, 0x10

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x5

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x108

    const-string v2, "CollinsEngToPorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Portuguese Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0xf

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x6

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x109

    const-string v2, "CollinsPorToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Portuguese Dictionary"

    const/16 v5, 0xf

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/4 v7, 0x7

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x116

    const-string v2, "CollinsEngToGreDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins English-Greek Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x24

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x8

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x117

    const-string v2, "CollinsGreToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Greek-English Dictionary"

    const/16 v5, 0x24

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x9

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x118

    const-string v2, "CollinsEngToPolDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Polish English Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x25

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xa

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x119

    const-string v2, "CollinsPolToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Polish English Dictionary"

    const/16 v5, 0x25

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xb

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x121

    const-string v2, "CollinsEngToEspUnabridgedUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Spanish Dictionary \u2013 Complete and Unabridged"

    const/4 v5, 0x2

    const/16 v6, 0xe

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xc

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x122

    const-string v2, "CollinsEspToEngUnabridgedUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins Spanish Dictionary \u2013 Complete and Unabridged"

    const/16 v5, 0xe

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xd

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x123

    const-string v2, "CollinsEngToGerUnabridgedUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins German Dictionary  \u2013 Complete and Unabridged"

    const/4 v5, 0x2

    const/16 v6, 0xd

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xe

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x124

    const-string v2, "CollinsGerToEngUnabridgedUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Collins German Dictionary  \u2013 Complete and Unabridged"

    const/16 v5, 0xd

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0xf

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x127

    const-string v2, "CollinsEngToAraDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Al-Mughni Al-Akbar English-Arabic"

    const/4 v5, 0x2

    const/16 v6, 0x19

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x10

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x128

    const-string v2, "CollinsAraToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "A. Al-Mughni Al-Wasit Arabic-English"

    const/16 v5, 0x19

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x11

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x51b

    const-string v2, "OxfordEngToMasDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford Fajar Bilingual Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x1e

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x12

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x51a

    const-string v2, "OxfordMasToEngMiniUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford Fajar Bilingual Dictionary"

    const/16 v5, 0x1e

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x13

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x200

    const-string v2, "LingvoEngToRusDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The Universal English-Russian Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x12

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x14

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x201

    const-string v2, "LingvoRusToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The Universal Russian-English Dictionary"

    const/16 v5, 0x12

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x15

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x204

    const-string v2, "LingvoEngToUkrDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The English-Ukrainian Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x13

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x16

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x205

    const-string v2, "LingvoUkrToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The Ukrainian-English Dictionary"

    const/16 v5, 0x13

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x17

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x817

    const-string v2, "BerlitzEngToTurStandardUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Berlitz Standard Dictionary Turkish"

    const/4 v5, 0x2

    const/16 v6, 0x11

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x18

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x816

    const-string v2, "BerlitzTurToEngStandardUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Berlitz Standard Dictionary Turkish"

    const/16 v5, 0x11

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x19

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa30

    const-string v2, "VandaleEngToNLDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Van Dale Dutch"

    const/4 v5, 0x2

    const/16 v6, 0x15

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1a

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa31

    const-string v2, "VandaleNLToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Van Dale Dutch"

    const/16 v5, 0x15

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1b

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa20

    const-string v2, "NorstedtsEngToSVDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Norstedts stora engelska ordbok"

    const/4 v5, 0x2

    const/16 v6, 0x17

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1c

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa21

    const-string v2, "NorstedtsSVToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Norstedts stora engelska ordbok"

    const/16 v5, 0x17

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1d

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa28

    const-string v2, "VegaForlagEngToNODictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Per Erik Kirkeby: Engelsk stor ordbok"

    const/4 v5, 0x2

    const/16 v6, 0x16

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1e

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa29

    const-string v2, "VegaForlagNOToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Per Erik Kirkeby: Engelsk stor ordbok"

    const/16 v5, 0x16

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x1f

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa24

    const-string v2, "GummerusEngToFIDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The Yellow mobile dictionary"

    const/4 v5, 0x2

    const/16 v6, 0x18

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x20

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa25

    const-string v2, "GummerusFIToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "The Yellow mobile dictionary"

    const/16 v5, 0x18

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x21

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa2a

    const-string v2, "GyldendalEngToDADictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "English-Danish General"

    const/4 v5, 0x2

    const/16 v6, 0x14

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x22

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa2b

    const-string v2, "GyldendalDAToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Danish-Englishl General"

    const/16 v5, 0x14

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x23

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa13

    const-string v2, "FnaGEngToGleDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Focl\u00f3ir P\u00f3ca - English-Irish"

    const/4 v5, 0x2

    const/16 v6, 0x26

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x24

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa14

    const-string v2, "FnaGGleToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Focl\u00f3ir P\u00f3ca - Irish-English"

    const/16 v5, 0x26

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x25

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa02

    const-string v2, "GramediaEngToIndDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Kamus Saku Inggris-Indonesia"

    const/4 v5, 0x2

    const/16 v6, 0x1a

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x26

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa03

    const-string v2, "GramediaIndToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Kamus Saku Indonesia-Inggris"

    const/16 v5, 0x1a

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x27

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa1c

    const-string v2, "DrWitEngToThaDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Dr. Wit SuperMicrosoft\u00a0Super Library English-Thai"

    const/4 v5, 0x2

    const/16 v6, 0x1c

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x28

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa1d

    const-string v2, "DrWitThaToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Dr. Wit SuperMicrosoft\u00a0Super Library Thai-English"

    const/16 v5, 0x1c

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x29

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa04

    const-string v2, "LacvietEngToVieDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "DioDict English-Vietnamese"

    const/4 v5, 0x2

    const/16 v6, 0x1b

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2a

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa05

    const-string v2, "LacvietVieToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "DioDict Vietnamese-English Dictionary"

    const/16 v5, 0x1b

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2b

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa09

    const-string v2, "StarPublicationsEngToHinDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S ENGLISH-HINDI DICTIONARY"

    const/4 v5, 0x2

    const/16 v6, 0x1f

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2c

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa0a

    const-string v2, "StarPublicationsHinToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S HINDI-ENGLISH DICTIONARY"

    const/16 v5, 0x1f

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2d

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa0f

    const-string v2, "StarPublicationsEngToPerDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S ENGLISH-FARSI DICTIONARY"

    const/4 v5, 0x2

    const/16 v6, 0x22

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2e

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa10

    const-string v2, "StarPublicationsPerToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S FARSI-ENGLISH DICTIONARY"

    const/16 v5, 0x22

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x2f

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa0d

    const-string v2, "StarPublicationsEngToUrdDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S ENGLISH-URDU DICTIONARY"

    const/4 v5, 0x2

    const/16 v6, 0x21

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x30

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xa19

    const-string v2, "StarPublicationsUrdArabicToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "STAR LEARNER\'S URDU-ENGLISH DICTIONARY"

    const/16 v5, 0x2b

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x31

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb04

    const-string v2, "NewACEEngToKorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE ENGLISH-KOREAN DICTIONARY"

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x32

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb03

    const-string v2, "NewACEKorToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE KOREAN-ENGLISH DICTIONARY"

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x33

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb05

    const-string v2, "NewACEKorToKorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE KOREAN DICTIONARY"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x34

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x702

    const-string v2, "ObunshaEngToJpnDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Obunsha Pocket Comprehensive English-Japanese Dictionary"

    const/4 v5, 0x2

    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x35

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x703

    const-string v2, "ObunshaJpnToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Obunsha Pocket Comprehensive Japanese-English Dictionary"

    const/16 v5, 0xa

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x36

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x704

    const-string v2, "ObunshaJpnToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Obunsha Pocket Comprehensive Japanese-English Dictionary"

    const/16 v5, 0xb

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x37

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb01

    const-string v2, "NewACEJpnToKorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE JAPANESE-KOREAN DICTIONARY"

    const/16 v5, 0xa

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x38

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb02

    const-string v2, "NewACEJpnToKorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE JAPANESE-KOREAN DICTIONARY"

    const/16 v5, 0xb

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x39

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xb00

    const-string v2, "NewACEKorToJpnDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "NEW-ACE KOREAN-JAPANESE DICTIONARY"

    const/4 v5, 0x0

    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3a

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xd01

    const-string v2, "MantouSimpToKorDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Mantou Chinese-Korean Dictionary"

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3b

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0xd00

    const-string v2, "MantouKorToSimpDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Mantou Korean-Chinese Dictionary"

    const/4 v5, 0x0

    const/4 v6, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3c

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x516

    const-string v2, "OxfordEngToChnDictionaryUCS2LEFLTRPSUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford\u00b7FLTRP English-Chinese Dictionary"

    const/4 v5, 0x2

    const/4 v6, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3d

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x517

    const-string v2, "OxfordChnToEngDictionaryUCS2LEFLTRPSUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford\u00b7FLTRP Chinese-English Dictionary"

    const/4 v5, 0x4

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3e

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x51c

    const-string v2, "OxfordEngToTradDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford Chinese Dictionary (Orthodox Edition)"

    const/4 v5, 0x2

    const/4 v6, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    const/16 v7, 0x3f

    new-instance v0, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    const/16 v1, 0x51d

    const-string v2, "OxfordTradToEngDictionaryUCS2LESUIDDBwH.dat"

    const/4 v3, 0x0

    const-string v4, "Oxford Chinese Dictionary (Orthodox Edition)"

    const/4 v5, 0x4

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v8, v7

    sput-object v8, Lcom/diotek/diodict4/main/DioDictSettings;->mSupportDBinfo:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .line 100
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_LIGHT_THEME:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/diotek/diodict4/main/DioDictSettings;->USE_DARK_THEME:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCSSPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    const-string v0, "/android_asset/"

    return-object v0
.end method

.method public static getDBFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DIRNAME_DICT_DB:Ljava/lang/String;

    return-object v0
.end method

.method public static getDBPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    return-object v0
.end method

.method public static getFontName(I)Ljava/lang/String;
    .locals 1
    .param p0, "enginetype"    # I

    .prologue
    .line 153
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 154
    const-string v0, "DioDictFnt3.ttf"

    .line 156
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getFontPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    const-string v0, "/android_asset/"

    return-object v0
.end method

.method public static getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/diotek/diodict4/main/DioDictSettings;->mSupportDBinfo:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    return-object v0
.end method

.method public static isDebuggable()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method
