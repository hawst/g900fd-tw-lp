.class public Lcom/sec/android/app/dictionary/util/DictUtils;
.super Ljava/lang/Object;
.source "DictUtils.java"


# static fields
.field public static final APP_TAG:Ljava/lang/String; = "dictionary:"

.field public static DEBUG:Z = false

.field public static DEFAULT_BG_COLOR:I = 0x0

.field public static DENSITY:F = 0.0f

.field public static final DIALOG_WARNING_MOBILE_DATA:I = 0x1

.field public static DIRNAME_DICT_DB:Ljava/lang/String; = null

.field public static FILE_NAME_DICT_CSS:Ljava/lang/String; = null

.field public static FILE_NAME_DICT_CSS_LIGHT:Ljava/lang/String; = null

.field public static IS_BUILD_TYPE_ENG:Z = false

.field public static IS_LIGHT_THEME:Z = false

.field public static IS_POPUP_MODE:Z = false

.field public static IS_SUPPORT_TTS:Z = false

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.dictionary"

.field public static PATH_DICT_CACHE:Ljava/lang/String; = null

.field public static PATH_DICT_CSS:Ljava/lang/String; = null

.field public static PATH_DICT_DB:Ljava/lang/String; = null

.field public static PATH_FILES_DIR:Ljava/lang/String; = null

.field public static final PATH_PRELOAD_DB:Ljava/lang/String;

.field public static final SHARED_PREF_DO_NOT_SHOW_WARNING:Ljava/lang/String; = "Dictionary_warning"

.field public static final SHARED_PREF_NAME:Ljava/lang/String; = "Dictionary"

.field private static TAG:Ljava/lang/String;

.field public static USE_MODEL_NAME:Z

.field private static dm:Landroid/util/DisplayMetrics;

.field public static final isTablet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    const-string v3, "dictionary:DictUtils"

    sput-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    .line 55
    sput-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    .line 57
    sput-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_BUILD_TYPE_ENG:Z

    .line 59
    sput-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_POPUP_MODE:Z

    .line 61
    sput-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_LIGHT_THEME:Z

    .line 63
    sput-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_SUPPORT_TTS:Z

    .line 65
    sput-boolean v1, Lcom/sec/android/app/dictionary/util/DictUtils;->USE_MODEL_NAME:Z

    .line 67
    const-string v3, "dictdb/"

    sput-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->DIRNAME_DICT_DB:Ljava/lang/String;

    .line 75
    const-string v3, "dict.css"

    sput-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->FILE_NAME_DICT_CSS:Ljava/lang/String;

    .line 77
    const-string v3, "dict_light.css"

    sput-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->FILE_NAME_DICT_CSS_LIGHT:Ljava/lang/String;

    .line 81
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/preloaddict"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_PRELOAD_DB:Ljava/lang/String;

    .line 86
    const/4 v3, -0x1

    sput v3, Lcom/sec/android/app/dictionary/util/DictUtils;->DEFAULT_BG_COLOR:I

    .line 97
    const-string v3, "ro.build.characteristics"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "tablet"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    sput-boolean v1, Lcom/sec/android/app/dictionary/util/DictUtils;->isTablet:Z

    .line 99
    return-void

    :cond_0
    move v1, v2

    .line 98
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 143
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    const/4 v4, 0x0

    .line 146
    .local v4, "files":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 152
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 153
    .local v8, "target":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 154
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 155
    sget-object v9, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mkdirs:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    array-length v9, v4

    if-nez v9, :cond_2

    .line 159
    const/4 v6, 0x0

    .line 161
    .local v6, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 166
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/dictionary/util/DictUtils;->copyFile(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 180
    .end local v6    # "in":Ljava/io/InputStream;
    .end local v8    # "target":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 147
    :catch_0
    move-exception v2

    .line 148
    .local v2, "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    const-string v10, "Failed to get asset file list."

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 162
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v6    # "in":Ljava/io/InputStream;
    .restart local v8    # "target":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 163
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to open "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 168
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "in":Ljava/io/InputStream;
    :cond_2
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v3, v0, v5

    .line 169
    .local v3, "filename":Ljava/lang/String;
    const/4 v6, 0x0

    .line 171
    .restart local v6    # "in":Ljava/io/InputStream;
    :try_start_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v6

    .line 177
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/dictionary/util/DictUtils;->copyFile(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 168
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 172
    :catch_2
    move-exception v2

    .line 173
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "is not file"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v9, v10}, Lcom/sec/android/app/dictionary/util/DictUtils;->copyAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static copyFile(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 11
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 183
    const/16 v0, 0x400

    .line 184
    .local v0, "SIZE_BUFFER":I
    const/16 v8, 0x400

    new-array v1, v8, [B

    .line 185
    .local v1, "buffer":[B
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    .local v3, "outFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 189
    .local v4, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .local v5, "outputStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v7

    .line 193
    .local v7, "total":I
    const/4 v6, 0x0

    .line 194
    .local v6, "size":I
    :goto_0
    if-lez v7, :cond_1

    .line 195
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_0

    .line 196
    invoke-virtual {v5, v1}, Ljava/io/OutputStream;->write([B)V

    .line 197
    sub-int/2addr v7, v6

    goto :goto_0

    .line 199
    :cond_0
    sget-object v8, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "copyFile() : read fail!! size="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_1
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 210
    if-eqz p0, :cond_2

    .line 211
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 215
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    .line 216
    :try_start_3
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v4, v5

    .line 220
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .end local v6    # "size":I
    .end local v7    # "total":I
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :cond_4
    :goto_2
    return-void

    .line 217
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v6    # "size":I
    .restart local v7    # "total":I
    :catch_0
    move-exception v8

    move-object v4, v5

    .line 219
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    goto :goto_2

    .line 205
    .end local v6    # "size":I
    .end local v7    # "total":I
    :catch_1
    move-exception v2

    .line 206
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    sget-object v8, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "copyFile() - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 210
    if-eqz p0, :cond_5

    .line 211
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 215
    :cond_5
    :goto_4
    if-eqz v4, :cond_4

    .line 216
    :try_start_6
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    .line 217
    :catch_2
    move-exception v8

    goto :goto_2

    .line 209
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 210
    :goto_5
    if-eqz p0, :cond_6

    .line 211
    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 215
    :cond_6
    :goto_6
    if-eqz v4, :cond_7

    .line 216
    :try_start_8
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 218
    :cond_7
    :goto_7
    throw v8

    .line 212
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v6    # "size":I
    .restart local v7    # "total":I
    :catch_3
    move-exception v8

    goto :goto_1

    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .end local v6    # "size":I
    .end local v7    # "total":I
    .restart local v2    # "e":Ljava/io/IOException;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_4
    move-exception v8

    goto :goto_4

    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    goto :goto_6

    .line 217
    :catch_6
    move-exception v9

    goto :goto_7

    .line 209
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    goto :goto_5

    .line 205
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    goto :goto_3
.end method

.method public static dpToPx(I)I
    .locals 2
    .param p0, "dp"    # I

    .prologue
    .line 134
    int-to-float v0, p0

    sget v1, Lcom/sec/android/app/dictionary/util/DictUtils;->DENSITY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static getBackgroundColor(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 272
    const/4 v1, 0x0

    .line 274
    .local v1, "defaultBgColor":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 275
    const v3, -0xedcbaa

    if-ne v1, v3, :cond_0

    .line 276
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 277
    .local v2, "value":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x1010054

    invoke-virtual {v3, v4, v2, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 278
    iget v3, v2, Landroid/util/TypedValue;->type:I

    const/16 v4, 0x1c

    if-lt v3, v4, :cond_1

    iget v3, v2, Landroid/util/TypedValue;->type:I

    const/16 v4, 0x1f

    if-gt v3, v4, :cond_1

    .line 281
    iget v1, v2, Landroid/util/TypedValue;->data:I

    .line 282
    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    const-string v4, "getBackgroundColor() type:color"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    .end local v2    # "value":Landroid/util/TypedValue;
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    const-string v4, "getBackgroundColor() value:%06X"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return v1

    .line 285
    .restart local v2    # "value":Landroid/util/TypedValue;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 286
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    instance-of v3, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v3, :cond_3

    move-object v3, v0

    .line 287
    check-cast v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v1

    .line 291
    :cond_2
    :goto_1
    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBackgroundColor() type:Drawable"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    :cond_3
    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_2

    move-object v3, v0

    .line 289
    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    goto :goto_1
.end method

.method public static getDefaultDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 307
    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    if-nez v1, :cond_0

    .line 308
    const-string v1, "window"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 309
    .local v0, "wm":Landroid/view/WindowManager;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    sput-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    .line 310
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 311
    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDefaultDisplayMetrics() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    .end local v0    # "wm":Landroid/view/WindowManager;
    :cond_0
    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->dm:Landroid/util/DisplayMetrics;

    return-object v1
.end method

.method public static getFontScale(Landroid/content/Context;)F
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 301
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->fontScale:F

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    .line 103
    const-string v0, "eng"

    const-string v1, "ro.build.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_BUILD_TYPE_ENG:Z

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_POPUP_MODE:Z

    .line 105
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_LIGHT_THEME:Z

    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_SUPPORT_TTS:Z

    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->USE_MODEL_NAME:Z

    .line 108
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DENSITY:F

    .line 109
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CACHE:Ljava/lang/String;

    .line 110
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_FILES_DIR:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->DIRNAME_DICT_DB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    .line 112
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_LIGHT_THEME:Z

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_FILES_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->FILE_NAME_DICT_CSS_LIGHT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    .line 117
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->getBackgroundColor(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEFAULT_BG_COLOR:I

    .line 118
    return-void

    .line 115
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_FILES_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->FILE_NAME_DICT_CSS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    goto :goto_0
.end method

.method public static isDataRoammingOff(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 424
    const-string v5, "ro.multisim.simslotcount"

    invoke-static {v5, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 426
    .local v1, "simslotcount":I
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 428
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    .line 429
    const-string v5, "persist.sys.dataprefer.simid"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 432
    .local v0, "dataPreferSimId":Ljava/lang/String;
    const-string v5, "0"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 433
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 441
    .end local v0    # "dataPreferSimId":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .restart local v0    # "dataPreferSimId":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 433
    goto :goto_0

    .line 434
    :cond_2
    const-string v5, "1"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 435
    invoke-static {v3}, Lcom/samsung/android/telephony/MultiSimManager;->getDataRoamingEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0

    .line 438
    .end local v0    # "dataPreferSimId":Ljava/lang/String;
    :cond_3
    if-nez v2, :cond_4

    move v3, v4

    .line 439
    goto :goto_0

    .line 441
    :cond_4
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public static isExistWebSearchActivity(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 330
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 331
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.WEB_SEARCH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 334
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 335
    const/4 v2, 0x1

    .line 337
    :cond_0
    return v2
.end method

.method public static isFlightMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 401
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isMobileDataOff(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 392
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 394
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 397
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isNoSignal(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 454
    const-string v6, "ro.multisim.simslotcount"

    invoke-static {v6, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 456
    .local v0, "simslotcount":I
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 459
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    if-ne v0, v7, :cond_6

    .line 460
    if-nez v3, :cond_1

    .line 489
    :cond_0
    :goto_0
    return v4

    .line 463
    :cond_1
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDataServiceState()I

    move-result v1

    .line 464
    .local v1, "state":I
    invoke-static {v5}, Lcom/samsung/android/telephony/MultiSimManager;->getDataServiceState(I)I

    move-result v2

    .line 466
    .local v2, "state2":I
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 468
    if-eq v1, v5, :cond_2

    if-ne v2, v5, :cond_3

    :cond_2
    move v4, v5

    .line 470
    goto :goto_0

    .line 471
    :cond_3
    if-eq v1, v7, :cond_4

    if-ne v2, v7, :cond_5

    :cond_4
    move v4, v5

    .line 473
    goto :goto_0

    :cond_5
    move v4, v5

    .line 475
    goto :goto_0

    .line 478
    .end local v1    # "state":I
    .end local v2    # "state2":I
    :cond_6
    if-eqz v3, :cond_0

    .line 481
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDataServiceState()I

    move-result v1

    .line 482
    .restart local v1    # "state":I
    if-eqz v1, :cond_0

    .line 484
    if-ne v1, v5, :cond_7

    move v4, v5

    .line 485
    goto :goto_0

    .line 486
    :cond_7
    if-ne v1, v7, :cond_8

    move v4, v5

    .line 487
    goto :goto_0

    :cond_8
    move v4, v5

    .line 489
    goto :goto_0
.end method

.method public static isReachToDataLimit(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 445
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 447
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 450
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isRoamming(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 406
    const-string v3, "ro.multisim.simslotcount"

    invoke-static {v3, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 408
    .local v2, "simslotcount":I
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 411
    .local v1, "manager":Landroid/telephony/TelephonyManager;
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 412
    const-string v3, "persist.sys.dataprefer.simid"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "dataPreferSimId":Ljava/lang/String;
    const-string v3, "0"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v3

    .line 420
    .end local v0    # "dataPreferSimId":Ljava/lang/String;
    :goto_0
    return v3

    .line 417
    .restart local v0    # "dataPreferSimId":Ljava/lang/String;
    :cond_0
    const-string v3, "1"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 418
    invoke-static {v5}, Lcom/samsung/android/telephony/MultiSimManager;->isNetworkRoaming(I)Z

    move-result v3

    goto :goto_0

    .line 420
    .end local v0    # "dataPreferSimId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v3

    goto :goto_0
.end method

.method public static isWIFIorETHERNETConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 367
    const/4 v2, 0x0

    .line 369
    .local v2, "result":Z
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 371
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 373
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 374
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 382
    const/4 v2, 0x1

    .line 387
    :cond_0
    :goto_0
    return v2

    .line 376
    :pswitch_0
    const/4 v2, 0x0

    .line 377
    goto :goto_0

    .line 374
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static lockRotation(Landroid/app/Activity;)V
    .locals 9
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 223
    const-string v3, "window"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 225
    .local v2, "windowManager":Landroid/view/WindowManager;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 226
    .local v0, "configuration":Landroid/content/res/Configuration;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 229
    .local v1, "rotation":I
    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v6, :cond_0

    if-eqz v1, :cond_1

    if-eq v1, v6, :cond_1

    :cond_0
    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_2

    if-eq v1, v4, :cond_1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_2

    .line 234
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 265
    :goto_0
    return-void

    .line 236
    :pswitch_0
    invoke-virtual {p0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-virtual {p0, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 242
    :pswitch_2
    invoke-virtual {p0, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 245
    :pswitch_3
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 250
    :cond_2
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 252
    :pswitch_4
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 255
    :pswitch_5
    invoke-virtual {p0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 258
    :pswitch_6
    invoke-virtual {p0, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 261
    :pswitch_7
    invoke-virtual {p0, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 250
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static pxToDp(I)I
    .locals 2
    .param p0, "px"    # I

    .prologue
    .line 138
    int-to-float v0, p0

    sget v1, Lcom/sec/android/app/dictionary/util/DictUtils;->DENSITY:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static releaseRotation(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 268
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 269
    return-void
.end method

.method public static showActionbarOverflowMenu(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    :try_start_0
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 123
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v2, Landroid/view/ViewConfiguration;

    const-string v3, "sHasPermanentMenuKey"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 124
    .local v1, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 125
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 126
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v1    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    return-void

    .line 128
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static startDictionary(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 342
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.dictionary.SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 348
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static startShareVia(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "menu"    # Ljava/lang/String;

    .prologue
    .line 355
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    :try_start_0
    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 360
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static startWebSearch(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 317
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 318
    .local v0, "send":Landroid/content/Intent;
    const-string v1, "new_search"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 319
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string v1, "com.android.browser.application_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    const/high16 v1, 0x10000000

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 323
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :goto_0
    return-void

    .line 324
    :catch_0
    move-exception v1

    goto :goto_0
.end method
