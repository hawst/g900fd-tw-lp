.class public Lcom/sec/android/app/dictionary/DictMeaningViewActivity;
.super Lcom/sec/android/app/dictionary/DictBaseActivity;
.source "DictMeaningViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;
    }
.end annotation


# static fields
.field protected static final MENU_TTS:I = 0x1


# instance fields
.field mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

.field mMeanLayout:Landroid/widget/FrameLayout;

.field mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

.field mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

.field mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

.field mTTS:Landroid/speech/tts/TextToSpeech;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 37
    new-instance v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;-><init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    .line 39
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 80
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    const v1, 0x7f030008

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->setContentView(I)V

    .line 83
    const v1, 0x7f0b0013

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanLayout:Landroid/widget/FrameLayout;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanLayout:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$1;-><init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 92
    const v1, 0x7f0b0014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/widget/DictWebView;

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setBackgroundColor(I)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setVerticalScrollBarEnabled(Z)V

    .line 97
    const/high16 v1, 0x42c80000    # 100.0f

    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->getFontScale(Landroid/content/Context;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 98
    .local v0, "scale":I
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFontScale : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/widget/DictWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    new-instance v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$2;-><init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->updateWebSerchButton()V

    .line 112
    sget-boolean v1, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_SUPPORT_TTS:Z

    if-eqz v1, :cond_0

    .line 113
    new-instance v1, Landroid/speech/tts/TextToSpeech;

    new-instance v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;-><init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V

    invoke-direct {v1, p0, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->parseIntent(Landroid/content/Intent;)V

    .line 136
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 170
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_SUPPORT_TTS:Z

    if-eqz v0, :cond_0

    .line 171
    const/4 v0, 0x0

    const-string v1, "TTS"

    invoke-interface {p1, v0, v2, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 173
    :cond_0
    return v2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 140
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    if-eqz v0, :cond_2

    .line 148
    sget-object v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->removeSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V

    .line 149
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onDestroy()V

    .line 150
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    .line 178
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 184
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    .line 187
    :goto_0
    return v1

    .line 180
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    const-string v2, "[(]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "keyword":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    aget-object v2, v0, v4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 187
    const/4 v1, 0x1

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 162
    const-string v0, "html"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->showView(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    const-string v0, "html"

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->getHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "text"

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 157
    return-void
.end method

.method onScrapbookClick()V
    .locals 4

    .prologue
    .line 259
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 260
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "intent.action.ACTION_ADD_TO_SCRAPBOOK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 262
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "type"

    const-string v3, "text"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v2, "title"

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v2, "app_deep_link"

    const-string v3, "appto://com.sec.android.app.dictionary/mean?"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 270
    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 271
    return-void
.end method

.method parseIntent(Landroid/content/Intent;)V
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 192
    const/4 v6, 0x0

    .line 194
    .local v6, "keyword":Ljava/lang/String;
    if-eqz p1, :cond_4

    .line 195
    const-string v12, "android.intent.action.VIEW"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 196
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    const-string v13, "keyword"

    invoke-virtual {v12, v13}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "keyword":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 220
    .restart local v6    # "keyword":Ljava/lang/String;
    :goto_0
    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    if-eqz v12, :cond_7

    .line 222
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    .line 223
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mWord:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ",mSearchKey:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v14, v14, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->searchKey:Ljava/util/ArrayList;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    sget-object v12, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v14, v14, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->searchKey:Ljava/util/ArrayList;

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v9

    .line 225
    .local v9, "meaningString":[Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 226
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v12, v9}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->append([Ljava/lang/String;)V

    .line 228
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v12, v12, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->next:[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    if-eqz v12, :cond_6

    .line 229
    const-string v12, "<div style=\'font-size:0;height:%dpx;\'></div>"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f080019

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v15

    float-to-int v15, v15

    invoke-static {v15}, Lcom/sec/android/app/dictionary/util/DictUtils;->pxToDp(I)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 233
    .local v8, "margin":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "<HR width=\'100%\' size=\'1px\' noshade />"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 234
    .local v2, "divider":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v1, v12, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->next:[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .local v1, "arr$":[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v7, :cond_6

    aget-object v5, v1, v4

    .line 235
    .local v5, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v12, v2}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->append(Ljava/lang/String;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    sget-object v13, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mWord:Ljava/lang/String;

    iget-object v15, v5, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->searchKey:Ljava/util/ArrayList;

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->append([Ljava/lang/String;)V

    .line 234
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 198
    .end local v1    # "arr$":[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .end local v2    # "divider":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .end local v7    # "len$":I
    .end local v8    # "margin":Ljava/lang/String;
    .end local v9    # "meaningString":[Ljava/lang/String;
    :cond_1
    const-string v12, "searchMgrId"

    const/4 v13, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 199
    .local v10, "mgrID":I
    const-string v12, "searchId"

    const/4 v13, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 200
    .local v11, "position":I
    const-string v12, "firstActivity"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 202
    .local v3, "firstActivity":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mgrID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ",position:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v12, -0x1

    if-le v10, v12, :cond_2

    const/4 v12, -0x1

    if-le v11, v12, :cond_2

    .line 204
    sget-object v12, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v12, v10}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getSearchListAdapter(I)Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .line 205
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    if-eqz v12, :cond_2

    .line 206
    sget-object v12, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v12, v10}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getSearchListAdapter(I)Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 210
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    if-eqz v12, :cond_3

    .line 211
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v6, v12, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->keyword:Ljava/lang/String;

    .line 213
    :cond_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->updateActionBar(Z)V

    goto/16 :goto_0

    .line 216
    .end local v3    # "firstActivity":Z
    .end local v10    # "mgrID":I
    .end local v11    # "position":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    const-string v13, "intent is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_5
    :goto_2
    return-void

    .line 240
    .restart local v9    # "meaningString":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v12}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->getHtml()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanBuffer:Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;

    invoke-virtual {v13}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->getText()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->showView(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget-boolean v12, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v12, :cond_5

    .line 243
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v13, v13, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v13, v13, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 246
    .end local v9    # "meaningString":[Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "parseIntent() skip - keyword : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method showView(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "html"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    const-string v1, "showView()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/dictionary/widget/DictWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mMeanLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    return-void
.end method
