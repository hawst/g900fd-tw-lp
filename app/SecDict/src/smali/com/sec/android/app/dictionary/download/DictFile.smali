.class public Lcom/sec/android/app/dictionary/download/DictFile;
.super Ljava/lang/Object;
.source "DictFile.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x400


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBuffer:[B

.field private mSourceZipFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "apkFile"    # Ljava/io/File;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    .line 23
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mBuffer:[B

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    .line 29
    return-void
.end method

.method private unzip(Ljava/io/File;Ljava/io/File;)Z
    .locals 12
    .param p1, "zipFile"    # Ljava/io/File;
    .param p2, "targetDir"    # Ljava/io/File;

    .prologue
    .line 113
    const/4 v2, 0x0

    .line 114
    .local v2, "fis":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 115
    .local v7, "zis":Ljava/util/zip/ZipInputStream;
    const/4 v6, 0x0

    .line 118
    .local v6, "zentry":Ljava/util/zip/ZipEntry;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/util/zip/ZipInputStream;

    invoke-direct {v8, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 121
    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .local v8, "zis":Ljava/util/zip/ZipInputStream;
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 122
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "fileNameToUnzip":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "assets"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 124
    const-string v9, "assets"

    const-string v10, ""

    invoke-virtual {v1, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 126
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 128
    .local v5, "targetFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "assets"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 129
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 130
    new-instance v4, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .local v4, "path":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-nez v9, :cond_0

    .line 132
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 144
    .end local v1    # "fileNameToUnzip":Ljava/lang/String;
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "targetFile":Ljava/io/File;
    :catch_0
    move-exception v0

    move-object v7, v8

    .end local v8    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v2, v3

    .line 145
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v0, "e":Ljava/io/IOException;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_1
    const/4 v9, 0x0

    .line 148
    if-eqz v7, :cond_2

    .line 149
    :try_start_3
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 153
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    .line 154
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 158
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :goto_3
    return v9

    .line 135
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v1    # "fileNameToUnzip":Ljava/lang/String;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "targetFile":Ljava/io/File;
    .restart local v8    # "zis":Ljava/util/zip/ZipInputStream;
    :cond_4
    :try_start_5
    new-instance v4, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .restart local v4    # "path":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-nez v9, :cond_5

    .line 137
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 139
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "extract - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-direct {p0, v8, v5}, Lcom/sec/android/app/dictionary/download/DictFile;->unzipEntry(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Ljava/io/File;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 147
    .end local v1    # "fileNameToUnzip":Ljava/lang/String;
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "targetFile":Ljava/io/File;
    :catchall_0
    move-exception v9

    move-object v7, v8

    .end local v8    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v2, v3

    .line 148
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v7, :cond_6

    .line 149
    :try_start_6
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 153
    :cond_6
    :goto_5
    if-eqz v2, :cond_7

    .line 154
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 156
    :cond_7
    :goto_6
    throw v9

    .line 148
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "zis":Ljava/util/zip/ZipInputStream;
    :cond_8
    if-eqz v8, :cond_9

    .line 149
    :try_start_8
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    .line 153
    :cond_9
    :goto_7
    if-eqz v3, :cond_a

    .line 154
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 158
    :cond_a
    :goto_8
    const/4 v9, 0x1

    move-object v7, v8

    .end local v8    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 150
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "zis":Ljava/util/zip/ZipInputStream;
    :catch_1
    move-exception v9

    goto :goto_7

    .line 155
    :catch_2
    move-exception v9

    goto :goto_8

    .line 150
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v10

    goto :goto_2

    .line 155
    :catch_4
    move-exception v10

    goto :goto_3

    .line 150
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v10

    goto :goto_5

    .line 155
    :catch_6
    move-exception v10

    goto :goto_6

    .line 147
    :catchall_1
    move-exception v9

    goto :goto_4

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 144
    :catch_7
    move-exception v0

    goto :goto_1

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v0

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private unzipEntry(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Ljava/io/File;
    .locals 5
    .param p1, "zis"    # Ljava/util/zip/ZipInputStream;
    .param p2, "targetFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    const/4 v0, 0x0

    .line 179
    .local v0, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 181
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .local v1, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 182
    .local v2, "len":I
    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mBuffer:[B

    invoke-virtual {p1, v3}, Ljava/util/zip/ZipInputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 183
    if-nez v2, :cond_1

    .line 184
    const/4 p2, 0x0

    .line 190
    .end local p2    # "targetFile":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 194
    :cond_0
    :goto_1
    return-object p2

    .line 186
    .restart local p2    # "targetFile":Ljava/io/File;
    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mBuffer:[B

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 187
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v3

    move-object v0, v1

    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "len":I
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    if-eqz v0, :cond_2

    .line 191
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    :cond_2
    throw v3

    .line 190
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "len":I
    :cond_3
    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .line 190
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "len":I
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    goto :goto_2
.end method

.method private unzipFilteredFiles(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Z
    .locals 11
    .param p1, "zipFile"    # Ljava/io/File;
    .param p2, "targetDir"    # Ljava/io/File;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v1, 0x0

    .line 70
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 71
    .local v6, "zis":Ljava/util/zip/ZipInputStream;
    const/4 v5, 0x0

    .line 72
    .local v5, "zentry":Ljava/util/zip/ZipEntry;
    const/4 v4, 0x0

    .line 75
    .local v4, "unzipCount":I
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 78
    .end local v6    # "zis":Ljava/util/zip/ZipInputStream;
    .local v7, "zis":Ljava/util/zip/ZipInputStream;
    :try_start_2
    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_0

    .line 79
    invoke-virtual {p2}, Ljava/io/File;->mkdirs()Z

    .line 81
    :cond_0
    :goto_0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 82
    new-instance v3, Ljava/io/File;

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, p2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    .local v3, "targetFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 84
    iget-object v8, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "extract - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    new-instance v8, Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/dictionary/download/DictFile;->unzipEntry(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Ljava/io/File;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 86
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "targetFile":Ljava/io/File;
    :cond_1
    if-eqz v7, :cond_2

    .line 95
    :try_start_3
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 99
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 100
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 104
    :cond_3
    :goto_2
    const/4 v8, 0x1

    if-ne v4, v8, :cond_8

    .line 105
    const/4 v8, 0x1

    move-object v6, v7

    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .line 108
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :cond_4
    :goto_3
    return v8

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/io/IOException;
    :goto_4
    :try_start_5
    iget-object v8, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failed - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 91
    const/4 v8, 0x0

    .line 94
    if-eqz v6, :cond_5

    .line 95
    :try_start_6
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 99
    :cond_5
    :goto_5
    if-eqz v1, :cond_4

    .line 100
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 101
    :catch_1
    move-exception v9

    goto :goto_3

    .line 93
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 94
    :goto_6
    if-eqz v6, :cond_6

    .line 95
    :try_start_8
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 99
    :cond_6
    :goto_7
    if-eqz v1, :cond_7

    .line 100
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 102
    :cond_7
    :goto_8
    throw v8

    .line 107
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "invalid dictionary db : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\' files found"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v8, 0x0

    move-object v6, v7

    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 96
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    :catch_2
    move-exception v8

    goto :goto_1

    .line 101
    :catch_3
    move-exception v8

    goto :goto_2

    .line 96
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "zis":Ljava/util/zip/ZipInputStream;
    :catch_4
    move-exception v9

    goto :goto_5

    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    goto :goto_7

    .line 101
    :catch_6
    move-exception v9

    goto :goto_8

    .line 93
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .line 89
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "zis":Ljava/util/zip/ZipInputStream;
    :catch_8
    move-exception v0

    move-object v6, v7

    .end local v7    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_4
.end method


# virtual methods
.method public clearCacheFile()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 45
    :cond_0
    return-void
.end method

.method public extractTo(Ljava/io/File;)Z
    .locals 4
    .param p1, "targetDir"    # Ljava/io/File;

    .prologue
    .line 32
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictFile;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveAs - source:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dest:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const/4 v0, 0x0

    .line 34
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/dictionary/download/DictFile;->unzip(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    .line 35
    return v0
.end method

.method public getPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "mPm":Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 61
    .local v1, "packageinfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 62
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 64
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getVersionNumber(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 49
    .local v0, "mPm":Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/dictionary/download/DictFile;->mSourceZipFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 51
    .local v1, "packageinfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 52
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 54
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method
