.class public Lcom/sec/android/app/dictionary/DictSearchListActivity;
.super Lcom/sec/android/app/dictionary/DictBaseActivity;
.source "DictSearchListActivity.java"


# instance fields
.field mClickListenerWordList:Landroid/widget/AdapterView$OnItemClickListener;

.field mEnable:Z

.field mMainLayout:Landroid/view/ViewGroup;

.field mNoDicationaryTextView:Landroid/widget/TextView;

.field mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

.field mSearchListLayout:Landroid/view/View;

.field mUri:Landroid/net/Uri;

.field mWordListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mUri:Landroid/net/Uri;

    .line 37
    new-instance v0, Lcom/sec/android/app/dictionary/DictSearchListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictSearchListActivity$1;-><init>(Lcom/sec/android/app/dictionary/DictSearchListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mClickListenerWordList:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->setContentView(I)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->updateActionBar(Z)V

    .line 52
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mMainLayout:Landroid/view/ViewGroup;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mMainLayout:Landroid/view/ViewGroup;

    new-instance v1, Lcom/sec/android/app/dictionary/DictSearchListActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/dictionary/DictSearchListActivity$2;-><init>(Lcom/sec/android/app/dictionary/DictSearchListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 61
    const/high16 v0, 0x7f0b0000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListLayout:Landroid/view/View;

    .line 63
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWordListView:Landroid/widget/ListView;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWordListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 66
    new-instance v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;-><init>(Lcom/sec/android/app/dictionary/DictSearchListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .line 67
    sget-object v0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->addSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWordListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWordListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mClickListenerWordList:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 72
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->updateWebSerchButton()V

    .line 77
    sget-object v0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getAvailableDictCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-class v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->startActivity(Ljava/lang/Class;)V

    .line 80
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 84
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    sget-object v0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->removeSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V

    .line 88
    invoke-super {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onDestroy()V

    .line 89
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mEnable:Z

    .line 112
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onPause()V

    .line 116
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onResume()V

    .line 100
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->parseIntent(Landroid/content/Intent;)V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mEnable:Z

    .line 106
    return-void
.end method

.method onScrapbookClick()V
    .locals 8

    .prologue
    .line 167
    iget-object v5, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 168
    .local v2, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    iget-object v3, v2, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->displayKeyword:Ljava/lang/String;

    .line 169
    .local v3, "keyword":Ljava/lang/String;
    iget-object v4, v2, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->preview:Ljava/lang/String;

    .line 170
    .local v4, "preview":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 171
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "intent.action.ACTION_ADD_TO_SCRAPBOOK"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "type"

    const-string v6, "text"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v5, "title"

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v5, "plain_text"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v5, "app_deep_link"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "appto://com.sec.android.app.dictionary/search?keyword="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->startActivity(Landroid/content/Intent;)V

    .line 180
    return-void
.end method

.method parseIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 119
    if-eqz p1, :cond_2

    .line 120
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "keyword"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWord:Ljava/lang/String;

    .line 131
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyword:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWord:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWord:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->search(Ljava/lang/String;)I

    .line 134
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->updateLayout(Z)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->notifyDataSetChanged()V

    .line 139
    .end local v0    # "action":Ljava/lang/String;
    :goto_1
    return-void

    .line 125
    .restart local v0    # "action":Ljava/lang/String;
    :cond_0
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWord:Ljava/lang/String;

    goto :goto_0

    .line 128
    :cond_1
    const-string v1, "keyword"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mWord:Ljava/lang/String;

    goto :goto_0

    .line 137
    .end local v0    # "action":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->TAG:Ljava/lang/String;

    const-string v2, "parseIntent() : intent is null !!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method updateLayout(Z)V
    .locals 4
    .param p1, "bAutoMean"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->getCount()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    :goto_0
    return-void

    .line 144
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getAvailableDictCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    const v1, 0x7f07002b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 149
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mNoDicationaryTextView:Landroid/widget/TextView;

    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 153
    :pswitch_1
    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mSearchListAdapter:Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->startMeaningActivity(Lcom/sec/android/app/dictionary/DictSearchListAdapter;IZ)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->finish()V

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
