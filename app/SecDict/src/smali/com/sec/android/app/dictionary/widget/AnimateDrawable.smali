.class public Lcom/sec/android/app/dictionary/widget/AnimateDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "AnimateDrawable.java"


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private mProxy:Landroid/graphics/drawable/Drawable;

.field private final mTransformation:Landroid/view/animation/Transformation;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "target"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 17
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    .line 21
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 27
    .local v1, "dr":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    .line 28
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 29
    .local v2, "sc":I
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    .line 30
    .local v0, "anim":Landroid/view/animation/Animation;
    if-eqz v0, :cond_0

    .line 31
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    invoke-virtual {v0, v4, v5, v3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 32
    iget-object v3, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 34
    :cond_0
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 35
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 38
    .end local v0    # "anim":Landroid/view/animation/Animation;
    .end local v2    # "sc":I
    :cond_1
    return-void
.end method

.method public getAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getProxy()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public hasEnded()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 50
    return-void
.end method

.method public setAnimation(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "anim"    # Landroid/view/animation/Animation;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    .line 70
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 56
    return-void
.end method

.method public setProxy(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "proxy"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 59
    if-eq p1, p0, :cond_0

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    .line 62
    :cond_0
    return-void
.end method
