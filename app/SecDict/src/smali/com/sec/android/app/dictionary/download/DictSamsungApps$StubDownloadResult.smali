.class public Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;
.super Ljava/lang/Object;
.source "DictSamsungApps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/download/DictSamsungApps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubDownloadResult"
.end annotation


# instance fields
.field appId:Ljava/lang/String;

.field contentSize:Ljava/lang/String;

.field downloadURI:Ljava/lang/String;

.field productId:Ljava/lang/String;

.field productName:Ljava/lang/String;

.field resultCode:Ljava/lang/String;

.field resultMsg:Ljava/lang/String;

.field versionCode:I

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->appId:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->resultCode:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->resultMsg:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->downloadURI:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->contentSize:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->versionCode:I

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->versionName:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->productId:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->productName:Ljava/lang/String;

    return-void
.end method
