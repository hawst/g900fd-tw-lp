.class public Lcom/sec/android/app/dictionary/engine/DictTextFormatter;
.super Ljava/lang/Object;
.source "DictTextFormatter.java"


# static fields
.field private static DEBUG:Z


# instance fields
.field public final CHAR:I

.field public final SPACE:I

.field private TAG:Ljava/lang/String;

.field public final ZWSP:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->TAG:Ljava/lang/String;

    .line 15
    const/16 v0, 0x200b

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->ZWSP:I

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->SPACE:I

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->CHAR:I

    return-void
.end method


# virtual methods
.method public covertPreviewText(Ljava/lang/String;)Ljava/lang/String;
    .locals 24
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 24
    .local v7, "buffer":[C
    const/16 v21, 0x7

    move/from16 v0, v21

    new-array v3, v0, [Ljava/lang/String;

    const/16 v21, 0x0

    const-string v22, "noun"

    aput-object v22, v3, v21

    const/16 v21, 0x1

    const-string v22, "n"

    aput-object v22, v3, v21

    const/16 v21, 0x2

    const-string v22, "verb"

    aput-object v22, v3, v21

    const/16 v21, 0x3

    const-string v22, "vt"

    aput-object v22, v3, v21

    const/16 v21, 0x4

    const-string v22, "vi"

    aput-object v22, v3, v21

    const/16 v21, 0x5

    const-string v22, "\ub3d9\uc0ac"

    aput-object v22, v3, v21

    const/16 v21, 0x6

    const-string v22, "\ud0c0\ub3d9\uc0ac"

    aput-object v22, v3, v21

    .line 27
    .local v3, "GrammarCategories":[Ljava/lang/String;
    const-string v4, "\u3010\u3011\u300e\u300f"

    .line 29
    .local v4, "Symbol":Ljava/lang/String;
    const-string v5, " ."

    .line 31
    .local v5, "TrimChars":Ljava/lang/String;
    const/16 v18, 0x0

    .local v18, "start":I
    const/4 v8, 0x0

    .line 32
    .local v8, "end":I
    const/16 v20, 0x0

    .local v20, "textStart":I
    const/16 v19, 0x0

    .line 33
    .local v19, "textEnd":I
    const/16 v17, 0x1

    .line 35
    .local v17, "run":Z
    sget-boolean v21, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->DEBUG:Z

    if-eqz v21, :cond_0

    .line 36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Before : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    :cond_0
    :goto_0
    if-eqz v17, :cond_9

    .line 40
    const-string v21, "\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v8

    .line 41
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v8, v0, :cond_1

    .line 42
    const/16 v17, 0x0

    .line 43
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v8, v21, -0x1

    move/from16 v19, v8

    .line 48
    :goto_1
    sub-int v13, v8, v18

    .line 51
    .local v13, "len":I
    move/from16 v20, v18

    .line 52
    move/from16 v10, v18

    .local v10, "i":I
    :goto_2
    if-ge v10, v8, :cond_3

    .line 53
    aget-char v21, v7, v10

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 54
    const/16 v21, 0x200b

    aput-char v21, v7, v10

    .line 55
    add-int/lit8 v20, v20, 0x1

    .line 56
    add-int/lit8 v13, v13, -0x1

    .line 52
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 45
    .end local v10    # "i":I
    .end local v13    # "len":I
    :cond_1
    add-int/lit8 v19, v8, -0x1

    goto :goto_1

    .line 58
    .restart local v10    # "i":I
    .restart local v13    # "len":I
    :cond_2
    aget-char v21, v7, v10

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 59
    add-int/lit8 v20, v20, 0x1

    .line 60
    add-int/lit8 v13, v13, -0x1

    goto :goto_3

    .line 68
    :cond_3
    move/from16 v10, v19

    :goto_4
    move/from16 v0, v20

    if-le v10, v0, :cond_4

    .line 69
    aget-char v21, v7, v10

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 70
    add-int/lit8 v19, v19, -0x1

    .line 71
    add-int/lit8 v13, v13, -0x1

    .line 68
    add-int/lit8 v10, v10, -0x1

    goto :goto_4

    .line 78
    :cond_4
    move-object v6, v3

    .local v6, "arr$":[Ljava/lang/String;
    array-length v14, v6

    .local v14, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_5
    if-ge v11, v14, :cond_6

    aget-object v12, v6, v11

    .line 79
    .local v12, "item":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-ne v0, v13, :cond_8

    .line 80
    const/4 v9, 0x1

    .line 81
    .local v9, "exist":Z
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    .line 82
    .local v15, "length":I
    const/4 v10, 0x0

    :goto_6
    if-ge v10, v15, :cond_5

    .line 83
    add-int v21, v20, v10

    aget-char v21, v7, v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v21

    invoke-virtual {v12, v10}, Ljava/lang/String;->charAt(I)C

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    .line 84
    const/4 v9, 0x0

    .line 88
    :cond_5
    if-eqz v9, :cond_8

    .line 89
    const/16 v21, 0x20

    aput-char v21, v7, v8

    .line 95
    .end local v9    # "exist":Z
    .end local v12    # "item":Ljava/lang/String;
    .end local v15    # "length":I
    :cond_6
    add-int/lit8 v18, v8, 0x1

    .line 96
    goto/16 :goto_0

    .line 82
    .restart local v9    # "exist":Z
    .restart local v12    # "item":Ljava/lang/String;
    .restart local v15    # "length":I
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 78
    .end local v9    # "exist":Z
    .end local v15    # "length":I
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 98
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v10    # "i":I
    .end local v11    # "i$":I
    .end local v12    # "item":Ljava/lang/String;
    .end local v13    # "len":I
    .end local v14    # "len$":I
    :cond_9
    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>([C)V

    .line 99
    .local v16, "result":Ljava/lang/String;
    sget-boolean v21, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->DEBUG:Z

    if-eqz v21, :cond_a

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "After : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_a
    return-object v16
.end method
