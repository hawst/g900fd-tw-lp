.class public Lcom/sec/android/app/dictionary/DictAboutActivity;
.super Lcom/sec/android/app/dictionary/DictBaseActivity;
.source "DictAboutActivity.java"


# instance fields
.field bottom:Ljava/lang/String;

.field head:Ljava/lang/String;

.field mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;-><init>()V

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<!DOCTYPE html PUBLIC \'-//W3C//DTD XHTML 1.1//EN\' \'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\'><html xmlns=\'http://www.w3.org/1999/xhtml\' lang=\'en\' xml:lang=\'en\'><head><meta http-equiv=\'Content-Type\' content=\'text/html;charset=utf-8\'/><style type=\'text/css\'>@font-face { font-family: \'DioDictFnt3\'; src: url(\'file:///android_asset/DioDictFnt3.ttf\'); font-weight: normal; font-style: normal;}</style><link rel=\'stylesheet\' type=\'text/css\' href=\'file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<title>About</title></head>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<body>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->head:Ljava/lang/String;

    .line 24
    const-string v0, "</body></html>"

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->bottom:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :cond_0
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictAboutActivity;->setContentView(I)V

    .line 33
    const v0, 0x7f0b0001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictAboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/widget/DictWebView;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setBackgroundColor(I)V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setVerticalScrollBarEnabled(Z)V

    .line 38
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->getFontScale(Landroid/content/Context;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v10, v0

    .line 39
    .local v10, "scale":I
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFontScale : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/DictWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 42
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 43
    .local v6, "body":Ljava/lang/StringBuffer;
    sget-object v0, Lcom/sec/android/app/dictionary/DictAboutActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getDictDataInfoMap()Landroid/util/SparseArray;

    move-result-object v8

    .line 44
    .local v8, "dictMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v11

    .line 45
    .local v11, "size":I
    if-lez v11, :cond_3

    .line 46
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v11, :cond_3

    .line 47
    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 48
    .local v7, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v0, v7, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mCopyRight:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_2

    .line 49
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 50
    const-string v0, "<BR/><BR/>"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    :cond_1
    iget-object v0, v7, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mCopyRight:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 57
    .end local v7    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v9    # "i":I
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->mWebView:Lcom/sec/android/app/dictionary/widget/DictWebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictAboutActivity;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->head:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictAboutActivity;->bottom:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/dictionary/widget/DictWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method
