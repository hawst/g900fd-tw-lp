.class Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;
.super Ljava/lang/Object;
.source "ListViewWidget.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForDrag(Landroid/graphics/Bitmap;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

.field final synthetic val$dragAnimation:Lcom/sec/android/app/dictionary/widget/MoveAnimation;

.field final synthetic val$localRect:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;Lcom/sec/android/app/dictionary/widget/MoveAnimation;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    iput-object p2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->val$dragAnimation:Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    iput-object p3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->val$localRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->val$dragAnimation:Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setScale(FFFF)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->val$dragAnimation:Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->startNow()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;->val$localRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->invalidate(Landroid/graphics/Rect;)V

    .line 331
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 335
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 339
    return-void
.end method
