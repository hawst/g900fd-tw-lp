.class Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;
.super Ljava/lang/Object;
.source "DictDownloadListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/DictDownloadListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 68
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$000(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setChangeOrderMode(Z)V

    .line 69
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onViewModeChanged()V

    goto :goto_0

    .line 73
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->getChangeOrderList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/dictionary/db/DictDbManager;->save(Ljava/util/List;Z)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$000(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setChangeOrderMode(Z)V

    .line 75
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onViewModeChanged()V

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0004
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
