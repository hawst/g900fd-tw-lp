.class Lcom/sec/android/app/dictionary/widget/DictWebView$2;
.super Ljava/lang/Object;
.source "DictWebView.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/widget/DictWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mMenu:[I

.field final synthetic this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getMenuPos(I)I
    .locals 2
    .param p1, "menuId"    # I

    .prologue
    .line 95
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->mMenu:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->mMenu:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 99
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 95
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/MenuItem;

    .prologue
    .line 134
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->getMenuPos(I)I

    move-result v0

    .line 135
    .local v0, "pos":I
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "title":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v2, p1, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v2

    .line 171
    :goto_0
    return v2

    .line 140
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    const-string v3, "window.getSelection().toString();"

    new-instance v4, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;

    invoke-direct {v4, p0, v0, v1}, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView$2;ILjava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 164
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 171
    const/4 v2, 0x1

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 114
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v4, v4, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v4, p1, p2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v2

    .line 115
    .local v2, "result":Z
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v4, v4, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreateActionMode() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-interface {p2}, Landroid/view/Menu;->size()I

    move-result v3

    .line 118
    .local v3, "size":I
    new-array v4, v3, [I

    iput-object v4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->mMenu:[I

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 121
    invoke-interface {p2, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 122
    .local v1, "item":Landroid/view/MenuItem;
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->mMenu:[I

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    aput v5, v4, v0

    .line 123
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 124
    sget-boolean v4, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 125
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v4, v4, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_1
    return v2
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 110
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method removeMenu(Landroid/view/Menu;I)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "pos"    # I

    .prologue
    .line 90
    invoke-interface {p1, p2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 91
    .local v0, "item":Landroid/view/MenuItem;
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 92
    return-void
.end method
