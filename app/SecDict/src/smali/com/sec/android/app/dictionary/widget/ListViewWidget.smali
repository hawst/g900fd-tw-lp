.class public Lcom/sec/android/app/dictionary/widget/ListViewWidget;
.super Landroid/widget/ListView;
.source "ListViewWidget.java"

# interfaces
.implements Landroid/view/View$OnDragListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;,
        Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;
    }
.end annotation


# static fields
.field private static mSrcDragPos:I


# instance fields
.field private mChangeOrderMode:Z

.field private mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

.field private mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

.field private mDragPos:I

.field private mDragY:I

.field private mDragging:Z

.field private mItemHeight:I

.field private mItemWidth:I

.field private mLowerBound:I

.field private mOffsetXInDraggingItem:I

.field private mOffsetYInDraggingItem:I

.field private mOnMoveListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

.field private final mScrollRunnable:Ljava/lang/Runnable;

.field private mUpperBound:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance v0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$1;-><init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mScrollRunnable:Ljava/lang/Runnable;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doScroll()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragging:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->scroll()V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/app/dictionary/widget/ListViewWidget;Lcom/sec/android/app/dictionary/widget/AnimateDrawable;)Lcom/sec/android/app/dictionary/widget/AnimateDrawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;
    .param p1, "x1"    # Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOnMoveListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    return v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    return v0
.end method

.method static synthetic access$602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 24
    sput p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    return p0
.end method

.method static synthetic access$700(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->clearChildViewsAnimation()V

    return-void
.end method

.method private adjustScrollBounds()V
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mUpperBound:I

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getBottom()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mLowerBound:I

    .line 160
    return-void
.end method

.method private clearChildViewsAnimation()V
    .locals 4

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildCount()I

    move-result v2

    .line 403
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 404
    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 405
    .local v1, "localView":Landroid/view/View;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 406
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 403
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 408
    .end local v1    # "localView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private createAnimation(IIII)Landroid/view/animation/Animation;
    .locals 5
    .param p1, "fromX"    # I
    .param p2, "toX"    # I
    .param p3, "fromY"    # I
    .param p4, "toY"    # I

    .prologue
    .line 294
    new-instance v0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method private doAnimationForDrag(Landroid/graphics/Bitmap;II)V
    .locals 8
    .param p1, "paramBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 314
    iget v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetXInDraggingItem:I

    sub-int v1, p2, v4

    .line 315
    .local v1, "i":I
    iget v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetYInDraggingItem:I

    sub-int v2, p3, v4

    .line 316
    .local v2, "j":I
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemWidth:I

    add-int/2addr v4, v1

    iget v5, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    add-int/2addr v5, v2

    invoke-direct {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 317
    .local v3, "localRect":Landroid/graphics/Rect;
    new-instance v4, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v5, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v4, v5}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    .line 318
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v4}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 319
    new-instance v0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    invoke-direct {v0, v6, v6, v6, v6}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;-><init>(FFFF)V

    .line 320
    .local v0, "dragAnimation":Lcom/sec/android/app/dictionary/widget/MoveAnimation;
    const v4, 0x3f8ccccd    # 1.1f

    invoke-virtual {v0, v7, v7, v7, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setScale(FFFF)V

    .line 321
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setPivot(FF)V

    .line 322
    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setDuration(J)V

    .line 323
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setFillAfter(Z)V

    .line 324
    new-instance v4, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;

    invoke-direct {v4, p0, v0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$3;-><init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;Lcom/sec/android/app/dictionary/widget/MoveAnimation;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 342
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->setAnimation(Landroid/view/animation/Animation;)V

    .line 343
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->startNow()V

    .line 344
    invoke-virtual {p0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->invalidate(Landroid/graphics/Rect;)V

    .line 345
    return-void
.end method

.method private doAnimationForDrop()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 234
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    if-eqz v4, :cond_0

    .line 235
    const/4 v2, 0x0

    .line 236
    .local v2, "x":F
    iget v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v5}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v3, v4, v5

    .line 237
    .local v3, "y":I
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v4}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 238
    .local v1, "localRect":Landroid/graphics/Rect;
    new-instance v0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    int-to-float v4, v3

    invoke-direct {v0, v7, v2, v7, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;-><init>(FFFF)V

    .line 239
    .local v0, "dropAnimation":Lcom/sec/android/app/dictionary/widget/MoveAnimation;
    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setDuration(J)V

    .line 240
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setFillAfter(Z)V

    .line 241
    invoke-virtual {v0, v6, v6, v6, v6}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setScale(FFFF)V

    .line 242
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setPivot(FF)V

    .line 243
    new-instance v4, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;-><init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 265
    iget-object v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->setAnimation(Landroid/view/animation/Animation;)V

    .line 266
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->startNow()V

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->invalidate()V

    .line 269
    .end local v0    # "dropAnimation":Lcom/sec/android/app/dictionary/widget/MoveAnimation;
    .end local v1    # "localRect":Landroid/graphics/Rect;
    .end local v2    # "x":F
    .end local v3    # "y":I
    :cond_0
    return-void
.end method

.method private doAnimationForMove(II)V
    .locals 5
    .param p1, "dragPos"    # I
    .param p2, "movePos"    # I

    .prologue
    const/4 v4, 0x0

    .line 208
    sget v1, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-lt v1, v2, :cond_1

    if-ge p1, p2, :cond_1

    .line 209
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v1, v4, v2, v4}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 212
    .local v0, "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    .line 231
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    sget v1, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-ge v1, v2, :cond_2

    if-ge p1, p2, :cond_2

    .line 215
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v4, v1, v4, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 218
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto :goto_0

    .line 220
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_2
    sget v1, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-gt v1, v2, :cond_3

    if-le p1, p2, :cond_3

    .line 221
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 222
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v1, v4, v2, v4}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 224
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto :goto_0

    .line 227
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_3
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v4, v1, v4, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 229
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private doAnimationForUpdate(I)V
    .locals 2
    .param p1, "movePos"    # I

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    .line 185
    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-ge v1, p1, :cond_1

    .line 186
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-ge v1, p1, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 188
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    .line 189
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForMove(II)V

    goto :goto_0

    .line 193
    :cond_1
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-le v1, p1, :cond_2

    .line 194
    :goto_1
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    if-le v1, p1, :cond_2

    .line 195
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    .line 196
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForMove(II)V

    goto :goto_1

    .line 199
    :cond_2
    return-void
.end method

.method private doScroll()Z
    .locals 13

    .prologue
    const/16 v12, 0x10

    const/4 v8, 0x0

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getLastVisiblePosition()I

    move-result v3

    .line 359
    .local v3, "lastVisiblePosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v1

    .line 360
    .local v1, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getCount()I

    move-result v0

    .line 361
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getListPaddingBottom()I

    move-result v5

    .line 363
    .local v5, "paddingBottom":I
    const/4 v6, 0x0

    .line 364
    .local v6, "ret":Z
    iget v9, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mLowerBound:I

    if-le v9, v10, :cond_4

    .line 365
    add-int/lit8 v9, v0, -0x1

    if-ne v3, v9, :cond_1

    sub-int v9, v3, v1

    invoke-virtual {p0, v9}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getHeight()I

    move-result v10

    sub-int/2addr v10, v5

    if-gt v9, v10, :cond_1

    .line 398
    :cond_0
    :goto_0
    return v8

    .line 370
    :cond_1
    iget v9, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mLowerBound:I

    iget v11, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    div-int/lit8 v11, v11, 0x4

    add-int/2addr v10, v11

    if-le v9, v10, :cond_3

    .line 371
    const/16 v7, 0xc

    .line 375
    .local v7, "scrollDistance":I
    :goto_1
    invoke-virtual {p0, v7, v12}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->smoothScrollBy(II)V

    .line 376
    const/4 v6, 0x1

    .line 389
    .end local v7    # "scrollDistance":I
    :cond_2
    :goto_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    sub-int v9, v3, v1

    if-ge v2, v9, :cond_8

    .line 390
    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 391
    .local v4, "localView":Landroid/view/View;
    sget v9, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    add-int v10, v2, v1

    if-ne v9, v10, :cond_7

    .line 392
    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 393
    const/4 v9, 0x4

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 389
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 373
    .end local v2    # "i":I
    .end local v4    # "localView":Landroid/view/View;
    :cond_3
    const/4 v7, 0x4

    .restart local v7    # "scrollDistance":I
    goto :goto_1

    .line 377
    .end local v7    # "scrollDistance":I
    :cond_4
    iget v9, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mUpperBound:I

    if-ge v9, v10, :cond_2

    .line 378
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v9

    if-nez v9, :cond_5

    invoke-virtual {p0, v8}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getListPaddingTop()I

    move-result v10

    if-ge v9, v10, :cond_0

    .line 381
    :cond_5
    iget v9, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mUpperBound:I

    iget v11, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    div-int/lit8 v11, v11, 0x4

    sub-int/2addr v10, v11

    if-ge v9, v10, :cond_6

    .line 382
    const/16 v7, -0xc

    .line 386
    .restart local v7    # "scrollDistance":I
    :goto_5
    invoke-virtual {p0, v7, v12}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->smoothScrollBy(II)V

    .line 387
    const/4 v6, 0x1

    goto :goto_2

    .line 384
    .end local v7    # "scrollDistance":I
    :cond_6
    const/4 v7, -0x4

    .restart local v7    # "scrollDistance":I
    goto :goto_5

    .line 395
    .end local v7    # "scrollDistance":I
    .restart local v2    # "i":I
    .restart local v4    # "localView":Landroid/view/View;
    :cond_7
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .end local v4    # "localView":Landroid/view/View;
    :cond_8
    move v8, v6

    .line 398
    goto :goto_0
.end method

.method private dragDrawable(II)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 285
    .local v0, "i":I
    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetYInDraggingItem:I

    sub-int v1, p2, v3

    .line 286
    .local v1, "j":I
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemWidth:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    add-int/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 287
    .local v2, "localRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v3}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v3}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setPivot(FF)V

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->invalidate()V

    .line 291
    return-void
.end method

.method private getItemForPosition(II)I
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 163
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetXInDraggingItem:I

    sub-int v6, p1, v6

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemWidth:I

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 164
    .local v4, "xPos":I
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetYInDraggingItem:I

    sub-int v6, p2, v6

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 166
    .local v5, "yPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildCount()I

    move-result v3

    .line 167
    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "pos":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 168
    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 170
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 171
    .local v1, "localRect":Landroid/graphics/Rect;
    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 172
    iget v6, v1, Landroid/graphics/Rect;->right:I

    add-int/lit16 v6, v6, 0x2710

    iput v6, v1, Landroid/graphics/Rect;->right:I

    .line 173
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 177
    .end local v0    # "itemView":Landroid/view/View;
    .end local v1    # "localRect":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildCount()I

    move-result v6

    if-lt v2, v6, :cond_2

    .line 178
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    .line 180
    :goto_1
    return v6

    .line 167
    .restart local v0    # "itemView":Landroid/view/View;
    .restart local v1    # "localRect":Landroid/graphics/Rect;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "itemView":Landroid/view/View;
    .end local v1    # "localRect":Landroid/graphics/Rect;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v6

    add-int/2addr v6, v2

    goto :goto_1
.end method

.method private getLeft(I)I
    .locals 1
    .param p1, "itemPos"    # I

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method private getTop(I)I
    .locals 3
    .param p1, "itemPos"    # I

    .prologue
    .line 276
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 277
    .local v0, "firstRowTop":I
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v2

    sub-int v1, p1, v2

    .line 280
    .local v1, "row":I
    iget v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    mul-int/2addr v2, v1

    add-int/2addr v2, v0

    return v2
.end method

.method private reverseAnimation(I)Z
    .locals 3
    .param p1, "paramInt"    # I

    .prologue
    .line 348
    const/4 v1, 0x0

    .line 349
    .local v1, "ret":Z
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 350
    .local v0, "moveAnimation":Landroid/view/animation/Animation;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    check-cast v0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;

    .end local v0    # "moveAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->reverseAnimation()V

    .line 352
    const/4 v1, 0x1

    .line 354
    :cond_0
    return v1
.end method

.method private scroll()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->post(Ljava/lang/Runnable;)Z

    .line 204
    return-void
.end method

.method public static setSrcDragPos(I)V
    .locals 0
    .param p0, "srcDragPos"    # I

    .prologue
    .line 145
    sput p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I

    .line 146
    return-void
.end method

.method private startAnimationByPosition(ILandroid/view/animation/Animation;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 299
    .local v0, "localView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {v0, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 302
    :cond_0
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 447
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/AnimateDrawable;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->invalidate()V

    .line 455
    :cond_0
    return-void
.end method

.method public isChangeOrderDragging()Z
    .locals 1

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mChangeOrderMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChangeOrderMode()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mChangeOrderMode:Z

    return v0
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 424
    const/4 v0, 0x0

    return v0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 442
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 430
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 434
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I

    .prologue
    .line 438
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v10

    if-eqz v10, :cond_4

    iget-boolean v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragging:Z

    if-ne v10, v9, :cond_4

    .line 82
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_0
    :goto_0
    move v8, v9

    .line 132
    :cond_1
    :goto_1
    return v8

    .line 84
    :pswitch_0
    iget-object v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    if-eqz v10, :cond_2

    .line 85
    iget-object v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    invoke-interface {v10}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;->startDrag()V

    .line 87
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v6, v10

    .line 88
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v7, v10

    .line 89
    .local v7, "y":I
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->pointToPosition(II)I

    move-result v5

    .line 90
    .local v5, "pos":I
    const/4 v10, -0x1

    if-le v5, v10, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getFirstVisiblePosition()I

    move-result v10

    sub-int v10, v5, v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 94
    .local v3, "localView":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setItemSize(II)V

    .line 95
    invoke-virtual {p0, v9}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setDragging(Z)V

    .line 96
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int v10, v6, v10

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v11

    sub-int v11, v7, v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setOffsetInDraggingItem(II)V

    .line 98
    invoke-virtual {v3, v9}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 99
    invoke-virtual {v3, v9}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 100
    invoke-virtual {v3}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 101
    .local v2, "localBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 103
    invoke-direct {p0, v2, v6, v7}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForDrag(Landroid/graphics/Bitmap;II)V

    .line 104
    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 105
    invoke-static {v5}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setSrcDragPos(I)V

    .line 106
    invoke-virtual {p0, v5}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setDragPos(I)V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->adjustScrollBounds()V

    goto :goto_0

    .line 110
    .end local v2    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "localView":Landroid/view/View;
    .end local v5    # "pos":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v0, v8

    .line 111
    .local v0, "i":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v1, v8

    .line 112
    .local v1, "j":I
    iput v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    .line 113
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->dragDrawable(II)V

    .line 114
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getItemForPosition(II)I

    move-result v4

    .line 115
    .local v4, "movePos":I
    invoke-direct {p0, v4}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForUpdate(I)V

    .line 116
    iget v8, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mUpperBound:I

    if-lt v8, v10, :cond_3

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragY:I

    iget v10, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mLowerBound:I

    if-le v8, v10, :cond_0

    .line 117
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->scroll()V

    goto/16 :goto_0

    .line 122
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v4    # "movePos":I
    :pswitch_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setDragging(Z)V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForDrop()V

    .line 124
    iget-object v8, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    if-eqz v8, :cond_0

    .line 125
    iget-object v8, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    invoke-interface {v8}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;->endDrag()V

    goto/16 :goto_0

    .line 132
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto/16 :goto_1

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 24
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 459
    if-nez p1, :cond_0

    .line 477
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 463
    new-instance v0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$4;-><init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public setChangeOrderMode(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 411
    iput-boolean p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mChangeOrderMode:Z

    .line 412
    return-void
.end method

.method public setDragListener(Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;

    .line 310
    return-void
.end method

.method public setDragPos(I)V
    .locals 0
    .param p1, "dragPos"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I

    .line 150
    return-void
.end method

.method public setDragging(Z)V
    .locals 0
    .param p1, "dragging"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragging:Z

    .line 142
    return-void
.end method

.method public setItemSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemWidth:I

    .line 137
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mItemHeight:I

    .line 138
    return-void
.end method

.method public setOffsetInDraggingItem(II)V
    .locals 0
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetXInDraggingItem:I

    .line 154
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOffsetYInDraggingItem:I

    .line 155
    return-void
.end method

.method public setOnMoveListener(Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;)V
    .locals 0
    .param p1, "moveListener"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOnMoveListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

    .line 306
    return-void
.end method
