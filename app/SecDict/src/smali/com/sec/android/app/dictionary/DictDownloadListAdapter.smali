.class public Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DictDownloadListAdapter.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private isNeedUpdateList:Z

.field private mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

.field private mAvailableHeader:Landroid/view/View;

.field mChangeOrderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

.field private mChangeOrderItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDownItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadedHeader:Landroid/view/View;

.field mHeaderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

.field private mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

.field private mMyItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0b0006

    const v4, 0x7f030003

    .line 89
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dictionary:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;

    .line 33
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    .line 37
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    .line 49
    new-instance v2, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$1;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)V

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mHeaderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 69
    new-instance v2, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)V

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 94
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v0, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownloadedHeader:Landroid/view/View;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownloadedHeader:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 96
    .local v1, "tv":Landroid/widget/TextView;
    const v2, 0x7f070028

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownloadedHeader:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mHeaderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v2, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 99
    invoke-virtual {v0, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mAvailableHeader:Landroid/view/View;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mAvailableHeader:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "tv":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 101
    .restart local v1    # "tv":Landroid/widget/TextView;
    const v2, 0x7f070029

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mAvailableHeader:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mHeaderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v2, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Lcom/sec/android/app/dictionary/DictDownloadListActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Lcom/sec/android/app/dictionary/widget/ListViewWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    return-object v0
.end method


# virtual methods
.method public canChangeOrder()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeOrderItem(II)Z
    .locals 6
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 328
    const/4 v1, 0x0

    .line 329
    .local v1, "isOrderChanged":Z
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 330
    .local v2, "length":I
    const/4 v0, 0x0

    .line 331
    .local v0, "idx":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 333
    .local v3, "srcItem":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v4, p2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 336
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v5, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v4, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    if-eq v5, v4, :cond_1

    .line 338
    const/4 v1, 0x1

    .line 343
    :cond_0
    return v1

    .line 336
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChangeOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    return-object v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 113
    .local v0, "total":I
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    if-nez v2, :cond_0

    move v1, v0

    .line 123
    .end local v0    # "total":I
    .local v1, "total":I
    :goto_0
    return v1

    .line 117
    .end local v1    # "total":I
    .restart local v0    # "total":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    move v1, v0

    .line 123
    .end local v0    # "total":I
    .restart local v1    # "total":I
    goto :goto_0

    .line 120
    .end local v1    # "total":I
    .restart local v0    # "total":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 129
    .local v1, "downSize":I
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 131
    .local v0, "availSize":I
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    if-nez v3, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-object v2

    .line 135
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 138
    :cond_2
    if-eqz p1, :cond_0

    add-int/lit8 v3, v1, 0x1

    if-eq p1, v3, :cond_0

    .line 140
    add-int/lit8 v3, v1, 0x1

    if-ge p1, v3, :cond_3

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 142
    :cond_3
    add-int/lit8 v3, v1, 0x1

    if-le p1, v3, :cond_0

    add-int v3, v1, v0

    add-int/lit8 v3, v3, 0x2

    if-ge p1, v3, :cond_0

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    sub-int v3, p1, v1

    add-int/lit8 v3, v3, -0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 151
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 156
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    if-nez v10, :cond_2

    .line 157
    :cond_0
    const/4 v9, 0x0

    .line 247
    :cond_1
    :goto_0
    return-object v9

    .line 159
    :cond_2
    move-object/from16 v9, p2

    .line 160
    .local v9, "view":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 161
    .local v3, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 163
    .local v4, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v10}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 164
    const v10, 0x7f030006

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 165
    const v10, 0x7f0b0008

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 166
    .local v7, "tv":Landroid/widget/TextView;
    const v10, 0x7f0b0011

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 167
    .local v2, "imgView":Landroid/widget/ImageView;
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    new-instance v10, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$3;

    invoke-direct {v10, p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$3;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)V

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v9, v10}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 179
    .end local v2    # "imgView":Landroid/widget/ImageView;
    .end local v7    # "tv":Landroid/widget/TextView;
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    .line 180
    .local v1, "downSize":I
    if-eqz p1, :cond_4

    add-int/lit8 v10, v1, 0x1

    if-ne p1, v10, :cond_6

    .line 181
    :cond_4
    if-nez p1, :cond_5

    .line 182
    iget-object v9, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownloadedHeader:Landroid/view/View;

    goto :goto_0

    .line 184
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mAvailableHeader:Landroid/view/View;

    goto :goto_0

    .line 186
    :cond_6
    add-int/lit8 v10, v1, 0x1

    if-ge p1, v10, :cond_9

    .line 187
    if-eqz v9, :cond_7

    .line 188
    const v10, 0x7f0b000f

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-nez v10, :cond_7

    .line 189
    const/4 v9, 0x0

    .line 191
    :cond_7
    if-nez v9, :cond_8

    .line 192
    const v10, 0x7f030005

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 193
    :cond_8
    const v10, 0x7f0b0008

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 194
    .restart local v7    # "tv":Landroid/widget/TextView;
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    const v10, 0x7f0b0009

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 197
    .local v8, "tv2":Landroid/widget/TextView;
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mName:Ljava/lang/String;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const v10, 0x7f0b000c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 201
    .local v5, "layout":Landroid/widget/FrameLayout;
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onButtonLayoutTouch:Landroid/view/View$OnTouchListener;

    invoke-virtual {v5, v10}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 203
    const v10, 0x7f0b000d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 204
    .local v0, "btn":Landroid/widget/ImageButton;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 205
    iget-object v10, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onDeleteClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const v10, 0x7f0b000e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    goto/16 :goto_0

    .line 209
    .end local v0    # "btn":Landroid/widget/ImageButton;
    .end local v5    # "layout":Landroid/widget/FrameLayout;
    .end local v7    # "tv":Landroid/widget/TextView;
    .end local v8    # "tv2":Landroid/widget/TextView;
    :cond_9
    add-int/lit8 v10, v1, 0x1

    if-le p1, v10, :cond_1

    .line 210
    if-eqz v9, :cond_a

    .line 211
    const v10, 0x7f0b0007

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-nez v10, :cond_a

    .line 212
    const/4 v9, 0x0

    .line 214
    :cond_a
    if-nez v9, :cond_b

    .line 215
    const v10, 0x7f030004

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 218
    :cond_b
    const v10, 0x7f0b0008

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 219
    .restart local v7    # "tv":Landroid/widget/TextView;
    const-string v6, ""

    .line 220
    .local v6, "sizeStr":Ljava/lang/String;
    iget-wide v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_c

    .line 221
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-wide v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    invoke-static {v11, v12, v13}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 222
    :cond_c
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v11, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    monitor-enter v11

    .line 225
    :try_start_0
    iget-object v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    const v10, 0x7f0b0009

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v12, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    .line 227
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    iget v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 229
    iget-object v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    const v10, 0x7f0b000c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/FrameLayout;

    iput-object v10, v12, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButtonLayout:Landroid/widget/FrameLayout;

    .line 231
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButtonLayout:Landroid/widget/FrameLayout;

    iget-object v12, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v12, v12, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onButtonLayoutTouch:Landroid/view/View$OnTouchListener;

    invoke-virtual {v10, v12}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 234
    iget-object v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    const v10, 0x7f0b000d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, v12, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    .line 236
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 237
    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v10, v10, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    iget-object v12, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v12, v12, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onDownloadClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v12}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    const v10, 0x7f0b000a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, v12, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressLayout:Landroid/widget/LinearLayout;

    .line 241
    iget-object v12, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    const v10, 0x7f0b000b

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ProgressBar;

    iput-object v10, v12, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressBar:Landroid/widget/ProgressBar;

    .line 243
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    invoke-virtual {p0, v4}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateDownloadProgress(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    goto/16 :goto_0

    .line 243
    :catchall_0
    move-exception v10

    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10
.end method

.method public makeChangeOrderItems()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 361
    return-void
.end method

.method public setListView(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V
    .locals 0
    .param p1, "list"    # Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .line 107
    return-void
.end method

.method public updateChangeOrderView()V
    .locals 2

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->isNeedUpdateList:Z

    if-eqz v0, :cond_0

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->isNeedUpdateList:Z

    .line 276
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 281
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;

    const-string v1, "updateChangeOrderView() : Skip - isNeedUpdateList is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public updateDownloadProgress(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    .line 251
    iget-object v1, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    monitor-enter v1

    .line 252
    :try_start_0
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    if-ne v0, v2, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->isDownloading()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 257
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressBar:Landroid/widget/ProgressBar;

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget-wide v2, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 258
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressBar:Landroid/widget/ProgressBar;

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget-wide v2, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedSize:J

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 260
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 261
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    const/high16 v2, 0x7f020000

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 270
    :cond_0
    :goto_0
    monitor-exit v1

    .line 271
    return-void

    .line 263
    :cond_1
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownload2ndTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadProgressLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 266
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 267
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mView:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo$DictView;->mDownloadButton:Landroid/widget/ImageButton;

    const v2, 0x7f020007

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateView(Z)Z
    .locals 8
    .param p1, "update"    # Z

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "bChanged":Z
    if-eqz p1, :cond_6

    .line 287
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v6}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderDragging()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 288
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->isNeedUpdateList:Z

    .line 289
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;

    const-string v7, "updateList() : Skip - Dragging..."

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 324
    .end local v0    # "bChanged":Z
    .local v1, "bChanged":I
    :goto_0
    return v1

    .line 293
    .end local v1    # "bChanged":I
    .restart local v0    # "bChanged":Z
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;

    const-string v7, "updateList()"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    sget-object v6, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v6}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->updateDictList()Ljava/util/List;

    move-result-object v3

    .line 296
    .local v3, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 299
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 300
    .local v5, "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-boolean v6, v5, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v6, :cond_1

    .line 301
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 303
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 307
    .end local v5    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mListView:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v6}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 308
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mMyItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 309
    .local v2, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 310
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v7, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 311
    const/4 v0, 0x1

    goto :goto_2

    .line 314
    .end local v2    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mDownItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 315
    .restart local v2    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 316
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mChangeOrderItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 317
    const/4 v0, 0x1

    goto :goto_3

    .line 323
    .end local v2    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v3    # "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    move v1, v0

    .line 324
    .restart local v1    # "bChanged":I
    goto/16 :goto_0
.end method
