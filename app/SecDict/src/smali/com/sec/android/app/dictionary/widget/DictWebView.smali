.class public Lcom/sec/android/app/dictionary/widget/DictWebView;
.super Landroid/webkit/WebView;
.source "DictWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
    }
.end annotation


# instance fields
.field TAG:Ljava/lang/String;

.field mClipboard:Landroid/content/ClipboardManager;

.field mCovertMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;",
            ">;"
        }
    .end annotation
.end field

.field mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

.field mDictActionModeCallback:Landroid/view/ActionMode$Callback;

.field mDictWebViewContext:Landroid/content/Context;

.field mDisableLongClick:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDisableLongClick:Z

    .line 85
    new-instance v0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/widget/DictWebView$2;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/widget/DictWebView;->init(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDisableLongClick:Z

    .line 85
    new-instance v0, Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/widget/DictWebView$2;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/widget/DictWebView;->init(Landroid/content/Context;)V

    .line 41
    return-void
.end method


# virtual methods
.method init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictWebViewContext:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/DictWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 47
    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mClipboard:Landroid/content/ClipboardManager;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x200b

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, ""

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x26a

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "I"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x274

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "N"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x280

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "R"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x28f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "Y"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x29c

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "H"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x29f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "L"

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x1d00

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "A"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x1d05

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "D"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x1d07

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "E"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x1d0d

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "M"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x1d0f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "O"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x2071

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "(1)"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x2072

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "(2)"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const/16 v1, 0x2610

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "\u25a1"

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xd7b3

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, ""

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xa45d

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, ""

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xe060

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "\u0254"

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xe0ae

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, ""

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xe0b8

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, ""

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xe0be

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "F"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    const v1, 0xe0c0

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    const-string v3, "S"

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDisableLongClick:Z

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lcom/sec/android/app/dictionary/widget/DictWebView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/widget/DictWebView$1;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/widget/DictWebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 81
    :cond_0
    return-void
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1
    .param p1, "callback"    # Landroid/view/ActionMode$Callback;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDefaultActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-super {p0, v0}, Landroid/webkit/WebView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    return-object v0
.end method

.method unescapeJava(Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 212
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 213
    .local v14, "text":Ljava/lang/StringBuffer;
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 214
    .local v16, "tmpOrg":Ljava/lang/StringBuffer;
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 216
    .local v15, "tmpCov":Ljava/lang/StringBuffer;
    const/4 v12, 0x1

    .line 217
    .local v12, "start":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v9, v17, -0x1

    .line 218
    .local v9, "end":I
    const/4 v13, 0x0

    .line 219
    .local v13, "step":I
    const/4 v7, -0x1

    .line 220
    .local v7, "cur":I
    const/4 v11, 0x0

    .line 221
    .local v11, "hexStr":Ljava/lang/String;
    const/4 v5, 0x0

    .line 222
    .local v5, "codeOrg":C
    const/4 v4, 0x1

    .line 223
    .local v4, "bSet":Z
    const/4 v3, 0x0

    .line 224
    .local v3, "bNeedConvert":Z
    const/4 v2, 0x0

    .line 226
    .local v2, "bFlushBuffer":Z
    :cond_0
    :goto_0
    const-string v17, "\\"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v7

    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v7, v0, :cond_d

    .line 227
    if-nez v2, :cond_1

    if-ge v12, v7, :cond_3

    .line 228
    :cond_1
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_2

    .line 229
    if-eqz v3, :cond_5

    move-object/from16 v17, v15

    :goto_1
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 230
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 231
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 233
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 234
    const/4 v3, 0x0

    .line 235
    const/4 v2, 0x0

    .line 238
    :cond_3
    add-int/lit8 v17, v7, 0x2

    move/from16 v0, v17

    if-gt v0, v9, :cond_c

    .line 239
    add-int/lit8 v17, v7, 0x1

    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v10

    .line 240
    .local v10, "escapeCode":C
    add-int/lit8 v7, v7, 0x2

    .line 241
    const/4 v13, 0x0

    .line 242
    packed-switch v10, :pswitch_data_0

    .line 291
    .end local v10    # "escapeCode":C
    :cond_4
    :goto_2
    :pswitch_0
    add-int v12, v7, v13

    .line 292
    if-le v12, v9, :cond_0

    .line 293
    move v12, v9

    goto :goto_0

    :cond_5
    move-object/from16 v17, v16

    .line 229
    goto :goto_1

    .line 244
    .restart local v10    # "escapeCode":C
    :pswitch_1
    const/4 v13, 0x4

    .line 245
    add-int/lit8 v17, v7, 0x4

    move/from16 v0, v17

    if-gt v0, v9, :cond_b

    .line 246
    add-int/lit8 v17, v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 247
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v11, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    int-to-char v5, v0

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->mCovertMap:Ljava/util/Map;

    move-object/from16 v17, v0

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;

    .line 250
    .local v6, "covInfo":Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
    const/4 v4, 0x1

    .line 251
    if-eqz v6, :cond_8

    .line 252
    iget-boolean v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isAlways:Z

    move/from16 v17, v0

    if-nez v17, :cond_6

    iget-boolean v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isExistInSystemFont:Z

    move/from16 v17, v0

    if-nez v17, :cond_6

    .line 253
    const/4 v3, 0x1

    .line 254
    :cond_6
    iget-object v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->conv:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_7

    .line 255
    const/4 v4, 0x0

    .line 256
    :cond_7
    iget-boolean v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isAlways:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    .line 257
    iget-object v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->conv:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 260
    :cond_8
    if-eqz v4, :cond_4

    .line 261
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 262
    if-eqz v6, :cond_9

    .line 263
    iget-object v0, v6, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->conv:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 288
    .end local v6    # "covInfo":Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
    .end local v10    # "escapeCode":C
    :catch_0
    move-exception v8

    .line 289
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    add-int v12, v7, v13

    .line 292
    if-le v12, v9, :cond_0

    .line 293
    move v12, v9

    goto/16 :goto_0

    .line 265
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v6    # "covInfo":Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
    .restart local v10    # "escapeCode":C
    :cond_9
    :try_start_2
    invoke-virtual {v15, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 291
    .end local v6    # "covInfo":Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
    .end local v10    # "escapeCode":C
    :catchall_0
    move-exception v17

    add-int v12, v7, v13

    .line 292
    if-le v12, v9, :cond_a

    .line 293
    move v12, v9

    :cond_a
    throw v17

    .line 268
    .restart local v10    # "escapeCode":C
    :cond_b
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "out of index : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 272
    :pswitch_2
    const-string v17, "\t"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 273
    const-string v17, "\t"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 276
    :pswitch_3
    const-string v17, "\n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    const-string v17, "\n"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 278
    const/4 v2, 0x1

    .line 279
    goto/16 :goto_2

    .line 281
    :pswitch_4
    const-string v17, "\r"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 282
    const-string v17, "\r"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 286
    .end local v10    # "escapeCode":C
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "out of index : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 297
    :cond_d
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_e

    .line 298
    if-eqz v3, :cond_10

    move-object/from16 v17, v15

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 299
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 300
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 302
    :cond_e
    if-ge v12, v9, :cond_f

    .line 303
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 306
    :cond_f
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    return-object v17

    :cond_10
    move-object/from16 v17, v16

    .line 298
    goto :goto_3

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x6e
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
