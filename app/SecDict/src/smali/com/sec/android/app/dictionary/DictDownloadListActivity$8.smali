.class Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;
.super Ljava/lang/Object;
.source "DictDownloadListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/DictDownloadListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 181
    .local v1, "pos":I
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 182
    .local v0, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iput-object v0, v2, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->dict_data:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 183
    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->isDownloading()Z

    move-result v2

    if-nez v2, :cond_3

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-static {v2}, Lcom/sec/android/app/dictionary/util/DictUtils;->isWIFIorETHERNETConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->TAG:Ljava/lang/String;

    const-string v3, "startDownload() : wifi on"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Dictionary_ConfigDataUsageWarning"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "TRUE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/dictionary/util/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->getDoNotShowCheck()Z

    move-result v2

    if-nez v2, :cond_1

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDialog(I)V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    invoke-static {v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$300(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    .line 202
    :goto_1
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # invokes: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDownloadGuideDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$400(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    goto :goto_1

    .line 200
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    invoke-static {v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$300(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->stopDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    goto :goto_1
.end method
