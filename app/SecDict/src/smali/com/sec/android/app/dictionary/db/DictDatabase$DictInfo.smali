.class public final Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;
.super Ljava/lang/Object;
.source "DictDatabase.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/db/DictDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DictInfo"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = ""

.field public static final FIELD_DICT_ORDER:Ljava/lang/String; = "dictOrder"

.field public static final FIELD_DICT_SIZE:Ljava/lang/String; = "size"

.field public static final FIELD_DICT_VERSION:Ljava/lang/String; = "version"

.field public static final FILED_DICT_NAME:Ljava/lang/String; = "name"

.field public static final NAME:Ljava/lang/String; = "dicts"

.field public static final SORT_BY_DICT_ORDER:Ljava/lang/String; = "dictOrder asc"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "content://com.sec.android.app.dictionary.provider/dicts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
