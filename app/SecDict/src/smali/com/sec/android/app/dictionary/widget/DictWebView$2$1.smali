.class Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;
.super Ljava/lang/Object;
.source "DictWebView.java"

# interfaces
.implements Landroid/webkit/ValueCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/widget/DictWebView$2;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/webkit/ValueCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

.field final synthetic val$pos:I

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView$2;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iput p2, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->val$pos:I

    iput-object p3, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->val$title:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onReceiveValue(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 141
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->onReceiveValue(Ljava/lang/String;)V

    return-void
.end method

.method public onReceiveValue(Ljava/lang/String;)V
    .locals 4
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 144
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/dictionary/widget/DictWebView;->unescapeJava(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "convert":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActionItemClicked()src : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActionItemClicked()out : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->val$pos:I

    packed-switch v1, :pswitch_data_0

    .line 162
    .end local v0    # "convert":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 151
    .restart local v0    # "convert":Ljava/lang/String;
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView;->mClipboard:Landroid/content/ClipboardManager;

    const/4 v2, 0x0

    invoke-static {v2, v0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictWebViewContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->val$title:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/dictionary/util/DictUtils;->startShareVia(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$2$1;->this$1:Lcom/sec/android/app/dictionary/widget/DictWebView$2;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView$2;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/widget/DictWebView;->mDictWebViewContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/app/dictionary/util/DictUtils;->startWebSearch(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
