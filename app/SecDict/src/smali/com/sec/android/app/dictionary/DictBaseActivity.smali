.class public Lcom/sec/android/app/dictionary/DictBaseActivity;
.super Landroid/app/Activity;
.source "DictBaseActivity.java"


# static fields
.field public static final EXTRA_KEY_FIRST_ACTIVITY:Ljava/lang/String; = "firstActivity"

.field public static final EXTRA_KEY_FORCE:Ljava/lang/String; = "force"

.field public static final EXTRA_KEY_KEYWORD:Ljava/lang/String; = "keyword"

.field public static final EXTRA_KEY_SEARCH_MGR_ID:Ljava/lang/String; = "searchMgrId"

.field public static final EXTRA_KEY_SEARCH_POSITION:Ljava/lang/String; = "searchId"

.field static sDefaultLocale:Ljava/util/Locale;

.field static sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;


# instance fields
.field protected TAG:Ljava/lang/String;

.field isPopupMode:Z

.field mActionBar:Landroid/app/ActionBar;

.field mHandler:Landroid/os/Handler;

.field mNewIntent:Landroid/content/Intent;

.field mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field mWebSearchButton:Landroid/widget/Button;

.field protected mWord:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    sput-object v0, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->isPopupMode:Z

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mHandler:Landroid/os/Handler;

    .line 60
    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mActionBar:Landroid/app/ActionBar;

    return-void
.end method

.method private onLocaleChanged(Ljava/util/Locale;)V
    .locals 7
    .param p1, "newLocale"    # Ljava/util/Locale;

    .prologue
    .line 226
    sget-object v4, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDefaultLocale:Ljava/util/Locale;

    invoke-virtual {p1, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 227
    sput-object p1, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDefaultLocale:Ljava/util/Locale;

    .line 228
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateLocale() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    sget-object v4, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v4}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getDictDataInfoMap()Landroid/util/SparseArray;

    move-result-object v1

    .line 230
    .local v1, "dictMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 231
    .local v3, "size":I
    if-lez v3, :cond_0

    .line 232
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 233
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 234
    .local v0, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    invoke-virtual {v0, p0}, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->updateShortName(Landroid/content/Context;)Ljava/lang/String;

    .line 232
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 238
    .end local v0    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v1    # "dictMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 6

    .prologue
    .line 187
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v5, "Base.onAttachedToWindow()"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-boolean v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->isPopupMode:Z

    if-eqz v4, :cond_2

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 191
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v4, :cond_1

    .line 192
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->getDefaultDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 193
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 194
    .local v2, "popUpWidth":I
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 196
    .local v1, "popUpHeight":I
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v4, v5, :cond_3

    .line 197
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/lit8 v4, v4, 0x3c

    div-int/lit8 v2, v4, 0x64

    .line 198
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 204
    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x35

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 206
    if-eqz v2, :cond_0

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 208
    :cond_0
    if-eqz v1, :cond_1

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 211
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    .end local v1    # "popUpHeight":I
    .end local v2    # "popUpWidth":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mPopupUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v3, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    .end local v3    # "view":Landroid/view/View;
    :cond_2
    return-void

    .line 200
    .restart local v0    # "dm":Landroid/util/DisplayMetrics;
    .restart local v1    # "popUpHeight":I
    .restart local v2    # "popUpWidth":I
    .restart local v3    # "view":Landroid/view/View;
    :cond_3
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v4, v4, 0x3c

    div-int/lit8 v2, v4, 0x64

    .line 201
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onLocaleChanged(Ljava/util/Locale;)V

    .line 149
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    sget-object v1, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->init(Landroid/content/Context;)V

    .line 69
    sget-boolean v1, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_POPUP_MODE:Z

    iput-boolean v1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->isPopupMode:Z

    .line 71
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->showActionbarOverflowMenu(Landroid/content/Context;)V

    .line 72
    iget-boolean v1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->isPopupMode:Z

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 74
    .local v0, "windowManager":Landroid/view/WindowManager$LayoutParams;
    const v1, 0x3ecccccd    # 0.4f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 78
    .end local v0    # "windowManager":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mActionBar:Landroid/app/ActionBar;

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 81
    sget-object v1, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDefaultLocale:Ljava/util/Locale;

    if-nez v1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    sput-object v1, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDefaultLocale:Ljava/util/Locale;

    .line 83
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "Base.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 89
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 94
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Base.onNewIntent() : mNewIntent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 159
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Base.onOptionsItemSelected() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 182
    :goto_0
    return v0

    .line 161
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onScrapbookClick()V

    .line 182
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 165
    :sswitch_1
    const-class v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->startActivity(Ljava/lang/Class;)V

    goto :goto_1

    .line 169
    :sswitch_2
    const-class v0, Lcom/sec/android/app/dictionary/DictAboutActivity;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->startActivity(Ljava/lang/Class;)V

    goto :goto_1

    .line 173
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "Base.onOptionsItemSelected() : finish()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->finish()V

    goto :goto_1

    .line 159
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_3
        0x7f0b0024 -> :sswitch_2
        0x7f0b0025 -> :sswitch_0
        0x7f0b0026 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 153
    const v0, 0x7f0b0025

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 141
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "onRestoreInstanceState()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "Base.onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new Intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onLocaleChanged(Ljava/util/Locale;)V

    .line 122
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 136
    return-void
.end method

.method onScrapbookClick()V
    .locals 2

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "text":Ljava/lang/String;
    const-string v0, "item_scrapbook"

    .line 243
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 244
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "Base.onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new Intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mHandler:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->getInstance(Lcom/sec/android/app/dictionary/DictBaseActivity;Landroid/os/Handler;)Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->install()V

    .line 109
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "Base.onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mHandler:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->getInstance(Lcom/sec/android/app/dictionary/DictBaseActivity;Landroid/os/Handler;)Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->stop()V

    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 129
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 217
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 218
    if-eqz p1, :cond_0

    .line 219
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->isPopupMode:Z

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 223
    :cond_0
    return-void
.end method

.method public startActivity(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-boolean v1, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startActivity() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startMeaningActivity(Lcom/sec/android/app/dictionary/DictSearchListAdapter;IZ)V
    .locals 3
    .param p1, "searchAdapter"    # Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    .param p2, "position"    # I
    .param p3, "firstActivity"    # Z

    .prologue
    .line 281
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 282
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "searchMgrId"

    iget v2, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->_id:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 283
    const-string v1, "searchId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 284
    const-string v1, "firstActivity"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 287
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 288
    sget-object v1, Lcom/sec/android/app/dictionary/DictBaseActivity;->sDictEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->addSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :goto_0
    return-void

    .line 289
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method startWebSearch()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWord:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/android/app/dictionary/util/DictUtils;->startWebSearch(Landroid/content/Context;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method updateActionBar(Z)V
    .locals 4
    .param p1, "firstActivity"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mActionBar:Landroid/app/ActionBar;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mActionBar:Landroid/app/ActionBar;

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 297
    return-void

    :cond_0
    move v0, v2

    .line 295
    goto :goto_0

    :cond_1
    move v1, v2

    .line 296
    goto :goto_1
.end method

.method updateWebSerchButton()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWebSearchButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    .line 248
    const v0, 0x7f0b0015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWebSearchButton:Landroid/widget/Button;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWebSearchButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWebSearchButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/dictionary/DictBaseActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/dictionary/DictBaseActivity$1;-><init>(Lcom/sec/android/app/dictionary/DictBaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isExistWebSearchActivity(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictBaseActivity;->mWebSearchButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 262
    :cond_0
    return-void
.end method
