.class public Lcom/sec/android/app/dictionary/widget/MoveAnimation;
.super Landroid/view/animation/Animation;
.source "MoveAnimation.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDuration:J

.field private mFromAlpha:F

.field private mFromScaleX:F

.field private mFromScaleY:F

.field private mFromXDelta:F

.field private mFromYDelta:F

.field private mPivotX:F

.field private mPivotY:F

.field private mToAlpha:F

.field private mToScaleX:F

.field private mToScaleY:F

.field private mToXDelta:F

.field private mToYDelta:F


# direct methods
.method public constructor <init>(FFFF)V
    .locals 4
    .param p1, "fromXDelta"    # F
    .param p2, "toXDelta"    # F
    .param p3, "fromYDelta"    # F
    .param p4, "toYDelta"    # F

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 41
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->TAG:Ljava/lang/String;

    .line 15
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mDuration:J

    .line 25
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleX:F

    .line 27
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleX:F

    .line 29
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleY:F

    .line 31
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleY:F

    .line 33
    iput v3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotX:F

    .line 35
    iput v3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotY:F

    .line 37
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromAlpha:F

    .line 39
    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToAlpha:F

    .line 42
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    .line 43
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    .line 44
    iput p3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    .line 45
    iput p4, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->init()V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " moveXdelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " moveYdelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    iget v3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fromXDelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " toXDelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fromYDelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " toYDelta : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    return-void
.end method

.method private final init()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mDuration:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setDuration(J)V

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setFillAfter(Z)V

    .line 55
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->initialize(IIII)V

    .line 56
    return-void
.end method

.method private swapMoveXY()V
    .locals 3

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    .line 137
    .local v0, "tempFromX":F
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    .line 138
    .local v1, "tempFromY":F
    iget v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    .line 139
    iput v0, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    .line 140
    iget v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    iput v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    .line 141
    iput v1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    .line 142
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 11
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 89
    const/high16 v3, 0x3f800000    # 1.0f

    .local v3, "scaleX":F
    const/high16 v4, 0x3f800000    # 1.0f

    .line 90
    .local v4, "scaleY":F
    iget v1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    .line 91
    .local v1, "f1":F
    iget v2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    .line 92
    .local v2, "f2":F
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->clear()V

    .line 93
    new-instance v5, Landroid/view/animation/Transformation;

    invoke-direct {v5}, Landroid/view/animation/Transformation;-><init>()V

    .line 95
    .local v5, "transformation":Landroid/view/animation/Transformation;
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_0

    .line 96
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToXDelta:F

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromXDelta:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v1, v6, v7

    .line 98
    :cond_0
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_1

    .line 99
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToYDelta:F

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromYDelta:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v2, v6, v7

    .line 101
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-eqz v6, :cond_2

    .line 102
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 103
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 104
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->clear()V

    .line 107
    :cond_2
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleX:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_3

    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleX:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_4

    .line 108
    :cond_3
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleX:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleX:F

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleX:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v3, v6, v7

    .line 110
    :cond_4
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleY:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_5

    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleY:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_6

    .line 111
    :cond_5
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleY:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleY:F

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleY:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v4, v6, v7

    .line 113
    :cond_6
    cmpl-float v6, v3, v10

    if-nez v6, :cond_7

    cmpl-float v6, v4, v10

    if-eqz v6, :cond_8

    .line 114
    :cond_7
    const/16 v6, 0x9

    new-array v0, v6, [F

    .line 115
    .local v0, "arrayOfFloat":[F
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 119
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotX:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_b

    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotY:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_b

    .line 120
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v3, v4, v9, v9}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 125
    :goto_0
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 126
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->clear()V

    .line 129
    .end local v0    # "arrayOfFloat":[F
    :cond_8
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromAlpha:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_9

    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToAlpha:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_a

    .line 130
    :cond_9
    iget v6, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromAlpha:F

    iget v7, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToAlpha:F

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromAlpha:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/view/animation/Transformation;->setAlpha(F)V

    .line 131
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 133
    :cond_a
    return-void

    .line 122
    .restart local v0    # "arrayOfFloat":[F
    :cond_b
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    const/4 v7, 0x2

    aget v7, v0, v7

    iget v8, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotX:F

    add-float/2addr v7, v8

    const/4 v8, 0x5

    aget v8, v0, v8

    iget v9, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotY:F

    add-float/2addr v8, v9

    invoke-virtual {v6, v3, v4, v7, v8}, Landroid/graphics/Matrix;->setScale(FFFF)V

    goto :goto_0
.end method

.method public reverseAnimation()V
    .locals 8

    .prologue
    .line 145
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 146
    .local v0, "curTime":J
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->getDuration()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->getStartTime()J

    move-result-wide v6

    sub-long v6, v0, v6

    sub-long v2, v4, v6

    .line 147
    .local v2, "remainDurationTime":J
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->swapMoveXY()V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->hasEnded()Z

    move-result v4

    if-nez v4, :cond_0

    .line 149
    sub-long v4, v0, v2

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setStartTime(J)V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    const-wide/16 v4, -0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->setStartTime(J)V

    goto :goto_0
.end method

.method public setAlpha(FF)V
    .locals 0
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromAlpha:F

    .line 77
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToAlpha:F

    .line 79
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mDuration:J

    .line 84
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 85
    return-void
.end method

.method public setFillAfter(Z)V
    .locals 0
    .param p1, "fillAfter"    # Z

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 61
    return-void
.end method

.method public setPivot(FF)V
    .locals 0
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotX:F

    .line 72
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mPivotY:F

    .line 73
    return-void
.end method

.method public setScale(FFFF)V
    .locals 0
    .param p1, "fromScaleX"    # F
    .param p2, "toScaleX"    # F
    .param p3, "fromScaleY"    # F
    .param p4, "toScaleY"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleX:F

    .line 65
    iput p2, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleX:F

    .line 66
    iput p3, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mFromScaleY:F

    .line 67
    iput p4, p0, Lcom/sec/android/app/dictionary/widget/MoveAnimation;->mToScaleY:F

    .line 68
    return-void
.end method
