.class Lcom/sec/android/app/dictionary/engine/DictEngineManager$1;
.super Ljava/lang/Object;
.source "DictEngineManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/engine/DictEngineManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/engine/DictEngineManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/engine/DictEngineManager;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager$1;->this$0:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)I
    .locals 3
    .param p1, "arg0"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .param p2, "arg1"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    const/16 v1, 0x2710

    .line 111
    iget-boolean v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v2, :cond_2

    iget v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 112
    .local v0, "diff":I
    :goto_0
    iget-boolean v2, p2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v2, :cond_0

    iget v1, p2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    :cond_0
    sub-int/2addr v0, v1

    .line 114
    if-nez v0, :cond_1

    .line 115
    iget v1, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    iget v2, p2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    sub-int v0, v1, v2

    .line 116
    :cond_1
    return v0

    .end local v0    # "diff":I
    :cond_2
    move v0, v1

    .line 111
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 108
    check-cast p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/dictionary/engine/DictEngineManager$1;->compare(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)I

    move-result v0

    return v0
.end method
