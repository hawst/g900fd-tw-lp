.class public Lcom/sec/android/app/dictionary/download/DictUpdater;
.super Landroid/os/AsyncTask;
.source "DictUpdater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictUpdater;->TAG:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 8
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 26
    iget-object v4, p0, Lcom/sec/android/app/dictionary/download/DictUpdater;->TAG:Ljava/lang/String;

    const-string v5, "DictUpdater() - start"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getList()Ljava/util/List;

    move-result-object v3

    .line 28
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 29
    .local v0, "i":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictUpdater;->isCancelled()Z

    move-result v4

    if-ne v4, v7, :cond_1

    .line 30
    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 45
    .end local v0    # "i":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :goto_1
    return-object v4

    .line 32
    .restart local v0    # "i":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_1
    iget-boolean v4, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v4, :cond_0

    .line 33
    iget-object v2, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 34
    .local v2, "info":Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;
    iget-object v4, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbPackageName:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/dictionary/download/DictSamsungApps;->getDownloadInfo(Ljava/lang/String;)Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    .line 35
    iget-object v4, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    if-eqz v4, :cond_2

    .line 36
    iget-object v4, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    iget v4, v4, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->versionCode:I

    iget v5, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    if-le v4, v5, :cond_2

    .line 37
    iget-object v4, p0, Lcom/sec/android/app/dictionary/download/DictUpdater;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DictUpdater() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    iput-boolean v7, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mUpdatable:Z

    goto :goto_0

    .line 42
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mUpdatable:Z

    goto :goto_0

    .line 45
    .end local v0    # "i":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v2    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;
    :cond_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictUpdater;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictUpdater;->TAG:Ljava/lang/String;

    const-string v1, "DictUpdater() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 58
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictUpdater;->TAG:Ljava/lang/String;

    const-string v1, "DictUpdater() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictUpdater;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 22
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Long;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Long;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/Long;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictUpdater;->onProgressUpdate([Ljava/lang/Long;)V

    return-void
.end method
