.class Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;
.super Ljava/lang/Object;
.source "DictMeaningViewActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 4
    .param p1, "status"    # I

    .prologue
    .line 116
    if-nez p1, :cond_3

    .line 117
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    .line 118
    .local v0, "loc":Ljava/util/Locale;
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mSearchItem:Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v0, v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mLocale:Ljava/util/Locale;

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, v0}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v1

    .line 122
    .local v1, "result":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 124
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    const-string v3, "Language is not available."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    .end local v0    # "loc":Ljava/util/Locale;
    .end local v1    # "result":I
    :cond_2
    :goto_0
    return-void

    .line 129
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$3;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    const-string v3, "Could not initialize TextToSpeech."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
