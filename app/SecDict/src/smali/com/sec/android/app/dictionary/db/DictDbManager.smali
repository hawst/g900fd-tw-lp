.class public Lcom/sec/android/app/dictionary/db/DictDbManager;
.super Ljava/lang/Object;
.source "DictDbManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/db/DictDbManager$DbQueryHelper;
    }
.end annotation


# static fields
.field private static final PREFERENCE_NAME:Ljava/lang/String; = "dictionary"

.field public static final PREF_CHECK_DATA_CONNECTION:Ljava/lang/String; = "check_data"

.field public static final PREF_CHECK_DATA_CONNECTION_ROAM:Ljava/lang/String; = "check_data_roam"

.field public static final instance:Lcom/sec/android/app/dictionary/db/DictDbManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/dictionary/db/DictDbManager;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/db/DictDbManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    const-string v1, "init()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mContext:Landroid/content/Context;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    .line 41
    :cond_1
    return-void
.end method

.method public load(Landroid/util/SparseArray;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dbMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 65
    .local v6, "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 70
    .local v8, "id":I
    invoke-virtual {p1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 71
    .local v9, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    if-eqz v9, :cond_0

    .line 72
    const-string v0, "dictOrder"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 73
    const-string v0, "size"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    .line 75
    :try_start_0
    const-string v0, "version"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_1
    iget-boolean v0, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-nez v0, :cond_0

    invoke-virtual {v9}, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->isChange()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/16 v0, 0x2710

    iput v0, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 84
    iput v10, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load() : reset : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p0, v9}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    goto :goto_0

    .line 77
    :catch_0
    move-exception v7

    .line 78
    .local v7, "e":Ljava/lang/NumberFormatException;
    iput v10, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    goto :goto_1

    .line 91
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v8    # "id":I
    .end local v9    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 93
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 94
    sget-object v0, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "load"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/dictionary/db/DictDbManager;->printDB(Landroid/net/Uri;Ljava/lang/String;)V

    .line 96
    :cond_2
    return-void
.end method

.method public loadPref(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mContext:Landroid/content/Context;

    const-string v2, "dictionary"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 188
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public printDB(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--- "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ---"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "dictOrder asc"

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 161
    .local v6, "c":Landroid/database/Cursor;
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 163
    .local v9, "str":Ljava/lang/StringBuffer;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v7

    .line 165
    .local v7, "count":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v7, :cond_0

    .line 166
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    const-string v0, ", "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 173
    .end local v7    # "count":I
    .end local v8    # "i":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    const-string v1, "--- end ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void
.end method

.method public removeItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 8
    .param p1, "dataInfo"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    new-array v3, v5, [Ljava/lang/String;

    iget v4, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v5, v3}, Lcom/sec/android/app/dictionary/db/DictDbManager$DbQueryHelper;->buildQuery(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 153
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    const-string v2, "removeItem:(%d) ID:%d O:%d, Name:%s, S:%d, V:%d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x2

    iget v5, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-wide v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget v5, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method

.method public save(Ljava/util/List;Z)V
    .locals 8
    .param p2, "always"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "dblist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    const/16 v7, 0x2710

    .line 99
    const/4 v1, 0x0

    .line 100
    .local v1, "count":I
    const/4 v0, 0x0

    .line 102
    .local v0, "bChanged":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 103
    .local v3, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    if-nez p2, :cond_1

    iget-boolean v4, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v4, :cond_3

    .line 104
    :cond_1
    iget v4, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    if-eq v4, v1, :cond_2

    .line 105
    iput v1, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 106
    invoke-virtual {p0, v3}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 107
    const/4 v0, 0x1

    .line 109
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    :cond_3
    iget v4, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    if-ge v4, v7, :cond_0

    .line 112
    iput v7, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 113
    const/4 v4, 0x0

    iput v4, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    .line 114
    iget-object v4, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "save() : reset : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p0, v3}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 116
    const/4 v0, 0x1

    goto :goto_0

    .line 120
    .end local v3    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_4
    sget-boolean v4, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v4, :cond_5

    if-eqz v0, :cond_5

    .line 121
    sget-object v4, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "save"

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/dictionary/db/DictDbManager;->printDB(Landroid/net/Uri;Ljava/lang/String;)V

    .line 122
    :cond_5
    return-void
.end method

.method public declared-synchronized saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 8
    .param p1, "dataInfo"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 126
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "_id"

    iget v3, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    const-string v2, "name"

    iget-object v3, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v2, "dictOrder"

    iget v3, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string v2, "size"

    iget-wide v4, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 130
    const-string v2, "version"

    iget v3, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget v7, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/sec/android/app/dictionary/db/DictDbManager$DbQueryHelper;->buildQuery(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 138
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/android/app/dictionary/db/DictDatabase$DictInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->TAG:Ljava/lang/String;

    const-string v3, "saveItem:(%d) ID:%d O:%d, Name:%s, S:%d, V:%d"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-wide v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget v6, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    monitor-exit p0

    return-void

    .line 125
    .end local v0    # "result":I
    .end local v1    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public savePref(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v2, p0, Lcom/sec/android/app/dictionary/db/DictDbManager;->mContext:Landroid/content/Context;

    const-string v3, "dictionary"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 180
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 181
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 182
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 183
    return-void
.end method
