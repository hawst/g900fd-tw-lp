.class public Lcom/sec/android/app/dictionary/diotek/DiotekDictList;
.super Lcom/diotek/diodict/engine/DBType;
.source "DiotekDictList.java"


# static fields
.field static final ADDDICT_PACKAGE_PREFIX:Ljava/lang/String; = "com.diotek.diodict3.sdictionary.adddict."

.field private static DEBUG:Z


# instance fields
.field mAdditionDbTypeMap:Landroid/util/SparseIntArray;

.field mContext:Landroid/content/Context;

.field mDictDataMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field mDisabledDictList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field mFullSupportDBinfoMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;",
            ">;"
        }
    .end annotation
.end field

.field mLocaleMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->IS_BUILD_TYPE_ENG:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->DEBUG:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Lcom/diotek/diodict/engine/DBType;-><init>()V

    .line 26
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    .line 28
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    .line 30
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mAdditionDbTypeMap:Landroid/util/SparseIntArray;

    .line 32
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDisabledDictList:Ljava/util/List;

    .line 34
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mFullSupportDBinfoMap:Landroid/util/SparseArray;

    .line 39
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mContext:Landroid/content/Context;

    .line 40
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->initSupportDBinfoMap()V

    .line 42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    const/4 v3, 0x2

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    const/4 v3, 0x0

    sget-object v4, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    const/16 v3, 0xa

    sget-object v4, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 45
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    const/16 v3, 0xc

    sget-object v4, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 46
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    const/4 v3, 0x4

    sget-object v4, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 48
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 49
    .local v16, "country":Ljava/lang/String;
    const-string v2, "ro.csc.countryiso_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 51
    .local v17, "countryISO":Ljava/lang/String;
    const-string v2, "KOREA"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "KR"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_0
    const/4 v14, 0x1

    .line 52
    .local v14, "bKR":Z
    :goto_0
    const-string v2, "JP"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "JP"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_1
    const/4 v13, 0x1

    .line 54
    .local v13, "bJP":Z
    :goto_1
    const-string v2, "CHINA"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "CN"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_2
    const/4 v11, 0x1

    .line 55
    .local v11, "bCN":Z
    :goto_2
    if-nez v11, :cond_3

    const-string v2, "HKTW"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_3
    const/4 v11, 0x1

    .line 56
    :goto_3
    if-nez v11, :cond_4

    const-string v2, "HONGKONG"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "HK"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_4
    const/4 v11, 0x1

    .line 57
    :goto_4
    if-nez v11, :cond_5

    const-string v2, "TAIWAN"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "TW"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_5
    const/4 v11, 0x1

    .line 59
    :goto_5
    const-string v2, "USA"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "US"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_6
    const/4 v15, 0x1

    .line 60
    .local v15, "bUS":Z
    :goto_6
    const-string v2, "Canada"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "CA"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_7
    const/4 v10, 0x1

    .line 61
    .local v10, "bCA":Z
    :goto_7
    if-nez v14, :cond_10

    if-nez v13, :cond_10

    if-nez v11, :cond_10

    if-nez v15, :cond_10

    if-nez v10, :cond_10

    const/4 v12, 0x1

    .line 63
    .local v12, "bEtc":Z
    :goto_8
    const/16 v3, 0x100

    const-string v4, "engeng"

    const v5, 0x7f070046

    const-wide/32 v6, 0x69ec49

    const/4 v8, 0x1

    const/high16 v9, 0x7f070000

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 66
    const/16 v3, 0xb04

    const-string v4, "engkor"

    const v5, 0x7f070077

    const-wide/32 v6, 0x11dd54c

    const v9, 0x7f07001b

    move-object/from16 v2, p0

    move v8, v14

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 67
    const/16 v3, 0xb03

    const-string v4, "koreng"

    const v5, 0x7f070078

    const-wide/32 v6, 0x97d4de

    const v9, 0x7f07001c

    move-object/from16 v2, p0

    move v8, v14

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 68
    const/16 v3, 0xb05

    const-string v4, "korkor"

    const v5, 0x7f070079

    const-wide/32 v6, 0x9e55e8

    if-eqz v14, :cond_11

    sget-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->isTablet:Z

    if-eqz v2, :cond_11

    const/4 v8, 0x1

    :goto_9
    const v9, 0x7f07001d

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 71
    const/16 v3, 0x702

    const-string v4, "engjpn"

    const v5, 0x7f07007a

    const-wide/32 v6, 0x4c2d8c

    const v9, 0x7f07001e

    move-object/from16 v2, p0

    move v8, v13

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 72
    const/16 v3, 0x703

    const-string v4, "jpneng"

    const v5, 0x7f07007b

    const-wide/32 v6, 0x6c3571

    const v9, 0x7f07001e

    move-object/from16 v2, p0

    move v8, v13

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 73
    const/16 v3, 0xb01

    const-string v4, "jpnkor"

    const v5, 0x7f07007c

    const-wide/32 v6, 0xe1d621

    const v9, 0x7f07001f

    move-object/from16 v2, p0

    move v8, v13

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 74
    const/16 v3, 0xb00

    const-string v4, "korjpn"

    const v5, 0x7f07007d

    const-wide/32 v6, 0x89beb9

    const v9, 0x7f070020

    move-object/from16 v2, p0

    move v8, v13

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 77
    const/16 v3, 0xd00

    const-string v4, "korchn"

    const v5, 0x7f07007e

    const-wide/32 v6, 0x50462e

    const v9, 0x7f070022

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 78
    const/16 v3, 0xd01

    const-string v4, "chnkor"

    const v5, 0x7f07007f

    const-wide/32 v6, 0x10be4fb

    const v9, 0x7f070021

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 79
    const/16 v3, 0x517

    const-string v4, "chneng"

    const v5, 0x7f070081

    const-wide/32 v6, 0x7a9eb1

    const v9, 0x7f070023

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 80
    const/16 v3, 0x516

    const-string v4, "engchn"

    const v5, 0x7f070080

    const-wide/32 v6, 0x507f88

    const v9, 0x7f070023

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 81
    const/16 v3, 0x51d

    const-string v4, "chteng"

    const v5, 0x7f070083

    const-wide/32 v6, 0x134cd0e

    const v9, 0x7f070024

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 82
    const/16 v3, 0x51c

    const-string v4, "engcht"

    const v5, 0x7f070082

    const-wide/32 v6, 0x693f2a

    const v9, 0x7f070024

    move-object/from16 v2, p0

    move v8, v11

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 85
    const/16 v3, 0x10c

    const-string v4, "engfra"

    const v5, 0x7f070047

    const-wide/32 v6, 0x33ab50

    const v9, 0x7f070001

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 86
    const/16 v3, 0x10d

    const-string v4, "fraeng"

    const v5, 0x7f070048

    const-wide/32 v6, 0x226e47

    const v9, 0x7f070001

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 87
    const/16 v3, 0x10e

    const-string v4, "engita"

    const v5, 0x7f070049

    const-wide/32 v6, 0x283b05

    const v9, 0x7f070002

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 88
    const/16 v3, 0x10f

    const-string v4, "itaeng"

    const v5, 0x7f07004a

    const-wide/32 v6, 0x261197

    const v9, 0x7f070002

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 89
    const/16 v3, 0x108

    const-string v4, "engpor"

    const v5, 0x7f07004b

    const-wide/32 v6, 0x111a20

    const v9, 0x7f070003

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 90
    const/16 v3, 0x109

    const-string v4, "poreng"

    const v5, 0x7f07004c

    const-wide/32 v6, 0x1409d8

    const v9, 0x7f070003

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 91
    const/16 v3, 0x116

    const-string v4, "enggre"

    const v5, 0x7f07004d

    const-wide/32 v6, 0x26fa08

    const v9, 0x7f070004

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 92
    const/16 v3, 0x117

    const-string v4, "greeng"

    const v5, 0x7f07004e

    const-wide/32 v6, 0x2ecb63

    const v9, 0x7f070004

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 93
    const/16 v3, 0x118

    const-string v4, "engpol"

    const v5, 0x7f07004f

    const-wide/32 v6, 0x14cc47

    const v9, 0x7f070005

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 94
    const/16 v3, 0x119

    const-string v4, "poleng"

    const v5, 0x7f070050

    const-wide/32 v6, 0x1846ec

    const v9, 0x7f070005

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 95
    const/16 v3, 0x121

    const-string v4, "engesp"

    const v5, 0x7f070051

    const-wide/32 v6, 0x4c1445

    const v9, 0x7f070006

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 96
    const/16 v3, 0x122

    const-string v4, "espeng"

    const v5, 0x7f070052

    const-wide/32 v6, 0x4b7259

    const v9, 0x7f070006

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 97
    const/16 v3, 0x123

    const-string v4, "engger"

    const v5, 0x7f070053

    const-wide/32 v6, 0x557433

    const v9, 0x7f070007

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 98
    const/16 v3, 0x124

    const-string v4, "gereng"

    const v5, 0x7f070054

    const-wide/32 v6, 0x71e405

    const v9, 0x7f070007

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 99
    const/16 v3, 0x127

    const-string v4, "engara"

    const v5, 0x7f070057

    const-wide/32 v6, 0x4decf4

    const v9, 0x7f070008

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 100
    const/16 v3, 0x128

    const-string v4, "araeng"

    const v5, 0x7f070058

    const-wide/32 v6, 0x16a229

    const v9, 0x7f070009

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 101
    const/16 v3, 0x51b

    const-string v4, "engmas"

    const v5, 0x7f070055

    const-wide/32 v6, 0x4a1124

    const v9, 0x7f07000a

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 102
    const/16 v3, 0x51a

    const-string v4, "maseng"

    const v5, 0x7f070056

    const-wide/32 v6, 0xccc8f

    const v9, 0x7f07000b

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 103
    const/16 v3, 0x200

    const-string v4, "engrus"

    const v5, 0x7f070059

    const-wide/32 v6, 0x4eef87

    const v9, 0x7f07000c

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 104
    const/16 v3, 0x201

    const-string v4, "ruseng"

    const v5, 0x7f07005a

    const-wide/32 v6, 0x3c5916

    const v9, 0x7f07000c

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 105
    const/16 v3, 0x204

    const-string v4, "engukr"

    const v5, 0x7f07005b

    const-wide/32 v6, 0x21b78d

    const v9, 0x7f07000c

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 106
    const/16 v3, 0x205

    const-string v4, "ukreng"

    const v5, 0x7f07005c

    const-wide/32 v6, 0x2f3e0a

    const v9, 0x7f07000c

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 107
    const/16 v3, 0x817

    const-string v4, "engtur"

    const v5, 0x7f07005d

    const-wide/32 v6, 0x182f8c

    const v9, 0x7f07000d

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 108
    const/16 v3, 0x816

    const-string v4, "tueng"

    const v5, 0x7f07005e

    const-wide/32 v6, 0x1b8a9c

    const v9, 0x7f07000d

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 109
    const/16 v3, 0xa30

    const-string v4, "engdut"

    const v5, 0x7f07005f

    const-wide/32 v6, 0x15a6ce

    const v9, 0x7f07000e

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 110
    const/16 v3, 0xa31

    const-string v4, "nleng"

    const v5, 0x7f070060

    const-wide/32 v6, 0x1cc223

    const v9, 0x7f07000e

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 111
    const/16 v3, 0xa20

    const-string v4, "engsv"

    const v5, 0x7f070061

    const-wide/32 v6, 0x2d8fa1

    const v9, 0x7f07000f

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 112
    const/16 v3, 0xa21

    const-string v4, "sweeng"

    const v5, 0x7f070062

    const-wide/32 v6, 0x2d652a

    const v9, 0x7f07000f

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 113
    const/16 v3, 0xa28

    const-string v4, "engnor"

    const v5, 0x7f070063

    const-wide/32 v6, 0x556018

    const v9, 0x7f070010

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 114
    const/16 v3, 0xa29

    const-string v4, "noreng"

    const v5, 0x7f070064

    const-wide/32 v6, 0x6bbd41

    const v9, 0x7f070010

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 115
    const/16 v3, 0xa24

    const-string v4, "engfin"

    const v5, 0x7f070065

    const-wide/32 v6, 0x443571

    const v9, 0x7f070011

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 116
    const/16 v3, 0xa25

    const-string v4, "fieng"

    const v5, 0x7f070066

    const-wide/32 v6, 0x373a56

    const v9, 0x7f070011

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 117
    const/16 v3, 0xa2a

    const-string v4, "engdan"

    const v5, 0x7f070067

    const-wide/32 v6, 0x2f2ce7

    const v9, 0x7f070012

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 118
    const/16 v3, 0xa2b

    const-string v4, "daneng"

    const v5, 0x7f070068

    const-wide/32 v6, 0x3dd213

    const v9, 0x7f070013

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 119
    const/16 v3, 0xa13

    const-string v4, "enggle"

    const v5, 0x7f070069

    const-wide/32 v6, 0x7d05f

    const v9, 0x7f070014

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 120
    const/16 v3, 0xa14

    const-string v4, "gleeng"

    const v5, 0x7f07006a

    const-wide/32 v6, 0x8368b

    const v9, 0x7f070014

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 121
    const/16 v3, 0xa02

    const-string v4, "engind"

    const v5, 0x7f07006b

    const-wide/32 v6, 0x4aa9b

    const v9, 0x7f070015

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 122
    const/16 v3, 0xa03

    const-string v4, "indeng"

    const v5, 0x7f07006c

    const-wide/32 v6, 0x94b8b

    const v9, 0x7f070015

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 123
    const/16 v3, 0xa1c

    const-string v4, "engtha"

    const v5, 0x7f07006d

    const-wide/32 v6, 0x8bb8cf

    const v9, 0x7f070016

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 124
    const/16 v3, 0xa1d

    const-string v4, "thaeng"

    const v5, 0x7f07006e

    const-wide/32 v6, 0x55ac5c

    const v9, 0x7f070016

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 125
    const/16 v3, 0xa04

    const-string v4, "engvie"

    const v5, 0x7f07006f

    const-wide/32 v6, 0xd531f7

    const v9, 0x7f070017

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 126
    const/16 v3, 0xa05

    const-string v4, "vieeng"

    const v5, 0x7f070070

    const-wide/32 v6, 0x786419

    const v9, 0x7f070017

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 127
    const/16 v3, 0xa09

    const-string v4, "enghin"

    const v5, 0x7f070071

    const-wide/32 v6, 0x53423

    const v9, 0x7f070018

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 128
    const/16 v3, 0xa0a

    const-string v4, "hineng"

    const v5, 0x7f070072

    const-wide/32 v6, 0x67751

    const v9, 0x7f070018

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 129
    const/16 v3, 0xa0f

    const-string v4, "engfar"

    const v5, 0x7f070073

    const-wide/32 v6, 0x3d3cb

    const v9, 0x7f070019

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 130
    const/16 v3, 0xa10

    const-string v4, "fareng"

    const v5, 0x7f070074

    const-wide/32 v6, 0x3ae1c

    const v9, 0x7f070019

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 131
    const/16 v3, 0xa0d

    const-string v4, "engurd"

    const v5, 0x7f070075

    const-wide/32 v6, 0x58e4b

    const v9, 0x7f07001a

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 132
    const/16 v3, 0xa19

    const-string v4, "urdeng"

    const v5, 0x7f070076

    const-wide/32 v6, 0x3f7bd

    const v9, 0x7f07001a

    move-object/from16 v2, p0

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->add(ILjava/lang/String;IJZI)V

    .line 134
    const/16 v2, 0x703

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x704

    aput v5, v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->addAdditionDB(I[I)V

    .line 135
    const/16 v2, 0xb01

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0xb02

    aput v5, v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->addAdditionDB(I[I)V

    .line 136
    return-void

    .line 51
    .end local v10    # "bCA":Z
    .end local v11    # "bCN":Z
    .end local v12    # "bEtc":Z
    .end local v13    # "bJP":Z
    .end local v14    # "bKR":Z
    .end local v15    # "bUS":Z
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 52
    .restart local v14    # "bKR":Z
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 54
    .restart local v13    # "bJP":Z
    :cond_a
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 55
    .restart local v11    # "bCN":Z
    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 56
    :cond_c
    const/4 v11, 0x0

    goto/16 :goto_4

    .line 57
    :cond_d
    const/4 v11, 0x0

    goto/16 :goto_5

    .line 59
    :cond_e
    const/4 v15, 0x0

    goto/16 :goto_6

    .line 60
    .restart local v15    # "bUS":Z
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 61
    .restart local v10    # "bCA":Z
    :cond_10
    const/4 v12, 0x0

    goto/16 :goto_8

    .line 68
    .restart local v12    # "bEtc":Z
    :cond_11
    const/4 v8, 0x0

    goto/16 :goto_9
.end method

.method private add(ILjava/lang/String;IJZI)V
    .locals 10
    .param p1, "dbtype"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "strID"    # I
    .param p4, "packageSize"    # J
    .param p6, "enable"    # Z
    .param p7, "copyRight"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mFullSupportDBinfoMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .line 172
    .local v1, "info":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    if-eqz v1, :cond_0

    .line 173
    iget-object v8, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    move-object v0, p0

    move v2, p3

    move-object v3, p2

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->newDictDataInfo(Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;ILjava/lang/String;JZI)Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    move-result-object v0

    invoke-virtual {v8, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 176
    :cond_0
    return-void
.end method

.method private addAdditionDB(I[I)V
    .locals 6
    .param p1, "baseDbType"    # I
    .param p2, "additionDbTypes"    # [I

    .prologue
    const/4 v5, 0x0

    .line 179
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->getDictDataInfo(I)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .line 180
    .local v2, "dict":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    if-eqz v2, :cond_1

    .line 181
    array-length v4, p2

    add-int/lit8 v4, v4, 0x1

    new-array v0, v4, [Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .line 182
    .local v0, "dbInfos":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    iget-object v4, v2, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    aget-object v4, v4, v5

    aput-object v4, v0, v5

    .line 183
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, p2

    if-ge v3, v4, :cond_0

    .line 184
    aget v1, p2, v3

    .line 185
    .local v1, "dbType":I
    add-int/lit8 v5, v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mFullSupportDBinfoMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    aput-object v4, v0, v5

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mAdditionDbTypeMap:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v1, p1}, Landroid/util/SparseIntArray;->put(II)V

    .line 183
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 188
    .end local v1    # "dbType":I
    :cond_0
    iput-object v0, v2, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .line 190
    .end local v0    # "dbInfos":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .end local v3    # "i":I
    :cond_1
    return-void
.end method

.method private initSupportDBinfoMap()V
    .locals 7

    .prologue
    .line 214
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getSupportDBinfo()[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    move-result-object v3

    .line 215
    .local v3, "infos":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    move-object v0, v3

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v0, v1

    .line 216
    .local v2, "info":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mFullSupportDBinfoMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v6

    invoke-virtual {v5, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v2    # "info":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_0
    return-void
.end method

.method private newDictDataInfo(Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;ILjava/lang/String;JZI)Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    .locals 4
    .param p1, "supportDBInfo"    # Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .param p2, "strID"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "packageSize"    # J
    .param p6, "enable"    # Z
    .param p7, "copyRight"    # I

    .prologue
    const/4 v3, 0x0

    .line 194
    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;-><init>()V

    .line 195
    .local v0, "item":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .line 196
    iget-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    aput-object p1, v1, v3

    .line 197
    invoke-virtual {p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDictName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mName:Ljava/lang/String;

    .line 198
    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDBPath:Ljava/lang/String;

    .line 199
    invoke-virtual {p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDictFilename()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDBFileName:Ljava/lang/String;

    .line 200
    iput p2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mShortNameResId:I

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->updateShortName(Landroid/content/Context;)Ljava/lang/String;

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.diotek.diodict3.sdictionary.adddict."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDbPackageName:Ljava/lang/String;

    .line 203
    iput-wide p4, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDbSize:J

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mLocaleMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getSourceLang()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mLocale:Ljava/util/Locale;

    .line 205
    iget-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mLocale:Ljava/util/Locale;

    if-nez v1, :cond_0

    .line 206
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mLocale:Ljava/util/Locale;

    .line 207
    :cond_0
    iput-boolean v3, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mIsAvailable:Z

    .line 208
    iput-boolean p6, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mCopyRight:Ljava/lang/String;

    .line 210
    return-object v0
.end method


# virtual methods
.method getDictDataInfo(I)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .locals 4
    .param p1, "dbtype"    # I

    .prologue
    .line 156
    iget-object v2, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 157
    .local v1, "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    if-nez v1, :cond_0

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mAdditionDbTypeMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 159
    .local v0, "additionDbType":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 162
    .end local v0    # "additionDbType":Ljava/lang/Integer;
    .restart local v1    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_0
    return-object v1
.end method

.method getDisabledDictList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDisabledDictList:Ljava/util/List;

    return-object v0
.end method

.method updateDictDataInfoList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "dblist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDisabledDictList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 142
    .local v2, "size":I
    if-lez v2, :cond_2

    .line 143
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDictDataMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .line 145
    .local v0, "dict":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    sget-boolean v3, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->DEBUG:Z

    if-nez v3, :cond_0

    iget-boolean v3, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    if-eqz v3, :cond_1

    .line 146
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    .line 147
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->mDisabledDictList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 153
    .end local v0    # "dict":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    .end local v1    # "i":I
    :cond_2
    return-void
.end method
