.class public Lcom/sec/android/app/dictionary/util/SharedDataStore;
.super Ljava/lang/Object;
.source "SharedDataStore.java"


# static fields
.field private static mIsCheckedShowDialog:Z

.field private static mPref:Landroid/content/SharedPreferences;

.field private static sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    sput-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    .line 32
    sput-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/dictionary/util/SharedDataStore;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    if-nez v0, :cond_1

    .line 36
    new-instance v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/util/SharedDataStore;-><init>()V

    sput-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    .line 37
    if-eqz p0, :cond_0

    .line 38
    const-string v0, "Dictionary"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    .line 41
    :cond_0
    invoke-static {}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->initValue()V

    .line 43
    :cond_1
    sget-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    return-object v0
.end method

.method private static initValue()V
    .locals 3

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->sInstance:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    const-string v1, "Dictionary_warning"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mIsCheckedShowDialog:Z

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public commitShowDataWarning()Z
    .locals 3

    .prologue
    .line 54
    sget-object v1, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 55
    const/4 v1, 0x0

    .line 59
    :goto_0
    return v1

    .line 57
    :cond_0
    sget-object v1, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "Dictionary_warning"

    sget-boolean v2, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mIsCheckedShowDialog:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 59
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    goto :goto_0
.end method

.method public getDoNotShowCheck()Z
    .locals 1

    .prologue
    .line 67
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mIsCheckedShowDialog:Z

    return v0
.end method

.method public setDoNotShowCheck(Z)V
    .locals 0
    .param p1, "bCheck"    # Z

    .prologue
    .line 63
    sput-boolean p1, Lcom/sec/android/app/dictionary/util/SharedDataStore;->mIsCheckedShowDialog:Z

    .line 64
    return-void
.end method
