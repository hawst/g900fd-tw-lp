.class public Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
.super Ljava/lang/Object;
.source "DictDataSearchItem.java"


# instance fields
.field public dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

.field public displayKeyword:Ljava/lang/String;

.field public keyword:Ljava/lang/String;

.field public next:[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

.field public preview:Ljava/lang/String;

.field public requestKeyword:Ljava/lang/String;

.field public searchKey:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->searchKey:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
