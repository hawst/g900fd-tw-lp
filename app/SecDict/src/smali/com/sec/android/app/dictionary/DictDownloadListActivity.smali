.class public Lcom/sec/android/app/dictionary/DictDownloadListActivity;
.super Lcom/sec/android/app/dictionary/DictBaseActivity;
.source "DictDownloadListActivity.java"

# interfaces
.implements Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;
.implements Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;


# static fields
.field private static mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;


# instance fields
.field private bDoNotCheck:Z

.field dict_data:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

.field private mCustomView:Landroid/view/View;

.field private mDailog:Landroid/app/Dialog;

.field private mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

.field private mDoneBtn:Landroid/widget/Button;

.field private mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

.field public onActinBarClick:Landroid/view/View$OnClickListener;

.field public onButtonLayoutTouch:Landroid/view/View$OnTouchListener;

.field public onDeleteClick:Landroid/view/View$OnClickListener;

.field public onDownloadClick:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictBaseActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->bDoNotCheck:Z

    .line 63
    new-instance v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$1;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onActinBarClick:Landroid/view/View$OnClickListener;

    .line 82
    new-instance v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$2;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onButtonLayoutTouch:Landroid/view/View$OnTouchListener;

    .line 177
    new-instance v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$8;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onDownloadClick:Landroid/view/View$OnClickListener;

    .line 207
    new-instance v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$9;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onDeleteClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private IntentForDialog(I)V
    .locals 3
    .param p1, "number"    # I

    .prologue
    .line 408
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 409
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 412
    const-string v1, "mobile_data_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 413
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 415
    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->startActivity(Landroid/content/Intent;)V

    .line 416
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/widget/ListViewWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    return-object v0
.end method

.method static synthetic access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->bDoNotCheck:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->bDoNotCheck:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/DictDownloadListActivity;
    .param p1, "x1"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDownloadGuideDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    return-void
.end method

.method public static getAdapter()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    return-object v0
.end method

.method private declared-synchronized showDataWarningDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Z)V
    .locals 10
    .param p1, "dict"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .param p2, "bRoaming"    # Z

    .prologue
    const v0, 0x7f070036

    .line 355
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->TAG:Ljava/lang/String;

    const-string v8, "showDataWarningDialog()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    if-eqz p2, :cond_0

    const-string v5, "check_data_roam"

    .line 360
    .local v5, "prefKey":Ljava/lang/String;
    :goto_0
    const-string v7, "1"

    sget-object v8, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/dictionary/db/DictDbManager;->loadPref(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 361
    if-eqz p2, :cond_1

    const v6, 0x7f070035

    .line 364
    .local v6, "titleId":I
    :goto_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_Dictionary_ConfigDataUsageWarning"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "TRUE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 367
    if-eqz p2, :cond_2

    .line 374
    .local v0, "bodyId":I
    :goto_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 375
    .local v4, "inflator":Landroid/view/LayoutInflater;
    const v7, 0x7f030001

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 376
    .local v2, "dialogView":Landroid/view/View;
    const v7, 0x7f0b0003

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 377
    .local v3, "doNotShow":Landroid/widget/CheckBox;
    new-instance v7, Lcom/sec/android/app/dictionary/DictDownloadListActivity$10;

    invoke-direct {v7, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$10;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    const v7, 0x7f0b0002

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 383
    .local v1, "bodyView":Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 385
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x104000a

    new-instance v9, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;

    invoke-direct {v9, p0, v5, v3, p1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Ljava/lang/String;Landroid/widget/CheckBox;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const/high16 v8, 0x1040000

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    .end local v0    # "bodyId":I
    .end local v1    # "bodyView":Landroid/widget/TextView;
    .end local v2    # "dialogView":Landroid/view/View;
    .end local v3    # "doNotShow":Landroid/widget/CheckBox;
    .end local v4    # "inflator":Landroid/view/LayoutInflater;
    .end local v6    # "titleId":I
    :goto_3
    monitor-exit p0

    return-void

    .line 357
    .end local v5    # "prefKey":Ljava/lang/String;
    :cond_0
    :try_start_1
    const-string v5, "check_data"

    goto :goto_0

    .line 361
    .restart local v5    # "prefKey":Ljava/lang/String;
    :cond_1
    const v6, 0x7f070031

    goto :goto_1

    .line 367
    .restart local v6    # "titleId":I
    :cond_2
    const v0, 0x7f070032

    goto :goto_2

    .line 370
    :cond_3
    if-eqz p2, :cond_4

    .restart local v0    # "bodyId":I
    :goto_4
    goto :goto_2

    .end local v0    # "bodyId":I
    :cond_4
    const v0, 0x7f070033

    goto :goto_4

    .line 402
    .end local v6    # "titleId":I
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    invoke-virtual {v7, p1}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    .line 403
    sget-object v7, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 355
    .end local v5    # "prefKey":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method private showDownloadGuideDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 3
    .param p1, "dict"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 419
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isFlightMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    invoke-direct {p0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->IntentForDialog(I)V

    .line 461
    :goto_0
    return-void

    .line 426
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isMobileDataOff(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 429
    invoke-direct {p0, v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->IntentForDialog(I)V

    goto :goto_0

    .line 433
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isRoamming(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isDataRoammingOff(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->IntentForDialog(I)V

    goto :goto_0

    .line 441
    :cond_2
    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDataWarningDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Z)V

    goto :goto_0

    .line 446
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isReachToDataLimit(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 449
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->IntentForDialog(I)V

    goto :goto_0

    .line 453
    :cond_4
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->isNoSignal(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 455
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->IntentForDialog(I)V

    goto :goto_0

    .line 460
    :cond_5
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDataWarningDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Z)V

    goto :goto_0
.end method

.method private updateActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDoneBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 307
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->invalidateOptionsMenu()V

    .line 308
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public endDrag()V
    .locals 0

    .prologue
    .line 321
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->releaseRotation(Landroid/app/Activity;)V

    .line 322
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 224
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 225
    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->setContentView(I)V

    .line 226
    const v1, 0x7f070027

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->setTitle(I)V

    .line 227
    invoke-static {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager(Landroid/content/Context;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    .line 230
    new-instance v1, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    sput-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    .line 232
    const v1, 0x7f0b0012

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    .line 233
    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->setListView(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    sget-object v2, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setOnMoveListener(Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setDragListener(Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnDragListener;)V

    .line 238
    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 240
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030002

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mCustomView:Landroid/view/View;

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mCustomView:Landroid/view/View;

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 242
    .local v0, "cancelBtn":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mCustomView:Landroid/view/View;

    const v2, 0x7f0b0005

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDoneBtn:Landroid/widget/Button;

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onActinBarClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDoneBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onActinBarClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mCustomView:Landroid/view/View;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->updateActionBar()V

    .line 248
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 9
    .param p1, "id"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 114
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    move v3, p1

    .line 116
    .local v3, "dialogId":I
    packed-switch p1, :pswitch_data_0

    .line 172
    :goto_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDailog:Landroid/app/Dialog;

    .line 173
    iget-object v6, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDailog:Landroid/app/Dialog;

    :goto_1
    return-object v6

    .line 118
    :pswitch_0
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/dictionary/util/SharedDataStore;

    move-result-object v5

    .line 119
    .local v5, "sds":Lcom/sec/android/app/dictionary/util/SharedDataStore;
    invoke-virtual {v5}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->getDoNotShowCheck()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 120
    iget-object v7, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    iget-object v8, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->dict_data:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    .line 121
    sget-object v7, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 125
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 126
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030009

    invoke-virtual {v4, v7, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 127
    .local v1, "customView":Landroid/view/View;
    const v6, 0x7f0b0018

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 128
    .local v0, "checkDoNotShow":Landroid/widget/CheckBox;
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 129
    new-instance v6, Lcom/sec/android/app/dictionary/DictDownloadListActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$3;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iput-boolean v8, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->bDoNotCheck:Z

    .line 135
    new-instance v6, Lcom/sec/android/app/dictionary/DictDownloadListActivity$4;

    invoke-direct {v6, p0, v3}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$4;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;I)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 144
    const v6, 0x7f070039

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 145
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 146
    new-instance v6, Lcom/sec/android/app/dictionary/DictDownloadListActivity$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$5;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    const v6, 0x7f070037

    new-instance v7, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;

    invoke-direct {v7, p0, v5}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Lcom/sec/android/app/dictionary/util/SharedDataStore;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    const v6, 0x7f070038

    new-instance v7, Lcom/sec/android/app/dictionary/DictDownloadListActivity$7;

    invoke-direct {v7, p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity$7;-><init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 336
    packed-switch p1, :pswitch_data_0

    .line 346
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 338
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setChangeOrderMode(Z)V

    .line 340
    sget-object v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onViewModeChanged()V

    .line 342
    const/4 v0, 0x1

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 277
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 287
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 279
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->setChangeOrderMode(Z)V

    .line 281
    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->makeChangeOrderItems()V

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onViewModeChanged()V

    goto :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0023
        :pswitch_0
    .end packed-switch
.end method

.method public onOrderChanged(II)V
    .locals 2
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 326
    if-eq p1, p2, :cond_0

    .line 327
    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->changeOrderItem(II)Z

    move-result v0

    .line 328
    .local v0, "isEnable":Z
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDoneBtn:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 329
    sget-object v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateChangeOrderView()V

    .line 331
    .end local v0    # "isEnable":Z
    :cond_0
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 110
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 111
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0b0023

    .line 258
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownList:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->isChangeOrderMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/dictionary/DictBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    return v2

    .line 261
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 262
    .local v1, "inflater":Landroid/view/MenuInflater;
    const/high16 v2, 0x7f0a0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 263
    sget-object v2, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->canChangeOrder()Z

    move-result v2

    if-nez v2, :cond_2

    .line 264
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 266
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager(Landroid/content/Context;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    .line 267
    .local v0, "dm":Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDownloadCount(I)I

    move-result v2

    if-lez v2, :cond_0

    .line 268
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method onViewModeChanged()V
    .locals 1

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->updateActionBar()V

    .line 351
    sget-object v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetInvalidated()V

    .line 352
    return-void
.end method

.method public startDrag()V
    .locals 0

    .prologue
    .line 316
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->lockRotation(Landroid/app/Activity;)V

    .line 317
    return-void
.end method
