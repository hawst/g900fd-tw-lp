.class public Lcom/sec/android/app/dictionary/download/DictDownloadManager;
.super Ljava/lang/Object;
.source "DictDownloadManager.java"


# static fields
.field public static final MAX_DOWNLOAD:I = 0x3

.field private static instance:Lcom/sec/android/app/dictionary/download/DictDownloadManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private cancelUpdateCheck()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/download/DictUpdater;->cancel(Z)Z

    .line 133
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;

    .line 134
    return-void
.end method

.method public static getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager(Landroid/content/Context;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    return-object v0
.end method

.method public static getDictDownloadManager(Landroid/content/Context;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->instance:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->instance:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    .line 76
    :cond_0
    if-eqz p0, :cond_1

    .line 77
    sget-object v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->instance:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    iput-object p0, v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    .line 79
    :cond_1
    sget-object v0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->instance:Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    return-object v0
.end method

.method private getUIUpdater()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getAdapter()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    return-object v0
.end method

.method private updateCheck()V
    .locals 3

    .prologue
    .line 125
    new-instance v0, Lcom/sec/android/app/dictionary/download/DictUpdater;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/download/DictUpdater;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mUpdater:Lcom/sec/android/app/dictionary/download/DictUpdater;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/dictionary/download/DictUpdater;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 127
    return-void
.end method


# virtual methods
.method public getDownloadCount(I)I
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getList()Ljava/util/List;

    move-result-object v2

    .line 84
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 85
    .local v3, "listSize":I
    const/4 v4, 0x0

    .line 86
    .local v4, "retVal":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 87
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 88
    .local v1, "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v5, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget v5, v5, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    if-ne v5, p1, :cond_0

    .line 89
    add-int/lit8 v4, v4, 0x1

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_1
    return v4
.end method

.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getDictList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showCompleteToast(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "dictName"    # Ljava/lang/String;
    .param p2, "success"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "retVal":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 67
    return-void

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07002f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z
    .locals 3
    .param p1, "info"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=startDownload(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-virtual {p1}, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->isDownloading()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' added in download queue"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;-><init>(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    iput-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startScheduler()V

    .line 102
    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is now DOWNLOADING"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x0

    goto :goto_0
.end method

.method startScheduler()V
    .locals 3

    .prologue
    .line 119
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/dictionary/download/DictDownloadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 121
    return-void
.end method

.method public stopDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z
    .locals 3
    .param p1, "info"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=stopDownload called(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->reset(I)V

    .line 115
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 137
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 138
    .local v3, "retVal":Ljava/lang/StringBuffer;
    const-string v4, "print DictDownloadManager state\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getList()Ljava/util/List;

    move-result-object v1

    .line 140
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 141
    .local v2, "listSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 142
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public updateDownloadProgress(Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getUIUpdater()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    .line 52
    .local v0, "adapter":Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    if-eqz v0, :cond_0

    .line 53
    iget-object v1, p1, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateDownloadProgress(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 55
    :cond_0
    return-void
.end method

.method public updateView(Z)V
    .locals 1
    .param p1, "update"    # Z

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getUIUpdater()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    .line 45
    .local v0, "adapter":Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0, p1}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 48
    :cond_0
    return-void
.end method
