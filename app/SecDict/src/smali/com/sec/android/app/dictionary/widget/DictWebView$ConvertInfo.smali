.class Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;
.super Ljava/lang/Object;
.source "DictWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/widget/DictWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConvertInfo"
.end annotation


# instance fields
.field conv:Ljava/lang/String;

.field isAlways:Z

.field isExistInSystemFont:Z

.field final synthetic this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView;IZ)V
    .locals 1
    .param p2, "str"    # I
    .param p3, "exist"    # Z

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    int-to-char v0, p2

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->conv:Ljava/lang/String;

    .line 194
    iput-boolean p3, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isExistInSystemFont:Z

    .line 195
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView;IZZ)V
    .locals 0
    .param p2, "str"    # I
    .param p3, "exist"    # Z
    .param p4, "always"    # Z

    .prologue
    .line 198
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;IZ)V

    .line 199
    iput-boolean p4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isAlways:Z

    .line 200
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "exist"    # Z

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->this$0:Lcom/sec/android/app/dictionary/widget/DictWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput-object p2, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->conv:Ljava/lang/String;

    .line 184
    iput-boolean p3, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isExistInSystemFont:Z

    .line 185
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;ZZ)V
    .locals 0
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "exist"    # Z
    .param p4, "always"    # Z

    .prologue
    .line 188
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;-><init>(Lcom/sec/android/app/dictionary/widget/DictWebView;Ljava/lang/String;Z)V

    .line 189
    iput-boolean p4, p0, Lcom/sec/android/app/dictionary/widget/DictWebView$ConvertInfo;->isAlways:Z

    .line 190
    return-void
.end method
