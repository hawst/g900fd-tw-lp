.class Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;
.super Ljava/lang/Object;
.source "DictMeaningViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/DictMeaningViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MeanBuffer"
.end annotation


# instance fields
.field bottom:Ljava/lang/String;

.field head:Ljava/lang/String;

.field htmlBuffer:Ljava/lang/StringBuffer;

.field textBuffer:Ljava/lang/StringBuffer;

.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictMeaningViewActivity;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->head:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->bottom:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->htmlBuffer:Ljava/lang/StringBuffer;

    .line 46
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->textBuffer:Ljava/lang/StringBuffer;

    return-void
.end method


# virtual methods
.method append(Ljava/lang/String;)V
    .locals 2
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->htmlBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->textBuffer:Ljava/lang/StringBuffer;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 67
    return-void
.end method

.method append([Ljava/lang/String;)V
    .locals 4
    .param p1, "src"    # [Ljava/lang/String;

    .prologue
    .line 49
    array-length v0, p1

    .line 51
    .local v0, "size":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 52
    const-string v1, ""

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->head:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->head:Ljava/lang/String;

    .line 55
    :cond_0
    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->append(Ljava/lang/String;)V

    .line 56
    const-string v1, ""

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->bottom:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x2

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->bottom:Ljava/lang/String;

    .line 62
    :cond_1
    :goto_0
    return-void

    .line 60
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->this$0:Lcom/sec/android/app/dictionary/DictMeaningViewActivity;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/DictMeaningViewActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "append() - String[] size ERROR!! : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method getHtml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->head:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->htmlBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->bottom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictMeaningViewActivity$MeanBuffer;->textBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
