.class Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;
.super Ljava/lang/Object;
.source "ListViewWidget.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/widget/ListViewWidget;->doAnimationForDrop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragDrawable:Lcom/sec/android/app/dictionary/widget/AnimateDrawable;
    invoke-static {v0, v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$302(Lcom/sec/android/app/dictionary/widget/ListViewWidget;Lcom/sec/android/app/dictionary/widget/AnimateDrawable;)Lcom/sec/android/app/dictionary/widget/AnimateDrawable;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOnMoveListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$400(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$500(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$500(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I
    invoke-static {}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$600()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I
    invoke-static {v1}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$500(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mOnMoveListener:Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$400(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;

    move-result-object v0

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I
    invoke-static {}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$600()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # getter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mDragPos:I
    invoke-static {v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$500(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/dictionary/widget/ListViewWidget$OnMoveListener;->onOrderChanged(II)V

    .line 254
    :goto_0
    const/4 v0, -0x1

    # setter for: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->mSrcDragPos:I
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$602(I)I

    .line 255
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/widget/ListViewWidget$2;->this$0:Lcom/sec/android/app/dictionary/widget/ListViewWidget;

    # invokes: Lcom/sec/android/app/dictionary/widget/ListViewWidget;->clearChildViewsAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/dictionary/widget/ListViewWidget;->access$700(Lcom/sec/android/app/dictionary/widget/ListViewWidget;)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 259
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 263
    return-void
.end method
