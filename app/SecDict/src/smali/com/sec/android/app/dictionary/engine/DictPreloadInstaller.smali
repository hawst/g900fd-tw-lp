.class public Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;
.super Ljava/lang/Object;
.source "DictPreloadInstaller.java"


# static fields
.field static final PATH_DST:Ljava/lang/String;

.field static final PATH_SRC:Ljava/lang/String;

.field static instance:Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;


# instance fields
.field TAG:Ljava/lang/String;

.field mContext:Lcom/sec/android/app/dictionary/DictBaseActivity;

.field mCount:I

.field mEnginManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

.field mHandler:Landroid/os/Handler;

.field mThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_PRELOAD_DB:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->PATH_SRC:Ljava/lang/String;

    .line 26
    sget-object v0, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->PATH_DST:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;-><init>()V

    sput-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->instance:Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    .line 22
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mEnginManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    return-void
.end method

.method public static getInstance(Lcom/sec/android/app/dictionary/DictBaseActivity;Landroid/os/Handler;)Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;
    .locals 1
    .param p0, "context"    # Lcom/sec/android/app/dictionary/DictBaseActivity;
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->instance:Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    iput-object p0, v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mContext:Lcom/sec/android/app/dictionary/DictBaseActivity;

    .line 40
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->instance:Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    iput-object p1, v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mHandler:Landroid/os/Handler;

    .line 41
    sget-object v0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->instance:Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;

    return-object v0
.end method


# virtual methods
.method copyFile(Ljava/io/FileInputStream;Ljava/lang/String;)V
    .locals 10
    .param p1, "inputStream"    # Ljava/io/FileInputStream;
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 166
    const/4 v1, 0x0

    .line 167
    .local v1, "fcin":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 168
    .local v6, "fcout":Ljava/nio/channels/FileChannel;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 169
    .local v7, "outFile":Ljava/io/File;
    const/4 v8, 0x0

    .line 171
    .local v8, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .local v9, "outputStream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 173
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 176
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    .line 177
    .local v4, "size":J
    const-wide/16 v2, 0x0

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    .line 178
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 184
    if-eqz v6, :cond_0

    .line 185
    :try_start_2
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 189
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 190
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 194
    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    .line 195
    :try_start_4
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 199
    :cond_2
    :goto_2
    if-eqz v9, :cond_3

    .line 200
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_3
    move-object v8, v9

    .line 204
    .end local v4    # "size":J
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    :cond_4
    :goto_3
    return-void

    .line 201
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "size":J
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    move-object v8, v9

    .line 203
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 180
    .end local v4    # "size":J
    :catch_1
    move-exception v0

    .line 181
    .local v0, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 184
    if-eqz v6, :cond_5

    .line 185
    :try_start_7
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 189
    :cond_5
    :goto_5
    if-eqz v1, :cond_6

    .line 190
    :try_start_8
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 194
    :cond_6
    :goto_6
    if-eqz p1, :cond_7

    .line 195
    :try_start_9
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 199
    :cond_7
    :goto_7
    if-eqz v8, :cond_4

    .line 200
    :try_start_a
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_3

    .line 201
    :catch_2
    move-exception v2

    goto :goto_3

    .line 183
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 184
    :goto_8
    if-eqz v6, :cond_8

    .line 185
    :try_start_b
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 189
    :cond_8
    :goto_9
    if-eqz v1, :cond_9

    .line 190
    :try_start_c
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    .line 194
    :cond_9
    :goto_a
    if-eqz p1, :cond_a

    .line 195
    :try_start_d
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    .line 199
    :cond_a
    :goto_b
    if-eqz v8, :cond_b

    .line 200
    :try_start_e
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    .line 202
    :cond_b
    :goto_c
    throw v2

    .line 186
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "size":J
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    goto :goto_0

    .line 191
    :catch_4
    move-exception v2

    goto :goto_1

    .line 196
    :catch_5
    move-exception v2

    goto :goto_2

    .line 186
    .end local v4    # "size":J
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "e":Ljava/io/IOException;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v2

    goto :goto_5

    .line 191
    :catch_7
    move-exception v2

    goto :goto_6

    .line 196
    :catch_8
    move-exception v2

    goto :goto_7

    .line 186
    .end local v0    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v3

    goto :goto_9

    .line 191
    :catch_a
    move-exception v3

    goto :goto_a

    .line 196
    :catch_b
    move-exception v3

    goto :goto_b

    .line 201
    :catch_c
    move-exception v3

    goto :goto_c

    .line 183
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    move-object v8, v9

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 180
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v0

    move-object v8, v9

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_4
.end method

.method deleteFiles(Ljava/io/File;)Z
    .locals 5
    .param p1, "path"    # Ljava/io/File;

    .prologue
    .line 207
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 208
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 209
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 210
    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->deleteFiles(Ljava/io/File;)Z

    .line 208
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 212
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 216
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v4

    return v4
.end method

.method public install()V
    .locals 4

    .prologue
    .line 45
    monitor-enter p0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mThread:Ljava/lang/Thread;

    if-nez v1, :cond_1

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mEnginManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->installAppData()V

    .line 49
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->PATH_SRC:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller$1;-><init>(Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mThread:Ljava/lang/Thread;

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mThread:Ljava/lang/Thread;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 60
    iget v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "install() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    .end local v0    # "dir":Ljava/io/File;
    :cond_0
    :goto_0
    monitor-exit p0

    .line 68
    return-void

    .line 64
    :cond_1
    iget v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "install() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method installFile(Ljava/io/File;Ljava/lang/String;)V
    .locals 9
    .param p1, "srcFile"    # Ljava/io/File;
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "filename":Ljava/lang/String;
    const/4 v0, 0x0

    .line 127
    .local v0, "dataInfo":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    const-string v6, "apk"

    const-string v7, "."

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 129
    new-instance v1, Lcom/sec/android/app/dictionary/download/DictFile;

    invoke-direct {v1, p1}, Lcom/sec/android/app/dictionary/download/DictFile;-><init>(Ljava/io/File;)V

    .line 130
    .local v1, "dictfile":Lcom/sec/android/app/dictionary/download/DictFile;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/dictionary/download/DictFile;->extractTo(Ljava/io/File;)Z

    .line 131
    iget-object v6, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mEnginManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iget-object v7, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mContext:Lcom/sec/android/app/dictionary/DictBaseActivity;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/dictionary/download/DictFile;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getDictDataInfo(Ljava/lang/String;)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_0

    .line 133
    iget-object v6, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mContext:Lcom/sec/android/app/dictionary/DictBaseActivity;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/dictionary/download/DictFile;->getVersionNumber(Landroid/content/Context;)I

    move-result v6

    iput v6, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    .line 134
    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/download/DictFile;->getSize()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    .line 135
    const/16 v6, -0x2710

    iput v6, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 136
    sget-object v6, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 149
    .end local v1    # "dictfile":Lcom/sec/android/app/dictionary/download/DictFile;
    :goto_0
    return-void

    .line 138
    .restart local v1    # "dictfile":Lcom/sec/android/app/dictionary/download/DictFile;
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dataInfo is null : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 141
    .end local v1    # "dictfile":Lcom/sec/android/app/dictionary/download/DictFile;
    :cond_1
    const/4 v4, 0x0

    .line 143
    .local v4, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 144
    .end local v4    # "in":Ljava/io/FileInputStream;
    .local v5, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->copyFile(Ljava/io/FileInputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 145
    :catch_0
    move-exception v2

    move-object v4, v5

    .line 146
    .end local v5    # "in":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to open "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 145
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method installFiles(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 80
    const/4 v1, 0x0

    .line 83
    .local v1, "count":I
    const/4 v6, 0x0

    .line 85
    .local v6, "files":[Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 86
    .local v9, "srcFile":Ljava/io/File;
    iget v10, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    if-lez v10, :cond_1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 87
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 88
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v2, "dstFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    .line 90
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 91
    iget-object v10, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mkdirs:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    invoke-virtual {v9}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 96
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    :try_start_0
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_4

    aget-object v5, v0, v7

    .line 97
    .local v5, "filename":Ljava/lang/String;
    iget v10, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    if-nez v10, :cond_2

    .line 98
    new-instance v10, Ljava/lang/Throwable;

    const-string v11, "Stop"

    invoke-direct {v10, v11}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .end local v5    # "filename":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :catch_0
    move-exception v3

    .line 112
    .local v3, "e":Ljava/lang/Throwable;
    iget-object v10, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    const-string v11, "stop installFiles()"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "dstFile":Ljava/io/File;
    .end local v3    # "e":Ljava/lang/Throwable;
    :cond_1
    :goto_1
    return v1

    .line 99
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "dstFile":Ljava/io/File;
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v7    # "i$":I
    .restart local v8    # "len$":I
    :cond_2
    :try_start_1
    new-instance v4, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 101
    invoke-virtual {p0, v4, p2}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->installFile(Ljava/io/File;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0, v4}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->deleteFiles(Ljava/io/File;)Z

    .line 103
    add-int/lit8 v1, v1, 0x1

    .line 96
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 105
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "is not file"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->installFiles(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    add-int/2addr v1, v10

    goto :goto_2

    .line 110
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "filename":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v9}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->deleteFiles(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 115
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "dstFile":Ljava/io/File;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_5
    invoke-virtual {p0, v9, p2}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->installFile(Ljava/io/File;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0, v9}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->deleteFiles(Ljava/io/File;)Z

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method notifyUI()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mEnginManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->updateDictList()Ljava/util/List;

    .line 153
    monitor-enter p0

    .line 154
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    if-lez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller$2;-><init>(Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 162
    :cond_0
    monitor-exit p0

    .line 163
    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 71
    monitor-enter p0

    .line 72
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    if-lez v0, :cond_0

    .line 73
    iget v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/dictionary/engine/DictPreloadInstaller;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    monitor-exit p0

    .line 77
    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
