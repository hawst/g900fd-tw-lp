.class Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;
.super Landroid/view/View$AccessibilityDelegate;
.source "DictDownloadListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 5
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 72
    const/16 v3, 0x40

    if-ne p2, v3, :cond_0

    .line 73
    const v3, 0x7f0b0008

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 74
    .local v2, "tv":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->mActivity:Lcom/sec/android/app/dictionary/DictDownloadListActivity;
    invoke-static {v3}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->access$000(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    move-result-object v3

    const v4, 0x7f07003e

    invoke-virtual {v3, v4}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "header":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 78
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 85
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "header":Ljava/lang/String;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v3

    return v3

    .line 82
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictDownloadListAdapter$2;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->access$100(Lcom/sec/android/app/dictionary/DictDownloadListAdapter;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "[ERROR] header_text View is null !!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
