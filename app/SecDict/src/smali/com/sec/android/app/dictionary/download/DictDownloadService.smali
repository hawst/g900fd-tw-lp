.class public Lcom/sec/android/app/dictionary/download/DictDownloadService;
.super Landroid/app/Service;
.source "DictDownloadService.java"


# static fields
.field public static mServiceContext:Landroid/content/Context;


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloadService;->TAG:Ljava/lang/String;

    return-void
.end method

.method private _startScheduler()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x0

    .line 31
    iget-object v9, p0, Lcom/sec/android/app/dictionary/download/DictDownloadService;->TAG:Ljava/lang/String;

    const-string v10, "startScheduler()"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager(Landroid/content/Context;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    .line 35
    .local v0, "dm":Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDownloadCount(I)I

    move-result v1

    .line 36
    .local v1, "downloadingCount":I
    invoke-virtual {v0, v12}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDownloadCount(I)I

    move-result v6

    .line 37
    .local v6, "readyCount":I
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDownloadCount(I)I

    move-result v8

    .line 38
    .local v8, "sucessCount":I
    invoke-virtual {v0, v13}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDownloadCount(I)I

    move-result v2

    .line 39
    .local v2, "failedCount":I
    iget-object v9, p0, Lcom/sec/android/app/dictionary/download/DictDownloadService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DOWNLOADIG:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", READY:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", SUCCESS:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", FAILED:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    if-ge v1, v13, :cond_0

    if-lez v6, :cond_0

    .line 43
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getList()Ljava/util/List;

    move-result-object v5

    .line 44
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    .line 45
    .local v7, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v7, :cond_0

    .line 46
    if-lt v1, v13, :cond_1

    .line 61
    .end local v3    # "i":I
    .end local v5    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .end local v7    # "size":I
    :cond_0
    return-void

    .line 50
    .restart local v3    # "i":I
    .restart local v5    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .restart local v7    # "size":I
    :cond_1
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v4, v9, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDownloadInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 51
    .local v4, "info":Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;
    if-eqz v4, :cond_2

    iget v9, v4, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    if-nez v9, :cond_2

    .line 52
    iget-object v9, p0, Lcom/sec/android/app/dictionary/download/DictDownloadService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "start download:\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v4, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v11, v11, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\', id:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v4, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v11, v11, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-virtual {v4, v12}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->reset(I)V

    .line 55
    new-instance v9, Lcom/sec/android/app/dictionary/download/DictDownloader;

    invoke-direct {v9, v4}, Lcom/sec/android/app/dictionary/download/DictDownloader;-><init>(Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;)V

    iput-object v9, v4, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

    .line 56
    iget-object v9, v4, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

    sget-object v10, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v11, v12, [Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/dictionary/download/DictDownloader;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 57
    add-int/lit8 v1, v1, 0x1

    .line 45
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/dictionary/download/DictDownloadService;->mServiceContext:Landroid/content/Context;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/download/DictDownloadService;->_startScheduler()V

    .line 27
    const/4 v0, 0x2

    return v0
.end method
