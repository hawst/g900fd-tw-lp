.class Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;
.super Ljava/lang/Object;
.source "DictDownloadListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/DictDownloadListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

.field final synthetic val$sds:Lcom/sec/android/app/dictionary/util/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Lcom/sec/android/app/dictionary/util/SharedDataStore;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iput-object p2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->val$sds:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 157
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$300(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->dict_data:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    .line 159
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->val$sds:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->bDoNotCheck:Z
    invoke-static {v1}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$200(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->setDoNotShowCheck(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$6;->val$sds:Lcom/sec/android/app/dictionary/util/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/util/SharedDataStore;->commitShowDataWarning()Z

    .line 162
    return-void
.end method
